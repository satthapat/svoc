ALTER TABLE vpm_data.U_CHG_LEAD_GEN_RETENTION_CAMPAIGN_BACKUP ADD response_flag int;
ALTER TABLE vpm_data.U_CHG_LEAD_GEN_RETENTION_CAMPAIGN_BACKUP ADD r_ttl_spending NUMERIC(21,6);
ALTER TABLE vpm_data.U_CHG_LEAD_GEN_RETENTION_CAMPAIGN_BACKUP ADD coupon_flag int;
ALTER TABLE vpm_data.U_CHG_LEAD_GEN_RETENTION_CAMPAIGN_BACKUP ADD campaign_start_date date;
ALTER TABLE vpm_data.U_CHG_LEAD_GEN_RETENTION_CAMPAIGN_BACKUP ADD campaign_end_date date;

CREATE TEMP TABLE u_chg_new_cultivation_campaign_offer_spendingtier_twd (
	segment varchar(20)
	,tier_spend int
	,offer_discount int
);

INSERT INTO u_chg_new_cultivation_campaign_offer_spendingtier_twd VALUES ('COMMERCIAL',4000,600);
INSERT INTO u_chg_new_cultivation_campaign_offer_spendingtier_twd VALUES ('HOME DECORATIVE SPENDERS',3000,450);
INSERT INTO u_chg_new_cultivation_campaign_offer_spendingtier_twd VALUES ('HOME IMPROVEMENT',4500,650);
INSERT INTO u_chg_new_cultivation_campaign_offer_spendingtier_twd VALUES ('HOME OWNER',2000,200);

CREATE temp TABLE temp_campaign_round_twd(campaign_round date,start_date date,end_date date);
INSERT INTO temp_campaign_round_twd VALUES ('2020-06-14','2020-06-19','2020-07-01');
INSERT INTO temp_campaign_round_twd VALUES ('2020-06-29','2020-07-03','2020-07-15');

update vpm_data.U_CHG_LEAD_GEN_RETENTION_CAMPAIGN_BACKUP 
SET campaign_start_date = t.start_date
	,campaign_end_date = t.end_date
FROM temp_campaign_round_twd t
WHERE vpm_data.U_CHG_LEAD_GEN_RETENTION_CAMPAIGN_BACKUP.campaign_sending_date = t.campaign_round
	AND vpm_data.U_CHG_LEAD_GEN_RETENTION_CAMPAIGN_BACKUP.bu_name = 'TWD';

DROP TABLE IF EXISTS txn_period;
CREATE temp TABLE txn_period as
SELECT customerid AS customer_id,transactiondate::date AS txn_date,spending AS txn_amt FROM analysis_data.salessku_twd2020 WHERE customerid <> '' AND spending > 0 AND transactiondate >= (SELECT min(start_date) FROM  temp_campaign_round_twd)
UNION ALL SELECT customerid AS customer_id,transactiondate::date AS txn_date,spending AS txn_amt FROM analysis_data.salessku_twd2021 WHERE customerid <> '' AND spending > 0 AND transactiondate <= (SELECT max(end_date) FROM  temp_campaign_round_twd);

DROP TABLE IF EXISTS lead_response;
SELECT t.customer_id,t.spending_tier,campaign_round,sum(txn_amt) AS r_ttl_spending INTO TEMP TABLE lead_response
FROM
(
	SELECT a.customer_id,a.spending_tier,b.* FROM vpm_data.U_CHG_LEAD_GEN_RETENTION_CAMPAIGN_BACKUP a
	LEFT JOIN temp_campaign_round_twd b ON a.campaign_sending_date = b.campaign_round
	WHERE a.campaign_round <> 1
		AND a.bu_name = 'TWD'
) T
INNER JOIN txn_period c ON t.customer_id = c.customer_id
WHERE c.txn_date BETWEEN t.start_date AND t.end_date
GROUP BY t.customer_id,t.spending_tier,campaign_round;

DROP TABLE IF EXISTS lead_response_final;
SELECT a.*,CASE WHEN r_ttl_spending > tier_spend1 THEN 1 ELSE 0 END AS coupon1_flag 
	,CASE WHEN r_ttl_spending > tier_spend2 THEN 1 ELSE 0 END AS coupon2_flag 
INTO TEMP TABLE lead_response_final
FROM lead_response a
LEFT JOIN u_chg_new_cultivation_campaign_offer_spendingtier_twd b
ON a.spending_tier = b.spending_tier;

update vpm_data.U_CHG_LEAD_GEN_RETENTION_CAMPAIGN_BACKUP
SET response_flag = NULL
	,r_ttl_spending = NULL
	,coupon1_flag = NULL
	,coupon2_flag = NULL;

update vpm_data.U_CHG_LEAD_GEN_RETENTION_CAMPAIGN_BACKUP
SET response_flag = 1
	,r_ttl_spending = t.r_ttl_spending
	,coupon1_flag = t.coupon1_flag
	,coupon2_flag = t.coupon2_flag
FROM lead_response_final t
WHERE vpm_data.U_CHG_LEAD_GEN_RETENTION_CAMPAIGN_BACKUP.customer_id = t.customer_id
	AND vpm_data.U_CHG_LEAD_GEN_RETENTION_CAMPAIGN_BACKUP.bu_name = 'TWD'
	AND vpm_data.U_CHG_LEAD_GEN_RETENTION_CAMPAIGN_BACKUP.campaign_sending_date = t.campaign_round;


-- BNB
CREATE TEMP TABLE u_chg_new_cultivation_campaign_offer_spendingtier_bnb (
	segment varchar(50)
	,tier_spend int
	,offer_discount int
);

INSERT INTO u_chg_new_cultivation_campaign_offer_spendingtier_bnb VALUES ('COMMERCIAL',4000,600);
INSERT INTO u_chg_new_cultivation_campaign_offer_spendingtier_bnb VALUES ('HOME DECORATIVE SPENDERS',2500,350);
INSERT INTO u_chg_new_cultivation_campaign_offer_spendingtier_bnb VALUES ('HOME IMPROVEMENT',3000,450);
INSERT INTO u_chg_new_cultivation_campaign_offer_spendingtier_bnb VALUES ('HOME OWNER',1200,150);


CREATE temp TABLE temp_campaign_round_bnb(brochure_date,start_date date,end_date date);
INSERT INTO temp_campaign_round_bnb VALUES ('BRO02-2021','2021-02-25','2021-03-14');
INSERT INTO temp_campaign_round_bnb VALUES ('BRO03-2021','2021-03-05','2021-03-31');

update vpm_data.U_CHG_LEAD_GEN_RETENTION_CAMPAIGN_BACKUP 
SET campaign_start_date = t.start_date
	,campaign_end_date = t.end_date
FROM temp_campaign_round_bnb t
WHERE vpm_data.U_CHG_LEAD_GEN_RETENTION_CAMPAIGN_BACKUP.brochure_date = t.brochure_date
	AND vpm_data.U_CHG_LEAD_GEN_RETENTION_CAMPAIGN_BACKUP.bu_name = 'BNB';

DROP TABLE IF EXISTS txn_period_bnb;
CREATE temp TABLE txn_period_bnb as
SELECT customerid AS customer_id,transactiondate::date AS txn_date,spending AS txn_amt FROM analysis_data.salessku_hws2020 WHERE customerid <> '' AND spending > 0 AND transactiondate >= (SELECT min(start_date) FROM  temp_campaign_round_bnb)
UNION ALL SELECT customerid AS customer_id,transactiondate::date AS txn_date,spending AS txn_amt FROM analysis_data.salessku_hws2021 WHERE customerid <> '' AND spending > 0 AND transactiondate <= (SELECT max(end_date) FROM  temp_campaign_round_bnb);

DROP TABLE IF EXISTS lead_response_bnb;
SELECT t.customer_id,t.spending_tier,campaign_round,sum(txn_amt) AS r_ttl_spending INTO TEMP TABLE lead_response_bnb
FROM
(
	SELECT a.customer_id,a.customer_segment,b.* FROM vpm_data.U_CHG_LEAD_GEN_RETENTION_CAMPAIGN_BACKUP a
	LEFT JOIN temp_campaign_round_bnb b ON a.brochure_date = b.brochure_date
	WHERE a.bu_name = 'BNB'
) T
INNER JOIN txn_period_bnb c ON t.customer_id = c.customer_id
WHERE c.txn_date BETWEEN t.start_date AND t.end_date
GROUP BY t.customer_id,t.spending_tier,brochure_date;

DROP TABLE IF EXISTS lead_response_final_bnb;
SELECT a.*,CASE WHEN r_ttl_spending > tier_spend THEN 1 ELSE 0 END AS coupon_flag
INTO TEMP TABLE lead_response_final_bnb
FROM lead_response_bnb a
LEFT JOIN u_chg_new_cultivation_campaign_offer_spendingtier_bnb b
ON a.spending_tier = b.spending_tier;


update vpm_data.U_CHG_LEAD_GEN_RETENTION_CAMPAIGN_BACKUP
SET response_flag = 1
	,r_ttl_spending = t.r_ttl_spending
	,coupon_flag = t.coupon1_flag
FROM lead_response_final_bnb t
WHERE vpm_data.U_CHG_LEAD_GEN_RETENTION_CAMPAIGN_BACKUP.customer_id = t.customer_id
	AND vpm_data.U_CHG_LEAD_GEN_RETENTION_CAMPAIGN_BACKUP.bu_name = 'BNB'
	AND vpm_data.U_CHG_LEAD_GEN_RETENTION_CAMPAIGN_BACKUP.brochure_date = t.brochure_date;


update vpm_data.U_CHG_LEAD_GEN_RETENTION_CAMPAIGN_BACKUP
SET response_flag = COALESCE(response_flag,0)
	,r_ttl_spending = COALESCE(r_ttl_spending,0)
	,coupon1_flag = COALESCE(coupon1_flag,0)
	,coupon2_flag = COALESCE(coupon2_flag,0);
