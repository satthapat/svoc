<<<<<<< HEAD
--finding txn lost from old schema, but occur in new schema
DROP TABLE IF EXISTS check_new_kids_txn;
DROP TABLE IF EXISTS old_kids_txn;
SELECT a.customer_id,member_number,sku_code,ticket_num
INTO temp TABLE old_kids_txn
SELECT * FROM vpm_data.pm_home_product_salessku_1yr LIMIT 1 a
LEFT JOIN analysis_data.customer_sbl b ON a.customer_id = b.customerid
WHERE bu_name = 'RBS' AND txn_date >= '2020-03-06' AND txn_date <= '2020-12-31';
SELECT member_number,sku_code,ticket_num,dept_code,txn_amt
INTO temp TABLE check_new_kids_txn
FROM vpm_data.pm_home_product_salessku_1yr_new_schema a
WHERE partner_code = 'RBS' AND txn_date >= '2020-03-06' AND txn_date <= '2020-12-31'
AND NOT EXISTS (SELECT * FROM old_kids_txn b WHERE a.sku_code::NUMERIC(38,0)=b.sku_code::NUMERIC(38,0)
AND a.ticket_num = b.ticket_num AND a.member_number = b.member_number);


--overall summary by deptcode in lost txn
--check whether deptcode is null,0,000,'' or not
SELECT dept_code,count(*),sum(txn_amt)
FROM check_new_kids_txn
GROUP BY dept_code
order BY CASE WHEN dept_code = '' OR dept_code = '0' OR dept_code = '000' THEN 0 ELSE 1 END
,sum(txn_amt) DESC;





--overall summary by deptcode in old schema
--check whether deptcode is null,0,000,'' or not	
SELECT deptcode,count(*),sum(spending)
FROM (SELECT a.customerid,b.member_number,skucode,deptcode,ticketnumber,spending
FROM analysis_data.salessku_rbs2020 a
LEFT JOIN analysis_data.customer_sbl b ON a.customerid = b.customerid
WHERE transactiondate::date >= '2020-03-06') a
WHERE EXISTS (SELECT * FROM check_new_kids_txn b
				WHERE a.spending = b.txn_amt AND a.ticketnumber::varchar(50) = b.ticket_num
				AND a.member_number = a.member_number)
GROUP BY deptcode
ORDER BY CASE WHEN deptcode IS NULL OR deptcode = '' OR deptcode = '000' OR deptcode = '0'
THEN 1 ELSE 0 END desc;



SELECT count(*),sum(spending)
FROM (SELECT a.customerid,b.member_number,skucode,deptcode,ticketnumber,spending
FROM analysis_data.salessku_rbs2020 a
LEFT JOIN analysis_data.customer_sbl b ON a.customerid = b.customerid
WHERE transactiondate::date >= '2020-03-06') a
WHERE EXISTS (SELECT * FROM check_new_kids_txn b
				WHERE a.spending = b.txn_amt AND a.ticketnumber::varchar(50) = b.ticket_num
				AND a.member_number = a.member_number);

			
			
SELECT count(*),sum(spending)
FROM (SELECT member_number,sku_id skucode,deptcode,receipt_no ticketnumber,net_price_tot spending
FROM analysis_data.sales_sku_rbs_2020 a
LEFT JOIN (SELECT DISTINCT pad_sku_id,dept_id AS deptcode FROM analysis_data.ms_product_cds) b ON a.sku_id = b.pad_sku_id
WHERE trans_date::date >= '2020-03-06') a
WHERE EXISTS (SELECT * FROM check_new_kids_txn b
				WHERE a.spending = b.txn_amt AND a.ticketnumber::varchar(50) = b.ticket_num
				AND a.member_number = a.member_number);

=======
DROP TABLE IF EXISTS DIFF_TABLE;
WITH OLD_TABLE AS(SELECT SKUcode AS sku_code,COUNT(*) AS TXN_OLD,SUM(spending) AS SALES_OLD
FROM analysis_data.salessku_rbs2020 a
INNER JOIN (SELECT DISTINCT skuid FROM analysis_data.productmaster_cds) b ON a.skucode = b.skuid
WHERE transactiondate::date >= '2020-03-06' AND transactiondate::date <= '2020-12-31'
GROUP BY skucode)
,NEW_TABLE AS (SELECT a.sku_id::NUMERIC(38,0) AS SKU_CODE,a.sku_id,COUNT(*) AS TXN_NEW,SUM(net_price_tot) AS SALES_NEW
FROM analysis_data.sales_sku_rbs_2020 a
INNER JOIN (SELECT DISTINCT pad_sku_id FROM analysis_data.ms_product_cds) b ON a.sku_id = b.pad_sku_id
WHERE trans_date::date >= '2020-03-06' AND Trans_DATE::date <= '2020-12-31'
GROUP BY a.sku_id)
SELECT A.*,B.sku_code AS sku_code_old,B.TXN_OLD,B.SALES_OLD
INTO TEMP TABLE DIFF_TABLE
FROM NEW_TABLE A
FULL OUTER JOIN OLD_TABLE B ON A.SKU_CODE = B.SKU_CODE::NUMERIC(38,0);



SELECT *
FROM (SELECT *,(COALESCE(SALES_NEW,0)-COALESCE(SALES_OLD,0)) AS diff_value
FROM DIFF_TABLE) a
ORDER BY diff_value


SELECT * FROM pm_skutagging_beauty_product_master_new_schema
WHERE sku_code = '12506104';

SELECT * FROM pm_skutagging_beauty_product_master
WHERE sku_code = '12506104';

SELECT * FROM analysis_data.productmaster_cds
WHERE skuid = '12506104'


SELECT * FROM analysis_data.ms_product_cds
WHERE sku_id = '12506104'

SELECT * FROM analysis_data.sales_sku_rbs_2020
WHERE sku_id = '0000000000000000012506104'

SELECT * FROM analysis_data.salessku_rbs2020
WHERE skucode = '12506104'

SELECT * FROM analysis_data.ms_product_cds
WHERE pad_sku_id = '0000000000000000006180065'

SELECT * FROM analysis_data.productmaster_cds
WHERE skuid = '6180065'



SELECT * FROM analysis_data.sales_sku_rbs_2020
WHERE receipt_no = '20121102140169202009121652'

1	2	523	2020-09-12 16:52:00	34B955FC-B8D7-4609-ADEF-B2D80458B74B	8025342907	20121102140169202009121652	12506104	4230.00






ORDER BY diff_value desc
LIMIT 10;


SELECT *
FROM (SELECT *,(SALES_NEW-SALES_OLD) AS diff_value
FROM DIFF_TABLE) a
ORDER BY diff_value DESC NULLs last
LIMIT 10;



SELECT CASE WHEN diff_value <0 THEN 'decrease' ELSE 'increase' END,
sum(diff_value)
FROM (SELECT *,(SALES_NEW-SALES_OLD) AS diff_value
FROM DIFF_TABLE) a
GROUP BY CASE WHEN diff_value <0 THEN 'decrease' ELSE 'increase' END
>>>>>>> branch 'master' of https://bambt@bitbucket.org/satthapat/svoc.git
