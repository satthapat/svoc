-------------------- BEAUTY
SELECT * FROM (
	SELECT 'BEAUTY_CONCERN_SEGMENT' AS FEATURE_NAME,COALESCE(BEAUTY_CONCERN_SEGMENT,'N/A') AS FEATURE_VALUE,COUNT(*) AS CNT
	FROM VPM_DATA.pm_skutagging_beauty_cust_segment 
	GROUP BY COALESCE(BEAUTY_CONCERN_SEGMENT,'N/A')
	UNION ALL
	SELECT 'BEAUTY_LUXURY_SEGMENT' AS FEATURE_NAME,COALESCE(CASE WHEN BEAUTY_TOTAL_SPEND > 0 OR OVERALL_LUXURY_SCORE > 0 THEN COALESCE(OVERALL_BEAUTY_LUXURY_SEGMENT,'MASS') ELSE 'N/A' END,'N/A') AS FEATURE_VALUE,COUNT(*) AS CNT
	FROM VPM_DATA.pm_skutagging_beauty_cust_segment 
	GROUP BY COALESCE(CASE WHEN BEAUTY_TOTAL_SPEND > 0 OR OVERALL_LUXURY_SCORE > 0 THEN COALESCE(OVERALL_BEAUTY_LUXURY_SEGMENT,'MASS') ELSE 'N/A' END,'N/A')
	UNION ALL
	SELECT 'BEAUTY_PERSONA_SEGMENT' AS FEATURE_NAME,COALESCE(BEAUTY_SEGMENT,'N/A') AS FEATURE_VALUE,COUNT(*) AS CNT
	FROM VPM_DATA.pm_skutagging_beauty_cust_segment 
	GROUP BY COALESCE(BEAUTY_SEGMENT,'N/A')
	UNION ALL
	SELECT 'CONCERN_ACNE_FLAG' AS FEATURE_NAME,COALESCE((CASE WHEN ACNE_FREQ >= 2 THEN 'YES' ELSE 'N/A' END),'N/A') AS FEATURE_VALUE,COUNT(*) AS CNT
	FROM VPM_DATA.pm_skutagging_beauty_cust_segment 
	GROUP BY COALESCE((CASE WHEN ACNE_FREQ >= 2 THEN 'YES' ELSE 'N/A' END),'N/A')
	UNION ALL
	SELECT 'CONCERN_ANTI_AGING_FLAG' AS FEATURE_NAME,COALESCE((CASE WHEN ANTI_AGING_FREQ >= 2 THEN 'YES' ELSE 'N/A' END),'N/A') AS FEATURE_VALUE,COUNT(*) AS CNT
	FROM VPM_DATA.pm_skutagging_beauty_cust_segment 
	GROUP BY COALESCE((CASE WHEN ANTI_AGING_FREQ >= 2 THEN 'YES' ELSE 'N/A' END),'N/A')
	UNION ALL
	SELECT 'CONCERN_MOISTURIZER_FLAG' AS FEATURE_NAME,COALESCE((CASE WHEN MOISTURIZER_FREQ >= 2 THEN 'YES' ELSE 'N/A' END),'N/A') AS FEATURE_VALUE,COUNT(*) AS CNT
	FROM VPM_DATA.pm_skutagging_beauty_cust_segment 
	GROUP BY COALESCE((CASE WHEN MOISTURIZER_FREQ >= 2 THEN 'YES' ELSE 'N/A' END),'N/A')
	UNION ALL
	SELECT 'CONCERN_SPOT_REDUCING_FLAG' AS FEATURE_NAME,COALESCE((CASE WHEN SPOT_REDUCING_FREQ >= 2 THEN 'YES' ELSE 'N/A' END),'N/A') AS FEATURE_VALUE,COUNT(*) AS CNT
	FROM VPM_DATA.pm_skutagging_beauty_cust_segment 
	GROUP BY COALESCE((CASE WHEN SPOT_REDUCING_FREQ >= 2 THEN 'YES' ELSE 'N/A' END),'N/A')
	UNION ALL
	SELECT 'CONCERN_SUN_PROTECTION_FLAG' AS FEATURE_NAME,COALESCE((CASE WHEN SUN_PROTECTION_FREQ >= 2 THEN 'YES' ELSE 'N/A' END),'N/A') AS FEATURE_VALUE,COUNT(*) AS CNT
	FROM VPM_DATA.pm_skutagging_beauty_cust_segment 
	GROUP BY COALESCE((CASE WHEN SUN_PROTECTION_FREQ >= 2 THEN 'YES' ELSE 'N/A' END),'N/A')
	UNION ALL
	SELECT 'CONCERN_WHITENING_FLAG' AS FEATURE_NAME,COALESCE((CASE WHEN WHITENING_FREQ >= 2 THEN 'YES' ELSE 'N/A' END),'N/A') AS FEATURE_VALUE,COUNT(*) AS CNT
	FROM VPM_DATA.pm_skutagging_beauty_cust_segment 
	GROUP BY COALESCE((CASE WHEN WHITENING_FREQ >= 2 THEN 'YES' ELSE 'N/A' END),'N/A')
	UNION ALL
	SELECT 'FRAGRANCE_LUXURY_SEGMENT' AS FEATURE_NAME, COALESCE(CASE WHEN CAT_FRAGRANCE_TICKET_SIZE > 0 /*OR CAT1.FRAGRANCE_LUXURY_SCORE > 0*/ THEN COALESCE(FRAGRANCE_LUXURY_SEGMENT,'MASS') ELSE 'N/A' END,'N/A') AS FEATURE_VALUE,COUNT(*) AS CNT
	FROM VPM_DATA.pm_skutagging_beauty_cust_segment 
	GROUP BY COALESCE(CASE WHEN CAT_FRAGRANCE_TICKET_SIZE > 0 /*OR CAT1.FRAGRANCE_LUXURY_SCORE > 0*/ THEN COALESCE(FRAGRANCE_LUXURY_SEGMENT,'MASS') ELSE 'N/A' END,'N/A')
	UNION ALL
	SELECT 'MAKEUP_LUXURY_SEGMENT' AS FEATURE_NAME,COALESCE( CASE WHEN MU_TICKET_SIZE > 0 /*OR CAT1.MAKEUP_LUXURY_SCORE > 0*/ THEN COALESCE(MAKEUP_LUXURY_SEGMENT,'MASS') ELSE 'N/A' END,'N/A') AS FEATURE_VALUE,COUNT(*) AS CNT
	FROM VPM_DATA.pm_skutagging_beauty_cust_segment 
	GROUP BY COALESCE( CASE WHEN MU_TICKET_SIZE > 0 /*OR CAT1.MAKEUP_LUXURY_SCORE > 0*/ THEN COALESCE(MAKEUP_LUXURY_SEGMENT,'MASS') ELSE 'N/A' END,'N/A')
	UNION ALL
	SELECT 'MAKEUP_PSYCHO_SEGMENT' AS FEATURE_NAME,COALESCE(MAKEUP_PSYCHO_SEGMENT,'N/A') AS FEATURE_VALUE,COUNT(*) AS CNT
	FROM VPM_DATA.pm_skutagging_beauty_cust_segment 
	GROUP BY COALESCE(MAKEUP_PSYCHO_SEGMENT,'N/A')
	UNION ALL
	SELECT 'SKINCARE_LUXURY_SEGMENT' AS FEATURE_NAME,COALESCE(CASE WHEN SKIN_TICKET_SIZE > 0 /*OR CAT1.SKINCARE_LUXURY_SCORE > 0*/ THEN COALESCE(SKINCARE_LUXURY_SEGMENT,'MASS') ELSE 'N/A' END,'N/A') AS FEATURE_VALUE,COUNT(*) AS CNT
	FROM VPM_DATA.pm_skutagging_beauty_cust_segment 
	GROUP BY COALESCE(CASE WHEN SKIN_TICKET_SIZE > 0 /*OR CAT1.SKINCARE_LUXURY_SCORE > 0*/ THEN COALESCE(SKINCARE_LUXURY_SEGMENT,'MASS') ELSE 'N/A' END,'N/A')
	UNION ALL
	SELECT 'SKINCARE_PSYCHO_SEGMENT' AS FEATURE_NAME,COALESCE(SKINCARE_PSYCHO_SEGMENT,'N/A') AS FEATURE_VALUE,COUNT(*) AS CNT
	FROM VPM_DATA.pm_skutagging_beauty_cust_segment 
	GROUP BY COALESCE(SKINCARE_PSYCHO_SEGMENT,'N/A')
	UNION ALL
	SELECT 'HBA_LUXURY_SEGMENT' AS FEATURE_NAME,COALESCE(HBA_LUXURY_SEGMENT,'N/A') AS FEATURE_VALUE,COUNT(*) AS CNT
	FROM VPM_DATA.pm_skutagging_beauty_cust_segment 
	GROUP BY COALESCE(HBA_LUXURY_SEGMENT,'N/A')
) A
ORDER BY FEATURE_NAME,FEATURE_VALUE;
