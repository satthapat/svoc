
-- create elec_gadget txn 
DROP TABLE IF EXISTS vpm_data.y_elec_gadget_commercial_txn 
;
SELECT *
INTO vpm_data.y_elec_gadget_commercial_txn
FROM vpm_data.pm_all_salessku_1yr
WHERE revised_category_level_1 = 'ELECTRONICS GADGET'
;

DROP TABLE IF EXISTS y_elec_gadget_commercial_cust_lvl1 --4.7M
;
SELECT member_number
	,revised_category_level_1
	,SUM(qty) AS total_qty
	,COUNT(DISTINCT txn_date) AS day_visit
	,SUM(txn_amt) AS total_sales
INTO temp table y_elec_gadget_commercial_cust_lvl1
FROM vpm_data.y_elec_gadget_commercial_txn
WHERE txn_amt <> 0
GROUP BY member_number
	,revised_category_level_1
;

--Qty per product
DROP TABLE IF EXISTS y_elec_gadget_commercial_qty_per_product
;
SELECT A.member_number
	,(A.partner_code || '-' || A.sku_code) AS bu_sku
	,MAX(A.product_name) AS product_name
	,MAX(A.cleaned_brandname) AS cleaned_brandname
	,MAX(A.revised_category_level_1) AS revised_category_level_1
--	,MAX(A.revised_category_level_2) AS revised_category_level_2
--	,MAX(A.revised_category_level_3) AS revised_category_level_3
	,SUM(A.qty) AS qty_per_product
	,COUNT(distinct TXN_DATE) as day_visit_per_product
	--elec_gadget
	,MAX(B.total_qty) AS elec_gadget_total_qty
	,MAX(B.day_visit) AS elec_gadget_day_visit
	,MAX(B.total_sales) AS elec_gadget_total_sales
INTO temp table y_elec_gadget_commercial_qty_per_product
FROM (SELECT * FROM vpm_data.y_elec_gadget_commercial_txn WHERE txn_amt <> 0) A
INNER JOIN (SELECT * FROM y_elec_gadget_commercial_cust_lvl1) B
ON A.member_number = B.member_number
GROUP BY A.member_number
	,(A.partner_code || '-' || A.sku_code) 
;

DROP TABLE IF EXISTS vpm_data.y_elec_gadget_commercial_all_dimension --- rename
;
SELECT member_number
	,elec_gadget_total_qty
	,elec_gadget_day_visit
	,elec_gadget_total_sales
	--Overall elec_gadget
	,MAX(CASE WHEN qty_row_num2 = 1 THEN product_name END) AS max_elec_gadget_product_name
	,MAX(CASE WHEN qty_row_num2 = 1 THEN qty_per_product END) AS max_elec_gadget_qty_per_product
	,MAX(CASE WHEN qty_row_num2 = 1 THEN day_visit_per_product END) AS max_elec_gadget_day_visit_per_product
	,MAX(CASE WHEN qty_row_num2 = 1 THEN qty_per_product/day_visit_per_product END) AS max_elec_gadget_qty_per_visit
INTO vpm_data.y_elec_gadget_commercial_all_dimension
FROM (SELECT *
			--,ROW_NUMBER() OVER(PARTITION BY member_number, revised_category_level_2 ORDER BY qty_per_product DESC) AS qty_row_num
			,ROW_NUMBER() OVER(PARTITION BY member_number ORDER BY qty_per_product DESC) AS qty_row_num2
	FROM y_elec_gadget_commercial_qty_per_product) A
--WHERE qty_row_num = 1
GROUP BY member_number
	,elec_gadget_total_qty
	,elec_gadget_day_visit
	,elec_gadget_total_sales
;

-- CREATE REF TABLE PERCENTILE OF ALL DIMENSIONS 
--DROP TABLE IF EXISTS vpm_data.y_elec_gadget_pct
--;
----Qty
--SELECT	'QUANTITY' as dimension
--		,'elec_gadget' as cate
--		,approximate percentile_disc (0.995) within group (order by elec_gadget_total_qty nulls first) as pct995
--		,approximate percentile_disc (0.999) within group (order by elec_gadget_total_qty nulls first) as pct999
--		,approximate percentile_disc (0.9998) within group (order by elec_gadget_total_qty nulls first) as pct9998
--INTO vpm_data.y_elec_gadget_pct
--FROM vpm_data.y_elec_gadget_commercial_all_dimension
--UNION ALL
----Max Qty by SKU
--SELECT	'MAX QUANTITY BY SKU' as dimension
--		,'elec_gadget' as cate
--		,approximate percentile_disc (0.995) within group (order by max_elec_gadget_qty_per_product nulls first) as pct995
--		,approximate percentile_disc (0.999) within group (order by max_elec_gadget_qty_per_product nulls first) as pct999
--		,approximate percentile_disc (0.9998) within group (order by max_elec_gadget_qty_per_product nulls first) as pct9998
--FROM vpm_data.y_elec_gadget_commercial_all_dimension
--UNION ALL		
----Spending
--SELECT	'SPENDING' as dimension
--		,'elec_gadget' as cate
--		,approximate percentile_disc (0.995) within group (order by elec_gadget_total_sales nulls first) as pct995
--		,approximate percentile_disc (0.999) within group (order by elec_gadget_total_sales nulls first) as pct999
--		,approximate percentile_disc (0.9998) within group (order by elec_gadget_total_sales nulls first) as pct9998
--FROM vpm_data.y_elec_gadget_commercial_all_dimension
--UNION ALL
----Day Visit
--SELECT	'DAY VISIT' as dimension
--		,'elec_gadget' as cate
--		,approximate percentile_disc (0.995) within group (order by elec_gadget_day_visit nulls first) as pct995
--		,approximate percentile_disc (0.999) within group (order by elec_gadget_day_visit nulls first) as pct999
--		,approximate percentile_disc (0.9998) within group (order by elec_gadget_day_visit nulls first) as pct9998
--FROM vpm_data.y_elec_gadget_commercial_all_dimension
--UNION ALL
----QTY PER VISIT
--SELECT	'QTY PER VISIT' as dimension
--		,'elec_gadget' as cate
--		,approximate percentile_disc (0.995) within group (order by max_elec_gadget_qty_per_visit nulls first) as pct995
--		,approximate percentile_disc (0.999) within group (order by max_elec_gadget_qty_per_visit nulls first) as pct999
--		,approximate percentile_disc (0.9998) within group (order by max_elec_gadget_qty_per_visit nulls first) as pct9998
--FROM vpm_data.y_elec_gadget_commercial_all_dimension
--ORDER BY cate, dimension
--;
--
--SELECT *
--from vpm_data.y_elec_gadget_pct
--order by dimension, cate
--;

/************************************************************ ADD COLUMN COMMERCIAL FLAG ************************************************************/
ALTER TABLE vpm_data.y_elec_gadget_commercial_all_dimension
ADD column elec_gadget_total_qty_score1 float
;
ALTER TABLE vpm_data.y_elec_gadget_commercial_all_dimension
ADD column max_elec_gadget_qty_per_product_score1 float
;

ALTER TABLE vpm_data.y_elec_gadget_commercial_all_dimension
ADD column elec_gadget_total_sales_score1 float
;

update vpm_data.y_elec_gadget_commercial_all_dimension
set elec_gadget_total_qty_score1 = case when elec_gadget_total_qty > 96 then 1 
										 when elec_gadget_total_qty < 0 then 0 
										 else elec_gadget_total_qty*1.0/96
									end
	,max_elec_gadget_qty_per_product_score1 = case when max_elec_gadget_qty_per_product > 80 then 1 
													 when max_elec_gadget_qty_per_product < 0 then 0 
													 else max_elec_gadget_qty_per_product*1.0/80
												end
	,elec_gadget_total_sales_score1 = case when elec_gadget_total_sales > 128000 then 1 
											 when elec_gadget_total_sales < 0 then 0 
											 else elec_gadget_total_sales*1.0/128000 
										end
												
;

ALTER TABLE vpm_data.y_elec_gadget_commercial_all_dimension
ADD COLUMN elec_gadget_score1 float
;

update vpm_data.y_elec_gadget_commercial_all_dimension
set elec_gadget_score1 = (elec_gadget_total_qty_score1*0.25) + (max_elec_gadget_qty_per_product_score1*0.5) + (elec_gadget_total_sales_score1*0.25)
;

ALTER TABLE vpm_data.y_elec_gadget_commercial_all_dimension
ADD COLUMN commercial_flag varchar(3)
;

update vpm_data.y_elec_gadget_commercial_all_dimension
set commercial_flag = case when elec_gadget_score1 > 0.90 and elec_gadget_total_sales >= 100000 then 'YES' else 'N/A' end
;

-- check
select count(*)
from vpm_data.y_elec_gadget_commercial_all_dimension
where commercial_flag = 'YES'




	