
-- create food_grocery txn 
DROP TABLE IF EXISTS vpm_data.y_food_grocery_commercial_txn 
;
SELECT *
INTO vpm_data.y_food_grocery_commercial_txn
FROM vpm_data.pm_all_salessku_1yr
WHERE revised_category_level_1 = 'FOOD GROCERY'
;

DROP TABLE IF EXISTS y_food_grocery_commercial_cust_lvl1
;
SELECT member_number
	,revised_category_level_1
	,SUM(qty) AS total_qty
	,COUNT(DISTINCT txn_date) AS day_visit
	,SUM(txn_amt) AS total_sales
INTO temp table y_food_grocery_commercial_cust_lvl1
FROM vpm_data.y_food_grocery_commercial_txn
WHERE txn_amt <> 0
GROUP BY member_number
	,revised_category_level_1
;

--Qty per product
DROP TABLE IF EXISTS y_food_grocery_commercial_qty_per_product
;
SELECT A.member_number
	,(A.partner_code || '-' || A.sku_code) AS bu_sku
	,MAX(A.product_name) AS product_name
	,MAX(A.cleaned_brandname) AS cleaned_brandname
	,MAX(A.revised_category_level_1) AS revised_category_level_1
--	,MAX(A.revised_category_level_2) AS revised_category_level_2
--	,MAX(A.revised_category_level_3) AS revised_category_level_3
	,SUM(A.qty) AS qty_per_product
	,COUNT(distinct TXN_DATE) as day_visit_per_product
	--food_grocery
	,MAX(B.total_qty) AS food_grocery_total_qty
	,MAX(B.day_visit) AS food_grocery_day_visit
	,MAX(B.total_sales) AS food_grocery_total_sales
INTO temp table y_food_grocery_commercial_qty_per_product
FROM (SELECT * FROM vpm_data.y_food_grocery_commercial_txn WHERE txn_amt <> 0) A
INNER JOIN (SELECT * FROM y_food_grocery_commercial_cust_lvl1) B
ON A.member_number = B.member_number
GROUP BY A.member_number
	,(A.partner_code || '-' || A.sku_code) 
;

DROP TABLE IF EXISTS vpm_data.y_food_grocery_commercial_all_dimension --- rename
;
SELECT member_number
	,food_grocery_total_qty
	,food_grocery_day_visit
	,food_grocery_total_sales
	--Overall food_grocery
	,MAX(CASE WHEN qty_row_num2 = 1 THEN product_name END) AS max_food_grocery_product_name
	,MAX(CASE WHEN qty_row_num2 = 1 THEN qty_per_product END) AS max_food_grocery_qty_per_product
	,MAX(CASE WHEN qty_row_num2 = 1 THEN day_visit_per_product END) AS max_food_grocery_day_visit_per_product
	,MAX(CASE WHEN qty_row_num2 = 1 THEN qty_per_product/day_visit_per_product END) AS max_food_grocery_qty_per_visit
INTO vpm_data.y_food_grocery_commercial_all_dimension
FROM (SELECT *
			--,ROW_NUMBER() OVER(PARTITION BY member_number, revised_category_level_2 ORDER BY qty_per_product DESC) AS qty_row_num
			,ROW_NUMBER() OVER(PARTITION BY member_number ORDER BY qty_per_product DESC) AS qty_row_num2
	FROM y_food_grocery_commercial_qty_per_product) A
--WHERE qty_row_num = 1
GROUP BY member_number
	,food_grocery_total_qty
	,food_grocery_day_visit
	,food_grocery_total_sales
;

-- CREATE REF TABLE PERCENTILE OF ALL DIMENSIONS 
--DROP TABLE IF EXISTS vpm_data.y_food_grocery_pct
--;
----Qty
--SELECT	'QUANTITY' as dimension
--		,'food_grocery' as cate
--		,approximate percentile_disc (0.995) within group (order by food_grocery_total_qty nulls first) as pct995
--		,approximate percentile_disc (0.999) within group (order by food_grocery_total_qty nulls first) as pct999
--		,approximate percentile_disc (0.9998) within group (order by food_grocery_total_qty nulls first) as pct9998
--INTO vpm_data.y_food_grocery_pct
--FROM vpm_data.y_food_grocery_commercial_all_dimension
--UNION ALL
----Max Qty by SKU
--SELECT	'MAX QUANTITY BY SKU' as dimension
--		,'food_grocery' as cate
--		,approximate percentile_disc (0.995) within group (order by max_food_grocery_qty_per_product nulls first) as pct995
--		,approximate percentile_disc (0.999) within group (order by max_food_grocery_qty_per_product nulls first) as pct999
--		,approximate percentile_disc (0.9998) within group (order by max_food_grocery_qty_per_product nulls first) as pct9998
--FROM vpm_data.y_food_grocery_commercial_all_dimension
--UNION ALL		
----Spending
--SELECT	'SPENDING' as dimension
--		,'food_grocery' as cate
--		,approximate percentile_disc (0.995) within group (order by food_grocery_total_sales nulls first) as pct995
--		,approximate percentile_disc (0.999) within group (order by food_grocery_total_sales nulls first) as pct999
--		,approximate percentile_disc (0.9998) within group (order by food_grocery_total_sales nulls first) as pct9998
--FROM vpm_data.y_food_grocery_commercial_all_dimension
--UNION ALL
----Day Visit
--SELECT	'DAY VISIT' as dimension
--		,'food_grocery' as cate
--		,approximate percentile_disc (0.995) within group (order by food_grocery_day_visit nulls first) as pct995
--		,approximate percentile_disc (0.999) within group (order by food_grocery_day_visit nulls first) as pct999
--		,approximate percentile_disc (0.9998) within group (order by food_grocery_day_visit nulls first) as pct9998
--FROM vpm_data.y_food_grocery_commercial_all_dimension
--UNION ALL
----QTY PER VISIT
--SELECT	'QTY PER VISIT' as dimension
--		,'food_grocery' as cate
--		,approximate percentile_disc (0.995) within group (order by max_food_grocery_qty_per_visit nulls first) as pct995
--		,approximate percentile_disc (0.999) within group (order by max_food_grocery_qty_per_visit nulls first) as pct999
--		,approximate percentile_disc (0.9998) within group (order by max_food_grocery_qty_per_visit nulls first) as pct9998
--FROM vpm_data.y_food_grocery_commercial_all_dimension
--ORDER BY cate, dimension
--;
--
--SELECT *
--from vpm_data.y_food_grocery_pct
--order by dimension, cate
--;

/************************************************************ ADD COLUMN COMMERCIAL FLAG ************************************************************/
ALTER TABLE vpm_data.y_food_grocery_commercial_all_dimension
ADD column food_grocery_total_qty_score1 float
;
ALTER TABLE vpm_data.y_food_grocery_commercial_all_dimension
ADD column max_food_grocery_qty_per_product_score1 float
;

ALTER TABLE vpm_data.y_food_grocery_commercial_all_dimension
ADD column food_grocery_total_sales_score1 float
;

update vpm_data.y_food_grocery_commercial_all_dimension
set food_grocery_total_qty_score1 = case when food_grocery_total_qty > 1768 then 1 
										 when food_grocery_total_qty < 0 then 0 
										 else food_grocery_total_qty*1.0/1768
									end
	,max_food_grocery_qty_per_product_score1 = case when max_food_grocery_qty_per_product > 332 then 1 
													 when max_food_grocery_qty_per_product < 0 then 0 
													 else max_food_grocery_qty_per_product*1.0/332
									end
	,food_grocery_total_sales_score1 = case when food_grocery_total_sales > 99000 then 1 
											 when food_grocery_total_sales < 0 then 0 
											 else food_grocery_total_sales*1.0/99000 
									end
												
;

ALTER TABLE vpm_data.y_food_grocery_commercial_all_dimension
ADD COLUMN food_grocery_score1 float
;

update vpm_data.y_food_grocery_commercial_all_dimension
set food_grocery_score1 = (food_grocery_total_qty_score1*0.25) + (max_food_grocery_qty_per_product_score1*0.5) + (food_grocery_total_sales_score1*0.25)
;

ALTER TABLE vpm_data.y_food_grocery_commercial_all_dimension
ADD COLUMN commercial_flag varchar(3)
;

update vpm_data.y_food_grocery_commercial_all_dimension
set commercial_flag = case when food_grocery_score1 > 0.90 and food_grocery_total_sales >= 100000 then 'YES' else 'N/A' end
;

-- check
select count(*)
from vpm_data.y_food_grocery_commercial_all_dimension
where commercial_flag = 'YES';


UPDATE VPM_DATA.PM_SKUTAGGING_FOOD_CUST_SEGMENT
SET FOOD_COMMERCIAL_FLAG = NULL
;


UPDATE VPM_DATA.PM_SKUTAGGING_FOOD_CUST_SEGMENT
SET FOOD_COMMERCIAL_FLAG = CASE WHEN commercial_flag='YES' THEN 1 END
FROM vpm_data.y_food_grocery_commercial_all_dimension A
WHERE A.MEMBER_NUMBER = VPM_DATA.PM_SKUTAGGING_FOOD_CUST_SEGMENT.MEMBER_NUMBER
;

UPDATE VPM_DATA.PM_SKUTAGGING_FOOD_CUST_SEGMENT
SET RESTAURANT_FLAG = NULL
,FOOD_HEALTH_SCORE = NULL
,READYTOEAT_SCORE = NULL
,FRUIT_SCORE = NULL
,MEAT_SCORE = NULL
,SEAFOOD_SCORE = NULL
,VEG_SCORE = NULL
,FOOD_HEALTH_FLAG = NULL
,RTE_FLAG = NULL
,FRUIT_LOVER_FLAG = NULL
,MEAT_LOVER_FLAG = NULL
,SEAFOOD_LOVER_FLAG = NULL
,VEG_FLAG = NULL
,FOOD_COMMERCIAL_SEGMENT = NULL;

--2.REATAURANT PURPOSE
--6,677

UPDATE VPM_DATA.PM_SKUTAGGING_FOOD_CUST_SEGMENT
SET RESTAURANT_FLAG = 1
WHERE FOOD_COMMERCIAL_FLAG = 1
AND FOOD_DAYS_SPEND >= 20	--FRQ
AND CNT_SKU >= 10	--PRD VARIETY 
AND (MAX_TYPE IN ('MEAT','VEGETABLE','SEAFOOD','MEAT&SEAFOOD','PASTA')
	OR MAX_CATEGORY_SKU IN ('FROZEN FOOD','FRESH')
	OR (PERISHABLE_AMT > 30000 AND PERISHABLE_QTY_ALL > 300)
	OR (MAX_CATEGORY_SKU IN ('READY TO EAT','INGREDIENT','LIQUOR')
		AND MAX_TYPE NOT IN ('INGREDIENT','KOREAN FOOD','OIL','OTHER','SWEETENER','BEER','SNACK','')))
AND FOOD_CNT_DATE_SPEND_1K >= 10
;

--3.COMMERCIAL WHO MAINLY BUY FRESH/PERISHABLE FOOD ; REATAURANT

UPDATE VPM_DATA.PM_SKUTAGGING_FOOD_CUST_SEGMENT
SET RESTAURANT_FLAG = 1
WHERE FOOD_COMMERCIAL_FLAG = 1 
		AND PERISHABLE_VISIT_PPT >= 0.4	--CHANGE
		AND FOOD_CNT_DATE_SPEND_1K >= 10
;

--2.1 HEALTH CONCERN

UPDATE VPM_DATA.PM_SKUTAGGING_FOOD_CUST_SEGMENT
SET FOOD_HEALTH_SCORE =  COALESCE(CASE WHEN HEALTH_RECENCY <= 7 THEN 1 ELSE (1-(((HEALTH_RECENCY-7)*1.0)/((180-7)*1.0))) END,0)*0.3
					+COALESCE((CASE WHEN HEALTH_FREQ >= 14 THEN 1 ELSE SQRT(HEALTH_FREQ*1.0/14) END)*(HEALTH_FREQ*1.0/FOOD_DAYS_SPEND),0)*0.4
					+COALESCE(((CASE WHEN SQRT(HEALTH_AMT) >= 45 THEN 45 ELSE SQRT(HEALTH_AMT) END)/45),0)*0.3
WHERE HEALTH_FREQ > 1 AND FOOD_COMMERCIAL_FLAG IS NULL AND HEALTH_AMT > 0
;
--2.2 READY TO EAT

UPDATE VPM_DATA.PM_SKUTAGGING_FOOD_CUST_SEGMENT
SET READYTOEAT_SCORE = CASE WHEN READYTOEAT_RECENCY  >180 THEN 0 ELSE (COALESCE(CASE WHEN READYTOEAT_RECENCY <= 9 THEN 1 ELSE (1-(((READYTOEAT_RECENCY-9)*1.0)/((180-9)*1.0))) END,0)*0.3) END
						+COALESCE((CASE WHEN READYTOEAT_FREQ >= 10 THEN 1 ELSE SQRT(READYTOEAT_FREQ*1.0/10) END)*(READYTOEAT_FREQ*1.0/FOOD_DAYS_SPEND),0)*0.4
						+COALESCE(((CASE WHEN SQRT(READYTOEAT_AMT) >= 32 THEN 32 ELSE SQRT(READYTOEAT_AMT) END)/32),0)*0.3
WHERE READYTOEAT_FREQ > 1 AND FOOD_COMMERCIAL_FLAG IS NULL AND READYTOEAT_AMT > 0
;


--2.4 FRUIT LOVER

UPDATE VPM_DATA.PM_SKUTAGGING_FOOD_CUST_SEGMENT
SET FRUIT_SCORE =  COALESCE(CASE WHEN FRUIT_RECENCY <= 8 THEN 1 ELSE (1-(((FRUIT_RECENCY-8)*1.0)/((180-8)*1.0))) END,0)*0.3
					+COALESCE((CASE WHEN FRUIT_FREQ >= 9 THEN 1 ELSE SQRT(FRUIT_FREQ*1.0/9) END)*(FRUIT_FREQ*1.0/FOOD_DAYS_SPEND),0)*0.4
					+COALESCE(CASE WHEN SQRT(FRUIT_AMT) >= 43 THEN 1 ELSE SQRT(FRUIT_AMT)/43 END,0)*0.3
WHERE FRUIT_FREQ > 1 AND FOOD_COMMERCIAL_FLAG IS NULL AND FRUIT_RECENCY <= 180 AND FRUIT_AMT > 0
;
--2.5 MEAT LOVER

UPDATE VPM_DATA.PM_SKUTAGGING_FOOD_CUST_SEGMENT
SET MEAT_SCORE =  COALESCE(CASE WHEN MEAT_RECENCY <= 8 THEN 1 ELSE (1-(((MEAT_RECENCY-8)*1.0)/((180-8)*1.0))) END,0)*0.3
					+COALESCE((CASE WHEN MEAT_FREQ >= 6 THEN 1 ELSE SQRT(MEAT_FREQ*1.0/6) END)*(MEAT_FREQ*1.0/FOOD_DAYS_SPEND),0)*0.4
					+COALESCE(CASE WHEN SQRT(MEAT_AMT) >= 28 THEN 1 ELSE SQRT(MEAT_AMT)/28 END,0)*0.3
WHERE MEAT_FREQ > 1 AND FOOD_COMMERCIAL_FLAG IS NULL AND MEAT_AMT > 0
;
--2.6 SEAFOOD

UPDATE VPM_DATA.PM_SKUTAGGING_FOOD_CUST_SEGMENT
SET SEAFOOD_SCORE =  COALESCE(CASE WHEN SEAFOOD_RECENCY <= 10 THEN 1 ELSE (1-(((SEAFOOD_RECENCY-10)*1.0)/((180-10)*1.0))) END,0)*0.3
					+COALESCE((CASE WHEN SEAFOOD_FREQ >= 4 THEN 1 ELSE SQRT(SEAFOOD_FREQ*1.0/4) END)*(SEAFOOD_FREQ*1.0/FOOD_DAYS_SPEND),0)*0.4
					+COALESCE(CASE WHEN SQRT(SEAFOOD_AMT) >= 26 THEN 1 ELSE SQRT(SEAFOOD_AMT)/26 END,0)*0.3
WHERE SEAFOOD_FREQ > 1 AND FOOD_COMMERCIAL_FLAG IS NULL AND SEAFOOD_AMT > 0
;

UPDATE VPM_DATA.PM_SKUTAGGING_FOOD_CUST_SEGMENT
SET VEG_SCORE = CASE WHEN VEG_RECENCY  >180 THEN 0 ELSE (COALESCE(CASE WHEN VEG_RECENCY <= 9 THEN 1 ELSE (1-(((VEG_RECENCY-9)*1.0)/((180-9)*1.0))) END,0)*0.3) END
						+COALESCE((CASE WHEN VEG_FREQ >= 8 THEN 1 ELSE SQRT(VEG_FREQ*1.0/8) END)*(VEG_FREQ*1.0/(FOOD_DAYS_SPEND*1.0)),0)*0.4
						+COALESCE(((CASE WHEN SQRT(VEG_AMT) >= 25.7 THEN 25.7 ELSE SQRT(VEG_AMT) END)/25.7),0)*0.3
WHERE VEG_FREQ > 1 AND FOOD_COMMERCIAL_FLAG IS NULL AND VEG_AMT > 0
;

-- 2.7 UPDATE FLAG , ADD NUM_KIDS

UPDATE VPM_DATA.PM_SKUTAGGING_FOOD_CUST_SEGMENT
SET	 FOOD_HEALTH_FLAG = CASE WHEN FOOD_HEALTH_SCORE > 0.6 THEN 1 END
	,RTE_FLAG = CASE WHEN READYTOEAT_SCORE > 0.6 THEN 1 END
	,FRUIT_LOVER_FLAG = CASE WHEN FRUIT_SCORE > 0.6 THEN 1 END
	,MEAT_LOVER_FLAG = CASE WHEN MEAT_SCORE > 0.6 THEN 1 END
	,SEAFOOD_LOVER_FLAG = CASE WHEN SEAFOOD_SCORE > 0.6 THEN 1 END
	,VEG_FLAG = CASE WHEN VEG_SCORE > 0.6 THEN 1 END
WHERE FOOD_COMMERCIAL_FLAG IS NULL
;


UPDATE VPM_DATA.PM_SKUTAGGING_FOOD_CUST_SEGMENT
SET FOOD_COMMERCIAL_SEGMENT = CASE WHEN MAX_CATEGORY_SKU = 'FRESH' OR MAX_CATEGORY_SKU = 'INGREDIENT' OR MAX_CATEGORY_SKU = 'FROZEN FOOD' THEN 'FRESH AND INGREDIENT'
								   WHEN MAX_CATEGORY_SKU = 'DRY FOOD' OR MAX_CATEGORY_SKU = 'BEVERAGE'  OR MAX_CATEGORY_SKU = 'DRY BEVERAGE' OR MAX_CATEGORY_SKU = 'CONFECTIONERY' THEN 'DRY FOOD AND BEVERAGE'
								   WHEN MAX_CATEGORY_SKU = 'LIQUOR' THEN 'LIQUOR'
							  ELSE 'OTHER' END
WHERE FOOD_COMMERCIAL_FLAG = 1
;

	