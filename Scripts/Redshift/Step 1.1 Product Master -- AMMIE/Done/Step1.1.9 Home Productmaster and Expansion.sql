
/*
	1.CREATE HOME PRODUCT MASTER
	
	SP_RENAME 'PM_HOME_PRODUCT_PRODUCT_MASTER','PM_HOME_PRODUCT_PRODUCT_MASTER_JUL'
*/

DROP TABLE IF EXISTS VPM_DATA.PM_HOME_PRODUCT_SALESSKU_1YR_OCT
;
SP_RENAME  'PM_HOME_PRODUCT_SALESSKU_1YR' ,'PM_HOME_PRODUCT_SALESSKU_1YR_NOV'
;

/* FLAG HOME PRODUCT*/

DROP TABLE IF EXISTS TEMP_HOME_PRODUCT_MASTER;
CREATE TEMP TABLE TEMP_HOME_PRODUCT_MASTER AS
SELECT	 BUID,BU_NAME,SKU_CODE,DEPT_CODE,SUBDEPT_CODE,CLASS_ID,SUBCLASS_ID
		,DEPT_NAME,SUBDEPT_NAME,CLASS_NAME,SUBCLASS_NAME
		,CLEANED_BRANDNAME,PRODUCT_NAME
		,GROUPING_CAT,CATEGORY_FINAL,TYPE_FINAL
		,CATEGORY_GROUP,CATEGORY		
FROM VPM_DATA.PM_PRODUCT_MASTER
WHERE HOME_FLAG = 1
;

UPDATE TEMP_HOME_PRODUCT_MASTER
SET GROUPING_CAT = CASE WHEN CLASS_NAME IN ('TABLE TOP','STORAGE UTENSILS','GLASS','LAUNDRY/CLEANING EQUIP.'
							,'LAUNDRY/CLEANING EQUIP.','KITCHEN UTENSILS','HOME S ACCESS.','ACCESS.')
							THEN 'HOME FINISHING'
							ELSE GROUPING_CAT END
;

UPDATE TEMP_HOME_PRODUCT_MASTER
SET CATEGORY_FINAL = CASE	WHEN SUBDEPT_NAME = 'FREESTAND / HOB - HOOD' THEN 'KITCHENWARE'
							WHEN DEPT_NAME = 'FAN & VACUUM' THEN 'HOUSEHOLD APPLIANCE'
							WHEN DEPT_NAME LIKE 'REF%IGERATOR%' THEN 'KITCHENWARE'
							WHEN DEPT_NAME = 'VISUAL PRODUCT' THEN 'HOME ENTERTAINMENT'
							WHEN DEPT_NAME = 'WASHER & DRYER' THEN 'HOUSEHOLD APPLIANCE'
							WHEN CLASS_NAME IN ('TABLE TOP','GLASS') THEN 'TABLETOPS'
							WHEN CLASS_NAME IN ('STORAGE UTENSILS','LAUNDRY/CLEANING EQUIP.'
							,'LAUNDRY/CLEANING EQUIP.','KITCHEN UTENSILS','HOME S ACCESS.','ACCESS.') THEN 'KITCHENWARE'
							ELSE CATEGORY_FINAL END
;

UPDATE TEMP_HOME_PRODUCT_MASTER
SET TYPE_FINAL = CASE WHEN SUBDEPT_NAME = 'FREESTAND / HOB - HOOD' THEN 'FREESTAND & HOB - HOOD'
					  WHEN DEPT_NAME LIKE '%REFRIGERATOR%' OR SUBDEPT_NAME LIKE '%REFRIGERATOR%' THEN 'REFRIGERATOR'
					  WHEN DEPT_NAME = 'VISUAL PRODUCT' THEN 'TV'
					  WHEN DEPT_NAME = 'WASHER & DRYER' THEN 'WASHER & DRYER'
					  WHEN SUBDEPT_NAME = 'COFFEE MAKER' THEN 'COFFEE MAKER'
					  WHEN SUBDEPT_NAME = 'ELECTRIC PAN / GRILLER' THEN 'ELECTRIC PAN & GRILLER'
					  WHEN CLASS_NAME = 'FRUIT EXTRACTOR' THEN 'JUICERS & FRUIT EXTRACTORS'
					  WHEN CLASS_NAME LIKE '%BLENDER%' THEN 'JUICERS & FRUIT EXTRACTORS'
					  WHEN SUBDEPT_NAME IN ('FREESTAND / HOB - HOOD') THEN 'FREESTAND & HOB - HOOD'
					  WHEN SUBDEPT_NAME = 'IRON' THEN 'IRON'
					  WHEN SUBDEPT_NAME = 'MICROWAVE' THEN 'MICROWAVE & OVEN'
					  WHEN SUBDEPT_NAME = 'MULTI COOKER' THEN 'MULTI COOKER'
					  WHEN SUBDEPT_NAME = 'THERMO POT' THEN 'THERMO POT'
					  WHEN SUBDEPT_NAME LIKE '%SANDWICH%MAKER%' THEN 'TOASTER & SANDWICH MAKER'
					  WHEN CLASS_NAME LIKE '%RICE%COOKER%' THEN 'RICE COOKER'
					  WHEN TYPE_FINAL != 'THERMO POT' AND PRODUCT_NAME LIKE '%��%������%' THEN 'THERMO POT'
					  ELSE TYPE_FINAL END
;

INSERT INTO TEMP_HOME_PRODUCT_MASTER
SELECT	 BUID,BU_NAME,SKU_CODE,DEPT_CODE,SUBDEPT_CODE,CLASS_ID,SUBCLASS_ID
		,DEPT_NAME,SUBDEPT_NAME,CLASS_NAME,SUBCLASS_NAME
		,CLEANED_BRANDNAME,PRODUCT_NAME
		,GROUPING_CAT,CATEGORY_FINAL,TYPE_FINAL
		,CATEGORY_GROUP,CATEGORY
FROM VPM_DATA.PM_PRODUCT_MASTER A
WHERE DEPT_NAME NOT LIKE '%DRY FOOD%'
AND SUBDEPT_NAME NOT LIKE '%AUTO%'
AND SUBDEPT_NAME NOT IN ('BILL & TOP UP','GIFT VOUCHER','PREMIUM','UNIFORM','GADGET','GIFT & PREMIUM','TRAVELLING')
AND BUID = '4'
AND NOT EXISTS (SELECT * FROM TEMP_HOME_PRODUCT_MASTER B WHERE A.BUID = B.BUID AND A.SKU_CODE = B.SKU_CODE)
;

INSERT INTO TEMP_HOME_PRODUCT_MASTER
SELECT	 BUID,BU_NAME,SKU_CODE,DEPT_CODE,SUBDEPT_CODE,CLASS_ID,SUBCLASS_ID
		,DEPT_NAME,SUBDEPT_NAME,CLASS_NAME,SUBCLASS_NAME
		,CLEANED_BRANDNAME,PRODUCT_NAME
		,GROUPING_CAT,CATEGORY_FINAL,TYPE_FINAL
		,CATEGORY_GROUP,CATEGORY
FROM VPM_DATA.PM_PRODUCT_MASTER A
WHERE CLASS_NAME IN ('AIR PURIFIER')
AND BUID = '6'
AND NOT EXISTS (SELECT * FROM TEMP_HOME_PRODUCT_MASTER B WHERE A.BUID = B.BUID AND A.SKU_CODE = B.SKU_CODE)
;

DELETE FROM TEMP_HOME_PRODUCT_MASTER
WHERE DEPT_NAME = 'PREMIUM : MDSG' 
OR SUBDEPT_NAME IN ('BETTER GIFTS')
OR CLASS_NAME IN ('STATIONARY')
OR SUBCLASS_NAME IN ('PET','SKIN CARE')
OR TYPE_FINAL = 'HAIR DRYER'
;

DROP TABLE IF EXISTS PM_HOME_PRODUCT_PRODUCT_MASTER;
SELECT *
		,CAST(NULL AS VARCHAR(50)) AS HOME_PRODUCT_GROUP
		,CAST(NULL AS VARCHAR(50)) AS PRODUCT_LIFESTYLE
		,CAST(NULL AS INT) AS HOUSEMOVING_FLAG
INTO VPM_DATA.PM_HOME_PRODUCT_PRODUCT_MASTER
FROM TEMP_HOME_PRODUCT_MASTER
;

UPDATE VPM_DATA.PM_HOME_PRODUCT_PRODUCT_MASTER
SET	 GROUPING_CAT = 'ELECTRONIC'
	,CATEGORY_FINAL = A.CATEGORY_FINAL
	,TYPE_FINAL = A.TYPE_FINAL
FROM PM_SKUTAGGING_ELECTRONICS_PRODUCT_MASTER A
WHERE A.BUID = PM_HOME_PRODUCT_PRODUCT_MASTER.BUID
AND A.SKU_CODE = PM_HOME_PRODUCT_PRODUCT_MASTER.SKU_CODE
;
UPDATE VPM_DATA.PM_HOME_PRODUCT_PRODUCT_MASTER
SET TYPE_FINAL = CASE WHEN SUBCLASS_NAME = 'CUTLERY' OR (SUBDEPT_NAME = 'HOUSEWARE' AND PRODUCT_NAME LIKE '%�մ%') THEN 'CUTLERY'
					  WHEN CLASS_NAME = 'VASE' THEN 'VEST'
					  WHEN SUBCLASS_NAME = 'BIN' THEN 'GARBAGE BINS & BAGS'
					  WHEN PRODUCT_NAME LIKE '�ҹ�ͧ%' AND PRODUCT_NAME NOT LIKE '%���͹%'
						   AND TYPE_FINAL = 'MATTRESS & ACCESSORIES' THEN 'BED'
					  ELSE TYPE_FINAL END
;

/***********************************************************************************************************/

--CHECK
SELECT	 BU_NAME,GROUPING_CAT,CATEGORY_FINAL,TYPE_FINAL
		,DEPT_NAME,SUBDEPT_NAME,CLASS_NAME,SUBCLASS_NAME,COUNT(*) AS CNT
FROM TEMP_HOME_PRODUCT_MASTER
GROUP BY BU_NAME,GROUPING_CAT,CATEGORY_FINAL,TYPE_FINAL
		,DEPT_NAME,SUBDEPT_NAME,CLASS_NAME,SUBCLASS_NAME
ORDER BY BU_NAME,GROUPING_CAT,DEPT_NAME,SUBDEPT_NAME,CLASS_NAME,SUBCLASS_NAME;	

SELECT DISTINCT GROUPING_CAT,CATEGORY_FINAL,TYPE_FINAL
FROM PM_HOME_PRODUCT_PRODUCT_MASTER
WHERE BU_NAME = 'PWB'
ORDER BY GROUPING_CAT,CATEGORY_FINAL,TYPE_FINAL;

/***********************************************************************************************************/

/* HOME_PRODUCT_GROUP */

UPDATE VPM_DATA.PM_HOME_PRODUCT_PRODUCT_MASTER
SET HOME_PRODUCT_GROUP = CASE WHEN TYPE_FINAL IN ('TV','AIR CONDITIONER','WASHER & DRYER','WATER HEATER','FREESTAND & HOB - HOOD','HOB & HOOD'
									,'MICROWAVE & OVEN','REFRIGERATOR','STOVE & INDUCTION','DISH WASHER','WASHER & DRYER','MICROWAVES & OVENS','WINE COOLER')
								OR TYPE_FINAL IN ('CABINET','CURTAIN','CURTAIN & BLIND','CURTAIN ROD & ACCESSORY','DINING TABLE SET','MADE TO ORDER'
									,'SHELF','WARDROBE','COMPACT KITCHEN','COUNTER DOOR','DISH WASHER','DIY KITCHEN','DRINKWARE & COOLER','GAS STOVE'
									,'GAS STOVE & OVEN ','HOB & HOOD & OVEN','HOB & HOOD SET','KITCHEN APP PARTS','SOFA')
								OR TYPE_FINAL IN ('BEDDING ACCESSORIES','BEDDING ACCESSORY','BATHROOM ACCESSORIES & DECOR','BED','MATTRESS & ACCESSORY')
								OR TYPE_FINAL IN ('BATHROOM ACCESSORIES & DECOR','BATHROOM CABINETS','BATHROOM SHELF','GRAB BAR','MIRROR','OTHER SPARE PARTS'
									,'SHOWER CURTAINS & RINGS & HOOKS','SHOWER ENCLOSURE','TANK','WASHING')
								OR SUBDEPT_NAME IN ('FURNITURE','KITCHEN APPLIANCES/SINKS') 
								THEN 'HOME FURNITURE'
							WHEN (CATEGORY_FINAL IN ('BATHROOM','LIGHTING','WINDOW & FRAME','DOOR & FRAME','PAINTING','DOOR LOCK') 
									OR TYPE_FINAL IN ('HOME AUTOMATION','BASIN','BATHTUB','SANITARYWARE','TRAP SET & PLUG WASTE','SINK','SHOWER & FAUCETS','TILE')
									OR (BU_NAME = 'HWS' AND CLASS_NAME = 'WALL PAPER')
									OR SUBDEPT_NAME IN ('FLOOR&TILE','LIGHTING'))
								AND TYPE_FINAL NOT IN ('WATER HOSE','TOWEL')
								THEN 'HOME FINISHING'
							WHEN GROUPING_CAT IN ('HOME CONSTRUCTION')
								THEN 'HOME CONSTRUCTION'
							ELSE 'HOME NECESSITY' END
;

UPDATE VPM_DATA.PM_HOME_PRODUCT_PRODUCT_MASTER
SET HOME_PRODUCT_GROUP = 'MAJOR HOUSEHOLD APPLIANCE'
WHERE HOME_PRODUCT_GROUP = 'HOME FURNITURE' AND GROUPING_CAT = 'ELECTRONIC'
;


/* PRODUCT_LIFESTYLE */

UPDATE VPM_DATA.PM_HOME_PRODUCT_PRODUCT_MASTER
SET PRODUCT_LIFESTYLE = 'HOME GARDENING'
WHERE PRODUCT_LIFESTYLE IS NULL
AND (CATEGORY_FINAL = 'GARDENING')
;
UPDATE VPM_DATA.PM_HOME_PRODUCT_PRODUCT_MASTER
SET PRODUCT_LIFESTYLE = 'HOME IMPROVEMENT'
WHERE (HOME_PRODUCT_GROUP IN ('HOME CONSTRUCTION','HOME FINISHING','HOME FURNITURE')
			AND CATEGORY_FINAL IN ('PAD LOCK','LIGHTING','PAINTING','DOOR LOCK'))
	OR (HOME_PRODUCT_GROUP IN ('HOME FURNITURE')
			AND CATEGORY_FINAL IN ('KITCHEN')
			AND TYPE_FINAL IN ('COMPACT KITCHEN','DIY KITCHEN','KITCHEN APP PARTS','COUNTER DOOR'))
	OR (CATEGORY_FINAL IN ('BATHROOM')
			AND TYPE_FINAL IN ('ANTI SLIPS','BATHROOM SHELVES','MIRRORS','BATHROOM CABINETS','MIRROR','SHOWER & FAUCETS'
								,'TRAP SET & PLUG WASTE','CURTAIN','BATHROOM SHELF','SHOWER CURTAINS & RINGS & HOOKS'))
;

UPDATE VPM_DATA.PM_HOME_PRODUCT_PRODUCT_MASTER
SET PRODUCT_LIFESTYLE = 'HOME DECORATION'
WHERE PRODUCT_LIFESTYLE IS NULL
AND (CATEGORY_FINAL IN ('FURNITURE & DECOR')
	OR TYPE_FINAL IN ('CUSHION & FOAM','SHELVES DIY','PICTURE FRAME')
	OR (HOME_PRODUCT_GROUP IN ('HOME NECESSITY')
		AND (TYPE_FINAL IN ('TOWEL') OR CATEGORY_FINAL IN ('BEDROOM'))
		)
	)
;

UPDATE VPM_DATA.PM_HOME_PRODUCT_PRODUCT_MASTER
SET TYPE_FINAL = 'MATTRESS & ACCESSORIES'
WHERE TYPE_FINAL = 'MATTRESS & ACCESSORY'
;
UPDATE VPM_DATA.PM_HOME_PRODUCT_PRODUCT_MASTER
SET HOME_PRODUCT_GROUP = 'HOME FURNITURE'
WHERE TYPE_FINAL = 'MATTRESS & ACCESSORIES'
AND HOME_PRODUCT_GROUP = 'HOME NECESSITY'
;

DELETE FROM VPM_DATA.PM_HOME_PRODUCT_PRODUCT_MASTER
WHERE CATEGORY_FINAL IN ('AUTOMOTIVE','STATIONERY')
OR DEPT_NAME IN ('NON MERCHADISE ITEM','NON MERCHANDISE','PREMIUM:MARKETING','HBC','SKIN CARE')
OR SUBDEPT_NAME IN ('FOREIGN BOOK(PAGE ONE)','BOOKS & MEDIA/NON FMCG')
;


/***********************************************************************************************************/

/* FLAG HOME PRODUCT*/

UPDATE VPM_DATA.PM_HOME_PRODUCT_PRODUCT_MASTER
SET HOUSEMOVING_FLAG = 1
WHERE DEPT_NAME NOT IN ('PREMIUM : MDSG') AND
(
CATEGORY_FINAL IN ('BRICK & BLOCK','BUILDING MATERIAL','CEILING','DECK BOARDS','DOOR & FRAME',
'FLOORING & WALLING','PAINTING','PLUMBING','ROOFING','WINDOW & FRAME')
OR
TYPE_FINAL IN ('TV','AIR CONDITIONER','REFRIGERATOR','MICROWAVE & OVEN','DISH WASHER','FREESTAND & HOB - HOOD','WASHER & DRYER'
,'HOB & HOOD','SINK','HOB & HOOD SET','GAS STOVE','GAS STOVE & OVEN ','HOB & HOOD & OVEN'
,'SHOWER CURTAINS & RINGS & HOOKS','SANITARYWARE','TRAP SET & PLUG WASTE','BATHROOM SHELF'
,'SHOWER ENCLOSURE','BATHTUB','BATHROOM CABINETS','WASHING'
,'MATTRESS & ACCESSORY','DRESSING TABLE'
,'CABINET','SOFA','WARDROBE','DINING TABLE SET','MADE TO ORDER','SOFA')
AND SUBCLASS_NAME NOT IN ('PLASTIC BAGS')
)
;

DELETE FROM VPM_DATA.PM_HOME_PRODUCT_PRODUCT_MASTER
WHERE (DEPT_NAME LIKE '%TISSUE%'
OR SUBDEPT_NAME LIKE '%TISSUE%'
OR CLASS_NAME LIKE '%TISSUE%'
OR SUBCLASS_NAME LIKE '%TISSUE%'
OR PRODUCT_NAME LIKE '%TISSUE%'
OR PRODUCT_NAME LIKE '%�Ԫ���%')
AND BU_NAME = 'TOPS';