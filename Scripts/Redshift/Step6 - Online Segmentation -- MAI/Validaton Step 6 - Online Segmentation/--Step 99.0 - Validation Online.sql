------------------------------------------------------------------------------ONLINE
SELECT * 
FROM (
	SELECT 'ONLINE_USER_FLAG' AS FEATURE_NAME,COALESCE((CASE WHEN ONLINE_USER_FLAG=1 THEN 'YES' ELSE 'N/A' END),'N/A') AS FEATURE_VALUE,count(*) AS cnt				
	FROM VPM_DATA.PM_ONLINE_CUST_SEGMENT
	GROUP BY COALESCE((CASE WHEN ONLINE_USER_FLAG=1 THEN 'YES' ELSE 'N/A' END),'N/A')
	UNION
	SELECT 'ONLINE_SHOPPING_BEAUTY_FLAG' AS FEATURE_NAME,COALESCE((CASE WHEN SHOPPING_BEAUTY_FLAG=1 THEN 'YES' ELSE 'N/A' END),'N/A') AS FEATURE_VALUE,count(*) AS cnt				
	FROM VPM_DATA.PM_ONLINE_CUST_SEGMENT
	GROUP BY COALESCE((CASE WHEN SHOPPING_BEAUTY_FLAG=1 THEN 'YES' ELSE 'N/A' END),'N/A')
	UNION
	SELECT 'ONLINE_SHOPPING_BOOKS_FLAG' AS FEATURE_NAME,COALESCE((CASE WHEN SHOPPING_BOOKS_FLAG=1 THEN 'YES' ELSE 'N/A' END),'N/A') AS FEATURE_VALUE,count(*) AS cnt				
	FROM VPM_DATA.PM_ONLINE_CUST_SEGMENT
	GROUP BY COALESCE((CASE WHEN SHOPPING_BOOKS_FLAG=1 THEN 'YES' ELSE 'N/A' END),'N/A')
	UNION
	SELECT 'ONLINE_SHOPPING_ELECTRONIC_FLAG' AS FEATURE_NAME,COALESCE((CASE WHEN SHOPPING_ELECTRONIC_FLAG=1 THEN 'YES' ELSE 'N/A' END),'N/A') AS FEATURE_VALUE,count(*) AS cnt				
	FROM VPM_DATA.PM_ONLINE_CUST_SEGMENT
	GROUP BY COALESCE((CASE WHEN SHOPPING_ELECTRONIC_FLAG=1 THEN 'YES' ELSE 'N/A' END),'N/A')
	UNION
	SELECT 'ONLINE_SHOPPING_ECOMMERCE_FLAG' AS FEATURE_NAME,COALESCE((CASE WHEN SHOPPING_ECOMMERCE_FLAG=1 THEN 'YES' ELSE 'N/A' END),'N/A') AS FEATURE_VALUE,count(*) AS cnt				
	FROM VPM_DATA.PM_ONLINE_CUST_SEGMENT
	GROUP BY COALESCE((CASE WHEN SHOPPING_ECOMMERCE_FLAG=1 THEN 'YES' ELSE 'N/A' END),'N/A')
	UNION
	SELECT 'ONLINE_SHOPPING_FASHION_FLAG' AS FEATURE_NAME,COALESCE((CASE WHEN SHOPPING_FASHION_FLAG=1 THEN 'YES' ELSE 'N/A' END),'N/A') AS FEATURE_VALUE,count(*) AS cnt				
	FROM VPM_DATA.PM_ONLINE_CUST_SEGMENT
	GROUP BY COALESCE((CASE WHEN SHOPPING_FASHION_FLAG=1 THEN 'YES' ELSE 'N/A' END),'N/A')
	UNION
	SELECT 'ONLINE_SHOPPING_GROCERY_FLAG' AS FEATURE_NAME,COALESCE((CASE WHEN SHOPPING_GROCERY_FLAG=1 THEN 'YES' ELSE 'N/A' END),'N/A') AS FEATURE_VALUE,count(*) AS cnt				
	FROM VPM_DATA.PM_ONLINE_CUST_SEGMENT
	GROUP BY COALESCE((CASE WHEN SHOPPING_GROCERY_FLAG=1 THEN 'YES' ELSE 'N/A' END),'N/A')
	UNION
	SELECT 'ONLINE_SHOPPING_HOME_FLAG' AS FEATURE_NAME,COALESCE((CASE WHEN SHOPPING_HOME_FLAG=1 THEN 'YES' ELSE 'N/A' END),'N/A') AS FEATURE_VALUE,count(*) AS cnt				
	FROM VPM_DATA.PM_ONLINE_CUST_SEGMENT
	GROUP BY COALESCE((CASE WHEN SHOPPING_HOME_FLAG=1 THEN 'YES' ELSE 'N/A' END),'N/A')
	UNION
	SELECT 'ONLINE_SHOPPING_KID_FLAG' AS FEATURE_NAME,COALESCE((CASE WHEN SHOPPING_KID_FLAG=1 THEN 'YES' ELSE 'N/A' END),'N/A') AS FEATURE_VALUE,count(*) AS cnt				
	FROM VPM_DATA.PM_ONLINE_CUST_SEGMENT
	GROUP BY COALESCE((CASE WHEN SHOPPING_KID_FLAG=1 THEN 'YES' ELSE 'N/A' END),'N/A')
	UNION
	SELECT 'ONLINE_SERVICE_SPA_FLAG' AS FEATURE_NAME,COALESCE((CASE WHEN SERVICE_SPA_FLAG=1 THEN 'YES' ELSE 'N/A' END),'N/A') AS FEATURE_VALUE,count(*) AS cnt				
	FROM VPM_DATA.PM_ONLINE_CUST_SEGMENT
	GROUP BY COALESCE((CASE WHEN SERVICE_SPA_FLAG=1 THEN 'YES' ELSE 'N/A' END),'N/A')
	UNION
	SELECT 'ONLINE_SERVICE_BUSINESS_FLAG' AS FEATURE_NAME,COALESCE((CASE WHEN SERVICE_BUSINESS_FLAG=1 THEN 'YES' ELSE 'N/A' END),'N/A') AS FEATURE_VALUE,count(*) AS cnt				
	FROM VPM_DATA.PM_ONLINE_CUST_SEGMENT
	GROUP BY COALESCE((CASE WHEN SERVICE_BUSINESS_FLAG=1 THEN 'YES' ELSE 'N/A' END),'N/A')
	UNION
	SELECT 'ONLINE_SERVICE_FINANCIAL_FLAG' AS FEATURE_NAME,COALESCE((CASE WHEN SERVICE_FINANCIAL_FLAG=1 THEN 'YES' ELSE 'N/A' END),'N/A') AS FEATURE_VALUE,count(*) AS cnt				
	FROM VPM_DATA.PM_ONLINE_CUST_SEGMENT
	GROUP BY COALESCE((CASE WHEN SERVICE_FINANCIAL_FLAG=1 THEN 'YES' ELSE 'N/A' END),'N/A')
	UNION
	SELECT 'ONLINE_SERVICE_KID_FLAG' AS FEATURE_NAME,COALESCE((CASE WHEN SERVICE_KID_FLAG=1 THEN 'YES' ELSE 'N/A' END),'N/A') AS FEATURE_VALUE,count(*) AS cnt				
	FROM VPM_DATA.PM_ONLINE_CUST_SEGMENT
	GROUP BY COALESCE((CASE WHEN SERVICE_KID_FLAG=1 THEN 'YES' ELSE 'N/A' END),'N/A')		
	UNION
	SELECT 'ONLINE_SERVICE_ACTIVITY_FLAG' AS FEATURE_NAME,COALESCE((CASE WHEN SERVICE_ACTIVITY_FLAG=1 THEN 'YES' ELSE 'N/A' END),'N/A') AS FEATURE_VALUE,count(*) AS cnt				
	FROM VPM_DATA.PM_ONLINE_CUST_SEGMENT
	GROUP BY COALESCE((CASE WHEN SERVICE_ACTIVITY_FLAG=1 THEN 'YES' ELSE 'N/A' END),'N/A')
	UNION
	SELECT 'ONLINE_SERVICE_LOGISTIC_FLAG' AS FEATURE_NAME,COALESCE((CASE WHEN SERVICE_LOGISTIC_FLAG=1 THEN 'YES' ELSE 'N/A' END),'N/A') AS FEATURE_VALUE,count(*) AS cnt				
	FROM VPM_DATA.PM_ONLINE_CUST_SEGMENT
	GROUP BY COALESCE((CASE WHEN SERVICE_LOGISTIC_FLAG=1 THEN 'YES' ELSE 'N/A' END),'N/A')
	UNION
	SELECT 'ONLINE_SERVICE_HOSPITAL_FLAG' AS FEATURE_NAME,COALESCE((CASE WHEN SERVICE_MEDICAL_FLAG=1 THEN 'YES' ELSE 'N/A' END),'N/A') AS FEATURE_VALUE,count(*) AS cnt				
	FROM VPM_DATA.PM_ONLINE_CUST_SEGMENT
	GROUP BY COALESCE((CASE WHEN SERVICE_MEDICAL_FLAG=1 THEN 'YES' ELSE 'N/A' END),'N/A')
	UNION
	SELECT 'ONLINE_SERVICE_COURSE_FLAG' AS FEATURE_NAME,COALESCE((CASE WHEN SERVICE_COURSE_FLAG=1 THEN 'YES' ELSE 'N/A' END),'N/A') AS FEATURE_VALUE,count(*) AS cnt				
	FROM VPM_DATA.PM_ONLINE_CUST_SEGMENT
	GROUP BY COALESCE((CASE WHEN SERVICE_COURSE_FLAG=1 THEN 'YES' ELSE 'N/A' END),'N/A')
	UNION
	SELECT 'ONLINE_SERVICE_TRAVEL_FLAG' AS FEATURE_NAME,COALESCE((CASE WHEN SERVICE_TRAVEL_FLAG=1 THEN 'YES' ELSE 'N/A' END),'N/A') AS FEATURE_VALUE,count(*) AS cnt				
	FROM VPM_DATA.PM_ONLINE_CUST_SEGMENT
	GROUP BY COALESCE((CASE WHEN SERVICE_TRAVEL_FLAG=1 THEN 'YES' ELSE 'N/A' END),'N/A')
	UNION
	SELECT 'ONLINE_SERVICE_UTILITIES_FLAG' AS FEATURE_NAME,COALESCE((CASE WHEN SERVICE_UTILITIES_FLAG=1 THEN 'YES' ELSE 'N/A' END),'N/A') AS FEATURE_VALUE,count(*) AS cnt				
	FROM VPM_DATA.PM_ONLINE_CUST_SEGMENT
	GROUP BY COALESCE((CASE WHEN SERVICE_UTILITIES_FLAG=1 THEN 'YES' ELSE 'N/A' END),'N/A')
	UNION
	
	SELECT 'ONLINE_AFFINITY_SEGMENT' AS FEATURE_NAME,COALESCE(ONLINE_AFFINITY_SEGMENT,'N/A') AS FEATURE_VALUE,count(*) AS cnt				
	FROM VPM_DATA.PM_ONLINE_CUST_SEGMENT
	GROUP BY COALESCE(ONLINE_AFFINITY_SEGMENT,'N/A')
) A
ORDER BY FEATURE_NAME,FEATURE_VALUE;
