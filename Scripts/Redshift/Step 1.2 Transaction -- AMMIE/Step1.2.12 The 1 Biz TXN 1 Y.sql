DROP TABLE IF EXISTS VPM_DATA.PM_THE1_BIZ_TXN_1YR ;
SELECT 'THE1 BIZ' AS DATASOURCE
      , A.partner_code 
	  ,A.MEMBER_NUMBER AS MEMBER_NUMBER
      ,A.CARDNO
      ,A.BRANCH_CODE AS BRANCHID
      ,A.TRANSACTIONDATE AS TXN_DATETIME
      ,CAST(A.TRANSACTIONDATE AS DATE) AS TXN_DATE
      ,A.TRANSACTIONID AS TICKET_NUM
      ,A.TOTAL_AMOUNT_BEFORE_REDEEMED_DISCOUNT AS TXN_AMT
      ,CASE WHEN UPPER(A.PARTNER_NAME_THE1BIZ) = 'CPN HO EVENT' THEN UPPER(BRANCH_NAME)
            WHEN UPPER(A.PARTNER_NAME_THE1BIZ) = 'CENTRAL PATTANA PUBLIC' AND UPDATED_BY_NAME LIKE 'ASSOCIATES%' THEN UPPER(BRANCH_NAME)
            WHEN UPPER(A.PARTNER_NAME_THE1BIZ) = 'CENTRAL PATTANA PUBLIC' THEN UPPER(UPDATED_BY_NAME)
            ELSE UPPER(A.PARTNER_NAME_THE1BIZ) END AS CLEANED_MERCHANT_NAME
      ,A.BRAND_NAME AS MERCHANT_NAME
      ,CASE WHEN UPPER(A.PARTNER_NAME_THE1BIZ) = 'CENTRAL PATTANA PUBLIC' THEN 'ELECTRONICS GADGET'
            WHEN UPPER(A.PARTNER_NAME_THE1BIZ) = 'SHU' THEN 'FASHION'
            WHEN UPPER(A.PARTNER_NAME_THE1BIZ) = 'UNDER ARMOUR' THEN 'FASHION'
            WHEN UPPER(A.PARTNER_NAME_THE1BIZ) = 'SUNGLASS HUT THAILAND' THEN 'FASHION'
            WHEN UPPER(A.PARTNER_NAME_THE1BIZ) = 'SABINA' THEN 'FASHION'
            WHEN UPPER(A.PARTNER_NAME_THE1BIZ) = 'DIAMOND FOR YOU' THEN 'FASHION'
            WHEN UPPER(A.PARTNER_NAME_THE1BIZ) = 'HEAVY' THEN 'FASHION'
            WHEN UPPER(A.PARTNER_NAME_THE1BIZ) = 'SEIKO THAILAND' THEN 'FASHION'
            WHEN UPPER(A.PARTNER_NAME_THE1BIZ) = 'OPTIC SQUARE' THEN 'FASHION'
            WHEN UPPER(A.PARTNER_NAME_THE1BIZ) = 'CHIC REPUBLIC (PUBLIC) COMPANY LIMITED' THEN 'HOME NON-ELECTRONICS'
            WHEN UPPER(A.PARTNER_NAME_THE1BIZ) = 'CPN HO EVENT' AND UPPER(BRANCH_NAME) = 'BABY BASKET CTW' THEN 'KIDS'
            WHEN UPPER(A.PARTNER_NAME_THE1BIZ) = 'CPN HO EVENT' AND UPPER(BRANCH_NAME) = 'THE FACE SHOP CTW' THEN 'BEAUTY'
            WHEN UPPER(A.PARTNER_NAME_THE1BIZ) = 'CPN HO EVENT' AND UPPER(BRANCH_NAME) = 'ZWILLING J.A. HENCKELS CTW' THEN 'HOME NON-ELECTRONICS'
            WHEN UPPER(A.PARTNER_NAME_THE1BIZ) = 'CPN HO EVENT' AND (UPPER(BRANCH_NAME) LIKE '%ADIDAS%'
     														  OR UPPER(BRANCH_NAME) LIKE '%AMERICAN EAGLE%'
     														  OR UPPER(BRANCH_NAME) LIKE '%BERSHKA%'
     														  OR UPPER(BRANCH_NAME) LIKE '%BETTER VISION%'
     														  OR UPPER(BRANCH_NAME) LIKE '%BIRKENSTOCK%'
     														  OR UPPER(BRANCH_NAME) LIKE '%COACH%'
     														  OR UPPER(BRANCH_NAME) LIKE '%ECCO%'
     														  OR UPPER(BRANCH_NAME) LIKE '%GEOX%'
     														  OR UPPER(BRANCH_NAME) LIKE '%KARL LAGERFELD%'
     														  OR UPPER(BRANCH_NAME) LIKE '%KATE SPADE%'
     														  OR UPPER(BRANCH_NAME) LIKE '%MANGO%'
     														  OR UPPER(BRANCH_NAME) LIKE '%MARC JACOBS%'
     														  OR UPPER(BRANCH_NAME) LIKE '%MASSIMO DUTTI%'
     														  OR UPPER(BRANCH_NAME) LIKE '%MICHAEL KORS%'
     														  OR UPPER(BRANCH_NAME) LIKE '%NEW ERA%'
     														  OR UPPER(BRANCH_NAME) LIKE '%O&B%'
     														  OR UPPER(BRANCH_NAME) LIKE '%ONITSUKA%'
     														  OR UPPER(BRANCH_NAME) LIKE '%OYSHO%'
     														  OR UPPER(BRANCH_NAME) LIKE '%PULL & BEAR%'
     														  OR UPPER(BRANCH_NAME) LIKE '%RIP CURL%'
     														  OR UPPER(BRANCH_NAME) LIKE '%SAMSONITE%'
     														  OR UPPER(BRANCH_NAME) LIKE '%SUPERDRY%'
     														  OR UPPER(BRANCH_NAME) LIKE '%SWAROVSKI%'
     														  OR UPPER(BRANCH_NAME) LIKE '%SWATCH%'
     														  OR UPPER(BRANCH_NAME) LIKE '%TED BAKER%'
     														  OR UPPER(BRANCH_NAME) LIKE '%TUMI%'
     														  OR UPPER(BRANCH_NAME) LIKE '%VICTORIA_S SECRET%'
     														  OR UPPER(BRANCH_NAME) LIKE '%YACCO MARICARD%'
     														  OR UPPER(BRANCH_NAME) LIKE '%ZARA%') THEN 'FASHION'
			ELSE NULL END AS REVISED_CATEGORY_LEVEL_1
      ,CAST(NULL AS VARCHAR(50)) AS REVISED_CATEGORY_LEVEL_2
      ,CAST(NULL AS VARCHAR(50)) AS REVISED_CATEGORY_LEVEL_3
      ,CAST(NULL AS FLOAT) AS HOSPITAL_FLAG
      ,CAST(NULL AS VARCHAR(100)) AS DINING_CUISINE_GROUP
	  ,CAST(NULL AS VARCHAR(50)) AS brand_cluster
	  ,CAST(NULL AS VARCHAR(10)) AS DINING_CLUSTER
	  ,CAST(NULL AS FLOAT) AS CATEGORY_RELEVANCE_FLAG
INTO VPM_DATA.PM_THE1_BIZ_TXN_1YR 
FROM analysis_data.the1biz_transaction_report A 
WHERE a.member_number <>''
AND transactiondate :: DATE BETWEEN VPM_DATA.GET_START_DATE(CURRENT_TIMESTAMP)  and  VPM_DATA.GET_END_DATE(CURRENT_TIMESTAMP) ;
;

--------------------------------------UPDATE CATEGORY RELEVANCE-------------------------------------
UPDATE VPM_DATA.PM_THE1_BIZ_TXN_1YR 
SET CATEGORY_RELEVANCE_FLAG = 1
WHERE REVISED_CATEGORY_LEVEL_1 IS NOT NULL
;

---------------------------------------------HOSPITAL-----------------------------------------------
UPDATE VPM_DATA.PM_THE1_BIZ_TXN_1YR 
SET REVISED_CATEGORY_LEVEL_1 = 'SERVICES'
	,HOSPITAL_FLAG=1
WHERE CLEANED_MERCHANT_NAME  LIKE '%HOSPITAL%' OR CLEANED_MERCHANT_NAME ='VITALLIFE' OR UPPER(partner_code) = 'BIH' 
;