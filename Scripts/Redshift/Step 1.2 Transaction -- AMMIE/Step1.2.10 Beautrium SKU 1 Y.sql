/************************************* BEAUTRIUM *************************************/

----------- TXN BEAUTRIUM 1 YEAR -----------
DROP TABLE IF EXISTS  VPM_DATA.PM_BTM_SALESSKU_1YR;
SELECT 'BEAUTRIUM' AS DATASOURCE
	   ,A.MEMBER_NUMBER
      ,A.Trans_Date AS TXN_DATETIME
      ,CAST(A.Trans_Date AS DATE) AS TXN_DATE
      ,A.RECEIPT_NO 
      ,A.SKU_ID AS SKU_CODE
      ,A.NET_PRICE_TOT AS  TXN_AMT
      ,B.DEPT_ID AS DEPT_ID
      ,B.SUBDEPT_ID AS SUBDEPT_ID
      ,A.QTY
      ,A.NET_PRICE_TOT/A.QTY AS PRICE_PER_UNIT
      ,A.PARTNER_CODE AS PARTNER_CODE
      ,B.DEPT_NAME AS DEPT_NAME
      ,B.SUBDEPT_NAME AS SUBDEPT_NAME
      ,B.CLASS_ID AS CLASS_ID
      ,B.CLASS_NAME AS CLASS_NAME
      ,B.SUBCLASS_ID AS SUBCLASS_ID
      ,B.SUBCLASS_NAME AS SUBCLASS_NAME
      ,B.BRANDNAME AS CLEANED_BRANDNAME
      ,B.PRODUCT_NAME
      ,CAST(B.EDITED_CAT_TYPE_GRP AS VARCHAR(50)) AS EDITED_CAT_TYPE_GRP
      ,B.BEAUTY_BRAND_CLUSTER
      ,revised_category_level_1
      ,revised_category_level_2
      ,revised_category_level_3
      ,category_relevance_flag
INTO VPM_DATA.PM_BTM_SALESSKU_1YR    
FROM Analysis_Data.sales_sku A INNER JOIN vpm_data.pm_btm_product_master B 
ON A.SKU_ID = B.PAD_SKU_ID --118655 (NULL 43661)
WHERE UPPER(A.PARTNER_CODE)='BTM' AND 
CAST(A.TRANS_DATE AS DATE) <= VPM_DATA.GET_END_DATE(current_timestamp) AND CAST(A.TRANS_DATE AS DATE)>= VPM_DATA.GET_START_DATE(current_timestamp)
AND COALESCE(MEMBER_NUMBER,'') <> ''
;
