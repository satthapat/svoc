/*
	Create Customer table - PM_CUST_SVOC_POINT_REDEMPTION
	- Add and combine the attribute given from T1 Team
	- Adjusted Extra Point Earning Score
	- Adjusted Reemption Point Score
	- T1 Reward Point Affinity Score/Segment
	- Customer Segmentation
*/

USE [VPM_DATA]
GO

--CUSTOMER LEVEL

DROP TABLE #P_EARNED
GO

DECLARE @MIN_DATE DATE, @MAX_DATE DATE
SELECT @MIN_DATE = MIN(RECEIPTDATE), @MAX_DATE = MAX(RECEIPTDATE) FROM PM_POINT_REDEMPTION
SELECT @MIN_DATE,@MAX_DATE
SELECT CustomerID,
			 SUM(NormalPoints) AS NORMAL_POINTS_EARNED, 
			 SUM(PromotionPoints) AS EXTRA_POINTS_EARNED, 
			 SUM(PromotionPoints)*1.0/SUM(NormalPoints)*1.0 AS EXTRA_POINTS_RATIO,
			 MAX(PromotionPoints) AS MAX_EXTRA_POINT,
			 COUNT(DISTINCT [Transactiondate]) AS TOTAL_EARN_VISIT,
			 COUNT(DISTINCT CASE WHEN PromotionPoints > 0 THEN [Transactiondate] END) AS TOTAL_EXTRA_EARN_VISIT
INTO #P_EARNED
FROM [AnalysisData].[dbo].[TranPointsByDay]
WHERE [Transactiondate] >= @MIN_DATE
AND [Transactiondate] <= @MAX_DATE
AND NormalPoints > 0
AND NormalPoints + PromotionPoints > 0
GROUP BY CustomerID
GO


--SPENDING
DROP TABLE #SPENDING

DECLARE @MIN_DATE DATE, @MAX_DATE DATE
SELECT @MIN_DATE = MIN(RECEIPTDATE), @MAX_DATE = MAX(RECEIPTDATE) FROM PM_POINT_REDEMPTION
SELECT @MIN_DATE,@MAX_DATE

SELECT CUSTOMERID AS CUSTOMER_ID, SUM(TOTAL_VISIT) AS TOTAL_VISIT
INTO #SPENDING
FROM
(
SELECT CUSTOMERID,COUNT(DISTINCT [TransactionDate]) AS TOTAL_VISIT
FROM [AnalysisData].[dbo].[ALLCustomer_SF_2018]
WHERE [TransactionDate] <= @MAX_DATE
GROUP BY CUSTOMERID
UNION
SELECT CUSTOMERID,COUNT(DISTINCT [TransactionDate]) AS TOTAL_VISIT
FROM [AnalysisData].[dbo].[ALLCustomer_SF_2017]
GROUP BY CUSTOMERID
UNION
SELECT CUSTOMERID,COUNT(DISTINCT [TransactionDate]) AS TOTAL_VISIT
FROM [AnalysisData].[dbo].[ALLCustomer_SF_2016]
WHERE [TransactionDate] >= @MIN_DATE
GROUP BY CUSTOMERID
) A
GROUP BY CUSTOMERID
GO

CREATE UNIQUE INDEX CUSTOMER_ID ON #SPENDING (CUSTOMER_ID)
GO

DROP TABLE PM_CUST_SVOC_POINT_REDEMPTION
GO

SELECT DISTINCT A.CUSTOMERID AS [CUSTOMER_ID]
	  ,COUNT(DISTINCT RECEIPTDATE) AS TOTAL_REDEEM_VISIT
      ,MAX(ISNULL(PERCENT_DISCOUNT,0)) AS MAX_DISCOUNT
      ,ISNULL(SUM(CASE WHEN ONLINE_FLAG = 1 THEN 1 ELSE 0 END),0) AS REDEEM_ONLINE
      ,ISNULL(SUM(CASE WHEN ONLINE_FLAG IS NULL THEN 1 ELSE 0 END),0) AS REDEEM_OFFLINE
      ,COUNT(CASE WHEN TYPEREDEEM LIKE '%TRANSFER%' THEN 1 END) AS TRANSFER_POINT
      ,COUNT(CASE WHEN REDEEM_METHOD = 'POINT REDEMPTION' THEN 1 END) AS REDEEM_POINT
      ,COUNT(CASE WHEN REDEEM_METHOD = 'SPENDING' THEN 1 END) AS REDEEM_SPENDING
      ,COUNT(CASE WHEN REDEEM_TYPE = 'CASH VOUCHER' THEN 1 END) AS REDEEM_CASH_VOUCHER_NUM
      ,COUNT(CASE WHEN REDEEM_TYPE = 'DISCOUNT' THEN 1 END) AS REDEEM_DISCOUNT_NUM
      ,COUNT(CASE WHEN REDEEM_TYPE = 'PREMIUM' THEN 1 END) AS REDEEM_PREMIUM_NUM
      ,COUNT(CASE WHEN REDEEM_TYPE = 'LUCKY DRAW' THEN 1 END) AS REDEEM_LUCKY_DRAW_NUM
      ,COUNT(DISTINCT CASE WHEN REDEEM_PURPOSE = 'INSTALLMENT' THEN 1 END) AS INSTALLMENT_FLAG
      ,SUM(CASE WHEN [REDEEM_PURPOSE] = 'CAR FUEL' THEN 1 ELSE 0 END) AS PETRO_PARTNER_TXN_CNT
	  ,SUM(CASE WHEN [REDEEM_PURPOSE] = 'HOTEL AND SPA' THEN 1 ELSE 0 END) AS HOTEL_SPA_PARTNER_TXN_CNT
	  ,SUM(CASE WHEN [REDEEM_PURPOSE] = 'TRAVEL' THEN 1 ELSE 0 END) AS TRAVEL_PARTNER_TXN_CNT
	  ,SUM(CASE WHEN [REDEEM_PURPOSE] = 'DINING' THEN 1 ELSE 0 END) AS DINING_PARTNER_TXN_CNT
      ,MAX(CASE WHEN [PointsBalance] >= PointsBefore THEN [PointsBalance] ELSE PointsBefore END) AS MAX_POINT_BALANCE
	  ,COUNT(Z.CUSTOMERID) AS CNT_REDEMPTION
	  ,MAX(CASE WHEN TypeRedeem != '7) Transfer Points (IN)' THEN PointsAffected ELSE 0 END) AS MAX_POINT_REDEEMED
	  ,SUM(CASE WHEN TypeRedeem != '7) Transfer Points (IN)' THEN PointsAffected ELSE 0 END) AS SUM_POINT_REDEEMED
	  ,CASE WHEN MAX(CASE WHEN [PointsBalance] >= PointsBefore THEN [PointsBalance] ELSE PointsBefore END) <= 0 THEN 0
		  ELSE CASE WHEN MAX(PointsBefore) > 0 THEN SUM(PointsAffected)*1.0/(MAX(PointsBefore)*1.0) END 
		  END AS POINT_TURNOVER
      ,D.[NORMAL_POINTS_EARNED]
      ,D.[EXTRA_POINTS_EARNED]
      ,D.[EXTRA_POINTS_RATIO]
      ,D.MAX_EXTRA_POINT
      ,D.TOTAL_EARN_VISIT
      ,D.TOTAL_EXTRA_EARN_VISIT
      ,MAX(CASE WHEN TypeRedeem != '7) Transfer Points (IN)' THEN PointsAffected END) AS POINT_USED_MAX
      ,MIN(CASE WHEN TypeRedeem != '7) Transfer Points (IN)' THEN PointsAffected END) AS POINT_USED_MIN
      ,AVG(CASE WHEN TypeRedeem != '7) Transfer Points (IN)' THEN PointsAffected END) AS POINT_USED_AVG
      ,STDEV(CASE WHEN TypeRedeem != '7) Transfer Points (IN)' THEN PointsAffected END) AS POINT_USED_SD
      ,MAX(PERCENT_DISCOUNT) AS PERCENT_DISCOUNT_MAX
      ,MIN(PERCENT_DISCOUNT) AS PERCENT_DISCOUNT_MIN
      ,AVG(CASE WHEN  REDEEM_TYPE IN ('CASH VOUCHER','DISCOUNT') THEN 
				  CASE WHEN TOTAL_CASH_VALUE/PointsAffected <= 0.5 THEN TOTAL_CASH_VALUE/PointsAffected
				  ELSE PERCENT_DISCOUNT END
			 END) AS PERCENT_DISCOUNT_AVG
,STDEV(PERCENT_DISCOUNT) AS PERCENT_DISCOUNT_SD
      ,CASE WHEN ISNULL([PointBalance],0) < 0 THEN 0 END AS POINT_BALANCE
      ,[TierPointBalance] AS TIER_POINT_BALANCE
      ,E.TOTAL_VISIT AS TOTAL_SPENDING_VISIT
INTO PM_CUST_SVOC_POINT_REDEMPTION_NEW
FROM AnalysisData..[PointBalanceCustomer] A	--14,446,996
LEFT JOIN PM_POINT_REDEMPTION Z
ON A.CUSTOMERID = Z.CUSTOMERID
	AND [PointsAffected] >= 0
LEFT JOIN #P_EARNED D
ON A.CUSTOMERID = D.CUSTOMERID
LEFT JOIN #SPENDING E
ON A.CUSTOMERID = E.CUSTOMER_ID
GROUP BY A.CUSTOMERID
      ,D.[NORMAL_POINTS_EARNED]
      ,D.[EXTRA_POINTS_EARNED]
      ,D.[EXTRA_POINTS_RATIO]
      ,D.MAX_EXTRA_POINT
      ,D.TOTAL_EARN_VISIT
      ,D.TOTAL_EXTRA_EARN_VISIT
      ,A.[PointBalance]
      ,A.[TierPointBalance]
      ,E.TOTAL_VISIT
GO

ALTER TABLE PM_CUST_SVOC_POINT_REDEMPTION
ADD POINT_USED_TO_EARNED_RATIO FLOAT
GO
UPDATE PM_CUST_SVOC_POINT_REDEMPTION
SET POINT_USED_TO_EARNED_RATIO = 
CASE WHEN ISNULL(NORMAL_POINTS_EARNED,0)+ISNULL(EXTRA_POINTS_EARNED,0) <= 0 THEN 0
	 WHEN ISNULL(SUM_POINT_REDEEMED,0) > 2*(ISNULL(NORMAL_POINTS_EARNED,0)+ISNULL(EXTRA_POINTS_EARNED,0)) THEN 2 
	   ELSE ISNULL(SUM_POINT_REDEEMED,0)*1.0
		  /(ISNULL(NORMAL_POINTS_EARNED,0)+ISNULL(EXTRA_POINTS_EARNED,0))*1.0 END
GO    

DROP TABLE #REDEEM_CUST
GO 
SELECT DISTINCT CUSTOMERID AS CUSTOMER_ID
INTO #REDEEM_CUST
FROM PM_POINT_REDEMPTION	
WHERE POINTSAFFECTED >= 0
GO
CREATE UNIQUE INDEX CUSTOMER_ID ON #REDEEM_CUST (CUSTOMER_ID)
GO
CREATE UNIQUE INDEX CUSTOMER_ID ON PM_CUST_SVOC_POINT_REDEMPTION (CUSTOMER_ID)
GO

ALTER TABLE PM_CUST_SVOC_POINT_REDEMPTION
ADD REDEEM_FLAG TINYINT
GO
UPDATE PM_CUST_SVOC_POINT_REDEMPTION
SET REDEEM_FLAG = 1
WHERE CUSTOMER_ID IN (SELECT CUSTOMER_ID FROM #REDEEM_CUST)
GO

--REDEMPTION TYPE
DROP TABLE #ROW_REDEEM
GO
SELECT *,ROW_NUMBER() OVER (PARTITION BY [CustomerID] ORDER BY CNT DESC) AS ROW
INTO #ROW_REDEEM
FROM (  SELECT [CustomerID],[REDEEM_TYPE],COUNT(*) AS CNT
	  FROM [VPM_DATA].[dbo].[PM_POINT_REDEMPTION]
	  WHERE [PointsAffected] >= 0
	  GROUP BY [CustomerID],[REDEEM_TYPE]
   ) A
GO	 
CREATE NONCLUSTERED INDEX CUST ON #ROW_REDEEM ([CustomerID])
GO
ALTER TABLE [PM_CUST_SVOC_POINT_REDEMPTION]
ADD MOST_1_REDEEM VARCHAR(30), MOST_2_REDEEM VARCHAR(30)
GO
UPDATE [PM_CUST_SVOC_POINT_REDEMPTION]
SET MOST_1_REDEEM = A.[REDEEM_TYPE]
FROM #ROW_REDEEM A
WHERE A.ROW = 1 AND A.[CustomerID] = [PM_CUST_SVOC_POINT_REDEMPTION].CUSTOMER_ID
GO
UPDATE [PM_CUST_SVOC_POINT_REDEMPTION]
SET MOST_2_REDEEM = A.[REDEEM_TYPE]
FROM #ROW_REDEEM A
WHERE A.ROW = 2 AND A.[CustomerID] = [PM_CUST_SVOC_POINT_REDEMPTION].CUSTOMER_ID
GO

ALTER TABLE [PM_CUST_SVOC_POINT_REDEMPTION]
DROP COLUMN PREFERRED_PREMIUM
GO
ALTER TABLE [PM_CUST_SVOC_POINT_REDEMPTION]
ADD PREFERRED_PREMIUM VARCHAR(30)
GO
UPDATE [PM_CUST_SVOC_POINT_REDEMPTION]
SET PREFERRED_PREMIUM = A.REDEEM_PURPOSE
FROM (	SELECT CUSTOMER_ID,REDEEM_PURPOSE,MAX(CNT_REDEEM) AS MAX
		FROM (	SELECT CUSTOMERID AS CUSTOMER_ID,REDEEM_PURPOSE,COUNT(*) AS CNT_REDEEM
				FROM [PM_POINT_REDEMPTION]
				WHERE REDEEM_TYPE = 'PREMIUM'
					AND [ReceiptDate] >= '2017-01-01' AND [PointsAffected] > 0
				GROUP BY CUSTOMERID,REDEEM_PURPOSE
			 ) B
		GROUP BY CUSTOMER_ID,REDEEM_PURPOSE
	 ) A
WHERE A.CUSTOMER_ID = [PM_CUST_SVOC_POINT_REDEMPTION].CUSTOMER_ID	 		 
GO

ALTER TABLE PM_CUST_SVOC_POINT_REDEMPTION
ADD ONLINE_SPENDER TINYINT
GO
UPDATE PM_CUST_SVOC_POINT_REDEMPTION
SET ONLINE_SPENDER = 1
FROM (	SELECT DISTINCT CUSTOMER_ID
		FROM PM_ONLINE_CG_SALESSKU_1YR
	 ) A
WHERE A.CUSTOMER_ID = PM_CUST_SVOC_POINT_REDEMPTION.CUSTOMER_ID
GO

ALTER TABLE PM_CUST_SVOC_POINT_REDEMPTION
ADD PERCENT_EXTRA_POINT FLOAT
GO
UPDATE PM_CUST_SVOC_POINT_REDEMPTION
SET PERCENT_EXTRA_POINT = (NORMAL_POINTS_EARNED+EXTRA_POINTS_EARNED)*1.0/NORMAL_POINTS_EARNED
WHERE ISNULL(NORMAL_POINTS_EARNED,0) > 0 
GO
 
--ALTER TABLE [VPM_DATA].[dbo].[PM_CUST_SVOC_POINT_REDEMPTION]
--ADD MOST_REDEMPTION_BU VARCHAR(5)
--GO
--UPDATE [PM_CUST_SVOC_POINT_REDEMPTION]
--SET MOST_REDEMPTION_BU = A.ITEMBUCODE
--FROM	(	SELECT CUSTOMERID,ITEMBUCODE,MAX(VISIT_DAY) AS VISIT_DAY
--			FROM
--				(	SELECT CUSTOMERID,ITEMBUCODE,COUNT(DISTINCT RECEIPTDATE) AS VISIT_DAY
--					FROM [PM_POINT_REDEMPTION]
--					GROUP BY CUSTOMERID,ITEMBUCODE
--				) B
--			GROUP BY CUSTOMERID,ITEMBUCODE
--	) A
--WHERE A.CUSTOMERID = [PM_CUST_SVOC_POINT_REDEMPTION].CUSTOMER_ID
--GO

UPDATE [PM_CUST_SVOC_POINT_REDEMPTION]
SET MAX_POINT_BALANCE = CASE WHEN MAX_POINT_BALANCE >= POINT_BALANCE THEN MAX_POINT_BALANCE
						ELSE POINT_BALANCE END
GO

UPDATE [PM_CUST_SVOC_POINT_REDEMPTION]
SET POINT_TURNOVER = MAX_POINT_BALANCE*1.0/SUM_POINT_REDEEMED*1.0
WHERE SUM_POINT_REDEEMED IS NOT NULL AND SUM_POINT_REDEEMED != 0
GO


ALTER TABLE PM_CUST_SVOC_POINT_REDEMPTION
ADD  EARNING_PARTNER_PREFER VARCHAR(10)
	,REDEEM_BU_PREFER VARCHAR(10)
	,REDEEM_PARTNER_PREFER VARCHAR(10)
GO

DROP TABLE #EARNING

SELECT CUSTOMER_ID,ITEMBUCODE,PARTNER_CLEAN
	  ,MAX(CNT_EARNING) AS MAX_CNT_EARNING
INTO #EARNING
FROM 
(
	SELECT CUSTOMERID AS CUSTOMER_ID,ITEMBUCODE,PARTNER_CLEAN,COUNT(*) AS CNT_EARNING
	FROM [VPM_DATA].[dbo].[PM_POINT_REDEMPTION] 
	WHERE TypeRedeem = '7) Transfer Points (IN)'
	GROUP BY CUSTOMERID,ITEMBUCODE,PARTNER_CLEAN
) A
GROUP BY CUSTOMER_ID,ITEMBUCODE,PARTNER_CLEAN

DROP TABLE #REDEMPTION

SELECT CUSTOMER_ID,ITEMBUCODE,PARTNER_CLEAN
	  ,ROW_NUMBER() OVER (PARTITION BY CUSTOMER_ID,PARTNER_CLEAN ORDER BY CNT_EARNING DESC) AS ROW
INTO #REDEMPTION
FROM 
(
	SELECT CUSTOMERID AS CUSTOMER_ID,ITEMBUCODE,PARTNER_CLEAN,COUNT(*) AS CNT_EARNING
	FROM [VPM_DATA].[dbo].[PM_POINT_REDEMPTION] 
	WHERE TypeRedeem != '7) Transfer Points (IN)'
	GROUP BY CUSTOMERID,ITEMBUCODE,PARTNER_CLEAN
) A
GO

UPDATE PM_CUST_SVOC_POINT_REDEMPTION
SET EARNING_PARTNER_PREFER = A.ITEMBUCODE
FROM #EARNING A
WHERE A.CUSTOMER_ID = PM_CUST_SVOC_POINT_REDEMPTION.CUSTOMER_ID
GO

UPDATE PM_CUST_SVOC_POINT_REDEMPTION
SET REDEEM_BU_PREFER = A.ITEMBUCODE
FROM #REDEMPTION A
WHERE A.CUSTOMER_ID = PM_CUST_SVOC_POINT_REDEMPTION.CUSTOMER_ID
AND PARTNER_CLEAN = 'CG' AND ROW = 1
GO

UPDATE PM_CUST_SVOC_POINT_REDEMPTION
SET REDEEM_PARTNER_PREFER = A.ITEMBUCODE
FROM #REDEMPTION A
WHERE A.CUSTOMER_ID = PM_CUST_SVOC_POINT_REDEMPTION.CUSTOMER_ID
AND PARTNER_CLEAN = 'Non-CG' AND ROW = 1
GO


SELECT *
FROM PM_CUST_SVOC_POINT_REDEMPTION
WHERE REDEEM_PARTNER_PREFER IS NOT NULL

/************************************************************************************************************/


/* EARNING SCORE --ADD FROM T1C TEAM */ 

ALTER TABLE [PM_CUST_SVOC_POINT_REDEMPTION]
ADD  TIER_TOTAL_POINT VARCHAR(50)
	,TIER_SHARE_PROMOTION_POINT VARCHAR(50)
	,TIER_MAX_PROMOTION_POINT VARCHAR(50)
	,TIER_TOTAL_POINT_VISIT VARCHAR(50)
	,TIER_EXTRA_POINT_VISIT VARCHAR(50)
GO

UPDATE [PM_CUST_SVOC_POINT_REDEMPTION]
SET TIER_TOTAL_POINT = CASE WHEN ISNULL([NORMAL_POINTS_EARNED]+[EXTRA_POINTS_EARNED],0) <= 100 THEN '01) 0-100 points'
							WHEN [NORMAL_POINTS_EARNED]+[EXTRA_POINTS_EARNED] <= 500 THEN '02) 100-500 points'
							WHEN [NORMAL_POINTS_EARNED]+[EXTRA_POINTS_EARNED] <= 1000 THEN '03) 500-1000 points'
							WHEN [NORMAL_POINTS_EARNED]+[EXTRA_POINTS_EARNED] <= 3000 THEN '04) 1000-3000 points'
							WHEN [NORMAL_POINTS_EARNED]+[EXTRA_POINTS_EARNED] <= 5000 THEN '05) 3000-5000 points'
							WHEN [NORMAL_POINTS_EARNED]+[EXTRA_POINTS_EARNED] > 5000 THEN '06) >5000 points'
							END
	,TIER_SHARE_PROMOTION_POINT = CASE WHEN [NORMAL_POINTS_EARNED]+[EXTRA_POINTS_EARNED] > 0 THEN
									   CASE WHEN ([EXTRA_POINTS_EARNED]*1.0)/([NORMAL_POINTS_EARNED]+[EXTRA_POINTS_EARNED]) = 0 THEN '01) 0% - 10%'
										    WHEN ([EXTRA_POINTS_EARNED]*1.0)/([NORMAL_POINTS_EARNED]+[EXTRA_POINTS_EARNED]) <= 0.1 THEN '01) 0% - 10%'			
										    WHEN ([EXTRA_POINTS_EARNED]*1.0)/([NORMAL_POINTS_EARNED]+[EXTRA_POINTS_EARNED]) <= 0.3 THEN '02) 10% - 30%'				
										    WHEN ([EXTRA_POINTS_EARNED]*1.0)/([NORMAL_POINTS_EARNED]+[EXTRA_POINTS_EARNED]) <= 0.5 THEN '03) 30% - 50%'			
										    WHEN ([EXTRA_POINTS_EARNED]*1.0)/([NORMAL_POINTS_EARNED]+[EXTRA_POINTS_EARNED]) <= 0.7 THEN '04) 50% - 70%'				
										    WHEN ([EXTRA_POINTS_EARNED]*1.0)/([NORMAL_POINTS_EARNED]+[EXTRA_POINTS_EARNED]) <= 0.9 THEN '05) 70% - 90%'	
										    WHEN ([EXTRA_POINTS_EARNED]*1.0)/([NORMAL_POINTS_EARNED]+[EXTRA_POINTS_EARNED]) > 0.9 THEN '06) >90%'
										    END
									   WHEN ISNULL([NORMAL_POINTS_EARNED]+[EXTRA_POINTS_EARNED],0) = 0 THEN '01) 0% - 10%'
								  END									   
	,TIER_MAX_PROMOTION_POINT = CASE WHEN ISNULL(MAX_EXTRA_POINT,0) <= 100 THEN '01) 0-100 point'
									 WHEN MAX_EXTRA_POINT <= 500 THEN '02) 100-500 points'	
									 WHEN MAX_EXTRA_POINT <= 1000 THEN '03) 500-1000 points'
									 WHEN MAX_EXTRA_POINT <= 2000 THEN '04) 1000-2000 points'
									 WHEN MAX_EXTRA_POINT <= 3000 THEN '05) 2000-3000 points'
									 WHEN MAX_EXTRA_POINT <= 5000 THEN '06) 3000-5000 points'
									 WHEN MAX_EXTRA_POINT > 5000 THEN '07) >5000 points'
									 END
	,TIER_TOTAL_POINT_VISIT = CASE WHEN TOTAL_EARN_VISIT = 1 THEN '1) 1 visit'
								   WHEN TOTAL_EARN_VISIT <= 3  THEN '2) 2-3 visits'
								   WHEN TOTAL_EARN_VISIT <= 6 THEN '3) 4-6 visits'
								   WHEN TOTAL_EARN_VISIT <= 9 THEN '4) 7-9 visits'
								   WHEN TOTAL_EARN_VISIT <= 12 THEN '5) 9-12 visits'
								   WHEN TOTAL_EARN_VISIT > 12 THEN '6) >12 visits'
								   END
	,TIER_EXTRA_POINT_VISIT = CASE WHEN TOTAL_EARN_VISIT > 0 THEN
								   CASE WHEN TOTAL_EXTRA_EARN_VISIT*1.0/TOTAL_EARN_VISIT = 0 THEN '01) 0% - 10%'
									   WHEN TOTAL_EXTRA_EARN_VISIT*1.0/TOTAL_EARN_VISIT <= 0.1 THEN '01) 0% - 10%'
									   WHEN TOTAL_EXTRA_EARN_VISIT*1.0/TOTAL_EARN_VISIT <= 0.3 THEN '02) 10% - 30%'
									   WHEN TOTAL_EXTRA_EARN_VISIT*1.0/TOTAL_EARN_VISIT <= 0.5 THEN '03) 30% - 50%'
									   WHEN TOTAL_EXTRA_EARN_VISIT*1.0/TOTAL_EARN_VISIT <= 0.7 THEN '04) 50% - 70%'
									   WHEN TOTAL_EXTRA_EARN_VISIT*1.0/TOTAL_EARN_VISIT <= 0.9 THEN '05) 70% - 90%'
									   WHEN TOTAL_EXTRA_EARN_VISIT*1.0/TOTAL_EARN_VISIT > 0.9 THEN '06) >90%'
									   END
								   WHEN ISNULL(TOTAL_EXTRA_EARN_VISIT*1.0/TOTAL_EARN_VISIT,0) = 0 THEN '01) 0% - 10%'
							  END 
GO
/***************************************************************************************************/

/* BURNING SCORE */


/* BURNING SCORE --ADD FROM T1C TEAM */

ALTER TABLE [PM_CUST_SVOC_POINT_REDEMPTION]
ADD  T1CPrivilege_VISIT INT
	,Tier_T1CPrivilege_VISIT VARCHAR(30)
	,ONLINE_VISIT INT
	,Tier_online_Redeem_Visit VARCHAR(20)
	,TIER_SUM_POINT_REDEEM VARCHAR(50)
	,TIER_REDEEM_RATIO VARCHAR(50)
	,TIER_REDEEM_VISIT VARCHAR(50)
	,TIER_SPENDING_VISIT VARCHAR(50)
	,TIER_RATIO_VISIT VARCHAR(50) --Tier_Share_Redeem_visit
	,TIER_MAX_REDEEM_POINT VARCHAR(50) --Tier_Max_Redeem_Point
GO

--PRIVILEGE
UPDATE [PM_CUST_SVOC_POINT_REDEMPTION]
SET	 T1CPrivilege_VISIT = A.T1CPrivilege_VISIT
	,Tier_T1CPrivilege_VISIT = CASE WHEN ISNULL(A.T1CPrivilege_VISIT,0) = 0 THEN '01) 0 visit'
									WHEN A.T1CPrivilege_VISIT = 1 THEN '02) 1 visit'
									WHEN A.T1CPrivilege_VISIT <= 4 THEN '03) 2-4 visits'
									WHEN A.T1CPrivilege_VISIT > 4 THEN '04) >4 visits'
									END
FROM (	SELECT CUSTOMERID,COUNT(DISTINCT Receiptdate) AS T1CPrivilege_VISIT
		FROM PM_POINT_REDEMPTION
		WHERE Receiptdate < '2018-07-21'
		AND PointsAffected > 0 AND PointsAffected <= 40
		AND REDEEM_TYPE != 'PREMIUM'
		GROUP BY CUSTOMERID
	 ) A
WHERE A.CUSTOMERID = [PM_CUST_SVOC_POINT_REDEMPTION].CUSTOMER_ID
GO

UPDATE [PM_CUST_SVOC_POINT_REDEMPTION]
SET Tier_T1CPrivilege_VISIT = '01) 0 visit'
WHERE T1CPrivilege_VISIT IS NULL
GO

UPDATE [PM_CUST_SVOC_POINT_REDEMPTION]
SET  ONLINE_VISIT = A.ONLINE_VISIT
	,Tier_online_Redeem_Visit = CASE WHEN ISNULL(A.ONLINE_VISIT,0) = 0 THEN '01) 0 visit'
									 WHEN A.ONLINE_VISIT = 1 THEN '02) 1 visit' 
									 WHEN A.ONLINE_VISIT <= 4 THEN '03) 2-4 visits' 
									 WHEN A.ONLINE_VISIT > 4 THEN '04) >4 visits'
									 END 
FROM (	SELECT CUSTOMERID,COUNT(DISTINCT RECEIPTDATE) AS ONLINE_VISIT
		FROM [PM_POINT_REDEMPTION]
		WHERE ONLINE_FLAG = 1
		GROUP BY CUSTOMERID
	 ) A
WHERE A.CUSTOMERID = [PM_CUST_SVOC_POINT_REDEMPTION].CUSTOMER_ID
GO

UPDATE [PM_CUST_SVOC_POINT_REDEMPTION]
SET TIER_SUM_POINT_REDEEM = CASE WHEN SUM_POINT_REDEEMED <= 0 THEN '00) 0 points'
								 WHEN SUM_POINT_REDEEMED <= 500 THEN '01) 1-500 points'
								 WHEN SUM_POINT_REDEEMED <= 1000 THEN '02) 301-1000 points'
								 WHEN SUM_POINT_REDEEMED <= 3000 THEN '03) 1001-3000 points'
								 WHEN SUM_POINT_REDEEMED <= 5000 THEN '04) 3001-5000 points'
								 WHEN SUM_POINT_REDEEMED <= 8000 THEN '05) 5001-8000 points'
								 WHEN SUM_POINT_REDEEMED > 8000 THEN '06) > 8000 points'
								 END
	,TIER_REDEEM_RATIO = CASE WHEN [NORMAL_POINTS_EARNED]+[EXTRA_POINTS_EARNED] > 0 THEN CASE
							  WHEN SUM_POINT_REDEEMED*1.0/([NORMAL_POINTS_EARNED]+[EXTRA_POINTS_EARNED]) <= 0.1 THEN '01) 0% - 10%'
							  WHEN SUM_POINT_REDEEMED*1.0/([NORMAL_POINTS_EARNED]+[EXTRA_POINTS_EARNED]) <= 0.3 THEN '02) 10% - 30%'
							  WHEN SUM_POINT_REDEEMED*1.0/([NORMAL_POINTS_EARNED]+[EXTRA_POINTS_EARNED]) <= 0.5 THEN '03) 30% - 50%'
							  WHEN SUM_POINT_REDEEMED*1.0/([NORMAL_POINTS_EARNED]+[EXTRA_POINTS_EARNED]) <= 0.7 THEN '04) 50% - 70%'
							  WHEN SUM_POINT_REDEEMED*1.0/([NORMAL_POINTS_EARNED]+[EXTRA_POINTS_EARNED]) <= 0.9 THEN '05) 70% - 90%'
							  WHEN SUM_POINT_REDEEMED*1.0/([NORMAL_POINTS_EARNED]+[EXTRA_POINTS_EARNED]) <= 1   THEN '06) > 90%'
							  WHEN SUM_POINT_REDEEMED*1.0/([NORMAL_POINTS_EARNED]+[EXTRA_POINTS_EARNED]) > 1   THEN '07) > 100%'
							  END
						WHEN [NORMAL_POINTS_EARNED]+[EXTRA_POINTS_EARNED] = 0 THEN '01) 0% - 10%'
						WHEN [NORMAL_POINTS_EARNED]+[EXTRA_POINTS_EARNED] IS NULL THEN '01) 0% - 10%'
						END						 
	,TIER_REDEEM_VISIT = CASE WHEN 	TOTAL_REDEEM_VISIT <= 1 THEN '01) 1 visit'
							  WHEN TOTAL_REDEEM_VISIT <= 3 THEN '02) 2-3 visits'
							  WHEN TOTAL_REDEEM_VISIT <= 5 THEN '03) 4-5 visits'
							  WHEN TOTAL_REDEEM_VISIT <= 7 THEN '04) 6-7 visits'
							  WHEN TOTAL_REDEEM_VISIT <= 9 THEN '05) 8-9 visits'
							  WHEN TOTAL_REDEEM_VISIT > 9 THEN '06) > 9 visits'
							  END
	,TIER_SPENDING_VISIT = CASE WHEN ISNULL(TOTAL_SPENDING_VISIT,0) = 0 THEN '00) 0 visit'
								WHEN TOTAL_SPENDING_VISIT <= 1 THEN '01) 1 visit'
								WHEN TOTAL_SPENDING_VISIT <= 3 THEN '02) 2-3 visits'
								WHEN TOTAL_SPENDING_VISIT <= 6 THEN '03) 4-6 visits'
								WHEN TOTAL_SPENDING_VISIT <= 9 THEN '04) 7-9 visits'
								WHEN TOTAL_SPENDING_VISIT <= 12 THEN '05) 10-12 visits'
								WHEN TOTAL_SPENDING_VISIT > 12 THEN '06) > 12 visits'
								END
    ,TIER_RATIO_VISIT = CASE WHEN TOTAL_SPENDING_VISIT > 0 THEN
							 CASE WHEN TOTAL_REDEEM_VISIT*1.0/TOTAL_SPENDING_VISIT <= 0.1 THEN '01) 0% - 10%'
								  WHEN TOTAL_REDEEM_VISIT*1.0/TOTAL_SPENDING_VISIT <= 0.3 Then '02) 10% - 30%'
								  WHEN TOTAL_REDEEM_VISIT*1.0/TOTAL_SPENDING_VISIT <= 0.5 Then '03) 30% - 50%'
								  WHEN TOTAL_REDEEM_VISIT*1.0/TOTAL_SPENDING_VISIT <= 0.7 Then '04) 50% - 70%'
								  WHEN TOTAL_REDEEM_VISIT*1.0/TOTAL_SPENDING_VISIT <= 0.9 Then '05) 70% - 90%'
								  WHEN TOTAL_REDEEM_VISIT*1.0/TOTAL_SPENDING_VISIT > 0.9 THEN '06) > 90%'
							 END
						WHEN ISNULL(TOTAL_SPENDING_VISIT,0) = 0 THEN '01) 0% - 10%'	  
						END
	,TIER_MAX_REDEEM_POINT = CASE WHEN ISNULL(POINT_USED_MAX,0) <= 100 THEN '01) 0-100 points'
								  WHEN POINT_USED_MAX <= 500 THEN '02) 100-500 points'
								  WHEN POINT_USED_MAX <= 1000 THEN '03) 500-1000 points'			  					 
								  WHEN POINT_USED_MAX <= 2000 THEN '04) 1000-2000 points'				  					 
								  WHEN POINT_USED_MAX <= 3000 THEN '05) 2000-3000 points'				  					 
								  WHEN POINT_USED_MAX <= 5000 THEN '06) 3000-5000 points'
								  WHEN POINT_USED_MAX > 5000 THEN '07) > 5000 points'
								  END
GO

/***************************************************************************************************************/


/*
	Adjusted Earning Score
*/


/* Earning Score */

USE [VPM_DATA]

ALTER TABLE PM_CUST_SVOC_POINT_REDEMPTION
ADD EXTRA_POINT_SCORE FLOAT
GO

DECLARE  @TOTAL_POINT_EARN_DECILE1 FLOAT
		,@TOTAL_EXTRA_EARN_VISIT_DECILE1 FLOAT
		,@MAX_EXTRA_POINT_DECILE1 FLOAT

--9817
SET @TOTAL_POINT_EARN_DECILE1 = (	SELECT MIN(TTL_POINTS_EARNED)
										FROM	(	SELECT EXTRA_POINTS_EARNED+NORMAL_POINTS_EARNED AS TTL_POINTS_EARNED,NTILE(10) OVER (ORDER BY EXTRA_POINTS_EARNED+NORMAL_POINTS_EARNED DESC) AS DECILE
													FROM PM_CUST_SVOC_POINT_REDEMPTION
													WHERE REDEEM_FLAG = 1 AND EXTRA_POINTS_EARNED+NORMAL_POINTS_EARNED > 0
											) A
										WHERE DECILE = 1
								  )
								  
--9							  
SET @TOTAL_EXTRA_EARN_VISIT_DECILE1 = (	SELECT MIN(TOTAL_EXTRA_EARN_VISIT)
										FROM	(	SELECT TOTAL_EXTRA_EARN_VISIT,NTILE(10) OVER (ORDER BY TOTAL_EXTRA_EARN_VISIT DESC) AS DECILE
													FROM PM_CUST_SVOC_POINT_REDEMPTION
													WHERE REDEEM_FLAG = 1 AND TOTAL_EXTRA_EARN_VISIT > 0
											) A
										WHERE DECILE = 1
									  )
									  
--1280									  
SET @MAX_EXTRA_POINT_DECILE1 = (	SELECT MIN(MAX_EXTRA_POINT)
									FROM	(	SELECT MAX_EXTRA_POINT,NTILE(10) OVER (ORDER BY MAX_EXTRA_POINT DESC) AS DECILE
												FROM PM_CUST_SVOC_POINT_REDEMPTION
												WHERE REDEEM_FLAG = 1 AND MAX_EXTRA_POINT > 0
										) A
									WHERE DECILE = 1
								)										  						  
SELECT @TOTAL_POINT_EARN_DECILE1
	  ,@TOTAL_EXTRA_EARN_VISIT_DECILE1
	  ,@MAX_EXTRA_POINT_DECILE1
	  
UPDATE PM_CUST_SVOC_POINT_REDEMPTION
SET EXTRA_POINT_SCORE =  (CASE WHEN EXTRA_POINTS_EARNED+NORMAL_POINTS_EARNED > @TOTAL_POINT_EARN_DECILE1 THEN 1
							   WHEN EXTRA_POINTS_EARNED+NORMAL_POINTS_EARNED > 0 THEN (EXTRA_POINTS_EARNED+NORMAL_POINTS_EARNED)/@TOTAL_POINT_EARN_DECILE1
							   ELSE 0 END) * 0.3
						+(CASE WHEN TOTAL_EXTRA_EARN_VISIT > @TOTAL_EXTRA_EARN_VISIT_DECILE1 THEN 1
							   WHEN TOTAL_EXTRA_EARN_VISIT > 0 THEN TOTAL_EXTRA_EARN_VISIT/@TOTAL_EXTRA_EARN_VISIT_DECILE1
							   ELSE 0 END) * 0.4
						+(CASE WHEN MAX_EXTRA_POINT > @MAX_EXTRA_POINT_DECILE1 THEN 1
							   WHEN MAX_EXTRA_POINT > 0 THEN MAX_EXTRA_POINT/@MAX_EXTRA_POINT_DECILE1
							   ELSE 0 END) * 0.3
GO		   							   						  

/***************************************************************************************************************/


/*
	Adjusted Burning Score
*/

/* Burning Score */

USE [VPM_DATA]

ALTER TABLE PM_CUST_SVOC_POINT_REDEMPTION
ADD REDEEM_POINT_SCORE FLOAT
GO

DECLARE  @SUM_POINT_REDEEMED_DECILE1 FLOAT
		,@TURNOVER_CNT_DECILE1 FLOAT
		,@REDEEM_ONLINE_DECILE1 FLOAT
		,@T1CPrivilege_VISIT_DECILE1 FLOAT
		
--8872
SET  @SUM_POINT_REDEEMED_DECILE1 = (	SELECT MIN(SUM_POINT_REDEEMED)
										FROM	(	SELECT SUM_POINT_REDEEMED,NTILE(10) OVER (ORDER BY SUM_POINT_REDEEMED DESC) AS DECILE
													FROM PM_CUST_SVOC_POINT_REDEMPTION
													WHERE REDEEM_FLAG = 1
												) A
										WHERE DECILE = 1
								  )
--11.28010853682900								  			  
SET @TURNOVER_CNT_DECILE1 = (	SELECT MIN(TURNOVER_CNT)
								FROM	(	SELECT POINT_TURNOVER*CNT_REDEMPTION AS TURNOVER_CNT,NTILE(10) OVER (ORDER BY POINT_TURNOVER*CNT_REDEMPTION DESC) AS DECILE
											FROM PM_CUST_SVOC_POINT_REDEMPTION
											WHERE REDEEM_FLAG = 1
									) A
								WHERE DECILE = 1
							  )
SELECT @SUM_POINT_REDEEMED_DECILE1,@TURNOVER_CNT_DECILE1
UPDATE PM_CUST_SVOC_POINT_REDEMPTION
SET REDEEM_POINT_SCORE = 
 0.4*ISNULL((CASE WHEN SUM_POINT_REDEEMED >= @SUM_POINT_REDEEMED_DECILE1 THEN 1 ELSE SUM_POINT_REDEEMED/@SUM_POINT_REDEEMED_DECILE1 END),0)
+0.4*ISNULL((CASE WHEN POINT_TURNOVER*CNT_REDEMPTION >= @TURNOVER_CNT_DECILE1 THEN 1 ELSE (POINT_TURNOVER*CNT_REDEMPTION)/@TURNOVER_CNT_DECILE1 END),0) 		
+0.1*ISNULL((CASE WHEN ISNULL(REDEEM_ONLINE,0) > 1 THEN 1 ELSE REDEEM_ONLINE/2 END),0) 	
+0.1*ISNULL((CASE WHEN ISNULL(T1CPrivilege_VISIT,0) > 0 THEN 1 ELSE 0 END),0)
GO

ALTER TABLE PM_CUST_SVOC_POINT_REDEMPTION 
ADD REDEEM_POINT_SEGMENT VARCHAR(30)
GO
UPDATE PM_CUST_SVOC_POINT_REDEMPTION
SET REDEEM_POINT_SEGMENT = CASE WHEN REDEEM_POINT_SCORE >= 0.7 THEN 'HIGH'
								WHEN REDEEM_POINT_SCORE >= 0.4 THEN 'MID'
								WHEN REDEEM_POINT_SCORE < 0.4 THEN 'LOW'
								END
GO		

/******************************************************************************************************/

/* Affinity Score */

ALTER TABLE PM_CUST_SVOC_POINT_REDEMPTION
DROP COLUMN POINT_AFFINITY_SCORE, POINT_AFFINITY_SEGMENT
GO
ALTER TABLE PM_CUST_SVOC_POINT_REDEMPTION
ADD POINT_AFFINITY_SCORE FLOAT, POINT_AFFINITY_SEGMENT VARCHAR(30)
GO

UPDATE PM_CUST_SVOC_POINT_REDEMPTION
SET POINT_AFFINITY_SCORE = (0.3*EXTRA_POINT_SCORE) + (0.7*REDEEM_POINT_SCORE)
WHERE REDEEM_FLAG = 1 OR POINT_BALANCE > 400
GO

UPDATE PM_CUST_SVOC_POINT_REDEMPTION
SET POINT_AFFINITY_SEGMENT = CASE	WHEN POINT_AFFINITY_SCORE > 0.6 THEN 'HIGH'
								WHEN POINT_AFFINITY_SCORE > 0.3 THEN 'MID'
								WHEN POINT_AFFINITY_SCORE <= 0.3 THEN 'LOW'
								END
WHERE POINT_AFFINITY_SCORE IS NOT NULL								
GO



/*******************************************************************************************************/


ALTER TABLE PM_CUST_SVOC_POINT_REDEMPTION
DROP COLUMN REDEMPTION_PATTERN_SEGMENT
GO
ALTER TABLE PM_CUST_SVOC_POINT_REDEMPTION
ADD REDEMPTION_PATTERN_SEGMENT VARCHAR(30)
GO
DECLARE  @CNT_REDEMPTION_DECILE1 FLOAT
		,@CNT_REDEMPTION_AVG FLOAT
		,@CNT_REDEMPTION_SD FLOAT
		,@EXTRA_POINT_DECILE1 FLOAT
		,@RATIO_POINT_DECILE1 FLOAT
		,@POINT_EARN_DECILE10 FLOAT
		,@POINT_EARN_AVG FLOAT
		,@POINT_EARN_MEDIAN FLOAT
		,@BURN_PER_TIME_DECILE1 FLOAT
		,@BURN_PER_TIME_AVG FLOAT
		,@EXTRA_POINT_AVG FLOAT
		,@POINT_BALANCE_AVG FLOAT
		,@PERCENT_DISCOUNT_AVG_DECILE1 FLOAT
		,@PERCENT_DISCOUNT_AVG_AVG FLOAT
		
--8
SET @CNT_REDEMPTION_DECILE1 = (	SELECT DISTINCT PERCENTILE_DISC(0.9) 
									   WITHIN GROUP (ORDER BY CNT_REDEMPTION) OVER ()
								FROM PM_CUST_SVOC_POINT_REDEMPTION
								WHERE REDEEM_FLAG = 1
							  )
SELECT @CNT_REDEMPTION_DECILE1

--3
SET @CNT_REDEMPTION_AVG = (	SELECT AVG(CNT_REDEMPTION)
							FROM PM_CUST_SVOC_POINT_REDEMPTION
							WHERE CNT_REDEMPTION < 500 AND REDEEM_FLAG = 1
						  )					   
SELECT @CNT_REDEMPTION_AVG

--4.8
SET @CNT_REDEMPTION_SD = (	SELECT STDEV(CNT_REDEMPTION)
							FROM PM_CUST_SVOC_POINT_REDEMPTION
							WHERE CNT_REDEMPTION < 500 AND REDEEM_FLAG = 1
						  )					   
SELECT @CNT_REDEMPTION_AVG

--0.233452593917710
SET @EXTRA_POINT_DECILE1 = (	SELECT DISTINCT PERCENTILE_DISC(0.9) 
										WITHIN GROUP (ORDER BY EXTRA_POINTS_RATIO) OVER ()
								FROM PM_CUST_SVOC_POINT_REDEMPTION
								WHERE REDEEM_FLAG = 1
						   )						   
SELECT @EXTRA_POINT_DECILE1

--0.143285721267117
SET @EXTRA_POINT_AVG =		(	SELECT AVG(EXTRA_POINTS_RATIO)
								FROM PM_CUST_SVOC_POINT_REDEMPTION
								WHERE REDEEM_FLAG = 1
							)
SELECT @EXTRA_POINT_AVG	

--4.850000000000
SET @RATIO_POINT_DECILE1 =	(	SELECT DISTINCT PERCENTILE_DISC(0.9) 
										WITHIN GROUP (ORDER BY (NORMAL_POINTS_EARNED + EXTRA_POINTS_EARNED)*1.0/(SUM_POINT_REDEEMED)) OVER ()
								FROM PM_CUST_SVOC_POINT_REDEMPTION
								WHERE SUM_POINT_REDEEMED > 0 AND REDEEM_FLAG = 1
							)
SELECT @RATIO_POINT_DECILE1

--9417
SET @POINT_EARN_DECILE10 =	(	SELECT DISTINCT PERCENTILE_DISC(0.1) 
										WITHIN GROUP (ORDER BY (NORMAL_POINTS_EARNED + EXTRA_POINTS_EARNED)) OVER ()
								FROM PM_CUST_SVOC_POINT_REDEMPTION
								WHERE (NORMAL_POINTS_EARNED + EXTRA_POINTS_EARNED) <= 30000 AND REDEEM_FLAG = 1
							)
SELECT @POINT_EARN_DECILE10

--4795
SET @POINT_EARN_AVG =	(	SELECT AVG((NORMAL_POINTS_EARNED + EXTRA_POINTS_EARNED))
							FROM PM_CUST_SVOC_POINT_REDEMPTION
							WHERE (NORMAL_POINTS_EARNED + EXTRA_POINTS_EARNED) <= 30000 AND REDEEM_FLAG = 1
						)				
SELECT @POINT_EARN_AVG

--1613
SET @POINT_EARN_MEDIAN =	(	SELECT DISTINCT PERCENTILE_DISC(0.5) 
								   WITHIN GROUP (ORDER BY (NORMAL_POINTS_EARNED + EXTRA_POINTS_EARNED)) OVER ()
								FROM PM_CUST_SVOC_POINT_REDEMPTION
								WHERE (NORMAL_POINTS_EARNED + EXTRA_POINTS_EARNED) <= 30000 AND REDEEM_FLAG = 1
							)
							
SELECT @POINT_EARN_MEDIAN

--1723.333333333333
SET @BURN_PER_TIME_DECILE1 =	(	SELECT DISTINCT PERCENTILE_DISC(0.9) 
									   WITHIN GROUP (ORDER BY (SUM_POINT_REDEEMED*1.0/CNT_REDEMPTION)) OVER ()
									FROM PM_CUST_SVOC_POINT_REDEMPTION
									WHERE CNT_REDEMPTION > 0 AND REDEEM_FLAG = 1
								)
SELECT @BURN_PER_TIME_DECILE1

--846.666686828328
SET @BURN_PER_TIME_AVG =	(	SELECT AVG(SUM_POINT_REDEEMED*1.0/CNT_REDEMPTION)
								FROM PM_CUST_SVOC_POINT_REDEMPTION
								WHERE CNT_REDEMPTION > 0 AND REDEEM_FLAG = 1
							)
SELECT @BURN_PER_TIME_AVG

--2188.02721979392
SET @POINT_BALANCE_AVG =	(   SELECT AVG(POINT_BALANCE)
								FROM PM_CUST_SVOC_POINT_REDEMPTION
								WHERE REDEEM_FLAG = 1
							)
SELECT @POINT_BALANCE_AVG

--0.2
SET @PERCENT_DISCOUNT_AVG_DECILE1 = (	SELECT  DISTINCT PERCENTILE_DISC(0.9) 
										WITHIN GROUP (ORDER BY PERCENT_DISCOUNT_AVG) OVER ()
										FROM PM_CUST_SVOC_POINT_REDEMPTION
										WHERE REDEEM_FLAG = 1
									)
SELECT @PERCENT_DISCOUNT_AVG_DECILE1

UPDATE PM_CUST_SVOC_POINT_REDEMPTION
SET REDEMPTION_PATTERN_SEGMENT =
CASE	WHEN EXTRA_POINTS_RATIO > @EXTRA_POINT_AVG /*0.143285721267117*/
			 AND CNT_REDEMPTION >= @CNT_REDEMPTION_DECILE1 /*8*/
			 THEN '1.PROMOTION HUNTER'
		WHEN CNT_REDEMPTION < @CNT_REDEMPTION_AVG AND CNT_REDEMPTION > 0 --CHANGE
			 AND (NORMAL_POINTS_EARNED + EXTRA_POINTS_EARNED) > @POINT_EARN_AVG /*4795*/
			 AND EXTRA_POINTS_RATIO > @EXTRA_POINT_AVG
			 THEN '2.COLLECTOR'
		WHEN SUM_POINT_REDEEMED > 0 
			 AND (NORMAL_POINTS_EARNED + EXTRA_POINTS_EARNED)/(SUM_POINT_REDEEMED) >= @RATIO_POINT_DECILE1 /*4.85*/
			 AND (SUM_POINT_REDEEMED*1.0/CNT_REDEMPTION) > @BURN_PER_TIME_AVG  /*846.666686828328*/
			 THEN '2.COLLECTOR'
		WHEN SUM_POINT_REDEEMED > 0 
			 AND CNT_REDEMPTION <= @CNT_REDEMPTION_AVG /*3*/
			 AND (SUM_POINT_REDEEMED*1.0/CNT_REDEMPTION) >= @BURN_PER_TIME_DECILE1 /*1723.333333333333*/
			 THEN '2.COLLECTOR'
		WHEN PERCENT_DISCOUNT_AVG > @PERCENT_DISCOUNT_AVG_DECILE1 THEN '3.VALUE SEEKER'
		WHEN CNT_REDEMPTION > @CNT_REDEMPTION_AVG + @CNT_REDEMPTION_SD/2 /*3*/ /*4.8*/
			 THEN '4.POINT SPENDER'
		WHEN POINT_BALANCE >= @POINT_BALANCE_AVG /*2188.02*/ AND (NORMAL_POINTS_EARNED + EXTRA_POINTS_EARNED) > @POINT_EARN_DECILE10
			 THEN '5.PASSIVE'
		WHEN (NORMAL_POINTS_EARNED + EXTRA_POINTS_EARNED) > @POINT_EARN_MEDIAN /*1613*/
			 THEN '5.PASSIVE'
		WHEN CNT_REDEMPTION = @CNT_REDEMPTION_AVG /*3*/ 
			 AND (NORMAL_POINTS_EARNED + EXTRA_POINTS_EARNED) > @POINT_EARN_DECILE10
			 THEN '5.PASSIVE'
		WHEN ISNULL(CNT_REDEMPTION,0) > 0THEN '6.LOW ACTIVITY'	 
		END
WHERE REDEEM_FLAG = 1		
GO

UPDATE PM_CUST_SVOC_POINT_REDEMPTION
SET REDEMPTION_PATTERN_SEGMENT = CASE WHEN POINT_BALANCE > 400 THEN '7.INACTIVE' END
WHERE REDEEM_SEGMENT IS NULL

ALTER TABLE PM_CUST_SVOC_POINT_REDEMPTION
ADD PERCENT_EXTRA_POINT FLOAT
GO
UPDATE PM_CUST_SVOC_POINT_REDEMPTION
SET PERCENT_EXTRA_POINT = (NORMAL_POINTS_EARNED+EXTRA_POINTS_EARNED)/NORMAL_POINTS_EARNED
WHERE NORMAL_POINTS_EARNED > 0 
