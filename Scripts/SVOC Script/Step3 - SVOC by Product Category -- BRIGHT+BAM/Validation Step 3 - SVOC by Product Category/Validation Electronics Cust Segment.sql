SELECT 'PM_SKUTAGGING_ELECTRONICS_CUST_SEGMENT' AS TABLE_NAME
,'APPLE_FLAG' AS SEGMENT
,COALESCE((CASE WHEN APPLE_FLAG= 1 THEN 'YES' ELSE 'N/A' END),'N/A') AS SEGMENT_VALUE
,COUNT(DISTINCT CUSTOMER_ID) AS TOTAL_CUST
FROM VPM_DATA.PM_SKUTAGGING_ELECTRONICS_CUST_SEGMENT
GROUP BY COALESCE((CASE WHEN APPLE_FLAG= 1 THEN 'YES' ELSE 'N/A' END),'N/A')
UNION ALL
SELECT 'PM_SKUTAGGING_ELECTRONICS_CUST_SEGMENT' AS TABLE_NAME
,'APPLE_LOVER_NEW_ARRIVAL' AS SEGMENT
,COALESCE((CASE WHEN APPLE_LOVER_NEW_ARRIVAL = 1 THEN 'YES' ELSE 'N/A' END),'N/A') AS SEGMENT_VALUE
,COUNT(DISTINCT CUSTOMER_ID) AS TOTAL_CUST
FROM VPM_DATA.PM_SKUTAGGING_ELECTRONICS_CUST_SEGMENT
GROUP BY COALESCE((CASE WHEN APPLE_LOVER_NEW_ARRIVAL = 1 THEN 'YES' ELSE 'N/A' END),'N/A')
UNION ALL
SELECT 'PM_SKUTAGGING_ELECTRONICS_CUST_SEGMENT' AS TABLE_NAME
,'CAMERA_SEGMENT' AS SEGMENT
,COALESCE(CAMERA_SEGMENT,'N/A') AS SEGMENT_VALUE
,COUNT(DISTINCT CUSTOMER_ID) AS TOTAL_CUST
FROM VPM_DATA.PM_SKUTAGGING_ELECTRONICS_CUST_SEGMENT
GROUP BY COALESCE(CAMERA_SEGMENT,'N/A')
UNION ALL
SELECT 'PM_SKUTAGGING_ELECTRONICS_CUST_SEGMENT' AS TABLE_NAME
,'ELECTRONICS_LUXURY_SEGMENT' AS SEGMENT
,COALESCE(CASE WHEN ELECTRONICS_TOTAL_SPEND > 0 OR ELECTRONICS_LUXURY_SCORE > 0 THEN COALESCE(ELECTRONICS_LUXURY_SEGMENT,'MASS') ELSE 'N/A' END,'N/A') AS SEGMENT_VALUE
,COUNT(DISTINCT CUSTOMER_ID) AS TOTAL_CUST
FROM VPM_DATA.PM_SKUTAGGING_ELECTRONICS_CUST_SEGMENT
GROUP BY COALESCE(CASE WHEN ELECTRONICS_TOTAL_SPEND > 0 OR ELECTRONICS_LUXURY_SCORE > 0 THEN COALESCE(ELECTRONICS_LUXURY_SEGMENT,'MASS') ELSE 'N/A' END,'N/A')
UNION ALL
SELECT 'PM_SKUTAGGING_ELECTRONICS_CUST_SEGMENT' AS TABLE_NAME
,'ELECTRONICS_PROMO_SENSITIVITY_SEGMENT' AS SEGMENT
,COALESCE(ELECTRONICS_PROMO_SENSITIVITY_SEGMENT,'NO SCORE') AS SEGMENT_VALUE
,COUNT(DISTINCT CUSTOMER_ID) AS TOTAL_CUST
FROM VPM_DATA.PM_SKUTAGGING_ELECTRONICS_CUST_SEGMENT
GROUP BY COALESCE(ELECTRONICS_PROMO_SENSITIVITY_SEGMENT,'NO SCORE')
UNION ALL
SELECT 'PM_SKUTAGGING_ELECTRONICS_CUST_SEGMENT' AS TABLE_NAME
,'TECH_SAVVY' AS SEGMENT
,COALESCE((CASE WHEN TECH_SAVVY= 1 THEN 'YES' ELSE 'N/A' END),'N/A') AS SEGMENT_VALUE
,COUNT(DISTINCT CUSTOMER_ID) AS TOTAL_CUST
FROM VPM_DATA.PM_SKUTAGGING_ELECTRONICS_CUST_SEGMENT
GROUP BY COALESCE((CASE WHEN TECH_SAVVY= 1 THEN 'YES' ELSE 'N/A' END),'N/A')
--UNION ALL
--SELECT 'PM_SKUTAGGING_ELECTRONICS_CUST_SEGMENT' AS TABLE_NAME
--,'' AS SEGMENT
--, AS SEGMENT_VALUE
--,COUNT(DISTINCT CUSTOMER_ID) AS TOTAL_CUST
--FROM VPM_DATA.PM_SKUTAGGING_ELECTRONICS_CUST_SEGMENT
--GROUP BY 