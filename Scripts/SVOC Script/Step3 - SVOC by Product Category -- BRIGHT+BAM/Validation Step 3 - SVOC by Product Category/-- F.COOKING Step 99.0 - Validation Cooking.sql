--------------- COOKING
SELECT * FROM (
	SELECT 'COOKING_FLAG' AS FEATURE_NAME,COALESCE((CASE WHEN COOKING_FLAG= 1 THEN 'YES' ELSE 'N/A' END),'N/A') AS FEATURE_VALUE,count(*) AS cnt				
	FROM VPM_DATA.pm_svoc_cooking
	GROUP BY COALESCE((CASE WHEN COOKING_FLAG= 1 THEN 'YES' ELSE 'N/A' END),'N/A')
	UNION ALL
	SELECT 'KITCHENWARE_LOVER_FLAG' AS FEATURE_NAME,COALESCE((CASE WHEN KITCHENWARE_LOVER_FLAG=1 THEN 'YES' ELSE 'N/A' END),'N/A') AS FEATURE_VALUE,count(*) AS cnt				
	FROM VPM_DATA.pm_svoc_cooking
	GROUP BY COALESCE((CASE WHEN KITCHENWARE_LOVER_FLAG=1 THEN 'YES' ELSE 'N/A' END),'N/A')
	UNION ALL
	SELECT 'COOKING_BAKING_FLAG' AS FEATURE_NAME,COALESCE((CASE WHEN  BAKING_FLAG=1 THEN 'YES' ELSE 'N/A' END),'N/A') AS FEATURE_VALUE,count(*) AS cnt				
	FROM VPM_DATA.pm_svoc_cooking
	GROUP BY COALESCE((CASE WHEN  BAKING_FLAG=1 THEN 'YES' ELSE 'N/A' END),'N/A')
) A
ORDER BY FEATURE_NAME,FEATURE_VALUE
