SELECT 'PM_HOME_PRODUCT_PET_CUST' AS TABLE_NAME
,'PET_OWNER' AS SEGMENT
,COALESCE((CASE WHEN PET_OWNER= 1 THEN 'YES' ELSE 'N/A' END),'N/A') AS SEGMENT_VALUE
,COUNT(DISTINCT CUSTOMER_ID) AS TOTAL_CUST
FROM VPM_DATA.PM_HOME_PRODUCT_PET_CUST
GROUP BY COALESCE((CASE WHEN PET_OWNER= 1 THEN 'YES' ELSE 'N/A' END),'N/A')
UNION ALL
SELECT 'PM_SVOC_COOKING' AS TABLE_NAME
,'BAKING_FLAG' AS SEGMENT
,COALESCE((CASE WHEN  BAKING_FLAG=1 THEN 'YES' ELSE 'N/A' END),'N/A') AS SEGMENT_VALUE
,COUNT(DISTINCT CUSTOMER_ID) AS TOTAL_CUST
FROM VPM_DATA.PM_SVOC_COOKING
GROUP BY COALESCE((CASE WHEN  BAKING_FLAG=1 THEN 'YES' ELSE 'N/A' END),'N/A')
UNION ALL
SELECT 'PM_SVOC_COOKING' AS TABLE_NAME
,'COOKING_FLAG' AS SEGMENT
,COALESCE((CASE WHEN COOKING_FLAG= 1 THEN 'YES' ELSE 'N/A' END),'N/A') AS SEGMENT_VALUE
,COUNT(DISTINCT CUSTOMER_ID) AS TOTAL_CUST
FROM VPM_DATA.PM_SVOC_COOKING
GROUP BY COALESCE((CASE WHEN COOKING_FLAG= 1 THEN 'YES' ELSE 'N/A' END),'N/A')
UNION ALL
SELECT 'PM_SVOC_COOKING' AS TABLE_NAME
,'KITCHENWARE_LOVER_FLAG' AS SEGMENT
,COALESCE((CASE WHEN KITCHENWARE_LOVER_FLAG=1 THEN 'YES' ELSE 'N/A' END),'N/A') AS SEGMENT_VALUE
,COUNT(DISTINCT CUSTOMER_ID) AS TOTAL_CUST
FROM VPM_DATA.PM_SVOC_COOKING
GROUP BY COALESCE((CASE WHEN KITCHENWARE_LOVER_FLAG=1 THEN 'YES' ELSE 'N/A' END),'N/A')
UNION ALL
SELECT 'PM_SVOC_CAR_OWNERSHIP' AS TABLE_NAME
,'FINAL_CAR_OWNERSHIP_FLAG' AS SEGMENT
,COALESCE((CASE WHEN FINAL_CAR_OWNERSHIP_FLAG=1 THEN 'YES' ELSE 'N/A' END),'N/A') AS SEGMENT_VALUE
,COUNT(DISTINCT CUSTOMER_ID) AS TOTAL_CUST
FROM VPM_DATA.PM_SVOC_CAR_OWNERSHIP
GROUP BY COALESCE((CASE WHEN FINAL_CAR_OWNERSHIP_FLAG=1 THEN 'YES' ELSE 'N/A' END),'N/A')
UNION ALL
SELECT 'PM_SVOC_CAR_OWNERSHIP' AS TABLE_NAME
,'FINAL_MOTOR_OWNERSHIP_FLAG' AS SEGMENT
,COALESCE((CASE WHEN FINAL_MOTOR_OWNERSHIP_FLAG=1 THEN 'YES' ELSE 'N/A' END),'N/A') AS SEGMENT_VALUE
,COUNT(DISTINCT CUSTOMER_ID) AS TOTAL_CUST
FROM VPM_DATA.PM_SVOC_CAR_OWNERSHIP
GROUP BY COALESCE((CASE WHEN FINAL_MOTOR_OWNERSHIP_FLAG=1 THEN 'YES' ELSE 'N/A' END),'N/A')
UNION ALL
SELECT 'PM_SVOC_CAR_OWNERSHIP' AS TABLE_NAME
,'FINAL_CAR_ENTHUSIAST_FLAG' AS SEGMENT
,COALESCE((CASE WHEN FINAL_CAR_ENTHUSIAST_FLAG=1 THEN 'YES' ELSE 'N/A' END) ,'N/A') AS SEGMENT_VALUE
,COUNT(DISTINCT CUSTOMER_ID) AS TOTAL_CUST
FROM VPM_DATA.PM_SVOC_CAR_OWNERSHIP
GROUP BY COALESCE((CASE WHEN FINAL_CAR_ENTHUSIAST_FLAG=1 THEN 'YES' ELSE 'N/A' END) ,'N/A')
UNION ALL
SELECT 'PM_SVOC_CAR_OWNERSHIP' AS TABLE_NAME
,'FINAL_CROSS_PROVINCE_DRIVER_FLAG' AS SEGMENT
,COALESCE((CASE WHEN  FINAL_CROSS_PROVINCE_DRIVER_FLAG=1 THEN 'YES' ELSE 'N/A' END),'N/A') AS SEGMENT_VALUE
,COUNT(DISTINCT CUSTOMER_ID) AS TOTAL_CUST
FROM VPM_DATA.PM_SVOC_CAR_OWNERSHIP
GROUP BY COALESCE((CASE WHEN  FINAL_CROSS_PROVINCE_DRIVER_FLAG=1 THEN 'YES' ELSE 'N/A' END),'N/A')
UNION ALL
SELECT 'PM_SVOC_CAR_OWNERSHIP' AS TABLE_NAME
,'FINAL_CROSS_PROVINCE_SHOPPER_FLAG' AS SEGMENT
,COALESCE((CASE WHEN  FINAL_CROSS_PROVINCE_SHOPPER_FLAG=1 THEN 'YES' ELSE 'N/A' END),'N/A') AS SEGMENT_VALUE
,COUNT(DISTINCT CUSTOMER_ID) AS TOTAL_CUST
FROM VPM_DATA.PM_SVOC_CAR_OWNERSHIP
GROUP BY COALESCE((CASE WHEN  FINAL_CROSS_PROVINCE_SHOPPER_FLAG=1 THEN 'YES' ELSE 'N/A' END),'N/A')
UNION ALL
SELECT 'PM_SVOC_CAR_OWNERSHIP' AS TABLE_NAME
,'FINAL_CROSS_PROVINCE_TRAVELLER_FLAG' AS SEGMENT
,COALESCE((CASE WHEN FINAL_CROSS_PROVINCE_DRIVER_FLAG=1 OR FINAL_CROSS_PROVINCE_SHOPPER_FLAG=1 THEN 'YES' ELSE 'N/A' END),'N/A') AS SEGMENT_VALUE
,COUNT(DISTINCT CUSTOMER_ID) AS TOTAL_CUST
FROM VPM_DATA.PM_SVOC_CAR_OWNERSHIP
GROUP BY COALESCE((CASE WHEN FINAL_CROSS_PROVINCE_DRIVER_FLAG=1 OR FINAL_CROSS_PROVINCE_SHOPPER_FLAG=1 THEN 'YES' ELSE 'N/A' END),'N/A')
UNION ALL
SELECT 'GCS_CUST_SVOC_SEGMENT' AS TABLE_NAME
,'EUROPE_CAR_OWNERSHIPS' AS SEGMENT
,COALESCE((CASE WHEN EUROPE_CAR_OWNERSHIPS>= 1 THEN 'YES' ELSE 'N/A' END),'N/A') AS SEGMENT_VALUE
,COUNT(DISTINCT CUSTOMERID) AS TOTAL_CUST
FROM VPM_BACKUP.GCS_CUST_SVOC_SEGMENT
GROUP BY COALESCE((CASE WHEN EUROPE_CAR_OWNERSHIPS>= 1 THEN 'YES' ELSE 'N/A' END),'N/A')
UNION ALL
SELECT 'GCS_CUST_SVOC_SEGMENT' AS TABLE_NAME
,'CAR_BRAND' AS SEGMENT
,COALESCE(CAR_BRAND,'N/A') AS SEGMENT_VALUE
,COUNT(DISTINCT CUSTOMERID) AS TOTAL_CUST
FROM VPM_BACKUP.GCS_CUST_SVOC_SEGMENT
GROUP BY COALESCE(CAR_BRAND,'N/A')
UNION ALL
SELECT 'GCS_CUST_SVOC_SEGMENT' AS TABLE_NAME
,'PUBLIC_TRANSPORTATION_MODE' AS SEGMENT
,COALESCE(PUBLIC_TRANSPORTATION_MODE,'N/A') AS SEGMENT_VALUE
,COUNT(DISTINCT CUSTOMERID) AS TOTAL_CUST
FROM VPM_BACKUP.GCS_CUST_SVOC_SEGMENT
GROUP BY COALESCE(PUBLIC_TRANSPORTATION_MODE,'N/A')
UNION ALL
SELECT 'PM_SKUTAGGING_FOOD_CUST_SEGMENT' AS TABLE_NAME
,'FOOD_COMMERCIAL_SEGMENT' AS SEGMENT
,COALESCE(FOOD_COMMERCIAL_SEGMENT,'N/A') AS SEGMENT_VALUE
,COUNT(DISTINCT CUSTOMER_ID) AS TOTAL_CUST
FROM VPM_DATA.PM_SKUTAGGING_FOOD_CUST_SEGMENT
GROUP BY COALESCE(FOOD_COMMERCIAL_SEGMENT,'N/A')
UNION ALL
SELECT 'PM_COMMERCIAL_SEGMENT' AS TABLE_NAME
,'BEAUTY_COMMERCIAL_FLAG' AS SEGMENT
,COALESCE((CASE WHEN BEAUTY_COMMERCIAL_FLAG=1 THEN 'YES' ELSE 'N/A' END),'N/A') AS SEGMENT_VALUE
,COUNT(DISTINCT CUSTOMER_ID) AS TOTAL_CUST
FROM VPM_DATA.PM_COMMERCIAL_SEGMENT
GROUP BY COALESCE((CASE WHEN BEAUTY_COMMERCIAL_FLAG=1 THEN 'YES' ELSE 'N/A' END),'N/A')
UNION ALL
SELECT 'PM_COMMERCIAL_SEGMENT' AS TABLE_NAME
,'FASHION_COMMERCIAL_FLAG' AS SEGMENT
,COALESCE((CASE WHEN FASHION_COMMERCIAL_FLAG=1 THEN 'YES' ELSE 'N/A' END),'N/A') AS SEGMENT_VALUE
,COUNT(DISTINCT CUSTOMER_ID) AS TOTAL_CUST
FROM VPM_DATA.PM_COMMERCIAL_SEGMENT
GROUP BY COALESCE((CASE WHEN FASHION_COMMERCIAL_FLAG=1 THEN 'YES' ELSE 'N/A' END),'N/A')
UNION ALL
SELECT 'PM_COMMERCIAL_SEGMENT' AS TABLE_NAME
,'FOOD_COMMERCIAL_FLAG' AS SEGMENT
,COALESCE((CASE WHEN FOOD_COMMERCIAL_FLAG=1 THEN 'YES' ELSE 'N/A' END),'N/A') AS SEGMENT_VALUE
,COUNT(DISTINCT CUSTOMER_ID) AS TOTAL_CUST
FROM VPM_DATA.PM_COMMERCIAL_SEGMENT
GROUP BY COALESCE((CASE WHEN FOOD_COMMERCIAL_FLAG=1 THEN 'YES' ELSE 'N/A' END),'N/A')
UNION ALL
SELECT 'PM_COMMERCIAL_SEGMENT' AS TABLE_NAME
,'RESTAURANT_FLAG' AS SEGMENT
,COALESCE((CASE WHEN RESTAURANT_FLAG=1 THEN 'YES' ELSE 'N/A' END),'N/A') AS SEGMENT_VALUE
,COUNT(DISTINCT CUSTOMER_ID) AS TOTAL_CUST
FROM VPM_DATA.PM_COMMERCIAL_SEGMENT
GROUP BY COALESCE((CASE WHEN RESTAURANT_FLAG=1 THEN 'YES' ELSE 'N/A' END),'N/A')
UNION ALL
SELECT 'PM_COMMERCIAL_SEGMENT' AS TABLE_NAME
,'HOUSEHOLD_COMMERCIAL_FLAG' AS SEGMENT
,COALESCE((CASE WHEN HOUSEHOLD_COMMERCIAL_FLAG=1 THEN 'YES' ELSE 'N/A' END),'N/A') AS SEGMENT_VALUE
,COUNT(DISTINCT CUSTOMER_ID) AS TOTAL_CUST
FROM VPM_DATA.PM_COMMERCIAL_SEGMENT
GROUP BY COALESCE((CASE WHEN HOUSEHOLD_COMMERCIAL_FLAG=1 THEN 'YES' ELSE 'N/A' END),'N/A')
UNION ALL
SELECT 'PM_COMMERCIAL_SEGMENT' AS TABLE_NAME
,'HOUSEHOLD_COMMERCIAL_SEGMENT' AS SEGMENT
,COALESCE(HOUSEHOLD_COMMERCIAL_SEGMENT,'N/A') AS SEGMENT_VALUE
,COUNT(DISTINCT CUSTOMER_ID) AS TOTAL_CUST
FROM VPM_DATA.PM_COMMERCIAL_SEGMENT
GROUP BY COALESCE(HOUSEHOLD_COMMERCIAL_SEGMENT,'N/A')
UNION ALL
SELECT 'PM_COMMERCIAL_SEGMENT' AS TABLE_NAME
,'ELECTRONICS_COMMERCIAL_FLAG' AS SEGMENT
,COALESCE((CASE WHEN ELECTRONICS_COMMERCIAL_FLAG=1 THEN 'YES' ELSE 'N/A' END),'N/A') AS SEGMENT_VALUE
,COUNT(DISTINCT CUSTOMER_ID) AS TOTAL_CUST
FROM VPM_DATA.PM_COMMERCIAL_SEGMENT
GROUP BY COALESCE((CASE WHEN ELECTRONICS_COMMERCIAL_FLAG=1 THEN 'YES' ELSE 'N/A' END),'N/A')
UNION ALL
SELECT 'PM_COMMERCIAL_SEGMENT' AS TABLE_NAME
,'ELECTRONICS_COMMERCIAL_SEGMENT' AS SEGMENT
,COALESCE(ELECTRONICS_COMMERCIAL_SEGMENT,'N/A') AS SEGMENT_VALUE
,COUNT(DISTINCT CUSTOMER_ID) AS TOTAL_CUST
FROM VPM_DATA.PM_COMMERCIAL_SEGMENT
GROUP BY COALESCE(ELECTRONICS_COMMERCIAL_SEGMENT,'N/A')
UNION ALL
SELECT 'PM_COMMERCIAL_SEGMENT' AS TABLE_NAME
,'CG_COMMERCIAL_SEGMENT' AS SEGMENT
,COALESCE(CG_COMMERCIAL_SEGMENT,'N/A') AS SEGMENT_VALUE
,COUNT(DISTINCT CUSTOMER_ID) AS TOTAL_CUST
FROM VPM_DATA.PM_COMMERCIAL_SEGMENT
GROUP BY COALESCE(CG_COMMERCIAL_SEGMENT,'N/A')
UNION ALL
SELECT 'PM_COMMERCIAL_SEGMENT' AS TABLE_NAME
,'CG_COMMERCIAL_SUBSEGMENT' AS SEGMENT
,COALESCE(CG_COMMERCIAL_SUBSEGMENT,'N/A') AS SEGMENT_VALUE
,COUNT(DISTINCT CUSTOMER_ID) AS TOTAL_CUST
FROM VPM_DATA.PM_COMMERCIAL_SEGMENT
GROUP BY COALESCE(CG_COMMERCIAL_SUBSEGMENT,'N/A')
--UNION ALL
--SELECT 'PM_HOME_PRODUCT_CUST_SEGMENT' AS TABLE_NAME
--,'' AS SEGMENT
--, AS SEGMENT_VALUE
--,COUNT(DISTINCT CUSTOMER_ID) AS TOTAL_CUST
--FROM VPM_DATA.PM_HOME_PRODUCT_CUST_SEGMENT
--GROUP BY 