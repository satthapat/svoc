/*
	Create Home Transaction 2years
*/
--1. CREATE HOME 2YEAR TRANSACTION

SET tcp_keepalives_idle = 300;

INSERT INTO VPM_DATA.SALESSKU_LOG_TIME(task,start_time) VALUES ('E.HOME STEP 6.0 - HOME LIFESTYLE',NOW());

/*
SELECT * INTO TEMP TABLE TEMP_PM_HOME_PRODUCT_SALESSKU_2YR 
FROM PM_HOME_PRODUCT_SALESSKU_2YR;*/
DO $$
DECLARE START_DATE DATE := (SELECT VPM_DATA.GET_START_DATE(NOW()-INTERVAL '12 MONTH'));
		END_DATE DATE := (SELECT VPM_DATA.GET_END_DATE(NOW()));
BEGIN	
	DROP TABLE IF EXISTS VPM_DATA.PM_HOME_PRODUCT_SALESSKU_2YR_CDS;
	CREATE TABLE VPM_DATA.PM_HOME_PRODUCT_SALESSKU_2YR_CDS AS
	SELECT CUSTOMERID AS CUSTOMER_ID,A.BUID ,BU_NAME,CAST(TransactionDate AS DATE) AS TXN_DATE
		  ,SKUCODE::VARCHAR(50) AS SKU_CODE,A.Spending::NUMERIC(21,6) AS TXN_AMT
	      ,QTY,B.GROUPING_CAT,B.CATEGORY_FINAL,B.TYPE_FINAL,HOUSEMOVING_FLAG,HOME_PRODUCT_GROUP,PRODUCT_LIFESTYLE
	FROM Analysis_Data.SalesSKU_CDS2018 A INNER JOIN VPM_DATA.PM_HOME_PRODUCT_PRODUCT_MASTER B
	ON CASE WHEN A.BUID IN ('1','71','998') THEN '1' ELSE A.BUID END ::VARCHAR(4) = B.BUID ::VARCHAR(4) --CDS/RBS REVISED PRODUCT MASTER
	AND A.SKUCODE::BIGINT = B.SKU_CODE::BIGINT
	AND CAST(TransactionDate AS DATE) >= START_DATE
	AND PRODUCT_LIFESTYLE IS NOT NULL
	WHERE CUSTOMERID <> ''
		AND SKUCODE <> '' AND SKUCODE <> 'NAN'
	UNION ALL
	SELECT CUSTOMERID AS CUSTOMER_ID,CAST(A.BUID AS INT) AS BUID ,BU_NAME,CAST(TransactionDate AS DATE) AS TXN_DATE
		  ,SKUCODE::VARCHAR(50) AS SKU_CODE,A.Spending::NUMERIC(21,6) AS TXN_AMT
	      ,QTY,B.GROUPING_CAT,B.CATEGORY_FINAL,B.TYPE_FINAL,HOUSEMOVING_FLAG,HOME_PRODUCT_GROUP,PRODUCT_LIFESTYLE   
	FROM Analysis_Data.SalesSKU_CDS2019 A INNER JOIN VPM_DATA.PM_HOME_PRODUCT_PRODUCT_MASTER B
	ON CASE WHEN A.BUID IN ('1','71','998') THEN '1' ELSE A.BUID END ::VARCHAR(4) = B.BUID ::VARCHAR(4) --CDS/RBS REVISED PRODUCT MASTER
	AND A.SKUCODE::BIGINT = B.SKU_CODE::BIGINT AND PRODUCT_LIFESTYLE IS NOT NULL
	WHERE CUSTOMERID <> ''
		AND SKUCODE <> '' AND SKUCODE <> 'NAN'
	UNION ALL
	SELECT CUSTOMERID AS CUSTOMER_ID,CAST(A.BUID AS INT) AS BUID ,BU_NAME::VARCHAR(5),CAST(TransactionDate AS DATE) AS TXN_DATE
		  ,SKUCODE::VARCHAR(50) AS SKU_CODE,A.Spending::NUMERIC(21,6) AS TXN_AMT
	      ,QTY,B.GROUPING_CAT,B.CATEGORY_FINAL,B.TYPE_FINAL,HOUSEMOVING_FLAG,HOME_PRODUCT_GROUP,PRODUCT_LIFESTYLE   
	FROM Analysis_Data.SalesSKU_CDS2020 A INNER JOIN VPM_DATA.PM_HOME_PRODUCT_PRODUCT_MASTER B
	ON CASE WHEN A.BUID IN ('1','71','998') THEN '1' ELSE A.BUID END ::VARCHAR(4) = B.BUID ::VARCHAR(4) --CDS/RBS REVISED PRODUCT MASTER
	AND A.SKUCODE::BIGINT = B.SKU_CODE::BIGINT
	AND CAST(TransactionDate AS DATE) <= END_DATE
	AND PRODUCT_LIFESTYLE IS NOT NULL
	WHERE CUSTOMERID <> ''
		AND SKUCODE <> '' AND SKUCODE <> 'NAN';
END $$;


DO $$
DECLARE START_DATE DATE := (SELECT VPM_DATA.GET_START_DATE(NOW()-INTERVAL '12 MONTH'));
		END_DATE DATE := (SELECT VPM_DATA.GET_END_DATE(NOW()));
BEGIN	
	DROP TABLE IF EXISTS VPM_DATA.PM_HOME_PRODUCT_SALESSKU_2YR_RBS;
	CREATE TABLE VPM_DATA.PM_HOME_PRODUCT_SALESSKU_2YR_RBS AS
	SELECT CUSTOMERID AS CUSTOMER_ID,CAST(A.BUID AS INT) AS BUID ,BU_NAME,CAST(TransactionDate AS DATE) AS TXN_DATE
		  ,SKUCODE::VARCHAR(50) AS SKU_CODE,A.Spending::NUMERIC(21,6) AS TXN_AMT
	      ,QTY,B.GROUPING_CAT,B.CATEGORY_FINAL,B.TYPE_FINAL,HOUSEMOVING_FLAG,HOME_PRODUCT_GROUP,PRODUCT_LIFESTYLE   
	FROM Analysis_Data.SalesSKU_RBS2018 A INNER JOIN VPM_DATA.PM_HOME_PRODUCT_PRODUCT_MASTER B
	ON CASE WHEN A.BUID = '2' THEN '1' ELSE A.BUID END ::VARCHAR(4) = B.BUID ::VARCHAR(4) --CDS/RBS REVISED PRODUCT MASTER
	AND A.SKUCODE::BIGINT = B.SKU_CODE::BIGINT
	AND CAST(TransactionDate AS DATE) >= START_DATE
	AND PRODUCT_LIFESTYLE IS NOT NULL
	WHERE CUSTOMERID <> ''
		AND SKUCODE <> '' AND SKUCODE <> 'NAN'
	UNION ALL
	SELECT CUSTOMERID AS CUSTOMER_ID,CAST(A.BUID AS INT) AS BUID ,BU_NAME,CAST(TransactionDate AS DATE) AS TXN_DATE
		  ,SKUCODE::VARCHAR(50) AS SKU_CODE,A.Spending::NUMERIC(21,6) AS TXN_AMT
	      ,QTY,B.GROUPING_CAT,B.CATEGORY_FINAL,B.TYPE_FINAL,HOUSEMOVING_FLAG,HOME_PRODUCT_GROUP,PRODUCT_LIFESTYLE   
	FROM Analysis_Data.SalesSKU_RBS2019 A INNER JOIN VPM_DATA.PM_HOME_PRODUCT_PRODUCT_MASTER B
	ON CASE WHEN A.BUID = '2' THEN '1' ELSE A.BUID END ::VARCHAR(4) = B.BUID ::VARCHAR(4) --CDS/RBS REVISED PRODUCT MASTER
	AND A.SKUCODE::BIGINT = B.SKU_CODE::BIGINT AND PRODUCT_LIFESTYLE IS NOT NULL
	WHERE CUSTOMERID <> ''
		AND SKUCODE <> '' AND SKUCODE <> 'NAN'
	UNION ALL
	SELECT CUSTOMERID AS CUSTOMER_ID,CAST(A.BUID AS INT) AS BUID ,BU_NAME::VARCHAR(5),CAST(TransactionDate AS DATE) AS TXN_DATE
		  ,SKUCODE::VARCHAR(50) AS SKU_CODE,A.Spending::NUMERIC(21,6) AS TXN_AMT
	      ,QTY,B.GROUPING_CAT,B.CATEGORY_FINAL,B.TYPE_FINAL,HOUSEMOVING_FLAG,HOME_PRODUCT_GROUP,PRODUCT_LIFESTYLE   
	FROM Analysis_Data.SalesSKU_RBS2020 A INNER JOIN VPM_DATA.PM_HOME_PRODUCT_PRODUCT_MASTER B
	ON CASE WHEN A.BUID = '2' THEN '1' ELSE A.BUID END ::VARCHAR(4) = B.BUID ::VARCHAR(4) --CDS/RBS REVISED PRODUCT MASTER
	AND A.SKUCODE::BIGINT = B.SKU_CODE::BIGINT
	AND CAST(TransactionDate AS DATE) <= END_DATE
	AND PRODUCT_LIFESTYLE IS NOT NULL
	WHERE CUSTOMERID <> ''
		AND SKUCODE <> '' AND SKUCODE <> 'NAN';
END $$;

DO $$
DECLARE START_DATE DATE := (SELECT VPM_DATA.GET_START_DATE(NOW()-INTERVAL '12 MONTH'));
		END_DATE DATE := (SELECT VPM_DATA.GET_END_DATE(NOW()));
BEGIN	
	DROP TABLE IF EXISTS VPM_DATA.PM_HOME_PRODUCT_SALESSKU_2YR_B2S;
	CREATE TABLE VPM_DATA.PM_HOME_PRODUCT_SALESSKU_2YR_B2S AS
	SELECT CUSTOMERID AS CUSTOMER_ID,CAST(A.BUID AS INT) AS BUID ,BU_NAME,CAST(TransactionDate AS DATE) AS TXN_DATE
		  ,SKUCODE::VARCHAR(50) AS SKU_CODE,A.Spending::NUMERIC(21,6) AS TXN_AMT
	      ,QTY,B.GROUPING_CAT,B.CATEGORY_FINAL,B.TYPE_FINAL,HOUSEMOVING_FLAG,HOME_PRODUCT_GROUP,PRODUCT_LIFESTYLE  
	FROM Analysis_Data.SalesSKU_B2S2018 A INNER JOIN VPM_DATA.PM_HOME_PRODUCT_PRODUCT_MASTER B
	ON A.BUID::VARCHAR(4) = B.BUID::VARCHAR(4) AND A.SKUCODE::BIGINT = B.SKU_CODE::BIGINT
	AND CAST(TransactionDate AS DATE) >= START_DATE
	AND PRODUCT_LIFESTYLE IS NOT NULL
	WHERE CUSTOMERID <> ''
		AND SKUCODE <> '' AND SKUCODE <> 'NAN'
	UNION ALL
	SELECT CUSTOMERID AS CUSTOMER_ID,CAST(A.BUID AS INT) AS BUID ,BU_NAME,CAST(TransactionDate AS DATE) AS TXN_DATE
		  ,SKUCODE::VARCHAR(50) AS SKU_CODE,A.Spending::NUMERIC(21,6) AS TXN_AMT
	      ,QTY,B.GROUPING_CAT,B.CATEGORY_FINAL,B.TYPE_FINAL,HOUSEMOVING_FLAG,HOME_PRODUCT_GROUP,PRODUCT_LIFESTYLE   
	FROM Analysis_Data.SalesSKU_B2S2019 A INNER JOIN VPM_DATA.PM_HOME_PRODUCT_PRODUCT_MASTER B
	ON A.BUID::VARCHAR(4) = B.BUID::VARCHAR(4) AND A.SKUCODE::BIGINT = B.SKU_CODE::BIGINT AND PRODUCT_LIFESTYLE IS NOT NULL
	WHERE CUSTOMERID <> ''
		AND SKUCODE <> '' AND SKUCODE <> 'NAN'
	UNION ALL
	SELECT CUSTOMERID AS CUSTOMER_ID,CAST(A.BUID AS INT) AS BUID ,BU_NAME::VARCHAR(5),CAST(TransactionDate AS DATE) AS TXN_DATE
		  ,SKUCODE::VARCHAR(50) AS SKU_CODE,A.Spending::NUMERIC(21,6) AS TXN_AMT
	      ,QTY,B.GROUPING_CAT,B.CATEGORY_FINAL,B.TYPE_FINAL,HOUSEMOVING_FLAG,HOME_PRODUCT_GROUP,PRODUCT_LIFESTYLE   
	FROM Analysis_Data.SalesSKU_B2S2020 A INNER JOIN VPM_DATA.PM_HOME_PRODUCT_PRODUCT_MASTER B
	ON A.BUID::VARCHAR(4) = B.BUID::VARCHAR(4) AND A.SKUCODE::BIGINT = B.SKU_CODE::BIGINT
	AND CAST(TransactionDate AS DATE) <= END_DATE
	AND PRODUCT_LIFESTYLE IS NOT NULL
	WHERE CUSTOMERID <> ''
		AND SKUCODE <> '' AND SKUCODE <> 'NAN';
END $$;

DO $$
DECLARE START_DATE DATE := (SELECT VPM_DATA.GET_START_DATE(NOW()-INTERVAL '12 MONTH'));
		END_DATE DATE := (SELECT VPM_DATA.GET_END_DATE(NOW()));
BEGIN	
	DROP TABLE IF EXISTS VPM_DATA.PM_HOME_PRODUCT_SALESSKU_2YR_CMG;
	CREATE TABLE VPM_DATA.PM_HOME_PRODUCT_SALESSKU_2YR_CMG AS
	SELECT CUSTOMERID AS CUSTOMER_ID,CAST(A.BUID AS INT) AS BUID ,BU_NAME,CAST(TransactionDate AS DATE) AS TXN_DATE
		  ,SKUCODE::VARCHAR(50) AS SKU_CODE,A.Spending::NUMERIC(21,6) AS TXN_AMT
	      ,QTY,B.GROUPING_CAT,B.CATEGORY_FINAL,B.TYPE_FINAL,HOUSEMOVING_FLAG,HOME_PRODUCT_GROUP,PRODUCT_LIFESTYLE  
	FROM Analysis_Data.SalesSKU_CMG2018_SBL A INNER JOIN VPM_DATA.PM_HOME_PRODUCT_PRODUCT_MASTER B
	ON (CASE WHEN A.BUID IN ('143','144') THEN '143' ELSE A.BUID END)::VARCHAR(4) = B.BUID::VARCHAR(4) AND A.SKUCODE::BIGINT = B.SKU_CODE::BIGINT
	AND CAST(TransactionDate AS DATE) >= START_DATE
	AND PRODUCT_LIFESTYLE IS NOT NULL
	AND LENGTH(SKU_CODE) <= 14
	AND SKU_CODE NOT LIKE '0000%'
	AND SKUCODE NOT LIKE '0000%'
	WHERE CUSTOMERID <> ''
		AND SKUCODE <> '' AND SKUCODE <> 'NAN'
	UNION ALL
	SELECT CUSTOMERID AS CUSTOMER_ID,CAST(A.BUID AS INT) AS BUID ,BU_NAME,CAST(TransactionDate AS DATE) AS TXN_DATE
		  ,SKUCODE::VARCHAR(50) AS SKU_CODE,A.Spending::NUMERIC(21,6) AS TXN_AMT
	      ,QTY,B.GROUPING_CAT,B.CATEGORY_FINAL,B.TYPE_FINAL,HOUSEMOVING_FLAG,HOME_PRODUCT_GROUP,PRODUCT_LIFESTYLE   
	FROM Analysis_Data.SalesSKU_CMG2019_SBL A INNER JOIN VPM_DATA.PM_HOME_PRODUCT_PRODUCT_MASTER B
	ON (CASE WHEN A.BUID IN ('143','144') THEN '143' ELSE A.BUID END)::VARCHAR(4) = B.BUID::VARCHAR(4) AND A.SKUCODE::BIGINT = B.SKU_CODE::BIGINT
	AND LENGTH(SKU_CODE) <= 14
	AND SKU_CODE NOT LIKE '0000%'
	AND SKUCODE NOT LIKE '0000%'
	AND PRODUCT_LIFESTYLE IS NOT NULL
	WHERE CUSTOMERID <> ''
		AND SKUCODE <> '' AND SKUCODE <> 'NAN'
	UNION ALL
	SELECT CUSTOMERID AS CUSTOMER_ID,CAST(A.BUID AS INT) AS BUID ,BU_NAME::VARCHAR(5),CAST(TransactionDate AS DATE) AS TXN_DATE
		  ,SKUCODE::VARCHAR(50) AS SKU_CODE,A.Spending::NUMERIC(21,6) AS TXN_AMT
	      ,QTY,B.GROUPING_CAT,B.CATEGORY_FINAL,B.TYPE_FINAL,HOUSEMOVING_FLAG,HOME_PRODUCT_GROUP,PRODUCT_LIFESTYLE   
	FROM Analysis_Data.SalesSKU_CMG2020_SBL A INNER JOIN VPM_DATA.PM_HOME_PRODUCT_PRODUCT_MASTER B
	ON (CASE WHEN A.BUID IN ('143','144') THEN '143' ELSE A.BUID END)::VARCHAR(4) = B.BUID::VARCHAR(4) AND A.SKUCODE::BIGINT = B.SKU_CODE::BIGINT
	AND CAST(TransactionDate AS DATE) <= END_DATE
	AND LENGTH(SKU_CODE) <= 14
	AND B.SKU_CODE::VARCHAR(20) NOT LIKE '0000%'
	AND A.SKUCODE::VARCHAR(20) NOT LIKE '0000%'
	AND PRODUCT_LIFESTYLE IS NOT NULL
	WHERE CUSTOMERID <> ''
		AND SKUCODE <> '' AND SKUCODE <> 'NAN';
END $$;

DO $$
DECLARE START_DATE DATE := (SELECT VPM_DATA.GET_START_DATE(NOW()-INTERVAL '12 MONTH'));
		END_DATE DATE := (SELECT VPM_DATA.GET_END_DATE(NOW()));
BEGIN	
	DROP TABLE IF EXISTS VPM_DATA.PM_HOME_PRODUCT_SALESSKU_2YR_PWB;
	CREATE TABLE VPM_DATA.PM_HOME_PRODUCT_SALESSKU_2YR_PWB AS
	SELECT CUSTOMERID AS CUSTOMER_ID,CAST(A.BUID AS INT) AS BUID ,BU_NAME,CAST(TransactionDate AS DATE) AS TXN_DATE
		  ,SKUCODE::VARCHAR(50) AS SKU_CODE,A.Spending::NUMERIC(21,6) AS TXN_AMT
	      ,QTY,B.GROUPING_CAT,B.CATEGORY_FINAL,B.TYPE_FINAL,HOUSEMOVING_FLAG,HOME_PRODUCT_GROUP,PRODUCT_LIFESTYLE   
	FROM Analysis_Data.SalesSKU_PWB2018 A INNER JOIN VPM_DATA.PM_HOME_PRODUCT_PRODUCT_MASTER B
	ON A.BUID::VARCHAR(4) = B.BUID::VARCHAR(4) AND A.SKUCODE::BIGINT = B.SKU_CODE::BIGINT
	AND CAST(TransactionDate AS DATE) >= START_DATE
	AND PRODUCT_LIFESTYLE IS NOT NULL
	WHERE CUSTOMERID <> ''
		AND SKUCODE <> '' AND SKUCODE <> 'NAN'
	UNION ALL
	SELECT CUSTOMERID AS CUSTOMER_ID,CAST(A.BUID AS INT) AS BUID ,BU_NAME,CAST(TransactionDate AS DATE) AS TXN_DATE
		  ,SKUCODE::VARCHAR(50) AS SKU_CODE,A.Spending::NUMERIC(21,6) AS TXN_AMT
	      ,QTY,B.GROUPING_CAT,B.CATEGORY_FINAL,B.TYPE_FINAL,HOUSEMOVING_FLAG,HOME_PRODUCT_GROUP,PRODUCT_LIFESTYLE   
	FROM Analysis_Data.SalesSKU_PWB2019 A INNER JOIN VPM_DATA.PM_HOME_PRODUCT_PRODUCT_MASTER B
	ON A.BUID::VARCHAR(4) = B.BUID::VARCHAR(4) AND A.SKUCODE::BIGINT = B.SKU_CODE::BIGINT
	AND PRODUCT_LIFESTYLE IS NOT NULL
	WHERE CUSTOMERID <> ''
		AND SKUCODE <> '' AND SKUCODE <> 'NAN'
	UNION ALL
	SELECT CUSTOMERID AS CUSTOMER_ID,CAST(A.BUID AS INT) AS BUID ,BU_NAME::VARCHAR(5),CAST(TransactionDate AS DATE) AS TXN_DATE
		  ,SKUCODE::VARCHAR(50) AS SKU_CODE,A.Spending::NUMERIC(21,6) AS TXN_AMT
	      ,QTY,B.GROUPING_CAT,B.CATEGORY_FINAL,B.TYPE_FINAL,HOUSEMOVING_FLAG,HOME_PRODUCT_GROUP,PRODUCT_LIFESTYLE   
	FROM Analysis_Data.SalesSKU_PWB2020 A INNER JOIN VPM_DATA.PM_HOME_PRODUCT_PRODUCT_MASTER B
	ON A.BUID::VARCHAR(4) = B.BUID::VARCHAR(4) AND A.SKUCODE::BIGINT = B.SKU_CODE::BIGINT
	AND CAST(TransactionDate AS DATE) <= END_DATE
	AND PRODUCT_LIFESTYLE IS NOT NULL
	WHERE CUSTOMERID <> ''
		AND SKUCODE <> '' AND SKUCODE <> 'NAN';
END $$;

DO $$
DECLARE START_DATE DATE := (SELECT VPM_DATA.GET_START_DATE(NOW()-INTERVAL '12 MONTH'));
		END_DATE DATE := (SELECT VPM_DATA.GET_END_DATE(NOW()));
BEGIN	
	DROP TABLE IF EXISTS VPM_DATA.PM_HOME_PRODUCT_SALESSKU_2YR_HWS;
	CREATE TABLE VPM_DATA.PM_HOME_PRODUCT_SALESSKU_2YR_HWS AS
	SELECT CUSTOMERID AS CUSTOMER_ID,CAST(A.BUID AS INT) AS BUID ,BU_NAME,CAST(TransactionDate AS DATE) AS TXN_DATE
		  ,SKUCODE::VARCHAR(50) AS SKU_CODE,A.Spending::NUMERIC(21,6) AS TXN_AMT
	      ,QTY,B.GROUPING_CAT,B.CATEGORY_FINAL,B.TYPE_FINAL,HOUSEMOVING_FLAG,HOME_PRODUCT_GROUP,PRODUCT_LIFESTYLE   
	FROM Analysis_Data.SalesSKU_HWS2018 A INNER JOIN VPM_DATA.PM_HOME_PRODUCT_PRODUCT_MASTER B
	ON A.BUID::VARCHAR(4) = B.BUID::VARCHAR(4) AND A.SKUCODE::BIGINT = B.SKU_CODE::BIGINT
	AND CAST(TransactionDate AS DATE) >= START_DATE
	AND PRODUCT_LIFESTYLE IS NOT NULL
	WHERE CUSTOMERID <> ''
		AND SKUCODE <> '' AND SKUCODE <> 'NAN'
	UNION ALL
	SELECT CUSTOMERID AS CUSTOMER_ID,CAST(A.BUID AS INT) AS BUID ,BU_NAME,CAST(TransactionDate AS DATE) AS TXN_DATE
		  ,SKUCODE::VARCHAR(50) AS SKU_CODE,A.Spending::NUMERIC(21,6) AS TXN_AMT
	      ,QTY,B.GROUPING_CAT,B.CATEGORY_FINAL,B.TYPE_FINAL,HOUSEMOVING_FLAG,HOME_PRODUCT_GROUP,PRODUCT_LIFESTYLE   
	FROM Analysis_Data.SalesSKU_HWS2019 A INNER JOIN VPM_DATA.PM_HOME_PRODUCT_PRODUCT_MASTER B
	ON A.BUID::VARCHAR(4) = B.BUID::VARCHAR(4) AND A.SKUCODE::BIGINT = B.SKU_CODE::BIGINT
	AND PRODUCT_LIFESTYLE IS NOT NULL
	WHERE CUSTOMERID <> ''
		AND SKUCODE <> '' AND SKUCODE <> 'NAN'
		AND SKUCODE <> 'NAN'
	UNION ALL
	SELECT CUSTOMERID AS CUSTOMER_ID,CAST(A.BUID AS INT) AS BUID ,BU_NAME::VARCHAR(5),CAST(TransactionDate AS DATE) AS TXN_DATE
		  ,SKUCODE::VARCHAR(50) AS SKU_CODE,A.Spending::NUMERIC(21,6) AS TXN_AMT
	      ,QTY,B.GROUPING_CAT,B.CATEGORY_FINAL,B.TYPE_FINAL,HOUSEMOVING_FLAG,HOME_PRODUCT_GROUP,PRODUCT_LIFESTYLE   
	FROM Analysis_Data.SalesSKU_HWS2020 A INNER JOIN VPM_DATA.PM_HOME_PRODUCT_PRODUCT_MASTER B
	ON A.BUID::VARCHAR(4) = B.BUID::VARCHAR(4) AND A.SKUCODE::BIGINT = B.SKU_CODE::BIGINT
	AND CAST(TransactionDate AS DATE) <= END_DATE
	AND PRODUCT_LIFESTYLE IS NOT NULL
	WHERE CUSTOMERID <> ''
		AND SKUCODE <> '' AND SKUCODE <> 'NAN';
END $$;

DO $$
DECLARE START_DATE DATE := (SELECT VPM_DATA.GET_START_DATE(NOW()-INTERVAL '12 MONTH'));
		END_DATE DATE := (SELECT VPM_DATA.GET_END_DATE(NOW()));
BEGIN	
	DROP TABLE IF EXISTS VPM_DATA.PM_HOME_PRODUCT_SALESSKU_2YR_TWD;
	CREATE TABLE VPM_DATA.PM_HOME_PRODUCT_SALESSKU_2YR_TWD AS
	SELECT CUSTOMERID AS CUSTOMER_ID,CAST(A.BUID AS INT) AS BUID ,BU_NAME,CAST(TransactionDate AS DATE) AS TXN_DATE
		  ,SKUCODE::VARCHAR(50) AS SKU_CODE,A.Spending::NUMERIC(21,6) AS TXN_AMT
	      ,QTY,B.GROUPING_CAT,B.CATEGORY_FINAL,B.TYPE_FINAL,HOUSEMOVING_FLAG,HOME_PRODUCT_GROUP,PRODUCT_LIFESTYLE   
	FROM Analysis_Data.SalesSKU_TWD2018 A INNER JOIN VPM_DATA.PM_HOME_PRODUCT_PRODUCT_MASTER B
	ON	B.BU_NAME = 'HWS' AND A.SKUCODE::BIGINT = B.SKU_CODE::BIGINT
	AND CAST(TransactionDate AS DATE) >= START_DATE
	AND PRODUCT_LIFESTYLE IS NOT NULL
	WHERE CUSTOMERID <> ''
		AND SKUCODE <> '' AND SKUCODE <> 'NAN'
		AND SKUCODE <> 'NAN'
	UNION ALL
	SELECT CUSTOMERID AS CUSTOMER_ID,CAST(A.BUID AS INT) AS BUID ,BU_NAME,CAST(TransactionDate AS DATE) AS TXN_DATE
		  ,SKUCODE::VARCHAR(50) AS SKU_CODE,A.Spending::NUMERIC(21,6) AS TXN_AMT
	      ,QTY,B.GROUPING_CAT,B.CATEGORY_FINAL,B.TYPE_FINAL,HOUSEMOVING_FLAG,HOME_PRODUCT_GROUP,PRODUCT_LIFESTYLE   
	FROM Analysis_Data.SalesSKU_TWD2019 A INNER JOIN VPM_DATA.PM_HOME_PRODUCT_PRODUCT_MASTER B
	ON	B.BU_NAME = 'HWS' AND A.SKUCODE::BIGINT = B.SKU_CODE::BIGINT
	AND PRODUCT_LIFESTYLE IS NOT NULL
	WHERE CUSTOMERID <> ''
		AND SKUCODE <> '' AND SKUCODE <> 'NAN'
		AND SKUCODE <> 'NAN'
	UNION ALL
	SELECT CUSTOMERID AS CUSTOMER_ID,CAST(A.BUID AS INT) AS BUID ,BU_NAME::VARCHAR(5),CAST(TransactionDate AS DATE) AS TXN_DATE
		  ,SKUCODE::VARCHAR(50) AS SKU_CODE,A.Spending::NUMERIC(21,6) AS TXN_AMT
	      ,QTY,B.GROUPING_CAT,B.CATEGORY_FINAL,B.TYPE_FINAL,HOUSEMOVING_FLAG,HOME_PRODUCT_GROUP,PRODUCT_LIFESTYLE   
	FROM Analysis_Data.SalesSKU_TWD2020 A INNER JOIN VPM_DATA.PM_HOME_PRODUCT_PRODUCT_MASTER B
	ON	B.BU_NAME = 'HWS' AND A.SKUCODE::BIGINT = B.SKU_CODE::BIGINT
	AND CAST(TransactionDate AS DATE) <= END_DATE
	AND PRODUCT_LIFESTYLE IS NOT NULL
	WHERE CUSTOMERID <> ''
		AND SKUCODE <> '' AND SKUCODE <> 'NAN'
		AND SKUCODE <> 'NAN';
END $$;

DROP TABLE IF EXISTS VPM_DATA.PM_HOME_PRODUCT_SALESSKU_2YR_TOPS_STAGING2018;
DO $$
DECLARE START_DATE DATE := (SELECT VPM_DATA.GET_START_DATE(NOW()-INTERVAL '12 MONTH'));
		END_DATE DATE := (SELECT VPM_DATA.GET_END_DATE(NOW()));
BEGIN		
	CREATE TABLE VPM_DATA.PM_HOME_PRODUCT_SALESSKU_2YR_TOPS_STAGING2018 AS
	SELECT CUSTOMERID AS CUSTOMER_ID,CAST(A.BUID AS INT) AS BUID ,BU_NAME,CAST(TransactionDate AS DATE) AS TXN_DATE
		  ,CASE WHEN length(SKUCode) = 12 THEN '0'||SKUCode
					WHEN length(SKUCode) = 11 THEN '00'||SKUCode
					WHEN length(SKUCode) = 10 THEN '000'||SKUCode
					WHEN length(SKUCode) = 9 THEN '0000'||SKUCode
					WHEN length(SKUCode) = 8 THEN '00000'||SKUCode
					WHEN length(SKUCode) = 7 THEN '000000'||SKUCode
					WHEN length(SKUCode) = 6 THEN '0000000'||SKUCode
					WHEN length(SKUCode) = 5 THEN '00000000'||SKUCode
					WHEN length(SKUCode) = 4 THEN '000000000'||SKUCode
					WHEN length(SKUCode) = 3 THEN '0000000000'||SKUCode
				ELSE SKUCode END AS SKU_CODE,A.Spending::NUMERIC(21,6) AS TXN_AMT
	      ,QTY,B.GROUPING_CAT,B.CATEGORY_FINAL,B.TYPE_FINAL,HOUSEMOVING_FLAG,HOME_PRODUCT_GROUP,PRODUCT_LIFESTYLE    
	FROM Analysis_Data.SalesSKU_TOPS2018 A INNER JOIN VPM_DATA.PM_HOME_PRODUCT_PRODUCT_MASTER B
	ON A.BUID::VARCHAR(4) = B.BUID::VARCHAR(4) AND A.SKUCODE::BIGINT = B.SKU_CODE::BIGINT
	AND CAST(TransactionDate AS DATE) >= START_DATE
	AND PRODUCT_LIFESTYLE IS NOT NULL
	WHERE CUSTOMERID <> ''
		AND SKU_CODE <> ''
		AND SKUCODE <> 'NAN';
END $$;

DROP TABLE IF EXISTS VPM_DATA.PM_HOME_PRODUCT_SALESSKU_2YR_TOPS_STAGING2019;
DO $$
DECLARE START_DATE DATE := (SELECT VPM_DATA.GET_START_DATE(NOW()-INTERVAL '12 MONTH'));
		END_DATE DATE := (SELECT VPM_DATA.GET_END_DATE(NOW()));
BEGIN		
	CREATE TABLE VPM_DATA.PM_HOME_PRODUCT_SALESSKU_2YR_TOPS_STAGING2019 AS
	SELECT CUSTOMERID AS CUSTOMER_ID,CAST(A.BUID AS INT) AS BUID ,BU_NAME,CAST(TransactionDate AS DATE) AS TXN_DATE
		  ,CASE WHEN length(SKUCode) = 12 THEN '0'||SKUCode
					WHEN length(SKUCode) = 11 THEN '00'||SKUCode
					WHEN length(SKUCode) = 10 THEN '000'||SKUCode
					WHEN length(SKUCode) = 9 THEN '0000'||SKUCode
					WHEN length(SKUCode) = 8 THEN '00000'||SKUCode
					WHEN length(SKUCode) = 7 THEN '000000'||SKUCode
					WHEN length(SKUCode) = 6 THEN '0000000'||SKUCode
					WHEN length(SKUCode) = 5 THEN '00000000'||SKUCode
					WHEN length(SKUCode) = 4 THEN '000000000'||SKUCode
					WHEN length(SKUCode) = 3 THEN '0000000000'||SKUCode
				ELSE SKUCode END AS SKU_CODE,A.Spending::NUMERIC(21,6) AS TXN_AMT
	      ,QTY,B.GROUPING_CAT,B.CATEGORY_FINAL,B.TYPE_FINAL,HOUSEMOVING_FLAG,HOME_PRODUCT_GROUP,PRODUCT_LIFESTYLE    
	FROM Analysis_Data.SalesSKU_TOPS2019 A INNER JOIN VPM_DATA.PM_HOME_PRODUCT_PRODUCT_MASTER B
	ON A.BUID::VARCHAR(4) = B.BUID::VARCHAR(4) AND A.SKUCODE::BIGINT = B.SKU_CODE::BIGINT
	AND PRODUCT_LIFESTYLE IS NOT NULL
	WHERE CUSTOMERID <> ''
		AND SKU_CODE <> ''
		AND SKUCODE <> 'NAN';
END $$;


DROP TABLE IF EXISTS VPM_DATA.PM_HOME_PRODUCT_SALESSKU_2YR_TOPS_STAGING2020;
DO $$
DECLARE START_DATE DATE := (SELECT VPM_DATA.GET_START_DATE(NOW()-INTERVAL '12 MONTH'));
		END_DATE DATE := (SELECT VPM_DATA.GET_END_DATE(NOW()));
BEGIN		
	CREATE TABLE VPM_DATA.PM_HOME_PRODUCT_SALESSKU_2YR_TOPS_STAGING2020 AS
	SELECT CUSTOMERID AS CUSTOMER_ID,CAST(A.BUID AS INT) AS BUID ,BU_NAME::VARCHAR(5),CAST(TransactionDate AS DATE) AS TXN_DATE
		  ,CASE WHEN length(SKUCode) = 12 THEN '0'||SKUCode
					WHEN length(SKUCode) = 11 THEN '00'||SKUCode
					WHEN length(SKUCode) = 10 THEN '000'||SKUCode
					WHEN length(SKUCode) = 9 THEN '0000'||SKUCode
					WHEN length(SKUCode) = 8 THEN '00000'||SKUCode
					WHEN length(SKUCode) = 7 THEN '000000'||SKUCode
					WHEN length(SKUCode) = 6 THEN '0000000'||SKUCode
					WHEN length(SKUCode) = 5 THEN '00000000'||SKUCode
					WHEN length(SKUCode) = 4 THEN '000000000'||SKUCode
					WHEN length(SKUCode) = 3 THEN '0000000000'||SKUCode
				ELSE SKUCode END AS SKU_CODE,A.Spending::NUMERIC(21,6) AS TXN_AMT
	      ,QTY,B.GROUPING_CAT,B.CATEGORY_FINAL,B.TYPE_FINAL,HOUSEMOVING_FLAG,HOME_PRODUCT_GROUP,PRODUCT_LIFESTYLE    
	FROM Analysis_Data.SalesSKU_TOPS2020 A INNER JOIN VPM_DATA.PM_HOME_PRODUCT_PRODUCT_MASTER B
	ON A.BUID::VARCHAR(4) = B.BUID::VARCHAR(4) AND A.SKUCODE::BIGINT = B.SKU_CODE::BIGINT
	AND CAST(TransactionDate AS DATE) <= END_DATE
	AND PRODUCT_LIFESTYLE IS NOT NULL
	WHERE CUSTOMERID <> ''
		AND SKU_CODE <> ''
		AND SKUCODE <> 'NAN';
END $$;

DROP TABLE IF EXISTS VPM_DATA.PM_HOME_PRODUCT_SALESSKU_2YR_TOPS;
CREATE TABLE VPM_DATA.PM_HOME_PRODUCT_SALESSKU_2YR_TOPS AS
SELECT * FROM VPM_DATA.PM_HOME_PRODUCT_SALESSKU_2YR_TOPS_STAGING2018
UNION ALL SELECT * FROM VPM_DATA.PM_HOME_PRODUCT_SALESSKU_2YR_TOPS_STAGING2019
UNION ALL SELECT * FROM VPM_DATA.PM_HOME_PRODUCT_SALESSKU_2YR_TOPS_STAGING2020;

DROP TABLE IF EXISTS VPM_DATA.PM_HOME_PRODUCT_SALESSKU_2YR;
CREATE TABLE VPM_DATA.PM_HOME_PRODUCT_SALESSKU_2YR AS
SELECT * FROM VPM_DATA.PM_HOME_PRODUCT_SALESSKU_2YR_CDS
UNION ALL SELECT * FROM VPM_DATA.PM_HOME_PRODUCT_SALESSKU_2YR_RBS
UNION ALL SELECT * FROM VPM_DATA.PM_HOME_PRODUCT_SALESSKU_2YR_B2S
UNION ALL SELECT * FROM VPM_DATA.PM_HOME_PRODUCT_SALESSKU_2YR_CMG
UNION ALL SELECT * FROM VPM_DATA.PM_HOME_PRODUCT_SALESSKU_2YR_PWB
UNION ALL SELECT * FROM VPM_DATA.PM_HOME_PRODUCT_SALESSKU_2YR_HWS
UNION ALL SELECT * FROM VPM_DATA.PM_HOME_PRODUCT_SALESSKU_2YR_TWD
UNION ALL SELECT * FROM VPM_DATA.PM_HOME_PRODUCT_SALESSKU_2YR_TOPS;


DROP TABLE IF EXISTS VPM_DATA.PM_HOME_PRODUCT_SALE2YR_PURCHASED;
SELECT	 CUSTOMER_ID
		,HOME_PRODUCT_GROUP
		,CATEGORY_FINAL
		,TYPE_FINAL
		,PRODUCT_LIFESTYLE
		,EXTRACT(YEAR FROM TXN_DATE) AS TXN_YEAR
		,EXTRACT(MONTH FROM TXN_DATE) AS TXN_MONTH
		,MIN(TXN_DATE) AS MIN_DATE
		,MAX(TXN_DATE) AS MAX_DATE
		,COUNT(DISTINCT TXN_DATE) AS VISIT_DAY
		,SUM(QTY) AS SUM_QTY
		,SUM(TXN_AMT) AS TTL_AMT
		--,DATEDIFF(MONTH,TXN_DATE,@MAX_DATE) AS RECENCY_MONTH
INTO VPM_DATA.PM_HOME_PRODUCT_SALE2YR_PURCHASED	
FROM VPM_DATA.PM_HOME_PRODUCT_SALESSKU_2YR
GROUP BY CUSTOMER_ID
		,HOME_PRODUCT_GROUP
		,CATEGORY_FINAL
		,TYPE_FINAL
		,PRODUCT_LIFESTYLE
		,EXTRACT(YEAR FROM TXN_DATE)
		,EXTRACT(MONTH FROM TXN_DATE)
;

/**********************************************************************************************************/

DO $$ --5m 10s
DECLARE END_DATE DATE := (SELECT vpm_data.get_end_date(NOW()));
BEGIN
	DROP TABLE IF EXISTS VPM_DATA.CUST_PURCHASED_MONTH;
	CREATE TABLE VPM_DATA.CUST_PURCHASED_MONTH AS
	SELECT	 CUSTOMER_ID,PRODUCT_LIFESTYLE
			,COUNT(DISTINCT TXN_DATE) AS VISIT_DAY
			,COUNT(DISTINCT EXTRACT(YEAR FROM TXN_DATE)||'-'||EXTRACT(MONTH FROM TXN_DATE)) AS CNT_MONTH
			,MAX(TXN_DATE) AS MAX_DATE
			,vpm_data.datediff('day',MAX(TXN_DATE),END_DATE) AS RECENCY
			,SUM(TXN_AMT) AS TTL_AMT
			,CAST(NULL AS FLOAT) AS LIFESTYLE_GARDENING_SCORE
			,CAST(NULL AS FLOAT) AS LIFESTYLE_IMPROVEMENT_SCORE
			,CAST(NULL AS FLOAT) AS LIFESTYLE_DECOR_SCORE
	FROM VPM_DATA.PM_HOME_PRODUCT_SALESSKU_2YR
	GROUP BY CUSTOMER_ID,PRODUCT_LIFESTYLE;
END $$;

/**********************************************************************************************************/

--3. HOME LIFESTYLE
--1. HOME GARDENING
DO $$
DECLARE RECENCY_D SMALLINT := (SELECT DISTINCT PERCENTILE_DISC(0.95) 
							WITHIN GROUP (ORDER BY RECENCY DESC NULLS LAST)
						FROM VPM_DATA.CUST_PURCHASED_MONTH
						WHERE PRODUCT_LIFESTYLE = 'HOME GARDENING'
						AND RECENCY <= 365);	
	FREQUENCY_D SMALLINT := (SELECT DISTINCT PERCENTILE_DISC(0.9999) 
						WITHIN GROUP (ORDER BY CNT_MONTH NULLS FIRST)
					FROM VPM_DATA.CUST_PURCHASED_MONTH
					WHERE PRODUCT_LIFESTYLE = 'HOME GARDENING');
BEGIN
	UPDATE VPM_DATA.CUST_PURCHASED_MONTH
	SET LIFESTYLE_GARDENING_SCORE = 
	0.2*(CASE WHEN RECENCY <= RECENCY_D THEN 1 
			  WHEN RECENCY > 365 THEN 0
		ELSE (1-(((RECENCY-RECENCY_D)*1.0)/((365-RECENCY_D)*1.0)))END)
	+ 0.8*(CASE WHEN CNT_MONTH >= FREQUENCY_D THEN 1 ELSE (CNT_MONTH*1.0)/FREQUENCY_D END)
	WHERE PRODUCT_LIFESTYLE = 'HOME GARDENING';
END $$;


/***************************************************************************************/

--2. HOME IMPROVEMENT
DO $$
DECLARE RECENCY_D SMALLINT := (SELECT DISTINCT PERCENTILE_DISC(0.9) 
							WITHIN GROUP (ORDER BY RECENCY DESC NULLS LAST)
						FROM VPM_DATA.CUST_PURCHASED_MONTH
						WHERE PRODUCT_LIFESTYLE = 'HOME IMPROVEMENT');
	FREQUENCY_D SMALLINT := (SELECT DISTINCT PERCENTILE_DISC(0.9) 
						WITHIN GROUP (ORDER BY CNT_MONTH NULLS FIRST)
					FROM VPM_DATA.CUST_PURCHASED_MONTH
					WHERE PRODUCT_LIFESTYLE = 'HOME IMPROVEMENT');
BEGIN
	UPDATE VPM_DATA.CUST_PURCHASED_MONTH
	SET LIFESTYLE_IMPROVEMENT_SCORE = 
	0.2*(CASE WHEN RECENCY <= RECENCY_D THEN 1 
		ELSE (1-(((RECENCY-RECENCY_D)*1.0)/(((365*2)-RECENCY_D)*1.0)))END)
	+ 0.8*(CASE WHEN CNT_MONTH >= FREQUENCY_D THEN 1 ELSE (CNT_MONTH*1.0)/FREQUENCY_D END)
	WHERE PRODUCT_LIFESTYLE = 'HOME IMPROVEMENT'
	;
END $$;
--RECHECK COUNT
SELECT COUNT(*) --185764 , 214279 (DEC2019)
FROM VPM_DATA.CUST_PURCHASED_MONTH
WHERE LIFESTYLE_IMPROVEMENT_SCORE >= 0.8;

/***************************************************************************************/

--3. HOME DECORATION
DO $$
DECLARE RECENCY_D SMALLINT := (SELECT DISTINCT PERCENTILE_DISC(0.9) 
							WITHIN GROUP (ORDER BY RECENCY DESC NULLS LAST)
						FROM VPM_DATA.CUST_PURCHASED_MONTH
						WHERE PRODUCT_LIFESTYLE = 'HOME DECORATION');	
	FREQUENCY_D SMALLINT := (SELECT DISTINCT PERCENTILE_DISC(0.9) 
						WITHIN GROUP (ORDER BY CNT_MONTH NULLS FIRST)
					FROM VPM_DATA.CUST_PURCHASED_MONTH
					WHERE PRODUCT_LIFESTYLE = 'HOME DECORATION');
BEGIN
	UPDATE VPM_DATA.CUST_PURCHASED_MONTH
	SET LIFESTYLE_DECOR_SCORE = 
	0.2*(CASE WHEN RECENCY <= RECENCY_D THEN 1 
		ELSE (1-(((RECENCY-RECENCY_D)*1.0)/(((365*2)-RECENCY_D)*1.0)))END)
	+ 0.8*(CASE WHEN CNT_MONTH >= FREQUENCY_D THEN 1 ELSE (CNT_MONTH*1.0)/FREQUENCY_D END)
	WHERE PRODUCT_LIFESTYLE = 'HOME DECORATION';
END $$;


/***************************************************************************************/

DROP TABLE IF EXISTS VPM_DATA.CUST_LIFESTYLE;
SELECT	 CUSTOMER_ID
		,MAX(CASE WHEN PRODUCT_LIFESTYLE = 'HOME GARDENING' THEN RECENCY END) AS HOME_GARDENING_RECENCY
		,MAX(CASE WHEN PRODUCT_LIFESTYLE = 'HOME GARDENING' THEN CNT_MONTH END) AS HOME_GARDENING_M_VISIT
		
		,MAX(CASE WHEN PRODUCT_LIFESTYLE = 'HOME IMPROVEMENT' THEN RECENCY END) AS HOME_IMPROVEMENT_RECENCY
		,MAX(CASE WHEN PRODUCT_LIFESTYLE = 'HOME IMPROVEMENT' THEN CNT_MONTH END) AS HOME_IMPROVEMENT_M_VISIT
		
		,MAX(CASE WHEN PRODUCT_LIFESTYLE = 'HOME DECORATION' THEN RECENCY END) AS HOME_DECOR_RECENCY
		,MAX(CASE WHEN PRODUCT_LIFESTYLE = 'HOME DECORATION' THEN CNT_MONTH END) AS HOME_DECOR_M_VISIT
		
		,MAX(CASE WHEN PRODUCT_LIFESTYLE = 'HOME GARDENING' THEN LIFESTYLE_GARDENING_SCORE END) AS LIFESTYLE_GARDENING_SCORE
		,MAX(CASE WHEN PRODUCT_LIFESTYLE = 'HOME IMPROVEMENT' THEN LIFESTYLE_IMPROVEMENT_SCORE END) AS LIFESTYLE_IMPROVEMENT_SCORE
		,MAX(CASE WHEN PRODUCT_LIFESTYLE = 'HOME DECORATION' THEN LIFESTYLE_DECOR_SCORE END) AS LIFESTYLE_DECOR_SCORE
INTO VPM_DATA.CUST_LIFESTYLE		
FROM VPM_DATA.CUST_PURCHASED_MONTH
GROUP BY CUSTOMER_ID;

UPDATE VPM_DATA.PM_HOME_PRODUCT_CUST_SEGMENT
SET	 HOME_GARDENING_RECENCY = A.HOME_GARDENING_RECENCY
	,HOME_GARDENING_VISIT = A.HOME_GARDENING_M_VISIT
	,HOME_IMPROVEMENT_RECENCY = A.HOME_IMPROVEMENT_RECENCY
	,HOME_IMPROVEMENT_VISIT = A.HOME_IMPROVEMENT_M_VISIT
	,HOME_DECOR_RECENCY = A.HOME_DECOR_RECENCY
	,HOME_DECOR_VISIT = A.HOME_DECOR_M_VISIT
	,LIFESTYLE_IMPROVEMENT_SCORE = A.LIFESTYLE_IMPROVEMENT_SCORE
	,LIFESTYLE_GARDENING_SCORE = A.LIFESTYLE_GARDENING_SCORE
	,LIFESTYLE_DECOR_SCORE = A.LIFESTYLE_DECOR_SCORE
FROM VPM_DATA.CUST_LIFESTYLE A
WHERE A.CUSTOMER_ID = PM_HOME_PRODUCT_CUST_SEGMENT.CUSTOMER_ID;

/***************************************************************************************/

--SEGMENT

UPDATE VPM_DATA.PM_HOME_PRODUCT_CUST_SEGMENT
SET	 LIFESTYLE_GARDENING_FLAG = CASE WHEN LIFESTYLE_GARDENING_SCORE > 0.5 THEN 1 END
	,LIFESTYLE_IMPROVEMENT_FLAG = CASE WHEN LIFESTYLE_IMPROVEMENT_SCORE > 0.9 THEN 1 END
	,LIFESTYLE_DECOR_FLAG = CASE WHEN LIFESTYLE_DECOR_SCORE > 0.9 THEN 1 END
WHERE PURPOSE = 'RESIDENTIAL';

UPDATE VPM_DATA.SALESSKU_LOG_TIME SET end_time = now() WHERE task = 'E.HOME STEP 6.0 - HOME LIFESTYLE' AND EXTRACT(MONTH FROM start_time) = EXTRACT(MONTH FROM now());
