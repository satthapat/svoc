
--147m 37s
INSERT INTO VPM_DATA.SALESSKU_LOG_TIME(task,start_time) VALUES ('Step1.3.4 Estimated Income from CC - GCS (Edited to 1 Yr)',NOW());
--587m 58s
DROP TABLE IF EXISTS VPM_DATA.TEMP_SALES_TXN_STAGING_CDS;
DROP TABLE IF EXISTS VPM_DATA.TEMP_SALES_TXN_STAGING_RBS;
DROP TABLE IF EXISTS VPM_DATA.TEMP_SALES_TXN_STAGING_B2S;
DROP TABLE IF EXISTS VPM_DATA.TEMP_SALES_TXN_STAGING_HWS;
DROP TABLE IF EXISTS VPM_DATA.TEMP_SALES_TXN_STAGING_SSP;
DROP TABLE IF EXISTS VPM_DATA.TEMP_SALES_TXN_STAGING_PWB;
DROP TABLE IF EXISTS VPM_DATA.TEMP_SALES_TXN_STAGING_ODP;
DROP TABLE IF EXISTS VPM_DATA.TEMP_SALES_TXN_STAGING_CMG;
DROP TABLE IF EXISTS VPM_DATA.TEMP_SALES_TXN_STAGING_TWD;
DROP TABLE IF EXISTS VPM_DATA.TEMP_SALES_TXN_STAGING_TOPS;
DROP TABLE IF EXISTS VPM_DATA.TEMP_SALES_TXN_STAGING_CFM;
DROP TABLE IF EXISTS VPM_DATA.TEMP_SALES_TXN_STAGING_GCS;
 
CREATE TABLE VPM_DATA.TEMP_SALES_TXN_STAGING_CDS AS
SELECT CUSTOMERID AS CUSTOMER_ID, VPM_DATA.DATEDIFF('month',TransactionDate::DATE,VPM_DATA.GET_END_DATE(NOW())) AS TXN_MONTH,SUM(Spending) AS MONTHLY_SPEND
FROM analysis_data.salessku_cds2020
WHERE transactiondate::DATE >= vpm_data.get_start_date(NOW())
GROUP BY CUSTOMERID, VPM_DATA.DATEDIFF('month',TransactionDate::DATE,VPM_DATA.GET_END_DATE(NOW()))
UNION ALL
SELECT CUSTOMERID AS CUSTOMER_ID, VPM_DATA.DATEDIFF('month',TransactionDate::DATE,VPM_DATA.GET_END_DATE(NOW())) AS TXN_MONTH,SUM(Spending) AS MONTHLY_SPEND
FROM analysis_data.salessku_cds2021
WHERE transactiondate::DATE <= vpm_data.get_end_date(NOW())
GROUP BY CUSTOMERID, VPM_DATA.DATEDIFF('month',TransactionDate::DATE,VPM_DATA.GET_END_DATE(NOW()));

CREATE TABLE VPM_DATA.TEMP_SALES_TXN_STAGING_RBS AS
SELECT CUSTOMERID AS CUSTOMER_ID, VPM_DATA.DATEDIFF('month',TransactionDate::DATE,VPM_DATA.GET_END_DATE(NOW())) AS TXN_MONTH,SUM(Spending) AS MONTHLY_SPEND
FROM analysis_data.salessku_RBS2020
WHERE transactiondate::DATE >= vpm_data.get_start_date(NOW())
GROUP BY CUSTOMERID, VPM_DATA.DATEDIFF('month',TransactionDate::DATE,VPM_DATA.GET_END_DATE(NOW()))
UNION ALL
SELECT CUSTOMERID AS CUSTOMER_ID, VPM_DATA.DATEDIFF('month',TransactionDate::DATE,VPM_DATA.GET_END_DATE(NOW())) AS TXN_MONTH,SUM(Spending) AS MONTHLY_SPEND
FROM analysis_data.salessku_RBS2021
WHERE transactiondate::DATE <= vpm_data.get_end_date(NOW())
GROUP BY CUSTOMERID, VPM_DATA.DATEDIFF('month',TransactionDate::DATE,VPM_DATA.GET_END_DATE(NOW()));

CREATE TABLE VPM_DATA.TEMP_SALES_TXN_STAGING_B2S AS
SELECT CUSTOMERID AS CUSTOMER_ID, VPM_DATA.DATEDIFF('month',TransactionDate::DATE,VPM_DATA.GET_END_DATE(NOW())) AS TXN_MONTH,SUM(Spending) AS MONTHLY_SPEND
FROM analysis_data.salessku_B2S2020
WHERE transactiondate::DATE >= vpm_data.get_start_date(NOW())
GROUP BY CUSTOMERID, VPM_DATA.DATEDIFF('month',TransactionDate::DATE,VPM_DATA.GET_END_DATE(NOW()))
UNION ALL
SELECT CUSTOMERID AS CUSTOMER_ID, VPM_DATA.DATEDIFF('month',TransactionDate::DATE,VPM_DATA.GET_END_DATE(NOW())) AS TXN_MONTH,SUM(Spending) AS MONTHLY_SPEND
FROM analysis_data.salessku_B2S2021
WHERE transactiondate::DATE <= vpm_data.get_end_date(NOW())
GROUP BY CUSTOMERID, VPM_DATA.DATEDIFF('month',TransactionDate::DATE,VPM_DATA.GET_END_DATE(NOW()));

CREATE TABLE VPM_DATA.TEMP_SALES_TXN_STAGING_HWS AS
SELECT CUSTOMERID AS CUSTOMER_ID, VPM_DATA.DATEDIFF('month',TransactionDate::DATE,VPM_DATA.GET_END_DATE(NOW())) AS TXN_MONTH,SUM(Spending) AS MONTHLY_SPEND
FROM analysis_data.salessku_HWS2020
WHERE transactiondate::DATE >= vpm_data.get_start_date(NOW())
GROUP BY CUSTOMERID, VPM_DATA.DATEDIFF('month',TransactionDate::DATE,VPM_DATA.GET_END_DATE(NOW()))
UNION ALL
SELECT CUSTOMERID AS CUSTOMER_ID, VPM_DATA.DATEDIFF('month',TransactionDate::DATE,VPM_DATA.GET_END_DATE(NOW())) AS TXN_MONTH,SUM(Spending) AS MONTHLY_SPEND
FROM analysis_data.salessku_HWS2021
WHERE transactiondate::DATE <= vpm_data.get_end_date(NOW())
GROUP BY CUSTOMERID, VPM_DATA.DATEDIFF('month',TransactionDate::DATE,VPM_DATA.GET_END_DATE(NOW()));

CREATE TABLE VPM_DATA.TEMP_SALES_TXN_STAGING_SSP AS
SELECT CUSTOMERID AS CUSTOMER_ID, VPM_DATA.DATEDIFF('month',TransactionDate::DATE,VPM_DATA.GET_END_DATE(NOW())) AS TXN_MONTH,SUM(Spending) AS MONTHLY_SPEND
FROM analysis_data.salessku_SSP2020
WHERE transactiondate::DATE >= vpm_data.get_start_date(NOW())
GROUP BY CUSTOMERID, VPM_DATA.DATEDIFF('month',TransactionDate::DATE,VPM_DATA.GET_END_DATE(NOW()))
UNION ALL
SELECT CUSTOMERID AS CUSTOMER_ID, VPM_DATA.DATEDIFF('month',TransactionDate::DATE,VPM_DATA.GET_END_DATE(NOW())) AS TXN_MONTH,SUM(Spending) AS MONTHLY_SPEND
FROM analysis_data.salessku_SSP2021
WHERE transactiondate::DATE <= vpm_data.get_end_date(NOW())
GROUP BY CUSTOMERID, VPM_DATA.DATEDIFF('month',TransactionDate::DATE,VPM_DATA.GET_END_DATE(NOW()));

CREATE TABLE VPM_DATA.TEMP_SALES_TXN_STAGING_PWB AS
SELECT CUSTOMERID AS CUSTOMER_ID, VPM_DATA.DATEDIFF('month',TransactionDate::DATE,VPM_DATA.GET_END_DATE(NOW())) AS TXN_MONTH,SUM(Spending) AS MONTHLY_SPEND
FROM analysis_data.salessku_PWB2020
WHERE transactiondate::DATE >= vpm_data.get_start_date(NOW())
GROUP BY CUSTOMERID, VPM_DATA.DATEDIFF('month',TransactionDate::DATE,VPM_DATA.GET_END_DATE(NOW()))
UNION ALL
SELECT CUSTOMERID AS CUSTOMER_ID, VPM_DATA.DATEDIFF('month',TransactionDate::DATE,VPM_DATA.GET_END_DATE(NOW())) AS TXN_MONTH,SUM(Spending) AS MONTHLY_SPEND
FROM analysis_data.salessku_PWB2021
WHERE transactiondate::DATE <= vpm_data.get_end_date(NOW())
GROUP BY CUSTOMERID, VPM_DATA.DATEDIFF('month',TransactionDate::DATE,VPM_DATA.GET_END_DATE(NOW()));

CREATE TABLE VPM_DATA.TEMP_SALES_TXN_STAGING_ODP AS
SELECT CUSTOMERID AS CUSTOMER_ID, VPM_DATA.DATEDIFF('month',TransactionDate::DATE,VPM_DATA.GET_END_DATE(NOW())) AS TXN_MONTH,SUM(Spending) AS MONTHLY_SPEND
FROM analysis_data.salessku_ODP2020
WHERE transactiondate::DATE >= vpm_data.get_start_date(NOW())
GROUP BY CUSTOMERID, VPM_DATA.DATEDIFF('month',TransactionDate::DATE,VPM_DATA.GET_END_DATE(NOW()))
UNION ALL
SELECT CUSTOMERID AS CUSTOMER_ID, VPM_DATA.DATEDIFF('month',TransactionDate::DATE,VPM_DATA.GET_END_DATE(NOW())) AS TXN_MONTH,SUM(Spending) AS MONTHLY_SPEND
FROM analysis_data.salessku_ODP2021
WHERE transactiondate::DATE <= vpm_data.get_end_date(NOW())
GROUP BY CUSTOMERID, VPM_DATA.DATEDIFF('month',TransactionDate::DATE,VPM_DATA.GET_END_DATE(NOW()));

CREATE TABLE VPM_DATA.TEMP_SALES_TXN_STAGING_CMG AS
SELECT CUSTOMERID AS CUSTOMER_ID, VPM_DATA.DATEDIFF('month',TransactionDate::DATE,VPM_DATA.GET_END_DATE(NOW())) AS TXN_MONTH,SUM(Spending) AS MONTHLY_SPEND
FROM analysis_data.salessku_cmg2020_sbl
WHERE transactiondate::DATE >= vpm_data.get_start_date(NOW())
GROUP BY CUSTOMERID, VPM_DATA.DATEDIFF('month',TransactionDate::DATE,VPM_DATA.GET_END_DATE(NOW()))
UNION ALL
SELECT CUSTOMERID AS CUSTOMER_ID, VPM_DATA.DATEDIFF('month',TransactionDate::DATE,VPM_DATA.GET_END_DATE(NOW())) AS TXN_MONTH,SUM(Spending) AS MONTHLY_SPEND
FROM analysis_data.salessku_cmg2021_sbl
WHERE transactiondate::DATE <= vpm_data.get_end_date(NOW())
GROUP BY CUSTOMERID, VPM_DATA.DATEDIFF('month',TransactionDate::DATE,VPM_DATA.GET_END_DATE(NOW()));

CREATE TABLE VPM_DATA.TEMP_SALES_TXN_STAGING_TWD AS
SELECT CUSTOMERID AS CUSTOMER_ID, VPM_DATA.DATEDIFF('month',TransactionDate::DATE,VPM_DATA.GET_END_DATE(NOW())) AS TXN_MONTH,SUM(Spending) AS MONTHLY_SPEND
FROM analysis_data.salessku_TWD2020
WHERE transactiondate::DATE >= vpm_data.get_start_date(NOW())
GROUP BY CUSTOMERID, VPM_DATA.DATEDIFF('month',TransactionDate::DATE,VPM_DATA.GET_END_DATE(NOW()))
UNION ALL
SELECT CUSTOMERID AS CUSTOMER_ID, VPM_DATA.DATEDIFF('month',TransactionDate::DATE,VPM_DATA.GET_END_DATE(NOW())) AS TXN_MONTH,SUM(Spending) AS MONTHLY_SPEND
FROM analysis_data.salessku_TWD2021
WHERE transactiondate::DATE <= vpm_data.get_end_date(NOW())
GROUP BY CUSTOMERID, VPM_DATA.DATEDIFF('month',TransactionDate::DATE,VPM_DATA.GET_END_DATE(NOW()));

CREATE TABLE VPM_DATA.TEMP_SALES_TXN_STAGING_TOPS AS
SELECT CUSTOMERID AS CUSTOMER_ID, VPM_DATA.DATEDIFF('month',TransactionDate::DATE,VPM_DATA.GET_END_DATE(NOW())) AS TXN_MONTH,SUM(Spending) AS MONTHLY_SPEND
FROM analysis_data.salessku_tops2020
WHERE transactiondate::DATE >= vpm_data.get_start_date(NOW())
GROUP BY CUSTOMERID, VPM_DATA.DATEDIFF('month',TransactionDate::DATE,VPM_DATA.GET_END_DATE(NOW()))
UNION ALL
SELECT CUSTOMERID AS CUSTOMER_ID, VPM_DATA.DATEDIFF('month',TransactionDate::DATE,VPM_DATA.GET_END_DATE(NOW())) AS TXN_MONTH,SUM(Spending) AS MONTHLY_SPEND
FROM analysis_data.salessku_tops2021
WHERE transactiondate::DATE <= vpm_data.get_end_date(NOW())
GROUP BY CUSTOMERID, VPM_DATA.DATEDIFF('month',TransactionDate::DATE,VPM_DATA.GET_END_DATE(NOW()));

CREATE TABLE VPM_DATA.TEMP_SALES_TXN_STAGING_CFM AS
SELECT CUSTOMERID AS CUSTOMER_ID, VPM_DATA.DATEDIFF('month',TransactionDate::DATE,VPM_DATA.GET_END_DATE(NOW())) AS TXN_MONTH,SUM(Spending) AS MONTHLY_SPEND
FROM analysis_data.salessku_cfm2020
WHERE transactiondate::DATE >= vpm_data.get_start_date(NOW())
GROUP BY CUSTOMERID, VPM_DATA.DATEDIFF('month',TransactionDate::DATE,VPM_DATA.GET_END_DATE(NOW()))
UNION ALL
SELECT CUSTOMERID AS CUSTOMER_ID, VPM_DATA.DATEDIFF('month',TransactionDate::DATE,VPM_DATA.GET_END_DATE(NOW())) AS TXN_MONTH,SUM(Spending) AS MONTHLY_SPEND
FROM analysis_data.salessku_cfm2021
WHERE transactiondate::DATE <= vpm_data.get_end_date(NOW())
GROUP BY CUSTOMERID, VPM_DATA.DATEDIFF('month',TransactionDate::DATE,VPM_DATA.GET_END_DATE(NOW()));

CREATE TABLE VPM_DATA.TEMP_SALES_TXN_STAGING_GCS AS
SELECT UPPER(CUSTOMERID),VPM_DATA.DATEDIFF('month',TransactionDate::DATE,VPM_DATA.GET_END_DATE(NOW())) AS TXN_MONTH,SUM(SpendingAmount) AS MONTHLY_SPEND   
FROM analysis_data.GCS_DetSpd
WHERE UPPER(BUorNonBU) = 'NON BU' 
AND CAST(TransactionDate AS DATE) >= VPM_DATA.GET_START_DATE(NOW()) AND CAST(TransactionDate AS DATE) <= VPM_DATA.GET_END_DATE(NOW())
GROUP BY CUSTOMERID,VPM_DATA.DATEDIFF('month',TransactionDate::DATE,VPM_DATA.GET_END_DATE(NOW()));

DROP TABLE IF EXISTS VPM_DATA.TEMP_SALES_TXN; --2m 50s
SELECT * INTO VPM_DATA.TEMP_SALES_TXN
FROM VPM_DATA.TEMP_SALES_TXN_STAGING_CDS
UNION SELECT * FROM VPM_DATA.TEMP_SALES_TXN_STAGING_RBS
UNION SELECT * FROM VPM_DATA.TEMP_SALES_TXN_STAGING_B2S
UNION SELECT * FROM VPM_DATA.TEMP_SALES_TXN_STAGING_HWS
UNION SELECT * FROM VPM_DATA.TEMP_SALES_TXN_STAGING_SSP
UNION SELECT * FROM VPM_DATA.TEMP_SALES_TXN_STAGING_PWB
UNION SELECT * FROM VPM_DATA.TEMP_SALES_TXN_STAGING_ODP
UNION SELECT * FROM VPM_DATA.TEMP_SALES_TXN_STAGING_CMG
UNION SELECT * FROM VPM_DATA.TEMP_SALES_TXN_STAGING_TWD
UNION SELECT * FROM VPM_DATA.TEMP_SALES_TXN_STAGING_TOPS
UNION SELECT * FROM VPM_DATA.TEMP_SALES_TXN_STAGING_CFM
UNION SELECT * FROM VPM_DATA.TEMP_SALES_TXN_STAGING_GCS;

/*
DROP TABLE IF EXISTS VPM_DATA.TEMP_SALES_TXN_STAGING1;
DROP TABLE IF EXISTS VPM_DATA.TEMP_SALES_TXN_STAGING2;
DROP TABLE IF EXISTS VPM_DATA.TEMP_SALES_TXN_STAGING3;
DROP TABLE IF EXISTS VPM_DATA.TEMP_SALES_TXN_STAGING4;*/
							 
/*** SPENDING PERSISTENCY ***/
--47m 4s
DROP TABLE IF EXISTS VPM_DATA.PM_CUST_MONTHLY_SPEND_1Y;

SELECT CUSTOMER_ID, TXN_MONTH, SUM(MONTHLY_SPEND) AS MONTHLY_SPEND
INTO VPM_DATA.PM_CUST_MONTHLY_SPEND_1Y
FROM VPM_DATA.TEMP_SALES_TXN T
GROUP BY CUSTOMER_ID, TXN_MONTH;

DROP TABLE IF EXISTS VPM_DATA.PM_CUST_SVOC_MONTHLY_SPEND_1Y_PERSISTENCY;
SELECT CUSTOMER_ID AS CUSTOMER_ID, 
	   COUNT(TXN_MONTH) AS CNT_TXN_MONTH,
	   MAX(MONTHLY_SPEND) AS MAX_MONTHLY_SPENDING, 
	   MAX(MONTHLY_SPEND)*(COUNT(TXN_MONTH)*1.0/12) AS WEIGHTED_MAX_MONTHLY_SPENDING,
	   SUM(MONTHLY_SPEND)/12 AS AVG_MONTHLY_SPEND,
	   CASE WHEN COALESCE(SUM(MONTHLY_SPEND)/12.0,0) = 0 THEN 0 
	   ELSE (STDDEV(MONTHLY_SPEND)*1.0)/(SUM(MONTHLY_SPEND)/12.0) END AS MONTHLY_SPEND_VOLATILITY
INTO VPM_DATA.PM_CUST_SVOC_MONTHLY_SPEND_1Y_PERSISTENCY
FROM VPM_DATA.PM_CUST_MONTHLY_SPEND_1Y
WHERE CUSTOMER_ID IS NOT NULL AND CUSTOMER_ID <> ''
GROUP BY CUSTOMER_ID;
	    				       

DROP TABLE IF EXISTS VPM_DATA.TEMP_CUST_ACTIVE;
SELECT DISTINCT CUSTOMER_ID, TXN_MONTH
INTO VPM_DATA.TEMP_CUST_ACTIVE 
FROM VPM_DATA.PM_CC_MONTHLY_SPEND_BY_CARD
WHERE TXN_MONTH BETWEEN 0 AND 11 AND CUSTOMER_ID IS NOT NULL AND CUSTOMER_ID <> ''
UNION
SELECT DISTINCT CUSTOMER_ID AS CUSTOMER_ID , TXN_MONTH
FROM VPM_DATA.PM_CUST_MONTHLY_SPEND_1Y
WHERE TXN_MONTH BETWEEN 0 AND 11 AND CUSTOMER_ID IS NOT NULL AND CUSTOMER_ID <> '';


DROP TABLE IF EXISTS VPM_DATA.PM_CUST_MONTHLY_CREDIT_SPEND;
SELECT A.*
	,(CASE WHEN COALESCE(C.MONTHLY_SPEND,0) < COALESCE(B.TOTAL_CREDIT_SPEND,0) THEN B.TOTAL_CREDIT_SPEND
				  ELSE COALESCE(C.MONTHLY_SPEND,0) END) AS MONTHLY_SPEND,
			 COALESCE(B.TOTAL_CREDIT_SPEND,0) AS TOTAL_CREDIT_SPEND, 
			 COALESCE(B.T1C_SPEND,0) AS T1C_SPEND, 
			 COALESCE(B.OTHER_CARDS_SPEND,0) AS OTHER_CARDS_SPEND, 	    
			(CASE WHEN COALESCE(C.MONTHLY_SPEND,0) < COALESCE(B.TOTAL_CREDIT_SPEND,0) THEN 0
				  ELSE COALESCE(C.MONTHLY_SPEND,0) - COALESCE(B.TOTAL_CREDIT_SPEND,0) END) AS TOTAL_CASH_SPEND
	,CAST(NULL AS FLOAT) AS CASH_TO_CREDIT_SPENDING_RATIO
	,CAST(NULL AS FLOAT) AS RECENCY_WEIGHT
INTO VPM_DATA.PM_CUST_MONTHLY_CREDIT_SPEND
FROM VPM_DATA.TEMP_CUST_ACTIVE A
LEFT JOIN 
(
SELECT CUSTOMER_ID, TXN_MONTH, 
	   SUM(MONTHLY_SPEND) AS TOTAL_CREDIT_SPEND,
	   SUM(CASE WHEN SUBSTRING(RangCreditCard,1,6) IN ('525668','525669','525667','528560','537798') THEN MONTHLY_SPEND ELSE 0 END) AS T1C_SPEND,
	   SUM(CASE WHEN SUBSTRING(RangCreditCard,1,6) NOT IN ('525668','525669','525667','528560','537798') THEN MONTHLY_SPEND ELSE 0 END) AS OTHER_CARDS_SPEND
FROM VPM_DATA.PM_CC_MONTHLY_SPEND_BY_CARD
WHERE MONTHLY_SPEND > 0
GROUP BY CUSTOMER_ID, TXN_MONTH) B ON A.CUSTOMER_ID = B.CUSTOMER_ID AND A.TXN_MONTH = B.TXN_MONTH
LEFT JOIN VPM_DATA.PM_CUST_MONTHLY_SPEND_1Y C ON A.CUSTOMER_ID = C.CUSTOMER_ID AND A.TXN_MONTH = C.TXN_MONTH
;

UPDATE VPM_DATA.PM_CUST_MONTHLY_CREDIT_SPEND
SET CASH_TO_CREDIT_SPENDING_RATIO = CASE WHEN (MONTHLY_SPEND*1.0) <= 0 THEN NULL
										 ELSE (TOTAL_CASH_SPEND*1.0)/(MONTHLY_SPEND*1.0) END,
	RECENCY_WEIGHT = (12-TXN_MONTH)/78.0
;									 


/*
select top 10000 * from PM_CUST_MONTHLY_CREDIT_SPEND order by CUSTOMER_ID*/

DROP TABLE IF EXISTS VPM_DATA.PM_CUST_SVOC_MONTHLY_CREDIT_SPEND;							 
SELECT CUSTOMER_ID,
		COUNT(DISTINCT TXN_MONTH) AS ACTIVE_MONTH
		,SUM(CASE WHEN TXN_MONTH = 0 THEN MONTHLY_SPEND ELSE 0 END) AS SPEND_AMT_M0
		,SUM(CASE WHEN TXN_MONTH = 1 THEN MONTHLY_SPEND ELSE 0 END) AS SPEND_AMT_M1
		,SUM(CASE WHEN TXN_MONTH = 2 THEN MONTHLY_SPEND ELSE 0 END) AS SPEND_AMT_M2
		,SUM(CASE WHEN TXN_MONTH = 3 THEN MONTHLY_SPEND ELSE 0 END) AS SPEND_AMT_M3
		,SUM(CASE WHEN TXN_MONTH = 4 THEN MONTHLY_SPEND ELSE 0 END) AS SPEND_AMT_M4
		,SUM(CASE WHEN TXN_MONTH = 5 THEN MONTHLY_SPEND ELSE 0 END) AS SPEND_AMT_M5
		,SUM(CASE WHEN TXN_MONTH = 6 THEN MONTHLY_SPEND ELSE 0 END) AS SPEND_AMT_M6
		,SUM(CASE WHEN TXN_MONTH = 7 THEN MONTHLY_SPEND ELSE 0 END) AS SPEND_AMT_M7
		,SUM(CASE WHEN TXN_MONTH = 8 THEN MONTHLY_SPEND ELSE 0 END) AS SPEND_AMT_M8
		,SUM(CASE WHEN TXN_MONTH = 9 THEN MONTHLY_SPEND ELSE 0 END) AS SPEND_AMT_M9
		,SUM(CASE WHEN TXN_MONTH = 10 THEN MONTHLY_SPEND ELSE 0 END) AS SPEND_AMT_M10
		,SUM(CASE WHEN TXN_MONTH = 11 THEN MONTHLY_SPEND ELSE 0 END) AS SPEND_AMT_M11
		,SUM(CASE WHEN TXN_MONTH = 0 THEN TOTAL_CREDIT_SPEND ELSE 0 END) AS CREDIT_AMT_M0
		,SUM(CASE WHEN TXN_MONTH = 1 THEN TOTAL_CREDIT_SPEND ELSE 0 END) AS CREDIT_AMT_M1
		,SUM(CASE WHEN TXN_MONTH = 2 THEN TOTAL_CREDIT_SPEND ELSE 0 END) AS CREDIT_AMT_M2
		,SUM(CASE WHEN TXN_MONTH = 3 THEN TOTAL_CREDIT_SPEND ELSE 0 END) AS CREDIT_AMT_M3
		,SUM(CASE WHEN TXN_MONTH = 4 THEN TOTAL_CREDIT_SPEND ELSE 0 END) AS CREDIT_AMT_M4
		,SUM(CASE WHEN TXN_MONTH = 5 THEN TOTAL_CREDIT_SPEND ELSE 0 END) AS CREDIT_AMT_M5
		,SUM(CASE WHEN TXN_MONTH = 6 THEN TOTAL_CREDIT_SPEND ELSE 0 END) AS CREDIT_AMT_M6
		,SUM(CASE WHEN TXN_MONTH = 7 THEN TOTAL_CREDIT_SPEND ELSE 0 END) AS CREDIT_AMT_M7
		,SUM(CASE WHEN TXN_MONTH = 8 THEN TOTAL_CREDIT_SPEND ELSE 0 END) AS CREDIT_AMT_M8
		,SUM(CASE WHEN TXN_MONTH = 9 THEN TOTAL_CREDIT_SPEND ELSE 0 END) AS CREDIT_AMT_M9
		,SUM(CASE WHEN TXN_MONTH = 10 THEN TOTAL_CREDIT_SPEND ELSE 0 END) AS CREDIT_AMT_M10
		,SUM(CASE WHEN TXN_MONTH = 11 THEN TOTAL_CREDIT_SPEND ELSE 0 END) AS CREDIT_AMT_M11
		,SUM(CASE WHEN TXN_MONTH BETWEEN 0 AND 2 THEN TOTAL_CREDIT_SPEND ELSE 0 END) AS TOTAL_CREDIT_AMT_3M
		,SUM(CASE WHEN TXN_MONTH BETWEEN 0 AND 5 THEN TOTAL_CREDIT_SPEND ELSE 0 END) AS TOTAL_CREDIT_AMT_6M
		,SUM(CASE WHEN TXN_MONTH BETWEEN 0 AND 11 THEN TOTAL_CREDIT_SPEND ELSE 0 END) AS TOTAL_CREDIT_AMT_12M
		,SUM(CASE WHEN TXN_MONTH = 0 THEN CASH_TO_CREDIT_SPENDING_RATIO ELSE 0 END) AS CASH_TO_CREDIT_M0
		,SUM(CASE WHEN TXN_MONTH = 1 THEN CASH_TO_CREDIT_SPENDING_RATIO ELSE 0 END) AS CASH_TO_CREDIT_M1
		,SUM(CASE WHEN TXN_MONTH = 2 THEN CASH_TO_CREDIT_SPENDING_RATIO ELSE 0 END) AS CASH_TO_CREDIT_M2
		,SUM(CASE WHEN TXN_MONTH = 3 THEN CASH_TO_CREDIT_SPENDING_RATIO ELSE 0 END) AS CASH_TO_CREDIT_M3
		,SUM(CASE WHEN TXN_MONTH = 4 THEN CASH_TO_CREDIT_SPENDING_RATIO ELSE 0 END) AS CASH_TO_CREDIT_M4
		,SUM(CASE WHEN TXN_MONTH = 5 THEN CASH_TO_CREDIT_SPENDING_RATIO ELSE 0 END) AS CASH_TO_CREDIT_M5
		,SUM(CASE WHEN TXN_MONTH = 6 THEN CASH_TO_CREDIT_SPENDING_RATIO ELSE 0 END) AS CASH_TO_CREDIT_M6
		,SUM(CASE WHEN TXN_MONTH = 7 THEN CASH_TO_CREDIT_SPENDING_RATIO ELSE 0 END) AS CASH_TO_CREDIT_M7
		,SUM(CASE WHEN TXN_MONTH = 8 THEN CASH_TO_CREDIT_SPENDING_RATIO ELSE 0 END) AS CASH_TO_CREDIT_M8
		,SUM(CASE WHEN TXN_MONTH = 9 THEN CASH_TO_CREDIT_SPENDING_RATIO ELSE 0 END) AS CASH_TO_CREDIT_M9
		,SUM(CASE WHEN TXN_MONTH = 10 THEN CASH_TO_CREDIT_SPENDING_RATIO ELSE 0 END) AS CASH_TO_CREDIT_M10
		,SUM(CASE WHEN TXN_MONTH = 11 THEN CASH_TO_CREDIT_SPENDING_RATIO ELSE 0 END) AS CASH_TO_CREDIT_M11
		,MAX(MONTHLY_SPEND) AS MAX_MONTHLY_SPEND
		,SUM(MONTHLY_SPEND) AS TOTAL_SPEND
		,SUM(TOTAL_CREDIT_SPEND) AS TOTAL_CREDIT_SPEND
		,SUM(T1C_SPEND) AS T1C_SPEND
		,CASE WHEN SUM(MONTHLY_SPEND) <= 0 THEN 0
			 ELSE SUM(TOTAL_CASH_SPEND*1.0)/SUM(MONTHLY_SPEND*1.0) END AS CASH_TO_CREDIT_SPENDING_RATIO
		,SUM(CASH_TO_CREDIT_SPENDING_RATIO*RECENCY_WEIGHT)/SUM(RECENCY_WEIGHT) AS CASH_TO_CREDIT_SPENDING_WEIGHTED_RCC_RATIO		
		,CAST(NULL AS VARCHAR(50)) AS PAYMENT_METHOD 
		,CAST(NULL AS FLOAT) AS NEW_TO_CREDIT_CARD_CONFIDENCE
		,CAST(NULL AS FLOAT) AS SPENDING_RATE_OF_CHG_M0
		,CAST(NULL AS FLOAT) AS SPENDING_RATE_OF_CHG_M1
		,CAST(NULL AS FLOAT) AS SPENDING_RATE_OF_CHG_M2
		,CAST(NULL AS FLOAT) AS SPENDING_RATE_OF_CHG_M3
		,CAST(NULL AS FLOAT) AS SPENDING_RATE_OF_CHG_M4
		,CAST(NULL AS FLOAT) AS SPENDING_RATE_OF_CHG_M5
		,CAST(NULL AS FLOAT) AS SPENDING_RATE_OF_CHG_M6
		,CAST(NULL AS FLOAT) AS SPENDING_RATE_OF_CHG_M7
		,CAST(NULL AS FLOAT) AS SPENDING_RATE_OF_CHG_M8
		,CAST(NULL AS FLOAT) AS SPENDING_RATE_OF_CHG_M9
		,CAST(NULL AS FLOAT) AS SPENDING_RATE_OF_CHG_M10
		,CAST(NULL AS FLOAT) AS AVG_RATE_OF_CHG
		,CAST(NULL AS FLOAT) AS WEIGHTED_RECENCY_RATE_OF_CHG
INTO VPM_DATA.PM_CUST_SVOC_MONTHLY_CREDIT_SPEND
FROM VPM_DATA.PM_CUST_MONTHLY_CREDIT_SPEND
GROUP BY CUSTOMER_ID
;

/*
select top 100 * from PM_CUST_SVOC_MONTHLY_CREDIT_SPEND*/

UPDATE VPM_DATA.PM_CUST_SVOC_MONTHLY_CREDIT_SPEND
SET PAYMENT_METHOD = CASE WHEN T1C_SPEND > 0 AND TOTAL_CREDIT_SPEND > 1.05*T1C_SPEND AND CASH_TO_CREDIT_SPENDING_RATIO > 0.05 THEN 'ALL'
			  WHEN T1C_SPEND > 0 AND TOTAL_CREDIT_SPEND <= 1.05*T1C_SPEND AND CASH_TO_CREDIT_SPENDING_RATIO > 0.05 THEN 'T1C + CASH'
			  WHEN T1C_SPEND > 0 AND TOTAL_CREDIT_SPEND > 1.05*T1C_SPEND AND CASH_TO_CREDIT_SPENDING_RATIO <= 0.05 THEN 'T1C + OTHER CC'
			  WHEN T1C_SPEND > 0 AND TOTAL_CREDIT_SPEND <= 1.05*T1C_SPEND AND CASH_TO_CREDIT_SPENDING_RATIO <= 0.05 THEN 'T1C ONLY'
			  WHEN T1C_SPEND = 0 AND TOTAL_CREDIT_SPEND <= 1.05*T1C_SPEND AND CASH_TO_CREDIT_SPENDING_RATIO > 0.05 THEN 'CASH ONLY'
			  WHEN T1C_SPEND = 0 AND TOTAL_CREDIT_SPEND > 1.05*T1C_SPEND AND CASH_TO_CREDIT_SPENDING_RATIO <= 0.05 THEN 'OTHER CC ONLY'
			  WHEN T1C_SPEND = 0 AND TOTAL_CREDIT_SPEND > 1.05*T1C_SPEND AND CASH_TO_CREDIT_SPENDING_RATIO > 0.05 THEN 'OTHER CC + CASH' ELSE '' END,
	NEW_TO_CREDIT_CARD_CONFIDENCE = CASE WHEN TOTAL_CREDIT_AMT_12M > 0 THEN
									((TOTAL_CREDIT_AMT_3M/TOTAL_CREDIT_AMT_12M)*2.0/3.0
									+(TOTAL_CREDIT_AMT_6M/TOTAL_CREDIT_AMT_12M)*1.0/3.0)*ACTIVE_MONTH/12.0 ELSE 0 END,
	SPENDING_RATE_OF_CHG_M0 = CASE WHEN SPEND_AMT_M0-SPEND_AMT_M1 > 0 THEN (SPEND_AMT_M0-SPEND_AMT_M1)/MAX_MONTHLY_SPEND
								   WHEN SPEND_AMT_M1 > 0 THEN (SPEND_AMT_M0-SPEND_AMT_M1)/MAX_MONTHLY_SPEND
								   ELSE 0 END,
	SPENDING_RATE_OF_CHG_M1 = CASE WHEN SPEND_AMT_M1-SPEND_AMT_M2 > 0 THEN (SPEND_AMT_M1-SPEND_AMT_M2)/MAX_MONTHLY_SPEND
								   WHEN SPEND_AMT_M2 > 0 THEN (SPEND_AMT_M1-SPEND_AMT_M2)/MAX_MONTHLY_SPEND
								   ELSE 0 END,
	SPENDING_RATE_OF_CHG_M2 = CASE WHEN SPEND_AMT_M2-SPEND_AMT_M3 > 0 THEN (SPEND_AMT_M2-SPEND_AMT_M3)/MAX_MONTHLY_SPEND
								   WHEN SPEND_AMT_M3 > 0 THEN (SPEND_AMT_M2-SPEND_AMT_M3)/MAX_MONTHLY_SPEND
								   ELSE 0 END,
	SPENDING_RATE_OF_CHG_M3 = CASE WHEN SPEND_AMT_M3-SPEND_AMT_M4 > 0 THEN (SPEND_AMT_M3-SPEND_AMT_M4)/MAX_MONTHLY_SPEND
								   WHEN SPEND_AMT_M4 > 0 THEN (SPEND_AMT_M3-SPEND_AMT_M4)/MAX_MONTHLY_SPEND
								   ELSE 0 END,
	SPENDING_RATE_OF_CHG_M4 = CASE WHEN SPEND_AMT_M4-SPEND_AMT_M5 > 0 THEN (SPEND_AMT_M4-SPEND_AMT_M5)/MAX_MONTHLY_SPEND
								   WHEN SPEND_AMT_M5 > 0 THEN (SPEND_AMT_M4-SPEND_AMT_M5)/MAX_MONTHLY_SPEND
								   ELSE 0 END,
	SPENDING_RATE_OF_CHG_M5 = CASE WHEN SPEND_AMT_M5-SPEND_AMT_M6 > 0 THEN (SPEND_AMT_M5-SPEND_AMT_M6)/MAX_MONTHLY_SPEND
								   WHEN SPEND_AMT_M6 > 0 THEN (SPEND_AMT_M5-SPEND_AMT_M6)/MAX_MONTHLY_SPEND
								   ELSE 0 END,
	SPENDING_RATE_OF_CHG_M6 = CASE WHEN SPEND_AMT_M6-SPEND_AMT_M7 > 0 THEN (SPEND_AMT_M6-SPEND_AMT_M7)/MAX_MONTHLY_SPEND
								   WHEN SPEND_AMT_M7 > 0 THEN (SPEND_AMT_M6-SPEND_AMT_M7)/MAX_MONTHLY_SPEND
								   ELSE 0 END,
	SPENDING_RATE_OF_CHG_M7 = CASE WHEN SPEND_AMT_M7-SPEND_AMT_M8 > 0 THEN (SPEND_AMT_M7-SPEND_AMT_M8)/MAX_MONTHLY_SPEND
								   WHEN SPEND_AMT_M8 > 0 THEN (SPEND_AMT_M7-SPEND_AMT_M8)/MAX_MONTHLY_SPEND
								   ELSE 0 END,
	SPENDING_RATE_OF_CHG_M8 = CASE WHEN SPEND_AMT_M8-SPEND_AMT_M9 > 0 THEN (SPEND_AMT_M8-SPEND_AMT_M9)/MAX_MONTHLY_SPEND
								   WHEN SPEND_AMT_M9 > 0 THEN (SPEND_AMT_M8-SPEND_AMT_M9)/MAX_MONTHLY_SPEND
								   ELSE 0 END,
	SPENDING_RATE_OF_CHG_M9 = CASE WHEN SPEND_AMT_M9-SPEND_AMT_M10 > 0 THEN (SPEND_AMT_M9-SPEND_AMT_M10)/MAX_MONTHLY_SPEND
								   WHEN SPEND_AMT_M10 > 0 THEN (SPEND_AMT_M9-SPEND_AMT_M10)/MAX_MONTHLY_SPEND
								   ELSE 0 END,
	SPENDING_RATE_OF_CHG_M10 = CASE WHEN SPEND_AMT_M10-SPEND_AMT_M11 > 0 THEN (SPEND_AMT_M10-SPEND_AMT_M11)/MAX_MONTHLY_SPEND
								   WHEN SPEND_AMT_M11 > 0 THEN (SPEND_AMT_M10-SPEND_AMT_M11)/MAX_MONTHLY_SPEND
								   ELSE 0 END
;

UPDATE VPM_DATA.PM_CUST_SVOC_MONTHLY_CREDIT_SPEND
SET AVG_RATE_OF_CHG = (SELECT AVG(col)
					   FROM (VALUES (SPENDING_RATE_OF_CHG_M0),(SPENDING_RATE_OF_CHG_M1),(SPENDING_RATE_OF_CHG_M2),
									(SPENDING_RATE_OF_CHG_M3),(SPENDING_RATE_OF_CHG_M4),(SPENDING_RATE_OF_CHG_M5),
									(SPENDING_RATE_OF_CHG_M6),(SPENDING_RATE_OF_CHG_M7),(SPENDING_RATE_OF_CHG_M8),
									(SPENDING_RATE_OF_CHG_M9),(SPENDING_RATE_OF_CHG_M10)) T (col))
	,WEIGHTED_RECENCY_RATE_OF_CHG = SPENDING_RATE_OF_CHG_M0*(11.0/66.0)
								   +SPENDING_RATE_OF_CHG_M1*(10.0/66.0)
								   +SPENDING_RATE_OF_CHG_M2*(9.0/66.0)
								   +SPENDING_RATE_OF_CHG_M3*(8.0/66.0)
								   +SPENDING_RATE_OF_CHG_M4*(7.0/66.0)
								   +SPENDING_RATE_OF_CHG_M5*(6.0/66.0)
								   +SPENDING_RATE_OF_CHG_M6*(5.0/66.0)
								   +SPENDING_RATE_OF_CHG_M7*(4.0/66.0)
								   +SPENDING_RATE_OF_CHG_M8*(3.0/66.0)
								   +SPENDING_RATE_OF_CHG_M9*(2.0/66.0)
								   +SPENDING_RATE_OF_CHG_M10*(1.0/66.0)
;
UPDATE VPM_DATA.SALESSKU_LOG_TIME SET end_time = now() WHERE task = 'Step1.3.4 Estimated Income from CC - GCS (Edited to 1 Yr)' AND EXTRACT(MONTH FROM start_time) = EXTRACT(MONTH FROM now());
/*
select top 100 WEIGHTED_RECENCY_RATE_OF_CHG from PM_CUST_SVOC_MONTHLY_CREDIT_SPEND
*/