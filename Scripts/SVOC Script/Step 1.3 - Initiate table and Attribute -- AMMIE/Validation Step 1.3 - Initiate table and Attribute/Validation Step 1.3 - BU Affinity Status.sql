SELECT 'PM_SVOC_AFFINITY' AS TABLE_NAME
,'CDS_ACTIVE_STATUS' AS SEGMENT
,COALESCE(CASE WHEN CDS_INACTIVE_DAYS <= 90 THEN 'ACTIVE 3M' WHEN CDS_INACTIVE_DAYS <= 180 THEN 'INACTIVE 4-6M' 				
			 WHEN CDS_INACTIVE_DAYS <= 365 THEN 'INACTIVE 7-12M' ELSE 'NEVER' END,'N/A') AS SEGMENT_VALUE
,COUNT(DISTINCT CUSTOMER_ID) AS TOTAL_CUST
FROM VPM_DATA.PM_SVOC_AFFINITY
GROUP BY COALESCE(CASE WHEN CDS_INACTIVE_DAYS <= 90 THEN 'ACTIVE 3M' WHEN CDS_INACTIVE_DAYS <= 180 THEN 'INACTIVE 4-6M' 				
			 WHEN CDS_INACTIVE_DAYS <= 365 THEN 'INACTIVE 7-12M' ELSE 'NEVER' END,'N/A')
UNION ALL
SELECT 'PM_SVOC_AFFINITY' AS TABLE_NAME
,'MUJI_ACTIVE_STATUS' AS SEGMENT
,COALESCE(CASE WHEN MUJI_INACTIVE_DAYS <= 90 THEN 'ACTIVE 3M' WHEN MUJI_INACTIVE_DAYS <= 180 THEN 'INACTIVE 4-6M' 				
			 WHEN MUJI_INACTIVE_DAYS <= 365 THEN 'INACTIVE 7-12M' ELSE 'NEVER' END,'N/A') AS SEGMENT_VALUE
,COUNT(DISTINCT CUSTOMER_ID) AS TOTAL_CUST
FROM VPM_DATA.PM_SVOC_AFFINITY
GROUP BY COALESCE(CASE WHEN MUJI_INACTIVE_DAYS <= 90 THEN 'ACTIVE 3M' WHEN MUJI_INACTIVE_DAYS <= 180 THEN 'INACTIVE 4-6M' 				
			 WHEN MUJI_INACTIVE_DAYS <= 365 THEN 'INACTIVE 7-12M' ELSE 'NEVER' END,'N/A')
UNION ALL
SELECT 'PM_SVOC_AFFINITY' AS TABLE_NAME
,'RBS_ACTIVE_STATUS' AS SEGMENT
,COALESCE(CASE WHEN RBS_INACTIVE_DAYS <= 90 THEN 'ACTIVE 3M' WHEN RBS_INACTIVE_DAYS <= 180 THEN 'INACTIVE 4-6M' 				
			 WHEN RBS_INACTIVE_DAYS <= 365 THEN 'INACTIVE 7-12M' ELSE 'NEVER' END,'N/A') AS SEGMENT_VALUE
,COUNT(DISTINCT CUSTOMER_ID) AS TOTAL_CUST
FROM VPM_DATA.PM_SVOC_AFFINITY
GROUP BY COALESCE(CASE WHEN RBS_INACTIVE_DAYS <= 90 THEN 'ACTIVE 3M' WHEN RBS_INACTIVE_DAYS <= 180 THEN 'INACTIVE 4-6M' 				
			 WHEN RBS_INACTIVE_DAYS <= 365 THEN 'INACTIVE 7-12M' ELSE 'NEVER' END,'N/A')
UNION ALL
SELECT 'PM_SVOC_AFFINITY' AS TABLE_NAME
,'B2S_ACTIVE_STATUS' AS SEGMENT
,COALESCE(CASE WHEN B2S_INACTIVE_DAYS <= 90 THEN 'ACTIVE 3M' WHEN B2S_INACTIVE_DAYS <= 180 THEN 'INACTIVE 4-6M' 				
			 WHEN B2S_INACTIVE_DAYS <= 365 THEN 'INACTIVE 7-12M' ELSE 'NEVER' END,'N/A') AS SEGMENT_VALUE
,COUNT(DISTINCT CUSTOMER_ID) AS TOTAL_CUST
FROM VPM_DATA.PM_SVOC_AFFINITY
GROUP BY COALESCE(CASE WHEN B2S_INACTIVE_DAYS <= 90 THEN 'ACTIVE 3M' WHEN B2S_INACTIVE_DAYS <= 180 THEN 'INACTIVE 4-6M' 				
			 WHEN B2S_INACTIVE_DAYS <= 365 THEN 'INACTIVE 7-12M' ELSE 'NEVER' END,'N/A')
UNION ALL
SELECT 'PM_SVOC_AFFINITY' AS TABLE_NAME
,'SSP_ACTIVE_STATUS' AS SEGMENT
,COALESCE(CASE WHEN SSP_INACTIVE_DAYS <= 90 THEN 'ACTIVE 3M' WHEN SSP_INACTIVE_DAYS <= 180 THEN 'INACTIVE 4-6M' 				
			 WHEN SSP_INACTIVE_DAYS <= 365 THEN 'INACTIVE 7-12M' ELSE 'NEVER' END,'N/A') AS SEGMENT_VALUE
,COUNT(DISTINCT CUSTOMER_ID) AS TOTAL_CUST
FROM VPM_DATA.PM_SVOC_AFFINITY
GROUP BY COALESCE(CASE WHEN SSP_INACTIVE_DAYS <= 90 THEN 'ACTIVE 3M' WHEN SSP_INACTIVE_DAYS <= 180 THEN 'INACTIVE 4-6M' 				
			 WHEN SSP_INACTIVE_DAYS <= 365 THEN 'INACTIVE 7-12M' ELSE 'NEVER' END,'N/A')
UNION ALL
SELECT 'PM_SVOC_AFFINITY' AS TABLE_NAME
,'PWB_ACTIVE_STATUS' AS SEGMENT
,COALESCE(CASE WHEN PWB_INACTIVE_DAYS <= 90 THEN 'ACTIVE 3M' WHEN PWB_INACTIVE_DAYS <= 180 THEN 'INACTIVE 4-6M' 				
			 WHEN PWB_INACTIVE_DAYS <= 365 THEN 'INACTIVE 7-12M' ELSE 'NEVER' END,'N/A') AS SEGMENT_VALUE
,COUNT(DISTINCT CUSTOMER_ID) AS TOTAL_CUST
FROM VPM_DATA.PM_SVOC_AFFINITY
GROUP BY COALESCE(CASE WHEN PWB_INACTIVE_DAYS <= 90 THEN 'ACTIVE 3M' WHEN PWB_INACTIVE_DAYS <= 180 THEN 'INACTIVE 4-6M' 				
			 WHEN PWB_INACTIVE_DAYS <= 365 THEN 'INACTIVE 7-12M' ELSE 'NEVER' END,'N/A')
UNION ALL
SELECT 'PM_SVOC_AFFINITY' AS TABLE_NAME
,'ODP_ACTIVE_STATUS' AS SEGMENT
,COALESCE(CASE WHEN ODP_INACTIVE_DAYS <= 90 THEN 'ACTIVE 3M' WHEN ODP_INACTIVE_DAYS <= 180 THEN 'INACTIVE 4-6M' 				
			 WHEN ODP_INACTIVE_DAYS <= 365 THEN 'INACTIVE 7-12M' ELSE 'NEVER' END,'N/A') AS SEGMENT_VALUE
,COUNT(DISTINCT CUSTOMER_ID) AS TOTAL_CUST
FROM VPM_DATA.PM_SVOC_AFFINITY
GROUP BY COALESCE(CASE WHEN ODP_INACTIVE_DAYS <= 90 THEN 'ACTIVE 3M' WHEN ODP_INACTIVE_DAYS <= 180 THEN 'INACTIVE 4-6M' 				
			 WHEN ODP_INACTIVE_DAYS <= 365 THEN 'INACTIVE 7-12M' ELSE 'NEVER' END,'N/A')
UNION ALL
SELECT 'PM_SVOC_AFFINITY' AS TABLE_NAME
,'CMG_ACTIVE_STATUS' AS SEGMENT
,COALESCE(CASE WHEN CMG_INACTIVE_DAYS <= 90 THEN 'ACTIVE 3M' WHEN CMG_INACTIVE_DAYS <= 180 THEN 'INACTIVE 4-6M' 				
			 WHEN CMG_INACTIVE_DAYS <= 365 THEN 'INACTIVE 7-12M' ELSE 'NEVER' END,'N/A') AS SEGMENT_VALUE
,COUNT(DISTINCT CUSTOMER_ID) AS TOTAL_CUST
FROM VPM_DATA.PM_SVOC_AFFINITY
GROUP BY COALESCE(CASE WHEN CMG_INACTIVE_DAYS <= 90 THEN 'ACTIVE 3M' WHEN CMG_INACTIVE_DAYS <= 180 THEN 'INACTIVE 4-6M' 				
			 WHEN CMG_INACTIVE_DAYS <= 365 THEN 'INACTIVE 7-12M' ELSE 'NEVER' END,'N/A')
UNION ALL
SELECT 'PM_SVOC_AFFINITY' AS TABLE_NAME
,'CFM_ACTIVE_STATUS' AS SEGMENT
,COALESCE(CASE WHEN CFM_INACTIVE_DAYS <= 90 THEN 'ACTIVE 3M' WHEN CFM_INACTIVE_DAYS <= 180 THEN 'INACTIVE 4-6M' 				
			 WHEN CFM_INACTIVE_DAYS <= 365 THEN 'INACTIVE 7-12M' ELSE 'NEVER' END,'N/A') AS SEGMENT_VALUE
,COUNT(DISTINCT CUSTOMER_ID) AS TOTAL_CUST
FROM VPM_DATA.PM_SVOC_AFFINITY
GROUP BY COALESCE(CASE WHEN CFM_INACTIVE_DAYS <= 90 THEN 'ACTIVE 3M' WHEN CFM_INACTIVE_DAYS <= 180 THEN 'INACTIVE 4-6M' 				
			 WHEN CFM_INACTIVE_DAYS <= 365 THEN 'INACTIVE 7-12M' ELSE 'NEVER' END,'N/A')
UNION ALL
SELECT 'PM_SVOC_AFFINITY' AS TABLE_NAME
,'TOPS_ACTIVE_STATUS' AS SEGMENT
,COALESCE(CASE WHEN TOPS_INACTIVE_DAYS <= 90 THEN 'ACTIVE 3M' WHEN TOPS_INACTIVE_DAYS <= 180 THEN 'INACTIVE 4-6M' 				
			 WHEN TOPS_INACTIVE_DAYS <= 365 THEN 'INACTIVE 7-12M' ELSE 'NEVER' END,'N/A') AS SEGMENT_VALUE
,COUNT(DISTINCT CUSTOMER_ID) AS TOTAL_CUST
FROM VPM_DATA.PM_SVOC_AFFINITY
GROUP BY COALESCE(CASE WHEN TOPS_INACTIVE_DAYS <= 90 THEN 'ACTIVE 3M' WHEN TOPS_INACTIVE_DAYS <= 180 THEN 'INACTIVE 4-6M' 				
			 WHEN TOPS_INACTIVE_DAYS <= 365 THEN 'INACTIVE 7-12M' ELSE 'NEVER' END,'N/A')
UNION ALL
SELECT 'PM_SVOC_AFFINITY' AS TABLE_NAME
,'TWD_ACTIVE_STATUS' AS SEGMENT
,COALESCE(CASE WHEN TWD_INACTIVE_DAYS <= 90 THEN 'ACTIVE 3M' WHEN TWD_INACTIVE_DAYS <= 180 THEN 'INACTIVE 4-6M' 				
			 WHEN TWD_INACTIVE_DAYS <= 365 THEN 'INACTIVE 7-12M' ELSE 'NEVER' END,'N/A') AS SEGMENT_VALUE
,COUNT(DISTINCT CUSTOMER_ID) AS TOTAL_CUST
FROM VPM_DATA.PM_SVOC_AFFINITY
GROUP BY COALESCE(CASE WHEN TWD_INACTIVE_DAYS <= 90 THEN 'ACTIVE 3M' WHEN TWD_INACTIVE_DAYS <= 180 THEN 'INACTIVE 4-6M' 				
			 WHEN TWD_INACTIVE_DAYS <= 365 THEN 'INACTIVE 7-12M' ELSE 'NEVER' END,'N/A')
UNION ALL
SELECT 'PM_SVOC_AFFINITY' AS TABLE_NAME
,'HWS_ACTIVE_STATUS' AS SEGMENT
,COALESCE(CASE WHEN HWS_INACTIVE_DAYS <= 90 THEN 'ACTIVE 3M' WHEN HWS_INACTIVE_DAYS <= 180 THEN 'INACTIVE 4-6M' 				
			 WHEN HWS_INACTIVE_DAYS <= 365 THEN 'INACTIVE 7-12M' ELSE 'NEVER' END,'N/A') AS SEGMENT_VALUE
,COUNT(DISTINCT CUSTOMER_ID) AS TOTAL_CUST
FROM VPM_DATA.PM_SVOC_AFFINITY
GROUP BY COALESCE(CASE WHEN HWS_INACTIVE_DAYS <= 90 THEN 'ACTIVE 3M' WHEN HWS_INACTIVE_DAYS <= 180 THEN 'INACTIVE 4-6M' 				
			 WHEN HWS_INACTIVE_DAYS <= 365 THEN 'INACTIVE 7-12M' ELSE 'NEVER' END,'N/A')
UNION ALL
SELECT 'PM_SVOC_AFFINITY' AS TABLE_NAME
,'TARGET_GROUP_ALL' AS SEGMENT
,COALESCE(TARGET_GROUP_ALL,'N/A') AS SEGMENT_VALUE
,COUNT(DISTINCT CUSTOMER_ID) AS TOTAL_CUST
FROM VPM_DATA.PM_SVOC_AFFINITY
GROUP BY COALESCE(TARGET_GROUP_ALL,'N/A')
--UNION ALL
--SELECT 'PM_SVOC_AFFINITY' AS TABLE_NAME
--,'' AS SEGMENT
--, AS SEGMENT_VALUE
--,COUNT(DISTINCT CUSTOMER_ID) AS TOTAL_CUST
--FROM VPM_DATA.PM_SVOC_AFFINITY
--GROUP BY 
