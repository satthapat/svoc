INSERT INTO VPM_DATA.SALESSKU_LOG_TIME(task,start_time) VALUES ('LOCATION Step 1_Create BUs Branches Location Ref',NOW());

UPDATE VPM_DATA.CG_LOCATION_MASTER
SET PROVINCE_ENG = CASE WHEN PROVINCE_ENG LIKE 'Songkha' THEN 'Songkhla'
		                WHEN PROVINCE_ENG = 'Chonburi' THEN 'Chon Buri'
			            WHEN PROVINCE_ENG = 'Lopburi' THEN 'Lop Buri'
			            WHEN PROVINCE_ENG = 'Phang Nga' THEN 'Phangnga'
		  	            WHEN PROVINCE_ENG = 'Samut Prakan' THEN 'Samut Prakarn'
			            WHEN PROVINCE_ENG = 'Prachinburi' THEN 'Prachin Buri'
			            WHEN PROVINCE_ENG = 'Nong Bua Lamphu' THEN 'Nong Bua Lam Phu'
			            WHEN PROVINCE_ENG = 'Angthong' THEN 'Ang Thong'
			            WHEN Province_ENG = 'Samut Prakan' THEN 'Samut Prakarn'
		                WHEN Province_ENG = 'กรุงเทพมหานคร' THEN 'Bangkok'
		                WHEN Province_ENG = 'บางรัก' THEN 'Bangkok'
	                    ELSE PROVINCE_ENG END; 

/** CREATE BRANCH LOCATION LOOKUP TABLE **/
DROP TABLE IF EXISTS VPM_Backup.GEO_BU_LOCATION_MASTER;

CREATE TABLE VPM_Backup.GEO_BU_LOCATION_MASTER AS
SELECT A.BUID
	,A.BUCode
	,A.BUEnglishname
	,B.BranchID
	,B.BranchCode
	,C.branch_name AS BranchEnglishname
	,C.LocationName
	,C.PROVINCE
	,C.DISTRICT
	,C.PROVINCE_ENG
	,C.STORE_TYPE
	,C.BUILDING
	,C.LocationName AS BUILDING_NAME
	,C.Centrality_Region
	,C.Centrality_Location
	,D.google_keyword AS GOOGLE_KEYWORD
	,CASE WHEN COALESCE(B.Latitude,0) <> 0 THEN B.Latitude ELSE COALESCE(REPLACE(D.google_lat,'',NULL)::FLOAT,0) END AS Latitude
	,CASE WHEN COALESCE(B.Longitude,0) <> 0 THEN B.Longitude ELSE COALESCE(REPLACE(D.google_lng,'',NULL)::FLOAT,0) END AS Longitude
	,D.postal AS POSTAL_CODE
	,E.REGION1 AS REGION1
	,CASE WHEN COALESCE(UPPER(C.PROVINCE_ENG),'') IN ('','ONLINE','EVENT','OTHER') THEN E.REGION2 ELSE C.PROVINCE_ENG END AS REGION2
	,CAST(NULL AS INT) AS MALL_BUID
	,CAST(NULL AS VARCHAR(4)) AS MALL_BUCODE
	,CAST(NULL AS VARCHAR(100)) AS MALL_BRANCH_NAME
	,CAST(NULL AS INT) AS MALL_BRANCHID
	,CAST(NULL AS float) AS PCT_OCCURANCE
	,CAST(NULL AS float) AS MALL_LATITUDE
	,CAST(NULL AS float) AS MALL_LONGITUDE
	,CAST(NULL AS VARCHAR(50)) AS MALL_POSTAL_CODE
	,CAST(NULL AS VARCHAR(50)) AS MALL_REGION1
	,CAST(NULL AS VARCHAR(50)) AS MALL_REGION2
  FROM Analysis_Data.BUSinessUnit_LT A
  LEFT JOIN Analysis_Data.Branch_LT B ON A.BUID::VARCHAR(4) = B.BUID::VARCHAR(4)
  LEFT JOIN VPM_DATA.CG_LOCATION_MASTER C ON B.BUID::VARCHAR(4) = C.BUID::VARCHAR(4) and B.BranchID::VARCHAR(5) = C.BranchID::VARCHAR(5) /* From K.BUM */													
  LEFT JOIN VPM_Backup.BU_Import D ON B.BUID::VARCHAR(4) = D.BUID::VARCHAR(4) 
  										AND B.BranchID::VARCHAR(5) = D.BranchID::VARCHAR(5) /* From Google API Cleansing (One-Time) */
  LEFT JOIN (SELECT DISTINCT "ZIP" AS ZIP,"REGION1"AS REGION1,
					CASE WHEN "REGION2" = 'Chonburi' THEN 'Chon Buri'
						 WHEN "REGION2" = 'Lopburi' THEN 'Lop Buri'
						 WHEN "REGION2" = 'Phang Nga' THEN 'Phangnga'
						 WHEN "REGION2" = 'Samut Prakan' THEN 'Samut Prakarn'
						 WHEN "REGION2" = 'Prachinburi' THEN 'Prachin Buri'
						 WHEN "REGION2" = 'Nong Bua Lamphu' THEN 'Nong Bua Lam Phu'
					ELSE "REGION2" END AS REGION2
			 FROM VPM_DATA.GeoPC) E ON D.postal = E.ZIP;

UPDATE VPM_Backup.GEO_BU_LOCATION_MASTER
SET REGION1 = T."REGION1"
FROM (SELECT DISTINCT "REGION1",
					CASE WHEN "REGION2" = 'Chonburi' THEN 'Chon Buri'
						 WHEN "REGION2" = 'Lopburi' THEN 'Lop Buri'
						 WHEN "REGION2" = 'Phang Nga' THEN 'Phangnga'
						 WHEN "REGION2" = 'Samut Prakan' THEN 'Samut Prakarn'
						 WHEN "REGION2" = 'Prachinburi' THEN 'Prachin Buri'
						 WHEN "REGION2" = 'Nong Bua Lamphu' THEN 'Nong Bua Lam Phu'
					ELSE "REGION2" END
	  FROM VPM_DATA.GeoPC) T
WHERE VPM_Backup.GEO_BU_LOCATION_MASTER.REGION2 = T."REGION2";

/*** CLEAN LOCATION MANUAL ***/
DROP TABLE IF EXISTS VPM_DATA.CLEAN_MANUAL;
SELECT A.BUID,A.BUCODE,A.BUENGLISHNAME,A.BRANCHID,A.BRANCHCODE,A.BRANCHENGLISHNAME,A.LOCATIONNAME,A.GOOGLE_KEYWORD,A.LATITUDE,A.LONGITUDE,A.POSTAL_CODE
	  ,CASE WHEN A.REGION1 = 'Cenrtral' THEN 'Central'
			WHEN A.REGION2 = 'Chonburi' THEN 'East'
	   ELSE A.REGION1 END AS REGION1
	  ,CASE WHEN A.REGION2 = 'Chonburi' THEN 'Chon Buri'
			WHEN A.REGION2 = 'Lopburi' THEN 'Lop Buri'
			WHEN A.REGION2 = 'Phang Nga' THEN 'Phangnga'
			WHEN A.REGION2 = 'Samut Prakan' THEN 'Samut Prakarn'
			WHEN A.REGION2 = 'Prachinburi' THEN 'Prachin Buri'
			WHEN A.REGION2 = 'Nong Bua Lamphu' THEN 'Nong Bua Lam Phu'
	   ELSE A.REGION2 END AS REGION2
INTO VPM_DATA.CLEAN_MANUAL
FROM VPM_Backup.BU_Master_Location_20190205 a /* From Thaiwatsadu Manual Tagging */
left join VPM_Backup.GEO_BU_LOCATION_MASTER b 
on a.BUID::VARCHAR(10) = b.BUID::VARCHAR(10) 
	and a.BranchID::VARCHAR(10) = b.BranchID::VARCHAR(10)
WHERE (b.POSTAL_CODE = '' or b.POSTAL_CODE is null)
and COALESCE(a.POSTAL_CODE,'')::VARCHAR(10) <> '' and COALESCE(a.REGION1,'')::VARCHAR(10) <> ''
and a.POSTAL_CODE <> 'NULL';

UPDATE VPM_Backup.GEO_BU_LOCATION_MASTER
SET GOOGLE_KEYWORD = CASE WHEN T.GOOGLE_KEYWORD <> '' THEN SUBSTRING(T.GOOGLE_KEYWORD,1,50) ELSE '' END,
	Latitude = CASE WHEN T.Latitude <> '' THEN T.Latitude::float ELSE NULL END,
	Longitude = CASE WHEN T.Longitude <> '' THEN T.Longitude::float ELSE NULL END,
	POSTAL_CODE = CAST(T.POSTAL_CODE AS varchar(50)),
	REGION1 = CAST(T.REGION1 AS varchar(60)),
	REGION2 = CAST(T.REGION2 AS varchar(60))
FROM VPM_DATA.CLEAN_MANUAL T
WHERE VPM_Backup.GEO_BU_LOCATION_MASTER.BranchID::VARCHAR(10) = T.BranchID::VARCHAR(10)
and VPM_Backup.GEO_BU_LOCATION_MASTER.BUCode::VARCHAR(10) = T.BUCode::VARCHAR(10);

/*** CLEAN CFM BRANCH PROVINCE FROM EXCEL FILE ***/
DROP TABLE IF EXISTS VPM_DATA.CLEAN_CFM;
SELECT A.BUID, A.BRANCHID, A.POSTAL_CODE, A.REGION1
	  ,CASE WHEN A.REGION2 = 'Chonburi' THEN 'Chon Buri'
			WHEN A.REGION2 = 'Lopburi' THEN 'Lop Buri'
			WHEN A.REGION2 = 'Phang Nga' THEN 'Phangnga'
			WHEN A.REGION2 = 'Samut Prakan' THEN 'Samut Prakarn'
			WHEN A.REGION2 = 'Prachinburi' THEN 'Prachin Buri'
			WHEN A.REGION2 = 'Nong Bua Lamphu' THEN 'Nong Bua Lam Phu'
	   ELSE A.REGION2 END AS REGION2
INTO VPM_DATA.CLEAN_CFM
FROM VPM_Backup.BU_CFM_LOCATION_20190305 a /* File from FAMILYMART */
left join VPM_Backup.GEO_BU_LOCATION_MASTER b on a.BUID=b.BUID and a.BranchID=b.BranchID
WHERE (b.POSTAL_CODE = '' or b.POSTAL_CODE is null)
and a.REGION2 IS NOT NULL;

UPDATE VPM_Backup.GEO_BU_LOCATION_MASTER
SET POSTAL_CODE = T.POSTAL_CODE,
	REGION1 = T.REGION1,
	REGION2 = T.REGION2
FROM VPM_DATA.CLEAN_CFM T
WHERE VPM_Backup.GEO_BU_LOCATION_MASTER.BranchID = T.BranchID
and VPM_Backup.GEO_BU_LOCATION_MASTER.BUID = T.BUID;

UPDATE VPM_DATA.SALESSKU_LOG_TIME SET end_time = now() WHERE task = 'LOCATION Step 1_Create BUs Branches Location Ref' AND EXTRACT(MONTH FROM start_time) = EXTRACT(MONTH FROM now());

/*** IMPORT GEOLOCATION FROM PYTHON ***/
/*
UPDATE VPM_DATA.BU_Import
SET postal = 10330
WHERE LocationName like '%Central%Embas%' AND postal = ''

UPDATE VPM_DATA.BU_Import
SET postal = 10330
WHERE LocationName like '%ZEN%' AND postal = ''

UPDATE VPM_DATA.BU_Import
SET BUID = 36
WHERE (BranchEnglishname like '%MJT%'
OR BranchEnglishname like '%MUJI%')
AND BUID = 1     
*/

/*

insert into VPM_DATA.BU_Import (IDX,BUID,BUCode,BUEnglishName,BranchEnglishname,
										  BranchID,BranchCode,postal,google_lat,google_lng,lat_lng,lat,lng)
values ('',1,'CDS','Central Department Store','MSL Clearance Chidlom',
		9322,'019070','10330',13.7440004348755,100.544998168945,'13.7440004348755 100.544998168945',13.7440004348755,100.544998168945)
*/

/** MANUAL ADD NEW BRANCH GEO LOCATION **/
/*


insert into VPM_DATA.BU_Import(IDX,BUID,BUCode,BUEnglishName,BranchID,BranchCode,google_lat,google_lng,lat_lng,lat,lng)
values ('',176,'TWD','Thai Watsadu',8790,'60868',13.9371055,100.7533776,'13.9371055 100.7533776',13.9371055,100.7533776)

UPDATE VPM_DATA.BU_Import
SET return_lat = 13.9371055,
  return_lng = 100.7533776,
  GOOGLE_KEYWORD = 'ไทวัสดุ ลำลูกกา',
  google_lat = 13.9371055,
  google_lng = 100.7533776,
  lat = 13.9371055,
  lng = 100.7533776,
  lat_lng = '13.9371055 100.7533776',
  postal = '12150'
WHERE BranchID = '8790'

*/

/*
insert into VPM_DATA.GeoPC
values ('TH','EN',1,'TH-15','North','Chiang Mai','Suthep','','50200','Pra Singh Sri Phum','','',18.924,98.909,'Asia/Bangkok','UTC+7','N')

insert into VPM_DATA.GeoPC
values ('TH','EN',1,'TH-15','West','Phetchaburi','Cha Am','','76120','Cha Am','','',12.814,99.620,'Asia/Bangkok','UTC+7','N')
*/

/** RENAME CFM REGION (From Excel file) to be The Same as GEOPC **/
/*
ALTER TABLE VPM_DATA.BU_CFM_LOCATION_20190305
ADD REGION2 VARCHAR(50), REGION1 VARCHAR(50), POSTAL_CODE VARCHAR(5)

UPDATE VPM_DATA.BU_CFM_LOCATION_20190305
SET REGION2 = CASE WHEN Province_ENG = 'Chon Buri' THEN 'Chonburi' 
					WHEN Province_ENG = 'Lop Buri' THEN 'Lopburi' 
					WHEN Province_ENG = 'Phang-nga' THEN 'Phang Nga' 
					WHEN Province_ENG = 'Phangnga' THEN 'Phang Nga' 
					WHEN Province_ENG = 'Samut Prakarn' THEN 'Samut Prakan' 
					WHEN Province_ENG = 'Singburi' THEN 'Sing Buri' 
					ELSE Province_ENG END

UPDATE VPM_DATA.BU_CFM_LOCATION_20190305
SET REGION1 = T.REGION1
FROM (SELECT DISTINCT REGION1,REGION2   
	  FROM VPM_DATA.GeoPC) T 
WHERE VPM_DATA.BU_CFM_LOCATION_20190305.REGION2 = T.REGION2

UPDATE VPM_DATA.BU_CFM_LOCATION_20190305
SET POSTAL_CODE = REPLACE(REPLACE(SUBSTRING(Address Field T,LEN(Address Field T)-4,5),'/',''),'"','')
WHERE LEN(RTRIM(LTRIM(REPLACE(REPLACE(SUBSTRING(Address Field T,LEN(Address Field T)-4,5),'/',''),'"','')))) = 5

*/




