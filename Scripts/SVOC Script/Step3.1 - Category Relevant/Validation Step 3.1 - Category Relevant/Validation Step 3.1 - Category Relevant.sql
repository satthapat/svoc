SELECT 'P_PM_CAT_RELEVANCE_SCORE_ALL' AS TABLE_NAME
,'FASHION_HIGH_RELEVANCE_SEGMENT' AS SEGMENT
,COALESCE(FASHION_HIGH_RELEVANCE_SEGMENT,'N/A') AS SEGMENT_VALUE
,COUNT(DISTINCT CUSTOMER_ID) AS TOTAL_CUST
FROM VPM_DATA.P_PM_CAT_RELEVANCE_SCORE_ALL
GROUP BY COALESCE(FASHION_HIGH_RELEVANCE_SEGMENT,'N/A')
UNION ALL
SELECT 'P_PM_CAT_RELEVANCE_SCORE_ALL' AS TABLE_NAME
,'BEAUTY_HIGH_RELEVANCE_SEGMENT' AS SEGMENT
,COALESCE(BEAUTY_HIGH_RELEVANCE_SEGMENT,'N/A') AS SEGMENT_VALUE
,COUNT(DISTINCT CUSTOMER_ID) AS TOTAL_CUST
FROM VPM_DATA.P_PM_CAT_RELEVANCE_SCORE_ALL
GROUP BY COALESCE(BEAUTY_HIGH_RELEVANCE_SEGMENT,'N/A')
UNION ALL
SELECT 'P_PM_CAT_RELEVANCE_SCORE_ALL' AS TABLE_NAME
,'GROCERY_HIGH_RELEVANCE_SEGMENT' AS SEGMENT
,COALESCE(GROCERY_HIGH_RELEVANCE_SEGMENT,'N/A') AS SEGMENT_VALUE
,COUNT(DISTINCT CUSTOMER_ID) AS TOTAL_CUST
FROM VPM_DATA.P_PM_CAT_RELEVANCE_SCORE_ALL
GROUP BY COALESCE(GROCERY_HIGH_RELEVANCE_SEGMENT,'N/A')
UNION ALL
SELECT 'P_PM_CAT_RELEVANCE_SCORE_ALL' AS TABLE_NAME
,'FOOD_GROCERY_HIGH_RELEVANCE_SEGMENT' AS SEGMENT
,COALESCE(FOOD_GROCERY_HIGH_RELEVANCE_SEGMENT,'N/A') AS SEGMENT_VALUE
,COUNT(DISTINCT CUSTOMER_ID) AS TOTAL_CUST
FROM VPM_DATA.P_PM_CAT_RELEVANCE_SCORE_ALL
GROUP BY COALESCE(FOOD_GROCERY_HIGH_RELEVANCE_SEGMENT,'N/A')
UNION ALL
SELECT 'P_PM_CAT_RELEVANCE_SCORE_ALL' AS TABLE_NAME
,'HOME_GROCERY_HIGH_RELEVANCE_SEGMENT' AS SEGMENT
,COALESCE(HOME_GROCERY_HIGH_RELEVANCE_SEGMENT,'N/A') AS SEGMENT_VALUE
,COUNT(DISTINCT CUSTOMER_ID) AS TOTAL_CUST
FROM VPM_DATA.P_PM_CAT_RELEVANCE_SCORE_ALL
GROUP BY COALESCE(HOME_GROCERY_HIGH_RELEVANCE_SEGMENT,'N/A')
UNION ALL
SELECT 'P_PM_CAT_RELEVANCE_SCORE_ALL' AS TABLE_NAME
,'HBA_HIGH_RELEVANCE_SEGMENT' AS SEGMENT
,COALESCE(HBA_HIGH_RELEVANCE_SEGMENT,'N/A') AS SEGMENT_VALUE
,COUNT(DISTINCT CUSTOMER_ID) AS TOTAL_CUST
FROM VPM_DATA.P_PM_CAT_RELEVANCE_SCORE_ALL
GROUP BY COALESCE(HBA_HIGH_RELEVANCE_SEGMENT,'N/A')
UNION ALL
SELECT 'P_PM_CAT_RELEVANCE_SCORE_ALL' AS TABLE_NAME
,'HOME_HIGH_RELEVANCE_SEGMENT' AS SEGMENT
,COALESCE(HOME_HIGH_RELEVANCE_SEGMENT,'N/A') AS SEGMENT_VALUE
,COUNT(DISTINCT CUSTOMER_ID) AS TOTAL_CUST
FROM VPM_DATA.P_PM_CAT_RELEVANCE_SCORE_ALL
GROUP BY COALESCE(HOME_HIGH_RELEVANCE_SEGMENT,'N/A')
UNION ALL
SELECT 'P_PM_CAT_RELEVANCE_SCORE_ALL' AS TABLE_NAME
,'ELEC_HIGH_RELEVANCE_SEGMENT' AS SEGMENT
,COALESCE(ELEC_HIGH_RELEVANCE_SEGMENT,'N/A') AS SEGMENT_VALUE
,COUNT(DISTINCT CUSTOMER_ID) AS TOTAL_CUST
FROM VPM_DATA.P_PM_CAT_RELEVANCE_SCORE_ALL
GROUP BY COALESCE(ELEC_HIGH_RELEVANCE_SEGMENT,'N/A')
UNION ALL
SELECT 'P_PM_CAT_RELEVANCE_SCORE_ALL' AS TABLE_NAME
,'HOME_NON_RELEVANCE_SEGMENT' AS SEGMENT
,COALESCE(HOME_NON_RELEVANCE_SEGMENT,'N/A') AS SEGMENT_VALUE
,COUNT(DISTINCT CUSTOMER_ID) AS TOTAL_CUST
FROM VPM_DATA.P_PM_CAT_RELEVANCE_SCORE_ALL
GROUP BY COALESCE(HOME_NON_RELEVANCE_SEGMENT,'N/A')
UNION ALL
SELECT 'P_PM_CAT_RELEVANCE_SCORE_ALL' AS TABLE_NAME
,'HOME_ELEC_RELEVANCE_SEGMENT' AS SEGMENT
,COALESCE(HOME_ELEC_RELEVANCE_SEGMENT,'N/A') AS SEGMENT_VALUE
,COUNT(DISTINCT CUSTOMER_ID) AS TOTAL_CUST
FROM VPM_DATA.P_PM_CAT_RELEVANCE_SCORE_ALL
GROUP BY COALESCE(HOME_ELEC_RELEVANCE_SEGMENT,'N/A')
UNION ALL
SELECT 'P_PM_CAT_RELEVANCE_SCORE_ALL' AS TABLE_NAME
,'ELEC_GAD_RELEVANCE_SEGMENT' AS SEGMENT
,COALESCE(ELEC_GAD_RELEVANCE_SEGMENT,'N/A') AS SEGMENT_VALUE
,COUNT(DISTINCT CUSTOMER_ID) AS TOTAL_CUST
FROM VPM_DATA.P_PM_CAT_RELEVANCE_SCORE_ALL
GROUP BY COALESCE(ELEC_GAD_RELEVANCE_SEGMENT,'N/A')
UNION ALL
SELECT 'P_PM_CAT_RELEVANCE_SCORE_ALL' AS TABLE_NAME
,'CON_HIGH_RELEVANCE_SEGMENT' AS SEGMENT
,COALESCE(CON_HIGH_RELEVANCE_SEGMENT,'N/A') AS SEGMENT_VALUE
,COUNT(DISTINCT CUSTOMER_ID) AS TOTAL_CUST
FROM VPM_DATA.P_PM_CAT_RELEVANCE_SCORE_ALL
GROUP BY COALESCE(CON_HIGH_RELEVANCE_SEGMENT,'N/A')
UNION ALL
SELECT 'P_PM_CAT_RELEVANCE_SCORE_ALL' AS TABLE_NAME
,'CATEGORY_RELEVANCE_PRIORITY' AS SEGMENT
,COALESCE(CATEGORY_RELEVANCE_PRIORITY,'N/A') AS SEGMENT_VALUE
,COUNT(DISTINCT CUSTOMER_ID) AS TOTAL_CUST
FROM VPM_DATA.P_PM_CAT_RELEVANCE_SCORE_ALL
GROUP BY COALESCE(CATEGORY_RELEVANCE_PRIORITY,'N/A')
--UNION ALL
--SELECT 'P_PM_CAT_RELEVANCE_SCORE_ALL' AS TABLE_NAME
--,'' AS SEGMENT
--, AS SEGMENT_VALUE
--,COUNT(DISTINCT CUSTOMER_ID) AS TOTAL_CUST
--FROM VPM_DATA.P_PM_CAT_RELEVANCE_SCORE_ALL
--GROUP BY 