SELECT 'PM_LARGE_FAMILY_CUST_1YR' AS TABLE_NAME
,'LARGE_FAMILY_FLAG' AS SEGMENT
,'YES' AS SEGMENT_VALUE
,COUNT(DISTINCT CUSTOMER_ID) AS TOTAL_CUST
FROM VPM_DATA.PM_LARGE_FAMILY_CUST_1YR
WHERE COALESCE(CUSTOMER_ID,'') <> ''
--UNION ALL
--SELECT 'PM_SKUTAGGING_HOBBY_INTEREST' AS TABLE_NAME
--,'' AS SEGMENT
--, AS SEGMENT_VALUE
--,COUNT(DISTINCT CUSTOMER_ID) AS TOTAL_CUST
--FROM VPM_DATA.PM_SKUTAGGING_HOBBY_INTEREST
--GROUP BY 