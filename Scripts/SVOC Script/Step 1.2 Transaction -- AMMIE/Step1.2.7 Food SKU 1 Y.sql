/*
	CREATE SALESKU 6 MONTHS TABLE
	TABLE : VPM_BACKUP.PM_SKUTAGGING_FOOD_SALESSKU_6M
	
	CHANGE : END DATE = '2018-07-20'
			 @START_DATE = '2018-02-21'
			 FROM ANALYSIS_DATA : CHANGE MONTH IN DIFF YEAR
*/

--CREATE NONCLUSTERED INDEX BU_SKU ON VPM_DATA.PM_SKUTAGGING_FOOD_PRODUCT_MASTER (BU_NAME,SKU_CODE,DEPT_CODE,SUBDEPT_CODE)
--;
--CREATE SALESSKU OF ALL BU

INSERT INTO VPM_DATA.SALESSKU_LOG_TIME(task,start_time) VALUES ('Step1.2.7 Food SKU 1 Y',NOW());

DROP TABLE IF EXISTS VPM_DATA.TEMP_PM_SKUTAGGING_FOOD_SALESSKU_STAGING_1; --78m 40s
CREATE TABLE VPM_DATA.TEMP_PM_SKUTAGGING_FOOD_SALESSKU_STAGING_1 AS
SELECT A.CUSTOMER_ID
      ,A.BUID
	  ,A.BRANCHID
      ,A.TXN_DATE
      ,A.TICKET_NUM
      ,A.SKU_CODE
      ,A.TXN_AMT
      ,A.PRICE_PER_UNIT
      ,A.DEPT_CODE
      ,A.SUBDEPT_CODE
      ,A.QTY
      ,A.BU_NAME
      ,A.DEPT_NAME
      ,A.SUBDEPT_NAME
      ,A.CLASS_ID
      ,A.CLASS_NAME
      ,A.SUBCLASS_ID
      ,A.SUBCLASS_NAME
      ,NULL AS SUBCAT_ID
      ,NULL AS SUB_CAT_NAME
      ,A.CLEANED_BRANDNAME
      ,A.PRODUCT_NAME
      ,B.CATEGORY_FINAL
      ,B.TYPE_FINAL
      ,B.SUBTYPE_FINAL
      ,B.INTERFOOD_FLAG
      ,B.HEALTHY_FLAG
      ,B.READY_EAT_FLAG
      ,B.PERISHABLE_FLAG
      ,B.COOKING_FLAG
      ,B.BASKET_FLAG
      ,B.LUXURY_INGREDIENT_FLAG
      ,B.ORGANIC_FLAG
      ,B.SIZE
      ,B.MEASUREMENT
      ,B.PACK
	  ,B.CAT_TYPE_GRP
      ,FULL_AMOUNT
      ,C.REVISED_CATEGORY_LEVEL_1
      ,C.REVISED_CATEGORY_LEVEL_2
      ,C.REVISED_CATEGORY_LEVEL_3
FROM VPM_DATA.PM_ALL_SALESSKU_1YR A
INNER JOIN VPM_DATA.PM_SKUTAGGING_FOOD_PRODUCT_MASTER B
ON CASE WHEN A.BUID IN ('4','176') THEN '4' 
		WHEN A.BUID IN ('1','2','71','998') THEN '1' --CDS/RBS REVISED PRODUCT MASTER
		WHEN A.BUID IN ('15','56') THEN '15'
		WHEN A.BUID IN ('143','144') THEN '143'
	ELSE A.BUID END 
	= CASE WHEN B.BUID IN ('4','176') THEN '4' 
		WHEN B.BUID IN ('1','2','71','998') THEN '1' --CDS/RBS REVISED PRODUCT MASTER
		WHEN B.BUID IN ('15','56') THEN '15'
		WHEN B.BUID IN ('143','144') THEN '143'
	ELSE B.BUID END 
	AND A.SKU_CODE = B.SKU_CODE
LEFT JOIN VPM_DATA.PM_PRODUCT_MASTER C
ON CASE WHEN A.BUID IN ('4','176') THEN '4' 
		WHEN A.BUID IN ('1','2','71','998') THEN '1' --CDS/RBS REVISED PRODUCT MASTER
		WHEN A.BUID IN ('15','56') THEN '15'
		WHEN A.BUID IN ('143','144') THEN '143'
	ELSE A.BUID END 
	= CASE WHEN C.BUID IN ('4','176') THEN '4' 
		WHEN C.BUID IN ('1','2','71','998') THEN '1' --CDS/RBS REVISED PRODUCT MASTER
		WHEN C.BUID IN ('15','56') THEN '15'
		WHEN C.BUID IN ('143','144') THEN '143'
	ELSE C.BUID END 
AND A.SKU_CODE = C.SKU_CODE
WHERE TXN_DATE >= (vpm_data.get_start_date(now())+INTERVAL '6 month')::DATE;
UPDATE VPM_DATA.SALESSKU_LOG_TIME SET end_time = now() WHERE task = 'FOOD STAGING 1' AND EXTRACT(MONTH FROM start_time) = EXTRACT(MONTH FROM now());

INSERT INTO VPM_DATA.SALESSKU_LOG_TIME(task,start_time) VALUES ('FOOD STAGING 2',NOW());
DROP TABLE IF EXISTS VPM_DATA.TEMP_PM_SKUTAGGING_FOOD_SALESSKU_STAGING_2;
CREATE TABLE VPM_DATA.TEMP_PM_SKUTAGGING_FOOD_SALESSKU_STAGING_2 AS
SELECT A.CUSTOMER_ID
      ,A.BUID
	  ,A.BRANCHID
      ,A.TXN_DATE
      ,A.TICKET_NUM
      ,A.SKU_CODE
      ,A.TXN_AMT
      ,A.PRICE_PER_UNIT
      ,A.DEPT_CODE
      ,A.SUBDEPT_CODE
      ,A.QTY
      ,A.BU_NAME
      ,A.DEPT_NAME
      ,A.SUBDEPT_NAME
      ,A.CLASS_ID
      ,A.CLASS_NAME
      ,A.SUBCLASS_ID
      ,A.SUBCLASS_NAME
      ,A.SUBCAT_ID
      ,A.SUB_CAT_NAME
      ,A.CLEANED_BRANDNAME
      ,A.PRODUCT_NAME
      ,B.CATEGORY_FINAL
      ,B.TYPE_FINAL
      ,B.SUBTYPE_FINAL
      ,B.INTERFOOD_FLAG
      ,B.HEALTHY_FLAG
      ,B.READY_EAT_FLAG
      ,B.PERISHABLE_FLAG
      ,B.COOKING_FLAG
      ,B.BASKET_FLAG
      ,B.LUXURY_INGREDIENT_FLAG
      ,B.ORGANIC_FLAG
      ,B.SIZE
      ,B.MEASUREMENT
      ,B.PACK
	  ,B.CAT_TYPE_GRP
      ,FULL_AMOUNT
      ,C.REVISED_CATEGORY_LEVEL_1
      ,C.REVISED_CATEGORY_LEVEL_2
      ,C.REVISED_CATEGORY_LEVEL_3
FROM VPM_DATA.PM_TOPS_SALESSKU_1YR A
INNER JOIN VPM_DATA.PM_SKUTAGGING_FOOD_PRODUCT_MASTER B
ON A.BUID = B.BUID 
	AND A.SKU_CODE = B.SKU_CODE
LEFT JOIN VPM_DATA.PM_PRODUCT_MASTER C
ON A.BUID = C.BUID
AND A.SKU_CODE = C.SKU_CODE
WHERE TXN_DATE >= (vpm_data.get_start_date(now())+INTERVAL '6 month')::DATE;
UPDATE VPM_DATA.SALESSKU_LOG_TIME SET END_TIME = NOW() WHERE TASK = 'FOOD STAGING 2' AND EXTRACT(MONTH FROM START_TIME) = EXTRACT(MONTH FROM NOW());

INSERT INTO VPM_DATA.SALESSKU_LOG_TIME(task,start_time) VALUES ('FOOD STAGING 3',NOW());
DROP TABLE IF EXISTS VPM_DATA.TEMP_PM_SKUTAGGING_FOOD_SALESSKU_STAGING_3; --46m 16s
CREATE TABLE VPM_DATA.TEMP_PM_SKUTAGGING_FOOD_SALESSKU_STAGING_3 AS
SELECT A.CUSTOMER_ID
      ,A.BUID
	  ,A.BRANCHID
      ,A.TXN_DATE
      ,A.TICKET_NUM
      ,A.SKU_CODE
      ,A.TXN_AMT
      ,A.PRICE_PER_UNIT
      ,A.DEPT_CODE
      ,A.SUBDEPT_CODE
      ,A.QTY
      ,A.BU_NAME
      ,A.DEPT_NAME
      ,A.SUBDEPT_NAME
      ,A.CLASS_ID
      ,A.CLASS_NAME
      ,A.SUBCLASS_ID
      ,A.SUBCLASS_NAME
      ,NULL AS SUBCAT_ID
      ,NULL AS SUB_CAT_NAME
      ,A.CLEANED_BRANDNAME
      ,A.PRODUCT_NAME
      ,B.CATEGORY_FINAL
      ,B.TYPE_FINAL
      ,B.SUBTYPE_FINAL
      ,B.INTERFOOD_FLAG
      ,B.HEALTHY_FLAG
      ,B.READY_EAT_FLAG
      ,B.PERISHABLE_FLAG
      ,B.COOKING_FLAG
      ,B.BASKET_FLAG
      ,B.LUXURY_INGREDIENT_FLAG
      ,B.ORGANIC_FLAG
      ,B.SIZE
      ,B.MEASUREMENT
      ,B.PACK
	  ,B.CAT_TYPE_GRP
      ,FULL_AMOUNT
      ,C.REVISED_CATEGORY_LEVEL_1
      ,C.REVISED_CATEGORY_LEVEL_2
      ,C.REVISED_CATEGORY_LEVEL_3
FROM VPM_DATA.PM_CFM_SALESSKU_1YR A
INNER JOIN VPM_DATA.PM_SKUTAGGING_FOOD_PRODUCT_MASTER B
ON A.BUID = B.BUID 
	AND A.SKU_CODE = B.SKU_CODE
LEFT JOIN VPM_DATA.PM_PRODUCT_MASTER C
ON A.BUID = C.BUID
AND A.SKU_CODE = C.SKU_CODE
WHERE TXN_DATE >= (vpm_data.get_start_date(now())+INTERVAL '6 month')::DATE;
UPDATE VPM_DATA.SALESSKU_LOG_TIME SET end_time = now() WHERE task = 'FOOD STAGING 3' AND EXTRACT(MONTH FROM start_time) = EXTRACT(MONTH FROM now());

INSERT INTO VPM_DATA.SALESSKU_LOG_TIME(task,start_time) VALUES ('FOOD 6M FINAL',NOW());
DROP TABLE IF EXISTS VPM_DATA.PM_SKUTAGGING_FOOD_SALESSKU_6M; --16m 1s --176,552,712 --35m 39s
CREATE TABLE VPM_DATA.PM_SKUTAGGING_FOOD_SALESSKU_6M AS
SELECT * FROM VPM_DATA.TEMP_PM_SKUTAGGING_FOOD_SALESSKU_STAGING_1
UNION ALL
SELECT * FROM VPM_DATA.TEMP_PM_SKUTAGGING_FOOD_SALESSKU_STAGING_2
UNION ALL
SELECT * FROM VPM_DATA.TEMP_PM_SKUTAGGING_FOOD_SALESSKU_STAGING_3;
UPDATE VPM_DATA.SALESSKU_LOG_TIME SET end_time = now() WHERE task = 'FOOD 6M FINAL' AND EXTRACT(MONTH FROM start_time) = EXTRACT(MONTH FROM now());


/*************************************************************************************************/


---------------------------------------------------- CREATE ATTRIBUTE

--03.51 HR --22m 28s 
DROP TABLE IF EXISTS VPM_DATA.TEMP_WEALTH_FOOD_SALES;
CREATE TABLE VPM_DATA.TEMP_WEALTH_FOOD_SALES AS
SELECT DISTINCT CUSTOMER_ID 
FROM VPM_DATA.PM_CUSTOMER_CC 
WHERE MINIMUM_DEPOSIT > 0 OR (UPPER(TRIM(ISSUING_BANK)) ='CENTRAL T1C' 
							AND UPPER(TRIM(CARD_DESC)) LIKE '%BLACK%');
CREATE INDEX CUSTOMER_ID_TEMP_WEALTH_FOOD_SALES ON VPM_DATA.TEMP_WEALTH_FOOD_SALES(CUSTOMER_ID);						

DROP TABLE IF EXISTS VPM_DATA.TMP_CNT_CUST_FOOD_SALESSKU;
CREATE TABLE VPM_DATA.TMP_CNT_CUST_FOOD_SALESSKU as
SELECT  A.SKU_CODE
	   ,A.BUID
	   ,AVG(A.PRICE_PER_UNIT) AS AVG_PRICE
	   ,SUM(A.TXN_AMT) AS SUM_AMT
	   ,COUNT(DISTINCT A.CUSTOMER_ID) AS CNT_CUST
	   ,COUNT(DISTINCT B.CUSTOMER_ID) AS HIGH_WEALTH_CNT_CUST
FROM VPM_DATA.PM_SKUTAGGING_FOOD_SALESSKU_6M AS A
LEFT JOIN VPM_DATA.TEMP_WEALTH_FOOD_SALES B
ON A.CUSTOMER_ID = B.CUSTOMER_ID
GROUP BY A.SKU_CODE,A.BUID;

CREATE INDEX BUID_SKUCODE_TMP_CNT_CUST_FOOD_SALESSKU ON VPM_DATA.TMP_CNT_CUST_FOOD_SALESSKU(buid,sku_code);

UPDATE VPM_DATA.PM_SKUTAGGING_FOOD_PRODUCT_MASTER 
SET AVG_PRICE = TMP_CNT_CUST_FOOD_SALESSKU.AVG_PRICE
	,SUM_AMT = TMP_CNT_CUST_FOOD_SALESSKU.SUM_AMT
	,CNT_CUST = TMP_CNT_CUST_FOOD_SALESSKU.CNT_CUST
	,HIGH_WEALTH_CNT_CUST = TMP_CNT_CUST_FOOD_SALESSKU.HIGH_WEALTH_CNT_CUST
FROM VPM_DATA.TMP_CNT_CUST_FOOD_SALESSKU
WHERE VPM_DATA.PM_SKUTAGGING_FOOD_PRODUCT_MASTER.SKU_CODE = TMP_CNT_CUST_FOOD_SALESSKU.SKU_CODE
AND VPM_DATA.PM_SKUTAGGING_FOOD_PRODUCT_MASTER.BUID  = CASE WHEN TMP_CNT_CUST_FOOD_SALESSKU.BUID IN ('1','2','71','998') THEN '1' ELSE TMP_CNT_CUST_FOOD_SALESSKU.BUID END --CDS/RBS REVISED PRODUCT MASTER
;

UPDATE VPM_DATA.PM_SKUTAGGING_FOOD_PRODUCT_MASTER
SET HIGH_WEALTH_CUST_RATIO = HIGH_WEALTH_CNT_CUST*1.0/CNT_CUST
;
UPDATE VPM_DATA.PM_SKUTAGGING_FOOD_PRODUCT_MASTER
SET WLTH_CUST_PURCHASE_SCORE = 
CASE WHEN  HIGH_WEALTH_CUST_RATIO >= (SELECT DISTINCT PERCENTILE_DISC (0.97) WITHIN GROUP (ORDER BY HIGH_WEALTH_CUST_RATIO ASC NULLS FIRST)
										FROM VPM_DATA.PM_SKUTAGGING_FOOD_PRODUCT_MASTER WHERE CNT_CUST >= 30 ) THEN 1
ELSE HIGH_WEALTH_CUST_RATIO/(SELECT DISTINCT PERCENTILE_DISC (0.97) WITHIN GROUP (ORDER BY HIGH_WEALTH_CUST_RATIO ASC NULLS FIRST)
								FROM VPM_DATA.PM_SKUTAGGING_FOOD_PRODUCT_MASTER WHERE CNT_CUST >= 30) END 
WHERE CNT_CUST >= 30
;

/* 2. ADD STANDARD_PRICE */

UPDATE VPM_DATA.PM_SKUTAGGING_FOOD_PRODUCT_MASTER
SET STANDARD_PRICE = 
CASE WHEN PACK IS NOT NULL AND SIZE IS NULL	THEN AVG_PRICE::FLOAT/PACK::FLOAT ELSE AVG_PRICE END
;

UPDATE VPM_DATA.PM_SKUTAGGING_FOOD_PRODUCT_MASTER
SET STANDARD_PRICE = 
CASE WHEN MEASUREMENT IN ('KG','L') AND SIZE IS NOT NULL THEN (1.0*AVG_PRICE)/(CAST (SIZE AS FLOAT)*1000/500)/(CASE WHEN PACK IS NULL THEN 1 ELSE CAST(PACK AS FLOAT) END)
	 WHEN MEASUREMENT IN ('CC','G','ML') AND SIZE IS NOT NULL THEN (1.0*AVG_PRICE)/(CAST (SIZE AS FLOAT)/500)/(CASE WHEN PACK IS NULL THEN 1 ELSE CAST(PACK AS FLOAT) END)
	 WHEN MEASUREMENT = 'OZ' AND SIZE IS NOT NULL THEN (1.0*AVG_PRICE)/(CAST (SIZE AS FLOAT)/16)/(CASE WHEN PACK IS NULL THEN 1 ELSE CAST(PACK AS FLOAT) END) -- 16OZ = 473 ML
 ELSE STANDARD_PRICE END
 WHERE ((CATEGORY_FINAL IN ('BEVERAGE','FRESH','DAIRY PRODUCT','LIQUOR') AND CAT_TYPE_GRP NOT IN ('HERB'))
 OR UPPER(TRIM(CAT_TYPE_GRP)) IN ('FROZEN FISH','FROZEN PORK','FROZEN BEEF','FROZEN CHICKEN','FROZEN SEAFOOD','FROZEN SPECIAL MEAT','FROZEN LOCAL SPECIAL MEAT','FROZEN VEGETABLE','FROZEN FRUIT','FROZEN POTATO')) 
 AND (SIZE IS NULL OR CAST(SIZE AS FLOAT) <>0)
;

--3.1 CREATE TMP FOR CALC PRICE RANGE

DROP TABLE IF EXISTS VPM_DATA.TMP;
CREATE TABLE VPM_DATA.TMP AS
SELECT DISTINCT CAT_TYPE_GRP
,PERCENTILE_DISC (0.95) WITHIN GROUP (ORDER BY STANDARD_PRICE ASC NULLS FIRST) AS MAX_PRICE
,PERCENTILE_DISC (0.05) WITHIN GROUP (ORDER BY STANDARD_PRICE ASC NULLS FIRST) AS MIN_PRICE
,PERCENTILE_DISC (0.95) WITHIN GROUP (ORDER BY STANDARD_PRICE ASC NULLS FIRST) - PERCENTILE_DISC (0.05) WITHIN GROUP (ORDER BY STANDARD_PRICE ASC) AS PRICE_RANGE
FROM VPM_DATA.PM_SKUTAGGING_FOOD_PRODUCT_MASTER 
WHERE CNT_CUST >= 30
GROUP BY CAT_TYPE_GRP
ORDER BY PRICE_RANGE DESC;

UPDATE VPM_DATA.PM_SKUTAGGING_FOOD_PRODUCT_MASTER
SET PRICE_RANGE = T.PRICE_RANGE
FROM VPM_DATA.TMP T
WHERE UPPER(TRIM(PM_SKUTAGGING_FOOD_PRODUCT_MASTER.CAT_TYPE_GRP)) = UPPER(TRIM(T.CAT_TYPE_GRP));

DROP TABLE IF EXISTS VPM_DATA.TMP_PRICE;
CREATE TABLE VPM_DATA.TMP_PRICE as
SELECT DISTINCT CATEGORY_FINAL,A.CAT_TYPE_GRP
		,CNT_PRODUCT
		,PRICE_50PCT
		,PRICE_80PCT
		,PRICE_90PCT
		,PRICE_95PCT
		,PRICE_99PCT
		,PRICE_RANGE		
FROM VPM_DATA.PM_SKUTAGGING_FOOD_PRODUCT_MASTER A
LEFT JOIN 
	(SELECT CAT_TYPE_GRP
		,COUNT(SKU_CODE) AS CNT_PRODUCT
		,PERCENTILE_DISC(0.5) WITHIN GROUP (ORDER BY STANDARD_PRICE NULLS FIRST) AS PRICE_50PCT
		,PERCENTILE_DISC(0.8) WITHIN GROUP (ORDER BY STANDARD_PRICE NULLS FIRST) AS PRICE_80PCT
		,PERCENTILE_DISC(0.9) WITHIN GROUP (ORDER BY STANDARD_PRICE NULLS FIRST) AS PRICE_90PCT
		,PERCENTILE_DISC(0.95) WITHIN GROUP (ORDER BY STANDARD_PRICE NULLS FIRST)AS PRICE_95PCT
		,PERCENTILE_DISC(0.99) WITHIN GROUP (ORDER BY STANDARD_PRICE NULLS FIRST) AS PRICE_99PCT
	FROM VPM_DATA.PM_SKUTAGGING_FOOD_PRODUCT_MASTER
	WHERE CAT_TYPE_GRP IS NOT NULL
	GROUP BY CAT_TYPE_GRP
		) B
ON A.CAT_TYPE_GRP = B.CAT_TYPE_GRP
WHERE A.CAT_TYPE_GRP IS NOT NULL;

-- ADD FOOD CLUSTER

UPDATE VPM_DATA.PM_SKUTAGGING_FOOD_PRODUCT_MASTER
SET FOOD_CLUSTER = 
CASE WHEN UPPER(TRIM(CAT_TYPE_GRP)) IN ('BRANDY','WHISKY','VODKA','WINE','OTHER LIQUOR')
		THEN CASE WHEN AVG_PRICE >= 10000 THEN 5
				  WHEN AVG_PRICE >= 3000 THEN 4
				  WHEN AVG_PRICE >= 1500 THEN 3
				  WHEN AVG_PRICE >= 750 THEN 2
				  WHEN AVG_PRICE >= 0 THEN 1
				  ELSE NULL END
	 WHEN UPPER(TRIM(CAT_TYPE_GRP)) IN ('FRESH BEEF','FROZEN BEEF')
		THEN CASE WHEN AVG_PRICE >= 2000 THEN 5
				  WHEN AVG_PRICE >= 1000 THEN 4
				  WHEN AVG_PRICE >= 500 THEN 3
				  WHEN AVG_PRICE >= 200 THEN 2
				  WHEN AVG_PRICE >= 0 THEN 1
				  ELSE NULL END
	 WHEN UPPER(TRIM(CAT_TYPE_GRP)) IN ('FRESH SEAFOOD','FROZEN SEAFOOD','RTE/RTC SEAFOOD')
		THEN CASE WHEN AVG_PRICE >= 2000 THEN 5
				  WHEN AVG_PRICE >= 1000 THEN 4
				  WHEN AVG_PRICE >= 500 THEN 3
				  WHEN AVG_PRICE >= 200 THEN 2
				  WHEN AVG_PRICE >= 0 THEN 1
				  ELSE NULL END
	WHEN UPPER(TRIM(CAT_TYPE_GRP)) IN ('CHILLED PROCESSED MEAT')
		THEN CASE WHEN AVG_PRICE >= 2000 THEN 5
				  WHEN AVG_PRICE >= 1000 THEN 4
				  WHEN AVG_PRICE >= 500 THEN 3
				  WHEN AVG_PRICE >= 200 THEN 2
				  WHEN AVG_PRICE >= 0 THEN 1
				  ELSE NULL END
	WHEN UPPER(TRIM(CAT_TYPE_GRP)) IN ('CAVIAR')
		THEN CASE WHEN AVG_PRICE >= 2000 THEN 5
				  WHEN AVG_PRICE >= 1000 THEN 4
				  WHEN AVG_PRICE >= 500 THEN 3
				  WHEN AVG_PRICE >= 200 THEN 2
				  WHEN AVG_PRICE >= 0 THEN 1
				  ELSE NULL END
	WHEN UPPER(TRIM(CAT_TYPE_GRP)) IN ('FOIE GRAS')
		THEN CASE WHEN AVG_PRICE >= 1500 THEN 5
				  WHEN AVG_PRICE >= 750 THEN 4
				  WHEN AVG_PRICE >= 300 THEN 3
				  WHEN AVG_PRICE >= 0 THEN 2
				  ELSE NULL END
	WHEN UPPER(TRIM(CAT_TYPE_GRP)) IN ('COLD CUTS')
		THEN CASE WHEN AVG_PRICE >= 1500 THEN 5
				  WHEN AVG_PRICE >= 1000 THEN 4
				  WHEN AVG_PRICE >= 500 THEN 3
				  WHEN AVG_PRICE >= 0 THEN 2
				  ELSE NULL END
	 WHEN UPPER(TRIM(CAT_TYPE_GRP)) IN ('FRESH FRUIT','FROZEN FRUIT')
		THEN CASE WHEN AVG_PRICE >= 1000 THEN 4
				  WHEN AVG_PRICE >= 300 THEN 3
				  WHEN AVG_PRICE >= 150 THEN 2
				  WHEN AVG_PRICE >= 0 THEN 1
				  ELSE NULL END
	 WHEN UPPER(TRIM(CAT_TYPE_GRP)) IN ('HONEY')
		THEN CASE WHEN AVG_PRICE >= 2000 THEN 5
				  WHEN AVG_PRICE >= 1000 THEN 4
				  WHEN AVG_PRICE >= 300 THEN 3
				  WHEN AVG_PRICE >= 100 THEN 2
				  WHEN AVG_PRICE >= 0 THEN 1
				  ELSE NULL END
	 WHEN UPPER(TRIM(CAT_TYPE_GRP)) IN ('CHEESE')
		THEN CASE WHEN AVG_PRICE >= 1500 THEN 5
				  WHEN AVG_PRICE >= 750 THEN 4
				  WHEN AVG_PRICE >= 300 THEN 3
				  WHEN AVG_PRICE >= 100 THEN 2
				  WHEN AVG_PRICE >= 0 THEN 1
				  ELSE NULL END
	 WHEN UPPER(TRIM(CAT_TYPE_GRP)) IN ('OIL')
		THEN CASE WHEN AVG_PRICE >= 750 THEN 4
				  WHEN AVG_PRICE >= 300 THEN 3
				  WHEN AVG_PRICE >= 100 THEN 2
				  WHEN AVG_PRICE >= 0 THEN 1
				  ELSE NULL END
	ELSE NULL END;

UPDATE VPM_DATA.PM_SKUTAGGING_FOOD_PRODUCT_MASTER
SET FOOD_CLUSTER = CASE WHEN STANDARD_PRICE >= PRICE_80PCT AND HIGH_WEALTH_CUST_RATIO >= 0.2 THEN 3
						WHEN STANDARD_PRICE >= PRICE_50PCT THEN 2 
						ELSE 1 END
FROM VPM_DATA.TMP_PRICE A
WHERE UPPER(TRIM(PM_SKUTAGGING_FOOD_PRODUCT_MASTER.CAT_TYPE_GRP)) = UPPER(TRIM(A.CAT_TYPE_GRP))
		AND PM_SKUTAGGING_FOOD_PRODUCT_MASTER.CATEGORY_FINAL = A.CATEGORY_FINAL
		AND PM_SKUTAGGING_FOOD_PRODUCT_MASTER.FOOD_CLUSTER IS NULL
		AND UPPER(TRIM(A.CAT_TYPE_GRP)) NOT IN ('HEALTHY DRINK','BASKET');


UPDATE VPM_DATA.PM_SKUTAGGING_FOOD_PRODUCT_MASTER
SET FOOD_CLUSTER = CASE WHEN STANDARD_PRICE >= PRICE_80PCT THEN 2 
						ELSE 1 END
FROM VPM_DATA.TMP_PRICE A
WHERE UPPER(TRIM(PM_SKUTAGGING_FOOD_PRODUCT_MASTER.CAT_TYPE_GRP)) = UPPER(TRIM(A.CAT_TYPE_GRP))
		AND PM_SKUTAGGING_FOOD_PRODUCT_MASTER.CATEGORY_FINAL = A.CATEGORY_FINAL
		AND PM_SKUTAGGING_FOOD_PRODUCT_MASTER.FOOD_CLUSTER IS NULL
		AND UPPER(TRIM(A.CAT_TYPE_GRP)) IN ('HEALTHY DRINK','BASKET');

DROP TABLE IF EXISTS VPM_DATA.TMP_PRICE;
UPDATE VPM_DATA.SALESSKU_LOG_TIME SET end_time = now() WHERE task = 'Step1.2.7 Food SKU 1 Y' AND EXTRACT(MONTH FROM start_time) = EXTRACT(MONTH FROM now());
