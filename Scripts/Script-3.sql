<<<<<<< HEAD
=======
ALTER TABLE vpm_data.u_chg_lead_gen_new_cultivation_campaign_spendingtier ADD response_flag int;
ALTER TABLE vpm_data.u_chg_lead_gen_new_cultivation_campaign_spendingtier ADD r_ttl_spending NUMERIC(21,6);
ALTER TABLE vpm_data.u_chg_lead_gen_new_cultivation_campaign_spendingtier ADD coupon1_flag int;
ALTER TABLE vpm_data.u_chg_lead_gen_new_cultivation_campaign_spendingtier ADD coupon2_flag int;
ALTER TABLE vpm_data.u_chg_lead_gen_new_cultivation_campaign_spendingtier ADD campaign_start_date date;
ALTER TABLE vpm_data.u_chg_lead_gen_new_cultivation_campaign_spendingtier ADD campaign_end_date date;

CREATE TABLE vpm_data.u_chg_new_cultivation_campaign_offer_spendingtier_twd (
	spending_tier varchar(20)
	,tier_spend1 int
	,offer_discount1 int
	,discount_percent1 int
	,tier_spend2 int
	,offer_discount2 int
	,discount_percent2 int
);

INSERT INTO vpm_data.u_chg_new_cultivation_campaign_offer_spendingtier_twd VALUES ('1) Tier 1,000-2,000',2000,100,5,4000,400,10);
INSERT INTO vpm_data.u_chg_new_cultivation_campaign_offer_spendingtier_twd VALUES ('2) Tier 2,000-4,000',4000,200,5,6000,600,10);
INSERT INTO vpm_data.u_chg_new_cultivation_campaign_offer_spendingtier_twd VALUES ('3) Tier 4,000-6,000',6000,300,5,8000,800,10);
INSERT INTO vpm_data.u_chg_new_cultivation_campaign_offer_spendingtier_twd VALUES ('4) Tier 6,000-8,000',8000,400,5,10000,1000,10);
INSERT INTO vpm_data.u_chg_new_cultivation_campaign_offer_spendingtier_twd VALUES ('5) Tier 8,000-10,000',10000,500,5,15000,1500,10);
INSERT INTO vpm_data.u_chg_new_cultivation_campaign_offer_spendingtier_twd VALUES ('6) Tier 10,000 up',15000,750,5,20000,2000,10);


CREATE temp TABLE new_cul_round(campaign_round date,start_date date,end_date date);
INSERT INTO new_cul_round VALUES ('2020-06-14','2020-06-19','2020-07-01');
INSERT INTO new_cul_round VALUES ('2020-06-29','2020-07-03','2020-07-15');
INSERT INTO new_cul_round VALUES ('2020-07-13','2020-07-17','2020-07-29');
INSERT INTO new_cul_round VALUES ('2020-07-27','2020-07-31','2020-08-12');
INSERT INTO new_cul_round VALUES ('2020-08-10','2020-08-14','2020-08-26');
INSERT INTO new_cul_round VALUES ('2020-08-24','2020-08-28','2020-09-09');
INSERT INTO new_cul_round VALUES ('2020-09-07','2020-09-11','2020-09-23');
INSERT INTO new_cul_round VALUES ('2020-09-21','2020-09-25','2020-10-07');
INSERT INTO new_cul_round VALUES ('2020-10-05','2020-10-09','2020-10-21');
INSERT INTO new_cul_round VALUES ('2020-10-19','2020-10-23','2020-11-04');
INSERT INTO new_cul_round VALUES ('2020-11-02','2020-11-06','2020-11-18');
INSERT INTO new_cul_round VALUES ('2020-11-16','2020-11-20','2020-12-02');
INSERT INTO new_cul_round VALUES ('2020-11-30','2020-12-04','2020-12-16');
INSERT INTO new_cul_round VALUES ('2020-12-14','2020-12-18','2020-12-30');
INSERT INTO new_cul_round VALUES ('2020-12-28','2021-01-08','2021-01-20');
INSERT INTO new_cul_round VALUES ('2021-01-18','2021-01-22','2021-02-03');
INSERT INTO new_cul_round VALUES ('2021-02-01','2021-02-05','2021-02-17');
INSERT INTO new_cul_round VALUES ('2021-02-15','2021-02-19','2021-03-03');

update vpm_data.u_chg_lead_gen_new_cultivation_campaign_spendingtier 
SET campaign_start_date = t.start_date
	,campaign_end_date = t.end_date
FROM new_cul_round t
WHERE vpm_data.u_chg_lead_gen_new_cultivation_campaign_spendingtier.campaign_sending_date = t.campaign_round
	AND vpm_data.u_chg_lead_gen_new_cultivation_campaign_spendingtier.bu_name = 'TWD';

DROP TABLE IF EXISTS txn_period;
CREATE temp TABLE txn_period as
SELECT customerid AS customer_id,transactiondate::date AS txn_date,spending AS txn_amt FROM analysis_data.salessku_twd2020 WHERE customerid <> '' AND spending > 0 AND transactiondate >= (SELECT min(start_date) FROM  new_cul_round)
UNION ALL SELECT customerid AS customer_id,transactiondate::date AS txn_date,spending AS txn_amt FROM analysis_data.salessku_twd2021 WHERE customerid <> '' AND spending > 0 AND transactiondate <= (SELECT max(end_date) FROM  new_cul_round);

DROP TABLE IF EXISTS lead_response;
SELECT t.customer_id,t.spending_tier,campaign_round,sum(txn_amt) AS r_ttl_spending INTO TEMP TABLE lead_response
FROM
(
	SELECT a.customer_id,a.spending_tier,b.* FROM vpm_data.u_chg_lead_gen_new_cultivation_campaign_spendingtier a
	LEFT JOIN new_cul_round b ON a.campaign_sending_date = b.campaign_round
	WHERE a.campaign_round <> 1
		AND a.bu_name = 'TWD'
) T
INNER JOIN txn_period c ON t.customer_id = c.customer_id
WHERE c.txn_date BETWEEN t.start_date AND t.end_date
GROUP BY t.customer_id,t.spending_tier,campaign_round;

DROP TABLE IF EXISTS lead_response_final;
SELECT a.*,CASE WHEN r_ttl_spending > tier_spend1 THEN 1 ELSE 0 END AS coupon1_flag 
	,CASE WHEN r_ttl_spending > tier_spend2 THEN 1 ELSE 0 END AS coupon2_flag 
INTO TEMP TABLE lead_response_final
FROM lead_response a
LEFT JOIN vpm_data.u_chg_new_cultivation_campaign_offer_spendingtier_twd b
ON a.spending_tier = b.spending_tier;

update vpm_data.u_chg_lead_gen_new_cultivation_campaign_spendingtier
SET response_flag = NULL
	,r_ttl_spending = NULL
	,coupon1_flag = NULL
	,coupon2_flag = NULL;

update vpm_data.u_chg_lead_gen_new_cultivation_campaign_spendingtier
SET response_flag = 1
	,r_ttl_spending = t.r_ttl_spending
	,coupon1_flag = t.coupon1_flag
	,coupon2_flag = t.coupon2_flag
FROM lead_response_final t
WHERE vpm_data.u_chg_lead_gen_new_cultivation_campaign_spendingtier.customer_id = t.customer_id
	AND vpm_data.u_chg_lead_gen_new_cultivation_campaign_spendingtier.bu_name = 'TWD'
	AND vpm_data.u_chg_lead_gen_new_cultivation_campaign_spendingtier.campaign_sending_date = t.campaign_round;


-- BNB
CREATE TABLE vpm_data.u_chg_new_cultivation_campaign_offer_spendingtier_bnb (
	spending_tier varchar(20)
	,tier_spend1 int
	,offer_discount1 int
	,discount_percent1 int
	,tier_spend2 int
	,offer_discount2 int
	,discount_percent2 int
);

INSERT INTO vpm_data.u_chg_new_cultivation_campaign_offer_spendingtier_bnb VALUES ('1) Tier 1,000-2,000',2000,200,5,4000,600,10);
INSERT INTO vpm_data.u_chg_new_cultivation_campaign_offer_spendingtier_bnb VALUES ('2) Tier 2,000-4,000',4000,400,5,6000,900,10);
INSERT INTO vpm_data.u_chg_new_cultivation_campaign_offer_spendingtier_bnb VALUES ('3) Tier 4,000-6,000',6000,600,5,8000,1200,10);
INSERT INTO vpm_data.u_chg_new_cultivation_campaign_offer_spendingtier_bnb VALUES ('4) Tier 6,000-8,000',8000,800,5,10000,1500,10);
INSERT INTO vpm_data.u_chg_new_cultivation_campaign_offer_spendingtier_bnb VALUES ('5) Tier 8,000-10,000',10000,1000,5,15000,2250,10);
INSERT INTO vpm_data.u_chg_new_cultivation_campaign_offer_spendingtier_bnb VALUES ('6) Tier 10,000 up',15000,1500,5,20000,3000,10);


CREATE temp TABLE new_cul_round_bnb(campaign_round date,start_date date,end_date date);
INSERT INTO new_cul_round_bnb VALUES ('2020-06-14','2020-06-19','2020-07-01');
INSERT INTO new_cul_round_bnb VALUES ('2020-06-29','2020-07-03','2020-07-15');
INSERT INTO new_cul_round_bnb VALUES ('2020-07-13','2020-07-17','2020-07-29');
INSERT INTO new_cul_round_bnb VALUES ('2020-07-27','2020-07-31','2020-08-12');
INSERT INTO new_cul_round_bnb VALUES ('2020-08-10','2020-08-14','2020-08-26');
INSERT INTO new_cul_round_bnb VALUES ('2020-08-24','2020-08-28','2020-09-09');
INSERT INTO new_cul_round_bnb VALUES ('2020-09-07','2020-09-11','2020-09-23');
INSERT INTO new_cul_round_bnb VALUES ('2020-09-21','2020-09-25','2020-10-07');
INSERT INTO new_cul_round_bnb VALUES ('2020-10-05','2020-10-09','2020-10-21');
INSERT INTO new_cul_round_bnb VALUES ('2020-10-19','2020-10-23','2020-11-04');
INSERT INTO new_cul_round_bnb VALUES ('2020-11-02','2020-11-06','2020-11-18');
INSERT INTO new_cul_round_bnb VALUES ('2020-11-16','2020-11-20','2020-12-02');
INSERT INTO new_cul_round_bnb VALUES ('2020-11-30','2020-12-04','2020-12-16');
INSERT INTO new_cul_round_bnb VALUES ('2020-12-14','2020-12-18','2020-12-30');
INSERT INTO new_cul_round_bnb VALUES ('2020-12-28','2021-01-08','2021-01-20');
INSERT INTO new_cul_round_bnb VALUES ('2021-01-18','2021-01-22','2021-02-03');
INSERT INTO new_cul_round_bnb VALUES ('2021-02-01','2021-02-05','2021-02-17');
INSERT INTO new_cul_round_bnb VALUES ('2021-02-15','2021-02-19','2021-03-03');

update vpm_data.u_chg_lead_gen_new_cultivation_campaign_spendingtier 
SET campaign_start_date = t.start_date
	,campaign_end_date = t.end_date
FROM new_cul_round_bnb t
WHERE vpm_data.u_chg_lead_gen_new_cultivation_campaign_spendingtier.campaign_sending_date = t.campaign_round
	AND vpm_data.u_chg_lead_gen_new_cultivation_campaign_spendingtier.bu_name = 'BNB';

DROP TABLE IF EXISTS txn_period_bnb;
CREATE temp TABLE txn_period_bnb as
SELECT customerid AS customer_id,transactiondate::date AS txn_date,spending AS txn_amt FROM analysis_data.salessku_hws2020 WHERE customerid <> '' AND spending > 0 AND transactiondate >= (SELECT min(start_date) FROM  new_cul_round_bnb)
UNION ALL SELECT customerid AS customer_id,transactiondate::date AS txn_date,spending AS txn_amt FROM analysis_data.salessku_hws2021 WHERE customerid <> '' AND spending > 0 AND transactiondate <= (SELECT max(end_date) FROM  new_cul_round_bnb);

DROP TABLE IF EXISTS lead_response_bnb;
SELECT t.customer_id,t.spending_tier,campaign_round,sum(txn_amt) AS r_ttl_spending INTO TEMP TABLE lead_response_bnb
FROM
(
	SELECT a.customer_id,a.spending_tier,b.* FROM vpm_data.u_chg_lead_gen_new_cultivation_campaign_spendingtier a
	LEFT JOIN new_cul_round_bnb b ON a.campaign_sending_date = b.campaign_round
	WHERE a.campaign_round <> 1
		AND a.bu_name = 'BNB'
) T
INNER JOIN txn_period_bnb c ON t.customer_id = c.customer_id
WHERE c.txn_date BETWEEN t.start_date AND t.end_date
GROUP BY t.customer_id,t.spending_tier,campaign_round;

DROP TABLE IF EXISTS lead_response_final_bnb;
SELECT a.*,CASE WHEN r_ttl_spending > tier_spend1 THEN 1 ELSE 0 END AS coupon1_flag 
	,CASE WHEN r_ttl_spending > tier_spend2 THEN 1 ELSE 0 END AS coupon2_flag 
INTO TEMP TABLE lead_response_final_bnb
FROM lead_response_bnb a
LEFT JOIN vpm_data.u_chg_new_cultivation_campaign_offer_spendingtier_bnb b
ON a.spending_tier = b.spending_tier;


update vpm_data.u_chg_lead_gen_new_cultivation_campaign_spendingtier
SET response_flag = 1
	,r_ttl_spending = t.r_ttl_spending
	,coupon1_flag = t.coupon1_flag
	,coupon2_flag = t.coupon2_flag
FROM lead_response_final_bnb t
WHERE vpm_data.u_chg_lead_gen_new_cultivation_campaign_spendingtier.customer_id = t.customer_id
	AND vpm_data.u_chg_lead_gen_new_cultivation_campaign_spendingtier.bu_name = 'BNB'
	AND vpm_data.u_chg_lead_gen_new_cultivation_campaign_spendingtier.campaign_sending_date = t.campaign_round;

SELECT count(*) FROM vpm_data.u_chg_lead_gen_new_cultivation_campaign_spendingtier
WHERE bu_name = 'BNB' AND campaign_round =2 

update vpm_data.u_chg_lead_gen_new_cultivation_campaign_spendingtier
SET response_flag = COALESCE(response_flag,0)
	,r_ttl_spending = COALESCE(r_ttl_spending,0)
	,coupon1_flag = COALESCE(coupon1_flag,0)
	,coupon2_flag = COALESCE(coupon2_flag,0);
>>>>>>> branch 'master' of https://bambt@bitbucket.org/satthapat/svoc.git
