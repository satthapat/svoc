

/*********************************************************************************/
/************************** SPENDING BY CONVENIENT TIME **************************/		
/*********************************************************************************/	

INSERT INTO VPM_DATA.SALESSKU_LOG_TIME(task,start_time) VALUES ('LOCATION Step 4_Cust Spending Time and Location',current_timestamp)
;

DROP TABLE IF EXISTS VPM_Backup.BRANCH_CLUSTER
;
SELECT 'CFR' AS PARTNER_CODE, B.BRANCH_CODE, BRANCH_CLUSTER
INTO VPM_Backup.BRANCH_CLUSTER
FROM VPM_Backup.GEO_TOPS_BRANCH_VISIT_CLUSTER A INNER JOIN VPM_DATA.CG_LOCATION_MASTER B ON A.BRANCHID::INT = B.BRANCHID
UNION
SELECT 'CFM' AS PARTNER_CODE, B.BRANCH_CODE, BRANCH_CLUSTER
FROM VPM_Backup.GEO_CFM_BRANCH_VISIT_CLUSTER A INNER JOIN VPM_DATA.CG_LOCATION_MASTER B ON A.BRANCHID::INT = B.BRANCHID
;

DROP TABLE IF EXISTS VPM_Backup.GEO_CUST_SVOC_TIME_SPENDING
;		 
SELECT	MEMBER_NUMBER
		/* Variety */
		,COUNT(DISTINCT A.PARTNER_CODE) AS CNT_BU
		,COUNT(DISTINCT A.BRANCH_CODE) AS CNT_BRANCH
		,SUM(TOTAL_SPEND) AS SUM_TOTAL_SPEND
		,COUNT(*) AS NUMBER_OF_ITEMS
		,SUM(CASE WHEN WORKING_DAY_FLAG = 1 THEN TOTAL_SPEND ELSE 0 END) AS SUM_WORKING_DAY_SPEND		
		,SUM(CASE WHEN WORKING_DAY_FLAG = 0 THEN TOTAL_SPEND ELSE 0 END) AS SUM_HOLIDAY_SPEND
		,SUM(CASE WHEN WORKING_DAY_OFFPEAK = 1 THEN TOTAL_SPEND ELSE 0 END) AS SUM_WORKING_DAY_OFFPEAK_SPEND
		/* Freq */
		,COUNT(DISTINCT TXN_DATE) AS CNT_DATE_SPEND
		,COUNT(DISTINCT CASE WHEN UPPER(B.BRANCH_CLUSTER) = 'MAINLY OFFICE WORKERS' 
							 THEN TXN_DATE ELSE NULL END) AS OFFICE_BR_CNT
		,COUNT(DISTINCT CASE WHEN UPPER(B.BRANCH_CLUSTER) = 'MAINLY BREAKFAST & LUNCH' 
							 THEN TXN_DATE ELSE NULL END) AS BREAKFAST_LUNCH_BR_CNT
		,COUNT(DISTINCT CASE WHEN UPPER(B.BRANCH_CLUSTER) = 'MAINLY OFFICE WORKERS' 
								  AND WORKING_DAY_FLAG = 1
							 THEN TXN_DATE ELSE NULL END) AS OFFICE_BR_CNT_WORKING_DAY
		,COUNT(DISTINCT CASE WHEN UPPER(B.BRANCH_CLUSTER) = 'MAINLY BREAKFAST & LUNCH' 
								  AND WORKING_DAY_FLAG = 1
							 THEN TXN_DATE ELSE NULL END) AS BREAKFAST_LUNCH_BR_CNT_WORKING_DAY
		,COUNT(DISTINCT CASE WHEN WEEKDAY_FLAG = 1 THEN TXN_DATE ELSE NULL END) AS CNT_WEEKDAY_SPEND
		,COUNT(DISTINCT CASE WHEN WORKING_DAY_FLAG = 1 THEN TXN_DATE ELSE NULL END) AS CNT_WORKING_DAY_SPEND
		,COUNT(DISTINCT CASE WHEN WORKING_DAY_FLAG = 0 THEN TXN_DATE ELSE NULL END) AS CNT_HOLIDAY_SPEND
		,COUNT(DISTINCT CASE WHEN IS_HOLIDAY = 1 THEN TXN_DATE ELSE NULL END) AS CNT_PUBLIC_HOLIDAY_SPEND
		,COUNT(DISTINCT CASE WHEN WORKING_DAY_OFFPEAK = 1 THEN TXN_DATE ELSE NULL END) AS CNT_WORKING_DAY_OFFPEAK_SPEND
		,COUNT(DISTINCT CASE WHEN WORKING_DAY_FLAG = 1 AND WORKING_DAY_OFFPEAK = 0 THEN TXN_DATE ELSE NULL END) AS CNT_WORKING_DAY_ONPEAK_SPEND
		,CAST(NULL AS FLOAT) AS OFF_PEAK_VISIT_RATIO
		,CAST(NULL AS FLOAT) AS WORKING_DAY_RATIO
		,CAST(NULL AS FLOAT) AS PUBLIC_HOLIDAY_RATIO
		,CAST(NULL AS VARCHAR(50)) AS PREFERRED_TIME_SEGMENT
INTO VPM_Backup.GEO_CUST_SVOC_TIME_SPENDING
FROM VPM_BACKUP.GEO_ALL_BU_VISIT_6M_LOCATION A
LEFT JOIN VPM_Backup.BRANCH_CLUSTER B ON A.PARTNER_CODE = B.PARTNER_CODE AND A.BRANCH_CODE = B.BRANCH_CODE
GROUP BY MEMBER_NUMBER
HAVING SUM(CASE WHEN TOTAL_SPEND = 0 THEN 1 ELSE TOTAL_SPEND END) > 0
;


UPDATE VPM_Backup.GEO_CUST_SVOC_TIME_SPENDING
SET OFF_PEAK_VISIT_RATIO = CNT_WORKING_DAY_OFFPEAK_SPEND*1.0/CNT_DATE_SPEND*1.0,
	WORKING_DAY_RATIO = CNT_WORKING_DAY_SPEND*1.0/CNT_DATE_SPEND*1.0,
	PUBLIC_HOLIDAY_RATIO = CNT_PUBLIC_HOLIDAY_SPEND*1.0/CNT_DATE_SPEND*1.0
;
	
UPDATE VPM_Backup.GEO_CUST_SVOC_TIME_SPENDING
SET PREFERRED_TIME_SEGMENT = CASE WHEN CNT_DATE_SPEND < 5 THEN 'LOW ACTIVITY'								  
								  WHEN OFF_PEAK_VISIT_RATIO >= 0.5 THEN 'MAINLY WORKING DAY OFF-PEAK'
								  WHEN OFF_PEAK_VISIT_RATIO >= 0.3 AND CNT_WORKING_DAY_OFFPEAK_SPEND >= 5 THEN 'MAINLY WORKING DAY OFF-PEAK'
								  WHEN OFF_PEAK_VISIT_RATIO >= 0.3 OR CNT_WORKING_DAY_OFFPEAK_SPEND >= 5 THEN 'ANY TIME'
								  /*WHEN PUBLIC_HOLIDAY_RATIO >= 0.5 THEN 'MAINLY PUBLIC HOLIDAY'*/
								  WHEN WORKING_DAY_RATIO >= 0.5 THEN 'MAINLY WORKING DAY ON-PEAK'
								  WHEN WORKING_DAY_RATIO < 0.5 THEN 'MAINLY HOLIDAY'
								  ELSE 'X' END
;
		  


/***********************************************************************************/
/************************** SPENDING BY BRANCHES PROVINCE **************************/		
/***********************************************************************************/
--DROP TABLE IF EXISTS TEMP_LOCATION;
SELECT DISTINCT "REGION1" AS REGION1,
				CASE WHEN "REGION2" = 'Bangkok' THEN 'Bangkok' 
					 WHEN "REGION2" IN ('Nonthaburi','Pathum Thani','Nakhon Pathom','Samut Prakarn','Samut Prakan','Samut Sakhon') THEN 'Suburb' 
				ELSE "REGION1" END AS SUB_REGION1,
				CASE WHEN "REGION2" = 'Chonburi' THEN 'Chon Buri'
					 WHEN "REGION2" = 'Lopburi' THEN 'Lop Buri'
					 WHEN "REGION2" = 'Phang Nga' THEN 'Phangnga'
					 WHEN "REGION2" = 'Samut Prakan' THEN 'Samut Prakarn'
					 WHEN "REGION2" = 'Prachinburi' THEN 'Prachin Buri'
					 WHEN "REGION2" = 'Nong Bua Lamphu' THEN 'Nong Bua Lam Phu'
				ELSE "REGION2" END AS REGION2
INTO TEMP TABLE TEMP_LOCATION
FROM VPM_DATA.GeoPC
;

/************************************* PROVINCE *************************************/

--DROP TABLE IF EXISTS TEMP_F_CUST_SVOC_PROVINCE_VISIT;
SELECT MEMBER_NUMBER, MAX(CASE WHEN RANK_PROVINCE = 1 THEN REGION2 END) AS MOST_VISIT_PROVINCE1
				 , MAX(CASE WHEN RANK_PROVINCE = 1 THEN CNT_DAYS_VISIT END) AS DAYS_VISIT_PROVINCE1
				 , MAX(CASE WHEN RANK_PROVINCE = 1 THEN TOTAL_SPEND END) AS TOTAL_SPEND_PROVINCE1
				 , MAX(CASE WHEN RANK_PROVINCE = 2 THEN REGION2 END) AS MOST_VISIT_PROVINCE2
				 , MAX(CASE WHEN RANK_PROVINCE = 2 THEN CNT_DAYS_VISIT END) AS DAYS_VISIT_PROVINCE2
				 , MAX(CASE WHEN RANK_PROVINCE = 2 THEN TOTAL_SPEND END) AS TOTAL_SPEND_PROVINCE2
				 , MAX(RANK_PROVINCE) AS CNT_PROVINCE
INTO TEMP TABLE TEMP_F_CUST_SVOC_PROVINCE_VISIT
FROM (
	SELECT *, ROW_NUMBER() OVER(PARTITION BY MEMBER_NUMBER ORDER BY CNT_DAYS_VISIT DESC, TOTAL_SPEND DESC NULLS LAST) AS RANK_PROVINCE
	FROM (
		SELECT MEMBER_NUMBER, REGION2
			   ,COUNT(DISTINCT TXN_DATE) AS CNT_DAYS_VISIT
			   ,SUM(TOTAL_SPEND) AS TOTAL_SPEND
		FROM VPM_BACKUP.GEO_ALL_BU_VISIT_6M_LOCATION
		WHERE COALESCE(REGION2,'') <> ''  
		GROUP BY MEMBER_NUMBER, REGION2) T1) T2
GROUP BY MEMBER_NUMBER
;

/************************************* SUB REGION *************************************/

--DROP TABLE IF EXISTS TEMP_SUB_REGION1;
SELECT A.MEMBER_NUMBER
	  ,B.SUB_REGION1 AS MOST_VISIT_SUB_REGION1
	  ,C.CNT_DAYS_VISIT AS DAYS_VISIT_SUB_REGION1
	  ,C.TOTAL_SPEND AS TOTAL_SPEND_SUB_REGION1
INTO TEMP TABLE TEMP_SUB_REGION1
FROM TEMP_F_CUST_SVOC_PROVINCE_VISIT A
LEFT JOIN TEMP_LOCATION B
ON A.MOST_VISIT_PROVINCE1 = B.REGION2
LEFT JOIN (
			SELECT MEMBER_NUMBER
				   ,(CASE WHEN REGION2 = 'Bangkok' THEN 'Bangkok' 
						  WHEN REGION2 IN ('Nonthaburi','Pathum Thani','Nakhon Pathom','Samut Prakarn','Samut Sakhon') THEN 'Suburb' 
						  ELSE REGION1 END) AS SUB_REGION
				   ,COUNT(DISTINCT TXN_DATE) AS CNT_DAYS_VISIT
				   ,SUM(TOTAL_SPEND) AS TOTAL_SPEND
			FROM VPM_BACKUP.GEO_ALL_BU_VISIT_6M_LOCATION
			WHERE COALESCE(REGION1,'') <> ''
			GROUP BY MEMBER_NUMBER
				   ,(CASE WHEN REGION2 = 'Bangkok' THEN 'Bangkok' 
						  WHEN REGION2 IN ('Nonthaburi','Pathum Thani','Nakhon Pathom','Samut Prakarn','Samut Sakhon') THEN 'Suburb' 
						  ELSE REGION1 END)
		  ) C
ON A.MEMBER_NUMBER = C.MEMBER_NUMBER AND
   B.SUB_REGION1 = C.SUB_REGION
;

--DROP TABLE IF EXISTS TEMP_SUB_REGION2;
SELECT A.MEMBER_NUMBER
	  ,B.SUB_REGION1 AS MOST_VISIT_SUB_REGION2
	  ,C.CNT_DAYS_VISIT AS DAYS_VISIT_SUB_REGION2
	  ,C.TOTAL_SPEND AS TOTAL_SPEND_SUB_REGION2
INTO TEMP TABLE TEMP_SUB_REGION2
FROM TEMP_F_CUST_SVOC_PROVINCE_VISIT A
LEFT JOIN TEMP_LOCATION B
ON A.MOST_VISIT_PROVINCE2 = B.REGION2
LEFT JOIN (
			SELECT MEMBER_NUMBER
				   ,(CASE WHEN REGION2 = 'Bangkok' THEN 'Bangkok' 
						  WHEN REGION2 IN ('Nonthaburi','Pathum Thani','Nakhon Pathom','Samut Prakarn','Samut Sakhon') THEN 'Suburb' 
						  ELSE REGION1 END) AS SUB_REGION
				   ,COUNT(DISTINCT TXN_DATE) AS CNT_DAYS_VISIT
				   ,SUM(TOTAL_SPEND) AS TOTAL_SPEND
			FROM VPM_BACKUP.GEO_ALL_BU_VISIT_6M_LOCATION
			WHERE COALESCE(REGION1,'') <> ''
			GROUP BY MEMBER_NUMBER
				   ,(CASE WHEN REGION2 = 'Bangkok' THEN 'Bangkok' 
						  WHEN REGION2 IN ('Nonthaburi','Pathum Thani','Nakhon Pathom','Samut Prakarn','Samut Sakhon') THEN 'Suburb' 
						  ELSE REGION1 END)
		  ) C
ON A.MEMBER_NUMBER = C.MEMBER_NUMBER AND
   B.SUB_REGION1 = C.SUB_REGION
;

--DROP TABLE IF EXISTS TEMP_F_CUST_SVOC_SUB_REGION_VISIT;
SELECT A.*
	  ,B.MOST_VISIT_SUB_REGION2
	  ,B.DAYS_VISIT_SUB_REGION2
	  ,B.TOTAL_SPEND_SUB_REGION2
	  ,C.CNT_SUB_REGION
INTO TEMP TABLE TEMP_F_CUST_SVOC_SUB_REGION_VISIT
FROM TEMP_SUB_REGION1 A
LEFT JOIN TEMP_SUB_REGION2 B
ON A.MEMBER_NUMBER = B.MEMBER_NUMBER
LEFT JOIN (
			SELECT MEMBER_NUMBER
				  ,COUNT(DISTINCT (CASE WHEN REGION2 = 'Bangkok' THEN 'Bangkok' 
										WHEN REGION2 IN ('Nonthaburi','Pathum Thani','Nakhon Pathom','Samut Prakarn','Samut Prakan','Samut Sakhon') THEN 'Suburb' 
									ELSE REGION1 END)) AS CNT_SUB_REGION
			FROM VPM_BACKUP.GEO_ALL_BU_VISIT_6M_LOCATION
			WHERE COALESCE(REGION1,'') <> '' 
			GROUP BY MEMBER_NUMBER
		  ) C
ON A.MEMBER_NUMBER = C.MEMBER_NUMBER
;

/************************************* REGION *************************************/

--DROP TABLE IF EXISTS TEMP_REGION1;
SELECT A.MEMBER_NUMBER
	  ,B.REGION1 AS MOST_VISIT_REGION1
	  ,C.CNT_DAYS_VISIT AS DAYS_VISIT_REGION1
	  ,C.TOTAL_SPEND AS TOTAL_SPEND_REGION1
INTO TEMP TABLE TEMP_REGION1
FROM TEMP_F_CUST_SVOC_SUB_REGION_VISIT A
LEFT JOIN (SELECT DISTINCT REGION1,SUB_REGION1 FROM TEMP_LOCATION) B
ON A.MOST_VISIT_SUB_REGION1 = B.SUB_REGION1
LEFT JOIN (
			SELECT MEMBER_NUMBER
				   ,REGION1
				   ,COUNT(DISTINCT TXN_DATE) AS CNT_DAYS_VISIT
				   ,SUM(TOTAL_SPEND) AS TOTAL_SPEND
			FROM VPM_BACKUP.GEO_ALL_BU_VISIT_6M_LOCATION
			WHERE COALESCE(REGION1,'') <> ''
			GROUP BY MEMBER_NUMBER,REGION1
		  ) C
ON A.MEMBER_NUMBER = C.MEMBER_NUMBER AND
   B.REGION1 = C.REGION1
;

--DROP TABLE IF EXISTS TEMP_REGION2;
SELECT A.MEMBER_NUMBER
	  ,B.REGION1 AS MOST_VISIT_REGION2
	  ,C.CNT_DAYS_VISIT AS DAYS_VISIT_REGION2
	  ,C.TOTAL_SPEND AS TOTAL_SPEND_REGION2
INTO TEMP TABLE TEMP_REGION2
FROM TEMP_F_CUST_SVOC_SUB_REGION_VISIT A
LEFT JOIN (SELECT DISTINCT REGION1,SUB_REGION1 FROM TEMP_LOCATION) B
ON A.MOST_VISIT_SUB_REGION2 = B.SUB_REGION1
LEFT JOIN (
			SELECT MEMBER_NUMBER
				   ,REGION1
				   ,COUNT(DISTINCT TXN_DATE) AS CNT_DAYS_VISIT
				   ,SUM(TOTAL_SPEND) AS TOTAL_SPEND
			FROM VPM_BACKUP.GEO_ALL_BU_VISIT_6M_LOCATION
			WHERE COALESCE(REGION1,'') <> ''
			GROUP BY MEMBER_NUMBER,REGION1
		  ) C
ON A.MEMBER_NUMBER = C.MEMBER_NUMBER AND
   B.REGION1 = C.REGION1
;

--DROP TABLE IF EXISTS TEMP_F_CUST_SVOC_REGION_VISIT;
SELECT A.*
	  ,B.MOST_VISIT_REGION2
	  ,B.DAYS_VISIT_REGION2
	  ,B.TOTAL_SPEND_REGION2
	  ,C.CNT_REGION
INTO TEMP TABLE TEMP_F_CUST_SVOC_REGION_VISIT
FROM TEMP_REGION1 A
LEFT JOIN TEMP_REGION2 B
ON A.MEMBER_NUMBER = B.MEMBER_NUMBER
LEFT JOIN (
			SELECT MEMBER_NUMBER
				  ,COUNT(DISTINCT REGION1) AS CNT_REGION
			FROM VPM_BACKUP.GEO_ALL_BU_VISIT_6M_LOCATION
			WHERE COALESCE(REGION1,'') <> '' 
			GROUP BY MEMBER_NUMBER
		  ) C
ON A.MEMBER_NUMBER = C.MEMBER_NUMBER
;

/************************************* POSTAL *************************************/

--DROP TABLE IF EXISTS TEMP_F_CUST_SVOC_POSTAL_VISIT;
SELECT MEMBER_NUMBER, MAX(CASE WHEN RANK_POSTAL_CODE = 1 THEN POSTAL_CODE END) AS MOST_VISIT_POSTAL_CODE1
				 , MAX(CASE WHEN RANK_POSTAL_CODE = 1 THEN CNT_DAYS_VISIT END) AS DAYS_VISIT_POSTAL_CODE1
				 , MAX(CASE WHEN RANK_POSTAL_CODE = 1 THEN TOTAL_SPEND END) AS TOTAL_SPEND_POSTAL_CODE1
				 , MAX(CASE WHEN RANK_POSTAL_CODE = 2 THEN POSTAL_CODE END) AS MOST_VISIT_POSTAL_CODE2
				 , MAX(CASE WHEN RANK_POSTAL_CODE = 2 THEN CNT_DAYS_VISIT END) AS DAYS_VISIT_POSTAL_CODE2
				 , MAX(CASE WHEN RANK_POSTAL_CODE = 2 THEN TOTAL_SPEND END) AS TOTAL_SPEND_POSTAL_CODE2
				 , MAX(RANK_POSTAL_CODE) AS CNT_POSTAL
INTO TEMP TABLE TEMP_F_CUST_SVOC_POSTAL_VISIT
FROM (
	SELECT *, ROW_NUMBER() OVER(PARTITION BY MEMBER_NUMBER ORDER BY CNT_DAYS_VISIT DESC, TOTAL_SPEND DESC NULLS LAST) AS RANK_POSTAL_CODE
	FROM (
		SELECT MEMBER_NUMBER, POSTAL_CODE
			   ,COUNT(DISTINCT TXN_DATE) AS CNT_DAYS_VISIT
			   ,SUM(TOTAL_SPEND) AS TOTAL_SPEND
		FROM VPM_BACKUP.GEO_ALL_BU_VISIT_6M_LOCATION
		WHERE COALESCE(POSTAL_CODE,'') <> ''
		GROUP BY MEMBER_NUMBER, POSTAL_CODE) T1) T2
GROUP BY MEMBER_NUMBER
;

/************************************* COMBINE *************************************/

DROP TABLE IF EXISTS VPM_BACKUP.GEO_CUST_SVOC_LOCATION_VISIT
;
				
SELECT T1.*, MOST_VISIT_SUB_REGION1, MOST_VISIT_SUB_REGION2
		, DAYS_VISIT_SUB_REGION1, DAYS_VISIT_SUB_REGION2
		, TOTAL_SPEND_SUB_REGION1, TOTAL_SPEND_SUB_REGION2, CNT_SUB_REGION
		, MOST_VISIT_REGION1, MOST_VISIT_REGION2
		, DAYS_VISIT_REGION1, DAYS_VISIT_REGION2
		, TOTAL_SPEND_REGION1, TOTAL_SPEND_REGION2, CNT_REGION
		, MOST_VISIT_PROVINCE1, MOST_VISIT_PROVINCE2
		, DAYS_VISIT_PROVINCE1, DAYS_VISIT_PROVINCE2
		, TOTAL_SPEND_PROVINCE1, TOTAL_SPEND_PROVINCE2, CNT_PROVINCE
		, MOST_VISIT_POSTAL_CODE1, MOST_VISIT_POSTAL_CODE2
		, DAYS_VISIT_POSTAL_CODE1, DAYS_VISIT_POSTAL_CODE2
		, TOTAL_SPEND_POSTAL_CODE1, TOTAL_SPEND_POSTAL_CODE2, CNT_POSTAL
		,CAST(NULL AS FLOAT) AS MOST_VISIT_SUB_REGION_RATIO
		,CAST(NULL AS FLOAT) AS MOST_VISIT_REGION_RATIO
		,CAST(NULL AS FLOAT ) AS MOST_VISIT_PROVINCE_RATIO
		,CAST(NULL AS FLOAT) AS MOST_VISIT_POSTAL_CODE_RATIO
		,CAST(NULL AS VARCHAR(50)) AS CUST_LOCATION_SEGMENT
INTO VPM_BACKUP.GEO_CUST_SVOC_LOCATION_VISIT
FROM (								
	SELECT MEMBER_NUMBER
		  ,COUNT(DISTINCT TXN_DATE) AS CNT_DAYS_VISIT
		  ,COUNT(DISTINCT CASE WHEN MALL_BRANCHID IS NOT NULL THEN MALL_BRANCH_NAME ELSE CAST(BRANCH_CODE AS VARCHAR(50)) END) AS CNT_BRANCH_VISIT
	FROM VPM_BACKUP.GEO_ALL_BU_VISIT_6M_LOCATION
	GROUP BY MEMBER_NUMBER) T1
INNER JOIN TEMP_F_CUST_SVOC_SUB_REGION_VISIT T2 ON T1.MEMBER_NUMBER = T2.MEMBER_NUMBER	
INNER JOIN TEMP_F_CUST_SVOC_REGION_VISIT T3 ON T1.MEMBER_NUMBER = T3.MEMBER_NUMBER		
INNER JOIN TEMP_F_CUST_SVOC_PROVINCE_VISIT T4 ON T1.MEMBER_NUMBER = T4.MEMBER_NUMBER		
INNER JOIN TEMP_F_CUST_SVOC_POSTAL_VISIT T5 ON T1.MEMBER_NUMBER = T5.MEMBER_NUMBER
;

	
UPDATE VPM_BACKUP.GEO_CUST_SVOC_LOCATION_VISIT
SET MOST_VISIT_SUB_REGION_RATIO = CASE WHEN DAYS_VISIT_SUB_REGION2 IS NULL THEN 1
									   ELSE DAYS_VISIT_SUB_REGION1*1.0/CNT_DAYS_VISIT*1.0 END,
	MOST_VISIT_REGION_RATIO = CASE WHEN DAYS_VISIT_REGION2 IS NULL THEN 1
									   ELSE DAYS_VISIT_REGION1*1.0/CNT_DAYS_VISIT*1.0 END,
	MOST_VISIT_PROVINCE_RATIO = CASE WHEN DAYS_VISIT_PROVINCE2 IS NULL THEN 1
									   ELSE DAYS_VISIT_PROVINCE1*1.0/CNT_DAYS_VISIT*1.0 END,
	MOST_VISIT_POSTAL_CODE_RATIO = CASE WHEN DAYS_VISIT_POSTAL_CODE2 IS NULL THEN 1
									   ELSE DAYS_VISIT_POSTAL_CODE1*1.0/CNT_DAYS_VISIT*1.0 END
;
	
UPDATE VPM_BACKUP.GEO_CUST_SVOC_LOCATION_VISIT
SET CUST_LOCATION_SEGMENT = CASE WHEN MOST_VISIT_SUB_REGION1 = 'Bangkok' 
									  AND (MOST_VISIT_SUB_REGION_RATIO >= 0.6) THEN 'MAINLY BANGKOK'
								 WHEN MOST_VISIT_SUB_REGION1 = 'Suburb' AND MOST_VISIT_SUB_REGION_RATIO >= 0.6 THEN 'MAINLY SUBURB'
								 WHEN MOST_VISIT_PROVINCE1 IN ('Nonthaburi','Pathum Thani','Nakhon Pathom','Samut Prakarn','Samut Sakhon')
									  AND MOST_VISIT_PROVINCE_RATIO >= 0.6 THEN 'MAINLY SUBURB'
								 WHEN MOST_VISIT_PROVINCE1 IN ('Chiang Mai','Songkhla','Nakhon Ratchasima','Chon Buri','Khon Kaen','Phuket','Prachuap Khiri Khan','Udon Thani') 
									  AND MOST_VISIT_PROVINCE_RATIO >= 0.6 THEN 'MAINLY BIG CITY UPC'
								 WHEN MOST_VISIT_SUB_REGION1 NOT IN ('Bangkok','Suburb')
									  AND MOST_VISIT_PROVINCE1 NOT IN ('Chiang Mai','Songkhla','Nakhon Ratchasima','Chon Buri','Khon Kaen','Phuket','Prachuap Khiri Khan','Udon Thani') 
									  AND MOST_VISIT_SUB_REGION_RATIO >= 0.6 THEN 'MAINLY UPC'
								 WHEN MOST_VISIT_REGION_RATIO < 0.6 THEN 'CROSS REGION SPENDING'
								 WHEN MOST_VISIT_PROVINCE_RATIO < 0.6 THEN 'CROSS PROVINCE SPENDING'
								 ELSE 'X' END
;
								 
								
UPDATE VPM_DATA.SALESSKU_LOG_TIME SET end_time = current_timestamp WHERE task = 'LOCATION Step 4_Cust Spending Time and Location' AND EXTRACT(MONTH FROM start_time) = EXTRACT(MONTH FROM current_timestamp)
;