/*********************************************************************************/
/*************** LOCATION VISIT (SAME OR DIFFERENT BRANCHES VISIT) ***************/	
/*********************************************************************************/

INSERT INTO VPM_DATA.SALESSKU_LOG_TIME(task,start_time) VALUES ('LOCATION Step 3_Cust CrossBUs Score and Visited BUs',current_timestamp)
;
/** SET NEW PERIOD **/
--DROP TABLE IF EXISTS GEO_ALL_BU_VISIT_6M_LOCATION_STAGING1;

CREATE TEMP TABLE GEO_ALL_BU_VISIT_6M_LOCATION_STAGING1 AS
SELECT	MEMBER_NUMBER,TXN_DATE,PARTNER_CODE,BRANCH_CODE,WEEKDAY_FLAG,WORKING_DAY_FLAG,IS_HOLIDAY, 
				MAX(WORKING_DAY_OFFPEAK) AS WORKING_DAY_OFFPEAK,
				SUM(TXN_AMT) AS TOTAL_SPEND
FROM VPM_DATA.PM_ALL_SALESSKU_1YR
WHERE PARTNER_CODE IS NOT NULL AND TXN_DATE >= dateadd(MONTH,6,vpm_data.get_start_date(current_timestamp)) AND TXN_DATE <= vpm_data.get_end_date(current_timestamp)
GROUP BY MEMBER_NUMBER,TXN_DATE,PARTNER_CODE,BRANCH_CODE,WEEKDAY_FLAG,WORKING_DAY_FLAG,IS_HOLIDAY;


DROP TABLE IF EXISTS VPM_Backup.GEO_ALL_BU_VISIT_6M_LOCATION
;
	CREATE TABLE VPM_Backup.GEO_ALL_BU_VISIT_6M_LOCATION AS
	SELECT A.*,B.BranchEnglishName
			  ,(CASE WHEN COALESCE(B.Latitude,0) = 0 THEN B.MALL_LATITUDE ELSE B.Latitude END) AS Latitude
			  ,(CASE WHEN COALESCE(B.Longitude,0) = 0 THEN B.MALL_LONGITUDE ELSE B.Longitude END) AS Longitude
			  ,(CASE WHEN COALESCE(B.POSTAL_CODE,'') = '' THEN B.MALL_POSTAL_CODE ELSE B.POSTAL_CODE END) AS POSTAL_CODE
			  ,(CASE WHEN COALESCE(B.REGION1,'') = '' THEN B.MALL_REGION1 ELSE B.REGION1 END) AS REGION1
			  ,(CASE WHEN COALESCE(B.REGION2,'') = '' THEN B.MALL_REGION2 ELSE B.REGION2 END) AS REGION2
			  /* CDS AND RBS ARE DEFINED AS MALL*/
			  ,B.MALL_BUCODE
			  ,B.MALL_BUID
			  ,B.MALL_BRANCHID
			  ,B.MALL_BRANCH_NAME
			  ,B.MALL_LATITUDE
			  ,B.MALL_LONGITUDE
			  ,B.MALL_POSTAL_CODE
			  ,B.MALL_REGION1
			  ,B.MALL_REGION2
FROM GEO_ALL_BU_VISIT_6M_LOCATION_STAGING1 A
LEFT JOIN VPM_Backup.GEO_BU_LOCATION_MASTER B ON A.PARTNER_CODE = B.PARTNER_CODE
												AND A.Branch_CODE = B.Branch_CODE
;


/*** FIND OVERALL STORES VISIT (Any BUs inside CDS/RBS) ***/
DROP TABLE IF EXISTS VPM_Backup.GEO_CUST_SVOC_ALL_BU_VISIT_IN_MALL_6M;
SELECT T1.*
	,T2.CNT_DAYS_CROSS_BU_SAME_DAY
	,T2.MAX_NO_OF_BU_VISIT_A_DAY
	,T2.AVG_NO_OF_BU_VISIT_A_DAY
	,CAST(NULL AS VARCHAR(50)) AS MALLS_VISIT_SEGMENT
	,CAST(NULL AS VARCHAR(50)) AS MOST_VISITED_MALL
	,CAST(NULL AS VARCHAR(50)) AS MOST_VISITED_MALL_RATIO
	,CAST(NULL AS FLOAT) AS SAME_DAY_CROSS_BU_RATIO
	,CAST(NULL AS FLOAT) AS CROSS_BU_SCORE
	,CAST(NULL AS FLOAT) AS NTILE_CROSS_BU_SCORE
	,CAST(NULL AS FLOAT) AS SCALED_VAR1
	,CAST(NULL AS FLOAT) AS SCALED_VAR2
	,CAST(NULL AS FLOAT) AS SCALED_VAR3
	,CAST(NULL AS FLOAT) AS SCALED_VAR4
	,CAST(NULL AS FLOAT) AS SCALED_VAR5
	,CAST(NULL AS FLOAT) AS SCALED_VAR6
INTO VPM_Backup.GEO_CUST_SVOC_ALL_BU_VISIT_IN_MALL_6M
FROM (
		SELECT	MEMBER_NUMBER
				,COUNT(DISTINCT PARTNER_CODE) AS CNT_BU_SPENT_IN_MALL		
				,COUNT(DISTINCT CASE WHEN MALL_BUCODE = 'CDS' THEN TXN_DATE ELSE NULL END) AS TOTAL_DAYS_VISIT_CDS
				,MAX(CASE WHEN MALL_BUCODE = 'CDS' THEN TXN_DATE ELSE NULL END) AS DATE_LAST_VISIT_CDS
				,COUNT(DISTINCT CASE WHEN PARTNER_CODE = 'CDS' THEN TXN_DATE ELSE NULL END) AS TOTAL_DAYS_SPENT_CDS
				,MAX(CASE WHEN PARTNER_CODE = 'CDS' THEN TXN_DATE ELSE NULL END) AS DATE_LAST_SPENT_CDS
				,COUNT(DISTINCT CASE WHEN MALL_BUCODE = 'RBS' THEN TXN_DATE ELSE NULL END) AS TOTAL_DAYS_VISIT_RBS
				,MAX(CASE WHEN MALL_BUCODE = 'RBS' THEN TXN_DATE ELSE NULL END) AS DATE_LAST_VISIT_RBS		
				,COUNT(DISTINCT CASE WHEN PARTNER_CODE = 'RBS' THEN TXN_DATE ELSE NULL END) AS TOTAL_DAYS_SPENT_RBS
				,MAX(CASE WHEN PARTNER_CODE = 'RBS' THEN TXN_DATE ELSE NULL END) AS DATE_LAST_SPENT_RBS
				,COUNT(DISTINCT TXN_DATE) AS TOTAL_DAYS_VISIT_MALLS
				,COUNT(DISTINCT CASE WHEN PARTNER_CODE IN ('CDS','RBS') THEN TXN_DATE ELSE NULL END) AS TOTAL_DAYS_SPENT_IN_DEPT_STORE
				,COUNT(DISTINCT CASE WHEN PARTNER_CODE IN ('CDS','RBS') THEN TXN_DATE ELSE NULL END)*1.0
				/COUNT(DISTINCT TXN_DATE)*1.0 AS RATIO_SPENT_IN_DEPT_STORE
				,COUNT(DISTINCT CASE WHEN PARTNER_CODE NOT IN ('CDS','RBS') AND MALL_BUCODE = 'CDS' 
									 THEN TXN_DATE ELSE NULL END) AS TOTAL_DAYS_SPENT_BU_IN_CDS
				,COUNT(DISTINCT CASE WHEN PARTNER_CODE NOT IN ('CDS','RBS') AND MALL_BUCODE = 'RBS' 
									 THEN TXN_DATE ELSE NULL END) AS TOTAL_DAYS_SPENT_BU_IN_RBS						
		FROM VPM_Backup.GEO_ALL_BU_VISIT_6M_LOCATION
		WHERE MALL_BUCODE IS NOT NULL
		GROUP BY MEMBER_NUMBER) T1
LEFT JOIN 
/*** FIND BUs VISITING A DAY ***/
(
	SELECT MEMBER_NUMBER
		,SUM(CASE WHEN CNT_BU_A_DAY >= 2 THEN 1 ELSE 0 END) AS CNT_DAYS_CROSS_BU_SAME_DAY
		,MAX(CNT_BU_A_DAY) AS MAX_NO_OF_BU_VISIT_A_DAY
		,AVG(CNT_BU_A_DAY*1.0) AS AVG_NO_OF_BU_VISIT_A_DAY
	FROM (
		SELECT MEMBER_NUMBER, TXN_DATE, COUNT(PARTNER_CODE) AS CNT_BU_A_DAY
		FROM VPM_Backup.GEO_ALL_BU_VISIT_6M_LOCATION
		WHERE MALL_BUCODE IS NOT NULL
		GROUP BY MEMBER_NUMBER, TXN_DATE) A
	GROUP BY MEMBER_NUMBER
) T2 
ON T1.MEMBER_NUMBER = T2.MEMBER_NUMBER
;



UPDATE VPM_Backup.GEO_CUST_SVOC_ALL_BU_VISIT_IN_MALL_6M
SET MALLS_VISIT_SEGMENT = CASE  WHEN TOTAL_DAYS_SPENT_CDS > 0 AND TOTAL_DAYS_SPENT_RBS > 0 THEN 'A.SPENT BOTH CDS AND RBS'
								WHEN TOTAL_DAYS_SPENT_CDS > 0 THEN 'B.SPENT CDS ONLY'
								WHEN TOTAL_DAYS_SPENT_RBS > 0 THEN 'C.SPENT RBS ONLY'
								WHEN TOTAL_DAYS_SPENT_CDS = 0 AND TOTAL_DAYS_SPENT_RBS = 0
									 AND TOTAL_DAYS_VISIT_CDS > 0
									 AND TOTAL_DAYS_VISIT_RBS > 0 THEN 'D.VISIT BOTH CDS AND RBS'
								WHEN TOTAL_DAYS_SPENT_CDS = 0 AND TOTAL_DAYS_VISIT_CDS > 0
									 THEN 'E.VISIT CDS ONLY'
								WHEN TOTAL_DAYS_SPENT_RBS = 0 AND TOTAL_DAYS_VISIT_RBS > 0
									 THEN 'F.VISIT RBS ONLY'
								ELSE '' END,
	MOST_VISITED_MALL = CASE WHEN TOTAL_DAYS_VISIT_CDS+TOTAL_DAYS_VISIT_RBS > 0 THEN 
							 CASE WHEN TOTAL_DAYS_VISIT_CDS >= TOTAL_DAYS_VISIT_RBS THEN 'CDS'
								  WHEN TOTAL_DAYS_VISIT_CDS < TOTAL_DAYS_VISIT_RBS THEN 'RBS' END
							 ELSE 'Non-Mall BUs' END,
	MOST_VISITED_MALL_RATIO = CASE WHEN TOTAL_DAYS_VISIT_CDS+TOTAL_DAYS_VISIT_RBS > 0 THEN 
								   CASE	WHEN TOTAL_DAYS_VISIT_CDS >= TOTAL_DAYS_VISIT_RBS THEN TOTAL_DAYS_VISIT_CDS*1.0/(TOTAL_DAYS_VISIT_CDS+TOTAL_DAYS_VISIT_RBS)*1.0
										WHEN TOTAL_DAYS_VISIT_CDS < TOTAL_DAYS_VISIT_RBS THEN TOTAL_DAYS_VISIT_RBS*1.0/(TOTAL_DAYS_VISIT_CDS+TOTAL_DAYS_VISIT_RBS)*1.0 END
								   ELSE 0 END,
	SAME_DAY_CROSS_BU_RATIO = CNT_DAYS_CROSS_BU_SAME_DAY*1.0/TOTAL_DAYS_VISIT_MALLS*1.0
;

/*** CROSS BUs Scoring ***/
DROP TABLE IF EXISTS var_percentile;
CREATE temp TABLE var_percentile AS
SELECT DISTINCT MEMBER_NUMBER,PERCENTILE_DISC (0.01) WITHIN GROUP (ORDER BY TOTAL_DAYS_VISIT_MALLS DESC NULLS LAST) OVER () AS var1 
		,PERCENTILE_DISC (0.01) WITHIN GROUP (ORDER BY CNT_BU_SPENT_IN_MALL DESC NULLS LAST) OVER () AS var2
		,PERCENTILE_DISC (0.01) WITHIN GROUP (ORDER BY MAX_NO_OF_BU_VISIT_A_DAY DESC NULLS LAST) OVER () AS var3
		,PERCENTILE_DISC (0.01) WITHIN GROUP (ORDER BY AVG_NO_OF_BU_VISIT_A_DAY DESC NULLS LAST) OVER () AS var4
		,PERCENTILE_DISC (0.01) WITHIN GROUP (ORDER BY CNT_DAYS_CROSS_BU_SAME_DAY DESC NULLS LAST) OVER () AS var5
		,PERCENTILE_DISC (0.01) WITHIN GROUP (ORDER BY SAME_DAY_CROSS_BU_RATIO DESC NULLS LAST) OVER () AS var6
FROM VPM_Backup.GEO_CUST_SVOC_ALL_BU_VISIT_IN_MALL_6M;

/* 1st percentile */
UPDATE VPM_Backup.GEO_CUST_SVOC_ALL_BU_VISIT_IN_MALL_6M
SET SCALED_VAR1 = CASE WHEN TOTAL_DAYS_VISIT_MALLS >= VAR1 THEN 1
					   ELSE SQRT(TOTAL_DAYS_VISIT_MALLS*1.0)/SQRT(VAR1*1.0) END
	,SCALED_VAR2 = CASE WHEN CNT_BU_SPENT_IN_MALL >= VAR2 THEN 1
					   ELSE CNT_BU_SPENT_IN_MALL*1.0/VAR2*1.0 END
	,SCALED_VAR3 = CASE WHEN MAX_NO_OF_BU_VISIT_A_DAY >= VAR3 THEN 1
					   ELSE MAX_NO_OF_BU_VISIT_A_DAY*1.0/VAR3*1.0 END
	,SCALED_VAR4 = CASE WHEN AVG_NO_OF_BU_VISIT_A_DAY >= VAR4 THEN 1
					   ELSE AVG_NO_OF_BU_VISIT_A_DAY*1.0/VAR4*1.0 END
	,SCALED_VAR5 = CASE WHEN CNT_DAYS_CROSS_BU_SAME_DAY >= VAR5 THEN 1
					   ELSE CNT_DAYS_CROSS_BU_SAME_DAY*1.0/VAR5*1.0 END
	,SCALED_VAR6 = CASE WHEN SAME_DAY_CROSS_BU_RATIO >= VAR6 THEN 1
					   ELSE SAME_DAY_CROSS_BU_RATIO*1.0/VAR6*1.0 END
FROM var_percentile T
WHERE VPM_Backup.GEO_CUST_SVOC_ALL_BU_VISIT_IN_MALL_6M.member_number = T.member_number;				   
			

CREATE temp TABLE scaled_min_max_var4 AS
SELECT MEMBER_NUMBER,MAX(SCALED_VAR4) AS MAX_SCALED_VAR4,MIN(SCALED_VAR4) AS MIN_SCALED_VAR4 FROM VPM_Backup.GEO_CUST_SVOC_ALL_BU_VISIT_IN_MALL_6M
GROUP BY MEMBER_NUMBER;

UPDATE VPM_Backup.GEO_CUST_SVOC_ALL_BU_VISIT_IN_MALL_6M /* Normalized Score */
SET SCALED_VAR4 = (CASE WHEN (MAX_SCALED_VAR4-MIN_SCALED_VAR4)<= 0 THEN 0 ELSE (SCALED_VAR4-MIN_SCALED_VAR4)*1.0/(MAX_SCALED_VAR4-MIN_SCALED_VAR4)*1.0 END)
FROM scaled_min_max_var4 T
WHERE VPM_Backup.GEO_CUST_SVOC_ALL_BU_VISIT_IN_MALL_6M.member_number = T.member_number;

					   
UPDATE VPM_Backup.GEO_CUST_SVOC_ALL_BU_VISIT_IN_MALL_6M
SET CROSS_BU_SCORE = (0.2*SCALED_VAR1)+(0.2*SCALED_VAR2)+(0.2*SCALED_VAR3)
					+(0.1*SCALED_VAR4)+(0.2*SCALED_VAR5)+(0.1*SCALED_VAR6)
;



/***********************************************************/									
/*** MOST VISITED MALLS BRANCHES AND BUs BASE ON HISTORY ***/

--DROP TABLE IF EXISTS CUST_TTL_VISIT;
SELECT	MEMBER_NUMBER
		,COUNT(distinct TXN_DATE) AS TTL_CNT_DAYS_VISIT
		,COUNT(distinct CASE WHEN MALL_BUID IS NOT NULL THEN TXN_DATE ELSE NULL END) AS TTL_CNT_DAYS_VISIT_IN_MALL
		,COUNT(distinct CASE WHEN MALL_BUID IS NOT NULL THEN TXN_DATE ELSE NULL END)*1.0
		/COUNT(distinct TXN_DATE)*1.0 AS IN_OUT_MALL_RATIO	
INTO TEMP TABLE CUST_TTL_VISIT			 
FROM VPM_Backup.GEO_ALL_BU_VISIT_6M_LOCATION
GROUP BY MEMBER_NUMBER
;


--DROP TABLE IF EXISTS VPM_Backup.GEO_CUST_VISIT_MAIN_MALLS
--;
--SELECT *, ROW_NUMBER() OVER(PARTITION BY MEMBER_NUMBER 
--					   ORDER BY VISIT_MALL_BRANCH_RATIO DESC NULLS LAST, SUM_SPEND_IN_MALL DESC NULLS LAST) AS SEQ
--INTO VPM_Backup.GEO_CUST_VISIT_MAIN_MALLS
--FROM (
--	SELECT A.*, MALL_BUCODE, MALL_BUID, MALL_BRANCHID, MALL_BRANCH_NAME, MALL_LATITUDE, MALL_LONGITUDE
--			  , CNT_DAYS_VISIT_MALL_BR
--			  , CASE WHEN TTL_CNT_DAYS_VISIT_IN_MALL = 0 THEN 0
--					 ELSE (CNT_DAYS_VISIT_MALL_BR*1.0)/(TTL_CNT_DAYS_VISIT_IN_MALL*1.0) END AS VISIT_MALL_BRANCH_RATIO
--			  , SUM_SPEND_IN_MALL			   	
--	FROM CUST_TTL_VISIT A LEFT JOIN
--		(SELECT	MEMBER_NUMBER, MALL_BUCODE, MALL_BUID, MALL_BRANCHID, MALL_BRANCH_NAME, MALL_LATITUDE, MALL_LONGITUDE  
--				,COUNT(distinct TXN_DATE) AS CNT_DAYS_VISIT_MALL_BR	
--				,SUM(TOTAL_SPEND) AS SUM_SPEND_IN_MALL			 
--		FROM VPM_Backup.GEO_ALL_BU_VISIT_6M_LOCATION
--		WHERE MALL_BUCode IS NOT NULL
--		GROUP BY MEMBER_NUMBER, MALL_BUCODE, MALL_BUID, MALL_BRANCHID, MALL_BRANCH_NAME, MALL_LATITUDE, MALL_LONGITUDE
--		 ) B ON A.MEMBER_NUMBER = B.MEMBER_NUMBER) C
--ORDER BY MEMBER_NUMBER
;
 


DROP TABLE IF EXISTS VPM_Backup.GEO_CUST_VISIT_MAIN_BU
;
SELECT *, ROW_NUMBER() OVER(PARTITION BY MEMBER_NUMBER 
					   ORDER BY VISIT_BU_BRANCH_IN_MALL_RATIO DESC NULLS LAST, SUM_SPEND_BU_IN_MALL DESC NULLS LAST) AS SEQ_IN_MALL
INTO VPM_Backup.GEO_CUST_VISIT_MAIN_BU	
FROM (
	SELECT A.*,	PARTNER_CODE
			  , CNT_DAYS_VISIT_BU
			  , (CNT_DAYS_VISIT_BU*1.0)/(TTL_CNT_DAYS_VISIT*1.0) AS VISIT_BU_BRANCH_RATIO
			  , SUM_SPEND_BU
			  , RANK() OVER(PARTITION BY A.MEMBER_NUMBER 
					   ORDER BY (CNT_DAYS_VISIT_BU*1.0)/(TTL_CNT_DAYS_VISIT*1.0) DESC NULLS LAST, SUM_SPEND_BU DESC NULLS LAST) AS SEQ 
			  , CNT_DAYS_VISIT_BU_IN_MALL			  
			  , CASE WHEN TTL_CNT_DAYS_VISIT_IN_MALL = 0 THEN 0
					 ELSE (CNT_DAYS_VISIT_BU_IN_MALL*1.0)/(TTL_CNT_DAYS_VISIT_IN_MALL*1.0) END AS VISIT_BU_BRANCH_IN_MALL_RATIO	
			  , SUM_SPEND_BU_IN_MALL	  
	FROM CUST_TTL_VISIT A LEFT JOIN	
		(SELECT	MEMBER_NUMBER, PARTNER_CODE
				,COUNT(distinct TXN_DATE) AS CNT_DAYS_VISIT_BU
				,SUM(TOTAL_SPEND) AS SUM_SPEND_BU
				,COUNT(distinct CASE WHEN MALL_BUID IS NOT NULL THEN TXN_DATE ELSE NULL END) AS CNT_DAYS_VISIT_BU_IN_MALL
				,SUM(CASE WHEN MALL_BUID IS NOT NULL THEN TOTAL_SPEND ELSE 0 END) AS SUM_SPEND_BU_IN_MALL				 
		FROM VPM_Backup.GEO_ALL_BU_VISIT_6M_LOCATION
		GROUP BY MEMBER_NUMBER, PARTNER_CODE) B ON A.MEMBER_NUMBER = B.MEMBER_NUMBER) C
ORDER BY MEMBER_NUMBER
;

/*** FINALIZE RECOMMENDED BU BRANCHES BY MOST VISITED MALL (CDS/RBS Location) ***/
--DROP TABLE IF EXISTS GEO_CUST_VISIT_MAIN_BU_BRANCHES_STAGING1;
SELECT	MEMBER_NUMBER, PARTNER_CODE, BRANCH_CODE, BranchEnglishName
		,Latitude,Longitude,POSTAL_CODE,REGION1,REGION2
		,MALL_BUCODE, MALL_BUID, MALL_BRANCHID, MALL_BRANCH_NAME, MALL_LATITUDE, MALL_LONGITUDE
		,COUNT(distinct TXN_DATE) AS CNT_DAYS_VISIT_BRANCH
		,SUM(TOTAL_SPEND) AS SUM_SPEND_BRANCH
		,COUNT(distinct CASE WHEN MALL_BUID IS NOT NULL THEN TXN_DATE ELSE NULL END) AS CNT_DAYS_VISIT_BRANCH_IN_MALL
		,SUM(CASE WHEN MALL_BUID IS NOT NULL THEN TOTAL_SPEND ELSE 0 END) AS SUM_SPEND_BRANCH_IN_MALL				 
INTO TEMP TABLE GEO_CUST_VISIT_MAIN_BU_BRANCHES_STAGING1
FROM VPM_Backup.GEO_ALL_BU_VISIT_6M_LOCATION
GROUP BY MEMBER_NUMBER, PARTNER_CODE, BRANCH_CODE, BranchEnglishName
		,Latitude,Longitude,POSTAL_CODE,REGION1,REGION2
		,MALL_BUCODE, MALL_BUID, MALL_BRANCHID, MALL_BRANCH_NAME, MALL_LATITUDE, MALL_LONGITUDE
;
	
--DROP TABLE IF EXISTS GEO_CUST_VISIT_MAIN_BU_BRANCHES_STAGING2;
SELECT A.*,	PARTNER_CODE, BRANCH_CODE, BranchEnglishName
		  , Latitude,Longitude,POSTAL_CODE,REGION1,REGION2
		  , MALL_BUCODE, MALL_BUID, MALL_BRANCHID, MALL_BRANCH_NAME, MALL_LATITUDE, MALL_LONGITUDE
		  , CNT_DAYS_VISIT_BRANCH
		  , (CNT_DAYS_VISIT_BRANCH*1.0)/(TTL_CNT_DAYS_VISIT*1.0) AS VISIT_BU_BRANCH_RATIO
		  , SUM_SPEND_BRANCH
		  , ROW_NUMBER() OVER(PARTITION BY A.MEMBER_NUMBER
				   ORDER BY (CNT_DAYS_VISIT_BRANCH*1.0)/(TTL_CNT_DAYS_VISIT*1.0) DESC NULLS LAST, SUM_SPEND_BRANCH DESC NULLS LAST) AS SEQ 
		  , CNT_DAYS_VISIT_BRANCH_IN_MALL			  
		  , CASE WHEN TTL_CNT_DAYS_VISIT_IN_MALL = 0 THEN 0
				 ELSE (CNT_DAYS_VISIT_BRANCH_IN_MALL*1.0)/(TTL_CNT_DAYS_VISIT_IN_MALL*1.0) END AS VISIT_BU_BRANCH_IN_MALL_RATIO	
		  , SUM_SPEND_BRANCH_IN_MALL	  
INTO TEMP TABLE GEO_CUST_VISIT_MAIN_BU_BRANCHES_STAGING2
FROM CUST_TTL_VISIT A 
LEFT JOIN GEO_CUST_VISIT_MAIN_BU_BRANCHES_STAGING1 B ON A.MEMBER_NUMBER = B.MEMBER_NUMBER
;


DROP TABLE IF EXISTS VPM_Backup.GEO_CUST_VISIT_MAIN_BU_BRANCHES
;
SELECT *, ROW_NUMBER() OVER(PARTITION BY MEMBER_NUMBER 
					   ORDER BY VISIT_BU_BRANCH_IN_MALL_RATIO DESC NULLS LAST, SUM_SPEND_BRANCH_IN_MALL DESC NULLS LAST) AS SEQ_IN_MALL
INTO VPM_Backup.GEO_CUST_VISIT_MAIN_BU_BRANCHES	
FROM GEO_CUST_VISIT_MAIN_BU_BRANCHES_STAGING2 C
ORDER BY MEMBER_NUMBER
;


UPDATE VPM_DATA.SALESSKU_LOG_TIME SET end_time = current_timestamp WHERE task = 'LOCATION Step 3_Cust CrossBUs Score and Visited BUs' AND EXTRACT(MONTH FROM start_time) = EXTRACT(MONTH FROM current_timestamp)
;

