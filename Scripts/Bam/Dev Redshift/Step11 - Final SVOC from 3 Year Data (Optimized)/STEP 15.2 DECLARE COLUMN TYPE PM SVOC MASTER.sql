--USE VPM_BACKUP;

CREATE OR REPLACE PROCEDURE RENAME_PM_SVOC_MASTER_TABLE()
LANGUAGE PLPGSQL
AS $$
DECLARE PREVIOUS_MONTH_CHAR TEXT;
BEGIN
PREVIOUS2_MONTH_CHAR := (SELECT TO_CHAR(CURRENT_TIMESTAMP-INTERVAL '2 MONTH','MON'));
EXECUTE '
alter table vpm_backup.pm_svoc_master_previous rename
TO PM_SVOC_MASTER_'||PREVIOUS2_MONTH_CHAR
;
EXECUTE '
alter table vpm_backup.pm_svoc_master rename
TO PM_SVOC_MASTER_previous'
;
END $$;
CALL RENAME_PM_SVOC_MASTER_TABLE();

DROP TABLE IF EXISTS VPM_BACKUP.PM_SVOC_MASTER
;

------DECLARED_NUM_CHILDREN BUG
CREATE TABLE VPM_BACKUP.PM_SVOC_MASTER
(		--MEMBER_NUMBER VARCHAR(50) PRIMARY KEY,
		MEMBER_NUMBER VARCHAR(50) PRIMARY KEY,
		T1_CARD_NUMBER VARCHAR(50),
		DOB DATE,
		CURRENT_AGE INT,
		ENTRY_AGE INT,
		NATIONALITY VARCHAR(500),
		DECLARED_OCCUPATION VARCHAR(500),
		INFERRED_SALARYMAN_FLAG VARCHAR(3),
		SALARYMAN_SCORE DECIMAL(12,6),
		EDUCATIONLEVEL VARCHAR(500),
		INFERRED_EDUCATION VARCHAR(15),
		MONTHLYINCOME VARCHAR(500),
		HOUSEHOLDINCOME VARCHAR(500),
		REGISTERDATE DATE,
		TENURE INT,
		DECLARED_GENDER VARCHAR(20),
		INFERRED_GENDER VARCHAR(20),
		DECLARED_MARITAL_STATUS VARCHAR(7),
		PROB_MARRIED NUMERIC,
		LIFE_STAGE VARCHAR(30),
		LARGE_FAMILY_FLAG VARCHAR(3),
		DECLARED_NUM_CHILDREN VARCHAR(3),
		DECLARED_NUM_KIDS INT,
		KID_TOTAL_SPEND DECIMAL(21,6),
		KID_SPENDING_RATIO DECIMAL(21,6),
		BOOK_STATIONERY_SPENDING_RATIO DECIMAL(21,6),
		KID_DAY_SPEND INT,
		KID_NUM_TYPE INT,
		INFERRED_NUM_KIDS INT,
		CURRENT_CHILD1_AGE INT,
		CURRENT_CHILD2_AGE INT,
		CURRENT_CHILD3_AGE INT,
		CURRENT_CHILD4_AGE INT,
		KIDS_FLAG VARCHAR(3),
		KIDS_SCORE DECIMAL(12,6),
		KID_DESC VARCHAR(50),
		KID_HIGH_ACTIVITY_FLAG VARCHAR(3),
		KID_MID_ACTIVITY_FLAG VARCHAR(3),
		KID_ROLLING_FLAG VARCHAR(3),
		KID_SBL_FLAG VARCHAR(3),
		--KID_ONIGIRI_FLAG VARCHAR(3),
		KID_CDS_KIDS_CLUB_FLAG VARCHAR(3),
		KID_RBS_MOM_BABY_CLUB_FLAG VARCHAR(3),
		KID_API_FLAG VARCHAR(3),
		KID_CPN_FLAG VARCHAR(3),
		KID_MOMANDKIDSPROJECT_FLAG VARCHAR(3),
		KID_UPDATEPROFILE_FLAG VARCHAR(3),
		--KID_THE1_APP_FLAG VARCHAR(3),
		--KID_HOSPITAL_FLAG VARCHAR(3),
		KIDS_SPENDING_SEGMENT VARCHAR(20),
		KID_STAGE_PRIORITY VARCHAR(20),
		KID_RELATIONSHIP VARCHAR(20),
		KID_LAST_PURCHASE_DATE DATE,
		KID_ACTIVITY VARCHAR(50),
		KID_INFANT VARCHAR(3),
		KID_INFANT_SPENDING_SCORE DECIMAL(12,6),
		INFANT_SPENDING_SEGMENT VARCHAR(50),
		INFANT_GENDER VARCHAR(6),
		ESTIMATED_DATE_OF_GIVE_BIRTH DATE,
		ESTIMATED_INFANT_MONTH INT,
		KID_TODDLER VARCHAR(3),
		KID_TODDLER_SPENDING_SCORE DECIMAL(12,6),
		TODDLER_SPENDING_SEGMENT VARCHAR(50),
		TODDLER_GENDER VARCHAR(6),
		KID_PRESCHOOL VARCHAR(3),
		KID_PRESCHOOL_SPENDING_SCORE DECIMAL(12,6),
		PRESCHOOL_SPENDING_SEGMENT VARCHAR(50),
		PRESCHOOL_GENDER VARCHAR(6),
		KID_JUNIOR VARCHAR(3),
		KID_JUNIOR_SPENDING_SCORE DECIMAL(12,6),
		JUNIOR_SPENDING_SEGMENT VARCHAR(50),
		JUNIOR_GENDER VARCHAR(6),
		KID_TEENAGE VARCHAR(3),
		KID_TEEN_SPENDING_SCORE DECIMAL(12,6),
		TEEN_SPENDING_SEGMENT VARCHAR(50),
		TEEN_GENDER VARCHAR(6),
		KID_BURDEN_SCORE DECIMAL(12,6),
		KID_BURDEN_LEVEL VARCHAR(50),
		OPPOSITE_GENDER_SPENDING_SCORE DECIMAL(12,6),
		SPENDING_AMT_FOR_HIM DECIMAL(21,6),
		SPENDING_AMT_FOR_HER DECIMAL(21,6),
		SHARED_CARD_FLAG VARCHAR(3),
		CARDREGISTERBU VARCHAR(10),
		CARDREGISTERLOCATION VARCHAR(100),
		DECLARED_PROVINCE VARCHAR(50),
		DECLARED_AMPHOR VARCHAR(200),
		DECLARED_TAMBON VARCHAR(200),
		DECLARED_ZIPCODE VARCHAR(6),
		REGION_ECONOMY_GRP VARCHAR(50),
		MOST_VISIT_REGION1 VARCHAR(100),
		DAYS_VISIT_REGION1 INT,
		MOST_VISIT_SUB_REGION1 VARCHAR(100),
		DAYS_VISIT_SUB_REGION1 INT,
		MOST_VISIT_PROVINCE1 VARCHAR(255),
		DAYS_VISIT_PROVINCE1 INT,
		MOST_VISIT_PROVINCE2 VARCHAR(255),
		DAYS_VISIT_PROVINCE2 INT,
		MOST_VISIT_POSTAL_CODE1 VARCHAR(50),
		DAYS_VISIT_POSTAL_CODE1 INT,
		MOST_VISIT_POSTAL_CODE2 VARCHAR(50),
		DAYS_VISIT_POSTAL_CODE2 INT,
		CUST_LOCATION_SEGMENT VARCHAR(50),
		CNT_DATE_SPEND INT,
		CNT_WORKING_DAY_SPEND INT,
		CNT_WORKING_DAY_ONPEAK_SPEND INT,
		CNT_WORKING_DAY_OFFPEAK_SPEND INT,
		OFF_PEAK_VISIT_RATIO DECIMAL(21,6),
		CNT_HOLIDAY_SPEND INT,
		OFFICE_BR_CNT_WORKING_DAY INT,
		PREFERRED_TIME_SEGMENT VARCHAR(50),
		NO_TXN_12M INT,
		NO_DAYS_SPEND_12M INT,
		NO_ACTIVE_MONTH_12M INT,
		SUM_TXN_AMT_12M DECIMAL(21,6),
		NO_BU_PURCHASE_12M INT,
		TOTAL_DAYS_VISIT_CDS_6M INT,
		TOTAL_DAYS_SPENT_CDS_6M INT,
		TOTAL_DAYS_VISIT_RBS_6M INT,
		TOTAL_DAYS_SPENT_RBS_6M INT,
		CNT_BU_SPENT_IN_MALL_6M INT,
		CNT_DAYS_CROSS_BU_SAME_DAY_6M INT,
		MOST_VISITED_MALL VARCHAR(50),
		MALL_BRANCHID INT,
		MALL_BRANCH_NAME VARCHAR(100),
		MALL_LATITUDE FLOAT,
		MALL_LONGITUDE FLOAT,
		MOST_CONVENIENT_MALL VARCHAR(50),
		MOST_CONVENIENT_MALL_BRANCHID INT,
		MOST_CONVENIENT_MALL_BRANCH_NAME VARCHAR(100),
		MOST_CONVENIENT_MALL_LATITUDE FLOAT,
		MOST_CONVENIENT_MALL_LONGITUDE FLOAT,
		AFFINITY_SCORE_CDS_6M DECIMAL(12,6),
		CDS_ACTIVE_STATUS VARCHAR(14),
		AFFINITY_SCORE_MUJI_6M DECIMAL(12,6),
		MUJI_ACTIVE_STATUS VARCHAR(14),
		AFFINITY_SCORE_RBS_6M DECIMAL(12,6),
		RBS_ACTIVE_STATUS VARCHAR(14),
		AFFINITY_SCORE_B2S_6M DECIMAL(12,6),
		B2S_ACTIVE_STATUS VARCHAR(14),
		AFFINITY_SCORE_SSP_6M DECIMAL(12,6),
		SSP_ACTIVE_STATUS VARCHAR(14),
		AFFINITY_SCORE_PWB_6M DECIMAL(12,6),
		PWB_ACTIVE_STATUS VARCHAR(14),
		AFFINITY_SCORE_ODP_6M DECIMAL(12,6),
		ODP_ACTIVE_STATUS VARCHAR(14),
		AFFINITY_SCORE_CMG_6M DECIMAL(12,6),
		CMG_ACTIVE_STATUS VARCHAR(14),
		AFFINITY_SCORE_CFM_6M DECIMAL(12,6),
		CFM_ACTIVE_STATUS VARCHAR(14),
		AFFINITY_SCORE_TOPS_6M DECIMAL(12,6),
		TOPS_ACTIVE_STATUS VARCHAR(14),
		AFFINITY_SCORE_TWD_6M DECIMAL(12,6),
		TWD_ACTIVE_STATUS VARCHAR(14),
		AFFINITY_SCORE_HWS_6M DECIMAL(12,6),
		HWS_ACTIVE_STATUS VARCHAR(14),
		AFFINITY_SCORE_ALL_BU_6M DECIMAL(12,6),
		AFFINITY_SEGMENT_ALL_BU VARCHAR(30),
		MAIN_BU VARCHAR(255),
		VISITED_BU_BRANCH1 VARCHAR(100),
		VISITED_BU_BRANCH1_RATIO DECIMAL(21,6),
		VISITED_BU_BRANCH2 VARCHAR(100),
		VISITED_BU_BRANCH2_RATIO DECIMAL(21,6),
		VISITED_BU_BRANCH3 VARCHAR(100),
		VISITED_BU_BRANCH3_RATIO DECIMAL(21,6),
		BRANCH_LATITUDE FLOAT,
		BRANCH_LONGITUDE FLOAT,
		MALLS_VISIT_SEGMENT VARCHAR(50),
		MAX_NO_OF_BU_VISIT_A_DAY INT,
		AVG_NO_OF_BU_VISIT_A_DAY NUMERIC,
		SAME_DAY_CROSS_BU_RATIO DECIMAL(21,6),
		CROSS_BU_SCORE DECIMAL(12,6),
		CC_CNT_CREDIT_CARD FLOAT,
		CC_CNT_BANK_ISSUED FLOAT,
		PRIMARY_BANK_CARD VARCHAR(100),
		PRIMARY_BANK_CARD_DESC VARCHAR(100),
		PRIVILEGE_BANK_CARD VARCHAR(50),
		PRIVILEGE_BANK_CARD_DESC VARCHAR(150),
		CC_T1C_BLACK_CARD_FLAG VARCHAR(3),
		CC_MINIMUM_INCOME FLOAT,
		CC_MINIMUM_DEPOSIT FLOAT,
		CC_MAX_MONTHLY_SPEND_PER_CARD DECIMAL(21,6),
		CC_WEIGHTED_MAX_MONTHLY_SPEND DECIMAL(21,6),
		TOTAL_CASH_SPEND DECIMAL(21,6),
		TOTAL_CREDIT_CARD_SPEND DECIMAL(21,6),
		T1_CREDIT_CARD_SPEND DECIMAL(21,6),
		CASH_TO_CREDIT_SPENDING_RATIO DECIMAL(21,6),
		PAYMENT_METHOD VARCHAR(50),
		BEAUTY_TOTAL_SPEND DECIMAL(21,6),
		BEAUTY_SPENDING_RATIO DECIMAL(21,6),
		BEAUTY_DAYS_SPEND INT,
		BEAUTY_LUXURY_SCORE DECIMAL(12,6),
		BEAUTY_LUXURY_SEGMENT VARCHAR(17),
		MAKEUP_LUXURY_SCORE DECIMAL(12,6),
		MAKEUP_LUXURY_SEGMENT VARCHAR(17),
		SKINCARE_LUXURY_SCORE DECIMAL(12,6),
		SKINCARE_LUXURY_SEGMENT VARCHAR(17),
		FRAGRANCE_LUXURY_SCORE DECIMAL(12,6),
		FRAGRANCE_LUXURY_SEGMENT VARCHAR(17),
		HBA_LUXURY_SCORE DECIMAL(12,6),
		HBA_LUXURY_SEGMENT VARCHAR(17),
		SKINCARE_PSYCHO_SEGMENT VARCHAR(30),
		MAKEUP_PSYCHO_SEGMENT VARCHAR(30),
		SKINCARE_PREFER_BRAND VARCHAR(100),
		MAKEUP_PREFER_BRAND VARCHAR(100),
		BEAUTY_PERSONA_SEGMENT VARCHAR(30),
		BEAUTY_CNT_PRODUCT_TYPE INT,
		BEAUTY_SUM_QTY BIGINT,
		MAKEUP_SCORE DECIMAL(12,6),
		SKINCARE_SCORE DECIMAL(12,6),
		FRAGRANCE_SCORE DECIMAL(12,6),
		BEAUTY_CONCERN_SEGMENT VARCHAR(50),
		BEAUTY_SKINCARE_PURPOSE VARCHAR(100),
		BEAUTY_MAKEUP_BODY_PART VARCHAR(100),
		CONCERN_ANTI_AGING_FLAG VARCHAR(3),
		CONCERN_WHITENING_FLAG VARCHAR(3),
		CONCERN_MOISTURIZER_FLAG VARCHAR(3),
		CONCERN_ACNE_FLAG VARCHAR(3),
		CONCERN_SPOT_REDUCING_FLAG VARCHAR(3),
		CONCERN_SUN_PROTECTION_FLAG VARCHAR(3),
		FACE_FREQ INT,
		CHEEK_FREQ INT,
		EYE_FREQ INT,
		EYEBROW_FREQ INT,
		LIP_FREQ INT,
		NAIL_CARE_FREQ INT,
		MAX_CLEANSER_PRICE DECIMAL(21,6),
		MAX_TONER_PRICE DECIMAL(21,6),
		MAX_CREAM_PRICE DECIMAL(21,6),
		MAX_SERUM_PRICE DECIMAL(21,6),
		MAX_SUNSCREEN_PRICE DECIMAL(21,6),
		MAX_SKIN_CARE_PRICE DECIMAL(21,6),
		MAX_POWDER_PRICE DECIMAL(21,6),
		MAX_FOUNDATION_PRICE DECIMAL(21,6),
		MAX_LIPSTICK_PRICE DECIMAL(21,6),
		MAX_BLUSH_ON_PRICE DECIMAL(21,6),
		MAX_MASCARA_PRICE DECIMAL(21,6),
		MAX_EYE_LINER_PRICE DECIMAL(21,6),
		MAX_EYE_SHADOW_PRICE DECIMAL(21,6),
		MAX_BASE_PRICE DECIMAL(21,6),
		MAX_CONCEALER_PRICE DECIMAL(21,6),
		MAX_NAIL_CARE_PRICE DECIMAL(21,6),
		MAX_MAKE_UP_PRICE DECIMAL(21,6),
		MAX_MAKE_UP_TOOL_PRICE DECIMAL(21,6),
		MAX_MAKEUP_SERVICE_PRICE DECIMAL(21,6),
		MAX_PERFUME_PRICE DECIMAL(21,6),
		MAX_FRAGRANCE_PRICE DECIMAL(21,6),
		MAX_TOILETRIES_PRICE DECIMAL(21,6),
		MAX_DEODORANT_PRICE DECIMAL(21,6),
		MAX_BATHING_PRICE DECIMAL(21,6),
		FASHION_TOTAL_SPEND DECIMAL(21,6),
		FASHION_SPENDING_RATIO DECIMAL(21,6),
		FASHION_DAYS_SPEND INT,
		FASHION_LUXURY_SCORE DECIMAL(12,6),
		FASHION_LUXURY_SEGMENT VARCHAR(17),
		APPAREL_LUXURY_SCORE DECIMAL(12,6),
		APPAREL_LUXURY_SEGMENT VARCHAR(17),
		APPAREL_PREFER_BRAND VARCHAR(100),
		LEATHER_GOODS_LUXURY_SCORE DECIMAL(12,6),
		LEATHER_GOODS_LUXURY_SEGMENT VARCHAR(17),
		LEATHER_GOODS_PREFER_BRAND VARCHAR(100),
		ACCESSORIES_LUXURY_SCORE DECIMAL(12,6),
		ACCESSORIES_LUXURY_SEGMENT VARCHAR(17),
		ACCESSORIES_PREFER_BRAND VARCHAR(100),
		FASHION_PROMO_SENSITIVITY_SCORE DECIMAL(12,6),
		FASHION_PROMO_SENSITIVITY_SEGMENT VARCHAR(4),
		FASHION_AVG_DISCOUNT_PERCENT DECIMAL(21,6),
		FASHION_SUM_QTY_FOR_HER BIGINT,
		FASHION_SUM_QTY_FOR_HIM BIGINT,
		FASHION_SUM_SPEND_FOR_HER DECIMAL(21,6),
		FASHION_SUM_SPEND_FOR_HIM DECIMAL(21,6),
		FASHION_PROMO_SENSITIVITY_SCORE_FEMALE DECIMAL(12,6),
		FASHION_PROMO_SENSITIVITY_SCORE_MALE DECIMAL(12,6),
		FASHION_NEW_ARRIVAL_SCORE DECIMAL(12,6),
		FASHION_NEW_ARRIVAL_SEGMENT VARCHAR(4),
		FASHION_SIZE VARCHAR(30),
		MAX_SHIRT_PRICE DECIMAL(21,6),
		MAX_TSHIRT_POLO_PRICE DECIMAL(21,6),
		MAX_SPORTWEAR_PRICE DECIMAL(21,6),
		MAX_UNDERWEAR_PRICE DECIMAL(21,6),
		MAX_PANTS_PRICE DECIMAL(21,6),
		MAX_DRESS_PRICE DECIMAL(21,6),
		MAX_SUIT_PRICE DECIMAL(21,6),
		MAX_FOOTWEAR_PRICE DECIMAL(21,6),
		MAX_SNEAKER_PRICE DECIMAL(21,6),
		MAX_OUTERWEAR_PRICE DECIMAL(21,6),
		MAX_BAG_PRICE DECIMAL(21,6),
		MAX_HANDBAG_PRICE DECIMAL(21,6),
		MAX_WATCH_PRICE DECIMAL(21,6),
		MAX_JEWELRY_PRICE DECIMAL(21,6),
		FASHION_EVENING_GOWN_FLAG VARCHAR(3),
		FASHION_LUXURY_THAI_DESIGNER_FLAG VARCHAR(3),
		FASHION_MADAME_STYLE_FLAG VARCHAR(3),
		FASHION_OFFICE_PROFESSIONAL_WEAR_FLAG VARCHAR(3),
		FASHION_FAST_FASHION_FLAG VARCHAR(3),
		FASHION_MATURE_FEMININE_FLAG VARCHAR(3),
		FASHION_STREET_STYLE_FLAG VARCHAR(3),
		FASHION_EVERYDAY_BASIC_FLAG VARCHAR(3),
		FASHION_YOUNG_FEMININE_FLAG VARCHAR(3),
		FASHION_SPORTS_FLAG VARCHAR(3),
		FASHION_ADVENTURE_FLAG VARCHAR(3),
		FASHION_TRAVEL_FLAG VARCHAR(3),
		FOOD_TOTAL_SPEND DECIMAL(21,6),
		FOOD_SPENDING_RATIO DECIMAL(21,6),
		FOOD_DAYS_SPEND INT,
		FOOD_GROCERY_LUXURY_SEGMENT VARCHAR(17),
		FOOD_PROMO_SENSITIVITY_SCORE DECIMAL(12,6),
		FOOD_PROMO_SENSITIVITY_SEGMENT VARCHAR(4),
		FOOD_AVG_PERCENT_DISCOUNT FLOAT,
		FOOD_COUNT_DISCOUNT_ITEM FLOAT,
		FOOD_HEALTH_SCORE DECIMAL(12,6),
		FOOD_HEALTH_FLAG VARCHAR(3),
		MEAT_LOVER_FLAG VARCHAR(3),
		FRUIT_LOVER_FLAG VARCHAR(3),
		SEAFOOD_LOVER_FLAG VARCHAR(3),
		READY_TO_EAT_FLAG VARCHAR(3),
		GIFT_BASKET_FLAG VARCHAR(3),
		BEEF_FLAG VARCHAR(3),
		PORK_FLAG VARCHAR(3),
		INGREDIENT_FREQ INT,
		READYTOEAT_FREQ INT,
		PERISHABLE_FREQ INT,
		INTERFOOD_FREQ INT,
		FOOD_TOPS_DAYS_SPEND INT,
		FOOD_TOPS_RECENCY INT,
		FOOD_TOPS_AMT_PER_VISIT DECIMAL(21,6),
		ELECTRONICS_TOTAL_SPEND DECIMAL(21,6),
		ELECTRONIC_SPENDING_RATIO DECIMAL(21,6),
		ELECTRONICS_DAYS_SPEND INT,
		ELECTRONICS_LUXURY_SCORE DECIMAL(12,6),
		ELECTRONICS_LUXURY_SEGMENT VARCHAR(17),
		ELECTRONICS_PROMO_SENSITIVITY_SCORE DECIMAL(12,6),
		ELECTRONICS_PROMO_SENSITIVITY_SEGMENT VARCHAR(4),
		ELECTRONICS_AVG_DISCOUNT_PERCENT FLOAT,
		CAMERA_SEGMENT VARCHAR(21),
		MOBILE_FREQ FLOAT,
		MOBILE_IPHONE_FREQ FLOAT,
		APPLE_FLAG VARCHAR(3),
		APPLE_LOVER_NEW_ARRIVAL VARCHAR(3),
		APPLE_LOVER_VARIETY_PROD VARCHAR(3),
		APPLE_DISCOUNT INT,
		MOBILE_IPHONE_RECENCY FLOAT,
		MOBILE_IPHONE_NEW_RECENCY FLOAT,
		MOBILE_IPHONE_DISCOUNT_RECENCY FLOAT,
		TECH_SAVVY VARCHAR(3),
		REFRIDGERATOR_SIZE DECIMAL(21,6),
		RICECOOKER_SIZE DECIMAL(21,6),
		OVEN_SIZE DECIMAL(21,6),
		WASHER_SIZE DECIMAL(21,6),
		AUDIO_INTEREST_FLAG VARCHAR(3),
		TRAVEL_GEAR_INTEREST_FLAG VARCHAR(3),
		GAMING_GEAR_INTEREST_FLAG VARCHAR(3),
		SMART_HOME_INTEREST_FLAG VARCHAR(3),
		SMART_WATCH_INTEREST_FLAG VARCHAR(3),
		IT_GADGET_INTEREST_FLAG VARCHAR(3),
		HOME_TOTAL_SPEND DECIMAL(21,6),
		HOME_NECESSITY_SPENDING_RATIO DECIMAL(21,6),
		HOME_CONSTRUCTION_SPENDING_RATIO DECIMAL(21,6),
		HOME_FURNISHING_SPENDING_RATIO DECIMAL(21,6),
		HOME_DAYS_SPEND INT,
		HOME_CONSTRUCTION_SCORE DECIMAL(12,6),
		HOME_FINISHING_SCORE DECIMAL(12,6),
		HOME_FURNITURE_SCORE DECIMAL(12,6),
		MAJOR_APPLIANCE_SCORE DECIMAL(12,6),
		HOME_NECESSITY_SCORE DECIMAL(12,6),
		HOME_LIFESTYLE_IMPROVEMENT_SCORE DECIMAL(12,6),
		HOME_LIFESTYLE_GARDENING_SCORE DECIMAL(12,6),
		HOME_LIFESTYLE_DECOR_SCORE DECIMAL(12,6),
		HOME_LIFESTYLE_IMPROVEMENT_FLAG VARCHAR(3),
		HOME_LIFESTYLE_GARDENING_FLAG VARCHAR(3),
		HOME_LIFESTYLE_DECOR_FLAG VARCHAR(3),
		HOME_SPENDING_PURPOSE VARCHAR(50),
		HOME_EVENT VARCHAR(100),
		HOME_PRODUCT_PREFERENCE VARCHAR(50),
		COOKING_FOOD_SCORE DECIMAL(12,6),
		COOKING_UTENSIL_SCORE DECIMAL(12,6),
		COOKING_ELECTRONIC_SCORE DECIMAL(12,6),
		COOKING_KITCHENWARE_SCORE DECIMAL(12,6),
		COOKING_SCORE DECIMAL(12,6),
		COOKING_FLAG VARCHAR(3),
		KITCHENWARE_LOVER_FLAG VARCHAR(3),
		COOKING_BAKING_SCORE DECIMAL(12,6),
		COOKING_BAKING_FLAG VARCHAR(3),
		AUTOMOTIVE_SPENDING_RATIO DECIMAL(21,6),
		CAR_OWNERSHIP_FLAG VARCHAR(3),
		MOTORCYCLE_OWNERSHIP_FLAG VARCHAR(3),
		EUROPE_CAR_OWNERSHIP_FLAG VARCHAR(3),
		CAR_BRAND VARCHAR(50),
		PUBLIC_TRANSPORTATION_MODE VARCHAR(11),
		CAR_ENTHUSIAST_FLAG VARCHAR(3),
		CROSS_PROVINCE_DRIVER_FLAG VARCHAR(3),
		CROSS_PROVINCE_SHOPPER_FLAG VARCHAR(3),
		CROSS_PROVINCE_TRAVELLER_FLAG VARCHAR(3),
		FASHION_RELEVANCE_SCORE DECIMAL(12,6),
		FASHION_RELEVANCE_SEGMENT VARCHAR(11),
		BEAUTY_RELEVANCE_SCORE DECIMAL(12,6),
		BEAUTY_RELEVANCE_SEGMENT VARCHAR(11),
		GROCERY_RELEVANCE_SCORE DECIMAL(12,6),
		GROCERY_RELEVANCE_SEGMENT VARCHAR(11),
		FOOD_GROCERY_RELEVANCE_SCORE DECIMAL(12,6),
		FOOD_GROCERY_RELEVANCE_SEGMENT VARCHAR(11),
		HOME_GROCERY_RELEVANCE_SCORE DECIMAL(12,6),
		HOME_GROCERY_RELEVANCE_SEGMENT VARCHAR(11),
		HBA_RELEVANCE_SCORE DECIMAL(12,6),
		HBA_RELEVANCE_SEGMENT VARCHAR(11),
		HOME_RELEVANCE_SCORE DECIMAL(12,6),
		HOME_RELEVANCE_SEGMENT VARCHAR(11),
		ELECTRONICS_RELEVANCE_SCORE DECIMAL(12,6),
		ELECTRONICS_RELEVANCE_SEGMENT VARCHAR(11),
		HOME_NON_ELEC_RELEVANCE_SCORE DECIMAL(12,6),
		HOME_NON_ELEC_RELEVANCE_SEGMENT VARCHAR(11),
		HOME_ELEC_RELEVANCE_SCORE DECIMAL(12,6),
		HOME_ELEC_RELEVANCE_SEGMENT VARCHAR(11),
		ELEC_GADGET_RELEVANCE_SCORE DECIMAL(12,6),
		ELEC_GADGET_RELEVANCE_SEGMENT VARCHAR(11),
		CONSTRUCTION_RELEVANCE_SCORE DECIMAL(12,6),
		CONSTRUCTION_RELEVANCE_SEGMENT VARCHAR(11),
		CATEGORY_RELEVANCE_PRIORITY VARCHAR(50),
		PET_FLAG VARCHAR(3),
		WINE_DRINKER_FLAG VARCHAR(3),
		BEER_DRINKER_FLAG VARCHAR(3),
		WHISKY_DRINKER_FLAG VARCHAR(3),
		SPORT_FLAG VARCHAR(3),
		ACTIVE_SPORT_FLAG VARCHAR(3),
		SPORT_PREFERRED_TYPE VARCHAR(150),
		TRAVEL_INTEREST_FLAG VARCHAR(3),
		DINING_INTEREST_FLAG VARCHAR(3),
		DINING_PREFERRED_CUISINE VARCHAR(200),
		DINING_PREFERRED_CUISINE_GROUP VARCHAR(150),
		DINING_SPENDING DECIMAL(21,6),
		DINING_LUXURY_SEGMENT VARCHAR(17),
		BOOK_INTEREST_FLAG VARCHAR(3),
		BOOK_PREFER_TYPE VARCHAR(150),
		HEALTHY_LIFESTYLE_FLAG VARCHAR(3),
		HEALTHY_LIFESTYLE_PREFERRED VARCHAR(27),
		BEAUTYSPA_INTEREST_FLAG VARCHAR(3),
		BEAUTY_PREFER_SERVICES VARCHAR(100),
		COFFEE_SEGMENT VARCHAR(17),
		TRAVEL_DOMESTICS_FLAG VARCHAR(3),
		TRAVEL_ABOARD_FLAG VARCHAR(3),
		TRAVEL_ASIA_FLAG VARCHAR(3),
		TRAVEL_SOUTHEAST_ASIA_FLAG VARCHAR(3),
		TRAVEL_USA_FLAG VARCHAR(3),
		TRAVEL_EUROPE_FLAG VARCHAR(3),
		TRAVEL_UK_FLAG VARCHAR(3),
		TRAVEL_JAPAN_FLAG VARCHAR(3),
		TRAVEL_CHINA_FLAG VARCHAR(3),
		TRAVEL_HONGKONG_FLAG VARCHAR(3),
		TRAVEL_MACAO_FLAG VARCHAR(3),
		TRAVEL_SOUTH_KOREA_FLAG VARCHAR(3),
		TRAVEL_SINGAPORE_FLAG VARCHAR(3),
		TRAVEL_LUXURY_SEGMENT VARCHAR(4),
		TRAVEL_FULL_SERVICE_AIRLINE_SPENDING DECIMAL(21,6),
		HOTEL_LUXURY_FLAG VARCHAR(3),
		HOSPITAL_FLAG VARCHAR(3),
		LUXURY_HOSPITAL_FLAG VARCHAR(3),
		MOVIE_INTEREST_FLAG VARCHAR(3),
		MUSIC_AND_CONCERT_INTEREST_FLAG VARCHAR(3),
		INVESTMENT_AMOUNT FLOAT,
		INSURANCE_PREMIUM FLOAT,
		BEAUTY_COMMERCIAL_FLAG VARCHAR(3),
		FASHION_COMMERCIAL_FLAG VARCHAR(3),
		FOOD_COMMERCIAL_FLAG VARCHAR(3),
--		FOOD_COMMERCIAL_SEGMENT VARCHAR(80),
		RESTAURANT_FLAG VARCHAR(3),
--		HOME_COMMERCIAL_FLAG VARCHAR(3),
--		HOME_COMMERCIAL_SEGMENT VARCHAR(100),
--		ELECTRONICS_COMMERCIAL_FLAG VARCHAR(3),
--		ELECTRONICS_COMMERCIAL_SEGMENT VARCHAR(50),
--		CG_COMMERCIAL_SEGMENT VARCHAR(50),
--		CG_COMMERCIAL_SUBSEGMENT VARCHAR(50),
--		CG_COMMERCIAL_SPENDING_RATIO DECIMAL(21,6),
		HOME_NON_ELEC_COMMERCIAL_FLAG VARCHAR(3),
		HOME_ELEC_COMMERCIAL_FLAG VARCHAR(3),
		ELEC_GADGET_COMMERCIAL_FLAG VARCHAR(3),
		HBA_COMMERCIAL_FLAG VARCHAR(3),
		HOME_GROCERY_COMMERCIAL_FLAG VARCHAR(3),
		OFFICE_SUPPLY_COMMERCIAL_FLAG VARCHAR(3),
		CONSTRUCTION_COMMERCIAL_FLAG VARCHAR(3),
		CG_COMMERCIAL_FLAG VARCHAR(3),
		POINT_BALANCE INT,
		NORMAL_POINTS_EARNED BIGINT,
		EXTRA_POINTS_EARNED BIGINT,
		EXTRA_POINTS_RATIO DECIMAL(21,6),
		POINT_TRANSFERRED INT,
		MAX_POINT_BALANCE BIGINT,
		CNT_POINT_REDEMPTION INT,
		MAX_POINT_REDEEMED INT,
		SUM_POINT_REDEEMED INT,
		POINT_TURNOVER BIGINT,
		POINT_USED_TO_EARNED_RATIO DECIMAL(21,6),
		EXTRA_POINT_SCORE DECIMAL(12,6),
		REDEEM_POINT_SCORE DECIMAL(12,6),
		POINT_AFFINITY_SCORE DECIMAL(12,6),
		POINT_AFFINITY_SEGMENT VARCHAR(4),
		HIGH_POINT_AFFINITY_SEGMENT VARCHAR(50),
		REDEMPTION_PATTERN_SEGMENT VARCHAR(30),
		MOST_REDEMPTION_BU VARCHAR(10),
		ONLINE_REDEMPTION_FLAG INT,
		OFFLINE_REDEMPTION_FLAG INT,
		MOST_REDEMPTION_TYPE1 VARCHAR(30),
		MOST_REDEMPTION_TYPE2 VARCHAR(30),
		AVG_PERCENT_DISCOUNT_REDEMPTION DECIMAL(21,6),
		PREFERRED_PREMIUM_REDEMPTION VARCHAR(30),
		REDEMPTION_PREFERRED_PARTNER VARCHAR(10),
		REDEMPTION_PETROL_TXN_CNT INT,
		REDEMPTION_HOTEL_SPA_CNT INT,
		REDEMPTION_TRAVEL_CNT INT,
		REDEMPTION_DINING_CNT INT,
		CHG_CUSTOMER_SEGMENT VARCHAR(100),
		ONLINE_USER_FLAG VARCHAR(3),
		ONLINE_TOTAL_SPEND DECIMAL(21,6),
		ONLINE_TOTAL_DAY_VISIT INT,
		ONLINE_CG_SPEND DECIMAL(21,6),
		ONLINE_CG_DAY_VISIT INT,
		ONLINE_NON_CG_SPEND DECIMAL(21,6),
		ONLINE_NON_CG_DAY_VISIT INT,
		ONLINE_SHOPPING_BEAUTY_FLAG VARCHAR(3),
		ONLINE_SHOPPING_BOOKS_FLAG VARCHAR(3),
		ONLINE_SHOPPING_ELECTRONIC_FLAG VARCHAR(3),
		ONLINE_SHOPPING_ECOMMERCE_FLAG VARCHAR(3),
		ONLINE_SHOPPING_FASHION_FLAG VARCHAR(3),
		ONLINE_SHOPPING_GROCERY_FLAG VARCHAR(3),
		ONLINE_SHOPPING_HOME_FLAG VARCHAR(3),
		ONLINE_SHOPPING_KID_FLAG VARCHAR(3),
		ONLINE_SERVICE_SPA_FLAG VARCHAR(3),
		ONLINE_SERVICE_BUSINESS_FLAG VARCHAR(3),
		ONLINE_SERVICE_FINANCIAL_FLAG VARCHAR(3),
		ONLINE_SERVICE_KID_FLAG VARCHAR(3),
		ONLINE_SERVICE_ACTIVITY_FLAG VARCHAR(3),
		ONLINE_SERVICE_LOGISTIC_FLAG VARCHAR(3),
		ONLINE_SERVICE_HOSPITAL_FLAG VARCHAR(3),
		ONLINE_SERVICE_COURSE_FLAG VARCHAR(3),
		ONLINE_SERVICE_TRAVEL_FLAG VARCHAR(3),
		ONLINE_SERVICE_UTILITIES_FLAG VARCHAR(3),
		ONLINE_AFFINITY_SCORE DECIMAL(12,6),
		ONLINE_AFFINITY_SEGMENT VARCHAR(23),
		WEALTH_SCORE_V5 DECIMAL(12,6),
		WEALTH_SCORE_V5_SEGMENT VARCHAR(12),
		WEALTH_V6_SEGMENT VARCHAR(12),
		WEALTH_POTENTIAL_FLAG VARCHAR(3),
		AFFLUENT_POTENTIAL_FLAG VARCHAR(3),
		CLUSTER_NAME VARCHAR(50)
)
;




