
-- create home grocery txn 
DROP TABLE IF EXISTS vpm_data.y_home_grocery_commercial_txn 
;
SELECT *
INTO vpm_data.y_home_grocery_commercial_txn
FROM vpm_data.pm_all_salessku_1yr
WHERE revised_category_level_1 = 'HOME GROCERY'
;

DROP TABLE IF EXISTS y_home_grocery_commercial_cust_lvl1 
;
SELECT member_number
	,revised_category_level_1
	,SUM(qty) AS total_qty
	,COUNT(DISTINCT txn_date) AS day_visit
	,SUM(txn_amt) AS total_sales
INTO temp table y_home_grocery_commercial_cust_lvl1
FROM vpm_data.y_home_grocery_commercial_txn
WHERE txn_amt <> 0
GROUP BY member_number
	,revised_category_level_1
;

--Qty per product
DROP TABLE IF EXISTS y_home_grocery_commercial_qty_per_product
;
SELECT A.member_number
	,(A.partner_code || '-' || A.sku_code) AS bu_sku
	,MAX(A.product_name) AS product_name
	,MAX(A.cleaned_brandname) AS cleaned_brandname
	,MAX(A.revised_category_level_1) AS revised_category_level_1
--	,MAX(A.revised_category_level_2) AS revised_category_level_2
--	,MAX(A.revised_category_level_3) AS revised_category_level_3
	,SUM(A.qty) AS qty_per_product
	,COUNT(distinct TXN_DATE) as day_visit_per_product
	--HOME GROCERY
	,MAX(B.total_qty) AS home_grocery_total_qty
	,MAX(B.day_visit) AS home_grocery_day_visit
	,MAX(B.total_sales) AS home_grocery_total_sales
INTO temp table y_home_grocery_commercial_qty_per_product
FROM (SELECT * FROM vpm_data.y_home_grocery_commercial_txn WHERE txn_amt <> 0) A
INNER JOIN (SELECT * FROM y_home_grocery_commercial_cust_lvl1) B
ON A.member_number = B.member_number
GROUP BY A.member_number
	,(A.partner_code || '-' || A.sku_code) 
;

DROP TABLE IF EXISTS vpm_data.y_home_grocery_commercial_all_dimension --- rename
;
SELECT member_number
	,home_grocery_total_qty
	,home_grocery_day_visit
	,home_grocery_total_sales
	--Overall fashion
	,MAX(CASE WHEN qty_row_num2 = 1 THEN product_name END) AS max_home_grocery_product_name
	,MAX(CASE WHEN qty_row_num2 = 1 THEN qty_per_product END) AS max_home_grocery_qty_per_product
	,MAX(CASE WHEN qty_row_num2 = 1 THEN day_visit_per_product END) AS max_home_grocery_day_visit_per_product
	,MAX(CASE WHEN qty_row_num2 = 1 THEN qty_per_product/day_visit_per_product END) AS max_home_grocery_qty_per_visit
INTO vpm_data.y_home_grocery_commercial_all_dimension
FROM (SELECT *
			--,ROW_NUMBER() OVER(PARTITION BY member_number, revised_category_level_2 ORDER BY qty_per_product DESC) AS qty_row_num
			,ROW_NUMBER() OVER(PARTITION BY member_number ORDER BY qty_per_product DESC) AS qty_row_num2
	FROM y_home_grocery_commercial_qty_per_product) A
--WHERE qty_row_num = 1
GROUP BY member_number
	,home_grocery_total_qty
	,home_grocery_day_visit
	,home_grocery_total_sales
;

-- CREATE REF TABLE PERCENTILE OF ALL DIMENSIONS 
--DROP TABLE IF EXISTS vpm_data.y_home_grocery_pct
--;
----Qty
--SELECT	'QUANTITY' as dimension
--		,'home grocery' as cate
--		,approximate percentile_disc (0.995) within group (order by home_grocery_total_qty nulls first) as pct995
--		,approximate percentile_disc (0.999) within group (order by home_grocery_total_qty nulls first) as pct999
--		,approximate percentile_disc (0.9998) within group (order by home_grocery_total_qty nulls first) as pct9998
--INTO vpm_data.y_home_grocery_pct
--FROM vpm_data.y_home_grocery_commercial_all_dimension
--UNION ALL
----Max Qty by SKU
--SELECT	'MAX QUANTITY BY SKU' as dimension
--		,'home grocery' as cate
--		,approximate percentile_disc (0.995) within group (order by max_home_grocery_qty_per_product nulls first) as pct995
--		,approximate percentile_disc (0.999) within group (order by max_home_grocery_qty_per_product nulls first) as pct999
--		,approximate percentile_disc (0.9998) within group (order by max_home_grocery_qty_per_product nulls first) as pct9998
--FROM vpm_data.y_home_grocery_commercial_all_dimension
--UNION ALL		
----Spending
--SELECT	'SPENDING' as dimension
--		,'home grocery' as cate
--		,approximate percentile_disc (0.995) within group (order by home_grocery_total_sales nulls first) as pct995
--		,approximate percentile_disc (0.999) within group (order by home_grocery_total_sales nulls first) as pct999
--		,approximate percentile_disc (0.9998) within group (order by home_grocery_total_sales nulls first) as pct9998
--FROM vpm_data.y_home_grocery_commercial_all_dimension
--UNION ALL
----Day Visit
--SELECT	'DAY VISIT' as dimension
--		,'home grocery' as cate
--		,approximate percentile_disc (0.995) within group (order by home_grocery_day_visit nulls first) as pct995
--		,approximate percentile_disc (0.999) within group (order by home_grocery_day_visit nulls first) as pct999
--		,approximate percentile_disc (0.9998) within group (order by home_grocery_day_visit nulls first) as pct9998
--FROM vpm_data.y_home_grocery_commercial_all_dimension
--UNION ALL
----QTY PER VISIT
--SELECT	'QTY PER VISIT' as dimension
--		,'home grocery' as cate
--		,approximate percentile_disc (0.995) within group (order by max_home_grocery_qty_per_visit nulls first) as pct995
--		,approximate percentile_disc (0.999) within group (order by max_home_grocery_qty_per_visit nulls first) as pct999
--		,approximate percentile_disc (0.9998) within group (order by max_home_grocery_qty_per_visit nulls first) as pct9998
--FROM vpm_data.y_home_grocery_commercial_all_dimension
--ORDER BY cate, dimension
--;
--
--SELECT *
--from vpm_data.y_home_grocery_pct
--order by dimension, cate
--;

/************************************************************ ADD COLUMN COMMERCIAL FLAG ************************************************************/
ALTER TABLE vpm_data.y_home_grocery_commercial_all_dimension
ADD column home_grocery_total_qty_score1 float
;
ALTER TABLE vpm_data.y_home_grocery_commercial_all_dimension
ADD column max_home_grocery_qty_per_product_score1 float
;

ALTER TABLE vpm_data.y_home_grocery_commercial_all_dimension
ADD column home_grocery_total_sales_score1 float
;

update vpm_data.y_home_grocery_commercial_all_dimension
set home_grocery_total_qty_score1 = case when home_grocery_total_qty > 414 then 1 
										 when home_grocery_total_qty < 0 then 0 
										 else home_grocery_total_qty*1.0/414 
									end
	,max_home_grocery_qty_per_product_score1 = case when max_home_grocery_qty_per_product > 108 then 1 
													 when max_home_grocery_qty_per_product < 0 then 0 
													 else max_home_grocery_qty_per_product*1.0/108 
												end
	,home_grocery_total_sales_score1 = case when home_grocery_total_sales > 25000 then 1 
											 when home_grocery_total_sales < 0 then 0 
											 else home_grocery_total_sales*1.0/25000 
										end
;

ALTER TABLE vpm_data.y_home_grocery_commercial_all_dimension
ADD COLUMN home_grocery_score1 float
;

update vpm_data.y_home_grocery_commercial_all_dimension
set home_grocery_score1 = (home_grocery_total_qty_score1*0.25) + (max_home_grocery_qty_per_product_score1*0.5) + (home_grocery_total_sales_score1*0.25)
;

ALTER TABLE vpm_data.y_home_grocery_commercial_all_dimension
ADD COLUMN commercial_flag varchar(3)
;

update vpm_data.y_home_grocery_commercial_all_dimension
set commercial_flag = case when home_grocery_score1 > 0.88 and home_grocery_total_sales >= 25000 then 'YES' else 'N/A' end
;

-- check
select count(*)
from vpm_data.y_home_grocery_commercial_all_dimension
where commercial_flag = 'YES'




	