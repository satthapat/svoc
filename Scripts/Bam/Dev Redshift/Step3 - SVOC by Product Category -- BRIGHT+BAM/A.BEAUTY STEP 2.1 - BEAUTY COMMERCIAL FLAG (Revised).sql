
-- create beauty txn 
DROP TABLE IF EXISTS vpm_data.y_beauty_commercial_txn 
;
SELECT a.*
INTO vpm_data.y_beauty_commercial_txn
FROM vpm_data.pm_all_salessku_1yr a inner join vpm_data.a_beautyprestige_product_master e
					ON CASE WHEN A.PARTNER_CODE = 'RBS' THEN 'CDS'
							WHEN A.PARTNER_CODE = 'TWD' THEN 'HWS' ELSE A.PARTNER_CODE END = E.PARTNER_CODE
								AND A.SKU_CODE = E.PAD_SKU_ID
WHERE a.revised_category_level_1 = 'BEAUTY' and coalesce(a.member_number,'') <> ''
	-- exclude some products
	and upper(a.product_name) not like '%�ͧ��%'
	and upper(a.product_name) not like '%FREE%GIFT%'
	and upper(a.product_name) not like '%��ԡ��%'
	and upper(a.product_name) not like '%����%'
	and upper(a.product_name) not like '%COTTON%PAD%'
	and upper(a.product_name) not like '%FACIAL%COTTON%'
	and upper(a.product_name) not like '%SILK%COTTON%'
	and upper(a.product_name) not like '%COTTON%'
	and upper(a.product_name) not like '%TISSUE%'
	and upper(a.product_name) not like '%PAPER%'
;

DROP TABLE IF EXISTS y_beauty_commercial_cust_lvl1 
;
SELECT member_number
	,revised_category_level_1
	,SUM(qty) AS total_qty
	,COUNT(DISTINCT txn_date) AS day_visit
	,SUM(txn_amt) AS total_sales
INTO temp table y_beauty_commercial_cust_lvl1
FROM vpm_data.y_beauty_commercial_txn
WHERE txn_amt <> 0
GROUP BY member_number
	,revised_category_level_1
;

--Qty per product
DROP TABLE IF EXISTS y_beauty_commercial_qty_per_product
;
SELECT A.member_number
	,(A.partner_code || '-' || A.sku_code) AS bu_sku
	,MAX(A.product_name) AS product_name
	,MAX(A.cleaned_brandname) AS cleaned_brandname
	,MAX(A.revised_category_level_1) AS revised_category_level_1
--	,MAX(A.revised_category_level_2) AS revised_category_level_2
--	,MAX(A.revised_category_level_3) AS revised_category_level_3
	,SUM(A.qty) AS qty_per_product
	,COUNT(distinct TXN_DATE) as day_visit_per_product
	--FASHION
	,MAX(B.total_qty) AS beauty_total_qty
	,MAX(B.day_visit) AS beauty_day_visit
	,MAX(B.total_sales) AS beauty_total_sales
INTO temp table y_beauty_commercial_qty_per_product
FROM (SELECT * FROM vpm_data.y_beauty_commercial_txn WHERE txn_amt <> 0) A
INNER JOIN (SELECT * FROM y_beauty_commercial_cust_lvl1) B
ON A.member_number = B.member_number
GROUP BY A.member_number
	,(A.partner_code || '-' || A.sku_code) 
;

DROP TABLE IF EXISTS vpm_data.y_beauty_commercial_all_dimension --- rename
;
SELECT member_number
	,beauty_total_qty
	,beauty_day_visit
	,beauty_total_sales
	--Overall beauty
	,MAX(CASE WHEN qty_row_num2 = 1 THEN product_name END) AS max_beauty_product_name
	,MAX(CASE WHEN qty_row_num2 = 1 THEN qty_per_product END) AS max_beauty_qty_per_product
	,MAX(CASE WHEN qty_row_num2 = 1 THEN day_visit_per_product END) AS max_beauty_day_visit_per_product
	,MAX(CASE WHEN qty_row_num2 = 1 THEN qty_per_product/day_visit_per_product END) AS max_beauty_qty_per_visit
INTO vpm_data.y_beauty_commercial_all_dimension
FROM (SELECT *
			--,ROW_NUMBER() OVER(PARTITION BY member_number, revised_category_level_2 ORDER BY qty_per_product DESC) AS qty_row_num
			,ROW_NUMBER() OVER(PARTITION BY member_number ORDER BY qty_per_product DESC) AS qty_row_num2
	FROM y_beauty_commercial_qty_per_product) A
--WHERE qty_row_num = 1
GROUP BY member_number
	,beauty_total_qty
	,beauty_day_visit
	,beauty_total_sales
;

-- CREATE REF TABLE PERCENTILE OF ALL DIMENSIONS 
--DROP TABLE IF EXISTS vpm_data.y_beauty_pct
--;
----Qty
--SELECT	'QUANTITY' as dimension
--		,'beauty' as cate
--		,approximate percentile_disc (0.995) within group (order by beauty_total_qty nulls first) as pct995
--		,approximate percentile_disc (0.999) within group (order by beauty_total_qty nulls first) as pct999
--		,approximate percentile_disc (0.9998) within group (order by beauty_total_qty nulls first) as pct9998
--INTO vpm_data.y_beauty_pct
--FROM vpm_data.y_beauty_commercial_all_dimension
--UNION ALL
----Max Qty by SKU
--SELECT	'MAX QUANTITY BY SKU' as dimension
--		,'beauty' as cate
--		,approximate percentile_disc (0.995) within group (order by max_beauty_qty_per_product nulls first) as pct995
--		,approximate percentile_disc (0.999) within group (order by max_beauty_qty_per_product nulls first) as pct999
--		,approximate percentile_disc (0.9998) within group (order by max_beauty_qty_per_product nulls first) as pct9998
--FROM vpm_data.y_beauty_commercial_all_dimension
--UNION ALL		
----Spending
--SELECT	'SPENDING' as dimension
--		,'beauty' as cate
--		,approximate percentile_disc (0.995) within group (order by beauty_total_sales nulls first) as pct995
--		,approximate percentile_disc (0.999) within group (order by beauty_total_sales nulls first) as pct999
--		,approximate percentile_disc (0.9998) within group (order by beauty_total_sales nulls first) as pct9998
--FROM vpm_data.y_beauty_commercial_all_dimension
--UNION ALL
----Day Visit
--SELECT	'DAY VISIT' as dimension
--		,'beauty' as cate
--		,approximate percentile_disc (0.995) within group (order by beauty_day_visit nulls first) as pct995
--		,approximate percentile_disc (0.999) within group (order by beauty_day_visit nulls first) as pct999
--		,approximate percentile_disc (0.9998) within group (order by beauty_day_visit nulls first) as pct9998
--FROM vpm_data.y_beauty_commercial_all_dimension
--UNION ALL
----QTY PER VISIT
--SELECT	'QTY PER VISIT' as dimension
--		,'beauty' as cate
--		,approximate percentile_disc (0.995) within group (order by max_beauty_qty_per_visit nulls first) as pct995
--		,approximate percentile_disc (0.999) within group (order by max_beauty_qty_per_visit nulls first) as pct999
--		,approximate percentile_disc (0.9998) within group (order by max_beauty_qty_per_visit nulls first) as pct9998
--FROM vpm_data.y_beauty_commercial_all_dimension
--ORDER BY cate, dimension
--;
--
--SELECT *
--from vpm_data.y_beauty_pct
--order by dimension, cate
--;

/************************************************************ ADD COLUMN COMMERCIAL FLAG ************************************************************/
ALTER TABLE vpm_data.y_beauty_commercial_all_dimension
ADD column beauty_total_qty_score1 float
;
ALTER TABLE vpm_data.y_beauty_commercial_all_dimension
ADD column max_beauty_qty_per_product_score1 float
;

ALTER TABLE vpm_data.y_beauty_commercial_all_dimension
ADD column beauty_total_sales_score1 float
;

ALTER TABLE vpm_data.y_beauty_commercial_all_dimension
ADD column max_beauty_qty_per_visit_score1 float
;

update vpm_data.y_beauty_commercial_all_dimension
set beauty_total_qty_score1 = case when beauty_total_qty > 112 then 1 
										 when beauty_total_qty < 0 then 0 
										 else beauty_total_qty*1.0/112 
									end
	,max_beauty_qty_per_product_score1 = case when max_beauty_qty_per_product > 40 then 1 
													 when max_beauty_qty_per_product < 0 then 0 
													 else max_beauty_qty_per_product*1.0/40
												end
	,beauty_total_sales_score1 = case when beauty_total_sales > 353000 then 1 
											 when beauty_total_sales < 0 then 0 
											 else beauty_total_sales*1.0/353000 
										end
	,max_beauty_qty_per_visit_score1 = case when max_beauty_qty_per_visit > 15 then 1 
													 when max_beauty_qty_per_visit < 0 then 0 
													 else max_beauty_qty_per_visit*1.0/15
												end
;

ALTER TABLE vpm_data.y_beauty_commercial_all_dimension
ADD COLUMN beauty_score1 float
;

update vpm_data.y_beauty_commercial_all_dimension
set beauty_score1 = (beauty_total_qty_score1*0.2) + (max_beauty_qty_per_product_score1*0.4) + (beauty_total_sales_score1*0.2) + (max_beauty_qty_per_visit_score1*0.2)
;

ALTER TABLE vpm_data.y_beauty_commercial_all_dimension
ADD COLUMN commercial_flag varchar(3)
;

update vpm_data.y_beauty_commercial_all_dimension
set commercial_flag = case when beauty_score1 > 0.82 then 'YES' else 'N/A' end
;

UPDATE vpm_data.PM_SKUTAGGING_BEAUTY_CUST_SEGMENT
SET BEAUTY_COMMERCIAL_FLAG = NULL
;

UPDATE vpm_data.PM_SKUTAGGING_BEAUTY_CUST_SEGMENT
SET BEAUTY_COMMERCIAL_FLAG = CASE WHEN commercial_flag = 'YES' THEN 1 END
FROM vpm_data.y_beauty_commercial_all_dimension A
WHERE A.MEMBER_NUMBER = vpm_data.PM_SKUTAGGING_BEAUTY_CUST_SEGMENT.MEMBER_NUMBER
;



-- check
select count(*)
from vpm_data.y_beauty_commercial_all_dimension
where commercial_flag = 'YES'




	