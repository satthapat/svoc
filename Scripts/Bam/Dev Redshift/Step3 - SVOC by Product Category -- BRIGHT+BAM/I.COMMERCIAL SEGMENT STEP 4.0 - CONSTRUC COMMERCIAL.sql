
DROP TABLE IF EXISTS vpm_data.y_construction_commercial_all_dimension;
select distinct member_number
				,case when customer_final_segment in ('BIG CONTRACTORS') and segment_year = '2020'
						then 'YES' else 'N/A' end as commercial_flag
into vpm_data.y_construction_commercial_all_dimension
from vpm_data.u_chg_svoc_dec20_new_schema 
;

-- check
select count(*)
from vpm_data.y_construction_commercial_all_dimension
where commercial_flag = 'YES'


	