INSERT INTO VPM_DATA.SALESSKU_LOG_TIME(task,start_time) VALUES ('LOCATION Step 1_Create BUs Branches Location Ref',current_timestamp)
;

/*UPDATE VPM_DATA.CG_LOCATION_MASTER
SET PROVINCE_ENG = CASE WHEN PROVINCE_ENG LIKE 'Songkha' THEN 'Songkhla'
		                WHEN PROVINCE_ENG = 'Chonburi' THEN 'Chon Buri'
			            WHEN PROVINCE_ENG = 'Lopburi' THEN 'Lop Buri'
			            WHEN PROVINCE_ENG = 'Phang Nga' THEN 'Phangnga'
		  	            WHEN PROVINCE_ENG = 'Samut Prakan' THEN 'Samut Prakarn'
			            WHEN PROVINCE_ENG = 'Prachinburi' THEN 'Prachin Buri'
			            WHEN PROVINCE_ENG = 'Nong Bua Lamphu' THEN 'Nong Bua Lam Phu'
			            WHEN PROVINCE_ENG = 'Angthong' THEN 'Ang Thong'
			            WHEN Province_ENG = 'Samut Prakan' THEN 'Samut Prakarn'
		                WHEN Province_ENG = 'กรุงเทพมหานคร' THEN 'Bangkok'
		                WHEN Province_ENG = 'บางรัก' THEN 'Bangkok'
	                    ELSE PROVINCE_ENG END; */

/** CREATE BRANCH LOCATION LOOKUP TABLE **/
CREATE temp TABLE cg_location_master_dedup as
SELECT * FROM (
SELECT *,row_number() OVER (partition BY partner_code,branch_code ORDER BY locationname) AS rn 
FROM the1.analysis_data.CG_LOCATION_MASTER
) a WHERE rn = 1;
                  
DROP TABLE IF EXISTS VPM_Backup.GEO_BU_LOCATION_MASTER;
CREATE TABLE VPM_Backup.GEO_BU_LOCATION_MASTER AS
SELECT A.PARTNER_CODE
	,A.PARTNER_NAME
	,B.branch_code
	,C.BRANCH_NAME AS BranchEnglishname
	,C.LocationName
	,C.PROVINCE
	,C.DISTRICT
	,C.PROVINCE_ENG
	,C.STORE_TYPE
	,C.BUILDING
	,C.LocationName AS BUILDING_NAME
	,C.Centrality_Region
	,C.Centrality_Location
	,D.google_keyword AS GOOGLE_KEYWORD
	,CASE WHEN COALESCE(C.Latitude,0) <> 0 THEN C.Latitude ELSE COALESCE(REPLACE(D.google_lat,'',NULL)::FLOAT,0) END AS Latitude
	,CASE WHEN COALESCE(C.Longitude,0) <> 0 THEN C.Longitude ELSE COALESCE(REPLACE(D.google_lng,'',NULL)::FLOAT,0) END AS Longitude
	,D.postal AS POSTAL_CODE
	,E.REGION1 AS REGION1
	,CASE WHEN COALESCE(UPPER(C.PROVINCE_ENG),'') IN ('','ONLINE','EVENT','OTHER','TELE SALES') THEN E.REGION2 ELSE C.PROVINCE_ENG END AS REGION2
	,CAST(NULL AS INT) AS MALL_BUID
	,CAST(NULL AS VARCHAR(4)) AS MALL_BUCODE
	,CAST(NULL AS VARCHAR(100)) AS MALL_BRANCH_NAME
	,CAST(NULL AS INT) AS MALL_BRANCHID
	,CAST(NULL AS float) AS PCT_OCCURANCE
	,CAST(NULL AS float) AS MALL_LATITUDE
	,CAST(NULL AS float) AS MALL_LONGITUDE
	,CAST(NULL AS VARCHAR(50)) AS MALL_POSTAL_CODE
	,CAST(NULL AS VARCHAR(50)) AS MALL_REGION1
	,CAST(NULL AS VARCHAR(50)) AS MALL_REGION2
  FROM the1.Analysis_Data.MS_PARTNER A
  LEFT JOIN the1.Analysis_Data.ms_branch B ON A.PARTNER_CODE = B.partner_code
  LEFT JOIN cg_location_master_dedup C ON B.partner_code = C.partner_code and B.branch_code = C.branch_code /* From K.BUM */													
  LEFT JOIN VPM_Backup.BU_Import_new_schema D ON B.partner_code = D.bucode 
  										AND B.branch_code = D.branchcode /* From Google API Cleansing (One-Time) */
  LEFT JOIN (SELECT DISTINCT "ZIP" AS ZIP,"REGION1"AS REGION1,
					CASE WHEN "REGION2" = 'Chonburi' THEN 'Chon Buri'
						 WHEN "REGION2" = 'Lopburi' THEN 'Lop Buri'
						 WHEN "REGION2" = 'Phang Nga' THEN 'Phangnga'
						 WHEN "REGION2" = 'Samut Prakan' THEN 'Samut Prakarn'
						 WHEN "REGION2" = 'Prachinburi' THEN 'Prachin Buri'
						 WHEN "REGION2" = 'Nong Bua Lamphu' THEN 'Nong Bua Lam Phu'
					ELSE "REGION2" END AS REGION2
			 FROM VPM_DATA.GeoPC) E ON D.postal = E.ZIP
;

UPDATE VPM_Backup.GEO_BU_LOCATION_MASTER
SET REGION1 = T."REGION1"
FROM (SELECT DISTINCT "REGION1",
					CASE WHEN "REGION2" = 'Chonburi' THEN 'Chon Buri'
						 WHEN "REGION2" = 'Lopburi' THEN 'Lop Buri'
						 WHEN "REGION2" = 'Phang Nga' THEN 'Phangnga'
						 WHEN "REGION2" = 'Samut Prakan' THEN 'Samut Prakarn'
						 WHEN "REGION2" = 'Prachinburi' THEN 'Prachin Buri'
						 WHEN "REGION2" = 'Nong Bua Lamphu' THEN 'Nong Bua Lam Phu'
					ELSE "REGION2" END
	  FROM VPM_DATA.GeoPC) T
WHERE VPM_Backup.GEO_BU_LOCATION_MASTER.REGION2 = T."REGION2"
;

/*** CLEAN LOCATION MANUAL ***/
--DROP TABLE IF EXISTS CLEAN_MANUAL;
SELECT A.BUID,A.BUCODE,A.BUENGLISHNAME,A.BRANCHID,A.BRANCHCODE,A.BRANCHENGLISHNAME,A.LOCATIONNAME,A.GOOGLE_KEYWORD,A.LATITUDE,A.LONGITUDE,A.POSTAL_CODE
	  ,CASE WHEN A.REGION1 = 'Cenrtral' THEN 'Central'
			WHEN A.REGION2 = 'Chonburi' THEN 'East'
	   ELSE A.REGION1 END AS REGION1
	  ,CASE WHEN A.REGION2 = 'Chonburi' THEN 'Chon Buri'
			WHEN A.REGION2 = 'Lopburi' THEN 'Lop Buri'
			WHEN A.REGION2 = 'Phang Nga' THEN 'Phangnga'
			WHEN A.REGION2 = 'Samut Prakan' THEN 'Samut Prakarn'
			WHEN A.REGION2 = 'Prachinburi' THEN 'Prachin Buri'
			WHEN A.REGION2 = 'Nong Bua Lamphu' THEN 'Nong Bua Lam Phu'
	   ELSE A.REGION2 END AS REGION2
INTO TEMP TABLE CLEAN_MANUAL
FROM VPM_Backup.BU_Master_Location_20210303 a /* From Thaiwatsadu Manual Tagging */
left join VPM_Backup.GEO_BU_LOCATION_MASTER b 
on a.bucode = b.partner_code
	and a.branchcode = b.branch_code
WHERE (b.POSTAL_CODE = '' or b.POSTAL_CODE is null)
and COALESCE(a.POSTAL_CODE,'') <> '' and COALESCE(a.REGION1,'') <> ''
and a.POSTAL_CODE <> 'NULL'
;


UPDATE VPM_Backup.GEO_BU_LOCATION_MASTER
SET GOOGLE_KEYWORD = CASE WHEN T.GOOGLE_KEYWORD <> '' THEN SUBSTRING(T.GOOGLE_KEYWORD,1,50) ELSE '' END,
	Latitude = CASE WHEN T.Latitude <> '' THEN T.Latitude::float ELSE NULL END,
	Longitude = CASE WHEN T.Longitude <> '' THEN T.Longitude::float ELSE NULL END,
	POSTAL_CODE = CAST(T.POSTAL_CODE AS varchar(50)),
	REGION1 = CAST(T.REGION1 AS varchar(60)),
	REGION2 = CAST(T.REGION2 AS varchar(60))
FROM CLEAN_MANUAL T
WHERE VPM_Backup.GEO_BU_LOCATION_MASTER.branch_code = T.branchcode
and VPM_Backup.GEO_BU_LOCATION_MASTER.partner_code = T.BUCode
;

/*** CLEAN CFM BRANCH PROVINCE FROM EXCEL FILE ***/
--DROP TABLE IF EXISTS CLEAN_CFM;
SELECT A.BUID, A.BRANCHID, A.POSTAL_CODE, A.REGION1, A.partner_code, A.branchcode
	  ,CASE WHEN A.REGION2 = 'Chonburi' THEN 'Chon Buri'
			WHEN A.REGION2 = 'Lopburi' THEN 'Lop Buri'
			WHEN A.REGION2 = 'Phang Nga' THEN 'Phangnga'
			WHEN A.REGION2 = 'Samut Prakan' THEN 'Samut Prakarn'
			WHEN A.REGION2 = 'Prachinburi' THEN 'Prachin Buri'
			WHEN A.REGION2 = 'Nong Bua Lamphu' THEN 'Nong Bua Lam Phu'
	   ELSE A.REGION2 END AS REGION2
INTO TEMP TABLE CLEAN_CFM
FROM VPM_Backup.BU_CFM_LOCATION_20210303 a /* File from FAMILYMART */
left join VPM_Backup.GEO_BU_LOCATION_MASTER b on a.partner_code = b.partner_code and a.branchcode = b.branch_code
WHERE (b.POSTAL_CODE = '' or b.POSTAL_CODE is null)
and a.REGION2 IS NOT NULL
;

UPDATE VPM_Backup.GEO_BU_LOCATION_MASTER
SET POSTAL_CODE = T.POSTAL_CODE,
	REGION1 = T.REGION1,
	REGION2 = T.REGION2
FROM CLEAN_CFM T
WHERE VPM_Backup.GEO_BU_LOCATION_MASTER.branch_code = T.branchcode
and VPM_Backup.GEO_BU_LOCATION_MASTER.partner_code = T.partner_code
;

UPDATE VPM_DATA.SALESSKU_LOG_TIME SET end_time = current_timestamp WHERE task = 'LOCATION Step 1_Create BUs Branches Location Ref' AND EXTRACT(MONTH FROM start_time) = EXTRACT(MONTH FROM current_timestamp)
;
