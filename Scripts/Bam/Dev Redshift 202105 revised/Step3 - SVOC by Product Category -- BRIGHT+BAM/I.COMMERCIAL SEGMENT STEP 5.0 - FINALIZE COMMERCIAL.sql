-- All CG commercial 
drop table if exists vpm_data.y_cg_commercial_cust;
select distinct member_number, 'YES' as cg_commercial_flag
into vpm_data.y_cg_commercial_cust
from 
	(-- fashion
		select distinct member_number 
		from vpm_data.y_fashion_commercial_all_dimension 
		where commercial_flag = 'YES'
		union all
		-- beauty
		select distinct member_number 
		from vpm_data.y_beauty_commercial_all_dimension
		where commercial_flag = 'YES'
		union all
		-- HBA
		select distinct member_number 
		from vpm_data.y_hba_commercial_all_dimension 
		where commercial_flag = 'YES'
		union all
		-- home grocery
		select distinct member_number 
		from vpm_data.y_home_grocery_commercial_all_dimension
		where commercial_flag = 'YES'
		union all
		-- food grocery
		select distinct member_number 
		from vpm_data.y_food_grocery_commercial_all_dimension
		where commercial_flag = 'YES'
		union all
		-- home elec
		select distinct member_number 
		from vpm_data.y_home_elec_commercial_all_dimension 
		where commercial_flag = 'YES'
		union all
		-- home non
		select distinct member_number 
		from vpm_data.y_home_non_elec_commercial_all_dimension 
		where commercial_flag = 'YES'
		union all
		-- elec gadget
		select distinct member_number 
		from vpm_data.y_elec_gadget_commercial_all_dimension 
		where commercial_flag = 'YES'
		union all
		-- office 
		select distinct member_number 
		from vpm_data.y_office_supply_commercial_all_dimension  
		where commercial_flag = 'YES'
		union all
		-- construction
		select distinct member_number 
		from vpm_data.y_construction_commercial_all_dimension  
		where commercial_flag = 'YES'
--		union all
		-- mom & kids
--		select distinct member_number
--		from vpm_data.m_kid_commercial_members   
		) a
		
		
		