CREATE OR REPLACE PROCEDURE rename_previous_table()
LANGUAGE plpgsql
AS $$
DECLARE
	PREVIOUS2_MONTH TEXT;
BEGIN
 	PREVIOUS2_MONTH := (SELECT TO_CHAR(CURRENT_TIMESTAMP-INTERVAL '2 MONTH','MON'));
--rename _previous to name as last two month
 	EXECUTE 'ALTER TABLE vpm_data.pm_product_master_previous RENAME TO pm_product_master_'||PREVIOUS2_MONTH;
	EXECUTE 'ALTER TABLE vpm_data.pm_skutagging_beauty_product_master_previous RENAME TO pm_skutagging_beauty_product_master_'||PREVIOUS2_MONTH;
	EXECUTE 'ALTER TABLE vpm_data.pm_skutagging_fashion_product_master_previous RENAME TO pm_skutagging_fashion_product_master_'||PREVIOUS2_MONTH;
	EXECUTE 'ALTER TABLE vpm_data.pm_skutagging_food_product_master_previous RENAME TO pm_skutagging_food_product_master_'||PREVIOUS2_MONTH;
	EXECUTE 'ALTER TABLE VPM_DATA.M_FOOD_GROCERY_LUXURY_CLUSTER_PRODUCT_MASTER_PREVIOUS RENAME TO M_FOOD_GROCERY_LUXURY_CLUSTER_PRODUCT_MASTER_'||PREVIOUS2_MONTH;
	EXECUTE 'ALTER TABLE vpm_data.pm_skutagging_kids_product_master_previous RENAME TO pm_skutagging_kids_product_master_'||PREVIOUS2_MONTH;
	EXECUTE 'ALTER TABLE vpm_data.pm_skutagging_electronics_product_master_previous RENAME TO pm_skutagging_electronics_product_master_'||PREVIOUS2_MONTH;
	EXECUTE 'ALTER TABLE vpm_data.pm_home_product_product_master_previous RENAME TO pm_home_product_product_master_'||PREVIOUS2_MONTH;
	EXECUTE 'ALTER TABLE vpm_data.pm_skutagging_beauty_salessku_1yr_previous RENAME TO pm_skutagging_beauty_salessku_1yr_'||PREVIOUS2_MONTH;
	EXECUTE 'ALTER TABLE vpm_data.pm_skutagging_fashion_salessku_1yr_previous RENAME TO pm_skutagging_fashion_salessku_1yr_'||PREVIOUS2_MONTH;
	EXECUTE 'ALTER TABLE vpm_data.pm_skutagging_kids_salessku_1yr_previous RENAME TO pm_skutagging_kids_salessku_1yr_'||PREVIOUS2_MONTH;
	EXECUTE 'ALTER TABLE vpm_data.pm_skutagging_electronics_salessku_3yr_previous RENAME TO pm_skutagging_electronics_salessku_3yr_'||PREVIOUS2_MONTH;
	EXECUTE 'ALTER TABLE vpm_data.pm_skutagging_food_salessku_6m_previous RENAME TO pm_skutagging_food_salessku_6m_'||PREVIOUS2_MONTH;
	EXECUTE 'ALTER TABLE vpm_data.pm_home_product_salessku_1yr_previous RENAME TO pm_home_product_salessku_1yr_'||PREVIOUS2_MONTH;
	EXECUTE 'ALTER TABLE vpm_data.pm_skutagging_beauty_cust_segment_previous RENAME TO pm_skutagging_beauty_cust_segment_'||PREVIOUS2_MONTH;
	EXECUTE 'ALTER TABLE vpm_data.pm_skutagging_fashion_cust_segment_previous RENAME TO pm_skutagging_fashion_cust_segment_'||PREVIOUS2_MONTH;
	EXECUTE 'ALTER TABLE vpm_data.pm_skutagging_food_cust_segment_previous RENAME TO pm_skutagging_food_cust_segment_'||PREVIOUS2_MONTH;
	EXECUTE 'ALTER TABLE vpm_data.pm_skutagging_electronics_cust_segment_previous RENAME TO pm_skutagging_electronics_cust_segment_'||PREVIOUS2_MONTH;
	EXECUTE 'ALTER TABLE vpm_data.pm_home_product_cust_segment_previous RENAME TO pm_home_product_cust_segment_'||PREVIOUS2_MONTH;
	EXECUTE 'ALTER TABLE vpm_data.pm_svoc_cooking_previous RENAME TO pm_svoc_cooking_'||PREVIOUS2_MONTH;
	EXECUTE 'ALTER TABLE vpm_data.pm_home_product_pet_cust_previous RENAME TO pm_home_product_pet_cust_'||PREVIOUS2_MONTH;
	EXECUTE 'ALTER TABLE vpm_data.pm_svoc_car_ownership_previous RENAME TO pm_svoc_car_ownership_'||PREVIOUS2_MONTH;
	EXECUTE 'ALTER TABLE vpm_data.pm_commercial_segment_previous RENAME TO pm_commercial_segment_'||PREVIOUS2_MONTH;
	EXECUTE 'ALTER TABLE vpm_data.pm_svoc_previous RENAME TO pm_svoc_'||PREVIOUS2_MONTH;
	EXECUTE 'ALTER TABLE vpm_data.pm_customer_cc_previous RENAME TO pm_customer_cc_'||PREVIOUS2_MONTH;
	EXECUTE 'ALTER TABLE vpm_data.pm_svoc_kids_new_previous RENAME TO pm_svoc_kids_new_'||PREVIOUS2_MONTH;


 --rename last month to _pevious
 	EXECUTE 'ALTER TABLE vpm_data.pm_product_master RENAME TO pm_product_master_previous';
	EXECUTE 'ALTER TABLE vpm_data.pm_skutagging_beauty_product_master RENAME TO pm_skutagging_beauty_product_master_previous';
	EXECUTE 'ALTER TABLE vpm_data.pm_skutagging_fashion_product_master RENAME TO pm_skutagging_fashion_product_master_previous';
	EXECUTE 'ALTER TABLE vpm_data.pm_skutagging_food_product_master RENAME TO pm_skutagging_food_product_master_previous';
	EXECUTE 'ALTER TABLE VPM_DATA.M_FOOD_GROCERY_LUXURY_CLUSTER_PRODUCT_MASTER RENAME TO M_FOOD_GROCERY_LUXURY_CLUSTER_PRODUCT_MASTER_PREVIOUS';
	EXECUTE 'ALTER TABLE vpm_data.pm_skutagging_kids_product_master RENAME TO pm_skutagging_kids_product_master_previous';
	EXECUTE 'ALTER TABLE vpm_data.pm_skutagging_electronics_product_master RENAME TO pm_skutagging_electronics_product_master_previous';
	EXECUTE 'ALTER TABLE vpm_data.pm_home_product_product_master RENAME TO pm_home_product_product_master_previous';
	EXECUTE 'ALTER TABLE vpm_data.pm_skutagging_beauty_salessku_1yr RENAME TO pm_skutagging_beauty_salessku_1yr_previous';
	EXECUTE 'ALTER TABLE vpm_data.pm_skutagging_fashion_salessku_1yr RENAME TO pm_skutagging_fashion_salessku_1yr_previous';
	EXECUTE 'ALTER TABLE vpm_data.pm_skutagging_kids_salessku_1yr RENAME TO pm_skutagging_kids_salessku_1yr_previous';
	EXECUTE 'ALTER TABLE vpm_data.pm_skutagging_electronics_salessku_3yr RENAME TO pm_skutagging_electronics_salessku_3yr_previous';
	EXECUTE 'ALTER TABLE vpm_data.pm_skutagging_food_salessku_6m RENAME TO pm_skutagging_food_salessku_6m_previous';
	EXECUTE 'ALTER TABLE vpm_data.pm_home_product_salessku_1yr RENAME TO pm_home_product_salessku_1yr_previous';
	EXECUTE 'ALTER TABLE vpm_data.pm_skutagging_beauty_cust_segment RENAME TO pm_skutagging_beauty_cust_segment_previous';
	EXECUTE 'ALTER TABLE vpm_data.pm_skutagging_fashion_cust_segment RENAME TO pm_skutagging_fashion_cust_segment_previous';
	EXECUTE 'ALTER TABLE vpm_data.pm_skutagging_food_cust_segment RENAME TO pm_skutagging_food_cust_segment_previous';
	EXECUTE 'ALTER TABLE vpm_data.pm_skutagging_electronics_cust_segment RENAME TO pm_skutagging_electronics_cust_segment_previous';
	EXECUTE 'ALTER TABLE vpm_data.pm_home_product_cust_segment RENAME TO pm_home_product_cust_segment_previous';
	EXECUTE 'ALTER TABLE vpm_data.pm_svoc_cooking RENAME TO pm_svoc_cooking_previous';
	EXECUTE 'ALTER TABLE vpm_data.pm_home_product_pet_cust RENAME TO pm_home_product_pet_cust_previous';
	EXECUTE 'ALTER TABLE vpm_data.pm_svoc_car_ownership RENAME TO pm_svoc_car_ownership_previous';
	EXECUTE 'ALTER TABLE vpm_data.pm_commercial_segment RENAME TO pm_commercial_segment_previous';
	EXECUTE 'ALTER TABLE vpm_data.pm_svoc RENAME TO pm_svoc_previous';
	EXECUTE 'ALTER TABLE vpm_data.pm_customer_cc RENAME TO pm_customer_cc_previous';
	EXECUTE 'ALTER TABLE vpm_data.pm_svoc_kids_new RENAME TO pm_svoc_kids_new_previous';

 
END;
$$;

CALL rename_previous_table();

