--Mom and Kid Product
DROP TABLE IF EXISTS tmp_mom_and_kid_product
;
CREATE temp TABLE tmp_mom_and_kid_product AS
--Book
SELECT	*
FROM vpm_data.pm_product_master
WHERE partner_code = 'B2S' AND dept_code = '001' AND Subdept_code IN ('29','143','226')
--142 �Ե������������

--Subscription Package
UNION
SELECT	*
FROM vpm_data.pm_product_master
WHERE partner_code = 'B2S' AND dept_code = '100' AND Subdept_code = '102' AND class_id = '12'

UNION
SELECT *
FROM vpm_data.pm_product_master
WHERE partner_code = 'B2S' AND dept_code = '100' AND Subdept_code = '105' AND class_id in ('14','15','16','22','23','24')

UNION
SELECT *
FROM vpm_data.pm_product_master
WHERE partner_code = 'B2S' AND dept_code = '100' AND Subdept_code = '106'

UNION
SELECT *
FROM vpm_data.pm_product_master
WHERE partner_code = 'B2S' AND dept_code = '100' AND Subdept_code = '108' AND class_id IN ('6','11')

UNION
SELECT *
FROM vpm_data.pm_product_master
WHERE partner_code = 'B2S' AND dept_code = '100' AND Subdept_code = '110' AND class_id IN ('1') AND subclass_id = '3'

UNION
SELECT *
FROM vpm_data.pm_product_master
WHERE partner_code = 'B2S' AND dept_code = '100' AND Subdept_code = '122' AND class_id IN ('5')

UNION
SELECT *
FROM vpm_data.pm_product_master
WHERE partner_code = 'B2S' AND dept_code = '100' AND Subdept_code = '126' AND class_id IN ('1','2','3','4','5','9','10','11','12')

UNION
SELECT *
FROM vpm_data.pm_product_master
WHERE partner_code = 'B2S' AND dept_code = '100' AND subdept_code = '127' AND class_id IN ('2','3','4')

UNION
SELECT *
FROM vpm_data.pm_product_master
WHERE partner_code = 'B2S' AND dept_code = '100' AND Subdept_code = '128'

UNION
SELECT *
FROM vpm_data.pm_product_master
WHERE partner_code = 'B2S' AND dept_code = '100' AND Subdept_code = '129'

/*B2S KIDS FOREIGN BOOK*/
UNION
SELECT *
FROM vpm_data.pm_product_master
WHERE partner_code = 'B2S' AND dept_code = '200' AND Subdept_code = '203' AND class_id = '12' AND subclass_id IN ('1','2','3')

UNION
SELECT *
FROM vpm_data.pm_product_master
WHERE partner_code = 'B2S' AND dept_code = '200' AND Subdept_code = '208'

UNION
SELECT *
FROM vpm_data.pm_product_master
WHERE partner_code = 'B2S' AND dept_code = '200' AND Subdept_code = '209' AND class_id = '1' AND subclass_id = '4'

UNION
SELECT *
FROM vpm_data.pm_product_master
WHERE partner_code = 'B2S' AND dept_code = '300' AND subdept_code IN ('304') AND class_id IN ('3') AND subclass_id in ('6')

UNION
SELECT *
FROM vpm_data.pm_product_master
WHERE partner_code = 'B2S' AND dept_code = '800' AND Subdept_code = '808'

/*B2S KIDS FURNITURE*/
UNION
SELECT *
FROM vpm_data.pm_product_master
WHERE partner_code = 'B2S' AND dept_code = '320' AND Subdept_code = '326' AND class_id = '4'

UNION
SELECT *
FROM vpm_data.pm_product_master
WHERE partner_code = 'B2S' AND dept_code = '320' AND Subdept_code = '328'

UNION
SELECT *
FROM vpm_data.pm_product_master
WHERE partner_code = 'B2S' AND dept_code = '330' AND Subdept_code = '333' AND class_id = '1' AND subclass_id = '2'

UNION
SELECT *
FROM vpm_data.pm_product_master
WHERE partner_code = 'B2S' AND dept_code = '360'

UNION
SELECT *
FROM vpm_data.pm_product_master
WHERE partner_code = 'B2S' AND dept_code = '400' AND subdept_code IN ('407') AND class_id IN ('19')

UNION
SELECT *
FROM vpm_data.pm_product_master
WHERE partner_code = 'B2S'
AND dept_name LIKE 'FOREIGN BOOKS'
AND subdept_name LIKE 'LIVING'
AND class_name LIKE 'FAMILY & RELATIONSHIP'
AND subclass_name IN ('BABY JOURNALS','MOTHER & CHILDCARE','PREGNANCY & CHILDBIRTH','TEENAGER')

UNION
SELECT *
FROM vpm_data.pm_product_master
WHERE partner_code = 'B2S'
AND dept_name LIKE 'FOREIGN BOOKS'
AND subdept_name LIKE 'MAGAZINES & NEWSPAPER'
AND class_name IN ('FAMILY & PARENTING','FAMILY PLANNING')

/*PWB GADGET*/
UNION
SELECT *
FROM vpm_data.pm_product_master
WHERE partner_code = 'PWB' AND dept_code = '10' AND Subdept_code = '1001' AND class_id = '100108'

UNION
SELECT *
FROM vpm_data.pm_product_master
WHERE partner_code = 'PWB' AND dept_code = '010' AND Subdept_code = '1002' AND class_id = '100205'

/*OFM/ODP KIDS FASHION*/
UNION
SELECT *
FROM vpm_data.pm_product_master
WHERE partner_code = 'OFM' AND dept_code = '580' AND Subdept_code = '809' AND class_id = '120'

/*CFR*/
UNION
SELECT *
FROM vpm_data.pm_product_master
WHERE partner_code = 'CFR' AND dept_code = '0002' AND subdept_code IN ('0028') AND class_id IN ('0283')

UNION
SELECT *
FROM vpm_data.pm_product_master
WHERE partner_code = 'CFR' AND dept_code = '0002' AND subdept_code IN ('0028') AND class_id IN ('0285') AND subclass_id in ('0001','0003')

/*CFM CHILDREN BOOK*/
UNION
SELECT *
FROM vpm_data.pm_product_master
WHERE partner_code = 'CFM' AND dept_code = '28' AND Subdept_code = '283' AND class_id = '1' AND subclass_id IN ('3','4')

UNION
SELECT *
FROM vpm_data.pm_product_master
WHERE partner_code = 'CFM' AND dept_code = '28' AND subdept_code IN ('285') AND class_id IN ('1')

UNION
SELECT *
FROM vpm_data.pm_product_master
WHERE partner_code = 'CFM' AND subdept_name = 'BABY CARE' AND class_name = 'BABY DIAPERS'
--partner_code = 'CFM' AND dept_code = '23' AND subdept_code = '232' AND subclass_id IN ('2','3','4','5','6')

--Fashion (rain coat, sock)
UNION
SELECT *
FROM vpm_data.pm_product_master
WHERE partner_code = 'CFM' AND class_name = 'BOY ACCESSORIES'
--partner_code = 'CFM' AND dept_code = '27' AND subdept_code = '278' AND class_id = '4'

UNION
SELECT *
FROM vpm_data.pm_product_master
WHERE partner_code = 'CFM' AND dept_name = 'FASHION' AND subdept_name = 'BABY GOODS'
--partner_code = 'CFM' AND dept_code = '27' AND subdept_code = '279'

UNION
SELECT *
FROM vpm_data.pm_product_master
WHERE partner_code = 'CFM' AND class_name = 'CHILDREN BOOK'

UNION
SELECT *
FROM vpm_data.pm_product_master
WHERE partner_code = 'CFM' AND dept_name = 'MILK & BABY FOOD'

UNION
SELECT *
FROM vpm_data.pm_product_master
WHERE partner_code = 'CFM' AND dept_name = 'PERSONAL CARE' AND subdept_name = 'BABIES PRODUCTS'

UNION
SELECT *
FROM vpm_data.pm_product_master
WHERE partner_code = 'CFM' AND dept_name = 'GM/NON FMCG' AND subdept_name = 'TOYS/GIFTS/ENTERTAIN/NON FMCG' AND class_name = 'TOYS/NON FMCG' AND subclass_name IN ('BOYS TOYS','GIRL TOYS')

UNION
SELECT *
FROM vpm_data.pm_product_master
WHERE partner_code = 'CFM' AND subclass_name LIKE 'CHILDREN''S TOOTHBRUSHES'

UNION
SELECT *
FROM vpm_data.pm_product_master
WHERE partner_code = 'CFM' AND class_name LIKE 'POWDER MILK'

--MJT
UNION
SELECT *
FROM vpm_data.pm_product_master
WHERE partner_code = 'MJT' AND dept_code LIKE '1' AND subdept_code LIKE '104' AND class_id LIKE '113' AND subclass_id LIKE '319'

UNION
SELECT *
FROM vpm_data.pm_product_master
WHERE partner_code = 'MJT' AND dept_code LIKE '1' AND subdept_code LIKE '102' AND class_id LIKE '11' AND subclass_id LIKE '88'

--CDS
UNION
SELECT	*
FROM vpm_data.pm_product_master
WHERE partner_code = 'CDS' and dept_code = '9'

UNION
SELECT	*
FROM vpm_data.pm_product_master
WHERE partner_code = 'CDS' and dept_name LIKE 'STANDALONE' AND class_name LIKE '%TOY%'

UNION
SELECT	*
FROM vpm_data.pm_product_master
WHERE partner_code = 'CDS' AND dept_code = '12' AND subdept_code IN ('122') AND class_id IN ('101','102','105','107','108')

UNION
SELECT	*
FROM vpm_data.pm_product_master
WHERE partner_code = 'CDS' AND dept_code = '12' AND subdept_code IN ('122') AND class_id IN ('104') AND subclass_id in ('1','5','7','19')

/*CDS,RBS Lingerie �ҡ keyword �س���*/
UNION
SELECT *
FROM vpm_data.pm_product_master
WHERE partner_code = 'CDS' AND product_name LIKE '%���%' AND subdept_name = 'LINGERIE'

UNION
SELECT *
FROM vpm_data.pm_product_master
WHERE partner_code = 'CDS' AND dept_name LIKE 'WOMEN' AND subdept_name LIKE 'MISSY APPARELS' AND product_name LIKE '%�ش������ͧ%'

/*CDS,RBS �ҡ keyword stretch mark*/
UNION
SELECT *
FROM vpm_data.pm_product_master
WHERE product_name LIKE '%STRETCH MARK%' OR product_name LIKE '%STRETCHMARK%'

/*Palmers oil*/
UNION
SELECT *
FROM vpm_data.pm_product_master
WHERE cleaned_brandname = 'Palmers' AND dept_name = 'SKIN CARE'

UNION
SELECT *
FROM vpm_data.pm_product_master
WHERE product_name LIKE '%����١%'

UNION
SELECT *
FROM vpm_data.pm_product_master
WHERE dept_name LIKE '%KID%' OR subdept_name LIKE '%KID%' OR class_name LIKE '%KID%' OR subclass_name LIKE '%KID%'
OR dept_name LIKE '%BABY%' OR subdept_name LIKE '%BABY%' OR class_name LIKE '%BABY%' OR subclass_name LIKE '%BABY%'
OR dept_name LIKE '%BABIES%' OR subdept_name LIKE '%BABIES%' OR class_name LIKE '%BABIES%' OR subclass_name LIKE '%BABIES%'
OR dept_name LIKE '%INFANT%' OR subdept_name LIKE '%INFANT%' OR class_name LIKE '%INFANT%' OR subclass_name LIKE '%INFANT%'
OR dept_name LIKE '%TODDLER%' OR subdept_name LIKE '%TODDLER%' OR class_name LIKE '%TODDLER%' OR subclass_name LIKE '%TODDLER%'
OR dept_name LIKE '%BOY%' OR subdept_name LIKE '%BOY%' OR class_name LIKE '%BOY%' OR subclass_name LIKE '%BOY%'
OR dept_name LIKE '%GIRL%' OR subdept_name LIKE '%GIRL%' OR class_name LIKE '%GIRL%' OR subclass_name LIKE '%GIRL%'
OR dept_name LIKE '%CHILD%' OR subdept_name LIKE '%CHILD%' OR class_name LIKE '%CHILD%' OR subclass_name LIKE '%CHILD%'
OR dept_name LIKE '%JUNIOR%' OR subdept_name LIKE '%JUNIOR%' OR class_name LIKE '%JUNIOR%' OR subclass_name LIKE '%JUNIOR%'
OR dept_name LIKE '%PREGNAN%' OR subdept_name LIKE '%PREGNAN%' OR class_name LIKE '%PREGNAN%' OR subclass_name LIKE '%PREGNAN%'
OR dept_name LIKE '%MATERNITY%' OR subdept_name LIKE '%MATERNITY%' OR class_name LIKE '%MATERNITY%' OR subclass_name LIKE '%MATERNITY%'
OR dept_name LIKE '%��%' OR subdept_name LIKE '%��%' OR class_name LIKE '%��%' OR subclass_name LIKE '%��%'
OR dept_name LIKE '%���%' OR subdept_name LIKE '%���%' OR class_name LIKE '%���%' OR subclass_name LIKE '%���%'
;

--1.0 create kid product master
DROP TABLE IF EXISTS vpm_data.pm_skutagging_kids_product_master
;
CREATE TABLE vpm_data.pm_skutagging_kids_product_master AS
SELECT A.partner_code
	,A.sku_code
	,A.pad_sku_id
	,UPPER(A.product_name) AS product_name
	,A.dept_code
	,UPPER(A.dept_name) AS dept_name
	,A.subdept_code
	,UPPER(A.SUBDEPT_NAME) AS SUBDEPT_NAME
	,A.CLASS_ID
	,UPPER(A.class_name) AS class_name
	,A.subclass_id
	,UPPER(A.subclass_name) AS subclass_name
	,A.partner_subcategory_id
	,UPPER(A.partner_subcategory_name) AS partner_subcategory_name
	,A.cleaned_brandname
	,1 kid_flag
	,UPPER(C.category_final) AS category_final
	,UPPER(C.type_final) AS type_final
	,C.kid_gender_final
	,C.age_range
	,CAST(NULL AS VARCHAR(100)) AS KIDS_STAGE_FINAL
	,CAST(NULL AS VARCHAR(100)) AS TYPE_TEXT
	,CAST(NULL AS VARCHAR(100)) AS TYPE_SUBCLASS
	,CAST(NULL AS VARCHAR(50)) AS KID_GENDER_KEYWORD
	,CAST(NULL AS VARCHAR(50)) AS KID_GENDER_SUBCLASS
	,CAST(NULL AS VARCHAR(20)) AS DIAPER_SIZE
	,CAST(NULL AS VARCHAR(20)) AS DIAPER_PCS
	,CAST(NULL AS VARCHAR(50)) AS KID_STAGE_SUBCLASS
	,CAST(NULL AS VARCHAR(5)) AS SIZE
	,CAST(NULL AS VARCHAR(5)) AS MEASUREMENT
	,CAST(NULL AS FLOAT) AS REF_DATE_FROM_T0
	,B.revised_category_level_1 AS revised_category_level_1
	,B.revised_category_level_2 AS revised_category_level_2
	,B.revised_category_level_3 AS revised_category_level_3
	,CAST(NULL AS FLOAT8) AS mom_and_kid_product_flag
	,CAST(NULL AS VARCHAR(3)) AS preg_flag
	,CAST(NULL AS VARCHAR(3)) AS unknown_preg_substage
	,CAST(NULL AS VARCHAR(3)) AS infant_flag
	,CAST(NULL AS VARCHAR(3)) AS unknown_infant_substage
	,CAST(NULL AS VARCHAR(3)) AS "STAGE_M-9_to_M-6"
	,CAST(NULL AS VARCHAR(3)) AS "STAGE_M-6_to_M-3"
	,CAST(NULL AS VARCHAR(3)) AS "STAGE_M-3_to_M-0"
	,CAST(NULL AS VARCHAR(3)) AS "STAGE_M+0_to_M+3"
	,CAST(NULL AS VARCHAR(3)) AS "STAGE_M+3_to_M+6"
	,CAST(NULL AS VARCHAR(3)) AS "STAGE_M+6_to_M+9"
	,CAST(NULL AS VARCHAR(3)) AS "STAGE_M+9_to_M+12"
	,CAST(NULL AS VARCHAR(3)) AS toddler_flag
	,CAST(NULL AS VARCHAR(3)) AS preschool_flag
	,CAST(NULL AS VARCHAR(3)) AS junior_flag
	,CAST(NULL AS VARCHAR(3)) AS teen_flag
	,CAST(NULL AS VARCHAR(100)) AS preg_0_3m_keyword
	,CAST(NULL AS VARCHAR(100)) AS preg_4_6m_keyword
	,CAST(NULL AS VARCHAR(100)) AS preg_7_9m_keyword
	,CAST(NULL AS VARCHAR(100)) AS preg_unknown_keyword
	,CAST(NULL AS VARCHAR(100)) AS infant_0_3m_keyword
	,CAST(NULL AS VARCHAR(100)) AS infant_4_6m_keyword
	,CAST(NULL AS VARCHAR(100)) AS infant_7_9m_keyword
	,CAST(NULL AS VARCHAR(100)) AS infant_10_12m_keyword
	,CAST(NULL AS VARCHAR(100)) AS infant_unknown_keyword
	,CAST(NULL AS VARCHAR(100)) AS toddler_keyword
	,CAST(NULL AS VARCHAR(100)) AS preschool_keyword
	,CAST(NULL AS VARCHAR(100)) AS junior_keyword
	,CAST(NULL AS VARCHAR(100)) AS teen_keyword
	,CAST(NULL AS VARCHAR(50)) AS new_revised_category_level_1
	,CAST(NULL AS VARCHAR(50)) AS new_revised_category_level_2
	,CAST(NULL AS VARCHAR(50)) AS new_revised_category_level_3
	,B.category_final AS new_category_final
	,B.type_final AS new_type_final
FROM tmp_mom_and_kid_product A
LEFT JOIN vpm_data.pm_product_master_previous B
ON A.sku_code = B.sku_code
AND A.partner_code = B.partner_code
LEFT JOIN vpm_data.pm_skutagging_kids_product_master_previous C
ON A.sku_code = C.sku_code
AND A.partner_code = C.partner_code
;

--UPDATE NOT MOM AND KID PRODUCT
UPDATE vpm_data.pm_skutagging_kids_product_master
SET MOM_AND_KID_PRODUCT_FLAG = (CASE WHEN (PRODUCT_NAME LIKE '%����%' AND PRODUCT_NAME NOT LIKE '%�������%' AND PRODUCT_NAME NOT LIKE '%������%') THEN 0
									WHEN PRODUCT_NAME LIKE '%COTTON%BUD%' THEN 0
									WHEN PRODUCT_NAME LIKE '%UHT%' AND PRODUCT_NAME NOT LIKE '%ANMUM%' THEN 0
									WHEN TYPE_FINAL = 'WIPE TISSUE' THEN 0
									WHEN PRODUCT_NAME LIKE '%຺����%' THEN 0
									WHEN PRODUCT_NAME LIKE '%຺��%�Ǿ�%' THEN 0
									WHEN PRODUCT_NAME LIKE '%CERTAINTY%' THEN 0
									WHEN PRODUCT_NAME LIKE '%LIFREE%' THEN 0
									WHEN PRODUCT_NAME LIKE 'LFC U%MU%' THEN 0
									WHEN PRODUCT_NAME LIKE '%������๡���ʧ��%' THEN 0
									WHEN PRODUCT_NAME LIKE '%����紷�%���Ҵ%' THEN 0
									WHEN PRODUCT_NAME LIKE '%TRAVEL CASE PLAID ASSORTED%' THEN 0
									WHEN PRODUCT_NAME LIKE 'HUGGIESCLEANCARE%' THEN 0
									WHEN PRODUCT_NAME LIKE '%�����¡%' AND PRODUCT_NAME NOT LIKE '%�����¡�紿ѹ����˧�͡%' THEN 0
									WHEN TYPE_FINAL LIKE 'BABY NAME BOOK' THEN 0
									ELSE 1 END)
;

UPDATE vpm_data.pm_skutagging_kids_product_master
SET MOM_AND_KID_PRODUCT_FLAG = 0
WHERE PRODUCT_NAME LIKE '%BABY%WIPE%'
OR PRODUCT_NAME LIKE '%PUREEN%WIPES%'
OR PRODUCT_NAME LIKE '%WET%WIPE%'
OR PRODUCT_NAME LIKE '%BABY%WIP%'
OR PRODUCT_NAME LIKE '%WET%TISSUE%'
OR PRODUCT_NAME LIKE '%KARISMA%WIPES%'
OR PRODUCT_NAME LIKE '%ORGANIC FRAGRANCEFREE%'
OR PRODUCT_NAME LIKE '%���紷Ӥ������Ҵ���%'
OR PRODUCT_NAME LIKE 'SOFTMATE COMPACT'
OR PRODUCT_NAME LIKE '%�Ԫ�ټ���๡���ʧ��%'
OR PRODUCT_NAME LIKE '%�蹷Ӥ������Ҵ%'
OR PRODUCT_NAME LIKE '%����礷Ӥ������Ҵ%'
OR PRODUCT_NAME LIKE '%�Ԫ�����¡%'
OR PRODUCT_NAME LIKE '%���ա�ҹ%'
OR PRODUCT_NAME LIKE '%������ǹ%'
OR PRODUCT_NAME LIKE '��͹�ͧ�;������һԴ��'
OR PRODUCT_NAME LIKE '%POLYETHYLENE%SHEET%TOTE%'
OR PRODUCT_NAME LIKE '%BEAR%BRAND%STER%'
OR PRODUCT_NAME LIKE '%BEAR BRAND LOW FAT%'
OR PRODUCT_NAME LIKE '%BEAR BRAND WHITE%'
OR PRODUCT_NAME LIKE '%BEAR%BRAND GOLD%'
OR PRODUCT_NAME LIKE 'BEAR BRAND MALTED%'
OR PRODUCT_NAME LIKE '%�������Ŵ�%'
OR PRODUCT_NAME LIKE '%����������Ԫ%'
OR PRODUCT_NAME LIKE '%�չͪ�ش���%'
OR PRODUCT_NAME LIKE '�๤�Ź����ͪ��%'
OR PRODUCT_NAME LIKE '%BEAR%BRAND%GIFT%SET%'
OR PRODUCT_NAME LIKE '%��չ���%'
OR PRODUCT_NAME LIKE '%�ǧ�ح�%'
OR product_name LIKE '��衴���� ICE CREAM%'
OR CLEANED_BRANDNAME IN ('VITAMILK','DUTCH MILL','FOREMOST','LACTASOY','DNA','ANLENE','BLUE DIAMOND','137 DEGREES','V SOY','VIFIT','CARNATION','SUNKIST','PALACE','TEAPOT','HOMESOY','HERSHEY''S','HERSHEY S','MALEE'
				,'4 CARE','OVALTINE','MALI','TOFUSAN','DAIMOND FRESH','NONGPHO','BSC','FALCON','SESAMILK','SCOTCH','MAGNOLIA','DE NOSH','DMALT','SIRICHAI','NESTLE','THAI DENMARK','MILO','BENECOL','UFC','SOYFRESH'
				,'CHITRALADA','DOIKHAM','MAGIC FARM','NATURE CHARM','TIPCO','BIRDWINGS','BENEFITT','GOLD MILK','PRO-FIT','SOY POP','MMILK','SEGAFREDO','TIPCO BEAT','TOPS','ORCHID','CHOKCHAI','YOMOST','SIPSO','THAI FARM MILK'
				,'SHIP','SANG SANG','TASTIFIT','ENSURE','GLUCENA SR','MARUSUN','NATURIE','ACAI','CALPIS','D PLUS','CAMPUS','GOODSERVE','KORACH MILK','LABAN SOY','MINUTE MAID','OHAYO','COUNTRY FRESH','CALCIMEX'
				,'MMILK','NONE')
;

UPDATE vpm_data.pm_skutagging_kids_product_master
SET MOM_AND_KID_PRODUCT_FLAG = 0
WHERE dept_name LIKE 'BOOK'
AND subdept_name NOT IN ('�Ե������������','��������','����������������Ѻ��������Ǫ�','����ٹ��/����ٹ�������','˹ѧ����� / �Էҹ�Ҿ����Ѻ��')
AND subclass_name IS NULL
;

UPDATE vpm_data.pm_skutagging_kids_product_master
SET MOM_AND_KID_PRODUCT_FLAG = 0
WHERE subdept_name IN ('ADULT CARE','VISUAL HI-FI','LITERATURE')
;

UPDATE vpm_data.pm_skutagging_kids_product_master
SET MOM_AND_KID_PRODUCT_FLAG = 0
WHERE class_name LIKE 'GADGET' AND subclass_name IN ('MOBILE ACCESSORIES','OTHERS')
;

UPDATE vpm_data.pm_skutagging_kids_product_master
SET MOM_AND_KID_PRODUCT_FLAG = 0
WHERE class_name LIKE 'ADULT CARE'
;

UPDATE vpm_data.pm_skutagging_kids_product_master
SET MOM_AND_KID_PRODUCT_FLAG = 0
WHERE subclass_name IN ('BOOKS & PRINTED MEDIA/NON FMCG','PHOTO ALBUM','THAI FAMILY / CHILDREN','FAMILY / CHILDREN','CARTOON COMICS','BOOK SET CARTOON'
						,'IT ACCESSORIES','MOBILE FAN','LIGHTING','PHOTO FRAME','HOME DECOR','SPEAKING AND CONVERSATION','BUSINESS AND OFFICE ENGLI'
						,'READING','KITCHEN WARES','CHILDREN''S CEREAL','CAN STERILIZED MILK')
;

UPDATE vpm_data.pm_skutagging_kids_product_master
SET MOM_AND_KID_PRODUCT_FLAG = 0
WHERE revised_category_level_2 IN ('AIR PURIFIER','AIR REFRESHMENT','BATHING','SWEET','PET')
;

UPDATE vpm_data.pm_skutagging_kids_product_master
SET MOM_AND_KID_PRODUCT_FLAG = 0
WHERE revised_category_level_3 IN ('ART & CRAFT','CANDY/GUM','OFFICE ELECTRONICS ACCESSORIES','ADULT DIAPERS')
;

UPDATE vpm_data.pm_skutagging_kids_product_master
SET MOM_AND_KID_PRODUCT_FLAG = 0
WHERE type_final = 'CONDENSED MILK'
;



-----MATERNITY
--UPDATE KEYWORD IF PREG_FLAG = 'YES'
UPDATE vpm_data.pm_skutagging_kids_product_master
SET PREG_FLAG = 'YES'
	,UNKNOWN_INFANT_SUBSTAGE = 'N/A'
	,"STAGE_M-6_TO_M-3" = 'YES'
	,PREG_4_6M_KEYWORD = (CASE WHEN PRODUCT_NAME LIKE '%��͹��Фͧ��ͧ%' THEN '��͹�ͧ�����'
								WHEN PRODUCT_NAME LIKE '%��͹�ʹ�س���%' THEN '��͹�ͧ�����'
								WHEN PRODUCT_NAME LIKE '%��͹��ا�����%' THEN '��͹�ͧ�����'
								WHEN CLEANED_BRANDNAME LIKE 'THREEANGELS MATERNITY' AND PRODUCT_NAME NOT LIKE '%������%' AND PRODUCT_NAME NOT LIKE '%��ЪѺ˹�ҷ�ͧ%' THEN '�ش���㹤س���'
								WHEN CLEANED_BRANDNAME LIKE 'WACOAL' AND PRODUCT_NAME LIKE '%��駤����%' THEN '�ش���㹤س����駤����'
								WHEN CLEANED_BRANDNAME LIKE 'WACOAL' AND PRODUCT_NAME LIKE '%�س���%' AND PRODUCT_NAME NOT LIKE '%��ѧ��ʹ%' THEN '�ش���㹤س���'
								WHEN CLEANED_BRANDNAME IN ('NITAN','SWEET PEA','MOTHERLYLOVE','NAPPI') THEN '�ش������ͧ'
								ELSE PREG_4_6M_KEYWORD END)
WHERE MOM_AND_KID_PRODUCT_FLAG = 1
AND ((PRODUCT_NAME LIKE '%��͹��Фͧ��ͧ%')
	OR (PRODUCT_NAME LIKE '%��͹�ʹ�س���%')
	OR (PRODUCT_NAME LIKE '%��͹��ا�����%')
	OR (CLEANED_BRANDNAME LIKE 'THREEANGELS MATERNITY' AND PRODUCT_NAME NOT LIKE '%������%' AND PRODUCT_NAME NOT LIKE '%��ЪѺ˹�ҷ�ͧ%')
	OR (CLEANED_BRANDNAME LIKE 'WACOAL' AND PRODUCT_NAME LIKE '%��駤����%')
	OR (CLEANED_BRANDNAME LIKE 'WACOAL' AND PRODUCT_NAME LIKE '%�س���%' AND PRODUCT_NAME NOT LIKE '%��ѧ��ʹ%')
	OR (CLEANED_BRANDNAME IN ('NITAN','SWEET PEA','MOTHERLYLOVE','NAPPI')))
;

UPDATE vpm_data.pm_skutagging_kids_product_master
SET PREG_FLAG = 'YES'
	,UNKNOWN_INFANT_SUBSTAGE = 'N/A'
	,"STAGE_M-3_TO_M-0" = 'YES'
	,PREG_7_9M_KEYWORD = (CASE WHEN PRODUCT_NAME LIKE '%��͹��Фͧ��ͧ%' THEN '��͹�ͧ�����'
								WHEN PRODUCT_NAME LIKE '%��͹�ʹ�س���%' THEN '��͹�ͧ�����'
								WHEN PRODUCT_NAME LIKE '%��͹��ا�����%' THEN '��͹�ͧ�����'
								WHEN PRODUCT_NAME LIKE '%����Ѵ%��ا��ͧ%' THEN '����Ѵ��ا�����'
								WHEN PRODUCT_NAME LIKE '%����Ѵ��ا�����%' THEN '����Ѵ��ا�����'
								WHEN PRODUCT_NAME LIKE '%BELLY%LIFT%SUPPORT%BELT%' THEN '����Ѵ��ا�����'
								WHEN CLEANED_BRANDNAME LIKE 'THREEANGELS MATERNITY' AND PRODUCT_NAME NOT LIKE '%������%' AND PRODUCT_NAME NOT LIKE '%��ЪѺ˹�ҷ�ͧ%' THEN '�ش���㹤س���'
								WHEN CLEANED_BRANDNAME LIKE 'WACOAL' AND PRODUCT_NAME LIKE '%��駤����%' THEN '�ش���㹤س����駤����'
								WHEN CLEANED_BRANDNAME LIKE 'WACOAL' AND PRODUCT_NAME LIKE '%�س���%' AND PRODUCT_NAME NOT LIKE '%��ѧ��ʹ%' THEN '�ش���㹤س���'
								WHEN CLEANED_BRANDNAME IN ('NITAN','SWEET PEA','MOTHERLYLOVE','NAPPI') THEN '�ش������ͧ'
								ELSE PREG_7_9M_KEYWORD END)
WHERE MOM_AND_KID_PRODUCT_FLAG = 1
AND ((PRODUCT_NAME LIKE '%��͹��Фͧ��ͧ%')
	OR (PRODUCT_NAME LIKE '%��͹�ʹ�س���%')
	OR (PRODUCT_NAME LIKE '%��͹��ا�����%')
	OR (PRODUCT_NAME LIKE '%����Ѵ%��ا��ͧ%')
	OR (PRODUCT_NAME LIKE '%����Ѵ��ا�����%')
	OR (PRODUCT_NAME LIKE '%BELLY%LIFT%SUPPORT%BELT%')
	OR (CLEANED_BRANDNAME LIKE 'THREEANGELS MATERNITY' AND PRODUCT_NAME NOT LIKE '%������%' AND PRODUCT_NAME NOT LIKE '%��ЪѺ˹�ҷ�ͧ%')
	OR (CLEANED_BRANDNAME LIKE 'WACOAL' AND PRODUCT_NAME LIKE '%��駤����%')
	OR (CLEANED_BRANDNAME LIKE 'WACOAL' AND PRODUCT_NAME LIKE '%�س���%' AND PRODUCT_NAME NOT LIKE '%��ѧ��ʹ%')
	OR (CLEANED_BRANDNAME IN ('NITAN','SWEET PEA','MOTHERLYLOVE','NAPPI')))
;

--BOTH PREG AND INFANT YES
UPDATE vpm_data.pm_skutagging_kids_product_master
SET PREG_FLAG = 'YES'
	,"STAGE_M+3_TO_M+6" = (CASE WHEN CLEANED_BRANDNAME IN ('NITAN','SWEET PEA','MOTHERLYLOVE','NAPPI') THEN 'YES'
								WHEN CLEANED_BRANDNAME LIKE 'THREEANGELS MATERNITY' AND PRODUCT_NAME NOT LIKE '%������%' AND PRODUCT_NAME NOT LIKE '%��ЪѺ˹�ҷ�ͧ%' THEN 'YES'
								WHEN CLEANED_BRANDNAME LIKE 'WACOAL' AND PRODUCT_NAME LIKE '%�س���%' AND PRODUCT_NAME NOT LIKE '%��ѧ��ʹ%' THEN 'YES'
								ELSE "STAGE_M+3_TO_M+6" END)
	,"STAGE_M+6_TO_M+9" = (CASE WHEN CLEANED_BRANDNAME IN ('NITAN','SWEET PEA','MOTHERLYLOVE','NAPPI') THEN 'YES'
								WHEN CLEANED_BRANDNAME LIKE 'THREEANGELS MATERNITY' AND PRODUCT_NAME NOT LIKE '%������%' AND PRODUCT_NAME NOT LIKE '%��ЪѺ˹�ҷ�ͧ%' THEN 'YES'
								WHEN CLEANED_BRANDNAME LIKE 'WACOAL' AND PRODUCT_NAME LIKE '%�س���%' AND PRODUCT_NAME NOT LIKE '%��ѧ��ʹ%' THEN 'YES'
								ELSE "STAGE_M+6_TO_M+9" END)
	,UNKNOWN_PREG_SUBSTAGE = (CASE WHEN CLEANED_BRANDNAME IN ('ANMUM','ENFAMAMA') THEN 'YES'
								WHEN CLEANED_BRANDNAME LIKE 'S-26' AND product_name LIKE '%MOM MILK%' THEN 'YES'
								WHEN CLEANED_BRANDNAME IN ('CLARINS','COCORO HANAKO','PALMERS','LAMOON') THEN 'YES'
								ELSE UNKNOWN_PREG_SUBSTAGE END)
	,PREG_4_6M_KEYWORD = (CASE WHEN CLEANED_BRANDNAME IN ('NITAN','SWEET PEA','MOTHERLYLOVE','NAPPI') THEN '�ش������ͧ/���������'
								WHEN CLEANED_BRANDNAME LIKE 'THREEANGELS MATERNITY' AND PRODUCT_NAME NOT LIKE '%������%' AND PRODUCT_NAME NOT LIKE '%��ЪѺ˹�ҷ�ͧ%' THEN '�ش���㹤س���'
								WHEN CLEANED_BRANDNAME LIKE 'WACOAL' AND PRODUCT_NAME LIKE '%�س���%' AND PRODUCT_NAME NOT LIKE '%��ѧ��ʹ%' THEN '�ش���㹤س���'
								ELSE PREG_4_6M_KEYWORD END)
	,PREG_7_9M_KEYWORD = (CASE WHEN CLEANED_BRANDNAME IN ('NITAN','SWEET PEA','MOTHERLYLOVE','NAPPI') THEN '�ش������ͧ/���������'
								WHEN CLEANED_BRANDNAME LIKE 'THREEANGELS MATERNITY' AND PRODUCT_NAME NOT LIKE '%������%' AND PRODUCT_NAME NOT LIKE '%��ЪѺ˹�ҷ�ͧ%' THEN '�ش���㹤س���'
								WHEN CLEANED_BRANDNAME LIKE 'WACOAL' AND PRODUCT_NAME LIKE '%�س���%' AND PRODUCT_NAME NOT LIKE '%��ѧ��ʹ%' THEN '�ش���㹤س���'
								ELSE PREG_7_9M_KEYWORD END)
	,PREG_UNKNOWN_KEYWORD = (CASE WHEN CLEANED_BRANDNAME IN ('ANMUM','ENFAMAMA') THEN 'MATERNAL MILK'
								WHEN CLEANED_BRANDNAME LIKE 'S-26' AND product_name LIKE '%MOM MILK%' THEN 'MATERNAL MILK'
								WHEN CLEANED_BRANDNAME IN ('CLARINS','COCORO HANAKO','PALMERS','LAMOON') THEN 'STRETCH MARK CREAM'
								ELSE PREG_UNKNOWN_KEYWORD END)
	,INFANT_FLAG = 'YES'
	,UNKNOWN_INFANT_SUBSTAGE = 'YES'
	,INFANT_UNKNOWN_KEYWORD = (CASE WHEN CLEANED_BRANDNAME IN ('NITAN','SWEET PEA','MOTHERLYLOVE','NAPPI') THEN '�ش������ͧ/���������'
									WHEN CLEANED_BRANDNAME LIKE 'THREEANGELS MATERNITY' AND PRODUCT_NAME NOT LIKE '%������%' AND PRODUCT_NAME NOT LIKE '%��ЪѺ˹�ҷ�ͧ%' THEN '�ش���㹤س���'
									WHEN CLEANED_BRANDNAME LIKE 'WACOAL' AND PRODUCT_NAME LIKE '%�س���%' THEN '�ش���㹤س���'
									WHEN CLEANED_BRANDNAME IN ('ANMUM','S-26','ENFAMAMA') THEN 'MATERNAL MILK'
									WHEN CLEANED_BRANDNAME IN ('CLARINS','COCORO HANAKO','PALMERS','LAMOON') THEN 'STRETCH MARK CREAM'
									ELSE INFANT_UNKNOWN_KEYWORD END)
WHERE MOM_AND_KID_PRODUCT_FLAG = 1
AND ((CLEANED_BRANDNAME IN ('NITAN','SWEET PEA','MOTHERLYLOVE','NAPPI','CLARINS','COCORO HANAKO','PALMERS','LAMOON','ANMUM','S-26','ENFAMAMA','THREEANGELS MATERNITY'))
	OR (CLEANED_BRANDNAME LIKE 'WACOAL' AND PRODUCT_NAME LIKE '%���%'))
;

--ONLY INFANT YES (UNKNOWN SUB-STAGE)
UPDATE vpm_data.pm_skutagging_kids_product_master
SET PREG_FLAG = 'N/A'
	,UNKNOWN_PREG_SUBSTAGE = 'N/A'
	,INFANT_FLAG = 'YES'
	,UNKNOWN_INFANT_SUBSTAGE = 'YES'
	,INFANT_UNKNOWN_KEYWORD = (CASE WHEN PRODUCT_NAME LIKE '%POSTPARTUM BELLY WRAP%' THEN '����Ѵ˹�ҷ�ͧ��ѧ��ʹ'
									WHEN PRODUCT_NAME LIKE '%����Ѵ˹�ҷ�ͧ%' THEN '����Ѵ˹�ҷ�ͧ��ѧ��ʹ'
									WHEN PRODUCT_NAME LIKE '%BELLV FITT BAMBOO%' THEN '����Ѵ˹�ҷ�ͧ��ѧ��ʹ'
									WHEN PRODUCT_NAME LIKE '%BELLY BUSTER%' THEN '����Ѵ˹�ҷ�ͧ��ѧ��ʹ'
									WHEN PRODUCT_NAME LIKE '%TUMMY WRAP AROUND%' THEN '����Ѵ˹�ҷ�ͧ��ѧ��ʹ'
									WHEN PRODUCT_NAME LIKE '%TUMMY TRAINER%' THEN '����Ѵ˹�ҷ�ͧ��ѧ��ʹ'
									WHEN PRODUCT_NAME LIKE '%MATERNITY SUPPORT BELT%' THEN '����Ѵ˹�ҷ�ͧ��ѧ��ʹ'
									WHEN PRODUCT_NAME LIKE '%����Ѵ��ѧ��ʹ%' THEN '����Ѵ˹�ҷ�ͧ��ѧ��ʹ'
									WHEN PRODUCT_NAME LIKE '%SHAPEFIXER%PANTY%' THEN '���㹡�ЪѺ�ٻ��ҧ��ѧ��ʹ'
									WHEN PRODUCT_NAME LIKE '%CASER%CARE%PANTY%' THEN '���㹡�ЪѺ�ٻ��ҧ��ѧ��ʹ'
									WHEN PRODUCT_NAME LIKE '%CESAR%CARE%PANTY%' THEN '���㹡�ЪѺ�ٻ��ҧ��ѧ��ʹ'
									WHEN PRODUCT_NAME LIKE '%����Ѵ��ЪѺ������%' THEN '����Ѵ��ЪѺ������'
									WHEN PRODUCT_NAME LIKE '�ҧࡧRECOVERY%' THEN '�ҧࡧ RECOVERY'
									WHEN PRODUCT_NAME LIKE 'HIP FIH BLACK' THEN '���㹡�ЪѺ�ٻ��ҧ��ѧ��ʹ'
									WHEN PRODUCT_NAME LIKE '%HEAT%PACK%' THEN 'HEAT PACK'
									WHEN PRODUCT_NAME LIKE '%BREAST THERAPY%' THEN '����Ф�˹��͡'
									WHEN PRODUCT_NAME LIKE '%��������ǹ�%' THEN '��������ǹ�'
									WHEN PRODUCT_NAME LIKE '%PURELAN 100%' THEN '��������ǹ�'
									WHEN PRODUCT_NAME LIKE '%��������ͧ��ǵ�駤����%' THEN '�����س���'
									WHEN PRODUCT_NAME LIKE 'MOMTHLY STICKER%' THEN 'BABY MONTH STICKER'
									WHEN PRODUCT_NAME LIKE '%�ش�͹%����%' THEN '���������'
									WHEN PRODUCT_NAME LIKE '%�����ٷ%����%' THEN '���������'
									WHEN PRODUCT_NAME LIKE '%�����%����%' THEN '���������'
									WHEN PRODUCT_NAME LIKE '%�ش����%' THEN '���������'
									WHEN PRODUCT_NAME LIKE '%�������%' THEN '���������'
									WHEN PRODUCT_NAME LIKE '%NIPPLE%FORMERS%' THEN 'NIPPLE FORMER'
									WHEN PRODUCT_NAME LIKE '%MILK%COLLECTION%SHELLS%' THEN 'MILK COLLECTION SHELL'
									WHEN PRODUCT_NAME LIKE '%��һ�����%' THEN '�ش���㹻�����'
									WHEN PRODUCT_NAME LIKE '%������%������%' THEN '�ش���㹻�����'
									WHEN PRODUCT_NAME LIKE '%�ش����%������%' THEN '�ش���㹻�����'
									WHEN PRODUCT_NAME LIKE '%���%������%' THEN '�ش���㹻�����'
									WHEN PRODUCT_NAME LIKE '%BUSTIER BRA%' THEN '�ش���㹻�����'
									WHEN PRODUCT_NAME LIKE '%BRA%������%' THEN '�ش���㹻�����'
									WHEN PRODUCT_NAME LIKE '%����ͪ���%������%' THEN '�ش���㹻�����'
									WHEN PRODUCT_NAME LIKE '%NURSE PLW%' THEN '��͹����'
									WHEN PRODUCT_NAME LIKE '%NURSING PILLOW%' THEN '��͹����'
									WHEN PRODUCT_NAME LIKE '%��͹%��%' THEN '��͹����'
									WHEN PRODUCT_NAME LIKE '%NURSING PONCHO%' THEN '��Ҥ�������'
									WHEN PRODUCT_NAME LIKE '%NURSING CAPE%' THEN '��Ҥ�������'
									WHEN PRODUCT_NAME LIKE '%BREASTFEEDING%BIB%' THEN '��Ҥ�������'
									WHEN PRODUCT_NAME LIKE '%COVER%BREASTFEEDING%' THEN '��Ҥ�������'
									WHEN PRODUCT_NAME LIKE '%BREASTFEEDING COVER%' THEN '��Ҥ�������'
									WHEN PRODUCT_NAME LIKE '%����ͤ���%' THEN '��Ҥ�������'
									WHEN PRODUCT_NAME LIKE '%��Ҥ���%' THEN '��Ҥ�������'
									WHEN PRODUCT_NAME LIKE '%COLLAR COVER%' THEN '��Ҥ�������'
									WHEN PRODUCT_NAME LIKE '%NURSING%COVER%' THEN '��Ҥ�������'
									WHEN PRODUCT_NAME LIKE 'LAMOON%BABY' THEN '��Ҥ�������'
									WHEN PRODUCT_NAME LIKE '%��ҾҴ���%' THEN '��ҾҴ���'
									WHEN PRODUCT_NAME LIKE '%���������س�����%' THEN '���������س�����'
									WHEN PRODUCT_NAME LIKE '%�������纤������%' THEN '���������س�����'
									WHEN PRODUCT_NAME LIKE '%BOTTLE BAG%' THEN '���������س�����'
									WHEN PRODUCT_NAME LIKE '%BOTTLE COOLER%' THEN '���������س�����'
									WHEN PRODUCT_NAME LIKE '%BOTTLE TOTE%' THEN '���������س�����'
									WHEN PRODUCT_NAME LIKE '���������秾��� 11L' THEN '���������س�����'
									WHEN PRODUCT_NAME LIKE '%������%�������%' THEN '�����Ҽ������'
									WHEN PRODUCT_NAME LIKE '%������%���%' THEN '�����Ҥس���'
									WHEN PRODUCT_NAME LIKE '%�����������%' THEN '�����Ҥس���'
									WHEN PRODUCT_NAME LIKE '%�����ҨѴ����º%' THEN '�����Ҥس���'
									WHEN PRODUCT_NAME LIKE '%SET MATERNITY%' THEN '����絤س���'
									WHEN PRODUCT_NAME LIKE '%��������%' THEN '����絤س���'
									WHEN PRODUCT_NAME LIKE '%MOSES BASKET%' THEN 'MOSES BASKET'
									WHEN PRODUCT_NAME LIKE '%�������%' THEN 'BABY CARRIER'
									WHEN PRODUCT_NAME LIKE '%�Ե��Թ��������ҧ��ӹ�%' THEN '�Ե��Թ��������ҧ��ӹ�'
									WHEN PRODUCT_NAME LIKE '%�Ե��Թ��ЪѺ�Ѵ��ǹ�س���%' THEN '�Ե��Թ��ЪѺ�Ѵ��ǹ�س���'
									WHEN CLEANED_BRANDNAME IN ('COLORLAND','CIPU','CMP GROUP','MAMAS&PAPAS','OKIEDOG','PICCOLO','SKIPHOP') THEN '�����Ҥس���'
									WHEN CLEANED_BRANDNAME IN ('QUEEN COWS') THEN '���������'
									WHEN CLEANED_BRANDNAME IN ('DOUBLE MONKEYS') THEN '��Ҥ�������'
									WHEN CLEANED_BRANDNAME IN ('THREEANGELS MATERNITY','ATTITUDE MOM') THEN '�ش���㹤س���'
									ELSE INFANT_UNKNOWN_KEYWORD END)
WHERE MOM_AND_KID_PRODUCT_FLAG = 1
AND CATEGORY_FINAL = 'NECESSITIES'
AND TYPE_FINAL LIKE '%MATERNITY%'
AND (PRODUCT_NAME LIKE '%POSTPARTUM BELLY WRAP%'
		OR PRODUCT_NAME LIKE '%����Ѵ˹�ҷ�ͧ%'
		OR PRODUCT_NAME LIKE '%BELLV FITT BAMBOO%'
		OR PRODUCT_NAME LIKE '%BELLY BUSTER%'
		OR PRODUCT_NAME LIKE '%TUMMY WRAP AROUND%'
		OR PRODUCT_NAME LIKE '%TUMMY TRAINER%'
		OR PRODUCT_NAME LIKE '%MATERNITY SUPPORT BELT%'
		OR PRODUCT_NAME LIKE '%����Ѵ��ѧ��ʹ%'
		OR PRODUCT_NAME LIKE '%SHAPEFIXER%PANTY%'
		OR PRODUCT_NAME LIKE '%CASER%CARE%PANTY%'
		OR PRODUCT_NAME LIKE '%CESAR%CARE%PANTY%'
		OR PRODUCT_NAME LIKE '%����Ѵ��ЪѺ������%'
		OR PRODUCT_NAME LIKE '�ҧࡧRECOVERY%'
		OR PRODUCT_NAME LIKE 'HIP FIH BLACK'
		OR PRODUCT_NAME LIKE '%HEAT%PACK%'
		OR PRODUCT_NAME LIKE '%BREAST THERAPY%'
		OR PRODUCT_NAME LIKE '%��������ǹ�%'
		OR PRODUCT_NAME LIKE '%PURELAN 100%'
		OR PRODUCT_NAME LIKE '%��������ͧ��ǵ�駤����%'
		OR PRODUCT_NAME LIKE 'MOMTHLY STICKER%'
		OR PRODUCT_NAME LIKE '%�ش�͹%����%'
		OR PRODUCT_NAME LIKE '%�����ٷ%����%'
		OR PRODUCT_NAME LIKE '%�����%����%'
		OR PRODUCT_NAME LIKE '%�ش����%'
		OR PRODUCT_NAME LIKE '%�������%'
		OR PRODUCT_NAME LIKE '%NIPPLE%FORMERS%'
		OR PRODUCT_NAME LIKE '%MILK%COLLECTION%SHELLS%'
		OR PRODUCT_NAME LIKE '%��һ�����%'
		OR PRODUCT_NAME LIKE '%������%������%'
		OR PRODUCT_NAME LIKE '%�ش����%������%'
		OR PRODUCT_NAME LIKE '%���%������%'
		OR PRODUCT_NAME LIKE '%BUSTIER BRA%'
		OR PRODUCT_NAME LIKE '%BRA%������%'
		OR PRODUCT_NAME LIKE '%����ͪ���%������%'
		OR PRODUCT_NAME LIKE '%NURSE PLW%'
		OR PRODUCT_NAME LIKE '%NURSING PILLOW%'
		OR PRODUCT_NAME LIKE '%��͹%��%'
		OR PRODUCT_NAME LIKE '%NURSING PONCHO%'
		OR PRODUCT_NAME LIKE '%NURSING CAPE%'
		OR PRODUCT_NAME LIKE '%BREASTFEEDING%BIB%'
		OR PRODUCT_NAME LIKE '%COVER%BREASTFEEDING%'
		OR PRODUCT_NAME LIKE '%BREASTFEEDING COVER%'
		OR PRODUCT_NAME LIKE '%����ͤ���%'
		OR PRODUCT_NAME LIKE '%��Ҥ���%'
		OR PRODUCT_NAME LIKE '%COLLAR COVER%'
		OR PRODUCT_NAME LIKE '%NURSING%COVER%'
		OR PRODUCT_NAME LIKE 'LAMOON%BABY'
		OR PRODUCT_NAME LIKE '%��ҾҴ���%'
		OR PRODUCT_NAME LIKE '%���������س�����%'
		OR PRODUCT_NAME LIKE '%�������纤������%'
		OR PRODUCT_NAME LIKE '%BOTTLE BAG%'
		OR PRODUCT_NAME LIKE '%BOTTLE COOLER%'
		OR PRODUCT_NAME LIKE '%BOTTLE TOTE%'
		OR PRODUCT_NAME LIKE '���������秾��� 11L'
		OR PRODUCT_NAME LIKE '%������%�������%'
		OR PRODUCT_NAME LIKE '%������%���%'
		OR PRODUCT_NAME LIKE '%�����������%'
		OR PRODUCT_NAME LIKE '%�����ҨѴ����º%'
		OR PRODUCT_NAME LIKE '%SET MATERNITY%'
		OR PRODUCT_NAME LIKE '%��������%'
		OR PRODUCT_NAME LIKE '%MOSES BASKET%'
		OR PRODUCT_NAME LIKE '%�������%'
		OR PRODUCT_NAME LIKE '%�Ե��Թ��������ҧ��ӹ�%'
		OR PRODUCT_NAME LIKE '%�Ե��Թ��ЪѺ�Ѵ��ǹ�س���%'
		OR CLEANED_BRANDNAME IN ('COLORLAND','CIPU','CMP GROUP','MAMAS&PAPAS','OKIEDOG','PICCOLO','SKIPHOP','QUEEN COWS','DOUBLE MONKEYS','THREEANGELS MATERNITY','ATTITUDE MOM'))
;

-----NEW PREG TAGGING
--PREG 0-6 NOT INFANT
UPDATE vpm_data.pm_skutagging_kids_product_master
SET "STAGE_M-9_TO_M-6" = 'YES'
	,"STAGE_M-6_TO_M-3" = 'YES'
	,"STAGE_M-3_TO_M-0" = 'N/A'
	,PREG_FLAG = 'YES'
	,UNKNOWN_PREG_SUBSTAGE = 'N/A'
	,INFANT_FLAG = 'N/A'
	,UNKNOWN_INFANT_SUBSTAGE = NULL
	,PREG_0_3M_KEYWORD = 'PREGNANCY GUIDE'
	,PREG_4_6M_KEYWORD = 'PREGNANCY GUIDE'
WHERE MOM_AND_KID_PRODUCT_FLAG = 1
AND COALESCE(PREG_FLAG,'N/A') = 'N/A'
AND COALESCE(INFANT_FLAG,'N/A') = 'N/A'
AND COALESCE(TODDLER_FLAG,'N/A') = 'N/A'
AND COALESCE(PRESCHOOL_FLAG,'N/A') = 'N/A'
AND COALESCE(JUNIOR_FLAG,'N/A') = 'N/A'
AND COALESCE(TEEN_FLAG,'N/A') = 'N/A'
AND (PRODUCT_NAME LIKE '%���ٺ��ا�����%'
	OR PRODUCT_NAME LIKE '%����§�١����Ҵ�����㹤��%'
	OR PRODUCT_NAME LIKE '%�������ǡ�͹��ʹ%'
	OR PRODUCT_NAME LIKE '%��ʹẺ�˹��%')
;

--PREG 4-9 NOT INFANT
UPDATE vpm_data.pm_skutagging_kids_product_master
SET "STAGE_M-9_TO_M-6" = 'N/A'
	,"STAGE_M-6_TO_M-3" = 'YES'
	,"STAGE_M-3_TO_M-0" = 'YES'
	,PREG_FLAG = 'YES'
	,UNKNOWN_PREG_SUBSTAGE = 'N/A'
	,INFANT_FLAG = 'N/A'
	,UNKNOWN_INFANT_SUBSTAGE = NULL
	,PREG_4_6M_KEYWORD = (CASE WHEN PRODUCT_NAME LIKE '%��͹��Фͧ��ͧ%' THEN '��͹�ͧ�����'
							WHEN PRODUCT_NAME LIKE '%��͹�ʹ�س���%' THEN '��͹�ͧ�����'
							WHEN PRODUCT_NAME LIKE '%��͹��ا�����%' THEN '��͹�ͧ�����'
							WHEN PRODUCT_NAME LIKE '%�ش������ͧ%' THEN '�ش������ͧ'
							WHEN CLEANED_BRANDNAME LIKE 'THREEANGELS MATERNITY' AND PRODUCT_NAME NOT LIKE '%������%' AND PRODUCT_NAME NOT LIKE '%��ЪѺ˹�ҷ�ͧ%' THEN '�ش������ͧ'
							WHEN CLEANED_BRANDNAME LIKE 'WACOAL' AND PRODUCT_NAME LIKE '%��駤����%' THEN '�ش���㹤س����駤����'
							ELSE PREG_4_6M_KEYWORD END)
	,PREG_7_9M_KEYWORD = (CASE WHEN PRODUCT_NAME LIKE '%��͹��Фͧ��ͧ%' THEN '��͹�ͧ�����'
							WHEN PRODUCT_NAME LIKE '%��͹�ʹ�س���%' THEN '��͹�ͧ�����'
							WHEN PRODUCT_NAME LIKE '%��͹��ا�����%' THEN '��͹�ͧ�����'
							WHEN PRODUCT_NAME LIKE '%�ش������ͧ%' THEN '�ش������ͧ'
							WHEN CLEANED_BRANDNAME LIKE 'THREEANGELS MATERNITY' AND PRODUCT_NAME NOT LIKE '%������%' AND PRODUCT_NAME NOT LIKE '%��ЪѺ˹�ҷ�ͧ%' THEN '�ش������ͧ'
							WHEN CLEANED_BRANDNAME LIKE 'WACOAL' AND PRODUCT_NAME LIKE '%��駤����%' THEN '�ش���㹤س����駤����'
							ELSE PREG_7_9M_KEYWORD END)
WHERE MOM_AND_KID_PRODUCT_FLAG = 1
AND COALESCE(PREG_FLAG,'N/A') = 'N/A'
AND COALESCE(INFANT_FLAG,'N/A') = 'N/A'
AND COALESCE(TODDLER_FLAG,'N/A') = 'N/A'
AND COALESCE(PRESCHOOL_FLAG,'N/A') = 'N/A'
AND COALESCE(JUNIOR_FLAG,'N/A') = 'N/A'
AND COALESCE(TEEN_FLAG,'N/A') = 'N/A'
AND (PRODUCT_NAME LIKE '%��͹��Фͧ��ͧ%'
	OR PRODUCT_NAME LIKE '%��͹�ʹ�س���%'
	OR PRODUCT_NAME LIKE '%��͹��ا�����%'
	OR PRODUCT_NAME LIKE '%�ش������ͧ%'
	OR (CLEANED_BRANDNAME LIKE 'THREEANGELS MATERNITY' AND PRODUCT_NAME NOT LIKE '%������%' AND PRODUCT_NAME NOT LIKE '%��ЪѺ˹�ҷ�ͧ%')
	OR (CLEANED_BRANDNAME LIKE 'WACOAL' AND PRODUCT_NAME LIKE '%��駤����%'))
;

--PREG 4-9 AND INFANT
UPDATE vpm_data.pm_skutagging_kids_product_master
SET "STAGE_M-9_TO_M-6" = 'N/A'
	,"STAGE_M-6_TO_M-3" = 'YES'
	,"STAGE_M-3_TO_M-0" = 'YES'
	,PREG_FLAG = 'YES'
	,UNKNOWN_PREG_SUBSTAGE = 'N/A'
	,INFANT_FLAG = 'YES'
	,UNKNOWN_INFANT_SUBSTAGE = 'YES'
	,PREG_4_6M_KEYWORD = (CASE WHEN CLEANED_BRANDNAME IN ('NITAN','SWEET PEA','MOTHERLYLOVE','NAPPI','LA FEMME') THEN '�ش������ͧ/���������'
							WHEN CLEANED_BRANDNAME LIKE 'WACOAL' AND PRODUCT_NAME LIKE '%�س���%' AND PRODUCT_NAME NOT LIKE '%��ѧ��ʹ%' THEN '�ش���㹤س���'
							WHEN (PRODUCT_NAME LIKE '%����%���%' AND PRODUCT_NAME NOT LIKE '%��ѧ��ʹ%' AND PRODUCT_NAME NOT LIKE '%��駤��%') THEN '�ش���㹤س���'
							WHEN PRODUCT_NAME LIKE '%�س���%' AND (DEPT_NAME = 'WOMEN' OR SUBDEPT_NAME = 'LINGERIE') AND PRODUCT_NAME NOT LIKE '%������%' THEN '�ش���㹤س���'
							WHEN PRODUCT_NAME LIKE '%��͹+��ѧ%' THEN '��͹�����ѧ��ʹ'
							WHEN PRODUCT_NAME LIKE '%��͹�����ѧ��ʹ%' THEN '��͹�����ѧ��ʹ'
							WHEN PRODUCT_NAME LIKE '%�ҧࡧ���㹡�ЪѺ˹�ҷ�ͧ%' THEN '�ҧࡧ���㹡�ЪѺ˹�ҷ�ͧ'
							WHEN CLASS_NAME = 'MATERNITY INNERWEARS&PAJA' THEN '�ش������ͧ/���������'
							ELSE PREG_4_6M_KEYWORD END)
	,PREG_7_9M_KEYWORD = (CASE WHEN CLEANED_BRANDNAME IN ('NITAN','SWEET PEA','MOTHERLYLOVE','NAPPI','LA FEMME') THEN '�ش������ͧ/���������'
							WHEN CLEANED_BRANDNAME LIKE 'WACOAL' AND PRODUCT_NAME LIKE '%�س���%' AND PRODUCT_NAME NOT LIKE '%��ѧ��ʹ%' THEN '�ش���㹤س���'
							WHEN (PRODUCT_NAME LIKE '%����%���%' AND PRODUCT_NAME NOT LIKE '%��ѧ��ʹ%' AND PRODUCT_NAME NOT LIKE '%��駤��%') THEN '�ش���㹤س���'
							WHEN PRODUCT_NAME LIKE '%�س���%' AND (DEPT_NAME = 'WOMEN' OR SUBDEPT_NAME = 'LINGERIE') AND PRODUCT_NAME NOT LIKE '%������%' THEN '�ش���㹤س���'
							WHEN PRODUCT_NAME LIKE '%��͹+��ѧ%' THEN '��͹�����ѧ��ʹ'
							WHEN PRODUCT_NAME LIKE '%��͹�����ѧ��ʹ%' THEN '��͹�����ѧ��ʹ'
							WHEN PRODUCT_NAME LIKE '%�ҧࡧ���㹡�ЪѺ˹�ҷ�ͧ%' THEN '�ҧࡧ���㹡�ЪѺ˹�ҷ�ͧ'
							WHEN CLASS_NAME = 'MATERNITY INNERWEARS&PAJA' THEN '�ش������ͧ/���������'
							ELSE PREG_7_9M_KEYWORD END)
	,INFANT_UNKNOWN_KEYWORD = (CASE WHEN CLEANED_BRANDNAME IN ('NITAN','SWEET PEA','MOTHERLYLOVE','NAPPI','LA FEMME') THEN '�ش������ͧ/���������'
							WHEN CLEANED_BRANDNAME LIKE 'WACOAL' AND PRODUCT_NAME LIKE '%�س���%' AND PRODUCT_NAME NOT LIKE '%��ѧ��ʹ%' THEN '�ش���㹤س���'
							WHEN (PRODUCT_NAME LIKE '%����%���%' AND PRODUCT_NAME NOT LIKE '%��ѧ��ʹ%' AND PRODUCT_NAME NOT LIKE '%��駤��%') THEN '�ش���㹤س���'
							WHEN PRODUCT_NAME LIKE '%�س���%' AND (DEPT_NAME = 'WOMEN' OR SUBDEPT_NAME = 'LINGERIE') AND PRODUCT_NAME NOT LIKE '%������%' THEN '�ش���㹤س���'
							WHEN PRODUCT_NAME LIKE '%��͹+��ѧ%' THEN '��͹�����ѧ��ʹ'
							WHEN PRODUCT_NAME LIKE '%��͹�����ѧ��ʹ%' THEN '��͹�����ѧ��ʹ'
							WHEN PRODUCT_NAME LIKE '%�ҧࡧ���㹡�ЪѺ˹�ҷ�ͧ%' THEN '�ҧࡧ���㹡�ЪѺ˹�ҷ�ͧ'
							WHEN CLASS_NAME = 'MATERNITY INNERWEARS&PAJA' THEN '�ش������ͧ/���������'
							ELSE INFANT_UNKNOWN_KEYWORD END)
WHERE MOM_AND_KID_PRODUCT_FLAG = 1
AND COALESCE(PREG_FLAG,'N/A') = 'N/A'
AND COALESCE(INFANT_FLAG,'N/A') = 'N/A'
AND COALESCE(TODDLER_FLAG,'N/A') = 'N/A'
AND COALESCE(PRESCHOOL_FLAG,'N/A') = 'N/A'
AND COALESCE(JUNIOR_FLAG,'N/A') = 'N/A'
AND COALESCE(TEEN_FLAG,'N/A') = 'N/A'
AND (CLEANED_BRANDNAME IN ('NITAN','SWEET PEA','MOTHERLYLOVE','NAPPI','LA FEMME')
	OR (CLEANED_BRANDNAME LIKE 'WACOAL' AND PRODUCT_NAME LIKE '%�س���%' AND PRODUCT_NAME NOT LIKE '%��ѧ��ʹ%')
	OR (PRODUCT_NAME LIKE '%����%���%' AND PRODUCT_NAME NOT LIKE '%��ѧ��ʹ%' AND PRODUCT_NAME NOT LIKE '%��駤��%')
	OR (PRODUCT_NAME LIKE '%�س���%' AND (DEPT_NAME = 'WOMEN' OR SUBDEPT_NAME = 'LINGERIE') AND PRODUCT_NAME NOT LIKE '%������%')
	OR PRODUCT_NAME LIKE '%��͹+��ѧ%'
	OR PRODUCT_NAME LIKE '%��͹�����ѧ��ʹ%'
	OR PRODUCT_NAME LIKE '%�ҧࡧ���㹡�ЪѺ˹�ҷ�ͧ%'
	OR CLASS_NAME = 'MATERNITY INNERWEARS&PAJA')
;

--PREG 7-9
UPDATE vpm_data.pm_skutagging_kids_product_master
SET "STAGE_M-9_TO_M-6" = 'N/A'
	,"STAGE_M-6_TO_M-3" = 'N/A'
	,"STAGE_M-3_TO_M-0" = 'YES'
	,PREG_FLAG = 'YES'
	,UNKNOWN_PREG_SUBSTAGE = 'N/A'
	,INFANT_FLAG = 'N/A'
	,UNKNOWN_INFANT_SUBSTAGE = NULL
	,PREG_7_9M_KEYWORD = (CASE WHEN PRODUCT_NAME LIKE '%����Ѵ%��ا��ͧ%' THEN '����Ѵ��ا�����'
							WHEN PRODUCT_NAME LIKE '%����Ѵ��ا�����%' THEN '����Ѵ��ا�����'
							WHEN PRODUCT_NAME LIKE '%BELLY%LIFT%SUPPORT%BELT%' THEN '����Ѵ��ا�����'
							WHEN PRODUCT_NAME LIKE '%����ͧ�ѧ���§��á㹤����%' THEN '����ͧ�ѧ���§��á㹤����'
							ELSE PREG_7_9M_KEYWORD END)
WHERE MOM_AND_KID_PRODUCT_FLAG = 1
AND COALESCE(PREG_FLAG,'N/A') = 'N/A'
AND COALESCE(INFANT_FLAG,'N/A') = 'N/A'
AND COALESCE(TODDLER_FLAG,'N/A') = 'N/A'
AND COALESCE(PRESCHOOL_FLAG,'N/A') = 'N/A'
AND COALESCE(JUNIOR_FLAG,'N/A') = 'N/A'
AND COALESCE(TEEN_FLAG,'N/A') = 'N/A'
AND (PRODUCT_NAME LIKE '%����Ѵ%��ا��ͧ%'
	OR PRODUCT_NAME LIKE '%����Ѵ��ا�����%'
	OR PRODUCT_NAME LIKE '%BELLY%LIFT%SUPPORT%BELT%'
	OR PRODUCT_NAME LIKE '%����ͧ�ѧ���§��á㹤����%')
;

--ONLY PREG (UNKNOWN SUBSTAGE)
UPDATE vpm_data.pm_skutagging_kids_product_master
SET PREG_FLAG = 'YES'
	,UNKNOWN_PREG_SUBSTAGE = 'YES'
	,INFANT_FLAG = 'N/A'
	,UNKNOWN_INFANT_SUBSTAGE = NULL
	,PREG_UNKNOWN_KEYWORD = 'MATERNITY'
WHERE MOM_AND_KID_PRODUCT_FLAG = 1
AND COALESCE(PREG_FLAG,'N/A') = 'N/A'
AND COALESCE(INFANT_FLAG,'N/A') = 'N/A'
AND COALESCE(TODDLER_FLAG,'N/A') = 'N/A'
AND COALESCE(PRESCHOOL_FLAG,'N/A') = 'N/A'
AND COALESCE(JUNIOR_FLAG,'N/A') = 'N/A'
AND COALESCE(TEEN_FLAG,'N/A') = 'N/A'
AND PRODUCT_NAME LIKE '%MATERNITY%'
;

--PREG AND INFANT, BUT UNKNOWN SUBSTAGE
UPDATE vpm_data.pm_skutagging_kids_product_master
SET PREG_FLAG = 'YES'
	,UNKNOWN_PREG_SUBSTAGE = 'YES'
	,INFANT_FLAG = 'YES'
	,UNKNOWN_INFANT_SUBSTAGE = 'YES'
	,PREG_UNKNOWN_KEYWORD = (CASE WHEN CLEANED_BRANDNAME LIKE 'ANMUM' THEN 'MATERNAL MILK'
									WHEN CLEANED_BRANDNAME LIKE 'ENFAMAMA' THEN 'MATERNAL MILK'
									WHEN (CLEANED_BRANDNAME LIKE 'S-26' AND PRODUCT_NAME LIKE '%MOM MILK%') THEN 'MATERNAL MILK'
									WHEN (CLEANED_BRANDNAME LIKE 'CLARINS' AND PRODUCT_NAME LIKE '%STRETCH%MARK%') THEN 'STRETCH MARK CREAM'
									WHEN (CLEANED_BRANDNAME LIKE 'COCORO HANAKO' AND PRODUCT_NAME NOT LIKE '%BABY%NATURAL%SENSITIVE%SKIN%') THEN 'STRETCH MARK CREAM'
									WHEN (CLEANED_BRANDNAME LIKE 'PALMERS' AND PRODUCT_NAME NOT LIKE '%FIRMING%LOTION%') THEN 'STRETCH MARK CREAM'
									WHEN (CLEANED_BRANDNAME LIKE 'LAMOON' AND PRODUCT_NAME LIKE '%ANTI%STRETCH%MARK%CREAM%') THEN 'STRETCH MARK CREAM'
									WHEN PRODUCT_NAME LIKE '%�ѵ���������ҧ��駤����%' THEN 'STRETCH MARK CREAM'
									WHEN PRODUCT_NAME LIKE '%��������ͧ��Ǫ�ǧ��駤����%' THEN 'STRETCH MARK CREAM'
									WHEN PRODUCT_NAME LIKE 'INTENSIVE ANTI STRETCH MARKS' THEN 'STRETCH MARK CREAM'
									WHEN PRODUCT_NAME LIKE 'TUMMY WRAP AROUND%' THEN 'TUMMY WRAP' --CHANGE, BABY PRODUCT NOT MATERNITY
									WHEN PRODUCT_NAME LIKE '%��ǻ��%' THEN '�����ǻ��ʡѴ'
									ELSE PREG_UNKNOWN_KEYWORD END)
	,INFANT_UNKNOWN_KEYWORD = (CASE WHEN CLEANED_BRANDNAME LIKE 'ANMUM' THEN 'MATERNAL MILK'
									WHEN CLEANED_BRANDNAME LIKE 'ENFAMAMA' THEN 'MATERNAL MILK'
									WHEN (CLEANED_BRANDNAME LIKE 'S-26' AND PRODUCT_NAME LIKE '%MOM MILK%') THEN 'MATERNAL MILK'
									WHEN (CLEANED_BRANDNAME LIKE 'CLARINS' AND PRODUCT_NAME LIKE '%STRETCH%MARK%') THEN 'STRETCH MARK CREAM'
									WHEN (CLEANED_BRANDNAME LIKE 'COCORO HANAKO' AND PRODUCT_NAME NOT LIKE '%BABY%NATURAL%SENSITIVE%SKIN%') THEN 'STRETCH MARK CREAM'
									WHEN (CLEANED_BRANDNAME LIKE 'PALMERS' AND PRODUCT_NAME NOT LIKE '%FIRMING%LOTION%') THEN 'STRETCH MARK CREAM'
									WHEN (CLEANED_BRANDNAME LIKE 'LAMOON' AND PRODUCT_NAME LIKE '%ANTI%STRETCH%MARK%CREAM%') THEN 'STRETCH MARK CREAM'
									WHEN PRODUCT_NAME LIKE '%�ѵ���������ҧ��駤����%' THEN 'STRETCH MARK CREAM'
									WHEN PRODUCT_NAME LIKE '%��������ͧ��Ǫ�ǧ��駤����%' THEN 'STRETCH MARK CREAM'
									WHEN PRODUCT_NAME LIKE 'INTENSIVE ANTI STRETCH MARKS' THEN 'STRETCH MARK CREAM'
									WHEN PRODUCT_NAME LIKE 'TUMMY WRAP AROUND%' THEN 'TUMMY WRAP' --CHANGE, BABY PRODUCT NOT MATERNITY
									WHEN PRODUCT_NAME LIKE '%��ǻ��%' THEN '�����ǻ��ʡѴ'
									ELSE INFANT_UNKNOWN_KEYWORD END)
WHERE MOM_AND_KID_PRODUCT_FLAG = 1
AND COALESCE(PREG_FLAG,'N/A') = 'N/A'
AND COALESCE(INFANT_FLAG,'N/A') = 'N/A'
AND COALESCE(TODDLER_FLAG,'N/A') = 'N/A'
AND COALESCE(PRESCHOOL_FLAG,'N/A') = 'N/A'
AND COALESCE(JUNIOR_FLAG,'N/A') = 'N/A'
AND COALESCE(TEEN_FLAG,'N/A') = 'N/A'
AND (CLEANED_BRANDNAME LIKE 'ANMUM'
	OR CLEANED_BRANDNAME LIKE 'ENFAMAMA'
	OR (CLEANED_BRANDNAME LIKE 'S-26' AND PRODUCT_NAME LIKE '%MOM MILK%')
	OR (CLEANED_BRANDNAME LIKE 'CLARINS' AND PRODUCT_NAME LIKE '%STRETCH%MARK%')
	OR (CLEANED_BRANDNAME LIKE 'COCORO HANAKO' AND PRODUCT_NAME NOT LIKE '%BABY%NATURAL%SENSITIVE%SKIN%')
	OR (CLEANED_BRANDNAME LIKE 'PALMERS' AND PRODUCT_NAME NOT LIKE '%FIRMING%LOTION%')
	OR (CLEANED_BRANDNAME LIKE 'LAMOON' AND PRODUCT_NAME LIKE '%ANTI%STRETCH%MARK%CREAM%')
	OR PRODUCT_NAME LIKE '%�ѵ���������ҧ��駤����%'
	OR PRODUCT_NAME LIKE '%��������ͧ��Ǫ�ǧ��駤����%'
	OR PRODUCT_NAME LIKE 'INTENSIVE ANTI STRETCH MARKS'
	OR PRODUCT_NAME LIKE '%STRETCH MARK%'
	OR PRODUCT_NAME LIKE '%STRETCHMARK%'
	OR PRODUCT_NAME LIKE '%FIRMING LOTION%'
	OR PRODUCT_NAME LIKE 'TUMMY WRAP AROUND%'
	OR PRODUCT_NAME LIKE '%��ǻ��%')
;

--ONLY INFANT PERIOD
UPDATE vpm_data.pm_skutagging_kids_product_master
SET PREG_FLAG = 'N/A'
	,UNKNOWN_PREG_SUBSTAGE = 'N/A'
	,INFANT_FLAG = 'YES'
	,UNKNOWN_INFANT_SUBSTAGE = 'YES'
	,INFANT_UNKNOWN_KEYWORD = (CASE WHEN PRODUCT_NAME LIKE '%�س�����ѧ��ʹ%' THEN '�س�����ѧ��ʹ'
									WHEN PRODUCT_NAME LIKE '%��ѧ��ʹ%' THEN '��ѧ��ʹ'
									WHEN PRODUCT_NAME LIKE '%�����Ҥس���%' THEN '�����Ҥس���'
									WHEN PRODUCT_NAME LIKE '%�����ż�Ҥ�ʹ%' THEN '�ش���㹴����ż�Ҥ�ʹ'
									WHEN PRODUCT_NAME LIKE '%�á��ʹ%' THEN '�á��ʹ'
									WHEN PRODUCT_NAME LIKE '%��鹵��������ѧ��ʹ%' THEN '��ѧ��ʹ'
									WHEN PRODUCT_NAME LIKE '%����Ѵ��ѧ��ʹ%' THEN '����Ѵ˹�ҷ�ͧ��ѧ��ʹ'
									WHEN PRODUCT_NAME LIKE '%NURSING%COVER%' THEN '��Ҥ�������'
									WHEN PRODUCT_NAME LIKE '%��Ҥ�������%' THEN '��Ҥ�������'
									WHEN PRODUCT_NAME LIKE '%�������%' THEN '��Ҥ�������'
									WHEN PRODUCT_NAME LIKE '%����١���%' THEN '����١��͹'
									WHEN (PRODUCT_NAME LIKE '%��Ҥ���%' AND CLEANED_BRANDNAME LIKE 'ATTITUDE MOM') THEN '��Ҥ�������'
									WHEN PRODUCT_NAME LIKE '%NURSING%BRA%' THEN '�ش���㹻�����'
									WHEN CLEANED_BRANDNAME = 'AMEDA' THEN '�ش���㹻�����'
									WHEN PRODUCT_NAME LIKE '%BREASTFEED%DRESS%' THEN '���������'
									WHEN PRODUCT_NAME LIKE '%MILK BAG%' THEN '�ا�纹�ӹ�'
									WHEN PRODUCT_NAME LIKE '%BREAST MILK BAGS%' THEN '�ا�纹�ӹ�'
									WHEN PRODUCT_NAME LIKE '%BREAST MILK STORAGE%' THEN '�ا�纹�ӹ�'
									WHEN PRODUCT_NAME LIKE 'C-SECTION BRIEF%' THEN '�ż�Ҥ�ʹ'
									WHEN PRODUCT_NAME LIKE '%NIPPLE CORRECTOR%' THEN 'BREAST SHIELD'
									WHEN PRODUCT_NAME LIKE '%NIPPLE PROTECTOR%' THEN 'BREAST SHIELD'
									WHEN PRODUCT_NAME LIKE '%NIPPLE FORMER%' THEN 'BREAST SHIELD'
									WHEN PRODUCT_NAME LIKE '%NIPPLE SHIELD%' THEN 'BREAST SHIELD'
									WHEN PRODUCT_NAME LIKE '%BREAST PAD%' THEN '�蹫Ѻ��ӹ�'
									WHEN PRODUCT_NAME LIKE '%BREASTPAD%' THEN '�蹫Ѻ��ӹ�'
									WHEN PRODUCT_NAME LIKE '%NURSING PADS%' THEN '�蹫Ѻ��ӹ�'
									WHEN PRODUCT_NAME LIKE '%BREAST SHELL%' THEN 'BREAST SHIELD'
									WHEN PRODUCT_NAME LIKE '%BREAST MILK%' THEN '��ӹ�'
									WHEN (SUBCLASS_NAME LIKE 'BREAST ACC/ELECTRIC' AND PRODUCT_NAME LIKE '%����%') THEN '������'
									ELSE INFANT_UNKNOWN_KEYWORD END)
WHERE MOM_AND_KID_PRODUCT_FLAG = 1
AND COALESCE(PREG_FLAG,'N/A') = 'N/A'
AND COALESCE(INFANT_FLAG,'N/A') = 'N/A'
AND COALESCE(TODDLER_FLAG,'N/A') = 'N/A'
AND COALESCE(PRESCHOOL_FLAG,'N/A') = 'N/A'
AND COALESCE(JUNIOR_FLAG,'N/A') = 'N/A'
AND COALESCE(TEEN_FLAG,'N/A') = 'N/A'
AND (PRODUCT_NAME LIKE '%�س�����ѧ��ʹ%'
	OR PRODUCT_NAME LIKE '%��ѧ��ʹ%'
	OR PRODUCT_NAME LIKE '%�����Ҥس���%'
	OR PRODUCT_NAME LIKE '%�����ż�Ҥ�ʹ%'
	OR PRODUCT_NAME LIKE '%�á��ʹ%'
	OR PRODUCT_NAME LIKE '%��鹵��������ѧ��ʹ%'
	OR PRODUCT_NAME LIKE '%����Ѵ��ѧ��ʹ%'
	OR PRODUCT_NAME LIKE '%NURSING%COVER%'
	OR PRODUCT_NAME LIKE '%��Ҥ�������%'
	OR PRODUCT_NAME LIKE '%�������%'
	OR PRODUCT_NAME LIKE '%����١���%'
	OR (PRODUCT_NAME LIKE '%��Ҥ���%' AND CLEANED_BRANDNAME LIKE 'ATTITUDE MOM')
	OR PRODUCT_NAME LIKE '%NURSING%BRA%'
	OR CLEANED_BRANDNAME = 'AMEDA'
	OR PRODUCT_NAME LIKE '%BREASTFEED%DRESS%'
	OR PRODUCT_NAME LIKE '%MILK BAG%'
	OR PRODUCT_NAME LIKE '%BREAST MILK BAGS%'
	OR PRODUCT_NAME LIKE '%BREAST MILK STORAGE%'
	OR PRODUCT_NAME LIKE 'C-SECTION BRIEF%'
	OR PRODUCT_NAME LIKE '%NIPPLE CORRECTOR%'
	OR PRODUCT_NAME LIKE '%NIPPLE PROTECTOR%'
	OR PRODUCT_NAME LIKE '%NIPPLE FORMER%'
	OR PRODUCT_NAME LIKE '%NIPPLE SHIELD%'
	OR PRODUCT_NAME LIKE '%BREAST PAD%'
	OR PRODUCT_NAME LIKE '%BREASTPAD%'
	OR PRODUCT_NAME LIKE '%NURSING PADS%'
	OR PRODUCT_NAME LIKE '%BREAST SHELL%'
	OR PRODUCT_NAME LIKE '%BREAST MILK%'
	OR (SUBCLASS_NAME LIKE 'BREAST ACC/ELECTRIC' AND PRODUCT_NAME LIKE '%����%'))
;

--UPDATE NOT INFANT FLAG
UPDATE vpm_data.pm_skutagging_kids_product_master
SET INFANT_FLAG = 'N/A'
	,UNKNOWN_INFANT_SUBSTAGE = NULL
	,INFANT_UNKNOWN_KEYWORD = NULL
WHERE MOM_AND_KID_PRODUCT_FLAG = 1
AND ((REVISED_CATEGORY_LEVEL_2 ='TOYS' AND (PRODUCT_NAME LIKE '%�ش��Сͺ%'
											OR PRODUCT_NAME LIKE '%�Թ����ѹ+����������¤��%'
											OR PRODUCT_NAME LIKE '%2Y+%'
											OR PRODUCT_NAME LIKE '%SPD TITAN POWER WEB WARRIORS-A%'))
	OR (REVISED_CATEGORY_LEVEL_2 = 'BOOK' AND (PRODUCT_NAME LIKE '%EF%'
											 OR PRODUCT_NAME LIKE  '%������%'
											 OR PRODUCT_NAME LIKE  '%��駤��%'
											 OR PRODUCT_NAME LIKE  '%�.3%'
											 OR PRODUCT_NAME LIKE '%�����͵�駪���%'
											 OR PRODUCT_NAME LIKE  '%�ç���¹%'))
	--PREGNANCY PRODUCT
	OR (CLEANED_BRANDNAME LIKE 'CAMERA' AND PRODUCT_NAME LIKE '%����Ѵ%��ا��ͧ%')
	OR (CLEANED_BRANDNAME LIKE 'MOM' AND PRODUCT_NAME LIKE '%BELLY%LIFT%SUPPORT%BELT%')
	OR (CLEANED_BRANDNAME LIKE 'DRINK IN THE BOX' AND PRODUCT_NAME LIKE '%��͹��Фͧ��ͧ%')
	OR (CLEANED_BRANDNAME LIKE 'GLOWY STAR' AND PRODUCT_NAME LIKE '%��͹�ʹ�س���%')
	OR (CLEANED_BRANDNAME LIKE 'NITID' AND PRODUCT_NAME LIKE '%��͹��ا�����%')
	OR (CLEANED_BRANDNAME LIKE 'WACOAL' AND PRODUCT_NAME LIKE '%��駤����%')
	OR (CLEANED_BRANDNAME LIKE 'MOTHERLYLOVE' AND PRODUCT_NAME LIKE '%PREGNANCY%LEGGING%' AND PRODUCT_NAME NOT LIKE '%����%')
	OR (CLEANED_BRANDNAME LIKE 'NAPPI' AND PRODUCT_NAME LIKE '%LEGGING%' AND PRODUCT_NAME NOT LIKE '%����%'))
;


------------------------------------------- DIAPERS -------------------------------------------
/* DIAPER SIZE M FOR 7-12 MONTHS */
UPDATE vpm_data.pm_skutagging_kids_product_master
SET "STAGE_M+0_TO_M+3" = 'N/A'
	,"STAGE_M+3_TO_M+6" = 'N/A'
	,"STAGE_M+6_TO_M+9" = 'YES'
	,"STAGE_M+9_TO_M+12" = 'YES'
	,INFANT_FLAG = 'YES'
	,UNKNOWN_INFANT_SUBSTAGE = 'N/A'
	,TODDLER_FLAG = 'N/A'
	,INFANT_7_9M_KEYWORD = 'DIAPER SIZE M'
	,INFANT_10_12M_KEYWORD = 'DIAPER SIZE M'
WHERE MOM_AND_KID_PRODUCT_FLAG = 1
AND COALESCE(PREG_FLAG,'N/A') = 'N/A'
AND COALESCE(INFANT_FLAG,'N/A') = 'N/A'
AND COALESCE(TODDLER_FLAG,'N/A') = 'N/A'
AND COALESCE(PRESCHOOL_FLAG,'N/A') = 'N/A'
AND COALESCE(JUNIOR_FLAG,'N/A') = 'N/A'
AND COALESCE(TEEN_FLAG,'N/A') = 'N/A'
AND ((REVISED_CATEGORY_LEVEL_2 = 'DIAPERS' AND (PRODUCT_NAME LIKE '%M[0-9]%'
												 OR PRODUCT_NAME LIKE '% M %' 
												 OR PRODUCT_NAME LIKE '%M [0-9]%' 
												 OR PRODUCT_NAME LIKE '%M X[0-9]%'
												 OR PRODUCT_NAME LIKE '%6M'
												 OR PRODUCT_NAME LIKE '% M'
												 OR PRODUCT_NAME LIKE '% M(%'
												 OR PRODUCT_NAME LIKE '% M-%'
												 OR PRODUCT_NAME LIKE 'HUGGIES%M36'))
	 OR (CLASS_NAME LIKE 'BABY DIAPERS' AND PRODUCT_NAME LIKE '%M%'))
;

/* DIAPER SIZE L FOR >1 YEAR */
UPDATE vpm_data.pm_skutagging_kids_product_master
SET TODDLER_FLAG = 'YES'
	,INFANT_FLAG = 'N/A'
	,TODDLER_KEYWORD = 'DIAPER SIZE L'
WHERE MOM_AND_KID_PRODUCT_FLAG = 1
AND COALESCE(PREG_FLAG,'N/A') = 'N/A'
AND COALESCE(INFANT_FLAG,'N/A') = 'N/A'
AND COALESCE(TODDLER_FLAG,'N/A') = 'N/A'
AND COALESCE(PRESCHOOL_FLAG,'N/A') = 'N/A'
AND COALESCE(JUNIOR_FLAG,'N/A') = 'N/A'
AND COALESCE(TEEN_FLAG,'N/A') = 'N/A'
AND ((REVISED_CATEGORY_LEVEL_2 = 'DIAPERS' AND (PRODUCT_NAME LIKE '%L[0-9]%'
												 OR PRODUCT_NAME LIKE '% L %' 
												 OR PRODUCT_NAME LIKE '%L [0-9]%' 
												 OR PRODUCT_NAME LIKE '%L X[0-9]%'
												 OR PRODUCT_NAME LIKE '%-L'
												 OR PRODUCT_NAME LIKE '%18M%'
												 OR PRODUCT_NAME LIKE '% L'
												 OR PRODUCT_NAME LIKE '%12M%'
												 OR PRODUCT_NAME LIKE '% L(%'
												 OR PRODUCT_NAME LIKE '%DIAPERL %')
											AND PRODUCT_NAME NOT LIKE '%XL%'
											AND PRODUCT_NAME NOT LIKE '%�Ѻ%')
	OR (CLASS_NAME LIKE 'BABY DIAPERS' AND PRODUCT_NAME LIKE '%L%' AND PRODUCT_NAME NOT LIKE '%XL%'))
AND PRODUCT_NAME NOT LIKE '%CERTAINTY%'
;

/* DIAPER SIZE S FOR 4-6 MONTHS */
UPDATE vpm_data.pm_skutagging_kids_product_master
SET "STAGE_M+0_TO_M+3" = 'N/A'
	,"STAGE_M+3_TO_M+6" = 'YES'
	,"STAGE_M+6_TO_M+9" = 'N/A'
	,"STAGE_M+9_TO_M+12" = 'N/A'
	,INFANT_FLAG = 'YES'
	,UNKNOWN_INFANT_SUBSTAGE = 'N/A'
	,TODDLER_FLAG = 'N/A'
	,INFANT_4_6M_KEYWORD = 'DIAPER SIZE S'
WHERE MOM_AND_KID_PRODUCT_FLAG = 1
AND COALESCE(PREG_FLAG,'N/A') = 'N/A'
AND COALESCE(INFANT_FLAG,'N/A') = 'N/A'
AND COALESCE(TODDLER_FLAG,'N/A') = 'N/A'
AND COALESCE(PRESCHOOL_FLAG,'N/A') = 'N/A'
AND COALESCE(JUNIOR_FLAG,'N/A') = 'N/A'
AND COALESCE(TEEN_FLAG,'N/A') = 'N/A'
AND ((REVISED_CATEGORY_LEVEL_2 = 'DIAPERS' AND (PRODUCT_NAME LIKE '%S[0-9]%' 
												 OR PRODUCT_NAME LIKE '% S %' 
												 OR PRODUCT_NAME LIKE '%S [0-9]%' 
												 OR PRODUCT_NAME LIKE '%S X[0-9]%'
												 OR PRODUCT_NAME LIKE '%-S'
												 OR PRODUCT_NAME LIKE '%-SMALL'
												 OR PRODUCT_NAME LIKE '% S-%'
												 OR PRODUCT_NAME LIKE '%SIZES%'
												 OR PRODUCT_NAME LIKE '% S'
												 OR PRODUCT_NAME LIKE '% S(%'))
	 OR (CLASS_NAME LIKE 'BABY DIAPERS' AND PRODUCT_NAME LIKE '%S%'))
;

/* DIAPER SIZE NB FOR 0-3 MONTHS */
UPDATE vpm_data.pm_skutagging_kids_product_master
SET "STAGE_M+0_TO_M+3" = 'YES'
	,"STAGE_M+3_TO_M+6" = 'N/A'
	,"STAGE_M+6_TO_M+9" = 'N/A'
	,"STAGE_M+9_TO_M+12" = 'N/A'
	,INFANT_FLAG = 'YES'
	,UNKNOWN_INFANT_SUBSTAGE = 'N/A'
	,TODDLER_FLAG = 'N/A'
	,INFANT_0_3M_KEYWORD = 'DIAPER SIZE NB'
WHERE MOM_AND_KID_PRODUCT_FLAG = 1
AND (REVISED_CATEGORY_LEVEL_2 = 'DIAPERS' OR CLASS_NAME LIKE 'BABY DIAPERS')
AND COALESCE(PREG_FLAG,'N/A') = 'N/A'
AND COALESCE(INFANT_FLAG,'N/A') = 'N/A'
AND COALESCE(TODDLER_FLAG,'N/A') = 'N/A'
AND COALESCE(PRESCHOOL_FLAG,'N/A') = 'N/A'
AND COALESCE(JUNIOR_FLAG,'N/A') = 'N/A'
AND COALESCE(TEEN_FLAG,'N/A') = 'N/A'
AND ((PRODUCT_NAME LIKE '%NB%' AND PRODUCT_NAME NOT LIKE '%GOO.NBABY%XL%' AND PRODUCT_NAME NOT LIKE '%GOONBABY%XL%')
     OR PRODUCT_NAME LIKE '%NEW BORN%' 
	 OR PRODUCT_NAME LIKE '%NEWBORN%'
	 OR PRODUCT_NAME LIKE '%෻��Ǻ���%'
	 OR PRODUCT_NAME LIKE '%0-3 MTH%')
;

/* DIAPER SIZE XL+ FOR TODDLER */
UPDATE vpm_data.pm_skutagging_kids_product_master
SET INFANT_FLAG = 'N/A'
	,TODDLER_FLAG = 'YES'
	,TODDLER_KEYWORD = 'DIAPER SIZE XL'
WHERE MOM_AND_KID_PRODUCT_FLAG = 1
AND COALESCE(PREG_FLAG,'N/A') = 'N/A'
AND COALESCE(INFANT_FLAG,'N/A') = 'N/A'
AND COALESCE(TODDLER_FLAG,'N/A') = 'N/A'
AND COALESCE(PRESCHOOL_FLAG,'N/A') = 'N/A'
AND COALESCE(JUNIOR_FLAG,'N/A') = 'N/A'
AND COALESCE(TEEN_FLAG,'N/A') = 'N/A'
AND (REVISED_CATEGORY_LEVEL_2 = 'DIAPERS' OR REVISED_CATEGORY_LEVEL_2 IS NULL)
AND PRODUCT_NAME LIKE '%XL%'
AND PRODUCT_NAME NOT LIKE '%CERTAINTY%'
;

------------------------------------------- KID FOOD -------------------------------------------
------------------ MILK POWDER ------------------
/* FOOD FOR <1 YEAR; MILK POWDER STEP 1 */
UPDATE vpm_data.pm_skutagging_kids_product_master
SET "STAGE_M+0_TO_M+3" = 'YES'
	,"STAGE_M+3_TO_M+6" = 'YES'
	,"STAGE_M+6_TO_M+9" = 'YES'
	,"STAGE_M+9_TO_M+12" = 'YES'
	,INFANT_FLAG = 'YES'
	,UNKNOWN_INFANT_SUBSTAGE = 'N/A'
	,TODDLER_FLAG = 'N/A'
	,INFANT_0_3M_KEYWORD = 'MILK POWDER FORMULA 1'
	,INFANT_4_6M_KEYWORD = 'MILK POWDER FORMULA 1'
	,INFANT_7_9M_KEYWORD = 'MILK POWDER FORMULA 1'
	,INFANT_10_12M_KEYWORD = 'MILK POWDER FORMULA 1'
WHERE MOM_AND_KID_PRODUCT_FLAG = 1
AND COALESCE(PREG_FLAG,'N/A') = 'N/A'
AND COALESCE(INFANT_FLAG,'N/A') = 'N/A'
AND COALESCE(TODDLER_FLAG,'N/A') = 'N/A'
AND COALESCE(PRESCHOOL_FLAG,'N/A') = 'N/A'
AND COALESCE(JUNIOR_FLAG,'N/A') = 'N/A'
AND COALESCE(TEEN_FLAG,'N/A') = 'N/A'
AND REVISED_CATEGORY_LEVEL_3 = 'MILK POWDER'
AND (PRODUCT_NAME LIKE '%ENFALAC 1%'
	OR PRODUCT_NAME LIKE '%ENFALAC1%'
	OR PRODUCT_NAME LIKE '%SIMILAC 1%'
	OR PRODUCT_NAME LIKE '%SIMILACCOMFORT 1%'
	OR PRODUCT_NAME LIKE '%SIMILAC%ALIMENTUM%'
	OR PRODUCT_NAME LIKE '%SIMILAC%INFANT%'
	OR PRODUCT_NAME LIKE '%DG1%'
	OR PRODUCT_NAME LIKE '%ENFALAC%A%PLUS1%'
	OR PRODUCT_NAME LIKE '%DULAC%'
	OR PRODUCT_NAME LIKE '%���Ť%'
	OR PRODUCT_NAME LIKE '%S26%INFANT%'
	OR PRODUCT_NAME LIKE '%S26%SMA%'
	OR PRODUCT_NAME LIKE '%HI%Q%1 %'
	OR PRODUCT_NAME LIKE '%HI%Q%INFANT%'
	OR PRODUCT_NAME LIKE '%HIQ%PEPTI%'
	OR PRODUCT_NAME LIKE '%HI Q%C SYNBIO%'
	OR PRODUCT_NAME LIKE '%NAN%INFANT%'
	OR PRODUCT_NAME LIKE '%NAN%HA1%'
	OR PRODUCT_NAME LIKE '%NAN%SUPREMIL1%'
	OR PRODUCT_NAME LIKE '%NUTRICIA%INFATRINI%'
	OR PRODUCT_NAME LIKE '%ISOMIL%INFANT%'
	OR PRODUCT_NAME LIKE '%ENFALAC%INFANT%'
	OR PRODUCT_NAME LIKE '%ENFALAC%PLUS1%'
	OR PRODUCT_NAME LIKE '%ENFALAC%DHA1%'
	OR PRODUCT_NAME LIKE '%ENFALAC%LACTOFREE%'
	OR PRODUCT_NAME LIKE '%ENFALAC%CARE%'
	OR PRODUCT_NAME LIKE '%NUTRAMIGEN%'
	OR PRODUCT_NAME LIKE '%BEBE1%'
	OR PRODUCT_NAME LIKE '%BEBE STARTINSTANT FORMULA%'
	OR PRODUCT_NAME LIKE '%ENFALAC A PLUS 350G%'
	OR PRODUCT_NAME LIKE '%ENFALAC 800G%'
	OR PRODUCT_NAME LIKE '%SIMILAC1%'
	OR PRODUCT_NAME LIKE '%SNOWMATE 1%'
	OR PRODUCT_NAME LIKE '%LACTOGEN1%'
	OR PRODUCT_NAME LIKE '%HIQ HA 1%'
	OR PRODUCT_NAME LIKE '%HIQ HA1%')
AND PRODUCT_NAME NOT LIKE '%HIQ%1PLUS%'
AND PRODUCT_NAME NOT LIKE '%HIQ%1 PLUS%'
AND PRODUCT_NAME NOT LIKE '%HI Q%1PLUS%'
AND PRODUCT_NAME NOT LIKE '%HI Q%1 PLUS%'
;

/* 6 MONTHS - 3 YEARS; MILK POWDER STEP 2 */
UPDATE vpm_data.pm_skutagging_kids_product_master
SET "STAGE_M+0_TO_M+3" = 'N/A'
	,"STAGE_M+3_TO_M+6" = 'N/A'
	,"STAGE_M+6_TO_M+9" = 'YES'
	,"STAGE_M+9_TO_M+12" = 'YES'
	,INFANT_FLAG = 'YES'
	,UNKNOWN_INFANT_SUBSTAGE = 'N/A'
	,TODDLER_FLAG = 'YES'
	,INFANT_7_9M_KEYWORD = 'MILK POWDER FORMULA 2'
	,INFANT_10_12M_KEYWORD = 'MILK POWDER FORMULA 2'
	,TODDLER_KEYWORD = 'MILK POWDER FORMULA 2'
WHERE MOM_AND_KID_PRODUCT_FLAG = 1
AND COALESCE(PREG_FLAG,'N/A') = 'N/A'
AND COALESCE(INFANT_FLAG,'N/A') = 'N/A'
AND COALESCE(TODDLER_FLAG,'N/A') = 'N/A'
AND COALESCE(PRESCHOOL_FLAG,'N/A') = 'N/A'
AND COALESCE(JUNIOR_FLAG,'N/A') = 'N/A'
AND COALESCE(TEEN_FLAG,'N/A') = 'N/A'
AND REVISED_CATEGORY_LEVEL_3 = 'MILK POWDER'
AND (PRODUCT_NAME LIKE '%ENFALAC 2%'
	OR PRODUCT_NAME LIKE '%ENFALAC2%'
	OR PRODUCT_NAME LIKE '%ENFALAC A PLUS2 %'
	OR PRODUCT_NAME LIKE '%ENFALAC A PLUS 2 %'
	OR PRODUCT_NAME LIKE '%ENFALAC A PLUS DHA2 %'
	OR PRODUCT_NAME LIKE '%ENFALAC A PLUS DHA 2 %'
	OR PRODUCT_NAME LIKE '%ENFALAC A PLUS DHA A 2 %'
	OR PRODUCT_NAME LIKE '%SIMILAC2%'
	OR PRODUCT_NAME LIKE '%SIMILACCOMFORT2%'
	OR PRODUCT_NAME LIKE '%DG2%'
	OR PRODUCT_NAME LIKE '%S26%FOLLOW ON%'
	OR PRODUCT_NAME LIKE '%S26%PROMIL%'
	OR PRODUCT_NAME LIKE '%HIQ 2%'
	OR PRODUCT_NAME LIKE '%HI Q SUPER GOLD 2%'
	OR PRODUCT_NAME LIKE '%NAN%HA2%'
	OR PRODUCT_NAME LIKE '%NAN%SUPREMIL2%'
	OR PRODUCT_NAME LIKE '%BEBE%2%'
	OR PRODUCT_NAME LIKE '%DUPRO%'
	OR PRODUCT_NAME LIKE '%����%������ͧ%'
	OR PRODUCT_NAME LIKE '%FOLLOW%'
	OR PRODUCT_NAME LIKE '%LACTOGEN2%'
	OR PRODUCT_NAME LIKE '%HIQ HA%2%'
	OR PRODUCT_NAME LIKE '%SNOWMATE2%'
	OR PRODUCT_NAME LIKE '%ENFALAC2%'
	OR PRODUCT_NAME LIKE '%FOLLOW ON%'
	OR PRODUCT_NAME LIKE '%BEAR BRAND2%')
;

/* >1 YEAR; MILK POWDER STEP 3 */
UPDATE vpm_data.pm_skutagging_kids_product_master
SET INFANT_FLAG = 'N/A'
	,TODDLER_FLAG = 'YES'
	,TODDLER_KEYWORD = 'MILK POWDER FORMULA 3'
WHERE MOM_AND_KID_PRODUCT_FLAG = 1
AND COALESCE(PREG_FLAG,'N/A') = 'N/A'
AND COALESCE(INFANT_FLAG,'N/A') = 'N/A'
AND COALESCE(TODDLER_FLAG,'N/A') = 'N/A'
AND COALESCE(PRESCHOOL_FLAG,'N/A') = 'N/A'
AND COALESCE(JUNIOR_FLAG,'N/A') = 'N/A'
AND COALESCE(TEEN_FLAG,'N/A') = 'N/A'
AND REVISED_CATEGORY_LEVEL_3 = 'MILK POWDER'
AND (PRODUCT_NAME LIKE '%ENFALAC 3%'
	OR PRODUCT_NAME LIKE '%SIMILAC 3%'
	OR PRODUCT_NAME LIKE '%SIMILAC COMFORT3%'
	OR PRODUCT_NAME LIKE '%DG3%'
	OR PRODUCT_NAME LIKE '%1 PLUS%'
	OR PRODUCT_NAME LIKE '%1PLUS%'
	OR PRODUCT_NAME LIKE '%DUMILK 3%'
	OR PRODUCT_NAME LIKE '%DUMILK3%'
	OR PRODUCT_NAME LIKE '%CARNATION%1%PLUS%'
	OR PRODUCT_NAME LIKE '%ENFAGROW%A%PLUS4%'
	OR PRODUCT_NAME LIKE '%ENFAGROW%3%'
	OR PRODUCT_NAME LIKE '%DUMEX%3%'
	OR PRODUCT_NAME LIKE '%S26%PROGRES%'
	OR PRODUCT_NAME LIKE '%S26%PE%'
	OR PRODUCT_NAME LIKE '%HI%Q%1%PLUS%'
	OR PRODUCT_NAME LIKE '%BEAR%BRAND%3%'
	OR PRODUCT_NAME LIKE '%NANKID%3%'
	OR PRODUCT_NAME LIKE '%NANKID%1PLUS%'
	OR PRODUCT_NAME LIKE '%DUGRO%3%'
	OR PRODUCT_NAME LIKE '%NUTRICIA%MILNUTRISURE%'
	OR PRODUCT_NAME LIKE '%ISOMIL PLUS AI Q PLUS%'
	OR PRODUCT_NAME LIKE '%DUTCHMILL%3%'
	OR PRODUCT_NAME LIKE '%LACTOGEN3%'
	OR PRODUCT_NAME LIKE 'NUTRICIANUTRINIDRINKVANILA400G')
;

------------------  KID NOURISHMENT ------------------
-- MORE THAN 3 MONTHS
UPDATE vpm_data.pm_skutagging_kids_product_master
SET "STAGE_M+0_TO_M+3" = 'N/A'
	,"STAGE_M+3_TO_M+6" = 'YES'
	,"STAGE_M+6_TO_M+9" = 'YES'
	,"STAGE_M+9_TO_M+12" = 'YES'
	,INFANT_FLAG = 'YES'
	,UNKNOWN_INFANT_SUBSTAGE = 'N/A'
	,INFANT_4_6M_KEYWORD = 'KID NOURISHMENT 3M+'
	,INFANT_7_9M_KEYWORD = 'KID NOURISHMENT 3M+'
	,INFANT_10_12M_KEYWORD = 'KID NOURISHMENT 3M+'
WHERE MOM_AND_KID_PRODUCT_FLAG = 1
AND COALESCE(PREG_FLAG,'N/A') = 'N/A'
AND COALESCE(INFANT_FLAG,'N/A') = 'N/A'
AND COALESCE(TODDLER_FLAG,'N/A') = 'N/A'
AND COALESCE(PRESCHOOL_FLAG,'N/A') = 'N/A'
AND COALESCE(JUNIOR_FLAG,'N/A') = 'N/A'
AND COALESCE(TEEN_FLAG,'N/A') = 'N/A'
AND REVISED_CATEGORY_LEVEL_3 = 'KID NOURISHMENT'
AND (PRODUCT_NAME LIKE '%HEINZ BABY CORN CHICKEN%'
     OR PRODUCT_NAME LIKE '%HEIN%ORGANIC%BABY%VEGETABLE%')
;

-- MORE THAN 6 MONTHS
UPDATE vpm_data.pm_skutagging_kids_product_master
SET "STAGE_M+0_TO_M+3" = 'N/A'
	,"STAGE_M+3_TO_M+6" = 'N/A'
	,"STAGE_M+6_TO_M+9" = 'YES'
	,"STAGE_M+9_TO_M+12" = 'YES'
	,INFANT_FLAG = 'YES'
	,UNKNOWN_INFANT_SUBSTAGE = 'N/A'
	,INFANT_7_9M_KEYWORD = 'KID NOURISHMENT 6M+'
	,INFANT_10_12M_KEYWORD = 'KID NOURISHMENT 6M+'
WHERE MOM_AND_KID_PRODUCT_FLAG = 1
AND COALESCE(PREG_FLAG,'N/A') = 'N/A'
AND COALESCE(INFANT_FLAG,'N/A') = 'N/A'
AND COALESCE(TODDLER_FLAG,'N/A') = 'N/A'
AND COALESCE(PRESCHOOL_FLAG,'N/A') = 'N/A'
AND COALESCE(JUNIOR_FLAG,'N/A') = 'N/A'
AND COALESCE(TEEN_FLAG,'N/A') = 'N/A'
AND REVISED_CATEGORY_LEVEL_3 = 'KID NOURISHMENT'
AND (PRODUCT_NAME LIKE '%APPLE MONKEY GRAINS HONEY%'
     OR PRODUCT_NAME LIKE '%APPLE MONKEY BANANA&STRW%'
     OR PRODUCT_NAME LIKE '%APPLE MONKEY PUMPKIN%'
     OR PRODUCT_NAME LIKE '%APPLE MONKEY CORN%'
     OR PRODUCT_NAME LIKE '%APPLE MONKEY STRW BANANA%'
     OR PRODUCT_NAME LIKE '%APPLE MONKEY BLUEBERY&STRW%'
     OR PRODUCT_NAME LIKE '%APPLE MONKEY SWEET POTATO%'
     OR PRODUCT_NAME LIKE 'BABY NATURA%'
     OR PRODUCT_NAME LIKE '%BEDELIGHT APPLE & PRUNE%'
	 OR PRODUCT_NAME LIKE 'CERELAC BABY%'
	 OR PRODUCT_NAME LIKE 'DOZO BABY%'
	 OR PRODUCT_NAME LIKE '%HEINZOGANICAPPLEBERRYBLUSH%'
	 OR PRODUCT_NAME LIKE '%HEINZ BABY EGG CUSTARD%'
	 OR PRODUCT_NAME LIKE '%HEINZ BABY CUSTARD STRAW%'
	 OR PRODUCT_NAME LIKE '%HEINZ BABY VEG CHICKEN%'
	 OR PRODUCT_NAME LIKE '%HEINZ CHOCO BISCOTTI SNACK%'
	 OR PRODUCT_NAME LIKE '%HEINZ ORGANICCREAMYVANILA%'
	 OR PRODUCT_NAME LIKE '%HOORAY PUMPKINOATMEALPUREE%'
	 OR PRODUCT_NAME LIKE 'LITTLE%FREDDIE%'
	 OR PRODUCT_NAME LIKE 'MAMACOOKS%'
	 OR PRODUCT_NAME LIKE 'HAPPY BITES%'
	 OR PRODUCT_NAME LIKE 'NAMCHOW HAPPY BITES%'
	 OR (PRODUCT_NAME LIKE '%PEACHY%' AND PRODUCT_NAME NOT LIKE '%SMOOTHIE%' AND PRODUCT_NAME NOT LIKE '%COOKIE%')
	 OR PRODUCT_NAME LIKE '%PICNICSUPERVEGGIESRICE&CHICKEN%'
	 OR PRODUCT_NAME LIKE '%PICNICBABY CHICKEN LIVER%'
	 OR PRODUCT_NAME LIKE '%PICNICBABY CHICKEN%'
	 OR PRODUCT_NAME LIKE '%PICNICBABY SALMON%'
	 OR PRODUCT_NAME LIKE '%RICELICIOUS%'
	 OR PRODUCT_NAME LIKE 'UNCLEMARK%'
	 OR PRODUCT_NAME LIKE 'UNCLE MARK%')
;

-- MORE THAN 9 MONTHS
UPDATE vpm_data.pm_skutagging_kids_product_master
SET "STAGE_M+0_TO_M+3" = 'N/A'
	,"STAGE_M+3_TO_M+6" = 'N/A'
	,"STAGE_M+6_TO_M+9" = 'N/A'
	,"STAGE_M+9_TO_M+12" = 'YES'
	,INFANT_FLAG = 'YES'
	,UNKNOWN_INFANT_SUBSTAGE = 'N/A'
	,TODDLER_FLAG = 'YES'
	,INFANT_10_12M_KEYWORD = 'KID NOURISHMENT 9M+'
	,TODDLER_KEYWORD = 'KID NOURISHMENT 9M+'
WHERE MOM_AND_KID_PRODUCT_FLAG = 1
AND COALESCE(PREG_FLAG,'N/A') = 'N/A'
AND COALESCE(INFANT_FLAG,'N/A') = 'N/A'
AND COALESCE(TODDLER_FLAG,'N/A') = 'N/A'
AND COALESCE(PRESCHOOL_FLAG,'N/A') = 'N/A'
AND COALESCE(JUNIOR_FLAG,'N/A') = 'N/A'
AND COALESCE(TEEN_FLAG,'N/A') = 'N/A'
AND REVISED_CATEGORY_LEVEL_3 = 'KID NOURISHMENT'
AND (PRODUCT_NAME LIKE '%KHAO KHUN MHOR%'
     OR PRODUCT_NAME LIKE '%KHAOKHUNMHOR%'
     OR PRODUCT_NAME LIKE '%HOORAY QUINOA & VEGTABLE%'
     OR PRODUCT_NAME LIKE '%HOORAYSALMONPORRIDGE%'
     OR PRODUCT_NAME LIKE '%KIDDI FISH TUNA FLOSS%'
     OR PRODUCT_NAME LIKE '%KIDDI FISH SALMON FLOSS%'
     OR PRODUCT_NAME LIKE '%PICNICBABY TASTYRICE SALMON%'
     OR PRODUCT_NAME LIKE '%PICNICBABYCHICKENLIVERBROCCOLI%'
     OR PRODUCT_NAME LIKE '%PICNICBABYRICEWITHCHICKEN%'
	 OR PRODUCT_NAME LIKE 'XONGDUR%')
;

-- MORE THAN 1 YEAR
UPDATE vpm_data.pm_skutagging_kids_product_master
SET INFANT_FLAG = 'N/A'
	,TODDLER_FLAG = 'YES'
	,TODDLER_KEYWORD = 'KID NOURISHMENT 1Y+'
WHERE MOM_AND_KID_PRODUCT_FLAG = 1
AND COALESCE(PREG_FLAG,'N/A') = 'N/A'
AND COALESCE(INFANT_FLAG,'N/A') = 'N/A'
AND COALESCE(TODDLER_FLAG,'N/A') = 'N/A'
AND COALESCE(PRESCHOOL_FLAG,'N/A') = 'N/A'
AND COALESCE(JUNIOR_FLAG,'N/A') = 'N/A'
AND COALESCE(TEEN_FLAG,'N/A') = 'N/A'
AND REVISED_CATEGORY_LEVEL_3 = 'KID NOURISHMENT'
AND (PRODUCT_NAME LIKE '%APPLE MONKEY BLUEBERYBANANA%'
     OR PRODUCT_NAME LIKE '%APPLE MONKEY CAROB BANANA%'
     OR PRODUCT_NAME LIKE '%BE DELIGHT RICE YOGURTBERRY%'
     OR PRODUCT_NAME LIKE '%BE DELIGHT BEETROOT&BANANA%'
     OR PRODUCT_NAME LIKE '%BE DELIGHT RICE STRW&BANANA%'
     OR PRODUCT_NAME LIKE 'NESTLE CERELAC%'
     OR PRODUCT_NAME LIKE '%FOODIE PLUS SALMON ORIGINAL%'
     OR PRODUCT_NAME LIKE '%FOODIESALMONFLOSSSESAMESEAWEED%'
     OR PRODUCT_NAME LIKE 'GREENDAY%'
	 OR PRODUCT_NAME LIKE '%HOORAY UDON CARROT CHICKEN%'
	 OR PRODUCT_NAME LIKE '%KIDDIFISHFRIEDSALMONFLOSS%'
	 OR PRODUCT_NAME LIKE '%KIDDIFISHSALMONSESAMELAVER%'
	 OR PRODUCT_NAME LIKE '%KIDDI FISH TUNA FLOSS%'
	 OR PRODUCT_NAME LIKE '%KIDDI FISH SALMON FLOSS%'
	 OR PRODUCT_NAME LIKE '%KIDDIFISHSALMONSESAMELAVER%'
	 OR PRODUCT_NAME LIKE '%NESTLE CERE%GROW%'
	 OR PRODUCT_NAME LIKE '%PEACHY%COOKIES%'
	 OR PRODUCT_NAME LIKE '%SWEET PEA DRIED PRUNE%'
	 OR CLEANED_BRANDNAME LIKE '%WEL B%')
;

------------------------------------------- KID FURNITURE -------------------------------------------
------------------ KID CAR SEAT AND TRAVEL GEAR ------------------
-- ������ 
UPDATE vpm_data.pm_skutagging_kids_product_master
SET "STAGE_M+0_TO_M+3" = NULL
	,"STAGE_M+3_TO_M+6" = NULL
	,"STAGE_M+6_TO_M+9" = NULL
	,"STAGE_M+9_TO_M+12" = NULL
	,TODDLER_FLAG = NULL
	,INFANT_FLAG = 'YES'
	,UNKNOWN_INFANT_SUBSTAGE = 'YES'
	,INFANT_UNKNOWN_KEYWORD = 'BABY CARRIER'
	,TODDLER_KEYWORD = 'BABY CARRIER'
WHERE MOM_AND_KID_PRODUCT_FLAG = 1
AND COALESCE(PREG_FLAG,'N/A') = 'N/A'
AND COALESCE(INFANT_FLAG,'N/A') = 'N/A'
AND COALESCE(TODDLER_FLAG,'N/A') = 'N/A'
AND COALESCE(PRESCHOOL_FLAG,'N/A') = 'N/A'
AND COALESCE(JUNIOR_FLAG,'N/A') = 'N/A'
AND COALESCE(TEEN_FLAG,'N/A') = 'N/A'
AND REVISED_CATEGORY_LEVEL_2 IN ('KID FURNITURE','KID ACCESSORIES','KID FASHION','TOYS')
AND (PRODUCT_NAME LIKE '%������%'
	OR PRODUCT_NAME LIKE '%�����%'
	OR PRODUCT_NAME LIKE '%BABY%CARRIER%')
;

------------------ KID BEDDING ------------------
/* BEDDING FOR 0-3 MONTHS */
UPDATE vpm_data.pm_skutagging_kids_product_master
SET "STAGE_M+0_TO_M+3" = 'YES'
	,"STAGE_M+3_TO_M+6" = 'N/A'
	,"STAGE_M+6_TO_M+9" = 'N/A'
	,"STAGE_M+9_TO_M+12" = 'N/A'
	,INFANT_FLAG = 'YES'
	,UNKNOWN_INFANT_SUBSTAGE = 'N/A'
	,TODDLER_FLAG = 'N/A'
	,INFANT_0_3M_KEYWORD = (CASE WHEN PRODUCT_NAME LIKE '%BOUNCER%' THEN 'BABY BOUNCER'
								WHEN PRODUCT_NAME LIKE '%ROCK%' THEN 'BABY BOUNCER'
								WHEN PRODUCT_NAME LIKE '%SWING%' THEN 'BABY BOUNCER'
								WHEN PRODUCT_NAME LIKE '%���¡%' THEN 'BABY BOUNCER'
								WHEN PRODUCT_NAME LIKE '%����%' THEN 'BABY BOUNCER'
								WHEN PRODUCT_NAME LIKE '%�Ԫ�����ë��Ź͹%' THEN 'BABY BOUNCER'
								ELSE INFANT_0_3M_KEYWORD END)
WHERE MOM_AND_KID_PRODUCT_FLAG = 1
AND COALESCE(PREG_FLAG,'N/A') = 'N/A'
AND COALESCE(INFANT_FLAG,'N/A') = 'N/A'
AND COALESCE(TODDLER_FLAG,'N/A') = 'N/A'
AND COALESCE(PRESCHOOL_FLAG,'N/A') = 'N/A'
AND COALESCE(JUNIOR_FLAG,'N/A') = 'N/A'
AND COALESCE(TEEN_FLAG,'N/A') = 'N/A'
AND REVISED_CATEGORY_LEVEL_2 = 'KID FURNITURE'
AND (PRODUCT_NAME LIKE '%BOUNCER%'
	OR PRODUCT_NAME LIKE '%ROCK%'
	OR PRODUCT_NAME LIKE '%SWING%'
	OR PRODUCT_NAME LIKE '%���¡%'
	OR PRODUCT_NAME LIKE '%����%'
	OR PRODUCT_NAME LIKE '%�Ԫ�����ë��Ź͹%')
;

/* BEDDING FOR 0-6 MONTHS */
UPDATE vpm_data.pm_skutagging_kids_product_master
SET "STAGE_M+0_TO_M+3" = 'YES'
	,"STAGE_M+3_TO_M+6" = 'YES'
	,"STAGE_M+6_TO_M+9" = 'N/A'
	,"STAGE_M+9_TO_M+12" = 'N/A'
	,INFANT_FLAG = 'YES'
	,UNKNOWN_INFANT_SUBSTAGE = 'N/A'
	,TODDLER_FLAG = 'N/A'
	,INFANT_0_3M_KEYWORD = (CASE WHEN PRODUCT_NAME LIKE '%0 6M%' THEN '0-6 MTH'
								WHEN PRODUCT_NAME LIKE '%��͹����%' THEN 'BABY HEAD SUPPORT'
								WHEN PRODUCT_NAME LIKE '%��ЫѾ���������%' THEN 'BABY HEAD SUPPORT'
								WHEN PRODUCT_NAME LIKE '%LOVENEST%' THEN 'BABY HEAD SUPPORT'
								WHEN PRODUCT_NAME LIKE '%CRADLE%' THEN 'CRADLE'
								WHEN PRODUCT_NAME LIKE '%SNUGGLE%NEST%' THEN 'SNUGGLE NEST'
								ELSE INFANT_0_3M_KEYWORD END)
	,INFANT_4_6M_KEYWORD = (CASE WHEN PRODUCT_NAME LIKE '%0 6M%' THEN '0-6 MTH'
								WHEN PRODUCT_NAME LIKE '%��͹����%' THEN 'BABY HEAD SUPPORT'
								WHEN PRODUCT_NAME LIKE '%��ЫѾ���������%' THEN 'BABY HEAD SUPPORT'
								WHEN PRODUCT_NAME LIKE '%LOVENEST%' THEN 'BABY HEAD SUPPORT'
								WHEN PRODUCT_NAME LIKE '%CRADLE%' THEN 'CRADLE'
								WHEN PRODUCT_NAME LIKE '%SNUGGLE%NEST%' THEN 'SNUGGLE NEST'
								ELSE INFANT_4_6M_KEYWORD END)
WHERE MOM_AND_KID_PRODUCT_FLAG = 1
AND COALESCE(PREG_FLAG,'N/A') = 'N/A'
AND COALESCE(INFANT_FLAG,'N/A') = 'N/A'
AND COALESCE(TODDLER_FLAG,'N/A') = 'N/A'
AND COALESCE(PRESCHOOL_FLAG,'N/A') = 'N/A'
AND COALESCE(JUNIOR_FLAG,'N/A') = 'N/A'
AND COALESCE(TEEN_FLAG,'N/A') = 'N/A'
AND REVISED_CATEGORY_LEVEL_2 = 'KID FURNITURE'
AND (PRODUCT_NAME LIKE '%��͹����%'
	OR PRODUCT_NAME LIKE '%CRADLE%'
	OR PRODUCT_NAME LIKE '%LOVENEST%'
	OR PRODUCT_NAME LIKE '%SNUGGLE%NEST%'
	OR PRODUCT_NAME LIKE '%��ЫѾ���������%'
	OR PRODUCT_NAME LIKE '%��͹˹ع%0 6M%')
;

/* BEDDING FOR >3 MONTHS */
UPDATE vpm_data.pm_skutagging_kids_product_master
SET "STAGE_M+0_TO_M+3" = 'N/A'
	,"STAGE_M+3_TO_M+6" = 'YES'
	,"STAGE_M+6_TO_M+9" = 'YES'
	,"STAGE_M+9_TO_M+12" = 'YES'
	,INFANT_FLAG = 'YES'
	,UNKNOWN_INFANT_SUBSTAGE = 'N/A'
	,TODDLER_FLAG = 'YES'
	,INFANT_4_6M_KEYWORD = 'PLAYPEN'
	,INFANT_7_9M_KEYWORD = 'PLAYPEN'
	,INFANT_10_12M_KEYWORD = 'PLAYPEN'
	,TODDLER_KEYWORD = 'PLAYPEN'
WHERE MOM_AND_KID_PRODUCT_FLAG = 1
AND COALESCE(PREG_FLAG,'N/A') = 'N/A'
AND COALESCE(INFANT_FLAG,'N/A') = 'N/A'
AND COALESCE(TODDLER_FLAG,'N/A') = 'N/A'
AND COALESCE(PRESCHOOL_FLAG,'N/A') = 'N/A'
AND COALESCE(JUNIOR_FLAG,'N/A') = 'N/A'
AND COALESCE(TEEN_FLAG,'N/A') = 'N/A'
AND REVISED_CATEGORY_LEVEL_2 = 'KID FURNITURE'
AND (PRODUCT_NAME LIKE '%��ྐྵ%'
	OR PRODUCT_NAME LIKE '%��ྐྵ%'
	OR PRODUCT_NAME LIKE '%PLAYPEN%')
;

------------------ OTHER KID FURNITURE ------------------
--������ҹ����
UPDATE vpm_data.pm_skutagging_kids_product_master
SET "STAGE_M+0_TO_M+3" = 'N/A'
	,"STAGE_M+3_TO_M+6" = 'N/A'
	,"STAGE_M+6_TO_M+9" = 'YES'
	,"STAGE_M+9_TO_M+12" = 'YES'
	,INFANT_FLAG = 'YES'
	,UNKNOWN_INFANT_SUBSTAGE = 'N/A'
	,TODDLER_FLAG = 'YES'
	,INFANT_7_9M_KEYWORD = '������֡�ҹ'
	,INFANT_10_12M_KEYWORD = '������֡�ҹ'
	,TODDLER_KEYWORD = '������֡�ҹ'
WHERE MOM_AND_KID_PRODUCT_FLAG = 1
AND COALESCE(PREG_FLAG,'N/A') = 'N/A'
AND COALESCE(INFANT_FLAG,'N/A') = 'N/A'
AND COALESCE(TODDLER_FLAG,'N/A') = 'N/A'
AND COALESCE(PRESCHOOL_FLAG,'N/A') = 'N/A'
AND COALESCE(JUNIOR_FLAG,'N/A') = 'N/A'
AND COALESCE(TEEN_FLAG,'N/A') = 'N/A'
AND REVISED_CATEGORY_LEVEL_2 = 'KID FURNITURE'
AND (PRODUCT_NAME LIKE '%������ҹ����%' 
     OR PRODUCT_NAME LIKE '%������%�����%'
     OR PRODUCT_NAME LIKE '%������%����%'
     OR PRODUCT_NAME LIKE '%������֡�ҹ%')
;

--�������Ѵ���
UPDATE vpm_data.pm_skutagging_kids_product_master
SET "STAGE_M+0_TO_M+3" = 'N/A'
	,"STAGE_M+3_TO_M+6" = 'YES'
	,"STAGE_M+6_TO_M+9" = 'N/A'
	,"STAGE_M+9_TO_M+12" = 'N/A'
	,INFANT_FLAG = 'YES'
	,UNKNOWN_INFANT_SUBSTAGE = 'N/A'
	,TODDLER_FLAG = 'N/A'
	,INFANT_4_6M_KEYWORD = '�������Ѵ���'
WHERE MOM_AND_KID_PRODUCT_FLAG = 1
AND COALESCE(PREG_FLAG,'N/A') = 'N/A'
AND COALESCE(INFANT_FLAG,'N/A') = 'N/A'
AND COALESCE(TODDLER_FLAG,'N/A') = 'N/A'
AND COALESCE(PRESCHOOL_FLAG,'N/A') = 'N/A'
AND COALESCE(JUNIOR_FLAG,'N/A') = 'N/A'
AND COALESCE(TEEN_FLAG,'N/A') = 'N/A'
AND REVISED_CATEGORY_LEVEL_2 = 'KID FURNITURE'
AND PRODUCT_NAME LIKE '%�������Ѵ���%'
;

/* WALKER FOR 3-6 MONTHS */
UPDATE vpm_data.pm_skutagging_kids_product_master
SET "STAGE_M+0_TO_M+3" = 'N/A'
	,"STAGE_M+3_TO_M+6" = 'YES'
	,"STAGE_M+6_TO_M+9" = 'N/A'
	,"STAGE_M+9_TO_M+12" = 'N/A'
	,INFANT_FLAG = 'YES'
	,UNKNOWN_INFANT_SUBSTAGE = 'N/A'
	,TODDLER_FLAG = 'N/A'
	,INFANT_4_6M_KEYWORD = 'ö�Ѵ�Թ'
WHERE MOM_AND_KID_PRODUCT_FLAG = 1
AND COALESCE(PREG_FLAG,'N/A') = 'N/A'
AND COALESCE(INFANT_FLAG,'N/A') = 'N/A'
AND COALESCE(TODDLER_FLAG,'N/A') = 'N/A'
AND COALESCE(PRESCHOOL_FLAG,'N/A') = 'N/A'
AND COALESCE(JUNIOR_FLAG,'N/A') = 'N/A'
AND COALESCE(TEEN_FLAG,'N/A') = 'N/A'
AND REVISED_CATEGORY_LEVEL_2 = 'KID FURNITURE'
AND (PRODUCT_NAME LIKE '%ö%�Ѵ�Թ%'
    OR PRODUCT_NAME LIKE '%�Ѵ�Թ%'
	OR PRODUCT_NAME LIKE '%ö��ا�Թ%'
	OR PRODUCT_NAME LIKE '%ö��ا�׹%'
	OR PRODUCT_NAME LIKE '%WALKER%' AND PRODUCT_NAME NOT LIKE '%MUSICAL%WALKER%'
	OR PRODUCT_NAME LIKE '%ö%�Ԩ�����Ѵ�Թ%')
;

/* WALKER FOR >6 MONTHS */
UPDATE vpm_data.pm_skutagging_kids_product_master
SET "STAGE_M+0_TO_M+3" = 'N/A'
	,"STAGE_M+3_TO_M+6" = 'N/A'
	,"STAGE_M+6_TO_M+9" = 'YES'
	,"STAGE_M+9_TO_M+12" = 'YES'
	,INFANT_FLAG = 'YES'
	,UNKNOWN_INFANT_SUBSTAGE = 'N/A'
	,TODDLER_FLAG = 'YES'
	,INFANT_7_9M_KEYWORD = 'ö��ѡ�Թ'
	,INFANT_10_12M_KEYWORD = 'ö��ѡ�Թ'
	,TODDLER_KEYWORD = 'ö��ѡ�Թ'
WHERE MOM_AND_KID_PRODUCT_FLAG = 1
AND COALESCE(PREG_FLAG,'N/A') = 'N/A'
AND COALESCE(INFANT_FLAG,'N/A') = 'N/A'
AND COALESCE(TODDLER_FLAG,'N/A') = 'N/A'
AND COALESCE(PRESCHOOL_FLAG,'N/A') = 'N/A'
AND COALESCE(JUNIOR_FLAG,'N/A') = 'N/A'
AND COALESCE(TEEN_FLAG,'N/A') = 'N/A'
AND REVISED_CATEGORY_LEVEL_2 = 'KID FURNITURE'
AND (PRODUCT_NAME LIKE '%ö%��ѡ�Թ%'
	OR PRODUCT_NAME LIKE '%MUSICAL%WALKER%')
;

--���ⶹ
UPDATE vpm_data.pm_skutagging_kids_product_master
SET INFANT_FLAG = 'N/A'
	,TODDLER_FLAG = 'YES'
	,TODDLER_KEYWORD = '���ⶹ'
WHERE MOM_AND_KID_PRODUCT_FLAG = 1
AND COALESCE(PREG_FLAG,'N/A') = 'N/A'
AND COALESCE(INFANT_FLAG,'N/A') = 'N/A'
AND COALESCE(TODDLER_FLAG,'N/A') = 'N/A'
AND COALESCE(PRESCHOOL_FLAG,'N/A') = 'N/A'
AND COALESCE(JUNIOR_FLAG,'N/A') = 'N/A'
AND COALESCE(TEEN_FLAG,'N/A') = 'N/A'
AND REVISED_CATEGORY_LEVEL_2 = 'KID FURNITURE'
AND PRODUCT_NAME LIKE '%���ⶹ%'
;

--�ç�Ǵ�˧�͡
UPDATE vpm_data.pm_skutagging_kids_product_master
SET "STAGE_M+0_TO_M+3" = 'N/A'
	,"STAGE_M+3_TO_M+6" = 'YES'
	,"STAGE_M+6_TO_M+9" = 'YES'
	,"STAGE_M+9_TO_M+12" = 'YES'
	,INFANT_FLAG = 'YES'
	,UNKNOWN_INFANT_SUBSTAGE = 'N/A'
	,INFANT_4_6M_KEYWORD = '�ç�Ǵ�˧�͡'
	,INFANT_7_9M_KEYWORD = '�ç�Ǵ�˧�͡'
	,INFANT_10_12M_KEYWORD = '�ç�Ǵ�˧�͡'
WHERE MOM_AND_KID_PRODUCT_FLAG = 1
AND COALESCE(PREG_FLAG,'N/A') = 'N/A'
AND COALESCE(INFANT_FLAG,'N/A') = 'N/A'
AND COALESCE(TODDLER_FLAG,'N/A') = 'N/A'
AND COALESCE(PRESCHOOL_FLAG,'N/A') = 'N/A'
AND COALESCE(JUNIOR_FLAG,'N/A') = 'N/A'
AND COALESCE(TEEN_FLAG,'N/A') = 'N/A'
AND (PRODUCT_NAME LIKE '%�ç�Ǵ�˧�͡%'
	OR PRODUCT_NAME LIKE '%�����¡�紿ѹ����˧�͡%'
	OR PRODUCT_NAME LIKE '%FINGER TIP TOOTH BRUSH%')
;

--0-12M
UPDATE vpm_data.pm_skutagging_kids_product_master
SET "STAGE_M+0_TO_M+3" = 'YES'
	,"STAGE_M+3_TO_M+6" = 'YES'
	,"STAGE_M+6_TO_M+9" = 'YES'
	,"STAGE_M+9_TO_M+12" = 'YES'
	,INFANT_FLAG = 'YES'
	,UNKNOWN_INFANT_SUBSTAGE = 'N/A'
	,INFANT_0_3M_KEYWORD = '0-12 MTH'
	,INFANT_4_6M_KEYWORD = '0-12 MTH'
	,INFANT_7_9M_KEYWORD = '0-12 MTH'
	,INFANT_10_12M_KEYWORD = '0-12 MTH'
WHERE MOM_AND_KID_PRODUCT_FLAG = 1
AND COALESCE(PREG_FLAG,'N/A') = 'N/A'
AND COALESCE(INFANT_FLAG,'N/A') = 'N/A'
AND COALESCE(TODDLER_FLAG,'N/A') = 'N/A'
AND COALESCE(PRESCHOOL_FLAG,'N/A') = 'N/A'
AND COALESCE(JUNIOR_FLAG,'N/A') = 'N/A'
AND COALESCE(TEEN_FLAG,'N/A') = 'N/A'
AND REVISED_CATEGORY_LEVEL_2 = 'KID FURNITURE'
AND (PRODUCT_NAME LIKE '%0-12%')
;

------------------------------------------- KID ACCESSORIES -------------------------------------------
UPDATE vpm_data.pm_skutagging_kids_product_master
SET "STAGE_M+0_TO_M+3" = 'YES'
	,"STAGE_M+3_TO_M+6" = 'N/A'
	,"STAGE_M+6_TO_M+9" = 'N/A'
	,"STAGE_M+9_TO_M+12" = 'N/A'
	,INFANT_FLAG = 'YES'
	,UNKNOWN_INFANT_SUBSTAGE = 'N/A'
	,INFANT_0_3M_KEYWORD = (CASE WHEN PRODUCT_NAME LIKE '%0-2 M%' THEN '0-2 MTH'
								WHEN PRODUCT_NAME LIKE '%0-3M%' THEN '0-3 MTH'
								WHEN PRODUCT_NAME LIKE '%0-3 M%' THEN '0-3 MTH'
								WHEN PRODUCT_NAME LIKE '%�Ǵ%�ء�Թ�%' THEN '�ء�Թ�'
								ELSE INFANT_0_3M_KEYWORD END)
WHERE MOM_AND_KID_PRODUCT_FLAG = 1
AND COALESCE(PREG_FLAG,'N/A') = 'N/A'
AND COALESCE(INFANT_FLAG,'N/A') = 'N/A'
AND COALESCE(TODDLER_FLAG,'N/A') = 'N/A'
AND COALESCE(PRESCHOOL_FLAG,'N/A') = 'N/A'
AND COALESCE(JUNIOR_FLAG,'N/A') = 'N/A'
AND COALESCE(TEEN_FLAG,'N/A') = 'N/A'
AND REVISED_CATEGORY_LEVEL_2 = 'KID ACCESSORIES'
AND (PRODUCT_NAME LIKE '%0-2 M%'
	OR PRODUCT_NAME LIKE '%0-3M%'
	OR PRODUCT_NAME LIKE '%0-3 M%'
	OR PRODUCT_NAME LIKE '%�Ǵ%�ء�Թ�%')
;

/* KID ACCESSORIES FOR 0-6 MONTHS */
UPDATE vpm_data.pm_skutagging_kids_product_master
SET "STAGE_M+0_TO_M+3" = 'YES'
	,"STAGE_M+3_TO_M+6" = 'YES'
	,"STAGE_M+6_TO_M+9" = 'N/A'
	,"STAGE_M+9_TO_M+12" = 'N/A'
	,INFANT_FLAG = 'YES'
	,UNKNOWN_INFANT_SUBSTAGE = 'N/A'
	,INFANT_0_3M_KEYWORD = '0-6 MTH'
	,INFANT_4_6M_KEYWORD = '0-6 MTH'
WHERE MOM_AND_KID_PRODUCT_FLAG = 1
AND COALESCE(PREG_FLAG,'N/A') = 'N/A'
AND COALESCE(INFANT_FLAG,'N/A') = 'N/A'
AND COALESCE(TODDLER_FLAG,'N/A') = 'N/A'
AND COALESCE(PRESCHOOL_FLAG,'N/A') = 'N/A'
AND COALESCE(JUNIOR_FLAG,'N/A') = 'N/A'
AND COALESCE(TEEN_FLAG,'N/A') = 'N/A'
AND REVISED_CATEGORY_LEVEL_2 = 'KID ACCESSORIES'
AND (PRODUCT_NAME LIKE '%0-6M%'
	OR PRODUCT_NAME LIKE '%0-6 M%'
	OR PRODUCT_NAME LIKE '%�ء��͡%0-6%')
;

/* KID ACCESSORIES FOR 3-6 MONTHS */
UPDATE vpm_data.pm_skutagging_kids_product_master
SET "STAGE_M+0_TO_M+3" = 'N/A'
	,"STAGE_M+3_TO_M+6" = 'YES'
	,"STAGE_M+6_TO_M+9" = 'N/A'
	,"STAGE_M+9_TO_M+12" = 'N/A'
	,INFANT_FLAG = 'YES'
	,UNKNOWN_INFANT_SUBSTAGE = 'N/A'
	,INFANT_4_6M_KEYWORD = '3-6 MTH'
WHERE MOM_AND_KID_PRODUCT_FLAG = 1
AND COALESCE(PREG_FLAG,'N/A') = 'N/A'
AND COALESCE(INFANT_FLAG,'N/A') = 'N/A'
AND COALESCE(TODDLER_FLAG,'N/A') = 'N/A'
AND COALESCE(PRESCHOOL_FLAG,'N/A') = 'N/A'
AND COALESCE(JUNIOR_FLAG,'N/A') = 'N/A'
AND COALESCE(TEEN_FLAG,'N/A') = 'N/A'
AND REVISED_CATEGORY_LEVEL_2 = 'KID ACCESSORIES'
AND (PRODUCT_NAME LIKE '%3-6M%'
	OR PRODUCT_NAME LIKE '%3-6 M%')
;

/* KID ACCESSORIES FOR 6-9 MONTHS */
UPDATE vpm_data.pm_skutagging_kids_product_master
SET "STAGE_M+0_TO_M+3" = 'N/A'
	,"STAGE_M+3_TO_M+6" = 'N/A'
	,"STAGE_M+6_TO_M+9" = 'YES'
	,"STAGE_M+9_TO_M+12" = 'N/A'
	,INFANT_FLAG = 'YES'
	,UNKNOWN_INFANT_SUBSTAGE = 'N/A'
	,INFANT_7_9M_KEYWORD = '6-9 MTH'
WHERE MOM_AND_KID_PRODUCT_FLAG = 1
AND COALESCE(PREG_FLAG,'N/A') = 'N/A'
AND COALESCE(INFANT_FLAG,'N/A') = 'N/A'
AND COALESCE(TODDLER_FLAG,'N/A') = 'N/A'
AND COALESCE(PRESCHOOL_FLAG,'N/A') = 'N/A'
AND COALESCE(JUNIOR_FLAG,'N/A') = 'N/A'
AND COALESCE(TEEN_FLAG,'N/A') = 'N/A'
AND REVISED_CATEGORY_LEVEL_2 = 'KID ACCESSORIES'
AND (PRODUCT_NAME LIKE '%6-9M%'
	OR PRODUCT_NAME LIKE '%6-9 M%')
;

------------------ FEEDING BOTTLE AND ACCESSORIES ------------------
/* KID ACCESSORIES FOR 3M+ */
UPDATE vpm_data.pm_skutagging_kids_product_master
SET "STAGE_M+0_TO_M+3" = 'N/A'
	,"STAGE_M+3_TO_M+6" = 'YES'
	,"STAGE_M+6_TO_M+9" = 'YES'
	,"STAGE_M+9_TO_M+12" = 'YES'
	,INFANT_FLAG = 'YES'
	,UNKNOWN_INFANT_SUBSTAGE = 'N/A'
	,TODDLER_FLAG = 'YES'
	,INFANT_4_6M_KEYWORD = (CASE WHEN PRODUCT_NAME LIKE '%�Ǵ��%M%' THEN '�Ǵ��'
								WHEN PRODUCT_NAME LIKE '%�ء��%M%' THEN '�ء��'
								WHEN PRODUCT_NAME LIKE '%�Ǵ%240%' THEN '�Ǵ��'
								WHEN PRODUCT_NAME LIKE '%�ء������͹%M%' THEN '�ء��'
								WHEN PRODUCT_NAME LIKE '%�Ǵ��%240%' THEN '�Ǵ��'
								WHEN PRODUCT_NAME LIKE '%�Ǵ����͹%M%' THEN '�Ǵ��'
								WHEN PRODUCT_NAME LIKE '%�Ǵ%8�͹��%' THEN '�Ǵ��'
								ELSE INFANT_4_6M_KEYWORD END)
	,INFANT_7_9M_KEYWORD = (CASE WHEN PRODUCT_NAME LIKE '%�Ǵ��%M%' THEN '�Ǵ��'
								WHEN PRODUCT_NAME LIKE '%�ء��%M%' THEN '�ء��'
								WHEN PRODUCT_NAME LIKE '%�Ǵ%240%' THEN '�Ǵ��'
								WHEN PRODUCT_NAME LIKE '%�ء������͹%M%' THEN '�ء��'
								WHEN PRODUCT_NAME LIKE '%�Ǵ��%240%' THEN '�Ǵ��'
								WHEN PRODUCT_NAME LIKE '%�Ǵ����͹%M%' THEN '�Ǵ��'
								WHEN PRODUCT_NAME LIKE '%�Ǵ%8�͹��%' THEN '�Ǵ��'
								ELSE INFANT_7_9M_KEYWORD END)
	,INFANT_10_12M_KEYWORD = (CASE WHEN PRODUCT_NAME LIKE '%�Ǵ��%M%' THEN '�Ǵ��'
								WHEN PRODUCT_NAME LIKE '%�ء��%M%' THEN '�ء��'
								WHEN PRODUCT_NAME LIKE '%�Ǵ%240%' THEN '�Ǵ��'
								WHEN PRODUCT_NAME LIKE '%�ء������͹%M%' THEN '�ء��'
								WHEN PRODUCT_NAME LIKE '%�Ǵ��%240%' THEN '�Ǵ��'
								WHEN PRODUCT_NAME LIKE '%�Ǵ����͹%M%' THEN '�Ǵ��'
								WHEN PRODUCT_NAME LIKE '%�Ǵ%8�͹��%' THEN '�Ǵ��'
								ELSE INFANT_10_12M_KEYWORD END)
	,TODDLER_KEYWORD = (CASE WHEN PRODUCT_NAME LIKE '%�Ǵ��%M%' THEN '�Ǵ��'
								WHEN PRODUCT_NAME LIKE '%�ء��%M%' THEN '�ء��'
								WHEN PRODUCT_NAME LIKE '%�Ǵ%240%' THEN '�Ǵ��'
								WHEN PRODUCT_NAME LIKE '%�ء������͹%M%' THEN '�ء��'
								WHEN PRODUCT_NAME LIKE '%�Ǵ��%240%' THEN '�Ǵ��'
								WHEN PRODUCT_NAME LIKE '%�Ǵ����͹%M%' THEN '�Ǵ��'
								WHEN PRODUCT_NAME LIKE '%�Ǵ%8�͹��%' THEN '�Ǵ��'
								ELSE TODDLER_KEYWORD END)
WHERE MOM_AND_KID_PRODUCT_FLAG = 1
AND COALESCE(PREG_FLAG,'N/A') = 'N/A'
AND COALESCE(INFANT_FLAG,'N/A') = 'N/A'
AND COALESCE(TODDLER_FLAG,'N/A') = 'N/A'
AND COALESCE(PRESCHOOL_FLAG,'N/A') = 'N/A'
AND COALESCE(JUNIOR_FLAG,'N/A') = 'N/A'
AND COALESCE(TEEN_FLAG,'N/A') = 'N/A'
AND REVISED_CATEGORY_LEVEL_3 = 'FEEDING BOTTLE AND ACCESSORIES'
AND (PRODUCT_NAME LIKE '%�Ǵ��%M%'
		OR (PRODUCT_NAME LIKE '%�ء��%M%' AND PRODUCT_NAME NOT LIKE '%0-6 M%')
		OR PRODUCT_NAME LIKE '%�Ǵ%240%'
		OR PRODUCT_NAME LIKE '%�ء������͹%M%'
		OR PRODUCT_NAME LIKE '%�Ǵ��%240%'
		OR PRODUCT_NAME LIKE '%�Ǵ����͹%M%'
		OR PRODUCT_NAME LIKE '%�Ǵ%8�͹��%')
;

/* KID ACCESSORIES FOR 6M+ */
UPDATE vpm_data.pm_skutagging_kids_product_master
SET "STAGE_M+0_TO_M+3" = 'N/A'
	,"STAGE_M+3_TO_M+6" = 'N/A'
	,"STAGE_M+6_TO_M+9" = 'YES'
	,"STAGE_M+9_TO_M+12" = 'YES'
	,INFANT_FLAG = 'YES'
	,UNKNOWN_INFANT_SUBSTAGE = 'N/A'
	,TODDLER_FLAG = 'YES'
	,INFANT_7_9M_KEYWORD = (CASE WHEN PRODUCT_NAME LIKE '%�ء����͹%L%' THEN '�ء�� L'
								WHEN PRODUCT_NAME LIKE '%�ء������͹%L%' THEN '�ء�� L'
								WHEN PRODUCT_NAME LIKE '%NATURAL%6M%' THEN '�ء�� 6M'
								WHEN PRODUCT_NAME LIKE '%NIPPLE%MINI%L%' THEN '�ء�� L'
								WHEN PRODUCT_NAME LIKE '%�ء��������%6 M%' THEN '�ء�� 6M'
								WHEN PRODUCT_NAME LIKE '%�Ǵ��%330%'  THEN '�Ǵ�� 330ML'
								WHEN PRODUCT_NAME LIKE '%NATURAL%330%' THEN '�Ǵ�� 330ML'
								WHEN PRODUCT_NAME LIKE '%NIPPLE%L%' THEN '�ء�� L'
								ELSE INFANT_7_9M_KEYWORD END)
	,INFANT_10_12M_KEYWORD = (CASE WHEN PRODUCT_NAME LIKE '%�ء����͹%L%' THEN '�ء�� L'
								WHEN PRODUCT_NAME LIKE '%�ء������͹%L%' THEN '�ء�� L'
								WHEN PRODUCT_NAME LIKE '%NATURAL%6M%' THEN '�ء�� 6M'
								WHEN PRODUCT_NAME LIKE '%NIPPLE%MINI%L%' THEN '�ء�� L'
								WHEN PRODUCT_NAME LIKE '%�ء��������%6 M%' THEN '�ء�� 6M'
								WHEN PRODUCT_NAME LIKE '%�Ǵ��%330%'  THEN '�Ǵ�� 330ML'
								WHEN PRODUCT_NAME LIKE '%NATURAL%330%' THEN '�Ǵ�� 330ML'
								WHEN PRODUCT_NAME LIKE '%NIPPLE%L%' THEN '�ء�� L'
								ELSE INFANT_10_12M_KEYWORD END)
	,TODDLER_KEYWORD = (CASE WHEN PRODUCT_NAME LIKE '%�ء����͹%L%' THEN '�ء�� L'
								WHEN PRODUCT_NAME LIKE '%�ء������͹%L%' THEN '�ء�� L'
								WHEN PRODUCT_NAME LIKE '%NATURAL%6M%' THEN '�ء�� 6M'
								WHEN PRODUCT_NAME LIKE '%NIPPLE%MINI%L%' THEN '�ء�� L'
								WHEN PRODUCT_NAME LIKE '%�ء��������%6 M%' THEN '�ء�� 6M'
								WHEN PRODUCT_NAME LIKE '%�Ǵ��%330%'  THEN '�Ǵ�� 330ML'
								WHEN PRODUCT_NAME LIKE '%NATURAL%330%' THEN '�Ǵ�� 330ML'
								WHEN PRODUCT_NAME LIKE '%NIPPLE%L%' THEN '�ء�� L'
								ELSE TODDLER_KEYWORD END)
WHERE MOM_AND_KID_PRODUCT_FLAG = 1
AND COALESCE(PREG_FLAG,'N/A') = 'N/A'
AND COALESCE(INFANT_FLAG,'N/A') = 'N/A'
AND COALESCE(TODDLER_FLAG,'N/A') = 'N/A'
AND COALESCE(PRESCHOOL_FLAG,'N/A') = 'N/A'
AND COALESCE(JUNIOR_FLAG,'N/A') = 'N/A'
AND COALESCE(TEEN_FLAG,'N/A') = 'N/A'
AND REVISED_CATEGORY_LEVEL_3 = 'FEEDING BOTTLE AND ACCESSORIES'
AND (PRODUCT_NAME LIKE '%�ء����͹%L%'
		OR PRODUCT_NAME LIKE '%�ء������͹%L%'
		OR PRODUCT_NAME LIKE '%NATURAL%6M%'
		OR PRODUCT_NAME LIKE '%NIPPLE%MINI%L%'
		OR PRODUCT_NAME LIKE '%�ء��������%6 M%'
		OR PRODUCT_NAME LIKE '%�Ǵ��%330%'
		OR PRODUCT_NAME LIKE '%NATURAL%330%'
		OR PRODUCT_NAME LIKE '%NIPPLE%L%')
;

/* KID ACCESSORIES FOR 9M+ */
UPDATE vpm_data.pm_skutagging_kids_product_master
SET "STAGE_M+0_TO_M+3" = 'N/A'
	,"STAGE_M+3_TO_M+6" = 'N/A'
	,"STAGE_M+6_TO_M+9" = 'N/A'
	,"STAGE_M+9_TO_M+12" = 'YES'
	,INFANT_FLAG = 'YES'
	,UNKNOWN_INFANT_SUBSTAGE = 'N/A'
	,TODDLER_FLAG = 'YES'
	,INFANT_10_12M_KEYWORD = (CASE WHEN PRODUCT_NAME LIKE '%�ء��%L%' THEN '�ء�� LL'
								WHEN PRODUCT_NAME LIKE '%�ء����͹%LL%' THEN '�ء�� LL'
								WHEN PRODUCT_NAME LIKE '%�ء������͹%LL%' THEN '�ء�� LL'
								WHEN PRODUCT_NAME LIKE '%NIPPLE%LL%' THEN '�ء�� LL'
								WHEN PRODUCT_NAME LIKE '%NATURAL%9M%' THEN '�ء�� 9M'
								ELSE INFANT_10_12M_KEYWORD END)
	,TODDLER_KEYWORD = (CASE WHEN PRODUCT_NAME LIKE '%�ء��%L%' THEN '�ء�� LL'
								WHEN PRODUCT_NAME LIKE '%�ء����͹%LL%' THEN '�ء�� LL'
								WHEN PRODUCT_NAME LIKE '%�ء������͹%LL%' THEN '�ء�� LL'
								WHEN PRODUCT_NAME LIKE '%NIPPLE%LL%' THEN '�ء�� LL'
								WHEN PRODUCT_NAME LIKE '%NATURAL%9M%' THEN '�ء�� 9M'
								ELSE TODDLER_KEYWORD END)
WHERE MOM_AND_KID_PRODUCT_FLAG = 1
AND COALESCE(PREG_FLAG,'N/A') = 'N/A'
AND COALESCE(INFANT_FLAG,'N/A') = 'N/A'
AND COALESCE(TODDLER_FLAG,'N/A') = 'N/A'
AND COALESCE(PRESCHOOL_FLAG,'N/A') = 'N/A'
AND COALESCE(JUNIOR_FLAG,'N/A') = 'N/A'
AND COALESCE(TEEN_FLAG,'N/A') = 'N/A'
AND REVISED_CATEGORY_LEVEL_3 = 'FEEDING BOTTLE AND ACCESSORIES'
AND (PRODUCT_NAME LIKE '%�ء��%L%'
		OR PRODUCT_NAME LIKE '%�ء����͹%LL%'
		OR PRODUCT_NAME LIKE '%�ء������͹%LL%'
		OR PRODUCT_NAME LIKE '%NIPPLE%LL%'
		OR PRODUCT_NAME LIKE '%NATURAL%9M%')
;

------------------ KID BATHROOM ACCESSORIES ------------------
--���ⶹ
UPDATE vpm_data.pm_skutagging_kids_product_master
SET INFANT_FLAG = 'N/A'
	,TODDLER_FLAG = 'YES'
	,TODDLER_KEYWORD = '���ⶹ'
WHERE MOM_AND_KID_PRODUCT_FLAG = 1
AND COALESCE(PREG_FLAG,'N/A') = 'N/A'
AND COALESCE(INFANT_FLAG,'N/A') = 'N/A'
AND COALESCE(TODDLER_FLAG,'N/A') = 'N/A'
AND COALESCE(PRESCHOOL_FLAG,'N/A') = 'N/A'
AND COALESCE(JUNIOR_FLAG,'N/A') = 'N/A'
AND COALESCE(TEEN_FLAG,'N/A') = 'N/A'
AND REVISED_CATEGORY_LEVEL_3 = 'KID BATHROOM ACCESSORIES'
AND (PRODUCT_NAME LIKE '%ⶽ֡���%'
	OR PRODUCT_NAME LIKE '%���ⶹ%')
;

--OTHERS 1Y ++
UPDATE vpm_data.pm_skutagging_kids_product_master
SET INFANT_FLAG = 'N/A'
	,TODDLER_FLAG = 'YES'
	,TODDLER_KEYWORD = (CASE WHEN PRODUCT_NAME LIKE '%12��͹%' THEN '12M+'
									WHEN PRODUCT_NAME LIKE '%1-4%' THEN '1-4Y'
									WHEN PRODUCT_NAME LIKE '%�������ҧ���15M%' THEN '15M+'
									WHEN PRODUCT_NAME LIKE '%18-36 MONTHS%' THEN '18-36 MTH'
									WHEN PRODUCT_NAME LIKE '%12 ��͹%' THEN '12M+'
									WHEN PRODUCT_NAME LIKE '%12 M%' THEN '12M+'
									ELSE TODDLER_KEYWORD END)
WHERE MOM_AND_KID_PRODUCT_FLAG = 1
AND COALESCE(PREG_FLAG,'N/A') = 'N/A'
AND COALESCE(INFANT_FLAG,'N/A') = 'N/A'
AND COALESCE(TODDLER_FLAG,'N/A') = 'N/A'
AND COALESCE(PRESCHOOL_FLAG,'N/A') = 'N/A'
AND COALESCE(JUNIOR_FLAG,'N/A') = 'N/A'
AND COALESCE(TEEN_FLAG,'N/A') = 'N/A'
AND REVISED_CATEGORY_LEVEL_3 = 'KID BATHROOM ACCESSORIES'
AND (PRODUCT_NAME LIKE '%12��͹%'
		OR PRODUCT_NAME LIKE '%1-4%'
		OR PRODUCT_NAME LIKE '%�������ҧ���15M%'
		OR PRODUCT_NAME LIKE '%18-36 MONTHS%'
		OR PRODUCT_NAME LIKE '%12 ��͹%'
		OR PRODUCT_NAME LIKE '%12 M%')
;

--�ç�տѹ�Ǵ�˧�͡ �ç�տѹ����⤹
UPDATE vpm_data.pm_skutagging_kids_product_master
SET "STAGE_M+0_TO_M+3" = 'N/A'
	,"STAGE_M+3_TO_M+6" = 'YES'
	,"STAGE_M+6_TO_M+9" = 'YES'
	,"STAGE_M+9_TO_M+12" = 'YES'
	,INFANT_FLAG = 'YES'
	,UNKNOWN_INFANT_SUBSTAGE = 'N/A'
	,INFANT_4_6M_KEYWORD = (CASE WHEN PRODUCT_NAME LIKE '%�ç�տѹ�Ǵ�˧�͡%' THEN '�ç�Ǵ�˧�͡'
								WHEN PRODUCT_NAME LIKE '%�ç����⤹%' THEN '�ç�Ǵ�˧�͡'
								WHEN PRODUCT_NAME LIKE '%PLAYPEN NETTING%' THEN 'PLAYPEN'
								WHEN PRODUCT_NAME LIKE '%�ç�տѹ���Ԥ͹%' THEN '�ç�Ǵ�˧�͡'
								WHEN PRODUCT_NAME LIKE '%�ç�Ǵ�˧�͡%' THEN '�ç�Ǵ�˧�͡'
								WHEN PRODUCT_NAME LIKE '%�Ǵ�˧�͡%' THEN '�ç�Ǵ�˧�͡'
								ELSE INFANT_4_6M_KEYWORD END)
	,INFANT_7_9M_KEYWORD = (CASE WHEN PRODUCT_NAME LIKE '%�ç�տѹ�Ǵ�˧�͡%' THEN '�ç�Ǵ�˧�͡'
								WHEN PRODUCT_NAME LIKE '%�ç����⤹%' THEN '�ç�Ǵ�˧�͡'
								WHEN PRODUCT_NAME LIKE '%PLAYPEN NETTING%' THEN 'PLAYPEN'
								WHEN PRODUCT_NAME LIKE '%�ç�տѹ���Ԥ͹%' THEN '�ç�Ǵ�˧�͡'
								WHEN PRODUCT_NAME LIKE '%�ç�Ǵ�˧�͡%' THEN '�ç�Ǵ�˧�͡'
								WHEN PRODUCT_NAME LIKE '%�Ǵ�˧�͡%' THEN '�ç�Ǵ�˧�͡'
								ELSE INFANT_7_9M_KEYWORD END)
	,INFANT_10_12M_KEYWORD = (CASE WHEN PRODUCT_NAME LIKE '%�ç�տѹ�Ǵ�˧�͡%' THEN '�ç�Ǵ�˧�͡'
								WHEN PRODUCT_NAME LIKE '%�ç����⤹%' THEN '�ç�Ǵ�˧�͡'
								WHEN PRODUCT_NAME LIKE '%PLAYPEN NETTING%' THEN 'PLAYPEN'
								WHEN PRODUCT_NAME LIKE '%�ç�տѹ���Ԥ͹%' THEN '�ç�Ǵ�˧�͡'
								WHEN PRODUCT_NAME LIKE '%�ç�Ǵ�˧�͡%' THEN '�ç�Ǵ�˧�͡'
								WHEN PRODUCT_NAME LIKE '%�Ǵ�˧�͡%' THEN '�ç�Ǵ�˧�͡'
								ELSE INFANT_10_12M_KEYWORD END)
WHERE MOM_AND_KID_PRODUCT_FLAG = 1
AND COALESCE(PREG_FLAG,'N/A') = 'N/A'
AND COALESCE(INFANT_FLAG,'N/A') = 'N/A'
AND COALESCE(TODDLER_FLAG,'N/A') = 'N/A'
AND COALESCE(PRESCHOOL_FLAG,'N/A') = 'N/A'
AND COALESCE(JUNIOR_FLAG,'N/A') = 'N/A'
AND COALESCE(TEEN_FLAG,'N/A') = 'N/A'
AND REVISED_CATEGORY_LEVEL_3 = 'KID BATHROOM ACCESSORIES'
AND (PRODUCT_NAME LIKE '%�ç�տѹ�Ǵ�˧�͡%'
		OR PRODUCT_NAME LIKE '%�ç����⤹%'
		OR PRODUCT_NAME LIKE '%PLAYPEN NETTING%'
		OR PRODUCT_NAME LIKE '%�ç�տѹ���Ԥ͹%'
		OR PRODUCT_NAME LIKE '%�ç�Ǵ�˧�͡%'
		OR PRODUCT_NAME LIKE '%�Ǵ�˧�͡%')
;

--3M+
UPDATE vpm_data.pm_skutagging_kids_product_master
SET "STAGE_M+0_TO_M+3" = 'N/A'
	,"STAGE_M+3_TO_M+6" = 'YES'
	,"STAGE_M+6_TO_M+9" = 'YES'
	,"STAGE_M+9_TO_M+12" = 'YES'
	,INFANT_FLAG = 'YES'
	,UNKNOWN_INFANT_SUBSTAGE = 'N/A'
	,TODDLER_FLAG = 'YES'
	,INFANT_4_6M_KEYWORD = '3M+'
	,INFANT_7_9M_KEYWORD = '3M+'
	,INFANT_10_12M_KEYWORD = '3M+'
	,TODDLER_KEYWORD = '3M+'
WHERE MOM_AND_KID_PRODUCT_FLAG = 1
AND COALESCE(PREG_FLAG,'N/A') = 'N/A'
AND COALESCE(INFANT_FLAG,'N/A') = 'N/A'
AND COALESCE(TODDLER_FLAG,'N/A') = 'N/A'
AND COALESCE(PRESCHOOL_FLAG,'N/A') = 'N/A'
AND COALESCE(JUNIOR_FLAG,'N/A') = 'N/A'
AND COALESCE(TEEN_FLAG,'N/A') = 'N/A'
AND REVISED_CATEGORY_LEVEL_3 = 'KID BATHROOM ACCESSORIES'
AND (PRODUCT_NAME LIKE '%3��͹����%')
;

--�ç�տѹ 6M++
UPDATE vpm_data.pm_skutagging_kids_product_master
SET "STAGE_M+0_TO_M+3" = 'N/A'
	,"STAGE_M+3_TO_M+6" = 'N/A'
	,"STAGE_M+6_TO_M+9" = 'YES'
	,"STAGE_M+9_TO_M+12" = 'YES'
	,INFANT_FLAG = 'YES'
	,UNKNOWN_INFANT_SUBSTAGE = 'N/A'
	,TODDLER_FLAG = 'YES'
	,INFANT_7_9M_KEYWORD = (CASE WHEN PRODUCT_NAME LIKE '%STEP1%' THEN 'STEP 1'
								WHEN PRODUCT_NAME LIKE '%TRAINING TOOTHBRUSH%' THEN 'TRAINING TOOTHBRUSH'
								WHEN PRODUCT_NAME LIKE '%6M%' THEN '6M+'
								WHEN PRODUCT_NAME LIKE '%STEP 1%' THEN 'STEP 1'
								WHEN PRODUCT_NAME LIKE '%��鹷��1%' THEN 'STEP 1'
								WHEN PRODUCT_NAME LIKE '%�������1%' THEN 'STEP 1'
								WHEN PRODUCT_NAME LIKE '%6-18 MONTHS%' THEN '6-18 MTH'
								WHEN PRODUCT_NAME LIKE '%������� 1%' THEN 'STEP 1'
								WHEN PRODUCT_NAME LIKE '%6 ��͹%' THEN '6M+'
								WHEN PRODUCT_NAME LIKE '%6 M%' THEN '6M+'
								ELSE INFANT_7_9M_KEYWORD END)
	,INFANT_10_12M_KEYWORD = (CASE WHEN PRODUCT_NAME LIKE '%STEP1%' THEN 'STEP 1'
								WHEN PRODUCT_NAME LIKE '%TRAINING TOOTHBRUSH%' THEN 'TRAINING TOOTHBRUSH'
								WHEN PRODUCT_NAME LIKE '%6M%' THEN '6M+'
								WHEN PRODUCT_NAME LIKE '%STEP 1%' THEN 'STEP 1'
								WHEN PRODUCT_NAME LIKE '%��鹷��1%' THEN 'STEP 1'
								WHEN PRODUCT_NAME LIKE '%�������1%' THEN 'STEP 1'
								WHEN PRODUCT_NAME LIKE '%6-18 MONTHS%' THEN '6-18 MTH'
								WHEN PRODUCT_NAME LIKE '%������� 1%' THEN 'STEP 1'
								WHEN PRODUCT_NAME LIKE '%6 ��͹%' THEN '6M+'
								WHEN PRODUCT_NAME LIKE '%6 M%' THEN '6M+'
								ELSE INFANT_10_12M_KEYWORD END)
	,TODDLER_KEYWORD = (CASE WHEN PRODUCT_NAME LIKE '%STEP1%' THEN 'STEP 1'
								WHEN PRODUCT_NAME LIKE '%TRAINING TOOTHBRUSH%' THEN 'TRAINING TOOTHBRUSH'
								WHEN PRODUCT_NAME LIKE '%6M%' THEN '6M+'
								WHEN PRODUCT_NAME LIKE '%STEP 1%' THEN 'STEP 1'
								WHEN PRODUCT_NAME LIKE '%��鹷��1%' THEN 'STEP 1'
								WHEN PRODUCT_NAME LIKE '%�������1%' THEN 'STEP 1'
								WHEN PRODUCT_NAME LIKE '%6-18 MONTHS%' THEN '6-18 MTH'
								WHEN PRODUCT_NAME LIKE '%������� 1%' THEN 'STEP 1'
								WHEN PRODUCT_NAME LIKE '%6 ��͹%' THEN '6M+'
								WHEN PRODUCT_NAME LIKE '%6 M%' THEN '6M+'
								ELSE TODDLER_KEYWORD END)
WHERE MOM_AND_KID_PRODUCT_FLAG = 1
AND COALESCE(PREG_FLAG,'N/A') = 'N/A'
AND COALESCE(INFANT_FLAG,'N/A') = 'N/A'
AND COALESCE(TODDLER_FLAG,'N/A') = 'N/A'
AND COALESCE(PRESCHOOL_FLAG,'N/A') = 'N/A'
AND COALESCE(JUNIOR_FLAG,'N/A') = 'N/A'
AND COALESCE(TEEN_FLAG,'N/A') = 'N/A'
AND REVISED_CATEGORY_LEVEL_3 = 'KID BATHROOM ACCESSORIES'
AND (PRODUCT_NAME LIKE '%STEP1%'
		OR PRODUCT_NAME LIKE '%TRAINING TOOTHBRUSH%'
		OR PRODUCT_NAME LIKE '%6M%'
		OR PRODUCT_NAME LIKE '%STEP 1%'
		OR PRODUCT_NAME LIKE '%��鹷��1%'
		OR PRODUCT_NAME LIKE '%�������1%'
		OR PRODUCT_NAME LIKE '%6-18 MONTHS%'
		OR PRODUCT_NAME LIKE '%������� 1%'
		OR PRODUCT_NAME LIKE '%6 ��͹%'
		OR PRODUCT_NAME LIKE '%6 M%')
;

------------------ OTHER KID ACCESSORIES ------------------
/* SOFT FOOD (MASH, CHOP) AND FOR 6-12 MONTHS */
UPDATE vpm_data.pm_skutagging_kids_product_master
SET "STAGE_M+0_TO_M+3" = 'N/A'
	,"STAGE_M+3_TO_M+6" = 'N/A'
	,"STAGE_M+6_TO_M+9" = 'YES'
	,"STAGE_M+9_TO_M+12" = 'YES'
	,INFANT_FLAG = 'YES'
	,UNKNOWN_INFANT_SUBSTAGE = 'N/A'
	,INFANT_7_9M_KEYWORD = (CASE WHEN PRODUCT_NAME LIKE '%�Ѵ����%' THEN '�����Ѵ����'
								WHEN PRODUCT_NAME LIKE '%���½֡�ٴ%' THEN '�����Ѵ����'
								WHEN PRODUCT_NAME LIKE '%�Ѳ�ҡ�ôٴ%' THEN '�����Ѵ����'
								WHEN PRODUCT_NAME LIKE '%TRANSITION%CUP%' THEN '�����Ѵ����'
								WHEN PRODUCT_NAME LIKE '%SIPPY CUP%' THEN '�����Ѵ����'
								WHEN PRODUCT_NAME LIKE '%���«Ի����%' THEN '�����Ѵ����'
								WHEN PRODUCT_NAME LIKE '%TRAINER CUP%' THEN '�����Ѵ����'
								WHEN PRODUCT_NAME LIKE '%STRAW%CUP%' THEN 'STRAW BOTTLE'
								WHEN PRODUCT_NAME LIKE '%STRAW%BOTTLE%' THEN 'STRAW BOTTLE'
								WHEN PRODUCT_NAME LIKE '%���%��ʹ�ٴ%' THEN 'STRAW BOTTLE'
								WHEN PRODUCT_NAME LIKE '%�����ʹ%' THEN 'STRAW BOTTLE�'
								WHEN PRODUCT_NAME LIKE '%���������%' THEN '���������'
								WHEN PRODUCT_NAME LIKE '%���%��%' THEN '�����'
								WHEN PRODUCT_NAME LIKE '%��麴%' THEN '��麴'
								ELSE INFANT_7_9M_KEYWORD END)
	,INFANT_10_12M_KEYWORD = (CASE WHEN PRODUCT_NAME LIKE '%�Ѵ����%' THEN '�����Ѵ����'
								WHEN PRODUCT_NAME LIKE '%���½֡�ٴ%' THEN '�����Ѵ����'
								WHEN PRODUCT_NAME LIKE '%�Ѳ�ҡ�ôٴ%' THEN '�����Ѵ����'
								WHEN PRODUCT_NAME LIKE '%TRANSITION%CUP%' THEN '�����Ѵ����'
								WHEN PRODUCT_NAME LIKE '%SIPPY CUP%' THEN '�����Ѵ����'
								WHEN PRODUCT_NAME LIKE '%���«Ի����%' THEN '�����Ѵ����'
								WHEN PRODUCT_NAME LIKE '%TRAINER CUP%' THEN '�����Ѵ����'
								WHEN PRODUCT_NAME LIKE '%STRAW%CUP%' THEN '�����Ѵ����'
								WHEN PRODUCT_NAME LIKE '%STRAW%BOTTLE%' THEN '�����Ѵ����'
								WHEN PRODUCT_NAME LIKE '%���%��ʹ�ٴ%' THEN '�����Ѵ����'
								WHEN PRODUCT_NAME LIKE '%�����ʹ%' THEN '�����Ѵ����'
								WHEN PRODUCT_NAME LIKE '%���������%' THEN '���������'
								WHEN PRODUCT_NAME LIKE '%���%��%' THEN '�����'
								WHEN PRODUCT_NAME LIKE '%��麴%' THEN '��麴'
								ELSE INFANT_10_12M_KEYWORD END)
WHERE MOM_AND_KID_PRODUCT_FLAG = 1
AND COALESCE(PREG_FLAG,'N/A') = 'N/A'
AND COALESCE(INFANT_FLAG,'N/A') = 'N/A'
AND COALESCE(TODDLER_FLAG,'N/A') = 'N/A'
AND COALESCE(PRESCHOOL_FLAG,'N/A') = 'N/A'
AND COALESCE(JUNIOR_FLAG,'N/A') = 'N/A'
AND COALESCE(TEEN_FLAG,'N/A') = 'N/A'
AND REVISED_CATEGORY_LEVEL_2 = 'KID ACCESSORIES'
AND (PRODUCT_NAME LIKE '%�Ѵ����%'
	OR PRODUCT_NAME LIKE '%���½֡�ٴ%'
	OR PRODUCT_NAME LIKE '%�Ѳ�ҡ�ôٴ%'
	OR PRODUCT_NAME LIKE '%TRANSITION%CUP%'
	OR PRODUCT_NAME LIKE '%SIPPY CUP%'
	OR PRODUCT_NAME LIKE '%���«Ի����%'
	OR PRODUCT_NAME LIKE '%TRAINER CUP%'
	OR PRODUCT_NAME LIKE '%STRAW%CUP%'
	OR PRODUCT_NAME LIKE '%STRAW%BOTTLE%'
	OR PRODUCT_NAME LIKE '%���%��ʹ�ٴ%'
	OR PRODUCT_NAME LIKE '%�����ʹ%'
	OR PRODUCT_NAME LIKE '%���������%'
	OR PRODUCT_NAME LIKE '%���%��%'
	OR PRODUCT_NAME LIKE '%��麴%')
;

/* BABY ACCESSORIES FOR >6 MONTHS */
UPDATE vpm_data.pm_skutagging_kids_product_master
SET "STAGE_M+0_TO_M+3" = 'N/A'
	,"STAGE_M+3_TO_M+6" = 'N/A'
	,"STAGE_M+6_TO_M+9" = 'YES'
	,"STAGE_M+9_TO_M+12" = 'YES'
	,INFANT_FLAG = 'YES'
	,UNKNOWN_INFANT_SUBSTAGE = 'N/A'
	,TODDLER_FLAG = 'YES'
	,INFANT_7_9M_KEYWORD = (CASE WHEN PRODUCT_NAME LIKE '%BABY SPOON%' THEN 'BABY SPOON'
								WHEN PRODUCT_NAME LIKE '%FEEDING SPOONS%' THEN 'BABY SPOON'
								WHEN PRODUCT_NAME LIKE '%FIRST SPOON%' THEN 'BABY SPOON'
								WHEN PRODUCT_NAME LIKE '%��͹�����%' THEN '��͹�����'
								WHEN PRODUCT_NAME LIKE '%��͹%�硷�á%' THEN 'BABY SPOON'
								WHEN PRODUCT_NAME LIKE '%��͹%�����%' THEN '��͹�����'
								WHEN PRODUCT_NAME LIKE '%�������͹%' THEN '��͹�����'
								WHEN PRODUCT_NAME LIKE '%���%�硷�á%' THEN '����硷�á'
								WHEN PRODUCT_NAME LIKE '%FEEDING STARTER%' THEN '��͹�����'
								WHEN PRODUCT_NAME LIKE '%��͹���¹���%' THEN 'BABY SPOON'
								WHEN PRODUCT_NAME LIKE '%����⤹�ٴ���%' THEN '����硷�á'
								WHEN PRODUCT_NAME LIKE '%FOOD FEEDER%' THEN '��͹�����'
								WHEN PRODUCT_NAME LIKE '%BENDABLE SPOON%' THEN '��͹��'
								WHEN PRODUCT_NAME LIKE '%TRAINING SPOON%' THEN 'BABY SPOON'
								WHEN PRODUCT_NAME LIKE '%EAR%THERMOMETER%' THEN 'EAR THERMOMETER'
								ELSE INFANT_7_9M_KEYWORD END)
	,INFANT_10_12M_KEYWORD = (CASE WHEN PRODUCT_NAME LIKE '%BABY SPOON%' THEN 'BABY SPOON'
								WHEN PRODUCT_NAME LIKE '%FEEDING SPOONS%' THEN 'BABY SPOON'
								WHEN PRODUCT_NAME LIKE '%FIRST SPOON%' THEN 'BABY SPOON'
								WHEN PRODUCT_NAME LIKE '%��͹�����%' THEN '��͹�����'
								WHEN PRODUCT_NAME LIKE '%��͹%�硷�á%' THEN 'BABY SPOON'
								WHEN PRODUCT_NAME LIKE '%��͹%�����%' THEN '��͹�����'
								WHEN PRODUCT_NAME LIKE '%�������͹%' THEN '��͹�����'
								WHEN PRODUCT_NAME LIKE '%���%�硷�á%' THEN '����硷�á'
								WHEN PRODUCT_NAME LIKE '%FEEDING STARTER%' THEN '��͹�����'
								WHEN PRODUCT_NAME LIKE '%��͹���¹���%' THEN 'BABY SPOON'
								WHEN PRODUCT_NAME LIKE '%����⤹�ٴ���%' THEN '����硷�á'
								WHEN PRODUCT_NAME LIKE '%FOOD FEEDER%' THEN '��͹�����'
								WHEN PRODUCT_NAME LIKE '%BENDABLE SPOON%' THEN '��͹��'
								WHEN PRODUCT_NAME LIKE '%TRAINING SPOON%' THEN 'BABY SPOON'
								WHEN PRODUCT_NAME LIKE '%EAR%THERMOMETER%' THEN 'EAR THERMOMETER'
								ELSE INFANT_10_12M_KEYWORD END)
	,TODDLER_KEYWORD = (CASE WHEN PRODUCT_NAME LIKE '%BABY SPOON%' THEN 'BABY SPOON'
								WHEN PRODUCT_NAME LIKE '%FEEDING SPOONS%' THEN 'BABY SPOON'
								WHEN PRODUCT_NAME LIKE '%FIRST SPOON%' THEN 'BABY SPOON'
								WHEN PRODUCT_NAME LIKE '%��͹�����%' THEN '��͹�����'
								WHEN PRODUCT_NAME LIKE '%��͹%�硷�á%' THEN 'BABY SPOON'
								WHEN PRODUCT_NAME LIKE '%��͹%�����%' THEN '��͹�����'
								WHEN PRODUCT_NAME LIKE '%�������͹%' THEN '��͹�����'
								WHEN PRODUCT_NAME LIKE '%���%�硷�á%' THEN '����硷�á'
								WHEN PRODUCT_NAME LIKE '%FEEDING STARTER%' THEN '��͹�����'
								WHEN PRODUCT_NAME LIKE '%��͹���¹���%' THEN 'BABY SPOON'
								WHEN PRODUCT_NAME LIKE '%����⤹�ٴ���%' THEN '����硷�á'
								WHEN PRODUCT_NAME LIKE '%FOOD FEEDER%' THEN '��͹�����'
								WHEN PRODUCT_NAME LIKE '%BENDABLE SPOON%' THEN '��͹��'
								WHEN PRODUCT_NAME LIKE '%TRAINING SPOON%' THEN 'BABY SPOON'
								WHEN PRODUCT_NAME LIKE '%EAR%THERMOMETER%' THEN 'BABY THERMOMETER'
								ELSE TODDLER_KEYWORD END)
WHERE MOM_AND_KID_PRODUCT_FLAG = 1
AND COALESCE(PREG_FLAG,'N/A') = 'N/A'
AND COALESCE(INFANT_FLAG,'N/A') = 'N/A'
AND COALESCE(TODDLER_FLAG,'N/A') = 'N/A'
AND COALESCE(PRESCHOOL_FLAG,'N/A') = 'N/A'
AND COALESCE(JUNIOR_FLAG,'N/A') = 'N/A'
AND COALESCE(TEEN_FLAG,'N/A') = 'N/A'
AND REVISED_CATEGORY_LEVEL_2 = 'KID ACCESSORIES'
AND (PRODUCT_NAME LIKE '%BABY SPOON%'
	OR PRODUCT_NAME LIKE '%FEEDING SPOONS%'
	OR PRODUCT_NAME LIKE '%FIRST SPOON%'
	OR PRODUCT_NAME LIKE '%��͹�����%'
	OR PRODUCT_NAME LIKE '%��͹%�硷�á%'
	OR PRODUCT_NAME LIKE '%��͹%�����%'
	OR PRODUCT_NAME LIKE '%�������͹%'
	OR PRODUCT_NAME LIKE '%���%�硷�á%'
	OR PRODUCT_NAME LIKE '%FEEDING STARTER%'
	OR PRODUCT_NAME LIKE '%��͹���¹���%'
	OR PRODUCT_NAME LIKE '%����⤹�ٴ���%'
	OR PRODUCT_NAME LIKE '%FOOD FEEDER%'
	OR PRODUCT_NAME LIKE '%BENDABLE SPOON%'
	OR PRODUCT_NAME LIKE '%TRAINING SPOON%'
	OR PRODUCT_NAME LIKE '%EAR%THERMOMETER%')
;

/* BABY THERMOMETER FOR INFANT */
UPDATE vpm_data.pm_skutagging_kids_product_master
SET INFANT_FLAG = 'YES'
	,UNKNOWN_INFANT_SUBSTAGE = 'YES'
	,INFANT_UNKNOWN_KEYWORD = 'BABY THERMOMETER'
WHERE MOM_AND_KID_PRODUCT_FLAG = 1
AND COALESCE(PREG_FLAG,'N/A') = 'N/A'
AND COALESCE(INFANT_FLAG,'N/A') = 'N/A'
AND COALESCE(TODDLER_FLAG,'N/A') = 'N/A'
AND COALESCE(PRESCHOOL_FLAG,'N/A') = 'N/A'
AND COALESCE(JUNIOR_FLAG,'N/A') = 'N/A'
AND COALESCE(TEEN_FLAG,'N/A') = 'N/A'
AND REVISED_CATEGORY_LEVEL_2 = 'KID ACCESSORIES'
AND (PRODUCT_NAME LIKE '%BATH%THERMOMETER%'
	OR PRODUCT_NAME LIKE '%RECTAL%THERMOMETER%')
;

/* BABY ACCESSORIES FOR >1 YEAR */
UPDATE vpm_data.pm_skutagging_kids_product_master
SET INFANT_FLAG = 'N/A'
	,TODDLER_FLAG = 'YES'
	,TODDLER_KEYWORD = (CASE WHEN PRODUCT_NAME LIKE '%FIRST CUTLERY%' THEN 'FIRST CUTLERY'
									WHEN PRODUCT_NAME LIKE '%�֡%�ҹ�����%' THEN 'FIRST CUTLERY'
									WHEN PRODUCT_NAME LIKE '%�ç�տѹ%���1%' THEN 'TOOTHBRUSH STEP 1'
									WHEN PRODUCT_NAME LIKE '%���������ѡ�á��%' THEN '�ͧ�ѡ�á'
									WHEN PRODUCT_NAME LIKE '%���ⶹ%' THEN '���ⶹ'
									WHEN PRODUCT_NAME LIKE '%�ͧ�����آ�ѳ��%' THEN '�ͧ�ѡ�á'
									WHEN PRODUCT_NAME LIKE '%�ͧ%�ѡ�á%' THEN '�ͧ�ѡ�á'
									WHEN PRODUCT_NAME LIKE '%POTTY%' THEN '���ⶹ'
									ELSE TODDLER_KEYWORD END)
WHERE MOM_AND_KID_PRODUCT_FLAG = 1
AND COALESCE(PREG_FLAG,'N/A') = 'N/A'
AND COALESCE(INFANT_FLAG,'N/A') = 'N/A'
AND COALESCE(TODDLER_FLAG,'N/A') = 'N/A'
AND COALESCE(PRESCHOOL_FLAG,'N/A') = 'N/A'
AND COALESCE(JUNIOR_FLAG,'N/A') = 'N/A'
AND COALESCE(TEEN_FLAG,'N/A') = 'N/A'
AND REVISED_CATEGORY_LEVEL_2 = 'KID ACCESSORIES'
AND (PRODUCT_NAME LIKE '%FIRST CUTLERY%'
	OR PRODUCT_NAME LIKE '%�֡%�ҹ�����%'
	OR PRODUCT_NAME LIKE '%�ç�տѹ%���1%'
	OR PRODUCT_NAME LIKE '%���������ѡ�á��%'
	OR PRODUCT_NAME LIKE '%���ⶹ%'
	OR PRODUCT_NAME LIKE '%�ͧ�����آ�ѳ��%'
	OR PRODUCT_NAME LIKE '%�ͧ%�ѡ�á%'
	OR PRODUCT_NAME LIKE '%POTTY%')
;

--�������
UPDATE vpm_data.pm_skutagging_kids_product_master
SET "STAGE_M+0_TO_M+3" = 'YES'
	,"STAGE_M+3_TO_M+6" = 'YES'
	,"STAGE_M+6_TO_M+9" = 'N/A'
	,"STAGE_M+9_TO_M+12" = 'N/A'
	,INFANT_FLAG = 'YES'
	,UNKNOWN_INFANT_SUBSTAGE = 'N/A'
	,INFANT_0_3M_KEYWORD = '�������'
	,INFANT_4_6M_KEYWORD = '�������'
WHERE MOM_AND_KID_PRODUCT_FLAG = 1
AND COALESCE(PREG_FLAG,'N/A') = 'N/A'
AND COALESCE(INFANT_FLAG,'N/A') = 'N/A'
AND COALESCE(TODDLER_FLAG,'N/A') = 'N/A'
AND COALESCE(PRESCHOOL_FLAG,'N/A') = 'N/A'
AND COALESCE(JUNIOR_FLAG,'N/A') = 'N/A'
AND COALESCE(TEEN_FLAG,'N/A') = 'N/A'
AND REVISED_CATEGORY_LEVEL_2 = 'KID ACCESSORIES'
AND (PRODUCT_NAME LIKE '%�������%')
;

--��٧
UPDATE vpm_data.pm_skutagging_kids_product_master
SET TODDLER_FLAG = 'YES'
	,PRESCHOOL_FLAG = 'YES'
	,TODDLER_KEYWORD = '��٧'
	,PRESCHOOL_KEYWORD = '��٧'
WHERE MOM_AND_KID_PRODUCT_FLAG = 1
AND COALESCE(PREG_FLAG,'N/A') = 'N/A'
AND COALESCE(INFANT_FLAG,'N/A') = 'N/A'
AND COALESCE(TODDLER_FLAG,'N/A') = 'N/A'
AND COALESCE(PRESCHOOL_FLAG,'N/A') = 'N/A'
AND COALESCE(JUNIOR_FLAG,'N/A') = 'N/A'
AND COALESCE(TEEN_FLAG,'N/A') = 'N/A'
AND REVISED_CATEGORY_LEVEL_2 = 'KID ACCESSORIES'
AND (PRODUCT_NAME LIKE '%��٧%')
;

--- KID NOURISH IN KID ACCESSARY
/* 4M++ */
UPDATE vpm_data.pm_skutagging_kids_product_master
SET "STAGE_M+0_TO_M+3" = 'N/A'
	,"STAGE_M+3_TO_M+6" = 'YES'
	,"STAGE_M+6_TO_M+9" = 'YES'
	,"STAGE_M+9_TO_M+12" = 'YES'
	,INFANT_FLAG = 'YES'
	,UNKNOWN_INFANT_SUBSTAGE = 'N/A'
	,INFANT_4_6M_KEYWORD = 'KID NOURISHMENT 3M+'
	,INFANT_7_9M_KEYWORD = 'KID NOURISHMENT 3M+'
	,INFANT_10_12M_KEYWORD = 'KID NOURISHMENT 3M+'
WHERE MOM_AND_KID_PRODUCT_FLAG = 1
AND COALESCE(PREG_FLAG,'N/A') = 'N/A'
AND COALESCE(INFANT_FLAG,'N/A') = 'N/A'
AND COALESCE(TODDLER_FLAG,'N/A') = 'N/A'
AND COALESCE(PRESCHOOL_FLAG,'N/A') = 'N/A'
AND COALESCE(JUNIOR_FLAG,'N/A') = 'N/A'
AND COALESCE(TEEN_FLAG,'N/A') = 'N/A'
AND REVISED_CATEGORY_LEVEL_3 = 'OTHER KID ACCESSORIES'
AND (CLEANED_BRANDNAME LIKE '%APPLE MONKEY%' 
      AND (PRODUCT_NAME LIKE '%����ͺ��ͺ%'
		OR PRODUCT_NAME LIKE '%���ǡ��ͧ%'
		OR PRODUCT_NAME LIKE '%����ͺ�ͧ%'
		OR PRODUCT_NAME LIKE '%��ʡ����%'))
;
-----------------------------------------------------------------------------------------------------------------------------------------------------
/* 6M++ */
UPDATE vpm_data.pm_skutagging_kids_product_master
SET "STAGE_M+0_TO_M+3" = 'N/A'
	,"STAGE_M+3_TO_M+6" = 'N/A'
	,"STAGE_M+6_TO_M+9" = 'YES'
	,"STAGE_M+9_TO_M+12" = 'YES'
	,INFANT_FLAG = 'YES'
	,UNKNOWN_INFANT_SUBSTAGE = 'N/A'
	,INFANT_7_9M_KEYWORD = 'KID NOURISHMENT 6M+'
	,INFANT_10_12M_KEYWORD = 'KID NOURISHMENT 6M+'
WHERE MOM_AND_KID_PRODUCT_FLAG = 1
AND COALESCE(PREG_FLAG,'N/A') = 'N/A'
AND COALESCE(INFANT_FLAG,'N/A') = 'N/A'
AND COALESCE(TODDLER_FLAG,'N/A') = 'N/A'
AND COALESCE(PRESCHOOL_FLAG,'N/A') = 'N/A'
AND COALESCE(JUNIOR_FLAG,'N/A') = 'N/A'
AND COALESCE(TEEN_FLAG,'N/A') = 'N/A'
AND REVISED_CATEGORY_LEVEL_3 = 'OTHER KID ACCESSORIES'
AND (CLEANED_BRANDNAME LIKE 'BABY NATURA%' 
     OR CLEANED_BRANDNAME LIKE '%BAMBOLINA%'
	 OR CLEANED_BRANDNAME LIKE '%BEBBY%'
	 OR PRODUCT_NAME LIKE '%����⾴�ѹ�� �躴%'
	 OR PRODUCT_NAME LIKE '%����мѡ��%'
	 OR PRODUCT_NAME LIKE '%�ػ�ѡ�����%'
	 OR PRODUCT_NAME LIKE '%�ѡ�ͧ�������%'
	 OR (CLEANED_BRANDNAME LIKE '%MAMA COOKS%' AND PRODUCT_NAME LIKE '%6MT%')
	 OR (CLEANED_BRANDNAME LIKE '%MAMA COOKS%' AND PRODUCT_NAME LIKE '%��%')
	 OR (CLEANED_BRANDNAME LIKE '%MILK PLUS & MORE%' AND PRODUCT_NAME LIKE '%��%')
	 OR CLEANED_BRANDNAME LIKE '%ONLY ORGANIC%'
	 OR CLEANED_BRANDNAME LIKE '%ORGANEH%'
	 OR (CLEANED_BRANDNAME LIKE '%PEACHY%' AND PRODUCT_NAME NOT LIKE '%SMOOTHIE%' AND PRODUCT_NAME NOT LIKE '%COOKIE%' AND PRODUCT_NAME NOT LIKE 'PUFF%')
	 OR (CLEANED_BRANDNAME LIKE '%XONGDUR%' AND PRODUCT_NAME LIKE 'SPROUTEDRICE%'))
;

/* 9M++ */
UPDATE vpm_data.pm_skutagging_kids_product_master
SET "STAGE_M+0_TO_M+3" = 'N/A'
	,"STAGE_M+3_TO_M+6" = 'N/A'
	,"STAGE_M+6_TO_M+9" = 'N/A'
	,"STAGE_M+9_TO_M+12" = 'YES'
	,INFANT_FLAG = 'YES'
	,UNKNOWN_INFANT_SUBSTAGE = 'N/A'
	,TODDLER_FLAG = 'YES'
	,INFANT_10_12M_KEYWORD = 'KID NOURISHMENT 9M+'
	,TODDLER_KEYWORD = 'KID NOURISHMENT 9M+'
WHERE MOM_AND_KID_PRODUCT_FLAG = 1
AND COALESCE(PREG_FLAG,'N/A') = 'N/A'
AND COALESCE(INFANT_FLAG,'N/A') = 'N/A'
AND COALESCE(TODDLER_FLAG,'N/A') = 'N/A'
AND COALESCE(PRESCHOOL_FLAG,'N/A') = 'N/A'
AND COALESCE(JUNIOR_FLAG,'N/A') = 'N/A'
AND COALESCE(TEEN_FLAG,'N/A') = 'N/A'
AND REVISED_CATEGORY_LEVEL_3 = 'OTHER KID ACCESSORIES'
AND (PRODUCT_NAME LIKE '%ʻ��絵���������%' 
     OR PRODUCT_NAME LIKE '%ʻ��絵������мѡ%'
	 OR PRODUCT_NAME LIKE '%ʵ�������%'
	 OR PRODUCT_NAME LIKE '%���ǵ��������͹%'
	 OR PRODUCT_NAME LIKE '%�ѹ����Ф�Թ�Ǻ�%'
	 OR CLEANED_BRANDNAME LIKE '%KHAO KHUN MHOR%'
	 OR CLEANED_BRANDNAME LIKE '%PICNIC BABY%'
	 OR (CLEANED_BRANDNAME LIKE '%XONGDUR%' AND PRODUCT_NAME LIKE '3 SPROUTEDRICE%'))
;

/* 1Y++ */
UPDATE vpm_data.pm_skutagging_kids_product_master
SET INFANT_FLAG = 'N/A'
	,TODDLER_FLAG = 'YES'
	,TODDLER_KEYWORD = 'KID NOURISHMENT 1Y+'
WHERE MOM_AND_KID_PRODUCT_FLAG = 1
AND COALESCE(PREG_FLAG,'N/A') = 'N/A'
AND COALESCE(INFANT_FLAG,'N/A') = 'N/A'
AND COALESCE(TODDLER_FLAG,'N/A') = 'N/A'
AND COALESCE(PRESCHOOL_FLAG,'N/A') = 'N/A'
AND COALESCE(JUNIOR_FLAG,'N/A') = 'N/A'
AND COALESCE(TEEN_FLAG,'N/A') = 'N/A'
AND REVISED_CATEGORY_LEVEL_3 = 'OTHER KID ACCESSORIES'
AND (PRODUCT_NAME LIKE '%���ͺ%' 
     OR PRODUCT_NAME LIKE '%�ٴ��������ͧ����躴%'
	 OR (CLEANED_BRANDNAME LIKE '%MAMA COOKS%' AND PRODUCT_NAME LIKE '%12MT%')
	 OR CLEANED_BRANDNAME LIKE '%WELB BABY%'
	 OR PRODUCT_NAME LIKE '%PEACHY%SMOOTHIE%'
	 OR PRODUCT_NAME LIKE '%PEACHY%COOKIE%'
	 OR PRODUCT_NAME LIKE '%PEACHY%PUFF%')
;

------------------------------------------- BOOK -------------------------------------------
UPDATE vpm_data.pm_skutagging_kids_product_master
SET PREG_FLAG = 'YES'
	,"STAGE_M-9_TO_M-6" = 'N/A'
	,"STAGE_M-6_TO_M-3" = 'N/A'
	,"STAGE_M-3_TO_M-0" = 'YES'
	,"STAGE_M+0_TO_M+3" = 'YES'
	,"STAGE_M+3_TO_M+6" = 'YES'
	,"STAGE_M+6_TO_M+9" = 'N/A'
	,"STAGE_M+9_TO_M+12" = 'N/A'
	,INFANT_FLAG = 'YES'
	,UNKNOWN_INFANT_SUBSTAGE = 'N/A'
	,INFANT_0_3M_KEYWORD = 'MOTHER AND BABY CARE'
	,INFANT_4_6M_KEYWORD = 'MOTHER AND BABY CARE'
WHERE MOM_AND_KID_PRODUCT_FLAG = 1
AND COALESCE(PREG_FLAG,'N/A') = 'N/A'
AND COALESCE(INFANT_FLAG,'N/A') = 'N/A'
AND COALESCE(TODDLER_FLAG,'N/A') = 'N/A'
AND COALESCE(PRESCHOOL_FLAG,'N/A') = 'N/A'
AND COALESCE(JUNIOR_FLAG,'N/A') = 'N/A'
AND COALESCE(TEEN_FLAG,'N/A') = 'N/A'
AND REVISED_CATEGORY_LEVEL_2 = 'BOOK'
AND (PRODUCT_NAME LIKE '%�����͡�ô�������١��ѧ��ʹ%')
;

UPDATE vpm_data.pm_skutagging_kids_product_master
SET PREG_FLAG = 'YES'
	,"STAGE_M-9_TO_M-6" = 'N/A'
	,"STAGE_M-6_TO_M-3" = 'N/A'
	,"STAGE_M-3_TO_M-0" = 'YES'
	,"STAGE_M+0_TO_M+3" = 'YES'
	,"STAGE_M+3_TO_M+6" = 'YES'
	,"STAGE_M+6_TO_M+9" = 'N/A'
	,"STAGE_M+9_TO_M+12" = 'N/A'
	,INFANT_FLAG = 'YES'
	,UNKNOWN_INFANT_SUBSTAGE = 'N/A'
	,PREG_7_9M_KEYWORD = 'PARENT GUIDE'
	,INFANT_0_3M_KEYWORD = 'PARENT GUIDE'
	,INFANT_4_6M_KEYWORD = 'PARENT GUIDE'
WHERE MOM_AND_KID_PRODUCT_FLAG = 1
AND COALESCE(PREG_FLAG,'N/A') = 'N/A'
AND COALESCE(INFANT_FLAG,'N/A') = 'N/A'
AND COALESCE(TODDLER_FLAG,'N/A') = 'N/A'
AND COALESCE(PRESCHOOL_FLAG,'N/A') = 'N/A'
AND COALESCE(JUNIOR_FLAG,'N/A') = 'N/A'
AND COALESCE(TEEN_FLAG,'N/A') = 'N/A'
AND REVISED_CATEGORY_LEVEL_2 = 'BOOK'
AND (PRODUCT_NAME LIKE '%��ͧ�Ѩ�������á�Դ%'
		OR PRODUCT_NAME LIKE '%˹ٹ����%�á�Դ%'
		OR PRODUCT_NAME LIKE '%�������١ �Թ�յ�͹�Ѻ����š��� �%'
		OR PRODUCT_NAME LIKE '%�����ͤس����١��͹%'
		OR PRODUCT_NAME LIKE '%�����͡�ô�������١��ѧ��ʹ%')
;

UPDATE vpm_data.pm_skutagging_kids_product_master
SET "STAGE_M+0_TO_M+3" = 'N/A'
	,"STAGE_M+3_TO_M+6" = 'YES'
	,"STAGE_M+6_TO_M+9" = 'YES'
	,"STAGE_M+9_TO_M+12" = 'YES'
	,INFANT_FLAG = 'YES'
	,UNKNOWN_INFANT_SUBSTAGE = 'N/A'
	,TODDLER_FLAG = 'YES'
	,INFANT_4_6M_KEYWORD = 'BABY FOOD COOKBOOK'
	,INFANT_7_9M_KEYWORD = 'BABY FOOD COOKBOOK'
	,INFANT_10_12M_KEYWORD = 'BABY FOOD COOKBOOK'
	,TODDLER_KEYWORD = 'BABY FOOD COOKBOOK'
WHERE MOM_AND_KID_PRODUCT_FLAG = 1
AND COALESCE(PREG_FLAG,'N/A') = 'N/A'
AND COALESCE(INFANT_FLAG,'N/A') = 'N/A'
AND COALESCE(TODDLER_FLAG,'N/A') = 'N/A'
AND COALESCE(PRESCHOOL_FLAG,'N/A') = 'N/A'
AND COALESCE(JUNIOR_FLAG,'N/A') = 'N/A'
AND COALESCE(TEEN_FLAG,'N/A') = 'N/A'
AND REVISED_CATEGORY_LEVEL_2 = 'BOOK'
AND (PRODUCT_NAME LIKE '%�������á�Դ%'
		OR PRODUCT_NAME LIKE '%�Թ��������%'
		OR PRODUCT_NAME LIKE '%�����á�ͧ˹�%'
		OR PRODUCT_NAME LIKE '����������¾ԪԵ���ҵ�ǹ���')
;

UPDATE vpm_data.pm_skutagging_kids_product_master
SET "STAGE_M+0_TO_M+3" = 'N/A'
	,"STAGE_M+3_TO_M+6" = 'N/A'
	,"STAGE_M+6_TO_M+9" = 'YES'
	,"STAGE_M+9_TO_M+12" = 'YES'
	,INFANT_FLAG = 'YES'
	,UNKNOWN_INFANT_SUBSTAGE = 'N/A'
	,TODDLER_FLAG = 'YES'
	,INFANT_7_9M_KEYWORD = 'BABY FOOD COOKBOOK'
	,INFANT_10_12M_KEYWORD = 'BABY FOOD COOKBOOK'
	,TODDLER_KEYWORD = 'BABY FOOD COOKBOOK'
WHERE MOM_AND_KID_PRODUCT_FLAG = 1
AND COALESCE(PREG_FLAG,'N/A') = 'N/A'
AND COALESCE(INFANT_FLAG,'N/A') = 'N/A'
AND COALESCE(TODDLER_FLAG,'N/A') = 'N/A'
AND COALESCE(PRESCHOOL_FLAG,'N/A') = 'N/A'
AND COALESCE(JUNIOR_FLAG,'N/A') = 'N/A'
AND COALESCE(TEEN_FLAG,'N/A') = 'N/A'
AND REVISED_CATEGORY_LEVEL_2 = 'BOOK'
AND (PRODUCT_NAME LIKE '%COOKING FOR BABY%'
		OR PRODUCT_NAME LIKE '%���������%')
;

--PREG BOOK
UPDATE vpm_data.pm_skutagging_kids_product_master
SET PREG_FLAG = 'YES'
	,"STAGE_M-9_TO_M-6" = 'YES'
	,"STAGE_M-6_TO_M-3" = 'YES'
	,PREG_0_3M_KEYWORD = 'PREGNANCY GUIDE'
	,PREG_4_6M_KEYWORD = 'PREGNANCY GUIDE'
WHERE MOM_AND_KID_PRODUCT_FLAG = 1
AND COALESCE(PREG_FLAG,'N/A') = 'N/A'
AND COALESCE(INFANT_FLAG,'N/A') = 'N/A'
AND COALESCE(TODDLER_FLAG,'N/A') = 'N/A'
AND COALESCE(PRESCHOOL_FLAG,'N/A') = 'N/A'
AND COALESCE(JUNIOR_FLAG,'N/A') = 'N/A'
AND COALESCE(TEEN_FLAG,'N/A') = 'N/A'
AND REVISED_CATEGORY_LEVEL_2 = 'BOOK'
AND CLASS_NAME = 'MOTHER & CHILD'
AND PRODUCT_NAME LIKE '%���%'
;

------------------------------------------- TOY -------------------------------------------
UPDATE vpm_data.pm_skutagging_kids_product_master
SET "STAGE_M+0_TO_M+3" = 'YES'
	,"STAGE_M+3_TO_M+6" = 'N/A'
	,"STAGE_M+6_TO_M+9" = 'N/A'
	,"STAGE_M+9_TO_M+12" = 'N/A'
	,INFANT_FLAG = 'YES'
	,UNKNOWN_INFANT_SUBSTAGE = 'N/A'
	,INFANT_0_3M_KEYWORD = 'BABY MOBILE'
WHERE MOM_AND_KID_PRODUCT_FLAG = 1
AND COALESCE(PREG_FLAG,'N/A') = 'N/A'
AND COALESCE(INFANT_FLAG,'N/A') = 'N/A'
AND COALESCE(TODDLER_FLAG,'N/A') = 'N/A'
AND COALESCE(PRESCHOOL_FLAG,'N/A') = 'N/A'
AND COALESCE(JUNIOR_FLAG,'N/A') = 'N/A'
AND COALESCE(TEEN_FLAG,'N/A') = 'N/A'
AND REVISED_CATEGORY_LEVEL_2 = 'TOYS'
AND ((PRODUCT_NAME LIKE '%�ǹ%'
			OR PRODUCT_NAME LIKE '%MUSICAL%PULL%STRING%'
			OR PRODUCT_NAME LIKE '%TAKE ALONG MOBILE%'
			OR PRODUCT_NAME LIKE '%PLAYGRO 3IN1 SAFARI SUPER GYM%'
			OR PRODUCT_NAME LIKE '%MINI MOBILE%'
			OR (PRODUCT_NAME LIKE '%DREAM%MOBILE%' AND PRODUCT_NAME NOT LIKE '%ö%')
			OR PRODUCT_NAME LIKE '%BUSY%BEE%MOBILE%'
			OR PRODUCT_NAME LIKE '%MUSIC%MOBILE%'
			OR PRODUCT_NAME LIKE '%MOBILE%MUSIC%'
			OR PRODUCT_NAME LIKE '%NB%MOBILE%'
			OR PRODUCT_NAME LIKE '%NEWBORN%MOBILE%'
			OR PRODUCT_NAME LIKE '%BABY%MOBILE%'
			OR PRODUCT_NAME LIKE '%STROLLER%MOBILE%'
			OR PRODUCT_NAME LIKE '%MOBILE%DAY%'
			OR PRODUCT_NAME LIKE '%SING%MOBILE%'
			OR PRODUCT_NAME LIKE '%HANG%MOBILE%'
			OR PRODUCT_NAME LIKE '%SOUND%MOBILE%'
			OR PRODUCT_NAME LIKE '%TRAVEL%MOBILE%'
			OR PRODUCT_NAME LIKE '%STAR%MOBILE%'
			OR PRODUCT_NAME LIKE '%MOBILE%LILYBELLE%'
			OR PRODUCT_NAME LIKE '%LULLABY%MOBILE%'
			OR PRODUCT_NAME LIKE '%MOTION%DEVELOP%MOBILE%'
			OR PRODUCT_NAME LIKE '%CLASSIC%MOBILE%'
			OR PRODUCT_NAME LIKE '%MOBILE%RAINFOREST%'
			OR PRODUCT_NAME LIKE 'LITTLE RED RIDING HOOD MOBILE'
			OR PRODUCT_NAME LIKE 'MY NATURE PALS MOBILE'
			OR PRODUCT_NAME LIKE '%TREE%MOBILE%'
			OR PRODUCT_NAME LIKE '%�����%')
		AND PRODUCT_NAME NOT LIKE '%������%'
		AND PRODUCT_NAME NOT LIKE '%�Ҥ��Ŵ%'
		AND PRODUCT_NAME NOT LIKE '%�ҧ�Ѵ%'
		AND PRODUCT_NAME NOT LIKE '�������%')
;

UPDATE vpm_data.pm_skutagging_kids_product_master
SET "STAGE_M+0_TO_M+3" = 'N/A'
	,"STAGE_M+3_TO_M+6" = 'YES'
	,"STAGE_M+6_TO_M+9" = 'YES'
	,"STAGE_M+9_TO_M+12" = 'N/A'
	,INFANT_FLAG = 'YES'
	,UNKNOWN_INFANT_SUBSTAGE = 'N/A'
	,INFANT_4_6M_KEYWORD = (CASE WHEN PRODUCT_NAME LIKE '%RATTLE%' THEN 'RATTLE'
								WHEN PRODUCT_NAME LIKE '%RING%' THEN 'RATTLE'
								WHEN PRODUCT_NAME LIKE '%PLAYGRO SOFT BLOCKS%' THEN 'SOFT BLOCK'
								ELSE INFANT_4_6M_KEYWORD END)
	,INFANT_7_9M_KEYWORD = (CASE WHEN PRODUCT_NAME LIKE '%RATTLE%' THEN 'RATTLE'
								WHEN PRODUCT_NAME LIKE '%RING%' THEN 'RATTLE'
								WHEN PRODUCT_NAME LIKE '%PLAYGRO SOFT BLOCKS%' THEN 'SOFT BLOCK'
								ELSE INFANT_7_9M_KEYWORD END)
WHERE MOM_AND_KID_PRODUCT_FLAG = 1
AND COALESCE(PREG_FLAG,'N/A') = 'N/A'
AND COALESCE(INFANT_FLAG,'N/A') = 'N/A'
AND COALESCE(TODDLER_FLAG,'N/A') = 'N/A'
AND COALESCE(PRESCHOOL_FLAG,'N/A') = 'N/A'
AND COALESCE(JUNIOR_FLAG,'N/A') = 'N/A'
AND COALESCE(TEEN_FLAG,'N/A') = 'N/A'
AND REVISED_CATEGORY_LEVEL_2 = 'TOYS'
AND (PRODUCT_NAME LIKE '%RATTLE%'
	OR PRODUCT_NAME LIKE '%RING%' --***************************
	OR PRODUCT_NAME LIKE '%PLAYGRO SOFT BLOCKS%')
;

UPDATE vpm_data.pm_skutagging_kids_product_master
SET "STAGE_M+0_TO_M+3" = 'N/A'
	,"STAGE_M+3_TO_M+6" = 'YES'
	,"STAGE_M+6_TO_M+9" = 'YES'
	,"STAGE_M+9_TO_M+12" = 'YES'
	,INFANT_FLAG = 'YES'
	,UNKNOWN_INFANT_SUBSTAGE = 'N/A'
	,TODDLER_FLAG = 'YES'
	,INFANT_4_6M_KEYWORD = (CASE WHEN PRODUCT_NAME LIKE '%�ҧ�Ѵ%' THEN '�ҧ�Ѵ'
								   WHEN PRODUCT_NAME LIKE '%TEETHER%' THEN '�ҧ�Ѵ'
								   WHEN PRODUCT_NAME LIKE '%�ѹ%' THEN '�ҧ�Ѵ'
								   WHEN PRODUCT_NAME LIKE '%STACKING%FUN%CUPS' THEN 'PEG TOYS'
								   WHEN PRODUCT_NAME LIKE 'FUN%PHONE%MINNIE' THEN 'TOY PHONE'
								   WHEN PRODUCT_NAME LIKE '%STRAW%BOTTLE%' THEN 'STRAW BOTTLE'
								   WHEN PRODUCT_NAME LIKE '%EXPLORE&MORE%ROLL%' THEN 'RATTLE'
								   WHEN PRODUCT_NAME LIKE '%����%' THEN 'RATTLE'
								   ELSE INFANT_4_6M_KEYWORD END)
	,INFANT_7_9M_KEYWORD = (CASE WHEN PRODUCT_NAME LIKE '%�ҧ�Ѵ%' THEN '�ҧ�Ѵ'
								   WHEN PRODUCT_NAME LIKE '%TEETHER%' THEN '�ҧ�Ѵ'
								   WHEN PRODUCT_NAME LIKE '%�ѹ%' THEN '�ҧ�Ѵ'
								   WHEN PRODUCT_NAME LIKE '%STACKING%FUN%CUPS' THEN 'PEG TOYS'
								   WHEN PRODUCT_NAME LIKE 'FUN%PHONE%MINNIE' THEN 'TOY PHONE'
								   WHEN PRODUCT_NAME LIKE '%STRAW%BOTTLE%' THEN 'STRAW BOTTLE'
								   WHEN PRODUCT_NAME LIKE '%EXPLORE&MORE%ROLL%' THEN 'RATTLE'
								   WHEN PRODUCT_NAME LIKE '%����%' THEN 'RATTLE'
								   ELSE INFANT_7_9M_KEYWORD END)
	,INFANT_10_12M_KEYWORD = (CASE WHEN PRODUCT_NAME LIKE '%�ҧ�Ѵ%' THEN '�ҧ�Ѵ'
								   WHEN PRODUCT_NAME LIKE '%TEETHER%' THEN '�ҧ�Ѵ'
								   WHEN PRODUCT_NAME LIKE '%�ѹ%' THEN '�ҧ�Ѵ'
								   WHEN PRODUCT_NAME LIKE '%STACKING%FUN%CUPS' THEN 'PEG TOYS'
								   WHEN PRODUCT_NAME LIKE 'FUN%PHONE%MINNIE' THEN 'TOY PHONE'
								   WHEN PRODUCT_NAME LIKE '%STRAW%BOTTLE%' THEN 'STRAW BOTTLE'
								   WHEN PRODUCT_NAME LIKE '%EXPLORE&MORE%ROLL%' THEN 'RATTLE'
								   WHEN PRODUCT_NAME LIKE '%����%' THEN 'RATTLE'
								   ELSE INFANT_10_12M_KEYWORD END)
	,TODDLER_KEYWORD = (CASE WHEN PRODUCT_NAME LIKE '%�ҧ�Ѵ%' THEN '�ҧ�Ѵ'
								   WHEN PRODUCT_NAME LIKE '%TEETHER%' THEN '�ҧ�Ѵ'
								   WHEN PRODUCT_NAME LIKE '%�ѹ%' THEN '�ҧ�Ѵ'
								   WHEN PRODUCT_NAME LIKE '%STACKING%FUN%CUPS' THEN 'PEG TOYS'
								   WHEN PRODUCT_NAME LIKE 'FUN%PHONE%MINNIE' THEN 'TOY PHONE'
								   WHEN PRODUCT_NAME LIKE '%STRAW%BOTTLE%' THEN 'STRAW BOTTLE'
								   WHEN PRODUCT_NAME LIKE '%EXPLORE&MORE%ROLL%' THEN 'RATTLE'
								   WHEN PRODUCT_NAME LIKE '%����%' THEN 'RATTLE'
								   ELSE TODDLER_KEYWORD END)
WHERE MOM_AND_KID_PRODUCT_FLAG = 1
AND COALESCE(PREG_FLAG,'N/A') = 'N/A'
AND COALESCE(INFANT_FLAG,'N/A') = 'N/A'
AND COALESCE(TODDLER_FLAG,'N/A') = 'N/A'
AND COALESCE(PRESCHOOL_FLAG,'N/A') = 'N/A'
AND COALESCE(JUNIOR_FLAG,'N/A') = 'N/A'
AND COALESCE(TEEN_FLAG,'N/A') = 'N/A'
AND REVISED_CATEGORY_LEVEL_2 = 'TOYS'
AND	(PRODUCT_NAME LIKE '%�ҧ�Ѵ%' 
	   OR PRODUCT_NAME LIKE '%TEETHER%'
	   OR PRODUCT_NAME LIKE '%�ѹ%'
	   OR PRODUCT_NAME LIKE '%STACKING%FUN%CUPS'
	   OR PRODUCT_NAME LIKE 'FUN%PHONE%MINNIE'
	   OR PRODUCT_NAME LIKE '%STRAW%BOTTLE%' 
	   OR PRODUCT_NAME LIKE '%EXPLORE&MORE%ROLL%'
	   OR PRODUCT_NAME LIKE '%����%')
;

UPDATE vpm_data.pm_skutagging_kids_product_master
SET "STAGE_M+0_TO_M+3" = 'N/A'
	,"STAGE_M+3_TO_M+6" = 'N/A'
	,"STAGE_M+6_TO_M+9" = 'YES'
	,"STAGE_M+9_TO_M+12" = 'YES'
	,INFANT_FLAG = 'YES'
	,UNKNOWN_INFANT_SUBSTAGE = 'N/A'
	,TODDLER_FLAG = 'YES'
	,INFANT_7_9M_KEYWORD = 'RATTLE'
	,INFANT_10_12M_KEYWORD = 'RATTLE'
	,TODDLER_KEYWORD = 'RATTLE'
WHERE MOM_AND_KID_PRODUCT_FLAG = 1
AND COALESCE(PREG_FLAG,'N/A') = 'N/A'
AND COALESCE(INFANT_FLAG,'N/A') = 'N/A'
AND COALESCE(TODDLER_FLAG,'N/A') = 'N/A'
AND COALESCE(PRESCHOOL_FLAG,'N/A') = 'N/A'
AND COALESCE(JUNIOR_FLAG,'N/A') = 'N/A'
AND COALESCE(TEEN_FLAG,'N/A') = 'N/A'
AND REVISED_CATEGORY_LEVEL_2 = 'TOYS'
AND	(PRODUCT_NAME LIKE '%SHAKE%' 
	OR PRODUCT_NAME LIKE '%BENDY%'
	OR PRODUCT_NAME LIKE '%PLAY%LEARN%')
;

UPDATE vpm_data.pm_skutagging_kids_product_master
SET INFANT_FLAG = 'YES'
	,TODDLER_FLAG = 'YES'
	,UNKNOWN_INFANT_SUBSTAGE = 'N/A'
	,TODDLER_KEYWORD = 'RATTLE'
WHERE MOM_AND_KID_PRODUCT_FLAG = 1
AND COALESCE(PREG_FLAG,'N/A') = 'N/A'
AND COALESCE(INFANT_FLAG,'N/A') = 'N/A'
AND COALESCE(TODDLER_FLAG,'N/A') = 'N/A'
AND COALESCE(PRESCHOOL_FLAG,'N/A') = 'N/A'
AND COALESCE(JUNIOR_FLAG,'N/A') = 'N/A'
AND COALESCE(TEEN_FLAG,'N/A') = 'N/A'
AND REVISED_CATEGORY_LEVEL_2 = 'TOYS'
AND PRODUCT_NAME LIKE 'PLAYGRO EXPLOR-A-BALL'
;

UPDATE vpm_data.pm_skutagging_kids_product_master
SET INFANT_FLAG = 'N/A'
	,TODDLER_FLAG = 'YES'
	,TODDLER_KEYWORD = (CASE WHEN PRODUCT_NAME LIKE '%12M%' THEN '12M+'
									WHEN PRODUCT_NAME LIKE '%18M%' THEN '18M+'
									WHEN PRODUCT_NAME LIKE '%WALK%' THEN '�Ѵ�Թ'
									WHEN PRODUCT_NAME LIKE '%�Ѵ�Թ%' THEN '�Ѵ�Թ'
									WHEN CLEANED_BRANDNAME LIKE '%PLAYGO%' THEN '�֡�Ѳ�ҡ����'
									ELSE TODDLER_KEYWORD END)
WHERE MOM_AND_KID_PRODUCT_FLAG = 1
AND COALESCE(PREG_FLAG,'N/A') = 'N/A'
AND COALESCE(INFANT_FLAG,'N/A') = 'N/A'
AND COALESCE(TODDLER_FLAG,'N/A') = 'N/A'
AND COALESCE(PRESCHOOL_FLAG,'N/A') = 'N/A'
AND COALESCE(JUNIOR_FLAG,'N/A') = 'N/A'
AND COALESCE(TEEN_FLAG,'N/A') = 'N/A'
AND REVISED_CATEGORY_LEVEL_2 = 'TOYS' 
AND (PRODUCT_NAME LIKE '%12M%'
	OR PRODUCT_NAME LIKE '%18M%'
	OR PRODUCT_NAME LIKE '%WALK%'
	OR PRODUCT_NAME LIKE '%�Ѵ�Թ%'
	OR (CLEANED_BRANDNAME LIKE '%PLAYGO%' AND (PRODUCT_NAME LIKE '%�ش%' OR PRODUCT_NAME LIKE '%�����緽֡�Ѳ�ҡ����3IN1%')
								  AND PRODUCT_NAME NOT LIKE '%�ش��Сͺ%'))
;

------------------------------------------- KID FASHION -------------------------------------------
/* KID FASHION FOR 0-3 MONTHS */
UPDATE vpm_data.pm_skutagging_kids_product_master
SET "STAGE_M+0_TO_M+3" = 'YES'
	,"STAGE_M+3_TO_M+6" = 'N/A'
	,"STAGE_M+6_TO_M+9" = 'N/A'
	,"STAGE_M+9_TO_M+12" = 'N/A'
	,INFANT_FLAG = 'YES'
	,UNKNOWN_INFANT_SUBSTAGE = 'N/A'
	,INFANT_0_3M_KEYWORD = (CASE WHEN PRODUCT_NAME LIKE '% 3M%' THEN '0-3 MTH'
								WHEN PRODUCT_NAME LIKE '%03M%' THEN '0-3 MTH'
								WHEN PRODUCT_NAME LIKE '%3M' THEN '0-3 MTH'
								WHEN PRODUCT_NAME LIKE '% 0M%' THEN '0-3 MTH'
								WHEN PRODUCT_NAME LIKE '% 1M%' THEN '1 MTH'
								WHEN PRODUCT_NAME LIKE '% 01M%' THEN '1 MTH'
								WHEN PRODUCT_NAME LIKE '%0-3 M%' THEN '0-3 MTH'
								WHEN PRODUCT_NAME LIKE '%0-3M%' THEN '0-3 MTH'
								WHEN PRODUCT_NAME LIKE '%0 - 3M%' THEN '0-3 MTH'
								WHEN PRODUCT_NAME LIKE '%2-3 M%' THEN '2-3 MTH'
								WHEN PRODUCT_NAME LIKE '%(50)%' THEN '50 CM'
								WHEN PRODUCT_NAME LIKE '%����ͧ������͹% 0' THEN '����ͧ������͹ 0'
								WHEN PRODUCT_NAME LIKE '%�ҧࡧ�����͹%0%' THEN '�ҧࡧ�����͹ 0'
								WHEN PRODUCT_NAME LIKE '%�ش�ʹ���ٷ����͹%50%' THEN '50 CM'
								WHEN PRODUCT_NAME LIKE '%�ا�������͹%' THEN '�ا�������͹'
								ELSE INFANT_0_3M_KEYWORD END)
WHERE MOM_AND_KID_PRODUCT_FLAG = 1
AND REVISED_CATEGORY_LEVEL_2 = 'KID FASHION'
AND COALESCE(PREG_FLAG,'N/A') = 'N/A'
AND COALESCE(INFANT_FLAG,'N/A') = 'N/A'
AND COALESCE(TODDLER_FLAG,'N/A') = 'N/A'
AND COALESCE(PRESCHOOL_FLAG,'N/A') = 'N/A'
AND COALESCE(JUNIOR_FLAG,'N/A') = 'N/A'
AND COALESCE(TEEN_FLAG,'N/A') = 'N/A'
AND ((PRODUCT_NAME LIKE '% 3M%' AND PRODUCT_NAME NOT LIKE '%3M 6M%')
	OR PRODUCT_NAME LIKE '%03M%'
	OR PRODUCT_NAME LIKE '%3M'
	OR PRODUCT_NAME LIKE '% 0M%'
	OR PRODUCT_NAME LIKE '% 1M%'
	OR PRODUCT_NAME LIKE '% 01M%'
	OR PRODUCT_NAME LIKE '%0-3 M%'
	OR PRODUCT_NAME LIKE '%0-3M%'
	OR PRODUCT_NAME LIKE '%0 - 3M%'
	OR PRODUCT_NAME LIKE '%2-3 M%'
	OR PRODUCT_NAME LIKE '%(50)%'
	OR PRODUCT_NAME LIKE '%����ͧ������͹% 0'
	OR PRODUCT_NAME LIKE '%�ҧࡧ�����͹%0%'
	OR PRODUCT_NAME LIKE '%�ش�ʹ���ٷ����͹%50%'
	OR PRODUCT_NAME LIKE '%�ا�������͹%')
;

/* KID FASHION FOR 0-6 MONTHS */
UPDATE vpm_data.pm_skutagging_kids_product_master
SET "STAGE_M+0_TO_M+3" = 'YES'
	,"STAGE_M+3_TO_M+6" = 'YES'
	,"STAGE_M+6_TO_M+9" = 'N/A'
	,"STAGE_M+9_TO_M+12" = 'N/A'
	,INFANT_FLAG = 'YES'
	,UNKNOWN_INFANT_SUBSTAGE = 'N/A'
	,INFANT_0_3M_KEYWORD = (CASE WHEN PRODUCT_NAME LIKE '%0-6M%' THEN '0-6 MTH'
								WHEN PRODUCT_NAME LIKE '%0-6 M%' THEN '0-6 MTH'
								WHEN PRODUCT_NAME LIKE '%0 - 6 M%' THEN '0-6 MTH'
								ELSE INFANT_0_3M_KEYWORD END)
	,INFANT_4_6M_KEYWORD = (CASE WHEN PRODUCT_NAME LIKE '%0-6M%' THEN '0-6 MTH'
								WHEN PRODUCT_NAME LIKE '%0-6 M%' THEN '0-6 MTH'
								WHEN PRODUCT_NAME LIKE '%0 - 6 M%' THEN '0-6 MTH'
								ELSE INFANT_4_6M_KEYWORD END)
WHERE MOM_AND_KID_PRODUCT_FLAG = 1
AND REVISED_CATEGORY_LEVEL_2 = 'KID FASHION'
AND COALESCE(PREG_FLAG,'N/A') = 'N/A'
AND COALESCE(INFANT_FLAG,'N/A') = 'N/A'
AND COALESCE(TODDLER_FLAG,'N/A') = 'N/A'
AND COALESCE(PRESCHOOL_FLAG,'N/A') = 'N/A'
AND COALESCE(JUNIOR_FLAG,'N/A') = 'N/A'
AND COALESCE(TEEN_FLAG,'N/A') = 'N/A'
AND (PRODUCT_NAME LIKE '%0-6M%'
	OR PRODUCT_NAME LIKE '%0-6 M%'
	OR PRODUCT_NAME LIKE '%0 - 6 M%')
;

/* KID FASHION FOR 0-9 MONTHS */
UPDATE vpm_data.pm_skutagging_kids_product_master
SET "STAGE_M+0_TO_M+3" = 'YES'
	,"STAGE_M+3_TO_M+6" = 'YES'
	,"STAGE_M+6_TO_M+9" = 'YES'
	,"STAGE_M+9_TO_M+12" = 'N/A'
	,INFANT_FLAG = 'YES'
	,UNKNOWN_INFANT_SUBSTAGE = 'N/A'
	,INFANT_0_3M_KEYWORD = (CASE WHEN PRODUCT_NAME LIKE '%0-9M%' THEN '0-9 MTH'
								WHEN PRODUCT_NAME LIKE '%(40-70)%' THEN '40-70 CM'
								ELSE INFANT_0_3M_KEYWORD END)
	,INFANT_4_6M_KEYWORD = (CASE WHEN PRODUCT_NAME LIKE '%0-9M%' THEN '0-9 MTH'
								WHEN PRODUCT_NAME LIKE '%(40-70)%' THEN '40-70 CM'
								ELSE INFANT_4_6M_KEYWORD END)
	,INFANT_7_9M_KEYWORD = (CASE WHEN PRODUCT_NAME LIKE '%0-9M%' THEN '0-9 MTH'
								WHEN PRODUCT_NAME LIKE '%(40-70)%' THEN '40-70 CM'
								ELSE INFANT_7_9M_KEYWORD END)
WHERE MOM_AND_KID_PRODUCT_FLAG = 1
AND REVISED_CATEGORY_LEVEL_2 = 'KID FASHION'
AND COALESCE(PREG_FLAG,'N/A') = 'N/A'
AND COALESCE(INFANT_FLAG,'N/A') = 'N/A'
AND COALESCE(TODDLER_FLAG,'N/A') = 'N/A'
AND COALESCE(PRESCHOOL_FLAG,'N/A') = 'N/A'
AND COALESCE(JUNIOR_FLAG,'N/A') = 'N/A'
AND COALESCE(TEEN_FLAG,'N/A') = 'N/A'
AND (PRODUCT_NAME LIKE '%0-9M%'
	OR PRODUCT_NAME LIKE '%(40-70)%')
;

/* KID FASHION FOR 4-6 MONTHS */
UPDATE vpm_data.pm_skutagging_kids_product_master
SET "STAGE_M+0_TO_M+3" = 'N/A'
	,"STAGE_M+3_TO_M+6" = 'YES'
	,"STAGE_M+6_TO_M+9" = 'N/A'
	,"STAGE_M+9_TO_M+12" = 'N/A'
	,INFANT_FLAG = 'YES'
	,UNKNOWN_INFANT_SUBSTAGE = 'N/A'
	,INFANT_4_6M_KEYWORD = (CASE WHEN PRODUCT_NAME LIKE '%3-4 M%' THEN '3-4 MTH'
								WHEN PRODUCT_NAME LIKE '%3-6M%' THEN '3-6 MTH'
								WHEN PRODUCT_NAME LIKE '%3-6 M%' THEN '3-6 MTH'
								WHEN PRODUCT_NAME LIKE '%5-6 M%' THEN '5-6 MTH'
								WHEN PRODUCT_NAME LIKE '% 6M%' THEN '3-6 MTH'
								WHEN PRODUCT_NAME LIKE '%06M%' THEN '3-6 MTH'
								WHEN PRODUCT_NAME LIKE '%����ͧ������͹%60%' THEN '60 CM'
								WHEN PRODUCT_NAME LIKE '%�ҧࡧ�����͹%1%' THEN '����͹ 1'
								WHEN PRODUCT_NAME LIKE '%�ش�ʹ���ٷ����͹%60%' THEN '60 CM'
								WHEN (PRODUCT_NAME LIKE '% 60%' AND DEPT_NAME = 'BABY & KIDS' AND SUBDEPT_NAME = 'BABY S WEAR') THEN '60 CM'
								WHEN (PRODUCT_NAME LIKE '%�ͧ���%�����%1%' AND PRODUCT_NAME NOT LIKE '%[0-9][0-9]%') THEN '�ͧ��� 1'
								WHEN (PRODUCT_NAME LIKE '%�ͧ���%105%' AND TYPE_FINAL = 'FOOTWEAR' AND CLASS_NAME LIKE 'INFANT%') THEN '�ͧ��� 10.5 CM'
								ELSE INFANT_4_6M_KEYWORD END)
WHERE MOM_AND_KID_PRODUCT_FLAG = 1
AND REVISED_CATEGORY_LEVEL_2 = 'KID FASHION'
AND COALESCE(PREG_FLAG,'N/A') = 'N/A'
AND COALESCE(INFANT_FLAG,'N/A') = 'N/A'
AND COALESCE(TODDLER_FLAG,'N/A') = 'N/A'
AND COALESCE(PRESCHOOL_FLAG,'N/A') = 'N/A'
AND COALESCE(JUNIOR_FLAG,'N/A') = 'N/A'
AND COALESCE(TEEN_FLAG,'N/A') = 'N/A'
AND (PRODUCT_NAME LIKE '%3-4 M%'
	OR PRODUCT_NAME LIKE '%3-6M%'
	OR PRODUCT_NAME LIKE '%3-6 M%'
	OR PRODUCT_NAME LIKE '%5-6 M%'
	OR PRODUCT_NAME LIKE '% 6M%'
	OR PRODUCT_NAME LIKE '%06M%'
	OR PRODUCT_NAME LIKE '%����ͧ������͹%60%'
	OR PRODUCT_NAME LIKE '%�ҧࡧ�����͹%1%'
	OR PRODUCT_NAME LIKE '%�ش�ʹ���ٷ����͹%60%'
	OR (PRODUCT_NAME LIKE '% 60%' AND DEPT_NAME = 'BABY & KIDS' AND SUBDEPT_NAME = 'BABY S WEAR')
	OR (PRODUCT_NAME LIKE '%�ͧ���%�����%1%' AND PRODUCT_NAME NOT LIKE '%[0-9][0-9]%')
	OR (PRODUCT_NAME LIKE '%�ͧ���%105%' AND TYPE_FINAL = 'FOOTWEAR' AND CLASS_NAME LIKE 'INFANT%'))
;

/* KID FASHION FOR 4-9 MONTHS */
UPDATE vpm_data.pm_skutagging_kids_product_master
SET "STAGE_M+0_TO_M+3" = 'N/A'
	,"STAGE_M+3_TO_M+6" = 'YES'
	,"STAGE_M+6_TO_M+9" = 'YES'
	,"STAGE_M+9_TO_M+12" = 'N/A'
	,INFANT_FLAG = 'YES'
	,UNKNOWN_INFANT_SUBSTAGE = 'N/A'
	,INFANT_4_6M_KEYWORD = (CASE WHEN PRODUCT_NAME LIKE '%�ا�������͹%1%' THEN '�ا�������͹ 1'
								WHEN PRODUCT_NAME LIKE '%�ͧ���%1%' THEN '�ͧ��� 1'
								ELSE INFANT_4_6M_KEYWORD END)
	,INFANT_7_9M_KEYWORD = (CASE WHEN PRODUCT_NAME LIKE '%�ا�������͹%1%' THEN '�ا�������͹ 1'
								WHEN PRODUCT_NAME LIKE '%�ͧ���%1%' THEN '�ͧ��� 1'
								ELSE INFANT_7_9M_KEYWORD END)
WHERE MOM_AND_KID_PRODUCT_FLAG = 1
AND REVISED_CATEGORY_LEVEL_2 = 'KID FASHION'
AND COALESCE(PREG_FLAG,'N/A') = 'N/A'
AND COALESCE(INFANT_FLAG,'N/A') = 'N/A'
AND COALESCE(TODDLER_FLAG,'N/A') = 'N/A'
AND COALESCE(PRESCHOOL_FLAG,'N/A') = 'N/A'
AND COALESCE(JUNIOR_FLAG,'N/A') = 'N/A'
AND COALESCE(TEEN_FLAG,'N/A') = 'N/A'
AND (PRODUCT_NAME LIKE '%�ا�������͹%1%'
	OR (PRODUCT_NAME LIKE '%�ͧ���%1' AND TYPE_FINAL = 'FOOTWEAR' AND CLASS_NAME LIKE 'INFANT%'))
;

/* KID FASHION FOR 7-9 MONTHS */
UPDATE vpm_data.pm_skutagging_kids_product_master
SET "STAGE_M+0_TO_M+3" = 'N/A'
	,"STAGE_M+3_TO_M+6" = 'N/A'
	,"STAGE_M+6_TO_M+9" = 'YES'
	,"STAGE_M+9_TO_M+12" = 'N/A'
	,INFANT_FLAG = 'YES'
	,UNKNOWN_INFANT_SUBSTAGE = 'N/A'
	,INFANT_7_9M_KEYWORD = (CASE WHEN PRODUCT_NAME LIKE '%6-9M%' THEN '6-9 MTH'
								WHEN PRODUCT_NAME LIKE '%6-9 M%' THEN '6-9 MTH'
								WHEN PRODUCT_NAME LIKE '% 9M%' THEN '6-9 MTH'
								WHEN PRODUCT_NAME LIKE '%7-8 M%' THEN '7-8 MTH'
								WHEN PRODUCT_NAME LIKE '%09M%' THEN '6-9 MTH'
								WHEN PRODUCT_NAME LIKE '%����ͧ������͹%70%' THEN '����ͧ������͹ 70'
								WHEN PRODUCT_NAME LIKE '%�ҧࡧ�����͹%2%' THEN '�ҧࡧ�����͹ 2'
								WHEN PRODUCT_NAME LIKE '%�ش�ʹ���ٷ��%70%' THEN '�ش�� 70'
								WHEN (PRODUCT_NAME LIKE '% 70%' AND DEPT_NAME = 'BABY & KIDS' AND SUBDEPT_NAME = 'BABY S WEAR') THEN '70 CM'
								WHEN (PRODUCT_NAME LIKE '%�ͧ���%�����%1%' AND PRODUCT_NAME NOT LIKE '%[0-9][0-9]%') THEN '�ͧ��� 1'
								WHEN (PRODUCT_NAME LIKE '%�ͧ���%115%' AND TYPE_FINAL = 'FOOTWEAR' AND CLASS_NAME LIKE 'INFANT%') THEN '�ͧ��� 11.5 CM'
								ELSE INFANT_7_9M_KEYWORD END)
WHERE MOM_AND_KID_PRODUCT_FLAG = 1
AND REVISED_CATEGORY_LEVEL_2 = 'KID FASHION'
AND COALESCE(PREG_FLAG,'N/A') = 'N/A'
AND COALESCE(INFANT_FLAG,'N/A') = 'N/A'
AND COALESCE(TODDLER_FLAG,'N/A') = 'N/A'
AND COALESCE(PRESCHOOL_FLAG,'N/A') = 'N/A'
AND COALESCE(JUNIOR_FLAG,'N/A') = 'N/A'
AND COALESCE(TEEN_FLAG,'N/A') = 'N/A'
AND (PRODUCT_NAME LIKE '%6-9M%'
	OR PRODUCT_NAME LIKE '%6-9 M%'
	OR PRODUCT_NAME LIKE '% 9M%'
	OR PRODUCT_NAME LIKE '%7-8 M%'
	OR PRODUCT_NAME LIKE '%09M%'
	OR PRODUCT_NAME LIKE '%����ͧ������͹%70%'
	OR PRODUCT_NAME LIKE '%�ҧࡧ�����͹%2%'
	OR PRODUCT_NAME LIKE '%�ش�ʹ���ٷ��%70%'
	OR (PRODUCT_NAME LIKE '% 70%' AND DEPT_NAME = 'BABY & KIDS' AND SUBDEPT_NAME = 'BABY S WEAR')
	OR (PRODUCT_NAME LIKE '%�ͧ���%�����%1%' AND PRODUCT_NAME NOT LIKE '%[0-9][0-9]%')
	OR (PRODUCT_NAME LIKE '%�ͧ���%115%' AND TYPE_FINAL = 'FOOTWEAR' AND CLASS_NAME LIKE 'INFANT%'))
;

/* KID FASHION FOR 7-12 MONTHS */
UPDATE vpm_data.pm_skutagging_kids_product_master
SET "STAGE_M+0_TO_M+3" = 'N/A'
	,"STAGE_M+3_TO_M+6" = 'N/A'
	,"STAGE_M+6_TO_M+9" = 'YES'
	,"STAGE_M+9_TO_M+12" = 'YES'
	,INFANT_FLAG = 'YES'
	,UNKNOWN_INFANT_SUBSTAGE = 'N/A'
	,INFANT_7_9M_KEYWORD = '6-12 MTH'
	,INFANT_10_12M_KEYWORD = '6-12 MTH'
WHERE MOM_AND_KID_PRODUCT_FLAG = 1
AND REVISED_CATEGORY_LEVEL_2 = 'KID FASHION'
AND COALESCE(PREG_FLAG,'N/A') = 'N/A'
AND COALESCE(INFANT_FLAG,'N/A') = 'N/A'
AND COALESCE(TODDLER_FLAG,'N/A') = 'N/A'
AND COALESCE(PRESCHOOL_FLAG,'N/A') = 'N/A'
AND COALESCE(JUNIOR_FLAG,'N/A') = 'N/A'
AND COALESCE(TEEN_FLAG,'N/A') = 'N/A'
AND (PRODUCT_NAME LIKE '%6-12M%'
	OR PRODUCT_NAME LIKE '%6-12 M%')
;

/* KID FASHION FOR 10-12 MONTHS */
UPDATE vpm_data.pm_skutagging_kids_product_master
SET "STAGE_M+0_TO_M+3" = 'N/A'
	,"STAGE_M+3_TO_M+6" = 'N/A'
	,"STAGE_M+6_TO_M+9" = 'N/A'
	,"STAGE_M+9_TO_M+12" = 'YES'
	,INFANT_FLAG = 'YES'
	,UNKNOWN_INFANT_SUBSTAGE = 'N/A'
	,INFANT_10_12M_KEYWORD = (CASE WHEN PRODUCT_NAME LIKE '%9-12M%' THEN '9-12 MTH'
									WHEN PRODUCT_NAME LIKE '%9-12 M%' THEN '9-12 MTH'
									WHEN PRODUCT_NAME LIKE '%12M%' THEN '9-12 MTH'
									WHEN PRODUCT_NAME LIKE '% 1Y%' THEN '1Y'
									WHEN PRODUCT_NAME LIKE '%01Y%' THEN '1Y'
									WHEN PRODUCT_NAME LIKE '% 1A%' THEN '1Y'
									WHEN PRODUCT_NAME LIKE '%����ͧ������͹%80%' THEN '����ͧ������͹ 80'
									WHEN PRODUCT_NAME LIKE '%�ҧࡧ�����͹%3%' THEN '�ҧࡧ�����͹ 3'
									WHEN PRODUCT_NAME LIKE '%�ا�������͹%2%' THEN '�ا�������͹ 2'
									WHEN PRODUCT_NAME LIKE '%�ͧ���%����͹%2%' THEN '�ͧ��� 2'
									WHEN (PRODUCT_NAME LIKE '% 80%' AND DEPT_NAME = 'BABY & KIDS' AND SUBDEPT_NAME = 'BABY S WEAR') THEN '80 CM'
									WHEN (PRODUCT_NAME LIKE '% 85%' AND DEPT_NAME = 'BABY & KIDS' AND SUBDEPT_NAME = 'BABY S WEAR') THEN '85 CM'
									WHEN (PRODUCT_NAME LIKE '%�ͧ���%125%' AND TYPE_FINAL = 'FOOTWEAR' AND CLASS_NAME LIKE 'INFANT%') THEN '�ͧ��� 12.5 CM'
									WHEN (PRODUCT_NAME LIKE '%�ͧ���%2' AND TYPE_FINAL = 'FOOTWEAR' AND CLASS_NAME LIKE 'INFANT%') THEN '�ͧ��� 2'
									ELSE INFANT_10_12M_KEYWORD END)
WHERE MOM_AND_KID_PRODUCT_FLAG = 1
AND REVISED_CATEGORY_LEVEL_2 = 'KID FASHION'
AND COALESCE(PREG_FLAG,'N/A') = 'N/A'
AND COALESCE(INFANT_FLAG,'N/A') = 'N/A'
AND COALESCE(TODDLER_FLAG,'N/A') = 'N/A'
AND COALESCE(PRESCHOOL_FLAG,'N/A') = 'N/A'
AND COALESCE(JUNIOR_FLAG,'N/A') = 'N/A'
AND COALESCE(TEEN_FLAG,'N/A') = 'N/A'
AND (PRODUCT_NAME LIKE '%9-12M%'
	OR PRODUCT_NAME LIKE '%9-12 M%'
	OR PRODUCT_NAME LIKE '%12M%'
	OR PRODUCT_NAME LIKE '% 1Y%'
	OR PRODUCT_NAME LIKE '%01Y%'
	OR PRODUCT_NAME LIKE '% 1A%'
	OR PRODUCT_NAME LIKE '%����ͧ������͹%80%'
	OR PRODUCT_NAME LIKE '%�ҧࡧ�����͹%3%'
	OR PRODUCT_NAME LIKE '%�ا�������͹%2%'
	OR PRODUCT_NAME LIKE '%�ͧ���%����͹%2%'
	OR (PRODUCT_NAME LIKE '% 80%' AND DEPT_NAME = 'BABY & KIDS' AND SUBDEPT_NAME = 'BABY S WEAR')
	OR (PRODUCT_NAME LIKE '% 85%' AND DEPT_NAME = 'BABY & KIDS' AND SUBDEPT_NAME = 'BABY S WEAR')
	OR (PRODUCT_NAME LIKE '%�ͧ���%125%' AND TYPE_FINAL = 'FOOTWEAR' AND CLASS_NAME LIKE 'INFANT%')
	OR (PRODUCT_NAME LIKE '%�ͧ���%2' AND TYPE_FINAL = 'FOOTWEAR' AND CLASS_NAME LIKE 'INFANT%'))
;

/* KID FASHION FOR 13-23 MONTHS */
UPDATE vpm_data.pm_skutagging_kids_product_master
SET INFANT_FLAG = 'N/A'
	,TODDLER_FLAG = 'YES'
	,TODDLER_KEYWORD = (CASE WHEN PRODUCT_NAME LIKE '%12-18M%' THEN '12-18 MTH'
									WHEN PRODUCT_NAME LIKE '%1-2Y%' THEN '1Y'
									WHEN PRODUCT_NAME LIKE '% 18M%' THEN '12-18 MTH'
									WHEN PRODUCT_NAME LIKE '%18-24M%' THEN '18-24 MTH'
									WHEN PRODUCT_NAME LIKE '% 24M%' THEN '18-24 MTH'
									WHEN PRODUCT_NAME LIKE '%M24%' THEN '18-24 MTH'
									WHEN PRODUCT_NAME LIKE '%12 18M%' THEN '12-18 MTH'
									WHEN PRODUCT_NAME LIKE '%12-24 M%' THEN '12-24 MTH'
									WHEN PRODUCT_NAME LIKE '%02Y%' THEN '1-2 Y'
									WHEN PRODUCT_NAME LIKE '% 2Y%' THEN '1-2 Y'
									WHEN PRODUCT_NAME LIKE '% 2T' THEN '1-2 Y'
									WHEN PRODUCT_NAME LIKE '% 2A%' THEN '1-2 Y'
									WHEN PRODUCT_NAME LIKE '%1 3Y%' THEN '1-3 Y'
									WHEN PRODUCT_NAME LIKE '%18 MONTH%' THEN '12-18 MTH'
									WHEN PRODUCT_NAME LIKE '%����ͧ������͹%90%' THEN '����ͧ������͹ 90'
									WHEN PRODUCT_NAME LIKE '%�ҧࡧ�����͹%4%' THEN '�ҧࡧ�����͹ 4'
									WHEN PRODUCT_NAME LIKE '%�ҧࡧ�����͹%5%' THEN '�ҧࡧ�����͹ 5'
									WHEN PRODUCT_NAME LIKE '%�ҧࡧ�����͹%6%' THEN '�ҧࡧ�����͹ 6'
									WHEN PRODUCT_NAME LIKE '%�ا�������͹%3%' THEN '�ا�������͹ 3'
									WHEN PRODUCT_NAME LIKE '%�ͧ���%����͹%3%' THEN '�ͧ�������͹ 3'
									WHEN PRODUCT_NAME LIKE '%�ͧ���%����͹%4%' THEN '�ͧ�������͹ 4'
									WHEN PRODUCT_NAME LIKE '%�ͧ���%����͹%5%' THEN '�ͧ�������͹ 5'
									WHEN PRODUCT_NAME LIKE '%�ͧ���%����͹%6%' THEN '�ͧ�������͹ 6'
									WHEN PRODUCT_NAME LIKE '%�ͧ���%�����%19%' THEN '�ͧ����� EU SIZE 19'
									WHEN PRODUCT_NAME LIKE '%�ͧ���%�����%20%' THEN '�ͧ����� EU SIZE 20'
									WHEN PRODUCT_NAME LIKE '%�ͧ���%�����%21%' THEN '�ͧ����� EU SIZE 21'
									WHEN PRODUCT_NAME LIKE '%�ͧ���%�����%22%' THEN '�ͧ����� EU SIZE 22'
									WHEN PRODUCT_NAME LIKE '%�Ѫ��%�����%20%' THEN '�ͧ����� EU SIZE 20'
									WHEN PRODUCT_NAME LIKE '%�Ѫ��%�����%21%' THEN '�ͧ����� EU SIZE 21'
									WHEN PRODUCT_NAME LIKE '%�ͧ���%�����%13.5%' THEN '�ͧ����� 13.5 CM'
									WHEN PRODUCT_NAME LIKE '%�ͧ���%�����%13 5%' THEN '�ͧ����� 13.5 CM'
									WHEN PRODUCT_NAME LIKE '%�ͧ���%�����%14 5%' THEN '�ͧ����� 14.5 CM'
									WHEN PRODUCT_NAME LIKE '%�ͧ���%�����%14.5%' THEN '�ͧ����� 14.5 CM'
									WHEN (PRODUCT_NAME LIKE '% 90%' AND DEPT_NAME = 'BABY & KIDS' AND SUBDEPT_NAME = 'BABY S WEAR') THEN '90 CM'
									WHEN (PRODUCT_NAME LIKE '% 95%' AND DEPT_NAME = 'BABY & KIDS' AND SUBDEPT_NAME = 'BABY S WEAR') THEN '95 CM'
									WHEN (PRODUCT_NAME LIKE '%�ͧ���%�����%3%' AND PRODUCT_NAME NOT LIKE '%[0-9][0-9]%') THEN '�ͧ�������͹ 3'
									WHEN (PRODUCT_NAME LIKE '%�ͧ���%�����%4%' AND PRODUCT_NAME NOT LIKE '%[0-9][0-9]%') THEN '�ͧ�������͹ 4'
									WHEN (PRODUCT_NAME LIKE '%�ͧ���%�����%5%' AND PRODUCT_NAME NOT LIKE '%[0-9][0-9]%') THEN '�ͧ�������͹ 5'
									WHEN (PRODUCT_NAME LIKE '%�ͧ���%�����%6%' AND PRODUCT_NAME NOT LIKE '%[0-9][0-9]%') THEN '�ͧ�������͹ 6'
									WHEN (PRODUCT_NAME LIKE '%18%' AND TYPE_FINAL = 'FOOTWEAR' AND CLASS_NAME LIKE 'INFANT%') THEN '�ͧ����� EU SIZE 18'
									WHEN (PRODUCT_NAME LIKE '%19%' AND TYPE_FINAL = 'FOOTWEAR' AND CLASS_NAME LIKE 'INFANT%') THEN '�ͧ����� EU SIZE 19'
									WHEN (PRODUCT_NAME LIKE '%20%' AND TYPE_FINAL = 'FOOTWEAR' AND CLASS_NAME LIKE 'INFANT%') THEN '�ͧ����� EU SIZE 20'
									WHEN (PRODUCT_NAME LIKE '%21%' AND TYPE_FINAL = 'FOOTWEAR' AND CLASS_NAME LIKE 'INFANT%') THEN '�ͧ����� EU SIZE 21'
									WHEN (PRODUCT_NAME LIKE '%22%' AND TYPE_FINAL = 'FOOTWEAR' AND CLASS_NAME LIKE 'INFANT%') THEN '�ͧ����� EU SIZE 22'
									WHEN (PRODUCT_NAME LIKE '%�ͧ���%13 5%' AND TYPE_FINAL = 'FOOTWEAR' AND CLASS_NAME LIKE 'INFANT%') THEN '�ͧ��� 13.5 CM'
									WHEN (PRODUCT_NAME LIKE '%�ͧ���%13%' AND TYPE_FINAL = 'FOOTWEAR' AND CLASS_NAME LIKE 'INFANT%') THEN '�ͧ��� 13 CM'
									WHEN (PRODUCT_NAME LIKE '%�ͧ���%135%' AND TYPE_FINAL = 'FOOTWEAR' AND CLASS_NAME LIKE 'INFANT%') THEN '�ͧ��� 13.5 CM'
									WHEN (PRODUCT_NAME LIKE '%�ͧ���%14 5%' AND TYPE_FINAL = 'FOOTWEAR' AND CLASS_NAME LIKE 'INFANT%') THEN '�ͧ��� 14.5 CM'
									WHEN (PRODUCT_NAME LIKE '%�ͧ���%14%' AND TYPE_FINAL = 'FOOTWEAR' AND CLASS_NAME LIKE 'INFANT%') THEN '�ͧ��� 14 CM'
									WHEN (PRODUCT_NAME LIKE '%�ͧ���%145%' AND TYPE_FINAL = 'FOOTWEAR' AND CLASS_NAME LIKE 'INFANT%') THEN '�ͧ��� 14.5 CM'
									WHEN (PRODUCT_NAME LIKE '%�ͧ���%15%' AND TYPE_FINAL = 'FOOTWEAR' AND CLASS_NAME LIKE 'INFANT%') THEN '�ͧ��� 15 CM'
									WHEN (PRODUCT_NAME LIKE '%�ͧ���%3' AND PRODUCT_NAME NOT LIKE '%[0-9][0-9]%' AND TYPE_FINAL = 'FOOTWEAR' AND CLASS_NAME LIKE 'INFANT%') THEN '�ͧ�������͹ 3'
									WHEN (PRODUCT_NAME LIKE '%�ͧ���%4' AND PRODUCT_NAME NOT LIKE '%[0-9][0-9]%' AND TYPE_FINAL = 'FOOTWEAR' AND CLASS_NAME LIKE 'INFANT%') THEN '�ͧ�������͹ 4'
									WHEN (PRODUCT_NAME LIKE '%�ͧ���%5' AND PRODUCT_NAME NOT LIKE '%[0-9][0-9]%' AND TYPE_FINAL = 'FOOTWEAR' AND CLASS_NAME LIKE 'INFANT%') THEN '�ͧ�������͹ 5'
									WHEN (PRODUCT_NAME LIKE '%�ͧ���%6' AND PRODUCT_NAME NOT LIKE '%[0-9][0-9]%' AND PRODUCT_NAME NOT LIKE '%0-6 M%' AND TYPE_FINAL = 'FOOTWEAR' AND CLASS_NAME LIKE 'INFANT%') THEN '�ͧ�������͹ 6'
									ELSE TODDLER_KEYWORD END)
WHERE MOM_AND_KID_PRODUCT_FLAG = 1
AND REVISED_CATEGORY_LEVEL_2 = 'KID FASHION'
AND COALESCE(PREG_FLAG,'N/A') = 'N/A'
AND COALESCE(INFANT_FLAG,'N/A') = 'N/A'
AND COALESCE(TODDLER_FLAG,'N/A') = 'N/A'
AND COALESCE(PRESCHOOL_FLAG,'N/A') = 'N/A'
AND COALESCE(JUNIOR_FLAG,'N/A') = 'N/A'
AND COALESCE(TEEN_FLAG,'N/A') = 'N/A'
AND (PRODUCT_NAME LIKE '%12-18M%'
	OR PRODUCT_NAME LIKE '%1-2Y%'
	OR PRODUCT_NAME LIKE '% 18M%'
	OR PRODUCT_NAME LIKE '%18-24M%'
	OR PRODUCT_NAME LIKE '% 24M%'
	OR PRODUCT_NAME LIKE '%M24%'
	OR PRODUCT_NAME LIKE '%12 18M%'
	OR PRODUCT_NAME LIKE '%12-24 M%'
	OR PRODUCT_NAME LIKE '%02Y%'
	OR PRODUCT_NAME LIKE '% 2Y%'
	OR PRODUCT_NAME LIKE '% 2T'
	OR (PRODUCT_NAME LIKE '% 2A%' AND PRODUCT_NAME NOT LIKE '%0-6M%')
	OR PRODUCT_NAME LIKE '%1 3Y%'
	OR PRODUCT_NAME LIKE '%18 MONTH%'
	OR PRODUCT_NAME LIKE '%����ͧ������͹%90%'
	OR PRODUCT_NAME LIKE '%����ͧ������͹%90%'
	OR PRODUCT_NAME LIKE '%�ҧࡧ�����͹%4%'
	OR PRODUCT_NAME LIKE '%�ҧࡧ�����͹%5%'
	OR PRODUCT_NAME LIKE '%�ҧࡧ�����͹%6%'
	OR PRODUCT_NAME LIKE '%�ا�������͹%3%'
	OR (PRODUCT_NAME LIKE '%�ͧ���%����͹%3%' AND PRODUCT_NAME NOT LIKE '%0-3M%')
	OR PRODUCT_NAME LIKE '%�ͧ���%����͹%4%'
	OR PRODUCT_NAME LIKE '%�ͧ���%����͹%5%'
	OR PRODUCT_NAME LIKE '%�ͧ���%����͹%6%'
	OR PRODUCT_NAME LIKE '%�ͧ���%�����%19%'
	OR PRODUCT_NAME LIKE '%�ͧ���%�����%20%'
	OR PRODUCT_NAME LIKE '%�ͧ���%�����%21%'
	OR PRODUCT_NAME LIKE '%�ͧ���%�����%22%'
	OR PRODUCT_NAME LIKE '%�Ѫ��%�����%20%'
	OR PRODUCT_NAME LIKE '%�Ѫ��%�����%21%'
	OR PRODUCT_NAME LIKE '%�ͧ���%�����%13.5%'
	OR PRODUCT_NAME LIKE '%�ͧ���%�����%13 5%'
	OR PRODUCT_NAME LIKE '%�ͧ���%�����%14 5%'
	OR PRODUCT_NAME LIKE '%�ͧ���%�����%14.5%'
	OR (PRODUCT_NAME LIKE '% 90%' AND DEPT_NAME = 'BABY & KIDS' AND SUBDEPT_NAME = 'BABY S WEAR')
	OR (PRODUCT_NAME LIKE '% 95%' AND DEPT_NAME = 'BABY & KIDS' AND SUBDEPT_NAME = 'BABY S WEAR')
	OR (PRODUCT_NAME LIKE '%�ͧ���%�����%3%' AND PRODUCT_NAME NOT LIKE '%[0-9][0-9]%' AND PRODUCT_NAME NOT LIKE '%0-3M%')
	OR (PRODUCT_NAME LIKE '%�ͧ���%�����%4%' AND PRODUCT_NAME NOT LIKE '%[0-9][0-9]%')
	OR (PRODUCT_NAME LIKE '%�ͧ���%�����%5%' AND PRODUCT_NAME NOT LIKE '%[0-9][0-9]%')
	OR (PRODUCT_NAME LIKE '%�ͧ���%�����%6%' AND PRODUCT_NAME NOT LIKE '%[0-9][0-9]%')
	OR (PRODUCT_NAME LIKE '%18%' AND TYPE_FINAL = 'FOOTWEAR' AND CLASS_NAME LIKE 'INFANT%')
	OR (PRODUCT_NAME LIKE '%19%' AND TYPE_FINAL = 'FOOTWEAR' AND CLASS_NAME LIKE 'INFANT%')
	OR (PRODUCT_NAME LIKE '%20%' AND TYPE_FINAL = 'FOOTWEAR' AND CLASS_NAME LIKE 'INFANT%')
	OR (PRODUCT_NAME LIKE '%21%' AND TYPE_FINAL = 'FOOTWEAR' AND CLASS_NAME LIKE 'INFANT%')
	OR (PRODUCT_NAME LIKE '%22%' AND TYPE_FINAL = 'FOOTWEAR' AND CLASS_NAME LIKE 'INFANT%')
	OR (PRODUCT_NAME LIKE '%�ͧ���%13 5%' AND TYPE_FINAL = 'FOOTWEAR' AND CLASS_NAME LIKE 'INFANT%')
	OR (PRODUCT_NAME LIKE '%�ͧ���%13%' AND TYPE_FINAL = 'FOOTWEAR' AND CLASS_NAME LIKE 'INFANT%')
	OR (PRODUCT_NAME LIKE '%�ͧ���%135%' AND TYPE_FINAL = 'FOOTWEAR' AND CLASS_NAME LIKE 'INFANT%')
	OR (PRODUCT_NAME LIKE '%�ͧ���%14 5%' AND TYPE_FINAL = 'FOOTWEAR' AND CLASS_NAME LIKE 'INFANT%')
	OR (PRODUCT_NAME LIKE '%�ͧ���%14%' AND TYPE_FINAL = 'FOOTWEAR' AND CLASS_NAME LIKE 'INFANT%')
	OR (PRODUCT_NAME LIKE '%�ͧ���%145%' AND TYPE_FINAL = 'FOOTWEAR' AND CLASS_NAME LIKE 'INFANT%')
	OR (PRODUCT_NAME LIKE '%�ͧ���%15%' AND TYPE_FINAL = 'FOOTWEAR' AND CLASS_NAME LIKE 'INFANT%')
	OR (PRODUCT_NAME LIKE '%�ͧ���%3' AND PRODUCT_NAME NOT LIKE '%[0-9][0-9]%' AND TYPE_FINAL = 'FOOTWEAR' AND CLASS_NAME LIKE 'INFANT%' AND PRODUCT_NAME NOT LIKE '%0-3M%')
	OR (PRODUCT_NAME LIKE '%�ͧ���%4' AND PRODUCT_NAME NOT LIKE '%[0-9][0-9]%' AND TYPE_FINAL = 'FOOTWEAR' AND CLASS_NAME LIKE 'INFANT%')
	OR (PRODUCT_NAME LIKE '%�ͧ���%5' AND PRODUCT_NAME NOT LIKE '%[0-9][0-9]%' AND TYPE_FINAL = 'FOOTWEAR' AND CLASS_NAME LIKE 'INFANT%')
	OR (PRODUCT_NAME LIKE '%�ͧ���%6' AND PRODUCT_NAME NOT LIKE '%[0-9][0-9]%' AND PRODUCT_NAME NOT LIKE '%0-6 M%' AND TYPE_FINAL = 'FOOTWEAR' AND CLASS_NAME LIKE 'INFANT%'))
;

/* KID FASHION - NOT INFANT */
UPDATE vpm_data.pm_skutagging_kids_product_master
SET INFANT_FLAG = NULL
	,UNKNOWN_INFANT_SUBSTAGE = NULL
	,"STAGE_M+0_TO_M+3" = NULL
	,"STAGE_M+3_TO_M+6" = NULL
	,"STAGE_M+6_TO_M+9" = NULL
	,"STAGE_M+9_TO_M+12" = NULL
	,INFANT_0_3M_KEYWORD = NULL
	,INFANT_4_6M_KEYWORD = NULL
	,INFANT_7_9M_KEYWORD = NULL
	,INFANT_10_12M_KEYWORD = NULL
	,INFANT_UNKNOWN_KEYWORD = NULL
WHERE MOM_AND_KID_PRODUCT_FLAG = 1
AND REVISED_CATEGORY_LEVEL_2 = 'KID FASHION'
AND (PRODUCT_NAME LIKE '%�ͧ���%23%'
	OR PRODUCT_NAME LIKE '%�ͧ���%24%'
	OR PRODUCT_NAME LIKE '%�ͧ���%26%'
	OR (PRODUCT_NAME LIKE '%�ͧ���%25%' AND PRODUCT_NAME NOT LIKE '%125%')
	OR (PRODUCT_NAME LIKE '%�ͧ���%�����%' AND CLEANED_BRANDNAME IN ('OSHKOSH','TOEZONE') AND PRODUCT_NAME LIKE '%[0-9][0-9]%')
	OR (PRODUCT_NAME LIKE '%�ͧ���%�����%7%' AND PRODUCT_NAME NOT LIKE '%[0-9][0-9]%' AND PRODUCT_NAME NOT LIKE '%[0-9]-[0-9]%')
	OR (PRODUCT_NAME LIKE '%�ͧ���%�����%8%' AND PRODUCT_NAME NOT LIKE '%[0-9][0-9]%' AND PRODUCT_NAME NOT LIKE '%[0-9]-[0-9]%')
	OR (PRODUCT_NAME LIKE '%�ͧ���%�����%9%' AND PRODUCT_NAME NOT LIKE '%[0-9][0-9]%' AND PRODUCT_NAME NOT LIKE '%[0-9]-[0-9]%'))
;


------------------------------------------------------------------------------------- NEW
--0-6M
UPDATE vpm_data.pm_skutagging_kids_product_master
SET "STAGE_M+0_TO_M+3" = 'YES'
	,"STAGE_M+3_TO_M+6" = 'YES'
	,"STAGE_M+6_TO_M+9" = 'N/A'
	,"STAGE_M+9_TO_M+12" = 'N/A'
	,INFANT_FLAG = 'YES'
	,UNKNOWN_INFANT_SUBSTAGE = 'N/A'
	,INFANT_0_3M_KEYWORD = '��͹����'
	,INFANT_4_6M_KEYWORD = '��͹����'
WHERE MOM_AND_KID_PRODUCT_FLAG = 1
AND PRODUCT_NAME LIKE '%��͹%����%'
AND COALESCE(PREG_FLAG,'N/A') = 'N/A'
AND COALESCE(INFANT_FLAG,'N/A') = 'N/A'
AND COALESCE(TODDLER_FLAG,'N/A') = 'N/A'
AND COALESCE(PRESCHOOL_FLAG,'N/A') = 'N/A'
AND COALESCE(JUNIOR_FLAG,'N/A') = 'N/A'
AND COALESCE(TEEN_FLAG,'N/A') = 'N/A'
;

--3-12M
UPDATE vpm_data.pm_skutagging_kids_product_master
SET "STAGE_M+0_TO_M+3" = 'N/A'
	,"STAGE_M+3_TO_M+6" = 'YES'
	,"STAGE_M+6_TO_M+9" = 'YES'
	,"STAGE_M+9_TO_M+12" = 'YES'
	,INFANT_FLAG = 'YES'
	,UNKNOWN_INFANT_SUBSTAGE = 'N/A'
	,INFANT_4_6M_KEYWORD = '�ا��� PETIT CHOU SSS'
	,INFANT_7_9M_KEYWORD = '�ا��� PETIT CHOU SSS'
	,INFANT_10_12M_KEYWORD = '�ا��� PETIT CHOU SSS'
WHERE MOM_AND_KID_PRODUCT_FLAG = 1
AND CLEANED_BRANDNAME LIKE 'PETIT CHOU'
AND PRODUCT_NAME LIKE '%�ا���%SSS%'
AND COALESCE(PREG_FLAG,'N/A') = 'N/A'
AND COALESCE(INFANT_FLAG,'N/A') = 'N/A'
AND COALESCE(TODDLER_FLAG,'N/A') = 'N/A'
AND COALESCE(PRESCHOOL_FLAG,'N/A') = 'N/A'
AND COALESCE(JUNIOR_FLAG,'N/A') = 'N/A'
AND COALESCE(TEEN_FLAG,'N/A') = 'N/A'
;

--9M-2Y
UPDATE vpm_data.pm_skutagging_kids_product_master
SET "STAGE_M+0_TO_M+3" = 'N/A'
	,"STAGE_M+3_TO_M+6" = 'N/A'
	,"STAGE_M+6_TO_M+9" = 'N/A'
	,"STAGE_M+9_TO_M+12" = 'YES'
	,INFANT_FLAG = 'YES'
	,UNKNOWN_INFANT_SUBSTAGE = 'N/A'
	,TODDLER_FLAG = 'YES'
	,INFANT_10_12M_KEYWORD = '��ǡ PETIT CHOU'
	,TODDLER_KEYWORD = '��ǡ PETIT CHOU'
WHERE MOM_AND_KID_PRODUCT_FLAG = 1
AND CLEANED_BRANDNAME LIKE 'PETIT CHOU'
AND PRODUCT_NAME LIKE '%��ǡ%'
AND PRODUCT_NAME NOT LIKE '%[0-9][0-9]M%'
AND PRODUCT_NAME NOT LIKE '%[0-9][0-9]Y%'
AND COALESCE(PREG_FLAG,'N/A') = 'N/A'
AND COALESCE(INFANT_FLAG,'N/A') = 'N/A'
AND COALESCE(TODDLER_FLAG,'N/A') = 'N/A'
AND COALESCE(PRESCHOOL_FLAG,'N/A') = 'N/A'
AND COALESCE(JUNIOR_FLAG,'N/A') = 'N/A'
AND COALESCE(TEEN_FLAG,'N/A') = 'N/A'
;

--1-2Y
UPDATE vpm_data.pm_skutagging_kids_product_master
SET INFANT_FLAG = 'N/A'
	,TODDLER_FLAG = 'YES'
	,TODDLER_KEYWORD = '�ا��� PETIT CHOU MMM'
WHERE MOM_AND_KID_PRODUCT_FLAG = 1
AND CLEANED_BRANDNAME LIKE 'PETIT CHOU'
AND PRODUCT_NAME LIKE '%�ا���%MMM%'
AND COALESCE(PREG_FLAG,'N/A') = 'N/A'
AND COALESCE(INFANT_FLAG,'N/A') = 'N/A'
AND COALESCE(TODDLER_FLAG,'N/A') = 'N/A'
AND COALESCE(PRESCHOOL_FLAG,'N/A') = 'N/A'
AND COALESCE(JUNIOR_FLAG,'N/A') = 'N/A'
AND COALESCE(TEEN_FLAG,'N/A') = 'N/A'
;

--1-2Y
UPDATE vpm_data.pm_skutagging_kids_product_master
SET INFANT_FLAG = 'N/A'
	,TODDLER_FLAG = 'YES'
	,TODDLER_KEYWORD = (CASE WHEN PRODUCT_NAME LIKE '%1 2Y%' THEN '1-2 Y'
									WHEN PRODUCT_NAME LIKE '% 2Y%' THEN '1-2 Y'
									WHEN PRODUCT_NAME LIKE '%02Y%' THEN '1-2 Y'
									WHEN PRODUCT_NAME LIKE '%๤�%' THEN '๤������'
									WHEN PRODUCT_NAME LIKE '%�١�е���%' THEN '�١�е��������'
									WHEN PRODUCT_NAME LIKE '%���������%' THEN '��������������'
									ELSE TODDLER_KEYWORD END)
WHERE MOM_AND_KID_PRODUCT_FLAG = 1
AND CLEANED_BRANDNAME LIKE 'PETIT CHOU'
AND COALESCE(PREG_FLAG,'N/A') = 'N/A'
AND COALESCE(INFANT_FLAG,'N/A') = 'N/A'
AND COALESCE(TODDLER_FLAG,'N/A') = 'N/A'
AND COALESCE(PRESCHOOL_FLAG,'N/A') = 'N/A'
AND COALESCE(JUNIOR_FLAG,'N/A') = 'N/A'
AND COALESCE(TEEN_FLAG,'N/A') = 'N/A'
AND (PRODUCT_NAME LIKE '%1 2Y%'
	OR PRODUCT_NAME LIKE '% 2Y%'
	OR PRODUCT_NAME LIKE '%02Y%'
	OR PRODUCT_NAME LIKE '%๤�%'--1-3Y
	OR PRODUCT_NAME LIKE '%�١�е���%'
	OR PRODUCT_NAME LIKE '%���������%')
;

--NOT INFANT (2-4Y TODDLER)
UPDATE vpm_data.pm_skutagging_kids_product_master
SET INFANT_FLAG = 'N/A'
	,TODDLER_FLAG = 'YES'
	,TODDLER_KEYWORD = (CASE WHEN PRODUCT_NAME LIKE '%�ا���%LLL%' THEN '�ا��� PETIT CHOU LLL'
								WHEN PRODUCT_NAME LIKE '%03Y%' THEN '2-3 Y'
								WHEN PRODUCT_NAME LIKE '%04Y%' THEN '3-4 Y'
								WHEN (PRODUCT_NAME LIKE '%DIAPER%' AND PRODUCT_NAME LIKE '%XL%') THEN 'DIAPER SIZE XL'
								WHEN (PRODUCT_NAME LIKE '%MAMYPOKO%' AND PRODUCT_NAME LIKE '%XL%') THEN 'DIAPER SIZE XL'
								WHEN (REVISED_CATEGORY_LEVEL_2 = 'DIAPERS' AND PRODUCT_NAME LIKE '%XL%') THEN 'DIAPER SIZE XL'
								WHEN (CLASS_NAME = 'BABY DIAPERS' AND PRODUCT_NAME LIKE '%XL%') THEN 'DIAPER SIZE XL'
								WHEN PRODUCT_NAME LIKE '%����¡%' THEN '����¡'
								WHEN PRODUCT_NAME LIKE '%36' THEN '24-36 MTH'
								ELSE TODDLER_KEYWORD END)
WHERE MOM_AND_KID_PRODUCT_FLAG = 1
AND COALESCE(PREG_FLAG,'N/A') = 'N/A'
AND COALESCE(INFANT_FLAG,'N/A') = 'N/A'
AND COALESCE(TODDLER_FLAG,'N/A') = 'N/A'
AND COALESCE(PRESCHOOL_FLAG,'N/A') = 'N/A'
AND COALESCE(JUNIOR_FLAG,'N/A') = 'N/A'
AND COALESCE(TEEN_FLAG,'N/A') = 'N/A'
AND ((CLEANED_BRANDNAME LIKE 'PETIT CHOU' AND (PRODUCT_NAME LIKE '%�ا���%LLL%'
										OR PRODUCT_NAME LIKE '%03Y%'
										OR PRODUCT_NAME LIKE '%04Y%'))
	OR (PRODUCT_NAME LIKE '%DIAPER%' AND PRODUCT_NAME LIKE '%XL%')
	OR (PRODUCT_NAME LIKE '%MAMYPOKO%' AND PRODUCT_NAME LIKE '%XL%')
	OR (REVISED_CATEGORY_LEVEL_2 = 'DIAPERS' AND PRODUCT_NAME LIKE '%XL%')
	OR (CLASS_NAME = 'BABY DIAPERS' AND PRODUCT_NAME LIKE '%XL%')
	OR PRODUCT_NAME LIKE '%����¡%'
	OR PRODUCT_NAME LIKE '%36')
;

--NOT INFANT (4-6Y PRESCHOOL)
UPDATE vpm_data.pm_skutagging_kids_product_master
SET INFANT_FLAG = 'N/A'
	,TODDLER_FLAG = 'N/A'
	,PRESCHOOL_FLAG = 'YES'
WHERE MOM_AND_KID_PRODUCT_FLAG = 1
AND CLEANED_BRANDNAME LIKE 'PETIT CHOU'
AND PRODUCT_NAME LIKE '%06Y%'
AND COALESCE(PREG_FLAG,'N/A') = 'N/A'
AND COALESCE(INFANT_FLAG,'N/A') = 'N/A'
AND COALESCE(TODDLER_FLAG,'N/A') = 'N/A'
AND COALESCE(PRESCHOOL_FLAG,'N/A') = 'N/A'
AND COALESCE(JUNIOR_FLAG,'N/A') = 'N/A'
AND COALESCE(TEEN_FLAG,'N/A') = 'N/A'
;

/* BEDDING FOR 0-3 MONTHS */ --FROM INFANT 3.0 ++
UPDATE vpm_data.pm_skutagging_kids_product_master
SET "STAGE_M+0_TO_M+3" = 'YES'
	,"STAGE_M+3_TO_M+6" = 'N/A'
	,"STAGE_M+6_TO_M+9" = 'N/A'
	,"STAGE_M+9_TO_M+12" = 'N/A'
	,INFANT_FLAG = 'YES'
	,UNKNOWN_INFANT_SUBSTAGE = 'N/A'
	,INFANT_0_3M_KEYWORD = 'BABY BOUNCER'
WHERE MOM_AND_KID_PRODUCT_FLAG = 1
AND REVISED_CATEGORY_LEVEL_2 = 'KID FURNITURE'
AND COALESCE(PREG_FLAG,'N/A') = 'N/A'
AND COALESCE(INFANT_FLAG,'N/A') = 'N/A'
AND COALESCE(TODDLER_FLAG,'N/A') = 'N/A'
AND COALESCE(PRESCHOOL_FLAG,'N/A') = 'N/A'
AND COALESCE(JUNIOR_FLAG,'N/A') = 'N/A'
AND COALESCE(TEEN_FLAG,'N/A') = 'N/A'
AND (PRODUCT_NAME LIKE '%BOUNCER%'
	OR PRODUCT_NAME LIKE '%ROCK%'
	OR PRODUCT_NAME LIKE '%SWING%'
	OR PRODUCT_NAME LIKE '%���¡%'
	OR PRODUCT_NAME LIKE '%����%'
	OR PRODUCT_NAME LIKE '%�Ԫ�����ë��Ź͹%'
	OR PRODUCT_NAME LIKE '%CARRY%COT%')
;

--0-6M
UPDATE vpm_data.pm_skutagging_kids_product_master
SET "STAGE_M+0_TO_M+3" = 'YES'
	,"STAGE_M+3_TO_M+6" = 'YES'
	,"STAGE_M+6_TO_M+9" = 'N/A'
	,"STAGE_M+9_TO_M+12" = 'N/A'
	,INFANT_FLAG = 'YES'
	,UNKNOWN_INFANT_SUBSTAGE = 'N/A'
	,INFANT_0_3M_KEYWORD = '0-6 MTH'
	,INFANT_4_6M_KEYWORD = '0-6 MTH'
WHERE MOM_AND_KID_PRODUCT_FLAG = 1
AND REVISED_CATEGORY_LEVEL_2 = 'KID FURNITURE'
AND COALESCE(PREG_FLAG,'N/A') = 'N/A'
AND COALESCE(INFANT_FLAG,'N/A') = 'N/A'
AND COALESCE(TODDLER_FLAG,'N/A') = 'N/A'
AND COALESCE(PRESCHOOL_FLAG,'N/A') = 'N/A'
AND COALESCE(JUNIOR_FLAG,'N/A') = 'N/A'
AND COALESCE(TEEN_FLAG,'N/A') = 'N/A'
AND (PRODUCT_NAME LIKE '%0 6��͹%')
;

/* BEDDING FOR >3 MONTHS */ --FROM INFANT 3.0 ++
UPDATE vpm_data.pm_skutagging_kids_product_master
SET "STAGE_M+0_TO_M+3" = 'N/A'
	,"STAGE_M+3_TO_M+6" = 'YES'
	,"STAGE_M+6_TO_M+9" = 'YES'
	,"STAGE_M+9_TO_M+12" = 'YES'
	,INFANT_FLAG = 'YES'
	,UNKNOWN_INFANT_SUBSTAGE = 'N/A'
	,TODDLER_FLAG = 'YES'
	,INFANT_4_6M_KEYWORD = 'PLAYPEN'
	,INFANT_7_9M_KEYWORD = 'PLAYPEN'
	,INFANT_10_12M_KEYWORD = 'PLAYPEN'
	,TODDLER_KEYWORD = 'PLAYPEN'
WHERE MOM_AND_KID_PRODUCT_FLAG = 1
AND REVISED_CATEGORY_LEVEL_2 = 'KID FURNITURE'
AND COALESCE(PREG_FLAG,'N/A') = 'N/A'
AND COALESCE(INFANT_FLAG,'N/A') = 'N/A'
AND COALESCE(TODDLER_FLAG,'N/A') = 'N/A'
AND COALESCE(PRESCHOOL_FLAG,'N/A') = 'N/A'
AND COALESCE(JUNIOR_FLAG,'N/A') = 'N/A'
AND COALESCE(TEEN_FLAG,'N/A') = 'N/A'
AND (PRODUCT_NAME LIKE '%��ྐྵ%'
	OR PRODUCT_NAME LIKE '%PLAYPEN%'
	OR PRODUCT_NAME LIKE '%����%'
	OR PRODUCT_NAME LIKE '%��ྐྵ%'
	OR PRODUCT_NAME LIKE '%PLAYARD%')
;

--6-12M
UPDATE vpm_data.pm_skutagging_kids_product_master
SET "STAGE_M+0_TO_M+3" = 'N/A'
	,"STAGE_M+3_TO_M+6" = 'N/A'
	,"STAGE_M+6_TO_M+9" = 'YES'
	,"STAGE_M+9_TO_M+12" = 'YES'
	,INFANT_FLAG = 'YES'
	,UNKNOWN_INFANT_SUBSTAGE = 'N/A'
	,INFANT_7_9M_KEYWORD = (CASE WHEN PRODUCT_NAME LIKE '%�Ѵ����%' THEN '�����Ѵ����'
									WHEN PRODUCT_NAME LIKE '%JUMPER%' THEN 'JUMPER'
									ELSE INFANT_7_9M_KEYWORD END)
	,INFANT_10_12M_KEYWORD = (CASE WHEN PRODUCT_NAME LIKE '%�Ѵ����%' THEN '�����Ѵ����'
									WHEN PRODUCT_NAME LIKE '%JUMPER%' THEN 'JUMPER'
									ELSE INFANT_7_9M_KEYWORD END)
WHERE MOM_AND_KID_PRODUCT_FLAG = 1
AND COALESCE(PREG_FLAG,'N/A') = 'N/A'
AND COALESCE(INFANT_FLAG,'N/A') = 'N/A'
AND COALESCE(TODDLER_FLAG,'N/A') = 'N/A'
AND COALESCE(PRESCHOOL_FLAG,'N/A') = 'N/A'
AND COALESCE(JUNIOR_FLAG,'N/A') = 'N/A'
AND COALESCE(TEEN_FLAG,'N/A') = 'N/A'
AND (PRODUCT_NAME LIKE '%�Ѵ����%'
	OR (REVISED_CATEGORY_LEVEL_2 = 'KID FURNITURE' AND PRODUCT_NAME LIKE '%JUMPER%'))
;

--6-2Y FURNITURE
UPDATE vpm_data.pm_skutagging_kids_product_master
SET "STAGE_M+0_TO_M+3" = 'N/A'
	,"STAGE_M+3_TO_M+6" = 'N/A'
	,"STAGE_M+6_TO_M+9" = 'YES'
	,"STAGE_M+9_TO_M+12" = 'YES'
	,INFANT_FLAG = 'YES'
	,UNKNOWN_INFANT_SUBSTAGE = 'N/A'
	,TODDLER_FLAG = 'YES'
	,INFANT_7_9M_KEYWORD = '������֡�ҹ'
	,INFANT_10_12M_KEYWORD = '������֡�ҹ'
	,TODDLER_KEYWORD = '������֡�ҹ'
WHERE MOM_AND_KID_PRODUCT_FLAG = 1
AND REVISED_CATEGORY_LEVEL_2 = 'KID FURNITURE'
AND COALESCE(PREG_FLAG,'N/A') = 'N/A'
AND COALESCE(INFANT_FLAG,'N/A') = 'N/A'
AND COALESCE(TODDLER_FLAG,'N/A') = 'N/A'
AND COALESCE(PRESCHOOL_FLAG,'N/A') = 'N/A'
AND COALESCE(JUNIOR_FLAG,'N/A') = 'N/A'
AND COALESCE(TEEN_FLAG,'N/A') = 'N/A'
AND (PRODUCT_NAME LIKE '%BOOSTER%SEAT%'
	OR PRODUCT_NAME LIKE '%HIGH%CHAIR%'
	OR PRODUCT_NAME LIKE '%������ҹ����%')
;

--6-2Y
UPDATE vpm_data.pm_skutagging_kids_product_master
SET "STAGE_M+0_TO_M+3" = 'N/A'
	,"STAGE_M+3_TO_M+6" = 'N/A'
	,"STAGE_M+6_TO_M+9" = 'YES'
	,"STAGE_M+9_TO_M+12" = 'YES'
	,INFANT_FLAG = 'YES'
	,UNKNOWN_INFANT_SUBSTAGE = 'N/A'
	,TODDLER_FLAG = 'YES'
	,INFANT_7_9M_KEYWORD = '���ͧ�ѹ�պ'
	,INFANT_10_12M_KEYWORD = '���ͧ�ѹ�պ'
	,TODDLER_KEYWORD = '���ͧ�ѹ�պ'
WHERE MOM_AND_KID_PRODUCT_FLAG = 1
AND COALESCE(PREG_FLAG,'N/A') = 'N/A'
AND COALESCE(INFANT_FLAG,'N/A') = 'N/A'
AND COALESCE(TODDLER_FLAG,'N/A') = 'N/A'
AND COALESCE(PRESCHOOL_FLAG,'N/A') = 'N/A'
AND COALESCE(JUNIOR_FLAG,'N/A') = 'N/A'
AND COALESCE(TEEN_FLAG,'N/A') = 'N/A'
AND (PRODUCT_NAME LIKE '%�Ҫ�к�èء��ͧ��%'
	OR PRODUCT_NAME LIKE '%���ͧ�����ѹ�պ%'
	OR (PRODUCT_NAME LIKE '����ͧ����й����� 3���' AND CLEANED_BRANDNAME LIKE 'CAMERA'))
;

--1-2Y
UPDATE vpm_data.pm_skutagging_kids_product_master
SET INFANT_FLAG = 'N/A'
	,TODDLER_FLAG = 'YES'
	,TODDLER_KEYWORD = 'ô�Ѵ�պ������'
WHERE MOM_AND_KID_PRODUCT_FLAG = 1
AND COALESCE(PREG_FLAG,'N/A') = 'N/A'
AND COALESCE(INFANT_FLAG,'N/A') = 'N/A'
AND COALESCE(TODDLER_FLAG,'N/A') = 'N/A'
AND COALESCE(PRESCHOOL_FLAG,'N/A') = 'N/A'
AND COALESCE(JUNIOR_FLAG,'N/A') = 'N/A'
AND COALESCE(TEEN_FLAG,'N/A') = 'N/A'
AND PRODUCT_NAME LIKE '%ô�Ѵ�պ������%'
;

UPDATE vpm_data.pm_skutagging_kids_product_master
SET PREG_FLAG = 'YES'
	,"STAGE_M-9_TO_M-6" = 'N/A'
	,"STAGE_M-6_TO_M-3" = 'N/A'
	,"STAGE_M-3_TO_M-0" = 'YES'
	,UNKNOWN_PREG_SUBSTAGE = 'N/A'
	,INFANT_FLAG = 'YES'
	,"STAGE_M+0_TO_M+3" = 'YES'
	,"STAGE_M+3_TO_M+6" = 'YES'
	,"STAGE_M+6_TO_M+9" = 'YES'
	,"STAGE_M+9_TO_M+12" = 'YES'
	,PREG_7_9M_KEYWORD = 'PARENTING BOOK'
	,INFANT_0_3M_KEYWORD = 'PARENTING BOOK'
	,INFANT_4_6M_KEYWORD = 'PARENTING BOOK'
WHERE MOM_AND_KID_PRODUCT_FLAG = 1
AND COALESCE(PREG_FLAG,'N/A') = 'N/A'
AND COALESCE(INFANT_FLAG,'N/A') = 'N/A'
AND COALESCE(TODDLER_FLAG,'N/A') = 'N/A'
AND COALESCE(PRESCHOOL_FLAG,'N/A') = 'N/A'
AND COALESCE(JUNIOR_FLAG,'N/A') = 'N/A'
AND COALESCE(TEEN_FLAG,'N/A') = 'N/A'
AND PRODUCT_NAME LIKE '%���ҧ���Ե���Ȩ������¹�ӹ�%'
;

--UNKNOWN SUB-STAGE
UPDATE vpm_data.pm_skutagging_kids_product_master
SET INFANT_FLAG = 'YES'
	,UNKNOWN_INFANT_SUBSTAGE = 'YES'
	,INFANT_UNKNOWN_KEYWORD = (CASE WHEN CLEANED_BRANDNAME LIKE 'PETIT CHOU' THEN 'PETIT CHOU'
									WHEN PRODUCT_NAME LIKE '%����%��%' THEN '��������'
									WHEN PRODUCT_NAME LIKE '%����%��%' THEN '��������'
									WHEN PRODUCT_NAME LIKE '%PUMP%' THEN '��������'
									WHEN PRODUCT_NAME LIKE '%�ا��%��%' THEN '�ا�纹�ӹ�'
									WHEN PRODUCT_NAME LIKE '%����%' THEN '����'
									WHEN PRODUCT_NAME LIKE '%�Ǵ%��%' THEN '�Ǵ��'
									WHEN PRODUCT_NAME LIKE '%����%' THEN '����'
									WHEN PRODUCT_NAME LIKE '%����觹�%' THEN '����'
									WHEN PRODUCT_NAME LIKE '%�ǧ��%' THEN '����'
									WHEN PRODUCT_NAME LIKE '%����ͧ�Ǵ��ҹ�%' THEN '����ͧ�Ǵ��ҹ�'
									WHEN PRODUCT_NAME LIKE '%�蹫Ѻ��ӹ�%' THEN '�蹫Ѻ��ӹ�'
									WHEN PRODUCT_NAME LIKE '%�蹫���⤹%��ǹ�%' THEN 'NIPPLE SHIELD'
									WHEN PRODUCT_NAME LIKE '%���¹����%' THEN '�����'
									WHEN PRODUCT_NAME LIKE '%ö��%' THEN 'STROLLER'
									WHEN PRODUCT_NAME LIKE '%����շ%' THEN 'CAR SEAT'
									WHEN PRODUCT_NAME LIKE '%CAR SEAT%' THEN 'CAR SEAT'
									WHEN PRODUCT_NAME LIKE '%CARSEAT%' THEN 'CAR SEAT'
									WHEN PRODUCT_NAME LIKE '%�ҫշ%' THEN 'CAR SEAT'
									WHEN PRODUCT_NAME LIKE '������%' THEN 'CAR SEAT'
									WHEN PRODUCT_NAME LIKE '%��еԴ%' THEN 'CAR SEAT'
									WHEN PRODUCT_NAME LIKE '%STROLLER%' THEN 'STROLLER'
									WHEN PRODUCT_NAME LIKE '%������%' THEN 'BABY CARRIER'
									WHEN PRODUCT_NAME LIKE '%NEXTFIT%' THEN 'CAR SEAT'
									WHEN PRODUCT_NAME LIKE '%WEGO LONG%' THEN 'CAR SEAT'
									WHEN PRODUCT_NAME LIKE '%COCOON%TRAVEL%' THEN 'STROLLER'
									ELSE INFANT_UNKNOWN_KEYWORD END)
WHERE MOM_AND_KID_PRODUCT_FLAG = 1
AND COALESCE(PREG_FLAG,'N/A') = 'N/A'
AND COALESCE(INFANT_FLAG,'N/A') = 'N/A'
AND COALESCE(TODDLER_FLAG,'N/A') = 'N/A'
AND COALESCE(PRESCHOOL_FLAG,'N/A') = 'N/A'
AND COALESCE(JUNIOR_FLAG,'N/A') = 'N/A'
AND COALESCE(TEEN_FLAG,'N/A') = 'N/A'
AND (CLEANED_BRANDNAME LIKE 'PETIT CHOU'
	OR PRODUCT_NAME LIKE '%����%��%'
	OR PRODUCT_NAME LIKE '%����%��%'
	OR PRODUCT_NAME LIKE '%PUMP%'
	OR PRODUCT_NAME LIKE '%�ا��%��%'
	OR PRODUCT_NAME LIKE '%����%'
	OR PRODUCT_NAME LIKE '%�Ǵ%��%'
	OR PRODUCT_NAME LIKE '%����%'
	OR PRODUCT_NAME LIKE '%����觹�%'
	OR PRODUCT_NAME LIKE '%�ǧ��%'
	OR PRODUCT_NAME LIKE '%����ͧ�Ǵ��ҹ�%'
	OR PRODUCT_NAME LIKE '%�蹫Ѻ��ӹ�%'
	OR PRODUCT_NAME LIKE '%�蹫���⤹%��ǹ�%'
	OR PRODUCT_NAME LIKE '%���¹����%'
	OR PRODUCT_NAME LIKE '%���ҧ���Ե���Ȩ������¹�ӹ�%'
	OR (REVISED_CATEGORY_LEVEL_2 IN ('KID FURNITURE','KID ACCESSORIES') AND (PRODUCT_NAME LIKE '%ö��%'
																			OR PRODUCT_NAME LIKE '%����շ%'
																			OR PRODUCT_NAME LIKE '%CAR SEAT%'
																			OR PRODUCT_NAME LIKE '%CARSEAT%'
																			OR PRODUCT_NAME LIKE '%�ҫշ%'
																			OR PRODUCT_NAME LIKE '������%'
																			OR PRODUCT_NAME LIKE '%��еԴ%'
																			OR PRODUCT_NAME LIKE '%STROLLER%'
																			OR PRODUCT_NAME LIKE '%������%'
																			OR PRODUCT_NAME LIKE '%NEXTFIT%'
																			OR PRODUCT_NAME LIKE '%WEGO LONG%'
																			OR PRODUCT_NAME LIKE '%COCOON%TRAVEL%')))
;

--MORE KID
--FEEDING BOTTLE AND ACCESSORIES
--BOTTLE 2OZ, 50ML/60ML FOR 0-3M
UPDATE vpm_data.pm_skutagging_kids_product_master
SET "STAGE_M+0_TO_M+3" = 'YES'
	,"STAGE_M+3_TO_M+6" = 'N/A'
	,"STAGE_M+6_TO_M+9" = 'N/A'
	,"STAGE_M+9_TO_M+12" = 'N/A'
	,INFANT_FLAG = 'YES'
	,UNKNOWN_INFANT_SUBSTAGE = 'N/A'
	,INFANT_0_3M_KEYWORD = (CASE WHEN PRODUCT_NAME LIKE '% 2 OZ%' THEN '�Ǵ�� 2 OZ'
									WHEN PRODUCT_NAME LIKE '% 2OZ%' THEN '�Ǵ�� 2 OZ'
									WHEN PRODUCT_NAME LIKE '% 50%ML%' THEN '�Ǵ�� 50 ML'
									WHEN PRODUCT_NAME LIKE '% 60%ML%' THEN '�Ǵ�� 60 ML'
									ELSE INFANT_0_3M_KEYWORD END)
WHERE MOM_AND_KID_PRODUCT_FLAG = 1
AND COALESCE(PREG_FLAG,'N/A') = 'N/A'
AND COALESCE(INFANT_FLAG,'N/A') = 'N/A'
AND COALESCE(TODDLER_FLAG,'N/A') = 'N/A'
AND COALESCE(PRESCHOOL_FLAG,'N/A') = 'N/A'
AND COALESCE(JUNIOR_FLAG,'N/A') = 'N/A'
AND COALESCE(TEEN_FLAG,'N/A') = 'N/A'
AND REVISED_CATEGORY_LEVEL_1 = 'KIDS'
AND REVISED_CATEGORY_LEVEL_3 = 'FEEDING BOTTLE AND ACCESSORIES'
AND (PRODUCT_NAME LIKE '% 2 OZ%'
	OR PRODUCT_NAME LIKE '% 2OZ%'
	OR PRODUCT_NAME LIKE '% 50%ML%'
	OR PRODUCT_NAME LIKE '% 60%ML%')
AND PRODUCT_NAME NOT LIKE '%500%ML%'
AND PRODUCT_NAME NOT LIKE '%����%'
;

--BOTTLE 4/5 OZ, 120/125/150 ML FOR 0-6M
UPDATE vpm_data.pm_skutagging_kids_product_master
SET "STAGE_M+0_TO_M+3" = 'YES'
	,"STAGE_M+3_TO_M+6" = 'YES'
	,"STAGE_M+6_TO_M+9" = 'N/A'
	,"STAGE_M+9_TO_M+12" = 'N/A'
	,INFANT_FLAG = 'YES'
	,UNKNOWN_INFANT_SUBSTAGE = 'N/A'
	,INFANT_0_3M_KEYWORD = (CASE WHEN PRODUCT_NAME LIKE '%4%OZ%' THEN '�Ǵ�� 4 OZ'
									WHEN PRODUCT_NAME LIKE '%5%OZ%' THEN '�Ǵ�� 5 OZ'
									WHEN PRODUCT_NAME LIKE '%120%ML%' THEN '�Ǵ�� 120 ML'
									WHEN PRODUCT_NAME LIKE '%125%ML%' THEN '�Ǵ�� 125 ML'
									WHEN PRODUCT_NAME LIKE '%150%ML%' THEN '�Ǵ�� 150 ML'
									WHEN PRODUCT_NAME LIKE '%5 �͹��%' THEN '�Ǵ�� 5 OZ'
									ELSE INFANT_0_3M_KEYWORD END)
	,INFANT_4_6M_KEYWORD = (CASE WHEN PRODUCT_NAME LIKE '%4%OZ%' THEN '�Ǵ�� 4 OZ'
									WHEN PRODUCT_NAME LIKE '%5%OZ%' THEN '�Ǵ�� 5 OZ'
									WHEN PRODUCT_NAME LIKE '%120%ML%' THEN '�Ǵ�� 120 ML'
									WHEN PRODUCT_NAME LIKE '%125%ML%' THEN '�Ǵ�� 125 ML'
									WHEN PRODUCT_NAME LIKE '%150%ML%' THEN '�Ǵ�� 150 ML'
									WHEN PRODUCT_NAME LIKE '%5 �͹��%' THEN '�Ǵ�� 5 OZ'
									ELSE INFANT_4_6M_KEYWORD END)
WHERE MOM_AND_KID_PRODUCT_FLAG = 1
AND COALESCE(PREG_FLAG,'N/A') = 'N/A'
AND COALESCE(INFANT_FLAG,'N/A') = 'N/A'
AND COALESCE(TODDLER_FLAG,'N/A') = 'N/A'
AND COALESCE(PRESCHOOL_FLAG,'N/A') = 'N/A'
AND COALESCE(JUNIOR_FLAG,'N/A') = 'N/A'
AND COALESCE(TEEN_FLAG,'N/A') = 'N/A'
AND REVISED_CATEGORY_LEVEL_1 = 'KIDS'
AND REVISED_CATEGORY_LEVEL_3 = 'FEEDING BOTTLE AND ACCESSORIES'
AND (PRODUCT_NAME LIKE '%4%OZ%'
	OR PRODUCT_NAME LIKE '%5%OZ%'
	OR PRODUCT_NAME LIKE '%120%ML%'
	OR PRODUCT_NAME LIKE '%125%ML%'
	OR PRODUCT_NAME LIKE '%150%ML%'
	OR PRODUCT_NAME LIKE '%5 �͹��%')
AND PRODUCT_NAME NOT LIKE '%FOOD%CONTAINER%'
AND PRODUCT_NAME NOT LIKE '%��лء���%'
AND PRODUCT_NAME NOT LIKE '%��Ǥ�鹹�Ӽ����%'
AND PRODUCT_NAME NOT LIKE '%BABY PORTION%'
AND PRODUCT_NAME NOT LIKE '%SQUEEZE POUCHES%'
AND PRODUCT_NAME NOT LIKE '%�ا�纹�ӹ�%'
AND PRODUCT_NAME NOT LIKE '%HANDLE CUP%'
AND PRODUCT_NAME NOT LIKE '%SPOON FEEDER%'
AND PRODUCT_NAME NOT LIKE '%CLIP PORTION%'
AND PRODUCT_NAME NOT LIKE '%FEEDERCEREAL%'
;

--BOTTLE 8/9 OZ, 240/250/300 ML FOR 7-24M
UPDATE vpm_data.pm_skutagging_kids_product_master
SET "STAGE_M+0_TO_M+3" = 'N/A'
	,"STAGE_M+3_TO_M+6" = 'N/A'
	,"STAGE_M+6_TO_M+9" = 'YES'
	,"STAGE_M+9_TO_M+12" = 'YES'
	,INFANT_FLAG = 'YES'
	,UNKNOWN_INFANT_SUBSTAGE = 'N/A'
	,TODDLER_FLAG = 'YES'
	,INFANT_7_9M_KEYWORD = (CASE WHEN PRODUCT_NAME LIKE '%8%OZ%' THEN '�Ǵ�� 8 OZ'
								WHEN PRODUCT_NAME LIKE '%9%OZ%'THEN '�Ǵ�� 9 OZ'
								WHEN PRODUCT_NAME LIKE '%12%OZ%'THEN '�Ǵ�� 12 OZ'
								WHEN PRODUCT_NAME LIKE '%300%ML%'THEN '�Ǵ�� 300 ML'
								WHEN PRODUCT_NAME LIKE '%250%ML%'THEN '�Ǵ�� 250 ML'
								WHEN PRODUCT_NAME LIKE '%250'THEN '�Ǵ�� 250 ML'
								WHEN PRODUCT_NAME LIKE '%240%ML%'THEN '�Ǵ�� 240 ML'
								ELSE INFANT_7_9M_KEYWORD END)
	,INFANT_10_12M_KEYWORD = (CASE WHEN PRODUCT_NAME LIKE '%8%OZ%' THEN '�Ǵ�� 8 OZ'
								WHEN PRODUCT_NAME LIKE '%9%OZ%'THEN '�Ǵ�� 9 OZ'
								WHEN PRODUCT_NAME LIKE '%12%OZ%'THEN '�Ǵ�� 12 OZ'
								WHEN PRODUCT_NAME LIKE '%300%ML%'THEN '�Ǵ�� 300 ML'
								WHEN PRODUCT_NAME LIKE '%250%ML%'THEN '�Ǵ�� 250 ML'
								WHEN PRODUCT_NAME LIKE '%250'THEN '�Ǵ�� 250 ML'
								WHEN PRODUCT_NAME LIKE '%240%ML%'THEN '�Ǵ�� 240 ML'
								ELSE INFANT_10_12M_KEYWORD END)
	,TODDLER_KEYWORD = (CASE WHEN PRODUCT_NAME LIKE '%8%OZ%' THEN '�Ǵ�� 8 OZ'
								WHEN PRODUCT_NAME LIKE '%9%OZ%'THEN '�Ǵ�� 9 OZ'
								WHEN PRODUCT_NAME LIKE '%12%OZ%'THEN '�Ǵ�� 12 OZ'
								WHEN PRODUCT_NAME LIKE '%300%ML%'THEN '�Ǵ�� 300 ML'
								WHEN PRODUCT_NAME LIKE '%250%ML%'THEN '�Ǵ�� 250 ML'
								WHEN PRODUCT_NAME LIKE '%250'THEN '�Ǵ�� 250 ML'
								WHEN PRODUCT_NAME LIKE '%240%ML%'THEN '�Ǵ�� 240 ML'
								ELSE TODDLER_KEYWORD END)
WHERE MOM_AND_KID_PRODUCT_FLAG = 1
AND COALESCE(PREG_FLAG,'N/A') = 'N/A'
AND COALESCE(INFANT_FLAG,'N/A') = 'N/A'
AND COALESCE(TODDLER_FLAG,'N/A') = 'N/A'
AND COALESCE(PRESCHOOL_FLAG,'N/A') = 'N/A'
AND COALESCE(JUNIOR_FLAG,'N/A') = 'N/A'
AND COALESCE(TEEN_FLAG,'N/A') = 'N/A'
AND REVISED_CATEGORY_LEVEL_1 = 'KIDS'
AND REVISED_CATEGORY_LEVEL_3 = 'FEEDING BOTTLE AND ACCESSORIES'
AND (PRODUCT_NAME LIKE '%8%OZ%'
	OR PRODUCT_NAME LIKE '%8 �͹��%'
	OR PRODUCT_NAME LIKE '%9%OZ%'
	OR PRODUCT_NAME LIKE '%12%OZ%'
	OR PRODUCT_NAME LIKE '%300%ML%'
	OR PRODUCT_NAME LIKE '%250%ML%'
	OR PRODUCT_NAME LIKE '%250'
	OR PRODUCT_NAME LIKE '%240%ML%'
	OR PRODUCT_NAME LIKE '%0 6M%'
	OR PRODUCT_NAME LIKE '%6-18 M%'
	OR PRODUCT_NAME LIKE '% 6M+ %')
AND CLEANED_BRANDNAME NOT LIKE 'DRINK IN THE BOX'
AND PRODUCT_NAME NOT LIKE '%���%'
AND PRODUCT_NAME NOT LIKE '%SPOON FEEDER%'
AND PRODUCT_NAME NOT LIKE '%����%'
AND PRODUCT_NAME NOT LIKE '%�ا�纹�ӹ�%'
AND PRODUCT_NAME NOT LIKE '%CUP%'
AND PRODUCT_NAME NOT LIKE '%NATURAL-H%'
;

--ELSE BOTTLE UNKNOWN SUBSTAGE
UPDATE vpm_data.pm_skutagging_kids_product_master
SET INFANT_FLAG = 'YES'
	,UNKNOWN_INFANT_SUBSTAGE = 'YES'
	,INFANT_UNKNOWN_KEYWORD = '�Ǵ������ػ�ó�'
WHERE MOM_AND_KID_PRODUCT_FLAG = 1
AND COALESCE(PREG_FLAG,'N/A') = 'N/A'
AND COALESCE(INFANT_FLAG,'N/A') = 'N/A'
AND COALESCE(TODDLER_FLAG,'N/A') = 'N/A'
AND COALESCE(PRESCHOOL_FLAG,'N/A') = 'N/A'
AND COALESCE(JUNIOR_FLAG,'N/A') = 'N/A'
AND COALESCE(TEEN_FLAG,'N/A') = 'N/A'
AND REVISED_CATEGORY_LEVEL_1 = 'KIDS'
AND REVISED_CATEGORY_LEVEL_3 = 'FEEDING BOTTLE AND ACCESSORIES'
;

--BATH ACCESSORIES
-- ����Һ��� 0-12M
UPDATE vpm_data.pm_skutagging_kids_product_master
SET "STAGE_M+0_TO_M+3" = 'YES'
	,"STAGE_M+3_TO_M+6" = 'YES'
	,"STAGE_M+6_TO_M+9" = 'YES'
	,"STAGE_M+9_TO_M+12" = 'YES'
	,INFANT_FLAG = 'YES'
	,UNKNOWN_INFANT_SUBSTAGE = 'N/A'
	,INFANT_0_3M_KEYWORD = '����Һ���'
	,INFANT_4_6M_KEYWORD = '����Һ���'
	,INFANT_7_9M_KEYWORD = '����Һ���'
	,INFANT_10_12M_KEYWORD = '����Һ���'
WHERE MOM_AND_KID_PRODUCT_FLAG = 1
AND COALESCE(PREG_FLAG,'N/A') = 'N/A'
AND COALESCE(INFANT_FLAG,'N/A') = 'N/A'
AND COALESCE(TODDLER_FLAG,'N/A') = 'N/A'
AND COALESCE(PRESCHOOL_FLAG,'N/A') = 'N/A'
AND COALESCE(JUNIOR_FLAG,'N/A') = 'N/A'
AND COALESCE(TEEN_FLAG,'N/A') = 'N/A'
AND REVISED_CATEGORY_LEVEL_1 = 'KIDS'
AND REVISED_CATEGORY_LEVEL_3 = 'KID BATHROOM ACCESSORIES'
AND (PRODUCT_NAME LIKE '%����Һ���%'
	OR PRODUCT_NAME LIKE '%CHANG%TABLE%'
	OR PRODUCT_NAME LIKE '%OCEAN%GALAX%CHANGE%')
;

--BABY BATH SEAT 6-12M
UPDATE vpm_data.pm_skutagging_kids_product_master
SET "STAGE_M+0_TO_M+3" = 'N/A'
	,"STAGE_M+3_TO_M+6" = 'N/A'
	,"STAGE_M+6_TO_M+9" = 'YES'
	,"STAGE_M+9_TO_M+12" = 'YES'
	,INFANT_FLAG = 'YES'
	,UNKNOWN_INFANT_SUBSTAGE = 'N/A'
	,INFANT_7_9M_KEYWORD = (CASE WHEN PRODUCT_NAME LIKE '%BABY BATH SEAT%' THEN 'BABY BATH SEAT'
								WHEN PRODUCT_NAME LIKE '%TOOTHBRUSH%' THEN 'BABY TOOTHBRUSH'
								ELSE INFANT_7_9M_KEYWORD END)
	,INFANT_10_12M_KEYWORD = (CASE WHEN PRODUCT_NAME LIKE '%BABY BATH SEAT%' THEN 'BABY BATH SEAT'
								WHEN PRODUCT_NAME LIKE '%TOOTHBRUSH%' THEN 'BABY TOOTHBRUSH'
								ELSE INFANT_10_12M_KEYWORD END)
WHERE MOM_AND_KID_PRODUCT_FLAG = 1
AND COALESCE(PREG_FLAG,'N/A') = 'N/A'
AND COALESCE(INFANT_FLAG,'N/A') = 'N/A'
AND COALESCE(TODDLER_FLAG,'N/A') = 'N/A'
AND COALESCE(PRESCHOOL_FLAG,'N/A') = 'N/A'
AND COALESCE(JUNIOR_FLAG,'N/A') = 'N/A'
AND COALESCE(TEEN_FLAG,'N/A') = 'N/A'
AND REVISED_CATEGORY_LEVEL_1 = 'KIDS'
AND REVISED_CATEGORY_LEVEL_3 = 'KID BATHROOM ACCESSORIES'
AND (PRODUCT_NAME LIKE '%BABY BATH SEAT%'
	OR (SUBDEPT_NAME = 'BABY S WEAR' AND PRODUCT_NAME LIKE '%TOOTHBRUSH%'))
;

--ELSE BATH ACC UNKNOWN SUBSTAGE
UPDATE vpm_data.pm_skutagging_kids_product_master
SET INFANT_FLAG = 'YES'
	,UNKNOWN_INFANT_SUBSTAGE = 'YES'
	,INFANT_UNKNOWN_KEYWORD = 'BABY BATHROOM ACCESSORIES'
WHERE MOM_AND_KID_PRODUCT_FLAG = 1
AND COALESCE(PREG_FLAG,'N/A') = 'N/A'
AND COALESCE(INFANT_FLAG,'N/A') = 'N/A'
AND COALESCE(TODDLER_FLAG,'N/A') = 'N/A'
AND COALESCE(PRESCHOOL_FLAG,'N/A') = 'N/A'
AND COALESCE(JUNIOR_FLAG,'N/A') = 'N/A'
AND COALESCE(TEEN_FLAG,'N/A') = 'N/A'
AND REVISED_CATEGORY_LEVEL_1 = 'KIDS'
AND REVISED_CATEGORY_LEVEL_3 = 'KID BATHROOM ACCESSORIES'
;

--OTHER KID ACCESSORIES
UPDATE vpm_data.pm_skutagging_kids_product_master
SET INFANT_FLAG = 'YES'
	,UNKNOWN_INFANT_SUBSTAGE = 'YES'
	,INFANT_UNKNOWN_KEYWORD = 'BABY ACCESSORIES'
WHERE MOM_AND_KID_PRODUCT_FLAG = 1
AND COALESCE(PREG_FLAG,'N/A') = 'N/A'
AND COALESCE(INFANT_FLAG,'N/A') = 'N/A'
AND COALESCE(TODDLER_FLAG,'N/A') = 'N/A'
AND COALESCE(PRESCHOOL_FLAG,'N/A') = 'N/A'
AND COALESCE(JUNIOR_FLAG,'N/A') = 'N/A'
AND COALESCE(TEEN_FLAG,'N/A') = 'N/A'
AND REVISED_CATEGORY_LEVEL_1 = 'KIDS'
AND REVISED_CATEGORY_LEVEL_3 = 'OTHER KID ACCESSORIES'
AND (CLASS_NAME = 'BABYSHOP'
	OR CLASS_NAME = 'BABY GOODS')
;

--KID CLOTHING
--0-3M
UPDATE vpm_data.pm_skutagging_kids_product_master
SET "STAGE_M+0_TO_M+3" = 'YES'
	,"STAGE_M+3_TO_M+6" = 'N/A'
	,"STAGE_M+6_TO_M+9" = 'N/A'
	,"STAGE_M+9_TO_M+12" = 'N/A'
	,INFANT_FLAG = 'YES'
	,UNKNOWN_INFANT_SUBSTAGE = 'N/A'
	,INFANT_0_3M_KEYWORD = '0-3 MTH'
WHERE MOM_AND_KID_PRODUCT_FLAG = 1
AND COALESCE(PREG_FLAG,'N/A') = 'N/A'
AND COALESCE(INFANT_FLAG,'N/A') = 'N/A'
AND COALESCE(TODDLER_FLAG,'N/A') = 'N/A'
AND COALESCE(PRESCHOOL_FLAG,'N/A') = 'N/A'
AND COALESCE(JUNIOR_FLAG,'N/A') = 'N/A'
AND COALESCE(TEEN_FLAG,'N/A') = 'N/A'
AND REVISED_CATEGORY_LEVEL_1 = 'KIDS'
AND REVISED_CATEGORY_LEVEL_3 = 'KID CLOTHING'
AND CLASS_NAME = 'BABYSHOP'
AND PRODUCT_NAME LIKE '%0-3'
;

--3-6M
UPDATE vpm_data.pm_skutagging_kids_product_master
SET "STAGE_M+0_TO_M+3" = 'N/A'
	,"STAGE_M+3_TO_M+6" = 'YES'
	,"STAGE_M+6_TO_M+9" = 'N/A'
	,"STAGE_M+9_TO_M+12" = 'N/A'
	,INFANT_FLAG = 'YES'
	,UNKNOWN_INFANT_SUBSTAGE = 'N/A'
	,INFANT_4_6M_KEYWORD = '3-6 MTH'
WHERE MOM_AND_KID_PRODUCT_FLAG = 1
AND COALESCE(PREG_FLAG,'N/A') = 'N/A'
AND COALESCE(INFANT_FLAG,'N/A') = 'N/A'
AND COALESCE(TODDLER_FLAG,'N/A') = 'N/A'
AND COALESCE(PRESCHOOL_FLAG,'N/A') = 'N/A'
AND COALESCE(JUNIOR_FLAG,'N/A') = 'N/A'
AND COALESCE(TEEN_FLAG,'N/A') = 'N/A'
AND REVISED_CATEGORY_LEVEL_1 = 'KIDS'
AND REVISED_CATEGORY_LEVEL_3 = 'KID CLOTHING'
AND CLASS_NAME = 'BABYSHOP'
AND PRODUCT_NAME LIKE '%3-6'
;

--6-12M
UPDATE vpm_data.pm_skutagging_kids_product_master
SET "STAGE_M+0_TO_M+3" = 'N/A'
	,"STAGE_M+3_TO_M+6" = 'N/A'
	,"STAGE_M+6_TO_M+9" = 'YES'
	,"STAGE_M+9_TO_M+12" = 'YES'
	,INFANT_FLAG = 'YES'
	,UNKNOWN_INFANT_SUBSTAGE = 'N/A'
	,INFANT_7_9M_KEYWORD = '6-12 MTH'
	,INFANT_10_12M_KEYWORD = '6-12 MTH'
WHERE MOM_AND_KID_PRODUCT_FLAG = 1
AND COALESCE(PREG_FLAG,'N/A') = 'N/A'
AND COALESCE(INFANT_FLAG,'N/A') = 'N/A'
AND COALESCE(TODDLER_FLAG,'N/A') = 'N/A'
AND COALESCE(PRESCHOOL_FLAG,'N/A') = 'N/A'
AND COALESCE(JUNIOR_FLAG,'N/A') = 'N/A'
AND COALESCE(TEEN_FLAG,'N/A') = 'N/A'
AND REVISED_CATEGORY_LEVEL_1 = 'KIDS'
AND REVISED_CATEGORY_LEVEL_3 = 'KID CLOTHING'
AND CLASS_NAME = 'BABYSHOP'
AND (PRODUCT_NAME LIKE '%6-12'
	OR PRODUCT_NAME LIKE '%6-1')
;

--INFANT CLOTHING FOR 12-24M
UPDATE vpm_data.pm_skutagging_kids_product_master
SET "STAGE_M+0_TO_M+3" = 'N/A'
	,"STAGE_M+3_TO_M+6" = 'N/A'
	,"STAGE_M+6_TO_M+9" = 'YES'
	,"STAGE_M+9_TO_M+12" = 'YES'
	,INFANT_FLAG = 'YES'
	,UNKNOWN_INFANT_SUBSTAGE = 'N/A'
	,INFANT_7_9M_KEYWORD = (CASE WHEN PRODUCT_NAME LIKE '%2Y%' AND PRODUCT_NAME NOT LIKE '%12Y%' THEN '1-2 Y'
								WHEN PRODUCT_NAME LIKE '%2 Y%' THEN '1-2 Y'
								WHEN PRODUCT_NAME LIKE '%12-18 M%' THEN '12-18 MTH'
								WHEN PRODUCT_NAME LIKE '%18-24' THEN '18-24 MTH'
								WHEN PRODUCT_NAME LIKE '%12-18' THEN '12-18 MTH'
								ELSE INFANT_7_9M_KEYWORD END)
	,INFANT_10_12M_KEYWORD = (CASE WHEN PRODUCT_NAME LIKE '%2Y%' AND PRODUCT_NAME NOT LIKE '%12Y%' THEN '1-2 Y'
								WHEN PRODUCT_NAME LIKE '%2 Y%' THEN '1-2 Y'
								WHEN PRODUCT_NAME LIKE '%12-18 M%' THEN '12-18 MTH'
								WHEN PRODUCT_NAME LIKE '%18-24' THEN '18-24 MTH'
								WHEN PRODUCT_NAME LIKE '%12-18' THEN '12-18 MTH'
								ELSE INFANT_10_12M_KEYWORD END)
WHERE MOM_AND_KID_PRODUCT_FLAG = 1
AND COALESCE(PREG_FLAG,'N/A') = 'N/A'
AND COALESCE(INFANT_FLAG,'N/A') = 'N/A'
AND COALESCE(TODDLER_FLAG,'N/A') = 'N/A'
AND COALESCE(PRESCHOOL_FLAG,'N/A') = 'N/A'
AND COALESCE(JUNIOR_FLAG,'N/A') = 'N/A'
AND COALESCE(TEEN_FLAG,'N/A') = 'N/A'
AND REVISED_CATEGORY_LEVEL_1 = 'KIDS'
AND REVISED_CATEGORY_LEVEL_3 = 'KID CLOTHING'
AND ((PRODUCT_NAME LIKE '%2Y%' AND PRODUCT_NAME NOT LIKE '%12Y%')
	OR PRODUCT_NAME LIKE '%2 Y%'
	OR PRODUCT_NAME LIKE '%12-18 M%'
	OR PRODUCT_NAME LIKE '%18-24'
	OR PRODUCT_NAME LIKE '%12-18')
;


--ELSE BABYSHOP UNKNOWN SUBSTAGE
UPDATE vpm_data.pm_skutagging_kids_product_master
SET INFANT_FLAG = 'YES'
	,UNKNOWN_INFANT_SUBSTAGE = 'YES'
	,INFANT_UNKNOWN_KEYWORD = 'BABY CLOTHING'
WHERE MOM_AND_KID_PRODUCT_FLAG = 1
AND COALESCE(PREG_FLAG,'N/A') = 'N/A'
AND COALESCE(INFANT_FLAG,'N/A') = 'N/A'
AND COALESCE(TODDLER_FLAG,'N/A') = 'N/A'
AND COALESCE(PRESCHOOL_FLAG,'N/A') = 'N/A'
AND COALESCE(JUNIOR_FLAG,'N/A') = 'N/A'
AND COALESCE(TEEN_FLAG,'N/A') = 'N/A'
AND REVISED_CATEGORY_LEVEL_1 = 'KIDS'
AND REVISED_CATEGORY_LEVEL_3 = 'KID CLOTHING'
AND CLASS_NAME = 'BABYSHOP'
AND PRODUCT_NAME NOT LIKE '%24-36M%'
AND PRODUCT_NAME NOT LIKE '%24-36'
AND PRODUCT_NAME NOT LIKE '%3Y%'
AND PRODUCT_NAME NOT LIKE '%3 Y%'
AND PRODUCT_NAME NOT LIKE '%4Y%'
AND PRODUCT_NAME NOT LIKE '%4-5Y%'
AND PRODUCT_NAME NOT LIKE '%4-5 Y%'
AND PRODUCT_NAME NOT LIKE '%6Y%'
AND PRODUCT_NAME NOT LIKE '%6-7Y%'
AND PRODUCT_NAME NOT LIKE '%8Y%'
AND PRODUCT_NAME NOT LIKE '%8-9Y%'
AND PRODUCT_NAME NOT LIKE '%9-10Y%'
AND PRODUCT_NAME NOT LIKE '%11-12Y%'
;

--NOT MOM AND KID PRODUCT
UPDATE vpm_data.pm_skutagging_kids_product_master
SET MOM_AND_KID_PRODUCT_FLAG = 0
WHERE MOM_AND_KID_PRODUCT_FLAG = 1
AND COALESCE(PREG_FLAG,'N/A') = 'N/A'
AND COALESCE(INFANT_FLAG,'N/A') = 'N/A'
AND COALESCE(TODDLER_FLAG,'N/A') = 'N/A'
AND COALESCE(PRESCHOOL_FLAG,'N/A') = 'N/A'
AND COALESCE(JUNIOR_FLAG,'N/A') = 'N/A'
AND COALESCE(TEEN_FLAG,'N/A') = 'N/A'
AND REVISED_CATEGORY_LEVEL_1 = 'KIDS'
AND REVISED_CATEGORY_LEVEL_3 = 'KID CLOTHING'
AND ((SUBDEPT_NAME = '' AND CLEANED_BRANDNAME = 'MARKS & SPENCER')
	OR (DEPT_NAME = 'NON-FOOD' AND CLEANED_BRANDNAME = 'CHERILON'))
;

--KID FASHION ACCESSORIES
--BABYSHOP UNKNOWN SUBSTAGE
UPDATE vpm_data.pm_skutagging_kids_product_master
SET INFANT_FLAG = 'YES'
	,UNKNOWN_INFANT_SUBSTAGE = 'YES'
	,INFANT_UNKNOWN_KEYWORD = 'BABY FASHION ACCESSORIES'
WHERE MOM_AND_KID_PRODUCT_FLAG = 1
AND COALESCE(PREG_FLAG,'N/A') = 'N/A'
AND COALESCE(INFANT_FLAG,'N/A') = 'N/A'
AND COALESCE(TODDLER_FLAG,'N/A') = 'N/A'
AND COALESCE(PRESCHOOL_FLAG,'N/A') = 'N/A'
AND COALESCE(JUNIOR_FLAG,'N/A') = 'N/A'
AND COALESCE(TEEN_FLAG,'N/A') = 'N/A'
AND REVISED_CATEGORY_LEVEL_1 = 'KIDS'
AND REVISED_CATEGORY_LEVEL_3 = 'KID FASHION ACCESSORIES'
AND (CLASS_NAME = 'BABYSHOP' OR CLASS_NAME = 'BABY GOODS')
;


--KID NOURISHMENT
--3M+
UPDATE vpm_data.pm_skutagging_kids_product_master
SET "STAGE_M+0_TO_M+3" = 'N/A'
	,"STAGE_M+3_TO_M+6" = 'YES'
	,"STAGE_M+6_TO_M+9" = 'YES'
	,"STAGE_M+9_TO_M+12" = 'YES'
	,INFANT_FLAG = 'YES'
	,UNKNOWN_INFANT_SUBSTAGE = 'N/A'
	,INFANT_4_6M_KEYWORD = 'HEINZ 3M+'
	,INFANT_7_9M_KEYWORD = 'HEINZ 3M+'
	,INFANT_10_12M_KEYWORD = 'HEINZ 3M+'
WHERE MOM_AND_KID_PRODUCT_FLAG = 1
AND COALESCE(PREG_FLAG,'N/A') = 'N/A'
AND COALESCE(INFANT_FLAG,'N/A') = 'N/A'
AND COALESCE(TODDLER_FLAG,'N/A') = 'N/A'
AND COALESCE(PRESCHOOL_FLAG,'N/A') = 'N/A'
AND COALESCE(JUNIOR_FLAG,'N/A') = 'N/A'
AND COALESCE(TEEN_FLAG,'N/A') = 'N/A'
AND REVISED_CATEGORY_LEVEL_1 = 'KIDS'
AND REVISED_CATEGORY_LEVEL_3 = 'KID NOURISHMENT'
AND CLEANED_BRANDNAME = 'HEINZ'
AND (PRODUCT_NAME LIKE 'HEINZ BABY FRUITY APPLE 110G'
	OR PRODUCT_NAME LIKE 'HEINZ BABY APPLE OATMEAL 110G'
	OR PRODUCT_NAME LIKE 'HEINZ BABY RADISH CARROT 110G'
	OR PRODUCT_NAME LIKE 'HEINZ BABY PUMPKIN&CORN 110G'
	OR PRODUCT_NAME LIKE 'HEINZ BABY FOOD APPLE&MANGO110'
	OR PRODUCT_NAME LIKE 'HEINZ BABY PEAR&BANANA 110G')
;

--6M+
UPDATE vpm_data.pm_skutagging_kids_product_master
SET "STAGE_M+0_TO_M+3" = 'N/A'
	,"STAGE_M+3_TO_M+6" = 'N/A'
	,"STAGE_M+6_TO_M+9" = 'YES'
	,"STAGE_M+9_TO_M+12" = 'YES'
	,INFANT_FLAG = 'YES'
	,UNKNOWN_INFANT_SUBSTAGE = 'N/A'
	,INFANT_7_9M_KEYWORD = (CASE WHEN CLEANED_BRANDNAME = 'FOODIE PLUS BY MINIMIN' THEN 'FOODIE PLUS BY MINIMIN 6M+'
								WHEN CLEANED_BRANDNAME = 'HAPPY BEAR' THEN 'HAPPY BEAR 6M+'
								WHEN CLEANED_BRANDNAME = 'HEINZ' THEN 'HEINZ 6M+'
								ELSE INFANT_7_9M_KEYWORD END)
	,INFANT_10_12M_KEYWORD = (CASE WHEN CLEANED_BRANDNAME = 'FOODIE PLUS BY MINIMIN' THEN 'FOODIE PLUS BY MINIMIN 6M+'
								WHEN CLEANED_BRANDNAME = 'HAPPY BEAR' THEN 'HAPPY BEAR 6M+'
								WHEN CLEANED_BRANDNAME = 'HEINZ' THEN 'HEINZ 6M+'
								ELSE INFANT_10_12M_KEYWORD END)
WHERE MOM_AND_KID_PRODUCT_FLAG = 1
AND COALESCE(PREG_FLAG,'N/A') = 'N/A'
AND COALESCE(INFANT_FLAG,'N/A') = 'N/A'
AND COALESCE(TODDLER_FLAG,'N/A') = 'N/A'
AND COALESCE(PRESCHOOL_FLAG,'N/A') = 'N/A'
AND COALESCE(JUNIOR_FLAG,'N/A') = 'N/A'
AND COALESCE(TEEN_FLAG,'N/A') = 'N/A'
AND REVISED_CATEGORY_LEVEL_1 = 'KIDS'
AND REVISED_CATEGORY_LEVEL_3 = 'KID NOURISHMENT'
AND (CLEANED_BRANDNAME IN ('FOODIE PLUS BY MINIMIN','HAPPY BEAR')
	OR (CLEANED_BRANDNAME = 'HEINZ' AND PRODUCT_NAME IN ('HEINZ FARLEYS RUSKSBANANA 120G','HEINZ FARLEYS ORIGINAL 120G','HEINZ BABY APPLE BLUEBERRY110G'
												,'HEINZ APPLE BISCOTTI SNACK 60G','HEINZ ORGANIC BISCOTTISNACK60G','HEINZ BABY MACARONI BEEF 110G')))
;

--9M+
UPDATE vpm_data.pm_skutagging_kids_product_master
SET "STAGE_M+0_TO_M+3" = 'N/A'
	,"STAGE_M+3_TO_M+6" = 'N/A'
	,"STAGE_M+6_TO_M+9" = 'N/A'
	,"STAGE_M+9_TO_M+12" = 'YES'
	,INFANT_FLAG = 'YES'
	,UNKNOWN_INFANT_SUBSTAGE = 'N/A'
	,TODDLER_FLAG = 'YES'
	,INFANT_10_12M_KEYWORD = 'CERELAC 9M+'
	,TODDLER_KEYWORD = 'CERELAC 9M+'
WHERE MOM_AND_KID_PRODUCT_FLAG = 1
AND COALESCE(PREG_FLAG,'N/A') = 'N/A'
AND COALESCE(INFANT_FLAG,'N/A') = 'N/A'
AND COALESCE(TODDLER_FLAG,'N/A') = 'N/A'
AND COALESCE(PRESCHOOL_FLAG,'N/A') = 'N/A'
AND COALESCE(JUNIOR_FLAG,'N/A') = 'N/A'
AND COALESCE(TEEN_FLAG,'N/A') = 'N/A'
AND REVISED_CATEGORY_LEVEL_1 = 'KIDS'
AND REVISED_CATEGORY_LEVEL_3 = 'KID NOURISHMENT'
AND CLEANED_BRANDNAME = 'CERELAC'
;

--12M+
UPDATE vpm_data.pm_skutagging_kids_product_master
SET INFANT_FLAG = 'N/A'
	,TODDLER_FLAG = 'YES'
	,TODDLER_KEYWORD = (CASE WHEN CLEANED_BRANDNAME = 'APPLE MONKEY' THEN 'APPLE MONKEY 12M+'
							 WHEN CLEANED_BRANDNAME = 'HAPPY MUNCHY' THEN 'HAPPY MUNCHY 12M+'
							 ELSE TODDLER_KEYWORD END)
WHERE MOM_AND_KID_PRODUCT_FLAG = 1
AND COALESCE(PREG_FLAG,'N/A') = 'N/A'
AND COALESCE(INFANT_FLAG,'N/A') = 'N/A'
AND COALESCE(TODDLER_FLAG,'N/A') = 'N/A'
AND COALESCE(PRESCHOOL_FLAG,'N/A') = 'N/A'
AND COALESCE(JUNIOR_FLAG,'N/A') = 'N/A'
AND COALESCE(TEEN_FLAG,'N/A') = 'N/A'
AND REVISED_CATEGORY_LEVEL_1 = 'KIDS'
AND REVISED_CATEGORY_LEVEL_3 = 'KID NOURISHMENT'
AND CLEANED_BRANDNAME IN ('APPLE MONKEY','HAPPY MUNCHY')
;


---------------------------------------------------------------------------------BY DEPT, SUBDEPT, CLASS, SUBCLASS
--SUB DEPT 4 - 8 BOYS
UPDATE vpm_data.pm_skutagging_kids_product_master
SET INFANT_FLAG = 'N/A'
	,UNKNOWN_INFANT_SUBSTAGE = NULL
	,TODDLER_FLAG = 'N/A'
	,PRESCHOOL_FLAG = 'YES'
	,JUNIOR_FLAG = 'YES'
	,JUNIOR_KEYWORD = '4 - 8 BOYS'
WHERE MOM_AND_KID_PRODUCT_FLAG = 1
AND COALESCE(PREG_FLAG,'N/A') = 'N/A'
AND COALESCE(INFANT_FLAG,'N/A') = 'N/A'
AND COALESCE(TODDLER_FLAG,'N/A') = 'N/A'
AND COALESCE(PRESCHOOL_FLAG,'N/A') = 'N/A'
AND COALESCE(JUNIOR_FLAG,'N/A') = 'N/A'
AND COALESCE(TEEN_FLAG,'N/A') = 'N/A'
AND SUBDEPT_NAME = 'KID S WEAR'
;

--CLASS JUNIOR HIGH SCHOOL(M1-3)
UPDATE vpm_data.pm_skutagging_kids_product_master
SET INFANT_FLAG = 'N/A'
	,UNKNOWN_INFANT_SUBSTAGE = NULL
	,TODDLER_FLAG = 'N/A'
	,PRESCHOOL_FLAG = 'N/A'
	,JUNIOR_FLAG = 'N/A'
	,TEEN_FLAG = 'YES'
	,TEEN_KEYWORD = 'JUNIOR HIGH SCHOOL(M1-3)'
WHERE MOM_AND_KID_PRODUCT_FLAG = 1
AND COALESCE(PREG_FLAG,'N/A') = 'N/A'
AND COALESCE(INFANT_FLAG,'N/A') = 'N/A'
AND COALESCE(TODDLER_FLAG,'N/A') = 'N/A'
AND COALESCE(PRESCHOOL_FLAG,'N/A') = 'N/A'
AND COALESCE(JUNIOR_FLAG,'N/A') = 'N/A'
AND COALESCE(TEEN_FLAG,'N/A') = 'N/A'
AND CLASS_NAME = 'JUNIOR HIGH SCHOOL(M1-3)'
;

--CLASS SENIOR HIGH SCHOOL(M4-6)
UPDATE vpm_data.pm_skutagging_kids_product_master
SET INFANT_FLAG = 'N/A'
	,UNKNOWN_INFANT_SUBSTAGE = NULL
	,TODDLER_FLAG = 'N/A'
	,PRESCHOOL_FLAG = 'N/A'
	,JUNIOR_FLAG = 'N/A'
	,TEEN_FLAG = 'YES'
	,TEEN_KEYWORD = 'SENIOR HIGH SCHOOL(M4-6)'
WHERE MOM_AND_KID_PRODUCT_FLAG = 1
AND COALESCE(PREG_FLAG,'N/A') = 'N/A'
AND COALESCE(INFANT_FLAG,'N/A') = 'N/A'
AND COALESCE(TODDLER_FLAG,'N/A') = 'N/A'
AND COALESCE(PRESCHOOL_FLAG,'N/A') = 'N/A'
AND COALESCE(JUNIOR_FLAG,'N/A') = 'N/A'
AND COALESCE(TEEN_FLAG,'N/A') = 'N/A'
AND CLASS_NAME = 'SENIOR HIGH SCHOOL(M4-6)'
;

--CLASS PRIMARY SCHOOL
UPDATE vpm_data.pm_skutagging_kids_product_master
SET INFANT_FLAG = 'N/A'
	,UNKNOWN_INFANT_SUBSTAGE = NULL
	,TODDLER_FLAG = 'N/A'
	,PRESCHOOL_FLAG = 'N/A'
	,JUNIOR_FLAG = 'YES'
	,JUNIOR_KEYWORD = 'PRIMARY SCHOOL'
WHERE MOM_AND_KID_PRODUCT_FLAG = 1
AND COALESCE(PREG_FLAG,'N/A') = 'N/A'
AND COALESCE(INFANT_FLAG,'N/A') = 'N/A'
AND COALESCE(TODDLER_FLAG,'N/A') = 'N/A'
AND COALESCE(PRESCHOOL_FLAG,'N/A') = 'N/A'
AND COALESCE(JUNIOR_FLAG,'N/A') = 'N/A'
AND COALESCE(TEEN_FLAG,'N/A') = 'N/A'
AND CLASS_NAME = 'PRIMARY SCHOOL'
AND SUBDEPT_NAME NOT LIKE 'EXAMINATION GUIDE'
;

--CLASS JUNIOR PRIMARY SCHOOL
--INFANT 1-2Y
UPDATE vpm_data.pm_skutagging_kids_product_master
SET INFANT_FLAG = 'YES'
	,TODDLER_FLAG = 'YES'
	,PRESCHOOL_FLAG = 'N/A'
	,JUNIOR_FLAG = 'N/A'
	,TODDLER_KEYWORD = '1-2 Y'
WHERE MOM_AND_KID_PRODUCT_FLAG = 1
AND COALESCE(PREG_FLAG,'N/A') = 'N/A'
AND COALESCE(INFANT_FLAG,'N/A') = 'N/A'
AND COALESCE(TODDLER_FLAG,'N/A') = 'N/A'
AND COALESCE(PRESCHOOL_FLAG,'N/A') = 'N/A'
AND COALESCE(JUNIOR_FLAG,'N/A') = 'N/A'
AND COALESCE(TEEN_FLAG,'N/A') = 'N/A'
AND CLASS_NAME = 'JUNIOR PRIMARY SCHOOL'
AND (PRODUCT_NAME LIKE '%1-2��%'
	OR PRODUCT_NAME LIKE '%1 ��%')
;

--TODDLER 2-4Y
UPDATE vpm_data.pm_skutagging_kids_product_master
SET INFANT_FLAG = 'N/A'
	,UNKNOWN_INFANT_SUBSTAGE = NULL
	,TODDLER_FLAG = 'YES'
	,PRESCHOOL_FLAG = 'N/A'
	,JUNIOR_FLAG = 'N/A'
	,TODDLER_KEYWORD = (CASE WHEN PRODUCT_NAME LIKE '%����Ѻ2�բ��%' THEN '2Y+'
							WHEN PRODUCT_NAME LIKE '%����Ѻ3�բ��%' THEN '3Y+'
							WHEN PRODUCT_NAME LIKE '%2-3��%' THEN '2-3 Y'
							WHEN PRODUCT_NAME LIKE '%3-4��%' THEN '3-4 Y'
							WHEN PRODUCT_NAME LIKE '%3-5��%' THEN '3-5 Y'
							ELSE TODDLER_KEYWORD END)
WHERE MOM_AND_KID_PRODUCT_FLAG = 1
AND COALESCE(PREG_FLAG,'N/A') = 'N/A'
AND COALESCE(INFANT_FLAG,'N/A') = 'N/A'
AND COALESCE(TODDLER_FLAG,'N/A') = 'N/A'
AND COALESCE(PRESCHOOL_FLAG,'N/A') = 'N/A'
AND COALESCE(JUNIOR_FLAG,'N/A') = 'N/A'
AND COALESCE(TEEN_FLAG,'N/A') = 'N/A'
AND CLASS_NAME = 'JUNIOR PRIMARY SCHOOL'
AND (PRODUCT_NAME LIKE '%����Ѻ2�բ��%'
	OR PRODUCT_NAME LIKE '%����Ѻ3�բ��%'
	OR PRODUCT_NAME LIKE '%2-3��%'
	OR PRODUCT_NAME LIKE '%3-4��%'
	OR PRODUCT_NAME LIKE '%3-5��%')
;

--PRESCHOOL 4-6Y
UPDATE vpm_data.pm_skutagging_kids_product_master
SET INFANT_FLAG = 'N/A'
	,UNKNOWN_INFANT_SUBSTAGE = NULL
	,TODDLER_FLAG = 'N/A'
	,PRESCHOOL_FLAG = 'YES'
	,JUNIOR_FLAG = 'N/A'
	,PRESCHOOL_KEYWORD = (CASE WHEN PRODUCT_NAME LIKE '%����Ѻ4�բ��%' THEN '4Y+'
								WHEN PRODUCT_NAME LIKE '%����Ѻ5�բ��%' THEN '5Y+'
								WHEN PRODUCT_NAME LIKE '%3-5��%' THEN '3-5Y'
								WHEN PRODUCT_NAME LIKE '%4-5��%' THEN '4-5Y'
								WHEN PRODUCT_NAME LIKE '%5-6��%' THEN '5-6Y'
								ELSE PRESCHOOL_KEYWORD END)
WHERE MOM_AND_KID_PRODUCT_FLAG = 1
AND COALESCE(PREG_FLAG,'N/A') = 'N/A'
AND COALESCE(INFANT_FLAG,'N/A') = 'N/A'
AND COALESCE(TODDLER_FLAG,'N/A') = 'N/A'
AND COALESCE(PRESCHOOL_FLAG,'N/A') = 'N/A'
AND COALESCE(JUNIOR_FLAG,'N/A') = 'N/A'
AND COALESCE(TEEN_FLAG,'N/A') = 'N/A'
AND CLASS_NAME = 'JUNIOR PRIMARY SCHOOL'
AND (PRODUCT_NAME LIKE '%����Ѻ4�բ��%'
	OR PRODUCT_NAME LIKE '%����Ѻ5�բ��%'
	OR PRODUCT_NAME LIKE '%3-5��%'
	OR PRODUCT_NAME LIKE '%4-5��%'
	OR PRODUCT_NAME LIKE '%5-6��%')
;

--ELSE JUNIOR 6-13Y
UPDATE vpm_data.pm_skutagging_kids_product_master
SET INFANT_FLAG = 'N/A'
	,UNKNOWN_INFANT_SUBSTAGE = NULL
	,TODDLER_FLAG = 'N/A'
	,PRESCHOOL_FLAG = 'N/A'
	,JUNIOR_FLAG = 'YES'
	,JUNIOR_KEYWORD = 'JUNIOR PRIMARY SCHOOL'
WHERE MOM_AND_KID_PRODUCT_FLAG = 1
AND COALESCE(PREG_FLAG,'N/A') = 'N/A'
AND COALESCE(INFANT_FLAG,'N/A') = 'N/A'
AND COALESCE(TODDLER_FLAG,'N/A') = 'N/A'
AND COALESCE(PRESCHOOL_FLAG,'N/A') = 'N/A'
AND COALESCE(JUNIOR_FLAG,'N/A') = 'N/A'
AND COALESCE(TEEN_FLAG,'N/A') = 'N/A'
AND CLASS_NAME = 'JUNIOR PRIMARY SCHOOL'
AND PRODUCT_NAME NOT LIKE '%�ͺ����.4%'
;



--DEPT_NAME = BABYSHOP AND CAN TELL SUBSTAGE
--0-3M
UPDATE vpm_data.pm_skutagging_kids_product_master
SET "STAGE_M+0_TO_M+3" = 'YES'
	,"STAGE_M+3_TO_M+6" = 'N/A'
	,"STAGE_M+6_TO_M+9" = 'N/A'
	,"STAGE_M+9_TO_M+12" = 'N/A'
	,INFANT_FLAG = 'YES'
	,UNKNOWN_INFANT_SUBSTAGE = 'N/A'
	,INFANT_0_3M_KEYWORD = '0-3 MTH'
WHERE MOM_AND_KID_PRODUCT_FLAG = 1
AND COALESCE(PREG_FLAG,'N/A') = 'N/A'
AND COALESCE(INFANT_FLAG,'N/A') = 'N/A'
AND COALESCE(TODDLER_FLAG,'N/A') = 'N/A'
AND COALESCE(PRESCHOOL_FLAG,'N/A') = 'N/A'
AND COALESCE(JUNIOR_FLAG,'N/A') = 'N/A'
AND COALESCE(TEEN_FLAG,'N/A') = 'N/A'
AND CLASS_NAME = 'BABYSHOP'
AND PRODUCT_NAME LIKE '%0-3M%'
;

--3-6M
UPDATE vpm_data.pm_skutagging_kids_product_master
SET "STAGE_M+0_TO_M+3" = 'N/A'
	,"STAGE_M+3_TO_M+6" = 'YES'
	,"STAGE_M+6_TO_M+9" = 'N/A'
	,"STAGE_M+9_TO_M+12" = 'N/A'
	,INFANT_FLAG = 'YES'
	,UNKNOWN_INFANT_SUBSTAGE = 'N/A'
	,INFANT_4_6M_KEYWORD = '3-6 MTH'
WHERE MOM_AND_KID_PRODUCT_FLAG = 1
AND COALESCE(PREG_FLAG,'N/A') = 'N/A'
AND COALESCE(INFANT_FLAG,'N/A') = 'N/A'
AND COALESCE(TODDLER_FLAG,'N/A') = 'N/A'
AND COALESCE(PRESCHOOL_FLAG,'N/A') = 'N/A'
AND COALESCE(JUNIOR_FLAG,'N/A') = 'N/A'
AND COALESCE(TEEN_FLAG,'N/A') = 'N/A'
AND CLASS_NAME = 'BABYSHOP'
AND (PRODUCT_NAME  LIKE '%6M')
;

--6-12M
UPDATE vpm_data.pm_skutagging_kids_product_master
SET "STAGE_M+0_TO_M+3" = 'N/A'
	,"STAGE_M+3_TO_M+6" = 'N/A'
	,"STAGE_M+6_TO_M+9" = 'YES'
	,"STAGE_M+9_TO_M+12" = 'YES'
	,INFANT_FLAG = 'YES'
	,UNKNOWN_INFANT_SUBSTAGE = 'N/A'
	,INFANT_7_9M_KEYWORD = '6-12 MTH'
	,INFANT_10_12M_KEYWORD = '6-12 MTH'
WHERE MOM_AND_KID_PRODUCT_FLAG = 1
AND COALESCE(PREG_FLAG,'N/A') = 'N/A'
AND COALESCE(INFANT_FLAG,'N/A') = 'N/A'
AND COALESCE(TODDLER_FLAG,'N/A') = 'N/A'
AND COALESCE(PRESCHOOL_FLAG,'N/A') = 'N/A'
AND COALESCE(JUNIOR_FLAG,'N/A') = 'N/A'
AND COALESCE(TEEN_FLAG,'N/A') = 'N/A'
AND CLASS_NAME = 'BABYSHOP'
AND (PRODUCT_NAME  LIKE '%6-12%')
;

--6M+
UPDATE vpm_data.pm_skutagging_kids_product_master
SET "STAGE_M+0_TO_M+3" = 'N/A'
	,"STAGE_M+3_TO_M+6" = 'N/A'
	,"STAGE_M+6_TO_M+9" = 'YES'
	,"STAGE_M+9_TO_M+12" = 'YES'
	,INFANT_FLAG = 'YES'
	,UNKNOWN_INFANT_SUBSTAGE = 'N/A'
	,TODDLER_FLAG = 'YES'
	,INFANT_7_9M_KEYWORD = (CASE WHEN PRODUCT_NAME LIKE '%6M+%' THEN '6M+'
								WHEN PRODUCT_NAME LIKE '%SPOON%FEEDER%' THEN '��͹��͹'
								ELSE INFANT_7_9M_KEYWORD END)
	,INFANT_10_12M_KEYWORD = (CASE WHEN PRODUCT_NAME LIKE '%6M+%' THEN '6M+'
								WHEN PRODUCT_NAME LIKE '%SPOON%FEEDER%' THEN '��͹��͹'
								ELSE INFANT_10_12M_KEYWORD END)
	,TODDLER_KEYWORD = (CASE WHEN PRODUCT_NAME LIKE '%6M+%' THEN '6M+'
								WHEN PRODUCT_NAME LIKE '%SPOON%FEEDER%' THEN '��͹��͹'
								ELSE TODDLER_KEYWORD END)
WHERE MOM_AND_KID_PRODUCT_FLAG = 1
AND COALESCE(PREG_FLAG,'N/A') = 'N/A'
AND COALESCE(INFANT_FLAG,'N/A') = 'N/A'
AND COALESCE(TODDLER_FLAG,'N/A') = 'N/A'
AND COALESCE(PRESCHOOL_FLAG,'N/A') = 'N/A'
AND COALESCE(JUNIOR_FLAG,'N/A') = 'N/A'
AND COALESCE(TEEN_FLAG,'N/A') = 'N/A'
AND CLASS_NAME = 'BABYSHOP'
AND (PRODUCT_NAME  LIKE '%6M+%'
	OR PRODUCT_NAME LIKE '%SPOON%FEEDER%')
;

--9-12M
UPDATE vpm_data.pm_skutagging_kids_product_master
SET "STAGE_M+0_TO_M+3" = 'N/A'
	,"STAGE_M+3_TO_M+6" = 'N/A'
	,"STAGE_M+6_TO_M+9" = 'N/A'
	,"STAGE_M+9_TO_M+12" = 'YES'
	,INFANT_FLAG = 'YES'
	,UNKNOWN_INFANT_SUBSTAGE = 'N/A'
	,INFANT_10_12M_KEYWORD = '9-12 M'
WHERE MOM_AND_KID_PRODUCT_FLAG = 1
AND COALESCE(PREG_FLAG,'N/A') = 'N/A'
AND COALESCE(INFANT_FLAG,'N/A') = 'N/A'
AND COALESCE(TODDLER_FLAG,'N/A') = 'N/A'
AND COALESCE(PRESCHOOL_FLAG,'N/A') = 'N/A'
AND COALESCE(JUNIOR_FLAG,'N/A') = 'N/A'
AND COALESCE(TEEN_FLAG,'N/A') = 'N/A'
AND CLASS_NAME = 'BABYSHOP'
AND (PRODUCT_NAME  LIKE '%9-12%'
	OR PRODUCT_NAME LIKE '%12M%'
	OR PRODUCT_NAME LIKE '%12 M%')
;

--12-24M
UPDATE vpm_data.pm_skutagging_kids_product_master
SET INFANT_FLAG = 'N/A'
	,TODDLER_FLAG = 'YES'
	,TODDLER_KEYWORD = (CASE WHEN PRODUCT_NAME LIKE '%24M%' THEN '18-24 MTH'
									WHEN PRODUCT_NAME LIKE '%24 M%' THEN '18-24 MTH'
									WHEN PRODUCT_NAME LIKE '%18M%' THEN '12-18 MTH'
									WHEN PRODUCT_NAME LIKE '%12-18 M%' THEN '12-18 MTH'
									WHEN PRODUCT_NAME LIKE '%12-18' THEN '12-18 MTH'
									WHEN PRODUCT_NAME LIKE '%18-24' THEN '18-24 MTH'
									WHEN PRODUCT_NAME LIKE '%1-2%' THEN '1-2 Y'
									WHEN PRODUCT_NAME LIKE '%2Y%' THEN '1-2 Y'
									WHEN PRODUCT_NAME LIKE '%2 Y%' THEN '1-2 Y'
									ELSE TODDLER_KEYWORD END)
WHERE MOM_AND_KID_PRODUCT_FLAG = 1
AND COALESCE(PREG_FLAG,'N/A') = 'N/A'
AND COALESCE(INFANT_FLAG,'N/A') = 'N/A'
AND COALESCE(TODDLER_FLAG,'N/A') = 'N/A'
AND COALESCE(PRESCHOOL_FLAG,'N/A') = 'N/A'
AND COALESCE(JUNIOR_FLAG,'N/A') = 'N/A'
AND COALESCE(TEEN_FLAG,'N/A') = 'N/A'
AND CLASS_NAME = 'BABYSHOP'
AND (PRODUCT_NAME LIKE '%24M%'
	OR PRODUCT_NAME LIKE '%24 M%'
	OR PRODUCT_NAME LIKE '%18M%'
	OR PRODUCT_NAME LIKE '%12-18 M%'
	OR PRODUCT_NAME LIKE '%12-18'
	OR PRODUCT_NAME LIKE '%18-24'
	OR PRODUCT_NAME LIKE '%1-2%'
	OR PRODUCT_NAME LIKE '%2Y%'
	OR PRODUCT_NAME LIKE '%2 Y%')
AND PRODUCT_NAME NOT LIKE '%2-3%'
AND PRODUCT_NAME NOT LIKE '%3-4%'
AND PRODUCT_NAME NOT LIKE '%4-5%'
AND PRODUCT_NAME NOT LIKE '%5-6%'
AND PRODUCT_NAME NOT LIKE '%6-7%'
AND PRODUCT_NAME NOT LIKE '%7-8%'
AND PRODUCT_NAME NOT LIKE '%8-9%'
AND PRODUCT_NAME NOT LIKE '%9-10%'
AND PRODUCT_NAME NOT LIKE '%12Y%'
;

--INFANT, UNKNOWN SUBSTAGE
UPDATE vpm_data.pm_skutagging_kids_product_master
SET INFANT_FLAG = 'YES'
	,UNKNOWN_INFANT_SUBSTAGE = 'YES'
	,INFANT_UNKNOWN_KEYWORD = (CASE WHEN PRODUCT_NAME LIKE '%BABY%WRAP%' THEN '�����͵�Ƿ�á'
									WHEN PRODUCT_NAME LIKE '%FEEDING%PILLOW%' THEN '��͹����'----CHANGE ��͹���� SUBSTAGE �������͹�ѹ
									WHEN REVISED_CATEGORY_LEVEL_2 = 'TOYS' THEN 'BOUNCER' --BOUNCER/SWING/ROCKER/BABY SEAT
									ELSE INFANT_UNKNOWN_KEYWORD END)
WHERE MOM_AND_KID_PRODUCT_FLAG = 1
AND COALESCE(PREG_FLAG,'N/A') = 'N/A'
AND COALESCE(INFANT_FLAG,'N/A') = 'N/A'
AND COALESCE(TODDLER_FLAG,'N/A') = 'N/A'
AND COALESCE(PRESCHOOL_FLAG,'N/A') = 'N/A'
AND COALESCE(JUNIOR_FLAG,'N/A') = 'N/A'
AND COALESCE(TEEN_FLAG,'N/A') = 'N/A'
AND CLASS_NAME = 'BABYSHOP'
AND (PRODUCT_NAME LIKE '%BABY%WRAP%'
	OR PRODUCT_NAME LIKE '%FEEDING%PILLOW%'
	OR REVISED_CATEGORY_LEVEL_2 = 'TOYS')
;


--NOT INFANT (TODDLER)
UPDATE vpm_data.pm_skutagging_kids_product_master
SET INFANT_FLAG = 'N/A'
	,UNKNOWN_INFANT_SUBSTAGE = NULL
	,TODDLER_FLAG = 'YES'
	,PRESCHOOL_FLAG = 'N/A'
	,TODDLER_KEYWORD = (CASE WHEN PRODUCT_NAME LIKE '%3Y%' THEN '2-3 Y'
							WHEN PRODUCT_NAME LIKE '%3 Y%' THEN '2-3 Y'
							WHEN PRODUCT_NAME LIKE '%24-30M%' THEN '24-30 MTH'
							WHEN PRODUCT_NAME LIKE '%36M%' THEN '24-36 MTH'
							WHEN PRODUCT_NAME LIKE '%36 M%' THEN '24-36 MTH'
							WHEN PRODUCT_NAME LIKE '%4Y%' THEN '3-4 Y'
							WHEN PRODUCT_NAME LIKE '%4 Y%' THEN '3-4 Y'
							ELSE TODDLER_KEYWORD END)
WHERE MOM_AND_KID_PRODUCT_FLAG = 1
AND COALESCE(PREG_FLAG,'N/A') = 'N/A'
AND COALESCE(INFANT_FLAG,'N/A') = 'N/A'
AND COALESCE(TODDLER_FLAG,'N/A') = 'N/A'
AND COALESCE(PRESCHOOL_FLAG,'N/A') = 'N/A'
AND COALESCE(JUNIOR_FLAG,'N/A') = 'N/A'
AND COALESCE(TEEN_FLAG,'N/A') = 'N/A'
AND CLASS_NAME = 'BABYSHOP'
AND (PRODUCT_NAME LIKE '%3Y%'
	OR PRODUCT_NAME LIKE '%3 Y%'
	OR PRODUCT_NAME LIKE '%24-30M%'
	OR PRODUCT_NAME LIKE '%36M%'
	OR PRODUCT_NAME LIKE '%36 M%'
	OR PRODUCT_NAME LIKE '%4Y%'
	OR PRODUCT_NAME LIKE '%4 Y%')
AND PRODUCT_NAME NOT LIKE '%13Y%'
AND PRODUCT_NAME NOT LIKE '%14Y%'
;

--NOT INFANT (PRESCHOOL)
UPDATE vpm_data.pm_skutagging_kids_product_master
SET INFANT_FLAG = 'N/A'
	,UNKNOWN_INFANT_SUBSTAGE = NULL
	,TODDLER_FLAG = 'N/A'
	,PRESCHOOL_FLAG = 'YES'
WHERE MOM_AND_KID_PRODUCT_FLAG = 1
AND COALESCE(PREG_FLAG,'N/A') = 'N/A'
AND COALESCE(INFANT_FLAG,'N/A') = 'N/A'
AND COALESCE(TODDLER_FLAG,'N/A') = 'N/A'
AND COALESCE(PRESCHOOL_FLAG,'N/A') = 'N/A'
AND COALESCE(JUNIOR_FLAG,'N/A') = 'N/A'
AND COALESCE(TEEN_FLAG,'N/A') = 'N/A'
AND CLASS_NAME = 'BABYSHOP'
AND (PRODUCT_NAME LIKE '%5Y%'
	OR PRODUCT_NAME LIKE '%5-6Y%'
	OR PRODUCT_NAME LIKE '% 6Y%'
	OR PRODUCT_NAME LIKE '%6Y'
	OR PRODUCT_NAME LIKE '%6Y %'
	OR PRODUCT_NAME LIKE '%6YWHT'
	OR PRODUCT_NAME LIKE '%6YBLU'
	OR PRODUCT_NAME LIKE '%6 Y%')
;

--NOT INFANT, NOT TODDLER, NOT PRESCHOOL (>6Y)
UPDATE vpm_data.pm_skutagging_kids_product_master
SET INFANT_FLAG = 'N/A'
	,UNKNOWN_INFANT_SUBSTAGE = NULL
	,TODDLER_FLAG = 'N/A'
	,PRESCHOOL_FLAG = 'N/A'
WHERE MOM_AND_KID_PRODUCT_FLAG = 1
AND COALESCE(PREG_FLAG,'N/A') = 'N/A'
AND COALESCE(INFANT_FLAG,'N/A') = 'N/A'
AND COALESCE(TODDLER_FLAG,'N/A') = 'N/A'
AND COALESCE(PRESCHOOL_FLAG,'N/A') = 'N/A'
AND COALESCE(JUNIOR_FLAG,'N/A') = 'N/A'
AND COALESCE(TEEN_FLAG,'N/A') = 'N/A'
AND CLASS_NAME = 'BABYSHOP'
AND (PRODUCT_NAME LIKE '%[0-9]Y%'
	OR PRODUCT_NAME LIKE '%8 Y%')
;

--ELSE INFANT AND TODDLER
UPDATE vpm_data.pm_skutagging_kids_product_master
SET INFANT_FLAG = 'YES'
	,UNKNOWN_INFANT_SUBSTAGE = 'YES'
	,TODDLER_FLAG = 'YES'
	,PRESCHOOL_FLAG = 'N/A'
	,INFANT_UNKNOWN_KEYWORD = 'BABYSHOP PRODUCT'
	,TODDLER_KEYWORD = 'BABYSHOP PRODUCT'
WHERE MOM_AND_KID_PRODUCT_FLAG = 1
AND COALESCE(PREG_FLAG,'N/A') = 'N/A'
AND COALESCE(INFANT_FLAG,'N/A') = 'N/A'
AND COALESCE(TODDLER_FLAG,'N/A') = 'N/A'
AND COALESCE(PRESCHOOL_FLAG,'N/A') = 'N/A'
AND COALESCE(JUNIOR_FLAG,'N/A') = 'N/A'
AND COALESCE(TEEN_FLAG,'N/A') = 'N/A'
AND CLASS_NAME = 'BABYSHOP'
;

--SUB DEPT BABY S ACCESSORY AND KNOW SUBSTAGE
--0-3M
UPDATE vpm_data.pm_skutagging_kids_product_master
SET "STAGE_M+0_TO_M+3" = 'YES'
	,"STAGE_M+3_TO_M+6" = 'N/A'
	,"STAGE_M+6_TO_M+9" = 'N/A'
	,"STAGE_M+9_TO_M+12" = 'N/A'
	,INFANT_FLAG = 'YES'
	,UNKNOWN_INFANT_SUBSTAGE = 'N/A'
	,INFANT_0_3M_KEYWORD = (CASE WHEN PRODUCT_NAME LIKE '%0 3%' THEN '0-3 MTH'
								WHEN PRODUCT_NAME LIKE '%0-3%' THEN '0-3 MTH'
								WHEN (PRODUCT_NAME LIKE '%3M%' AND PRODUCT_NAME NOT LIKE '%3M-18M%') THEN '0-3 MTH'
								WHEN PRODUCT_NAME LIKE '%SLEEPSUITS 0%' THEN 'SLEEPSUIT 0'
								WHEN PRODUCT_NAME LIKE '%�ء����⤹% S%' THEN '�ء�� S'
								WHEN PRODUCT_NAME LIKE '%0 2M%' THEN '0-2 MTH'
								WHEN PRODUCT_NAME LIKE '%-0M%' THEN '0M'
								WHEN PRODUCT_NAME LIKE '%�ء% S' THEN '�ء�� S'
								WHEN PRODUCT_NAME LIKE '%�ء% S1' THEN '�ء�� S'
								WHEN PRODUCT_NAME LIKE '%�ء%SS' THEN '�ء�� S'
								ELSE INFANT_0_3M_KEYWORD END)
WHERE MOM_AND_KID_PRODUCT_FLAG = 1
AND COALESCE(PREG_FLAG,'N/A') = 'N/A'
AND COALESCE(INFANT_FLAG,'N/A') = 'N/A'
AND COALESCE(TODDLER_FLAG,'N/A') = 'N/A'
AND COALESCE(PRESCHOOL_FLAG,'N/A') = 'N/A'
AND COALESCE(JUNIOR_FLAG,'N/A') = 'N/A'
AND COALESCE(TEEN_FLAG,'N/A') = 'N/A'
AND SUBDEPT_NAME = 'BABY S ACCESSORIES'
AND REVISED_CATEGORY_LEVEL_2 <> 'MATERNITY'
AND (PRODUCT_NAME LIKE '%0 3%'
	OR PRODUCT_NAME LIKE '%0-3%'
	OR (PRODUCT_NAME LIKE '%3M%' AND PRODUCT_NAME NOT LIKE '%3M-18M%')
	OR PRODUCT_NAME LIKE '%SLEEPSUITS 0%'
	OR PRODUCT_NAME LIKE '%�ء����⤹% S%'
	OR PRODUCT_NAME LIKE '%0 2M%'
	OR PRODUCT_NAME LIKE '%-0M%'
	OR PRODUCT_NAME LIKE '%�ء% S'
	OR PRODUCT_NAME LIKE '%�ء% S1'
	OR PRODUCT_NAME LIKE '%�ء%SS')
;

--0-6M
UPDATE vpm_data.pm_skutagging_kids_product_master
SET "STAGE_M+0_TO_M+3" = 'YES'
	,"STAGE_M+3_TO_M+6" = 'YES'
	,"STAGE_M+6_TO_M+9" = 'N/A'
	,"STAGE_M+9_TO_M+12" = 'N/A'
	,INFANT_FLAG = 'YES'
	,UNKNOWN_INFANT_SUBSTAGE = 'N/A'
	,INFANT_0_3M_KEYWORD = (CASE WHEN PRODUCT_NAME LIKE '%0 6%' THEN '0-6 MTH'
								WHEN PRODUCT_NAME LIKE '%0-6%' THEN '0-6 MTH'
								WHEN PRODUCT_NAME LIKE '%0M-6M%' THEN '0-6 MTH'
								WHEN PRODUCT_NAME LIKE '%�ء��͡%STAGE 1%' THEN '�ء��͡ STAGE 1'
								ELSE INFANT_0_3M_KEYWORD END)
	,INFANT_4_6M_KEYWORD = (CASE WHEN PRODUCT_NAME LIKE '%0 6%' THEN '0-6 MTH'
								WHEN PRODUCT_NAME LIKE '%0-6%' THEN '0-6 MTH'
								WHEN PRODUCT_NAME LIKE '%0M-6M%' THEN '0-6 MTH'
								WHEN PRODUCT_NAME LIKE '%�ء��͡%STAGE 1%' THEN '�ء��͡ STAGE 1'
								ELSE INFANT_4_6M_KEYWORD END)
WHERE MOM_AND_KID_PRODUCT_FLAG = 1
AND COALESCE(PREG_FLAG,'N/A') = 'N/A'
AND COALESCE(INFANT_FLAG,'N/A') = 'N/A'
AND COALESCE(TODDLER_FLAG,'N/A') = 'N/A'
AND COALESCE(PRESCHOOL_FLAG,'N/A') = 'N/A'
AND COALESCE(JUNIOR_FLAG,'N/A') = 'N/A'
AND COALESCE(TEEN_FLAG,'N/A') = 'N/A'
AND SUBDEPT_NAME = 'BABY S ACCESSORIES'
AND (PRODUCT_NAME LIKE '%0 6%'
	OR PRODUCT_NAME LIKE '%0-6%'
	OR PRODUCT_NAME LIKE '%0M-6M%'
	OR PRODUCT_NAME LIKE '%�ء��͡%STAGE 1%')
AND PRODUCT_NAME NOT LIKE '%60X60%'
;

--0-9M
UPDATE vpm_data.pm_skutagging_kids_product_master
SET "STAGE_M+0_TO_M+3" = 'YES'
	,"STAGE_M+3_TO_M+6" = 'YES'
	,"STAGE_M+6_TO_M+9" = 'YES'
	,"STAGE_M+9_TO_M+12" = 'N/A'
	,INFANT_FLAG = 'YES'
	,UNKNOWN_INFANT_SUBSTAGE = 'N/A'
	,INFANT_0_3M_KEYWORD = (CASE WHEN PRODUCT_NAME LIKE '%GYM%' THEN 'PLAYGYM'
								WHEN PRODUCT_NAME LIKE '%���%' THEN 'PLAYGYM'
								WHEN PRODUCT_NAME LIKE '%���%' THEN 'PLAYGYM'
								ELSE INFANT_0_3M_KEYWORD END)
	,INFANT_4_6M_KEYWORD = (CASE WHEN PRODUCT_NAME LIKE '%GYM%' THEN 'PLAYGYM'
								WHEN PRODUCT_NAME LIKE '%���%' THEN 'PLAYGYM'
								WHEN PRODUCT_NAME LIKE '%���%' THEN 'PLAYGYM'
								ELSE INFANT_4_6M_KEYWORD END)
	,INFANT_7_9M_KEYWORD = (CASE WHEN PRODUCT_NAME LIKE '%GYM%' THEN 'PLAYGYM'
								WHEN PRODUCT_NAME LIKE '%���%' THEN 'PLAYGYM'
								WHEN PRODUCT_NAME LIKE '%���%' THEN 'PLAYGYM'
								ELSE INFANT_7_9M_KEYWORD END)
WHERE MOM_AND_KID_PRODUCT_FLAG = 1
AND COALESCE(PREG_FLAG,'N/A') = 'N/A'
AND COALESCE(INFANT_FLAG,'N/A') = 'N/A'
AND COALESCE(TODDLER_FLAG,'N/A') = 'N/A'
AND COALESCE(PRESCHOOL_FLAG,'N/A') = 'N/A'
AND COALESCE(JUNIOR_FLAG,'N/A') = 'N/A'
AND COALESCE(TEEN_FLAG,'N/A') = 'N/A'
AND SUBDEPT_NAME = 'BABY S ACCESSORIES'
AND (PRODUCT_NAME LIKE '%GYM%'
	OR PRODUCT_NAME LIKE '%���%'
	OR PRODUCT_NAME LIKE '%���%')
;

--0-12M
UPDATE vpm_data.pm_skutagging_kids_product_master
SET INFANT_FLAG = 'YES'
	,UNKNOWN_INFANT_SUBSTAGE = 'YES'
	,INFANT_0_3M_KEYWORD = (CASE WHEN PRODUCT_NAME LIKE '%��͹%0 12%' THEN '0-12 MTH'
								WHEN PRODUCT_NAME LIKE '%12M%' THEN '0-12 MTH'
								WHEN PRODUCT_NAME LIKE '%0-12%' THEN '0-12 MTH'
								WHEN PRODUCT_NAME LIKE '%0-1Y%' THEN '0-1 Y'
								ELSE INFANT_0_3M_KEYWORD END)
	,INFANT_4_6M_KEYWORD = (CASE WHEN PRODUCT_NAME LIKE '%��͹%0 12%' THEN '0-12 MTH'
								WHEN PRODUCT_NAME LIKE '%12M%' THEN '0-12 MTH'
								WHEN PRODUCT_NAME LIKE '%0-12%' THEN '0-12 MTH'
								WHEN PRODUCT_NAME LIKE '%0-1Y%' THEN '0-1 Y'
								ELSE INFANT_4_6M_KEYWORD END)
	,INFANT_7_9M_KEYWORD = (CASE WHEN PRODUCT_NAME LIKE '%��͹%0 12%' THEN '0-12 MTH'
								WHEN PRODUCT_NAME LIKE '%12M%' THEN '0-12 MTH'
								WHEN PRODUCT_NAME LIKE '%0-12%' THEN '0-12 MTH'
								WHEN PRODUCT_NAME LIKE '%0-1Y%' THEN '0-1 Y'
								ELSE INFANT_7_9M_KEYWORD END)
	,INFANT_10_12M_KEYWORD = (CASE WHEN PRODUCT_NAME LIKE '%��͹%0 12%' THEN '0-12 MTH'
								WHEN PRODUCT_NAME LIKE '%12M%' THEN '0-12 MTH'
								WHEN PRODUCT_NAME LIKE '%0-12%' THEN '0-12 MTH'
								WHEN PRODUCT_NAME LIKE '%0-1Y%' THEN '0-1 Y'
								ELSE INFANT_10_12M_KEYWORD END)
WHERE MOM_AND_KID_PRODUCT_FLAG = 1
AND COALESCE(PREG_FLAG,'N/A') = 'N/A'
AND COALESCE(INFANT_FLAG,'N/A') = 'N/A'
AND COALESCE(TODDLER_FLAG,'N/A') = 'N/A'
AND COALESCE(PRESCHOOL_FLAG,'N/A') = 'N/A'
AND COALESCE(JUNIOR_FLAG,'N/A') = 'N/A'
AND COALESCE(TEEN_FLAG,'N/A') = 'N/A'
AND SUBDEPT_NAME = 'BABY S ACCESSORIES'
	AND (PRODUCT_NAME LIKE '%��͹%0 12%'
		OR PRODUCT_NAME LIKE '%12M%'
		OR PRODUCT_NAME LIKE '%0-12%'
		OR PRODUCT_NAME LIKE '%0-1Y%')
;

--3-6M
UPDATE vpm_data.pm_skutagging_kids_product_master
SET "STAGE_M+0_TO_M+3" = 'N/A'
	,"STAGE_M+3_TO_M+6" = 'YES'
	,"STAGE_M+6_TO_M+9" = 'N/A'
	,"STAGE_M+9_TO_M+12" = 'N/A'
	,INFANT_FLAG = 'YES'
	,UNKNOWN_INFANT_SUBSTAGE = 'N/A'
	,INFANT_4_6M_KEYWORD = (CASE WHEN (PRODUCT_NAME LIKE '%3 6%' AND PRODUCT_NAME NOT LIKE '%3 600%') THEN '3-6 MTH'
								WHEN PRODUCT_NAME LIKE '%3-6%' THEN '3-6 MTH'
								WHEN (PRODUCT_NAME LIKE '%6M%' AND PRODUCT_NAME NOT LIKE '%6M-18M%') THEN '3-6 MTH'
								WHEN PRODUCT_NAME LIKE '%06 M%' THEN '3-6 MTH'
								WHEN PRODUCT_NAME LIKE '% 6 M%' THEN '3-6 MTH'
								WHEN PRODUCT_NAME LIKE '%SLEEPSUITS 3%' THEN 'SLEEPSUIT 3'
								WHEN PRODUCT_NAME LIKE '%�ء����⤹% M%' THEN '�ء�� M'
								WHEN PRODUCT_NAME LIKE '%�ء% M' THEN '�ء�� M'
								ELSE INFANT_4_6M_KEYWORD END)
WHERE MOM_AND_KID_PRODUCT_FLAG = 1
AND COALESCE(PREG_FLAG,'N/A') = 'N/A'
AND COALESCE(INFANT_FLAG,'N/A') = 'N/A'
AND COALESCE(TODDLER_FLAG,'N/A') = 'N/A'
AND COALESCE(PRESCHOOL_FLAG,'N/A') = 'N/A'
AND COALESCE(JUNIOR_FLAG,'N/A') = 'N/A'
AND COALESCE(TEEN_FLAG,'N/A') = 'N/A'
AND SUBDEPT_NAME = 'BABY S ACCESSORIES'
AND ((PRODUCT_NAME LIKE '%3 6%' AND PRODUCT_NAME NOT LIKE '%3 600%')
	OR PRODUCT_NAME LIKE '%3-6%'
	OR (PRODUCT_NAME LIKE '%6M%' AND PRODUCT_NAME NOT LIKE '%6M-18M%')
	OR PRODUCT_NAME LIKE '%06 M%'
	OR PRODUCT_NAME LIKE '% 6 M%'
	OR PRODUCT_NAME LIKE '%SLEEPSUITS 3%'
	OR PRODUCT_NAME LIKE '%�ء����⤹% M%'
	OR PRODUCT_NAME LIKE '%�ء% M')
;

--3M+
UPDATE vpm_data.pm_skutagging_kids_product_master
SET "STAGE_M+0_TO_M+3" = 'N/A'
	,"STAGE_M+3_TO_M+6" = 'YES'
	,"STAGE_M+6_TO_M+9" = 'YES'
	,"STAGE_M+9_TO_M+12" = 'YES'
	,INFANT_FLAG = 'YES'
	,UNKNOWN_INFANT_SUBSTAGE = 'N/A'
	,TODDLER_FLAG = 'YES'
	,INFANT_4_6M_KEYWORD = (CASE WHEN PRODUCT_NAME LIKE '%3M-18M%' THEN '3-18 MTH'
								WHEN PRODUCT_NAME LIKE '%FLOORMAT%' THEN 'FLOORMAT'
								ELSE INFANT_4_6M_KEYWORD END)
	,INFANT_7_9M_KEYWORD = (CASE WHEN PRODUCT_NAME LIKE '%3M-18M%' THEN '3-18 MTH'
								WHEN PRODUCT_NAME LIKE '%FLOORMAT%' THEN 'FLOORMAT'
								ELSE INFANT_7_9M_KEYWORD END)
	,INFANT_10_12M_KEYWORD = (CASE WHEN PRODUCT_NAME LIKE '%3M-18M%' THEN '3-18 MTH'
								WHEN PRODUCT_NAME LIKE '%FLOORMAT%' THEN 'FLOORMAT'
								ELSE INFANT_10_12M_KEYWORD END)
	,TODDLER_KEYWORD = (CASE WHEN PRODUCT_NAME LIKE '%3M-18M%' THEN '3-18 MTH'
								WHEN PRODUCT_NAME LIKE '%FLOORMAT%' THEN 'FLOORMAT'
								ELSE TODDLER_KEYWORD END)
WHERE MOM_AND_KID_PRODUCT_FLAG = 1
AND COALESCE(PREG_FLAG,'N/A') = 'N/A'
AND COALESCE(INFANT_FLAG,'N/A') = 'N/A'
AND COALESCE(TODDLER_FLAG,'N/A') = 'N/A'
AND COALESCE(PRESCHOOL_FLAG,'N/A') = 'N/A'
AND COALESCE(JUNIOR_FLAG,'N/A') = 'N/A'
AND COALESCE(TEEN_FLAG,'N/A') = 'N/A'
AND SUBDEPT_NAME = 'BABY S ACCESSORIES'
AND (PRODUCT_NAME LIKE '%3M-18M%'
	OR PRODUCT_NAME LIKE '%FLOORMAT%')
;

--6-9M
UPDATE vpm_data.pm_skutagging_kids_product_master
SET "STAGE_M+0_TO_M+3" = 'N/A'
	,"STAGE_M+3_TO_M+6" = 'N/A'
	,"STAGE_M+6_TO_M+9" = 'YES'
	,"STAGE_M+9_TO_M+12" = 'N/A'
	,INFANT_FLAG = 'YES'
	,UNKNOWN_INFANT_SUBSTAGE = 'N/A'
	,INFANT_7_9M_KEYWORD = (CASE WHEN PRODUCT_NAME LIKE '%6 9%' THEN '6-9 MTH'
								WHEN PRODUCT_NAME LIKE '%6-9%' THEN '6-9 MTH'
								WHEN PRODUCT_NAME LIKE '%SLEEPSUITS 6%' THEN 'SLEEPSUIT 6'
								ELSE INFANT_7_9M_KEYWORD END)
WHERE MOM_AND_KID_PRODUCT_FLAG = 1
AND COALESCE(PREG_FLAG,'N/A') = 'N/A'
AND COALESCE(INFANT_FLAG,'N/A') = 'N/A'
AND COALESCE(TODDLER_FLAG,'N/A') = 'N/A'
AND COALESCE(PRESCHOOL_FLAG,'N/A') = 'N/A'
AND COALESCE(JUNIOR_FLAG,'N/A') = 'N/A'
AND COALESCE(TEEN_FLAG,'N/A') = 'N/A'
AND SUBDEPT_NAME = 'BABY S ACCESSORIES'
AND (PRODUCT_NAME LIKE '%6 9%'
	OR PRODUCT_NAME LIKE '%6-9%'
	OR PRODUCT_NAME LIKE '%SLEEPSUITS 6%')
;

--6-12M
UPDATE vpm_data.pm_skutagging_kids_product_master
SET "STAGE_M+0_TO_M+3" = 'N/A'
	,"STAGE_M+3_TO_M+6" = 'N/A'
	,"STAGE_M+6_TO_M+9" = 'YES'
	,"STAGE_M+9_TO_M+12" = 'YES'
	,INFANT_FLAG = 'YES'
	,UNKNOWN_INFANT_SUBSTAGE = 'N/A'
	,INFANT_7_9M_KEYWORD = (CASE WHEN PRODUCT_NAME LIKE '%6 12%' THEN '6-12 MTH'
								WHEN PRODUCT_NAME LIKE '%6-12%' THEN '6-12 MTH'
								WHEN PRODUCT_NAME LIKE '%�ء��͡%STAGE 2%' THEN '�ء��͡ STAGE 2'
								ELSE INFANT_7_9M_KEYWORD END)
	,INFANT_10_12M_KEYWORD = (CASE WHEN PRODUCT_NAME LIKE '%6 12%' THEN '6-12 MTH'
								WHEN PRODUCT_NAME LIKE '%6-12%' THEN '6-12 MTH'
								WHEN PRODUCT_NAME LIKE '%�ء��͡%STAGE 2%' THEN '�ء��͡ STAGE 2'
								ELSE INFANT_10_12M_KEYWORD END)
WHERE MOM_AND_KID_PRODUCT_FLAG = 1
AND COALESCE(PREG_FLAG,'N/A') = 'N/A'
AND COALESCE(INFANT_FLAG,'N/A') = 'N/A'
AND COALESCE(TODDLER_FLAG,'N/A') = 'N/A'
AND COALESCE(PRESCHOOL_FLAG,'N/A') = 'N/A'
AND COALESCE(JUNIOR_FLAG,'N/A') = 'N/A'
AND COALESCE(TEEN_FLAG,'N/A') = 'N/A'
AND SUBDEPT_NAME = 'BABY S ACCESSORIES'
AND (PRODUCT_NAME LIKE '%6 12%'
	OR PRODUCT_NAME LIKE '%6-12%'
	OR PRODUCT_NAME LIKE '%�ء��͡%STAGE 2%')
;

--6M+
UPDATE vpm_data.pm_skutagging_kids_product_master
SET "STAGE_M+0_TO_M+3" = 'N/A'
	,"STAGE_M+3_TO_M+6" = 'N/A'
	,"STAGE_M+6_TO_M+9" = 'YES'
	,"STAGE_M+9_TO_M+12" = 'YES'
	,INFANT_FLAG = 'YES'
	,UNKNOWN_INFANT_SUBSTAGE = 'N/A'
	,TODDLER_FLAG = 'YES'
	,INFANT_7_9M_KEYWORD = (CASE WHEN PRODUCT_NAME LIKE '%6M-18M%' THEN '6-18 MTH'
								WHEN PRODUCT_NAME LIKE '%6-18%' THEN '6-18 MTH'
								WHEN PRODUCT_NAME LIKE '%6 18%' THEN '6-18 MTH'
								WHEN PRODUCT_NAME LIKE '%�ء����⤹% L%' THEN '�ء�� L'
								WHEN PRODUCT_NAME LIKE '%�ء%L' THEN '�ء�� L'
								WHEN PRODUCT_NAME LIKE '%������%' THEN '������'
								WHEN PRODUCT_NAME LIKE '%��͹%' THEN '��͹'
								ELSE INFANT_7_9M_KEYWORD END)
	,INFANT_10_12M_KEYWORD = (CASE WHEN PRODUCT_NAME LIKE '%6M-18M%' THEN '6-18 MTH'
								WHEN PRODUCT_NAME LIKE '%6-18%' THEN '6-18 MTH'
								WHEN PRODUCT_NAME LIKE '%6 18%' THEN '6-18 MTH'
								WHEN PRODUCT_NAME LIKE '%�ء����⤹% L%' THEN '�ء�� L'
								WHEN PRODUCT_NAME LIKE '%�ء%L' THEN '�ء�� L'
								WHEN PRODUCT_NAME LIKE '%������%' THEN '������'
								WHEN PRODUCT_NAME LIKE '%��͹%' THEN '��͹'
								ELSE INFANT_10_12M_KEYWORD END)
	,TODDLER_KEYWORD = (CASE WHEN PRODUCT_NAME LIKE '%6M-18M%' THEN '6-18 MTH'
								WHEN PRODUCT_NAME LIKE '%6-18%' THEN '6-18 MTH'
								WHEN PRODUCT_NAME LIKE '%6 18%' THEN '6-18 MTH'
								WHEN PRODUCT_NAME LIKE '%�ء����⤹% L%' THEN '�ء�� L'
								WHEN PRODUCT_NAME LIKE '%�ء%L' THEN '�ء�� L'
								WHEN PRODUCT_NAME LIKE '%������%' THEN '������'
								WHEN PRODUCT_NAME LIKE '%��͹%' THEN '��͹'
								ELSE TODDLER_KEYWORD END)
WHERE MOM_AND_KID_PRODUCT_FLAG = 1
AND COALESCE(PREG_FLAG,'N/A') = 'N/A'
AND COALESCE(INFANT_FLAG,'N/A') = 'N/A'
AND COALESCE(TODDLER_FLAG,'N/A') = 'N/A'
AND COALESCE(PRESCHOOL_FLAG,'N/A') = 'N/A'
AND COALESCE(JUNIOR_FLAG,'N/A') = 'N/A'
AND COALESCE(TEEN_FLAG,'N/A') = 'N/A'
AND SUBDEPT_NAME = 'BABY S ACCESSORIES'
AND (PRODUCT_NAME LIKE '%6M-18M%'
	OR PRODUCT_NAME LIKE '%6-18%'
	OR PRODUCT_NAME LIKE '%6 18%'
	OR PRODUCT_NAME LIKE '%�ء����⤹% L%'
	OR PRODUCT_NAME LIKE '%�ء%L'
	OR PRODUCT_NAME LIKE '%������%'
	OR PRODUCT_NAME LIKE '%��͹%')
;

--9-12M
UPDATE vpm_data.pm_skutagging_kids_product_master
SET "STAGE_M+0_TO_M+3" = 'N/A'
	,"STAGE_M+3_TO_M+6" = 'N/A'
	,"STAGE_M+6_TO_M+9" = 'N/A'
	,"STAGE_M+9_TO_M+12" = 'YES'
	,INFANT_FLAG = 'YES'
	,UNKNOWN_INFANT_SUBSTAGE = 'N/A'
	,INFANT_10_12M_KEYWORD = (CASE WHEN PRODUCT_NAME LIKE '%9 12%' THEN '9-12 MTH'
									WHEN PRODUCT_NAME LIKE '%9-12%' THEN '9-12 MTH'
									WHEN PRODUCT_NAME LIKE '%12 M%' THEN '9-12 MTH'
									WHEN PRODUCT_NAME LIKE '%SLEEPSUITS 9%' THEN 'SLEEPSUIT 9'
									ELSE INFANT_10_12M_KEYWORD END)
WHERE MOM_AND_KID_PRODUCT_FLAG = 1
AND COALESCE(PREG_FLAG,'N/A') = 'N/A'
AND COALESCE(INFANT_FLAG,'N/A') = 'N/A'
AND COALESCE(TODDLER_FLAG,'N/A') = 'N/A'
AND COALESCE(PRESCHOOL_FLAG,'N/A') = 'N/A'
AND COALESCE(JUNIOR_FLAG,'N/A') = 'N/A'
AND COALESCE(TEEN_FLAG,'N/A') = 'N/A'
AND SUBDEPT_NAME = 'BABY S ACCESSORIES'
AND (PRODUCT_NAME LIKE '%9 12%'
	OR PRODUCT_NAME LIKE '%9-12%'
	OR PRODUCT_NAME LIKE '%12 M%'
	OR PRODUCT_NAME LIKE '%SLEEPSUITS 9%')
;

--12-24M
UPDATE vpm_data.pm_skutagging_kids_product_master
SET INFANT_FLAG = 'YES'
	,TODDLER_FLAG = 'YES'
	,TODDLER_KEYWORD = (CASE WHEN PRODUCT_NAME LIKE '%12 18%' THEN '12-18 MTH'
									WHEN PRODUCT_NAME LIKE '%12-18%' THEN '12-18 MTH'
									WHEN PRODUCT_NAME LIKE '%12 24%' THEN '12-24 MTH'
									WHEN PRODUCT_NAME LIKE '%18 24%' THEN '18-24 MTH'
									WHEN PRODUCT_NAME LIKE '%18-24%' THEN '18-24 MTH'
									WHEN PRODUCT_NAME LIKE '%18 M%' THEN '12-18 MTH'
									WHEN PRODUCT_NAME LIKE '%24 M%' THEN '18-24 MTH'
									WHEN PRODUCT_NAME LIKE '%1-2Y%' THEN '1-2 Y'
									WHEN PRODUCT_NAME LIKE '% 2Y%' THEN '1-2 Y'
									WHEN PRODUCT_NAME LIKE '%12.5%' THEN '�ͧ����� 12.5 CM'
									WHEN PRODUCT_NAME LIKE '%12 5%' THEN '�ͧ����� 12.5 CM'
									WHEN PRODUCT_NAME LIKE '%13.5%' THEN '�ͧ����� 13.5 CM'
									WHEN PRODUCT_NAME LIKE '%13 5%' THEN '�ͧ����� 13.5 CM'
									WHEN PRODUCT_NAME LIKE '%14.5%' THEN '�ͧ����� 14.5 CM'
									WHEN PRODUCT_NAME LIKE '%14 5%' THEN '�ͧ����� 14.5 CM'
									WHEN PRODUCT_NAME LIKE '%SLEEPSUITS 12%' THEN 'SLEEPSUIT 12'
									WHEN PRODUCT_NAME LIKE '%SLEEPSUITS 18%' THEN 'SLEEPSUIT 18'
									WHEN PRODUCT_NAME LIKE '%�ء��͡%STAGE 3%' THEN '�ء��͡ STAGE 3'
									ELSE TODDLER_KEYWORD END)
WHERE MOM_AND_KID_PRODUCT_FLAG = 1
AND COALESCE(PREG_FLAG,'N/A') = 'N/A'
AND COALESCE(INFANT_FLAG,'N/A') = 'N/A'
AND COALESCE(TODDLER_FLAG,'N/A') = 'N/A'
AND COALESCE(PRESCHOOL_FLAG,'N/A') = 'N/A'
AND COALESCE(JUNIOR_FLAG,'N/A') = 'N/A'
AND COALESCE(TEEN_FLAG,'N/A') = 'N/A'
AND SUBDEPT_NAME = 'BABY S ACCESSORIES'
AND (PRODUCT_NAME LIKE '%12 18%'
	OR PRODUCT_NAME LIKE '%12-18%'
	OR PRODUCT_NAME LIKE '%12 24%'
	OR PRODUCT_NAME LIKE '%18 24%'
	OR PRODUCT_NAME LIKE '%18-24%'
	OR PRODUCT_NAME LIKE '%18 M%'
	OR PRODUCT_NAME LIKE '%24 M%'
	OR PRODUCT_NAME LIKE '%1-2Y%'
	OR PRODUCT_NAME LIKE '% 2Y%'
	--SHOES SIZE (CM)
	OR PRODUCT_NAME LIKE '%12.5%'
	OR PRODUCT_NAME LIKE '%12 5%'
	OR PRODUCT_NAME LIKE '%13.5%'
	OR PRODUCT_NAME LIKE '%13 5%'
	OR PRODUCT_NAME LIKE '%14.5%'
	OR PRODUCT_NAME LIKE '%14 5%'
	OR PRODUCT_NAME LIKE '%SLEEPSUITS 12%'
	OR PRODUCT_NAME LIKE '%SLEEPSUITS 18%'
	OR PRODUCT_NAME LIKE '%�ء��͡%STAGE 3%')
;

--INFANT UNKNOWN SUBSTAGE
UPDATE vpm_data.pm_skutagging_kids_product_master
SET INFANT_FLAG = 'YES'
	,UNKNOWN_INFANT_SUBSTAGE = 'YES'
	,INFANT_UNKNOWN_KEYWORD = (CASE WHEN PRODUCT_NAME LIKE '%0-2%' THEN '0-2 Y'
									WHEN PRODUCT_NAME LIKE '%�Ǵ��%' THEN '�Ǵ��'
									WHEN PRODUCT_NAME LIKE '%BOTTLE%' THEN '�Ǵ��'
									WHEN PRODUCT_NAME LIKE '%�ء%' THEN '�ء��'
									WHEN PRODUCT_NAME LIKE '%�����͵��%' THEN '�����͵��'
									WHEN PRODUCT_NAME LIKE '%�蹫Ѻ��ӹ�%' THEN '�蹫Ѻ��ӹ�'
									ELSE INFANT_UNKNOWN_KEYWORD END)
WHERE MOM_AND_KID_PRODUCT_FLAG = 1
AND COALESCE(PREG_FLAG,'N/A') = 'N/A'
AND COALESCE(INFANT_FLAG,'N/A') = 'N/A'
AND COALESCE(TODDLER_FLAG,'N/A') = 'N/A'
AND COALESCE(PRESCHOOL_FLAG,'N/A') = 'N/A'
AND COALESCE(JUNIOR_FLAG,'N/A') = 'N/A'
AND COALESCE(TEEN_FLAG,'N/A') = 'N/A'
AND SUBDEPT_NAME = 'BABY S ACCESSORIES'
AND (PRODUCT_NAME LIKE '%0-2%'
	OR PRODUCT_NAME LIKE '%�Ǵ��%'
	OR PRODUCT_NAME LIKE '%BOTTLE%'
	OR PRODUCT_NAME LIKE '%�ء%'
	OR PRODUCT_NAME LIKE '%�����͵��%'
	OR PRODUCT_NAME LIKE '%�蹫Ѻ��ӹ�%')
;

--INFANT AND TODDLER 12-36M
UPDATE vpm_data.pm_skutagging_kids_product_master
SET INFANT_FLAG = 'N/A'
	,TODDLER_FLAG = 'YES'
	,TODDLER_KEYWORD = (CASE WHEN PRODUCT_NAME LIKE '%12-36%' THEN '12-36 MTH'
									WHEN PRODUCT_NAME LIKE '%���տѹ%' THEN '���տѹ'
									ELSE TODDLER_KEYWORD END)
WHERE MOM_AND_KID_PRODUCT_FLAG = 1
AND COALESCE(PREG_FLAG,'N/A') = 'N/A'
AND COALESCE(INFANT_FLAG,'N/A') = 'N/A'
AND COALESCE(TODDLER_FLAG,'N/A') = 'N/A'
AND COALESCE(PRESCHOOL_FLAG,'N/A') = 'N/A'
AND COALESCE(JUNIOR_FLAG,'N/A') = 'N/A'
AND COALESCE(TEEN_FLAG,'N/A') = 'N/A'
AND SUBDEPT_NAME = 'BABY S ACCESSORIES'
AND (PRODUCT_NAME LIKE '%12-36%'
	OR PRODUCT_NAME LIKE '%���տѹ%')
;

--NOT INFANT (TODDLER)
UPDATE vpm_data.pm_skutagging_kids_product_master
SET INFANT_FLAG = 'N/A'
	,UNKNOWN_INFANT_SUBSTAGE = NULL
	,TODDLER_FLAG = 'YES'
	,TODDLER_KEYWORD = (CASE WHEN PRODUCT_NAME LIKE '%24-36%' THEN '24-36 MTH'
							WHEN PRODUCT_NAME LIKE '%2-4Y%' THEN '2-4 Y'
							WHEN PRODUCT_NAME LIKE '%3-12Y%' THEN '3-12 Y'
							WHEN PRODUCT_NAME LIKE '%�ç���¹%' THEN '�ç���¹'
							WHEN PRODUCT_NAME LIKE '%READ WITH ME VIOLET%' THEN 'READ WITH ME VIOLET'
							WHEN (PRODUCT_NAME LIKE '%˹ѧ���%' AND PRODUCT_NAME NOT LIKE '%�����ͷ������%') THEN '˹ѧ��͹Էҹ'
							WHEN PRODUCT_NAME LIKE '%15.5%' THEN '�ͧ����� 15.5 CM'
							WHEN PRODUCT_NAME LIKE '%15 5%' THEN '�ͧ����� 15.5 CM'
							WHEN PRODUCT_NAME LIKE '%16.5%' THEN '�ͧ����� 16.5 CM'
							WHEN PRODUCT_NAME LIKE '%16 5%' THEN '�ͧ����� 16.5 CM'
							ELSE TODDLER_KEYWORD END)
WHERE MOM_AND_KID_PRODUCT_FLAG = 1
AND COALESCE(PREG_FLAG,'N/A') = 'N/A'
AND COALESCE(INFANT_FLAG,'N/A') = 'N/A'
AND COALESCE(TODDLER_FLAG,'N/A') = 'N/A'
AND COALESCE(PRESCHOOL_FLAG,'N/A') = 'N/A'
AND COALESCE(JUNIOR_FLAG,'N/A') = 'N/A'
AND COALESCE(TEEN_FLAG,'N/A') = 'N/A'
AND SUBDEPT_NAME = 'BABY S ACCESSORIES'
AND (PRODUCT_NAME LIKE '%24-36%'
	OR PRODUCT_NAME LIKE '%2-4Y%'
	OR PRODUCT_NAME LIKE '%3-12Y%'
	OR PRODUCT_NAME LIKE '%�ç���¹%'
	OR PRODUCT_NAME LIKE '%READ WITH ME VIOLET%'
	OR (PRODUCT_NAME LIKE '%˹ѧ���%' AND PRODUCT_NAME NOT LIKE '%�����ͷ������%')
	--SHOES SIZE (CM)
	OR PRODUCT_NAME LIKE '%15.5%'
	OR PRODUCT_NAME LIKE '%15 5%'
	OR PRODUCT_NAME LIKE '%16.5%'
	OR PRODUCT_NAME LIKE '%16 5%')
;

--NOT INFANT (PRESCHOOL)
UPDATE vpm_data.pm_skutagging_kids_product_master
SET INFANT_FLAG = 'N/A'
	,UNKNOWN_INFANT_SUBSTAGE = NULL
	,TODDLER_FLAG = 'N/A'
	,PRESCHOOL_FLAG = 'YES'
WHERE MOM_AND_KID_PRODUCT_FLAG = 1
AND COALESCE(PREG_FLAG,'N/A') = 'N/A'
AND COALESCE(INFANT_FLAG,'N/A') = 'N/A'
AND COALESCE(TODDLER_FLAG,'N/A') = 'N/A'
AND COALESCE(PRESCHOOL_FLAG,'N/A') = 'N/A'
AND COALESCE(JUNIOR_FLAG,'N/A') = 'N/A'
AND COALESCE(TEEN_FLAG,'N/A') = 'N/A'
AND SUBDEPT_NAME = 'BABY S ACCESSORIES'
AND (PRODUCT_NAME LIKE '%4-6Y%')
;

--ELSE BY SUBCLASS
--0-3M MOBILE
UPDATE vpm_data.pm_skutagging_kids_product_master
SET "STAGE_M+0_TO_M+3" = 'YES'
	,"STAGE_M+3_TO_M+6" = 'N/A'
	,"STAGE_M+6_TO_M+9" = 'N/A'
	,"STAGE_M+9_TO_M+12" = 'N/A'
	,INFANT_FLAG = 'YES'
	,UNKNOWN_INFANT_SUBSTAGE = 'N/A'
	,INFANT_0_3M_KEYWORD = 'MOBILE'
WHERE MOM_AND_KID_PRODUCT_FLAG = 1
AND COALESCE(PREG_FLAG,'N/A') = 'N/A'
AND COALESCE(INFANT_FLAG,'N/A') = 'N/A'
AND COALESCE(TODDLER_FLAG,'N/A') = 'N/A'
AND COALESCE(PRESCHOOL_FLAG,'N/A') = 'N/A'
AND COALESCE(JUNIOR_FLAG,'N/A') = 'N/A'
AND COALESCE(TEEN_FLAG,'N/A') = 'N/A'
AND SUBDEPT_NAME = 'BABY S ACCESSORIES'
AND SUBCLASS_NAME LIKE 'MOBILE'
;

--3M+ PLAYMAT
UPDATE vpm_data.pm_skutagging_kids_product_master
SET "STAGE_M+0_TO_M+3" = 'N/A'
	,"STAGE_M+3_TO_M+6" = 'YES'
	,"STAGE_M+6_TO_M+9" = 'YES'
	,"STAGE_M+9_TO_M+12" = 'YES'
	,INFANT_FLAG = 'YES'
	,UNKNOWN_INFANT_SUBSTAGE = 'N/A'
	,TODDLER_FLAG = 'YES'
	,INFANT_4_6M_KEYWORD = 'PLAYMAT'
	,INFANT_7_9M_KEYWORD = 'PLAYMAT'
	,INFANT_10_12M_KEYWORD = 'PLAYMAT'
	,TODDLER_KEYWORD = 'PLAYMAT'
WHERE MOM_AND_KID_PRODUCT_FLAG = 1
AND COALESCE(PREG_FLAG,'N/A') = 'N/A'
AND COALESCE(INFANT_FLAG,'N/A') = 'N/A'
AND COALESCE(TODDLER_FLAG,'N/A') = 'N/A'
AND COALESCE(PRESCHOOL_FLAG,'N/A') = 'N/A'
AND COALESCE(JUNIOR_FLAG,'N/A') = 'N/A'
AND COALESCE(TEEN_FLAG,'N/A') = 'N/A'
AND SUBDEPT_NAME = 'BABY S ACCESSORIES'
AND SUBCLASS_NAME LIKE 'PLAYMAT'
;

--ELSE BABY S ACCESSORY'
UPDATE vpm_data.pm_skutagging_kids_product_master
SET INFANT_FLAG = 'YES'
	,UNKNOWN_INFANT_SUBSTAGE = 'YES'
	,TODDLER_FLAG = 'YES'
	,INFANT_UNKNOWN_KEYWORD = 'BABY ACCESSORIES'
	,TODDLER_KEYWORD = SUBCLASS_NAME
WHERE MOM_AND_KID_PRODUCT_FLAG = 1
AND COALESCE(PREG_FLAG,'N/A') = 'N/A'
AND COALESCE(INFANT_FLAG,'N/A') = 'N/A'
AND COALESCE(TODDLER_FLAG,'N/A') = 'N/A'
AND COALESCE(PRESCHOOL_FLAG,'N/A') = 'N/A'
AND COALESCE(JUNIOR_FLAG,'N/A') = 'N/A'
AND COALESCE(TEEN_FLAG,'N/A') = 'N/A'
AND SUBDEPT_NAME = 'BABY S ACCESSORIES'
;

-----�ء��͡/�ء��
--�ء��͡/�ء�� 0-3M
UPDATE vpm_data.pm_skutagging_kids_product_master
SET "STAGE_M+0_TO_M+3" = 'YES'
	,"STAGE_M+3_TO_M+6" = 'N/A'
	,"STAGE_M+6_TO_M+9" = 'N/A'
	,"STAGE_M+9_TO_M+12" = 'N/A'
	,INFANT_FLAG = 'YES'
	,UNKNOWN_INFANT_SUBSTAGE = 'N/A'
	,INFANT_0_3M_KEYWORD = (CASE WHEN PRODUCT_NAME LIKE '%�ء��͡%0��͹%' THEN '0M+'
								WHEN PRODUCT_NAME LIKE '%�ء��͡%0 M%' THEN '0M+'
								WHEN PRODUCT_NAME LIKE '%�ء��%S%' THEN '�ء�� S'
								WHEN PRODUCT_NAME LIKE '%�ء����� 1%' THEN '�ء�� STEP 1'
								ELSE INFANT_0_3M_KEYWORD END)
WHERE MOM_AND_KID_PRODUCT_FLAG = 1
AND COALESCE(PREG_FLAG,'N/A') = 'N/A'
AND COALESCE(INFANT_FLAG,'N/A') = 'N/A'
AND COALESCE(TODDLER_FLAG,'N/A') = 'N/A'
AND COALESCE(PRESCHOOL_FLAG,'N/A') = 'N/A'
AND COALESCE(JUNIOR_FLAG,'N/A') = 'N/A'
AND COALESCE(TEEN_FLAG,'N/A') = 'N/A'
AND (PRODUCT_NAME LIKE '%�ء��͡%0��͹%'
	OR PRODUCT_NAME LIKE '%�ء��͡%0 M%'
	OR PRODUCT_NAME LIKE '%�ء��%S%'
	OR PRODUCT_NAME LIKE '%�ء����� 1%')
;

--�ء��͡/�ء�� 3-6M
UPDATE vpm_data.pm_skutagging_kids_product_master
SET "STAGE_M+0_TO_M+3" = 'N/A'
	,"STAGE_M+3_TO_M+6" = 'YES'
	,"STAGE_M+6_TO_M+9" = 'N/A'
	,"STAGE_M+9_TO_M+12" = 'N/A'
	,INFANT_FLAG = 'YES'
	,UNKNOWN_INFANT_SUBSTAGE = 'N/A'
	,INFANT_4_6M_KEYWORD = (CASE WHEN PRODUCT_NAME LIKE '%�ء��͡%3-6%' THEN '�ء��͡ 3-6 MTH'
								WHEN PRODUCT_NAME LIKE '%�ء��%���M%' THEN '�ء�� M'
								WHEN PRODUCT_NAME LIKE '%�ء����� 2%' THEN '�ء�� STEP 2'
								ELSE INFANT_4_6M_KEYWORD END)
WHERE MOM_AND_KID_PRODUCT_FLAG = 1
AND COALESCE(PREG_FLAG,'N/A') = 'N/A'
AND COALESCE(INFANT_FLAG,'N/A') = 'N/A'
AND COALESCE(TODDLER_FLAG,'N/A') = 'N/A'
AND COALESCE(PRESCHOOL_FLAG,'N/A') = 'N/A'
AND COALESCE(JUNIOR_FLAG,'N/A') = 'N/A'
AND COALESCE(TEEN_FLAG,'N/A') = 'N/A'
AND (PRODUCT_NAME LIKE '%�ء��͡%3-6%'
	OR PRODUCT_NAME LIKE '%�ء��%���M%'
	OR PRODUCT_NAME LIKE '%�ء����� 2%')
;

--�ء��͡/�ء�� 6M+
UPDATE vpm_data.pm_skutagging_kids_product_master
SET "STAGE_M+0_TO_M+3" = 'N/A'
	,"STAGE_M+3_TO_M+6" = 'N/A'
	,"STAGE_M+6_TO_M+9" = 'YES'
	,"STAGE_M+9_TO_M+12" = 'YES'
	,INFANT_FLAG = 'YES'
	,UNKNOWN_INFANT_SUBSTAGE = 'N/A'
	,TODDLER_FLAG = 'YES'
	,INFANT_7_9M_KEYWORD = (CASE WHEN PRODUCT_NAME LIKE '%�ء��͡%6-18%' THEN '6-18 MTH'
								WHEN PRODUCT_NAME LIKE '%�ء��͡%6 M UP%' THEN '6M+'
								WHEN PRODUCT_NAME LIKE '%�ء��%L%' THEN '�ء�� L'
								WHEN PRODUCT_NAME LIKE '%�ء����� 3%' THEN '�ء�� STEP 3'
								ELSE INFANT_7_9M_KEYWORD END)
	,INFANT_10_12M_KEYWORD = (CASE WHEN PRODUCT_NAME LIKE '%�ء��͡%6-18%' THEN '6-18 MTH'
								WHEN PRODUCT_NAME LIKE '%�ء��͡%6 M UP%' THEN '6M+'
								WHEN PRODUCT_NAME LIKE '%�ء��%L%' THEN '�ء�� L'
								WHEN PRODUCT_NAME LIKE '%�ء����� 3%' THEN '�ء�� STEP 3'
								ELSE INFANT_10_12M_KEYWORD END)
	,TODDLER_KEYWORD = (CASE WHEN PRODUCT_NAME LIKE '%�ء��͡%6-18%' THEN '6-18 MTH'
								WHEN PRODUCT_NAME LIKE '%�ء��͡%6 M UP%' THEN '6M+'
								WHEN PRODUCT_NAME LIKE '%�ء��%L%' THEN '�ء�� L'
								WHEN PRODUCT_NAME LIKE '%�ء����� 3%' THEN '�ء�� STEP 3'
								ELSE TODDLER_KEYWORD END)
WHERE MOM_AND_KID_PRODUCT_FLAG = 1
AND COALESCE(PREG_FLAG,'N/A') = 'N/A'
AND COALESCE(INFANT_FLAG,'N/A') = 'N/A'
AND COALESCE(TODDLER_FLAG,'N/A') = 'N/A'
AND COALESCE(PRESCHOOL_FLAG,'N/A') = 'N/A'
AND COALESCE(JUNIOR_FLAG,'N/A') = 'N/A'
AND COALESCE(TEEN_FLAG,'N/A') = 'N/A'
AND (PRODUCT_NAME LIKE '%�ء��͡%6-18%'
	OR PRODUCT_NAME LIKE '%�ء��͡%6 M UP%'
	OR PRODUCT_NAME LIKE '%�ء��%L%'
	OR PRODUCT_NAME LIKE '%�ء����� 3%')
;

--�ء��͡/�ء�� 18-36M
UPDATE vpm_data.pm_skutagging_kids_product_master
SET INFANT_FLAG = 'N/A'
	,TODDLER_FLAG = 'YES'
	,TODDLER_KEYWORD = '18-36 MTH'
WHERE MOM_AND_KID_PRODUCT_FLAG = 1
AND COALESCE(PREG_FLAG,'N/A') = 'N/A'
AND COALESCE(INFANT_FLAG,'N/A') = 'N/A'
AND COALESCE(TODDLER_FLAG,'N/A') = 'N/A'
AND COALESCE(PRESCHOOL_FLAG,'N/A') = 'N/A'
AND COALESCE(JUNIOR_FLAG,'N/A') = 'N/A'
AND COALESCE(TEEN_FLAG,'N/A') = 'N/A'
AND (PRODUCT_NAME LIKE '%�ء��͡%18-36%')
;

--ELSE �ء��͡/�ء��
UPDATE vpm_data.pm_skutagging_kids_product_master
SET INFANT_FLAG = 'YES'
	,UNKNOWN_INFANT_SUBSTAGE = 'YES'
	,INFANT_UNKNOWN_KEYWORD = '�ء��͡'
WHERE MOM_AND_KID_PRODUCT_FLAG = 1
AND COALESCE(PREG_FLAG,'N/A') = 'N/A'
AND COALESCE(INFANT_FLAG,'N/A') = 'N/A'
AND COALESCE(TODDLER_FLAG,'N/A') = 'N/A'
AND COALESCE(PRESCHOOL_FLAG,'N/A') = 'N/A'
AND COALESCE(JUNIOR_FLAG,'N/A') = 'N/A'
AND COALESCE(TEEN_FLAG,'N/A') = 'N/A'
AND (PRODUCT_NAME LIKE '%�ء��͡%'
	OR PRODUCT_NAME LIKE '%�ء��%')
;

-----SUB DEPT INFANT/TODDLERS
--0-3M
UPDATE vpm_data.pm_skutagging_kids_product_master
SET "STAGE_M+0_TO_M+3" = 'YES'
	,"STAGE_M+3_TO_M+6" = 'N/A'
	,"STAGE_M+6_TO_M+9" = 'N/A'
	,"STAGE_M+9_TO_M+12" = 'N/A'
	,INFANT_FLAG = 'YES'
	,UNKNOWN_INFANT_SUBSTAGE = 'N/A'
	,INFANT_0_3M_KEYWORD = '0-3 MTH'
WHERE MOM_AND_KID_PRODUCT_FLAG = 1
AND COALESCE(PREG_FLAG,'N/A') = 'N/A'
AND COALESCE(INFANT_FLAG,'N/A') = 'N/A'
AND COALESCE(TODDLER_FLAG,'N/A') = 'N/A'
AND COALESCE(PRESCHOOL_FLAG,'N/A') = 'N/A'
AND COALESCE(JUNIOR_FLAG,'N/A') = 'N/A'
AND COALESCE(TEEN_FLAG,'N/A') = 'N/A'
AND SUBDEPT_NAME = 'BABY S WEAR'
AND (PRODUCT_NAME LIKE '%3M'
	OR PRODUCT_NAME LIKE '% 0'
	OR PRODUCT_NAME LIKE '% 00'
	OR PRODUCT_NAME LIKE '%M01'
	OR PRODUCT_NAME LIKE '% 50')
;

--3-6M
UPDATE vpm_data.pm_skutagging_kids_product_master
SET "STAGE_M+0_TO_M+3" = 'N/A'
	,"STAGE_M+3_TO_M+6" = 'YES'
	,"STAGE_M+6_TO_M+9" = 'N/A'
	,"STAGE_M+9_TO_M+12" = 'N/A'
	,INFANT_FLAG = 'YES'
	,UNKNOWN_INFANT_SUBSTAGE = 'N/A'
	,INFANT_4_6M_KEYWORD = (CASE WHEN PRODUCT_NAME LIKE '%�ҧࡧ�������% SS%' THEN 'DIAPER SIZE S'
								WHEN PRODUCT_NAME LIKE '%�ҧࡧ�������% S%' THEN 'DIAPER SIZE S'
								WHEN PRODUCT_NAME LIKE '% 6M' THEN '3-6 MTH'
								WHEN PRODUCT_NAME LIKE '%06M' THEN '3-6 NTH'
								WHEN PRODUCT_NAME LIKE '%M06' THEN '3-6 MTH'
								ELSE INFANT_4_6M_KEYWORD END)
WHERE MOM_AND_KID_PRODUCT_FLAG = 1
AND COALESCE(PREG_FLAG,'N/A') = 'N/A'
AND COALESCE(INFANT_FLAG,'N/A') = 'N/A'
AND COALESCE(TODDLER_FLAG,'N/A') = 'N/A'
AND COALESCE(PRESCHOOL_FLAG,'N/A') = 'N/A'
AND COALESCE(JUNIOR_FLAG,'N/A') = 'N/A'
AND COALESCE(TEEN_FLAG,'N/A') = 'N/A'
AND SUBDEPT_NAME = 'BABY S WEAR'
AND (PRODUCT_NAME LIKE '%�ҧࡧ�������% SS%'
	OR PRODUCT_NAME LIKE '%�ҧࡧ�������% S%'
	OR PRODUCT_NAME LIKE '% 6M'
	OR PRODUCT_NAME LIKE '%06M'
	OR PRODUCT_NAME LIKE '%M06')
;

--6-9M
UPDATE vpm_data.pm_skutagging_kids_product_master
SET "STAGE_M+0_TO_M+3" = 'N/A'
	,"STAGE_M+3_TO_M+6" = 'N/A'
	,"STAGE_M+6_TO_M+9" = 'YES'
	,"STAGE_M+9_TO_M+12" = 'N/A'
	,INFANT_FLAG = 'YES'
	,UNKNOWN_INFANT_SUBSTAGE = 'N/A'
	,INFANT_7_9M_KEYWORD = '6-9 MTH'
WHERE MOM_AND_KID_PRODUCT_FLAG = 1
AND COALESCE(PREG_FLAG,'N/A') = 'N/A'
AND COALESCE(INFANT_FLAG,'N/A') = 'N/A'
AND COALESCE(TODDLER_FLAG,'N/A') = 'N/A'
AND COALESCE(PRESCHOOL_FLAG,'N/A') = 'N/A'
AND COALESCE(JUNIOR_FLAG,'N/A') = 'N/A'
AND COALESCE(TEEN_FLAG,'N/A') = 'N/A'
AND SUBDEPT_NAME = 'BABY S WEAR'
AND (PRODUCT_NAME LIKE '%M09')
;

--6-12
UPDATE vpm_data.pm_skutagging_kids_product_master
SET "STAGE_M+0_TO_M+3" = 'N/A'
	,"STAGE_M+3_TO_M+6" = 'N/A'
	,"STAGE_M+6_TO_M+9" = 'YES'
	,"STAGE_M+9_TO_M+12" = 'YES'
	,INFANT_FLAG = 'YES'
	,UNKNOWN_INFANT_SUBSTAGE = 'N/A'
	,INFANT_7_9M_KEYWORD = 'DIAPER SIZE M'
	,INFANT_10_12M_KEYWORD = 'DIAPER SIZE M'
WHERE MOM_AND_KID_PRODUCT_FLAG = 1
AND COALESCE(PREG_FLAG,'N/A') = 'N/A'
AND COALESCE(INFANT_FLAG,'N/A') = 'N/A'
AND COALESCE(TODDLER_FLAG,'N/A') = 'N/A'
AND COALESCE(PRESCHOOL_FLAG,'N/A') = 'N/A'
AND COALESCE(JUNIOR_FLAG,'N/A') = 'N/A'
AND COALESCE(TEEN_FLAG,'N/A') = 'N/A'
AND SUBDEPT_NAME = 'BABY S WEAR'
AND PRODUCT_NAME LIKE '%�ҧࡧ�������% M%'
;

--9-12M
UPDATE vpm_data.pm_skutagging_kids_product_master
SET "STAGE_M+0_TO_M+3" = 'N/A'
	,"STAGE_M+3_TO_M+6" = 'N/A'
	,"STAGE_M+6_TO_M+9" = 'N/A'
	,"STAGE_M+9_TO_M+12" = 'YES'
	,INFANT_FLAG = 'YES'
	,UNKNOWN_INFANT_SUBSTAGE = 'N/A'
	,INFANT_10_12M_KEYWORD = '9-12 MTH'
WHERE MOM_AND_KID_PRODUCT_FLAG = 1
AND COALESCE(PREG_FLAG,'N/A') = 'N/A'
AND COALESCE(INFANT_FLAG,'N/A') = 'N/A'
AND COALESCE(TODDLER_FLAG,'N/A') = 'N/A'
AND COALESCE(PRESCHOOL_FLAG,'N/A') = 'N/A'
AND COALESCE(JUNIOR_FLAG,'N/A') = 'N/A'
AND COALESCE(TEEN_FLAG,'N/A') = 'N/A'
AND SUBDEPT_NAME = 'BABY S WEAR'
AND (PRODUCT_NAME LIKE '%12M'
	OR PRODUCT_NAME LIKE '%M12'
	OR PRODUCT_NAME LIKE '%Y01'
	OR (CLEANED_BRANDNAME LIKE 'LAUGH' AND PRODUCT_NAME LIKE '%XXS'))
;

--1-2Y
UPDATE vpm_data.pm_skutagging_kids_product_master
SET INFANT_FLAG = 'N/A'
	,TODDLER_FLAG = 'YES'
	,TODDLER_KEYWORD = (CASE WHEN PRODUCT_NAME LIKE '%�ҧࡧ�%80%' THEN '80 CM'
									WHEN PRODUCT_NAME LIKE '%�ҧࡧ�%90%' THEN '90 CM'
									WHEN PRODUCT_NAME LIKE '%�ҧࡧ�%100%' THEN '100 CM'
									WHEN PRODUCT_NAME LIKE '%�ҧࡧ�%105%' THEN '105 CM'
									WHEN PRODUCT_NAME LIKE '%����͡����%80%' THEN '80 CM'
									WHEN PRODUCT_NAME LIKE '%����͡����%90%' THEN '90 CM'
									WHEN PRODUCT_NAME LIKE '%����͡����%100%' THEN '100 CM'
									WHEN PRODUCT_NAME LIKE '%����͡����%105%' THEN '105 CM'
									WHEN PRODUCT_NAME LIKE '%�ҧࡧ�������% L%' THEN 'DIAPER SIZE L'
									WHEN PRODUCT_NAME LIKE '%18M' THEN '12-18 MTH'
									WHEN PRODUCT_NAME LIKE '%24M' THEN '18-24 MTH'
									WHEN PRODUCT_NAME LIKE '%100' THEN '100 CM'
									WHEN PRODUCT_NAME LIKE '%105' THEN '105 CM'
									WHEN PRODUCT_NAME LIKE '%M18' THEN '12-18 MTH'
									WHEN PRODUCT_NAME LIKE '%Y02' THEN '1-2 Y'
									WHEN (CLEANED_BRANDNAME LIKE 'LAUGH' AND PRODUCT_NAME LIKE '%XS') THEN '����ͼ���� LAUGH SIZE XS'
									WHEN (CLEANED_BRANDNAME LIKE 'LAUGH' AND PRODUCT_NAME LIKE '%SX') THEN '����ͼ���� LAUGH SIZE XS'
									WHEN (CLEANED_BRANDNAME LIKE 'LAUGH' AND PRODUCT_NAME LIKE '%S') THEN '����ͼ���� LAUGH SIZE S'
									ELSE TODDLER_KEYWORD END)
WHERE MOM_AND_KID_PRODUCT_FLAG = 1
AND COALESCE(PREG_FLAG,'N/A') = 'N/A'
AND COALESCE(INFANT_FLAG,'N/A') = 'N/A'
AND COALESCE(TODDLER_FLAG,'N/A') = 'N/A'
AND COALESCE(PRESCHOOL_FLAG,'N/A') = 'N/A'
AND COALESCE(JUNIOR_FLAG,'N/A') = 'N/A'
AND COALESCE(TEEN_FLAG,'N/A') = 'N/A'
AND SUBDEPT_NAME = 'BABY S WEAR'
AND (PRODUCT_NAME LIKE '%�ҧࡧ�%80%'
	OR PRODUCT_NAME LIKE '%�ҧࡧ�%90%'
	OR PRODUCT_NAME LIKE '%�ҧࡧ�%100%'
	OR PRODUCT_NAME LIKE '%�ҧࡧ�%105%'
	OR PRODUCT_NAME LIKE '%����͡����%80%'
	OR PRODUCT_NAME LIKE '%����͡����%90%'
	OR PRODUCT_NAME LIKE '%����͡����%100%'
	OR PRODUCT_NAME LIKE '%����͡����%105%'
	OR PRODUCT_NAME LIKE '%�ҧࡧ�������% L%'
	OR PRODUCT_NAME LIKE '%18M'
	OR PRODUCT_NAME LIKE '%24M'
	OR PRODUCT_NAME LIKE '%100'
	OR PRODUCT_NAME LIKE '%105'
	OR PRODUCT_NAME LIKE '%M18'
	OR PRODUCT_NAME LIKE '%Y02'
	OR (CLEANED_BRANDNAME LIKE 'LAUGH' AND PRODUCT_NAME LIKE '%XS')
	OR (CLEANED_BRANDNAME LIKE 'LAUGH' AND PRODUCT_NAME LIKE '%SX')
	OR (CLEANED_BRANDNAME LIKE 'LAUGH' AND PRODUCT_NAME LIKE '%S'))
;

--NOT INFANT (TODDLER) 2-4
UPDATE vpm_data.pm_skutagging_kids_product_master
SET INFANT_FLAG = 'N/A'
	,UNKNOWN_INFANT_SUBSTAGE = NULL
	,TODDLER_FLAG = 'YES'
	,PRESCHOOL_FLAG = 'N/A'
	,JUNIOR_FLAG = 'N/A'
	,TODDLER_KEYWORD = (CASE WHEN PRODUCT_NAME LIKE '%36M' THEN '24-36 MTH'
							WHEN PRODUCT_NAME LIKE '%3Y' THEN '2-3 Y'
							WHEN PRODUCT_NAME LIKE '%4Y' THEN '3-4 Y'
							WHEN PRODUCT_NAME LIKE '%3A' THEN '2-3 Y'
							WHEN PRODUCT_NAME LIKE '%4A' THEN '3-4 Y'
							WHEN PRODUCT_NAME LIKE '%Y03' THEN '2-3 Y'
							WHEN PRODUCT_NAME LIKE '%Y04' THEN '3-4 Y'
							WHEN PRODUCT_NAME LIKE '% 03' THEN '2-3 Y'
							WHEN PRODUCT_NAME LIKE '% 04' THEN '3-4 Y'
							WHEN (CLEANED_BRANDNAME LIKE 'LAUGH' AND PRODUCT_NAME LIKE '%M') THEN '����ͼ���� LAUGH SIZE M'
							WHEN (CLEANED_BRANDNAME LIKE 'LAUGH' AND PRODUCT_NAME LIKE '%L' AND PRODUCT_NAME NOT LIKE '%XL') THEN '����ͼ���� LAUGH SIZE XL'
							ELSE TODDLER_KEYWORD END)
WHERE MOM_AND_KID_PRODUCT_FLAG = 1
AND COALESCE(PREG_FLAG,'N/A') = 'N/A'
AND COALESCE(INFANT_FLAG,'N/A') = 'N/A'
AND COALESCE(TODDLER_FLAG,'N/A') = 'N/A'
AND COALESCE(PRESCHOOL_FLAG,'N/A') = 'N/A'
AND COALESCE(JUNIOR_FLAG,'N/A') = 'N/A'
AND COALESCE(TEEN_FLAG,'N/A') = 'N/A'
AND SUBDEPT_NAME = 'BABY S WEAR'
AND (PRODUCT_NAME LIKE '%36M'
	OR PRODUCT_NAME LIKE '%3Y'
	OR PRODUCT_NAME LIKE '%4Y'
	OR PRODUCT_NAME LIKE '%3A'
	OR PRODUCT_NAME LIKE '%4A'
	OR PRODUCT_NAME LIKE '%Y03'
	OR PRODUCT_NAME LIKE '%Y04'
	OR PRODUCT_NAME LIKE '% 03'
	OR PRODUCT_NAME LIKE '% 04'
	OR (CLEANED_BRANDNAME LIKE 'LAUGH' AND PRODUCT_NAME LIKE '%M')
	OR (CLEANED_BRANDNAME LIKE 'LAUGH' AND PRODUCT_NAME LIKE '%L' AND PRODUCT_NAME NOT LIKE '%XL'))
;

--NOT INFANT (PRESCHOOL) 4-6
UPDATE vpm_data.pm_skutagging_kids_product_master
SET INFANT_FLAG = 'N/A'
	,UNKNOWN_INFANT_SUBSTAGE = NULL
	,TODDLER_FLAG = 'N/A'
	,PRESCHOOL_FLAG = 'YES'
	,JUNIOR_FLAG = 'N/A'
WHERE MOM_AND_KID_PRODUCT_FLAG = 1
AND COALESCE(PREG_FLAG,'N/A') = 'N/A'
AND COALESCE(INFANT_FLAG,'N/A') = 'N/A'
AND COALESCE(TODDLER_FLAG,'N/A') = 'N/A'
AND COALESCE(PRESCHOOL_FLAG,'N/A') = 'N/A'
AND COALESCE(JUNIOR_FLAG,'N/A') = 'N/A'
AND COALESCE(TEEN_FLAG,'N/A') = 'N/A'
AND SUBDEPT_NAME = 'BABY S WEAR'
AND (PRODUCT_NAME LIKE '%5Y'
	OR PRODUCT_NAME LIKE '%6Y'
	OR PRODUCT_NAME LIKE '%A5'
	OR PRODUCT_NAME LIKE '%5A%'
	OR PRODUCT_NAME LIKE '% 05'
	OR PRODUCT_NAME LIKE '% 06'
	OR PRODUCT_NAME LIKE '%Y05'
	OR PRODUCT_NAME LIKE '%Y06'
	OR (CLEANED_BRANDNAME LIKE 'LAUGH' AND PRODUCT_NAME LIKE '%XL' AND PRODUCT_NAME NOT LIKE '%3XL')
	OR (CLEANED_BRANDNAME LIKE 'LAUGH' AND PRODUCT_NAME LIKE '%XXL'))
;

--NOT INFANT (JUNIOR) 6-13
UPDATE vpm_data.pm_skutagging_kids_product_master
SET INFANT_FLAG = 'N/A'
	,UNKNOWN_INFANT_SUBSTAGE = NULL
	,TODDLER_FLAG = 'N/A'
	,PRESCHOOL_FLAG = 'N/A'
	,JUNIOR_FLAG = 'YES'
WHERE MOM_AND_KID_PRODUCT_FLAG = 1
AND COALESCE(PREG_FLAG,'N/A') = 'N/A'
AND COALESCE(INFANT_FLAG,'N/A') = 'N/A'
AND COALESCE(TODDLER_FLAG,'N/A') = 'N/A'
AND COALESCE(PRESCHOOL_FLAG,'N/A') = 'N/A'
AND COALESCE(JUNIOR_FLAG,'N/A') = 'N/A'
AND COALESCE(TEEN_FLAG,'N/A') = 'N/A'
AND SUBDEPT_NAME = 'BABY S WEAR'
AND (PRODUCT_NAME LIKE '%8Y'
	OR PRODUCT_NAME LIKE '%8A'
	OR PRODUCT_NAME LIKE '%10Y'
	OR PRODUCT_NAME LIKE '%Y08'
	OR PRODUCT_NAME LIKE '%Y09'
	OR PRODUCT_NAME LIKE '%Y10'
	OR PRODUCT_NAME LIKE '%Y12'
	OR (CLEANED_BRANDNAME LIKE 'LAUGH' AND PRODUCT_NAME LIKE '%3XL'))
;

--NOT INFANT (TEEN) 13-18
UPDATE vpm_data.pm_skutagging_kids_product_master
SET INFANT_FLAG = 'N/A'
	,UNKNOWN_INFANT_SUBSTAGE = NULL
	,TODDLER_FLAG = 'N/A'
	,PRESCHOOL_FLAG = 'N/A'
	,JUNIOR_FLAG = 'N/A'
	,TEEN_FLAG = 'YES'
WHERE MOM_AND_KID_PRODUCT_FLAG = 1
AND COALESCE(PREG_FLAG,'N/A') = 'N/A'
AND COALESCE(INFANT_FLAG,'N/A') = 'N/A'
AND COALESCE(TODDLER_FLAG,'N/A') = 'N/A'
AND COALESCE(PRESCHOOL_FLAG,'N/A') = 'N/A'
AND COALESCE(JUNIOR_FLAG,'N/A') = 'N/A'
AND COALESCE(TEEN_FLAG,'N/A') = 'N/A'
AND SUBDEPT_NAME = 'BABY S WEAR'
AND (PRODUCT_NAME LIKE '%Y14')
;

--ELSE CLASS_NAME TODDLER
UPDATE vpm_data.pm_skutagging_kids_product_master
SET INFANT_FLAG = 'N/A'
	,UNKNOWN_INFANT_SUBSTAGE = NULL
	,TODDLER_FLAG = 'YES'
	,PRESCHOOL_FLAG = 'N/A'
	,JUNIOR_FLAG = 'N/A'
	,TODDLER_KEYWORD = 'TODDLER'
WHERE MOM_AND_KID_PRODUCT_FLAG = 1
AND COALESCE(PREG_FLAG,'N/A') = 'N/A'
AND COALESCE(INFANT_FLAG,'N/A') = 'N/A'
AND COALESCE(TODDLER_FLAG,'N/A') = 'N/A'
AND COALESCE(PRESCHOOL_FLAG,'N/A') = 'N/A'
AND COALESCE(JUNIOR_FLAG,'N/A') = 'N/A'
AND COALESCE(TEEN_FLAG,'N/A') = 'N/A'
AND SUBDEPT_NAME = 'BABY S WEAR'
AND CLASS_NAME LIKE 'TODDLER'
;

--ELSE INFANT/TODDLERS
UPDATE vpm_data.pm_skutagging_kids_product_master
SET INFANT_FLAG = 'YES'
	,UNKNOWN_INFANT_SUBSTAGE = 'YES'
	,TODDLER_FLAG = 'YES'
	,PRESCHOOL_FLAG = 'N/A'
	,JUNIOR_FLAG = 'N/A'
	,INFANT_UNKNOWN_KEYWORD = (CASE WHEN SUBDEPT_NAME = 'BABY S WEAR' THEN SUBDEPT_NAME
									WHEN SUBDEPT_NAME = 'BABY GOODS' THEN SUBDEPT_NAME
									WHEN PRODUCT_NAME LIKE '%�������%' THEN '�������'
									WHEN PRODUCT_NAME LIKE '%���ٴ����١%' THEN '���ٴ����١'
									WHEN PRODUCT_NAME LIKE '%���ҧ�ٹ͹%' THEN '���ҧ�ٹ͹'
									WHEN PRODUCT_NAME LIKE '%����%��蹼��%' THEN '������蹼������'
									WHEN PRODUCT_NAME LIKE '%�ç��ҧ�Ǵ��%' THEN '�ç��ҧ�Ǵ��'
									WHEN PRODUCT_NAME LIKE '%�蹫Ѻ��ӹ�%' THEN '�蹫Ѻ��ӹ�'
									WHEN PRODUCT_NAME LIKE '%�Ǵ��%' THEN '�Ǵ��'
									ELSE INFANT_UNKNOWN_KEYWORD END)
	,TODDLER_KEYWORD = (CASE WHEN SUBDEPT_NAME = 'INFANT/TODDLERS' THEN SUBDEPT_NAME
									WHEN SUBDEPT_NAME = 'BABY GOODS' THEN SUBDEPT_NAME
									WHEN PRODUCT_NAME LIKE '%�������%' THEN '�������'
									WHEN PRODUCT_NAME LIKE '%���ٴ����١%' THEN '���ٴ����١'
									WHEN PRODUCT_NAME LIKE '%���ҧ�ٹ͹%' THEN '���ҧ�ٹ͹'
									WHEN PRODUCT_NAME LIKE '%����%��蹼��%' THEN '������蹼������'
									WHEN PRODUCT_NAME LIKE '%�ç��ҧ�Ǵ��%' THEN '�ç��ҧ�Ǵ��'
									WHEN PRODUCT_NAME LIKE '%�蹫Ѻ��ӹ�%' THEN '�蹫Ѻ��ӹ�'
									WHEN PRODUCT_NAME LIKE '%�Ǵ��%' THEN '�Ǵ��'
									ELSE TODDLER_KEYWORD END)
WHERE MOM_AND_KID_PRODUCT_FLAG = 1
AND COALESCE(PREG_FLAG,'N/A') = 'N/A'
AND COALESCE(INFANT_FLAG,'N/A') = 'N/A'
AND COALESCE(TODDLER_FLAG,'N/A') = 'N/A'
AND COALESCE(PRESCHOOL_FLAG,'N/A') = 'N/A'
AND COALESCE(JUNIOR_FLAG,'N/A') = 'N/A'
AND COALESCE(TEEN_FLAG,'N/A') = 'N/A'
AND (SUBDEPT_NAME = 'BABY S WEAR'
	OR SUBDEPT_NAME = 'BABY GOODS'
	OR PRODUCT_NAME LIKE '%�������%'
	OR PRODUCT_NAME LIKE '%���ٴ����١%'
	OR PRODUCT_NAME LIKE '%���ҧ�ٹ͹%'
	OR PRODUCT_NAME LIKE '%����%��蹼��%'
	OR PRODUCT_NAME LIKE '%�ç��ҧ�Ǵ��%'
	OR PRODUCT_NAME LIKE '%�蹫Ѻ��ӹ�%'
	OR PRODUCT_NAME LIKE '%�Ǵ��%')
;

-----MILK & BABY FOOD
--MOM SUPPLEMENT
UPDATE vpm_data.pm_skutagging_kids_product_master
SET PREG_FLAG = 'YES'
	,UNKNOWN_PREG_SUBSTAGE = 'YES'
	,INFANT_FLAG = 'YES'
	,UNKNOWN_INFANT_SUBSTAGE = 'YES'
	,PREG_UNKNOWN_KEYWORD = 'MATERNITY SUPPLEMENT'
	,INFANT_UNKNOWN_KEYWORD = 'MATERNITY SUPPLEMENT'
WHERE MOM_AND_KID_PRODUCT_FLAG = 1
AND COALESCE(PREG_FLAG,'N/A') = 'N/A'
AND COALESCE(INFANT_FLAG,'N/A') = 'N/A'
AND COALESCE(TODDLER_FLAG,'N/A') = 'N/A'
AND COALESCE(PRESCHOOL_FLAG,'N/A') = 'N/A'
AND COALESCE(JUNIOR_FLAG,'N/A') = 'N/A'
AND COALESCE(TEEN_FLAG,'N/A') = 'N/A'
AND SUBDEPT_NAME = 'MILK & BABY FOOD'
AND (PRODUCT_NAME LIKE '%MAMA%'
	OR CLEANED_BRANDNAME LIKE 'MOMLIKE'
	OR CLEANED_BRANDNAME LIKE 'MOMMY JUICY'
	OR PRODUCT_NAME LIKE '%DUMUM%PREGNANT%')
;

--6M+
UPDATE vpm_data.pm_skutagging_kids_product_master
SET "STAGE_M+0_TO_M+3" = 'N/A'
	,"STAGE_M+3_TO_M+6" = 'N/A'
	,"STAGE_M+6_TO_M+9" = 'YES'
	,"STAGE_M+9_TO_M+12" = 'YES'
	,INFANT_FLAG = 'YES'
	,UNKNOWN_INFANT_SUBSTAGE = 'N/A'
	,TODDLER_FLAG = 'YES'
	,INFANT_7_9M_KEYWORD = (CASE WHEN PRODUCT_NAME LIKE 'HOORAY%' THEN 'HOORAY 6M+'
								WHEN CLEANED_BRANDNAME LIKE 'SWEETPEA' THEN 'SWEETPEA'
								WHEN CLEANED_BRANDNAME LIKE 'NAMCHOW HAPPY BITES' THEN 'NAMCHOW HAPPY BITES'
								WHEN CLEANED_BRANDNAME LIKE 'PEACHY' THEN 'PEACHY'
								ELSE INFANT_7_9M_KEYWORD END)
	,INFANT_10_12M_KEYWORD = (CASE WHEN PRODUCT_NAME LIKE 'HOORAY%' THEN 'HOORAY 6M+'
								WHEN CLEANED_BRANDNAME LIKE 'SWEETPEA' THEN 'SWEETPEA'
								WHEN CLEANED_BRANDNAME LIKE 'NAMCHOW HAPPY BITES' THEN 'NAMCHOW HAPPY BITES'
								WHEN CLEANED_BRANDNAME LIKE 'PEACHY' THEN 'PEACHY'
								ELSE INFANT_10_12M_KEYWORD END)
WHERE MOM_AND_KID_PRODUCT_FLAG = 1
AND COALESCE(PREG_FLAG,'N/A') = 'N/A'
AND COALESCE(INFANT_FLAG,'N/A') = 'N/A'
AND COALESCE(TODDLER_FLAG,'N/A') = 'N/A'
AND COALESCE(PRESCHOOL_FLAG,'N/A') = 'N/A'
AND COALESCE(JUNIOR_FLAG,'N/A') = 'N/A'
AND COALESCE(TEEN_FLAG,'N/A') = 'N/A'
AND (PRODUCT_NAME LIKE 'HOORAY CHICKEN&VEGETABLE%'
	OR PRODUCT_NAME LIKE 'HOORAYCORNPOTATOCHICKENPURE%'
	OR CLEANED_BRANDNAME IN ('SWEETPEA','NAMCHOW HAPPY BITES')
	OR (CLEANED_BRANDNAME = 'PEACHY' AND (PRODUCT_NAME NOT LIKE '%SMOOTHIE%'
								AND PRODUCT_NAME NOT LIKE '%PUFF%'
								AND PRODUCT_NAME NOT LIKE '%COOKIE%'
								AND PRODUCT_NAME NOT LIKE '%BISCUIT%')))
;

--10M+
UPDATE vpm_data.pm_skutagging_kids_product_master
SET "STAGE_M+0_TO_M+3" = 'N/A'
	,"STAGE_M+3_TO_M+6" = 'N/A'
	,"STAGE_M+6_TO_M+9" = 'N/A'
	,"STAGE_M+9_TO_M+12" = 'YES'
	,INFANT_FLAG = 'YES'
	,UNKNOWN_INFANT_SUBSTAGE = 'N/A'
	,TODDLER_FLAG = 'YES'
	,INFANT_10_12M_KEYWORD = 'HOORAY 10M+'
	,TODDLER_KEYWORD = 'HOORAY 10M+'
WHERE MOM_AND_KID_PRODUCT_FLAG = 1
AND COALESCE(PREG_FLAG,'N/A') = 'N/A'
AND COALESCE(INFANT_FLAG,'N/A') = 'N/A'
AND COALESCE(TODDLER_FLAG,'N/A') = 'N/A'
AND COALESCE(PRESCHOOL_FLAG,'N/A') = 'N/A'
AND COALESCE(JUNIOR_FLAG,'N/A') = 'N/A'
AND COALESCE(TEEN_FLAG,'N/A') = 'N/A'
AND PRODUCT_NAME LIKE 'HOORAYCHICKBOLOGNESSPAGHETI%'
;

--FORMULA 3 (1Y+)
UPDATE vpm_data.pm_skutagging_kids_product_master
SET INFANT_FLAG = 'N/A'
	,TODDLER_FLAG = 'YES'
	,TODDLER_KEYWORD = (CASE WHEN PRODUCT_NAME LIKE 'HI Q%' THEN 'MILK POWDER FORMULA 3'
									WHEN PRODUCT_NAME LIKE 'BEAR BRAND%' THEN 'MILK POWDER FORMULA 3'
									WHEN PRODUCT_NAME LIKE 'S26%' THEN 'MILK POWDER FORMULA 3'
									WHEN PRODUCT_NAME LIKE 'DUGRO%' THEN 'MILK POWDER FORMULA 3'
									WHEN PRODUCT_NAME LIKE 'ENFAGROW3%' THEN 'MILK POWDER FORMULA 3'
									ELSE 'KID NOURISHMENT 1Y+' END)
WHERE MOM_AND_KID_PRODUCT_FLAG = 1
AND COALESCE(PREG_FLAG,'N/A') = 'N/A'
AND COALESCE(INFANT_FLAG,'N/A') = 'N/A'
AND COALESCE(TODDLER_FLAG,'N/A') = 'N/A'
AND COALESCE(PRESCHOOL_FLAG,'N/A') = 'N/A'
AND COALESCE(JUNIOR_FLAG,'N/A') = 'N/A'
AND COALESCE(TEEN_FLAG,'N/A') = 'N/A'
AND SUBDEPT_NAME = 'MILK & BABY FOOD'
AND (PRODUCT_NAME LIKE 'HI Q 1 PLUS%'
	OR PRODUCT_NAME LIKE 'HI Q 1PLUS%'
	OR PRODUCT_NAME LIKE 'BEAR BRAND 3 %'
	OR PRODUCT_NAME LIKE 'S26 GOLD PROGRESS 3%'
	OR PRODUCT_NAME LIKE 'S26 GOLD PROGRESS3%'
	OR PRODUCT_NAME LIKE 'DUGRO 1PLUS%'
	OR PRODUCT_NAME LIKE 'ENFAGROW3%'
	OR CLEANED_BRANDNAME LIKE 'LITTLE MUNCHY'
	OR PRODUCT_NAME LIKE '%PEACHY%SMOOTHIE%'
	OR PRODUCT_NAME LIKE '%PEACHY%PUFF%'
	OR PRODUCT_NAME LIKE '%PEACHY%COOKIE%'
	OR PRODUCT_NAME LIKE '%PEACHY%BISCUIT%')
;

--FORMULA 4 (3Y+)
UPDATE vpm_data.pm_skutagging_kids_product_master
SET INFANT_FLAG = 'N/A'
	,UNKNOWN_INFANT_SUBSTAGE = NULL
	,TODDLER_FLAG = 'YES'
	,TODDLER_KEYWORD = 'MILK POWDER FORMULA 4'
WHERE MOM_AND_KID_PRODUCT_FLAG = 1
AND COALESCE(PREG_FLAG,'N/A') = 'N/A'
AND COALESCE(INFANT_FLAG,'N/A') = 'N/A'
AND COALESCE(TODDLER_FLAG,'N/A') = 'N/A'
AND COALESCE(PRESCHOOL_FLAG,'N/A') = 'N/A'
AND COALESCE(JUNIOR_FLAG,'N/A') = 'N/A'
AND COALESCE(TEEN_FLAG,'N/A') = 'N/A'
AND SUBDEPT_NAME = 'MILK & BABY FOOD'
AND (PRODUCT_NAME LIKE 'HIQ 3PLUS%'
	OR PRODUCT_NAME LIKE 'BEAR BRAND 4 %'
	OR PRODUCT_NAME LIKE 'CARNATION 3 PLUS%'
	OR PRODUCT_NAME LIKE 'S26 GOLD PROGRESS 4%'
	OR PRODUCT_NAME LIKE 'S26 GOLD PROGRESS4%'
	OR PRODUCT_NAME LIKE 'DUGRO 3PLUS%'
	OR PRODUCT_NAME LIKE 'ENFAGROW4%'
	OR PRODUCT_NAME LIKE 'PEDIASURE 3 PLUS%'
	OR PRODUCT_NAME LIKE 'DUMILK4%')
;

--FORMULA 4 (3Y+)
UPDATE vpm_data.pm_skutagging_kids_product_master
SET INFANT_FLAG = 'N/A'
	,UNKNOWN_INFANT_SUBSTAGE = NULL
	,TODDLER_FLAG = 'YES'
	,TODDLER_KEYWORD = 'MILK POWDER FORMULA 4'
WHERE MOM_AND_KID_PRODUCT_FLAG = 1
AND COALESCE(PREG_FLAG,'N/A') = 'N/A'
AND COALESCE(INFANT_FLAG,'N/A') = 'N/A'
AND COALESCE(TODDLER_FLAG,'N/A') = 'N/A'
AND COALESCE(PRESCHOOL_FLAG,'N/A') = 'N/A'
AND COALESCE(JUNIOR_FLAG,'N/A') = 'N/A'
AND COALESCE(TEEN_FLAG,'N/A') = 'N/A'
AND REVISED_CATEGORY_LEVEL_2 = 'DAIRY PRODUCT'
AND (PRODUCT_NAME LIKE 'HIQ 3PLUS%')
;

--UNKNOWN SUBSTAGE
UPDATE vpm_data.pm_skutagging_kids_product_master
SET INFANT_FLAG = 'YES'
	,UNKNOWN_INFANT_SUBSTAGE = 'YES'
	,INFANT_UNKNOWN_KEYWORD = '����'
WHERE MOM_AND_KID_PRODUCT_FLAG = 1
AND COALESCE(PREG_FLAG,'N/A') = 'N/A'
AND COALESCE(INFANT_FLAG,'N/A') = 'N/A'
AND COALESCE(TODDLER_FLAG,'N/A') = 'N/A'
AND COALESCE(PRESCHOOL_FLAG,'N/A') = 'N/A'
AND COALESCE(JUNIOR_FLAG,'N/A') = 'N/A'
AND COALESCE(TEEN_FLAG,'N/A') = 'N/A'
AND SUBDEPT_NAME = 'MILK & BABY FOOD'
AND CLEANED_BRANDNAME IN ('ENFAGROW','PEDIASURE')
;

UPDATE vpm_data.pm_skutagging_kids_product_master
SET MOM_AND_KID_PRODUCT_FLAG = 0
WHERE MOM_AND_KID_PRODUCT_FLAG = 1
AND COALESCE(PREG_FLAG,'N/A') = 'N/A'
AND COALESCE(INFANT_FLAG,'N/A') = 'N/A'
AND COALESCE(TODDLER_FLAG,'N/A') = 'N/A'
AND COALESCE(PRESCHOOL_FLAG,'N/A') = 'N/A'
AND COALESCE(JUNIOR_FLAG,'N/A') = 'N/A'
AND COALESCE(TEEN_FLAG,'N/A') = 'N/A'
AND SUBDEPT_NAME = 'MILK & BABY FOOD'
AND CLEANED_BRANDNAME IN ('VITAMILK','DUTCH MILL','FOREMOST','LACTASOY','BEAR BRAND','DNA','ANLENE','BLUE DIAMOND','137 DEGREES','V SOY','VIFIT','CARNATION','SUNKIST','PALACE','TEAPOT','HOMESOY','HERSHEY''S','MALEE'
				,'4 CARE','OVALTINE','MALI','TOFUSAN','DAIMOND FRESH','NONGPHO','BSC','FALCON','SESAMILK','SCOTCH','MAGNOLIA','DE NOSH','DMALT','SIRICHAI','NESTLE','THAI DENMARK','MILO','BENECOL','UFC','SOYFRESH'
				,'CHITRALADA','DOIKHAM','MAGIC FARM','NATURE CHARM','TIPCO','BIRDWINGS','BENEFITT','GOLD MILK','PRO-FIT','SOY POP','MMILK','SEGAFREDO','TIPCO BEAT','TOPS','ORCHID','CHOKCHAI','YOMOST','SIPSO','THAI FARM MILK'
				,'SHIP','SANG SANG','TASTIFIT','DUGRO','ENSURE','ENFASCHOOL','GLUCENA SR')
;

UPDATE vpm_data.pm_skutagging_kids_product_master
SET MOM_AND_KID_PRODUCT_FLAG = 0
WHERE MOM_AND_KID_PRODUCT_FLAG = 1
AND COALESCE(PREG_FLAG,'N/A') = 'N/A'
AND COALESCE(INFANT_FLAG,'N/A') = 'N/A'
AND COALESCE(TODDLER_FLAG,'N/A') = 'N/A'
AND COALESCE(PRESCHOOL_FLAG,'N/A') = 'N/A'
AND COALESCE(JUNIOR_FLAG,'N/A') = 'N/A'
AND COALESCE(TEEN_FLAG,'N/A') = 'N/A'
AND (CLEANED_BRANDNAME LIKE 'FOREMOST')
;


-----DEPT NAME MILK & BABY FOOD
--FORMULA 1 (<1Y)
UPDATE vpm_data.pm_skutagging_kids_product_master
SET INFANT_FLAG = 'YES'
	,UNKNOWN_INFANT_SUBSTAGE = 'YES'
	,TODDLER_FLAG = 'N/A'
	,INFANT_0_3M_KEYWORD = 'MILK POWDER FORMULA 1'
	,INFANT_4_6M_KEYWORD = 'MILK POWDER FORMULA 1'
	,INFANT_7_9M_KEYWORD = 'MILK POWDER FORMULA 1'
	,INFANT_10_12M_KEYWORD = 'MILK POWDER FORMULA 1'
WHERE MOM_AND_KID_PRODUCT_FLAG = 1
AND COALESCE(PREG_FLAG,'N/A') = 'N/A'
AND COALESCE(INFANT_FLAG,'N/A') = 'N/A'
AND COALESCE(TODDLER_FLAG,'N/A') = 'N/A'
AND COALESCE(PRESCHOOL_FLAG,'N/A') = 'N/A'
AND COALESCE(JUNIOR_FLAG,'N/A') = 'N/A'
AND COALESCE(TEEN_FLAG,'N/A') = 'N/A'
AND DEPT_NAME = 'MILK & BABY FOOD'
AND PRODUCT_NAME LIKE '%�����á�Դ%'
;

--FORMULA 2 (6M-3Y)
UPDATE vpm_data.pm_skutagging_kids_product_master
SET "STAGE_M+0_TO_M+3" = 'N/A'
	,"STAGE_M+3_TO_M+6" = 'N/A'
	,"STAGE_M+6_TO_M+9" = 'YES'
	,"STAGE_M+9_TO_M+12" = 'YES'
	,INFANT_FLAG = 'YES'
	,UNKNOWN_INFANT_SUBSTAGE = 'N/A'
	,TODDLER_FLAG = 'YES'
	,INFANT_7_9M_KEYWORD = 'MILK POWDER FORMULA 2'
	,INFANT_10_12M_KEYWORD = 'MILK POWDER FORMULA 2'
	,TODDLER_KEYWORD = 'MILK POWDER FORMULA 2'
WHERE MOM_AND_KID_PRODUCT_FLAG = 1
AND COALESCE(PREG_FLAG,'N/A') = 'N/A'
AND COALESCE(INFANT_FLAG,'N/A') = 'N/A'
AND COALESCE(TODDLER_FLAG,'N/A') = 'N/A'
AND COALESCE(PRESCHOOL_FLAG,'N/A') = 'N/A'
AND COALESCE(JUNIOR_FLAG,'N/A') = 'N/A'
AND COALESCE(TEEN_FLAG,'N/A') = 'N/A'
AND DEPT_NAME = 'MILK & BABY FOOD'
AND PRODUCT_NAME LIKE '%����%������ͧ%'
;

--ELSE UNKNOWN SUBSTAGE
UPDATE vpm_data.pm_skutagging_kids_product_master
SET INFANT_FLAG = 'YES'
	,UNKNOWN_INFANT_SUBSTAGE = 'YES'
	,INFANT_UNKNOWN_KEYWORD = 'MILK POWDER AND BABY NOURISHMENT'
WHERE MOM_AND_KID_PRODUCT_FLAG = 1
AND COALESCE(PREG_FLAG,'N/A') = 'N/A'
AND COALESCE(INFANT_FLAG,'N/A') = 'N/A'
AND COALESCE(TODDLER_FLAG,'N/A') = 'N/A'
AND COALESCE(PRESCHOOL_FLAG,'N/A') = 'N/A'
AND COALESCE(JUNIOR_FLAG,'N/A') = 'N/A'
AND COALESCE(TEEN_FLAG,'N/A') = 'N/A'
AND DEPT_NAME = 'MILK & BABY FOOD'
;

-----CLASS BABY GOODS
--NIPPLE SIZE S 0-3M
UPDATE vpm_data.pm_skutagging_kids_product_master
SET "STAGE_M+0_TO_M+3" = 'YES'
	,"STAGE_M+3_TO_M+6" = 'N/A'
	,"STAGE_M+6_TO_M+9" = 'N/A'
	,"STAGE_M+9_TO_M+12" = 'N/A'
	,INFANT_FLAG = 'YES'
	,UNKNOWN_INFANT_SUBSTAGE = 'N/A'
	,TODDLER_FLAG = 'N/A'
	,INFANT_0_3M_KEYWORD = '�ء�� S'
WHERE MOM_AND_KID_PRODUCT_FLAG = 1
AND COALESCE(PREG_FLAG,'N/A') = 'N/A'
AND COALESCE(INFANT_FLAG,'N/A') = 'N/A'
AND COALESCE(TODDLER_FLAG,'N/A') = 'N/A'
AND COALESCE(PRESCHOOL_FLAG,'N/A') = 'N/A'
AND COALESCE(JUNIOR_FLAG,'N/A') = 'N/A'
AND COALESCE(TEEN_FLAG,'N/A') = 'N/A'
AND CLASS_NAME = 'BABY GOODS'
AND (PRODUCT_NAME LIKE '%NIPPLE S %'
	OR PRODUCT_NAME LIKE '%NIPPLE S'
	OR PRODUCT_NAME LIKE '%NIPPLE SIZE S%')
;

--NIPPLE SIZE M 3-6M
UPDATE vpm_data.pm_skutagging_kids_product_master
SET "STAGE_M+0_TO_M+3" = 'N/A'
	,"STAGE_M+3_TO_M+6" = 'YES'
	,"STAGE_M+6_TO_M+9" = 'N/A'
	,"STAGE_M+9_TO_M+12" = 'N/A'
	,INFANT_FLAG = 'YES'
	,UNKNOWN_INFANT_SUBSTAGE = 'N/A'
	,TODDLER_FLAG = 'N/A'
	,INFANT_4_6M_KEYWORD = '�ء�� M'
WHERE MOM_AND_KID_PRODUCT_FLAG = 1
AND COALESCE(PREG_FLAG,'N/A') = 'N/A'
AND COALESCE(INFANT_FLAG,'N/A') = 'N/A'
AND COALESCE(TODDLER_FLAG,'N/A') = 'N/A'
AND COALESCE(PRESCHOOL_FLAG,'N/A') = 'N/A'
AND COALESCE(JUNIOR_FLAG,'N/A') = 'N/A'
AND COALESCE(TEEN_FLAG,'N/A') = 'N/A'
AND CLASS_NAME = 'BABY GOODS'
AND (PRODUCT_NAME LIKE '%NIPPLE M %'
	OR PRODUCT_NAME LIKE '%NIPPLE M'
	OR PRODUCT_NAME LIKE '%NIPPLE SIZE M%')
;

--ELSE NIPPLE UNKNOWN SUBSTAGE
UPDATE vpm_data.pm_skutagging_kids_product_master
SET INFANT_FLAG = 'YES'
	,UNKNOWN_INFANT_SUBSTAGE = 'YES'
	,TODDLER_FLAG = 'N/A'
	,INFANT_UNKNOWN_KEYWORD = '�ء��'
WHERE MOM_AND_KID_PRODUCT_FLAG = 1
AND COALESCE(PREG_FLAG,'N/A') = 'N/A'
AND COALESCE(INFANT_FLAG,'N/A') = 'N/A'
AND COALESCE(TODDLER_FLAG,'N/A') = 'N/A'
AND COALESCE(PRESCHOOL_FLAG,'N/A') = 'N/A'
AND COALESCE(JUNIOR_FLAG,'N/A') = 'N/A'
AND COALESCE(TEEN_FLAG,'N/A') = 'N/A'
AND CLASS_NAME = 'BABY GOODS'
AND (PRODUCT_NAME LIKE '%NIPPLE%')
;

--0-3M
UPDATE vpm_data.pm_skutagging_kids_product_master
SET "STAGE_M+0_TO_M+3" = 'YES'
	,"STAGE_M+3_TO_M+6" = 'N/A'
	,"STAGE_M+6_TO_M+9" = 'N/A'
	,"STAGE_M+9_TO_M+12" = 'N/A'
	,INFANT_FLAG = 'YES'
	,UNKNOWN_INFANT_SUBSTAGE = 'N/A'
	,INFANT_0_3M_KEYWORD = '0-3 MTH'
WHERE MOM_AND_KID_PRODUCT_FLAG = 1
AND COALESCE(PREG_FLAG,'N/A') = 'N/A'
AND COALESCE(INFANT_FLAG,'N/A') = 'N/A'
AND COALESCE(TODDLER_FLAG,'N/A') = 'N/A'
AND COALESCE(PRESCHOOL_FLAG,'N/A') = 'N/A'
AND COALESCE(JUNIOR_FLAG,'N/A') = 'N/A'
AND COALESCE(TEEN_FLAG,'N/A') = 'N/A'
AND CLASS_NAME = 'BABY GOODS'
AND PRODUCT_NAME LIKE '%0 TO 3 MONTH%'
;

--PACIFIER 0-12M
UPDATE vpm_data.pm_skutagging_kids_product_master
SET INFANT_FLAG = 'YES'
	,UNKNOWN_INFANT_SUBSTAGE = 'YES'
	,TODDLER_FLAG = 'N/A'
	,INFANT_0_3M_KEYWORD = 'PACIFIER'
	,INFANT_4_6M_KEYWORD = 'PACIFIER'
	,INFANT_7_9M_KEYWORD = 'PACIFIER'
	,INFANT_10_12M_KEYWORD = 'PACIFIER'
WHERE MOM_AND_KID_PRODUCT_FLAG = 1
AND COALESCE(PREG_FLAG,'N/A') = 'N/A'
AND COALESCE(INFANT_FLAG,'N/A') = 'N/A'
AND COALESCE(TODDLER_FLAG,'N/A') = 'N/A'
AND COALESCE(PRESCHOOL_FLAG,'N/A') = 'N/A'
AND COALESCE(JUNIOR_FLAG,'N/A') = 'N/A'
AND COALESCE(TEEN_FLAG,'N/A') = 'N/A'
AND PRODUCT_NAME LIKE '%PACIFIER%'
;

--3-6M
UPDATE vpm_data.pm_skutagging_kids_product_master
SET "STAGE_M+0_TO_M+3" = 'N/A'
	,"STAGE_M+3_TO_M+6" = 'YES'
	,"STAGE_M+6_TO_M+9" = 'N/A'
	,"STAGE_M+9_TO_M+12" = 'N/A'
	,INFANT_FLAG = 'YES'
	,UNKNOWN_INFANT_SUBSTAGE = 'N/A'
	,INFANT_4_6M_KEYWORD = '3-6 MTH'
WHERE MOM_AND_KID_PRODUCT_FLAG = 1
AND COALESCE(PREG_FLAG,'N/A') = 'N/A'
AND COALESCE(INFANT_FLAG,'N/A') = 'N/A'
AND COALESCE(TODDLER_FLAG,'N/A') = 'N/A'
AND COALESCE(PRESCHOOL_FLAG,'N/A') = 'N/A'
AND COALESCE(JUNIOR_FLAG,'N/A') = 'N/A'
AND COALESCE(TEEN_FLAG,'N/A') = 'N/A'
AND CLASS_NAME = 'BABY GOODS'
AND PRODUCT_NAME LIKE '%3 TO 6 MONTH%'
;

--3-9M
UPDATE vpm_data.pm_skutagging_kids_product_master
SET "STAGE_M+0_TO_M+3" = 'N/A'
	,"STAGE_M+3_TO_M+6" = 'YES'
	,"STAGE_M+6_TO_M+9" = 'YES'
	,"STAGE_M+9_TO_M+12" = 'N/A'
	,INFANT_FLAG = 'YES'
	,UNKNOWN_INFANT_SUBSTAGE = 'N/A'
	,INFANT_4_6M_KEYWORD = 'RATTLE'
	,INFANT_7_9M_KEYWORD = 'RATTLE'
WHERE MOM_AND_KID_PRODUCT_FLAG = 1
AND COALESCE(PREG_FLAG,'N/A') = 'N/A'
AND COALESCE(INFANT_FLAG,'N/A') = 'N/A'
AND COALESCE(TODDLER_FLAG,'N/A') = 'N/A'
AND COALESCE(PRESCHOOL_FLAG,'N/A') = 'N/A'
AND COALESCE(JUNIOR_FLAG,'N/A') = 'N/A'
AND COALESCE(TEEN_FLAG,'N/A') = 'N/A'
AND CLASS_NAME = 'BABY GOODS'
AND (PRODUCT_NAME LIKE '%RADDLE%'
	OR PRODUCT_NAME LIKE '%RATTLE%')
;

--TEETHER 3M+
UPDATE vpm_data.pm_skutagging_kids_product_master
SET "STAGE_M+0_TO_M+3" = 'N/A'
	,"STAGE_M+3_TO_M+6" = 'YES'
	,"STAGE_M+6_TO_M+9" = 'YES'
	,"STAGE_M+9_TO_M+12" = 'YES'
	,INFANT_FLAG = 'YES'
	,UNKNOWN_INFANT_SUBSTAGE = 'N/A'
	,INFANT_4_6M_KEYWORD = '�ҧ�Ѵ'
	,INFANT_7_9M_KEYWORD = '�ҧ�Ѵ'
	,INFANT_10_12M_KEYWORD = '�ҧ�Ѵ'
WHERE MOM_AND_KID_PRODUCT_FLAG = 1
AND COALESCE(PREG_FLAG,'N/A') = 'N/A'
AND COALESCE(INFANT_FLAG,'N/A') = 'N/A'
AND COALESCE(TODDLER_FLAG,'N/A') = 'N/A'
AND COALESCE(PRESCHOOL_FLAG,'N/A') = 'N/A'
AND COALESCE(JUNIOR_FLAG,'N/A') = 'N/A'
AND COALESCE(TEEN_FLAG,'N/A') = 'N/A'
AND CLASS_NAME = 'BABY GOODS'
AND (PRODUCT_NAME LIKE '%TEETHE%'
	OR PRODUCT_NAME LIKE '%THEEHER%')
;

--6M+
UPDATE vpm_data.pm_skutagging_kids_product_master
SET "STAGE_M+0_TO_M+3" = 'N/A'
	,"STAGE_M+3_TO_M+6" = 'N/A'
	,"STAGE_M+6_TO_M+9" = 'YES'
	,"STAGE_M+9_TO_M+12" = 'YES'
	,INFANT_FLAG = 'YES'
	,UNKNOWN_INFANT_SUBSTAGE = 'N/A'
	,TODDLER_FLAG = 'YES'
	,INFANT_7_9M_KEYWORD = (CASE WHEN PRODUCT_NAME LIKE '%CUP%' THEN '����'
								WHEN PRODUCT_NAME LIKE '%SPOON%' THEN '��͹'
								WHEN PRODUCT_NAME LIKE '%BOWL%' THEN '���'
								WHEN PRODUCT_NAME LIKE '%FOOD%GRINDER%' THEN '��躴�����'
								WHEN PRODUCT_NAME LIKE '%GUM SOOTHER%' THEN  '�ҧ�Ѵ'
								WHEN PRODUCT_NAME LIKE '%KNEE%GUARD%' THEN 'KNEE GUARD'
								WHEN PRODUCT_NAME LIKE '%MELODY%' THEN 'MELODY'
								ELSE INFANT_7_9M_KEYWORD END)
	,INFANT_10_12M_KEYWORD = (CASE WHEN PRODUCT_NAME LIKE '%CUP%' THEN '����'
								WHEN PRODUCT_NAME LIKE '%SPOON%' THEN '��͹'
								WHEN PRODUCT_NAME LIKE '%BOWL%' THEN '���'
								WHEN PRODUCT_NAME LIKE '%FOOD%GRINDER%' THEN '��躴�����'
								WHEN PRODUCT_NAME LIKE '%GUM SOOTHER%' THEN  '�ҧ�Ѵ'
								WHEN PRODUCT_NAME LIKE '%KNEE%GUARD%' THEN 'KNEE GUARD'
								WHEN PRODUCT_NAME LIKE '%MELODY%' THEN 'MELODY'
								ELSE INFANT_10_12M_KEYWORD END)
	,TODDLER_KEYWORD = (CASE WHEN PRODUCT_NAME LIKE '%CUP%' THEN '����'
								WHEN PRODUCT_NAME LIKE '%SPOON%' THEN '��͹'
								WHEN PRODUCT_NAME LIKE '%BOWL%' THEN '���'
								WHEN PRODUCT_NAME LIKE '%FOOD%GRINDER%' THEN '��躴�����'
								WHEN PRODUCT_NAME LIKE '%GUM SOOTHER%' THEN  '�ҧ�Ѵ'
								WHEN PRODUCT_NAME LIKE '%KNEE%GUARD%' THEN 'KNEE GUARD'
								WHEN PRODUCT_NAME LIKE '%MELODY%' THEN 'MELODY'
								ELSE TODDLER_KEYWORD END)
WHERE MOM_AND_KID_PRODUCT_FLAG = 1
AND COALESCE(PREG_FLAG,'N/A') = 'N/A'
AND COALESCE(INFANT_FLAG,'N/A') = 'N/A'
AND COALESCE(TODDLER_FLAG,'N/A') = 'N/A'
AND COALESCE(PRESCHOOL_FLAG,'N/A') = 'N/A'
AND COALESCE(JUNIOR_FLAG,'N/A') = 'N/A'
AND COALESCE(TEEN_FLAG,'N/A') = 'N/A'
AND CLASS_NAME = 'BABY GOODS'
AND (PRODUCT_NAME LIKE '%CUP%'
	OR PRODUCT_NAME LIKE '%SPOON%'
	OR PRODUCT_NAME LIKE '%BOWL%'
	OR PRODUCT_NAME LIKE '%FOOD%GRINDER%'
	OR PRODUCT_NAME LIKE '%GUM SOOTHER%'
	OR PRODUCT_NAME LIKE '%KNEE%GUARD%'
	OR PRODUCT_NAME LIKE '%MELODY%')
;

--1Y+
UPDATE vpm_data.pm_skutagging_kids_product_master
SET INFANT_FLAG = 'N/A'
	,TODDLER_FLAG = 'YES'
	,TODDLER_KEYWORD = '���ⶹ'
WHERE MOM_AND_KID_PRODUCT_FLAG = 1
AND COALESCE(PREG_FLAG,'N/A') = 'N/A'
AND COALESCE(INFANT_FLAG,'N/A') = 'N/A'
AND COALESCE(TODDLER_FLAG,'N/A') = 'N/A'
AND COALESCE(PRESCHOOL_FLAG,'N/A') = 'N/A'
AND COALESCE(JUNIOR_FLAG,'N/A') = 'N/A'
AND COALESCE(TEEN_FLAG,'N/A') = 'N/A'
AND CLASS_NAME = 'BABY GOODS'
AND (PRODUCT_NAME LIKE '%BABY%TOILET%')
;

--BOTTLE FOR INFANT UNKNOWN SUBSTAGE
UPDATE vpm_data.pm_skutagging_kids_product_master
SET INFANT_FLAG = 'YES'
	,UNKNOWN_INFANT_SUBSTAGE = 'YES'
	,TODDLER_FLAG = 'YES'
	,INFANT_UNKNOWN_KEYWORD = (CASE WHEN PRODUCT_NAME LIKE '%BOTTLE%' THEN '�Ǵ��'
									WHEN PRODUCT_NAME LIKE '%INFANT%' THEN SUBCLASS_NAME
									ELSE INFANT_UNKNOWN_KEYWORD END)
	,TODDLER_KEYWORD  = (CASE WHEN PRODUCT_NAME LIKE '%BOTTLE%' THEN '�Ǵ��'
									WHEN PRODUCT_NAME LIKE '%INFANT%' THEN SUBCLASS_NAME
									ELSE TODDLER_KEYWORD END)
WHERE MOM_AND_KID_PRODUCT_FLAG = 1
AND COALESCE(PREG_FLAG,'N/A') = 'N/A'
AND COALESCE(INFANT_FLAG,'N/A') = 'N/A'
AND COALESCE(TODDLER_FLAG,'N/A') = 'N/A'
AND COALESCE(PRESCHOOL_FLAG,'N/A') = 'N/A'
AND COALESCE(JUNIOR_FLAG,'N/A') = 'N/A'
AND COALESCE(TEEN_FLAG,'N/A') = 'N/A'
AND CLASS_NAME = 'BABY GOODS'
AND (PRODUCT_NAME LIKE '%BOTTLE%'
	OR PRODUCT_NAME LIKE '%INFANT%')
;

--ELSE BABY GOODS
UPDATE vpm_data.pm_skutagging_kids_product_master
SET INFANT_FLAG = 'YES'
	,UNKNOWN_INFANT_SUBSTAGE = 'YES'
	,TODDLER_FLAG = 'YES'
	,INFANT_UNKNOWN_KEYWORD = SUBCLASS_NAME
	,TODDLER_KEYWORD = SUBCLASS_NAME
WHERE MOM_AND_KID_PRODUCT_FLAG = 1
AND COALESCE(PREG_FLAG,'N/A') = 'N/A'
AND COALESCE(INFANT_FLAG,'N/A') = 'N/A'
AND COALESCE(TODDLER_FLAG,'N/A') = 'N/A'
AND COALESCE(PRESCHOOL_FLAG,'N/A') = 'N/A'
AND COALESCE(JUNIOR_FLAG,'N/A') = 'N/A'
AND COALESCE(TEEN_FLAG,'N/A') = 'N/A'
AND CLASS_NAME = 'BABY GOODS'
;

-----SUB DEPT BABY S FURNITURE
--0-3M
UPDATE vpm_data.pm_skutagging_kids_product_master
SET "STAGE_M+0_TO_M+3" = 'YES'
	,"STAGE_M+3_TO_M+6" = 'N/A'
	,"STAGE_M+6_TO_M+9" = 'N/A'
	,"STAGE_M+9_TO_M+12" = 'N/A'
	,INFANT_FLAG = 'YES'
	,UNKNOWN_INFANT_SUBSTAGE = 'N/A'
	,INFANT_0_3M_KEYWORD = 'BOUNCER'
WHERE MOM_AND_KID_PRODUCT_FLAG = 1
AND COALESCE(PREG_FLAG,'N/A') = 'N/A'
AND COALESCE(INFANT_FLAG,'N/A') = 'N/A'
AND COALESCE(TODDLER_FLAG,'N/A') = 'N/A'
AND COALESCE(PRESCHOOL_FLAG,'N/A') = 'N/A'
AND COALESCE(JUNIOR_FLAG,'N/A') = 'N/A'
AND COALESCE(TEEN_FLAG,'N/A') = 'N/A'
AND SUBDEPT_NAME = 'BABY S FURNITURES'
AND (PRODUCT_NAME LIKE '%�������¡%KICK N PLAY%'
	OR PRODUCT_NAME LIKE '%�Ź͹%BOUNCE%')
;

--3M+
UPDATE vpm_data.pm_skutagging_kids_product_master
SET "STAGE_M+0_TO_M+3" = 'N/A'
	,"STAGE_M+3_TO_M+6" = 'YES'
	,"STAGE_M+6_TO_M+9" = 'YES'
	,"STAGE_M+9_TO_M+12" = 'YES'
	,INFANT_FLAG = 'YES'
	,UNKNOWN_INFANT_SUBSTAGE = 'N/A'
	,TODDLER_FLAG = 'YES'
	,INFANT_4_6M_KEYWORD = '������Ԩ����'
	,INFANT_7_9M_KEYWORD = '������Ԩ����'
	,INFANT_10_12M_KEYWORD = '������Ԩ����'
	,TODDLER_KEYWORD = '������Ԩ����'
WHERE MOM_AND_KID_PRODUCT_FLAG = 1
AND COALESCE(PREG_FLAG,'N/A') = 'N/A'
AND COALESCE(INFANT_FLAG,'N/A') = 'N/A'
AND COALESCE(TODDLER_FLAG,'N/A') = 'N/A'
AND COALESCE(PRESCHOOL_FLAG,'N/A') = 'N/A'
AND COALESCE(JUNIOR_FLAG,'N/A') = 'N/A'
AND COALESCE(TEEN_FLAG,'N/A') = 'N/A'
AND SUBDEPT_NAME = 'BABY S FURNITURES'
AND (PRODUCT_NAME LIKE '%������Ԩ����%')
;

--6M+
UPDATE vpm_data.pm_skutagging_kids_product_master
SET "STAGE_M+0_TO_M+3" = 'N/A'
	,"STAGE_M+3_TO_M+6" = 'N/A'
	,"STAGE_M+6_TO_M+9" = 'YES'
	,"STAGE_M+9_TO_M+12" = 'YES'
	,INFANT_FLAG = 'YES'
	,UNKNOWN_INFANT_SUBSTAGE = 'N/A'
	,TODDLER_FLAG = 'YES'
	,INFANT_7_9M_KEYWORD = '������֡�ҹ'
	,INFANT_10_12M_KEYWORD = '������֡�ҹ'
	,TODDLER_KEYWORD = '������֡�ҹ'
WHERE MOM_AND_KID_PRODUCT_FLAG = 1
AND COALESCE(PREG_FLAG,'N/A') = 'N/A'
AND COALESCE(INFANT_FLAG,'N/A') = 'N/A'
AND COALESCE(TODDLER_FLAG,'N/A') = 'N/A'
AND COALESCE(PRESCHOOL_FLAG,'N/A') = 'N/A'
AND COALESCE(JUNIOR_FLAG,'N/A') = 'N/A'
AND COALESCE(TEEN_FLAG,'N/A') = 'N/A'
AND SUBDEPT_NAME = 'BABY S FURNITURES'
AND (PRODUCT_NAME LIKE '%�������Ѵ�ҹ%'
	OR PRODUCT_NAME LIKE '%������%BOOSTER%'
	OR PRODUCT_NAME LIKE '%������%RECIPE%'
	OR PRODUCT_NAME LIKE '%������%SNACKER%'
	OR PRODUCT_NAME LIKE '%PORTABLE%BOOSTER%'
	OR PRODUCT_NAME LIKE '%������%MULTIPLY%PETITE%CITY%'
	OR PRODUCT_NAME LIKE '%4 IN 1 BABY SYSTEM'
	OR PRODUCT_NAME LIKE '%STACK 3IN1 MULTI CHAIR%'
	OR PRODUCT_NAME LIKE '%������%MIMZY%3 IN1%'
	OR PRODUCT_NAME LIKE '%MIMZY%SANCKER%')
;

-----LVL2 BOOK AND SUB CLASS TOY
--10M+
UPDATE vpm_data.pm_skutagging_kids_product_master
SET "STAGE_M+0_TO_M+3" = 'N/A'
	,"STAGE_M+3_TO_M+6" = 'N/A'
	,"STAGE_M+6_TO_M+9" = 'N/A'
	,"STAGE_M+9_TO_M+12" = 'YES'
	,INFANT_FLAG = 'YES'
	,UNKNOWN_INFANT_SUBSTAGE = 'N/A'
	,TODDLER_FLAG = 'YES'
	,INFANT_10_12M_KEYWORD = '10M+'
	,TODDLER_KEYWORD = '10M+'
WHERE MOM_AND_KID_PRODUCT_FLAG = 1
AND COALESCE(PREG_FLAG,'N/A') = 'N/A'
AND COALESCE(INFANT_FLAG,'N/A') = 'N/A'
AND COALESCE(TODDLER_FLAG,'N/A') = 'N/A'
AND COALESCE(PRESCHOOL_FLAG,'N/A') = 'N/A'
AND COALESCE(JUNIOR_FLAG,'N/A') = 'N/A'
AND COALESCE(TEEN_FLAG,'N/A') = 'N/A'
AND REVISED_CATEGORY_LEVEL_2 = 'BOOK'
AND SUBCLASS_NAME = 'TOY'
AND (PRODUCT_NAME LIKE '%10M+%')
;

--12M+
UPDATE vpm_data.pm_skutagging_kids_product_master
SET INFANT_FLAG = 'N/A'
	,TODDLER_FLAG = 'YES'
	,TODDLER_KEYWORD = (CASE WHEN PRODUCT_NAME LIKE '%12M+%' THEN '12M+'
									WHEN PRODUCT_NAME LIKE '%18M+%' THEN '18M+'
									ELSE TODDLER_KEYWORD END)
WHERE MOM_AND_KID_PRODUCT_FLAG = 1
AND COALESCE(PREG_FLAG,'N/A') = 'N/A'
AND COALESCE(INFANT_FLAG,'N/A') = 'N/A'
AND COALESCE(TODDLER_FLAG,'N/A') = 'N/A'
AND COALESCE(PRESCHOOL_FLAG,'N/A') = 'N/A'
AND COALESCE(JUNIOR_FLAG,'N/A') = 'N/A'
AND COALESCE(TEEN_FLAG,'N/A') = 'N/A'
AND REVISED_CATEGORY_LEVEL_2 = 'BOOK'
AND SUBCLASS_NAME = 'TOY'
AND (PRODUCT_NAME LIKE '%12M+%'
	OR PRODUCT_NAME LIKE '%18M+%')
;

--2Y+
UPDATE vpm_data.pm_skutagging_kids_product_master
SET INFANT_FLAG = 'N/A'
	,TODDLER_FLAG = 'YES'
	,PRESCHOOL_FLAG = 'YES'
	,TODDLER_KEYWORD = (CASE WHEN PRODUCT_NAME LIKE '%2Y+%' THEN '2Y+'
							WHEN PRODUCT_NAME LIKE '%3Y+%' THEN '3Y+'
							ELSE TODDLER_KEYWORD END)
WHERE MOM_AND_KID_PRODUCT_FLAG = 1
AND COALESCE(PREG_FLAG,'N/A') = 'N/A'
AND COALESCE(INFANT_FLAG,'N/A') = 'N/A'
AND COALESCE(TODDLER_FLAG,'N/A') = 'N/A'
AND COALESCE(PRESCHOOL_FLAG,'N/A') = 'N/A'
AND COALESCE(JUNIOR_FLAG,'N/A') = 'N/A'
AND COALESCE(TEEN_FLAG,'N/A') = 'N/A'
AND REVISED_CATEGORY_LEVEL_2 = 'BOOK'
AND SUBCLASS_NAME = 'TOY'
AND (PRODUCT_NAME LIKE '%2Y+%'
	OR PRODUCT_NAME LIKE '%3Y+%')
;

UPDATE vpm_data.pm_skutagging_kids_product_master
SET INFANT_FLAG = 'N/A'
	,TODDLER_FLAG = 'YES'
	,TODDLER_KEYWORD = '����¡'
WHERE MOM_AND_KID_PRODUCT_FLAG = 1
AND COALESCE(PREG_FLAG,'N/A') = 'N/A'
AND COALESCE(INFANT_FLAG,'N/A') = 'N/A'
AND COALESCE(TODDLER_FLAG,'N/A') = 'N/A'
AND COALESCE(PRESCHOOL_FLAG,'N/A') = 'N/A'
AND COALESCE(JUNIOR_FLAG,'N/A') = 'N/A'
AND COALESCE(TEEN_FLAG,'N/A') = 'N/A'
AND PRODUCT_NAME LIKE '%����¡%'
AND CLEANED_BRANDNAME = 'MAMAS&PAPAS'
;



--TODDLER
--KEY WORD
UPDATE vpm_data.pm_skutagging_kids_product_master 
SET TODDLER_FLAG = 'YES'
WHERE MOM_AND_KID_PRODUCT_FLAG = 1
AND REVISED_CATEGORY_LEVEL_2 = 'KID FASHION'
AND COALESCE(PREG_FLAG,'N/A') = 'N/A'
AND COALESCE(INFANT_FLAG,'N/A') = 'N/A'
AND COALESCE(TODDLER_FLAG,'N/A') = 'N/A'
AND COALESCE(PRESCHOOL_FLAG,'N/A') = 'N/A'
AND COALESCE(JUNIOR_FLAG,'N/A') = 'N/A'
AND COALESCE(TEEN_FLAG,'N/A') = 'N/A'
AND (PRODUCT_NAME LIKE '%3Y%' 
   OR PRODUCT_NAME LIKE '%4Y%'
   OR PRODUCT_NAME LIKE '%24M%'  
   OR PRODUCT_NAME LIKE '%36M%'
   OR PRODUCT_NAME LIKE '%12-18%'
   OR PRODUCT_NAME LIKE '%2-3%'
   OR PRODUCT_NAME LIKE '%3-4%')  
;

UPDATE vpm_data.pm_skutagging_kids_product_master 
SET TODDLER_FLAG = 'N/A'
	,PRESCHOOL_FLAG = (CASE WHEN PRODUCT_NAME LIKE '%5Y%' THEN 'YES'
							WHEN PRODUCT_NAME LIKE '%6Y%' THEN 'YES'
							ELSE PRESCHOOL_FLAG END)
	,JUNIOR_FLAG = (CASE WHEN PRODUCT_NAME LIKE '%6-7Y%' THEN 'YES'
						    WHEN PRODUCT_NAME LIKE '%7-8Y%' THEN 'YES'
						    WHEN PRODUCT_NAME LIKE '%8Y%' THEN 'YES'
						    WHEN PRODUCT_NAME LIKE '%8-9Y%' THEN 'YES'
						    WHEN PRODUCT_NAME LIKE '%10Y%' THEN 'YES'
						    WHEN PRODUCT_NAME LIKE '%11-12Y%' THEN 'YES'
							ELSE JUNIOR_FLAG END)
WHERE MOM_AND_KID_PRODUCT_FLAG = 1
AND REVISED_CATEGORY_LEVEL_2 = 'KID FASHION'
AND COALESCE(PREG_FLAG,'N/A') = 'N/A'
AND COALESCE(INFANT_FLAG,'N/A') = 'N/A'
AND COALESCE(TODDLER_FLAG,'N/A') = 'N/A'
AND COALESCE(PRESCHOOL_FLAG,'N/A') = 'N/A'
AND COALESCE(JUNIOR_FLAG,'N/A') = 'N/A'
AND COALESCE(TEEN_FLAG,'N/A') = 'N/A'
AND (PRODUCT_NAME LIKE '%5Y%'
   OR PRODUCT_NAME LIKE '%6Y%' 
   OR PRODUCT_NAME LIKE '%6-7Y%'
   OR PRODUCT_NAME LIKE '%7-8Y%'
   OR PRODUCT_NAME LIKE '%8Y%'
   OR PRODUCT_NAME LIKE '%8-9Y%'
   OR PRODUCT_NAME LIKE '%10Y%'
   OR PRODUCT_NAME LIKE '%11-12Y%')
;

--ELSE TODDLER
UPDATE vpm_data.pm_skutagging_kids_product_master 
SET TODDLER_FLAG = 'YES'
WHERE MOM_AND_KID_PRODUCT_FLAG = 1
AND REVISED_CATEGORY_LEVEL_2 = 'KID FASHION'
AND COALESCE(PREG_FLAG,'N/A') = 'N/A'
AND COALESCE(INFANT_FLAG,'N/A') = 'N/A'
AND COALESCE(TODDLER_FLAG,'N/A') = 'N/A'
AND COALESCE(PRESCHOOL_FLAG,'N/A') = 'N/A'
AND COALESCE(JUNIOR_FLAG,'N/A') = 'N/A'
AND COALESCE(TEEN_FLAG,'N/A') = 'N/A'
AND (SUBDEPT_NAME LIKE 'BABY S WEAR'
     OR CLASS_NAME LIKE '%TODDLER%'
     OR SUBCLASS_NAME LIKE '%TODDLER%')
;

------------------------------- MATERNITY -------------------------------

UPDATE vpm_data.pm_skutagging_kids_product_master 
SET TODDLER_FLAG = 'YES'
WHERE MOM_AND_KID_PRODUCT_FLAG = 1
AND REVISED_CATEGORY_LEVEL_2 = 'MATERNITY'
AND COALESCE(PREG_FLAG,'N/A') = 'N/A'
AND COALESCE(INFANT_FLAG,'N/A') = 'N/A'
AND COALESCE(TODDLER_FLAG,'N/A') = 'N/A'
AND COALESCE(PRESCHOOL_FLAG,'N/A') = 'N/A'
AND COALESCE(JUNIOR_FLAG,'N/A') = 'N/A'
AND COALESCE(TEEN_FLAG,'N/A') = 'N/A'
AND (PRODUCT_NAME LIKE '%�ç�տѹ%' 
	OR PRODUCT_NAME LIKE '%TOOTHBRUSH%'
    OR PRODUCT_NAME LIKE '%0-3%YEARS%'
	OR PRODUCT_NAME LIKE '%0-5��%'
	OR PRODUCT_NAME LIKE '%18-36%MONTH%'
	OR PRODUCT_NAME LIKE '%���տѹ%')
;

UPDATE vpm_data.pm_skutagging_kids_product_master 
SET TODDLER_FLAG = 'YES'
WHERE MOM_AND_KID_PRODUCT_FLAG = 1
AND REVISED_CATEGORY_LEVEL_2 = 'MATERNITY'
AND COALESCE(PREG_FLAG,'N/A') = 'N/A'
AND COALESCE(INFANT_FLAG,'N/A') = 'N/A'
AND COALESCE(TODDLER_FLAG,'N/A') = 'N/A'
AND COALESCE(PRESCHOOL_FLAG,'N/A') = 'N/A'
AND COALESCE(JUNIOR_FLAG,'N/A') = 'N/A'
AND COALESCE(TEEN_FLAG,'N/A') = 'N/A'
AND (CLASS_NAME LIKE '%TODDLER%'
	 OR SUBCLASS_NAME LIKE '%TODDLER%')
	 --SUBDEPT_NAME LIKE '%TODDLER%' CHANGE TO 'BABY S WEAR', BUT WILL BE MIXED WITH INFANT
;



------------------------------- KID FURNITURE -------------------------------

--KEY WORD
UPDATE vpm_data.pm_skutagging_kids_product_master 
SET TODDLER_FLAG = 'YES'
WHERE MOM_AND_KID_PRODUCT_FLAG = 1
AND REVISED_CATEGORY_LEVEL_2 = 'KID FURNITURE'
AND COALESCE(PREG_FLAG,'N/A') = 'N/A'
AND COALESCE(INFANT_FLAG,'N/A') = 'N/A'
AND COALESCE(TODDLER_FLAG,'N/A') = 'N/A'
AND COALESCE(PRESCHOOL_FLAG,'N/A') = 'N/A'
AND COALESCE(JUNIOR_FLAG,'N/A') = 'N/A'
AND COALESCE(TEEN_FLAG,'N/A') = 'N/A'
AND (PRODUCT_NAME LIKE '%MODES TRAVEL SYSTEM DAVIS%'
   OR PRODUCT_NAME LIKE '%NEXTFIT%'
   OR PRODUCT_NAME LIKE '%CARSEAT%'  
   OR PRODUCT_NAME LIKE '%ö��%'
   OR PRODUCT_NAME LIKE '%STROLLER%'  
   OR PRODUCT_NAME LIKE '%CAR SEAT%'
   OR PRODUCT_NAME LIKE '%�Ҫշ%' 
   OR PRODUCT_NAME LIKE '%EVFö��%'
   OR PRODUCT_NAME LIKE '%MECHACAL%'
   OR PRODUCT_NAME LIKE '4EVER%'
   OR PRODUCT_NAME LIKE 'CITY MINI%'
   OR PRODUCT_NAME LIKE '%����շ%'
   OR PRODUCT_NAME LIKE '%BRAVO TRAVEL%'
   OR PRODUCT_NAME LIKE '%TRAVEL MUZE%'
   OR PRODUCT_NAME LIKE '%��еԴö%'
   OR PRODUCT_NAME LIKE '%�ҫշ%'
   OR PRODUCT_NAME LIKE '%PNP%NAPPER CHANGER%'
   OR PRODUCT_NAME LIKE '%������%'
   OR PRODUCT_NAME LIKE '%LULLABY%PLAYARD%'
   OR PRODUCT_NAME LIKE '%MT ELEGANT%'  
   OR PRODUCT_NAME LIKE '%LITE WAY BASIC WITH BUMPER%'  
   OR PRODUCT_NAME LIKE '%������%'  
   OR PRODUCT_NAME LIKE '%TRICYCLE%'  
   OR PRODUCT_NAME LIKE '%PLAYARD COMMUTER CHANGE%'  
   OR PRODUCT_NAME LIKE '%TODLR%'  
   OR PRODUCT_NAME LIKE '%WEGO LONG NAVY%'  
   OR PRODUCT_NAME LIKE '%MODES TS SNUGRIDE SNUGLOG%'  
   OR PRODUCT_NAME LIKE '%CORTINA CX TS WITH ISOFIX%'  
   OR PRODUCT_NAME LIKE '%BABYZEN YOYO FRAME%'  
   OR PRODUCT_NAME LIKE '%NEMULILA AUTO SWING%'  
   OR PRODUCT_NAME LIKE '%FEALETTO AUTO SWING%'  
   OR PRODUCT_NAME LIKE '%PACE TRAVEL SYSTEM%'  
   OR PRODUCT_NAME LIKE '%CORTINA TS W%'  
   OR PRODUCT_NAME LIKE '%COMFY CRUISER TRAVEL%'  
   OR PRODUCT_NAME LIKE '%MECHACAL HANDY%'  
   OR PRODUCT_NAME LIKE '%CITILITE-R%'  
   OR PRODUCT_NAME LIKE '%SIMPLICITY%PLUS%'  
   OR PRODUCT_NAME LIKE '%LATCH HARRIS%'  
   OR PRODUCT_NAME LIKE '%BUON JUNIOR%'  
   OR PRODUCT_NAME LIKE '%HYBRID CRADLE%'  
   OR PRODUCT_NAME LIKE '%SNACK BOOSTER SEAT%'  
   OR PRODUCT_NAME LIKE '%PACT LITE%'  
   OR PRODUCT_NAME LIKE '%COMMUTER CHANGE%'  
   OR PRODUCT_NAME LIKE '%NEMULTLA BEDIAT%'  
   OR PRODUCT_NAME LIKE '%MICKEY PLANE ACT RIDE ON%'  
   OR PRODUCT_NAME LIKE '%EMBIX C81%'  
   OR PRODUCT_NAME LIKE '%PACK NPLAY CUDDLE COVE%'  
   OR PRODUCT_NAME LIKE '%KID FIT%'  
   OR PRODUCT_NAME LIKE '%��ྐྵ%'
   OR PRODUCT_NAME LIKE '%BABY JOGGER%'  
   OR PRODUCT_NAME LIKE '%TODDLER%'  
   OR PRODUCT_NAME LIKE '%LITERIDER%'  
   OR PRODUCT_NAME LIKE '%CUP HOLDER%'  
   OR PRODUCT_NAME LIKE '%STORLLER%'  
   OR PRODUCT_NAME LIKE '%GRACO COMFY CRUISER%'  
   OR PRODUCT_NAME LIKE '%TRAVEL SYSTERM AIRE STEP%'  
   OR PRODUCT_NAME LIKE '%TRAVEL SYSTEM%'  
   OR PRODUCT_NAME LIKE '%AIRE STEP%'  
   OR PRODUCT_NAME LIKE '%RIDE ON%'  
   OR PRODUCT_NAME LIKE '%GB POCKIT%'  
   OR PRODUCT_NAME LIKE '%BRAVO LE 3IN1%'  
   OR PRODUCT_NAME LIKE '%ö�������%'  
   OR PRODUCT_NAME LIKE '%MEGA RIDE%'  
   OR PRODUCT_NAME LIKE '%BABYZEN YOYO%'  
   OR PRODUCT_NAME LIKE '%MEGERIDE DX%'  
   OR PRODUCT_NAME LIKE '%BRAVO PLUS%'  
   OR PRODUCT_NAME LIKE '%EEZY%'  
   OR PRODUCT_NAME LIKE '%GRACO MODES%'  
   OR PRODUCT_NAME LIKE '%BUGGY%'  
   OR PRODUCT_NAME LIKE '%TRAVE SYSTEM%'  
   OR CLEANED_BRANDNAME LIKE '%COMBI%'  
   OR CLEANED_BRANDNAME LIKE '%GRACO%'  
   OR PRODUCT_NAME LIKE '%LITEWAY BASIC%'  
   OR PRODUCT_NAME LIKE '%BABY JOG%'  
   OR (CLEANED_BRANDNAME LIKE '%CHICCO%' AND TYPE_FINAL NOT LIKE '%BABY%')
   OR PRODUCT_NAME LIKE '%TRIKE COMPLETE SET%'  
   OR PRODUCT_NAME LIKE '%DISNEY CARS LIGHT & SOUND%'  
   OR PRODUCT_NAME LIKE '%���%'  
   OR PRODUCT_NAME LIKE '%�á�Դ-12�Ǻ%'  
   OR PRODUCT_NAME LIKE '%GB QBIT PLUS%'  
   OR PRODUCT_NAME LIKE '%MARK II%'  
   OR PRODUCT_NAME LIKE '%QUEST MEDIEVAL%'  
   OR PRODUCT_NAME LIKE '%MEDIEVAL QUEST%'  
   OR PRODUCT_NAME LIKE '%�������շ%'  
   OR PRODUCT_NAME LIKE '%SCOOTER%'  
   OR PRODUCT_NAME LIKE '%HIGHCHAIR%'  
   OR PRODUCT_NAME LIKE '%������ҹ����%'  
   OR PRODUCT_NAME LIKE '%��͹����%'  
   OR PRODUCT_NAME LIKE '%PEG PEREGO%'  
   OR PRODUCT_NAME LIKE '%PRIM LUXE%'  
   OR PRODUCT_NAME LIKE '%PRIAM LUXE%'  
   OR PRODUCT_NAME LIKE '%BABYZEN%'  
   OR PRODUCT_NAME LIKE '%�ٴ����١%'  
   OR PRODUCT_NAME LIKE '%��٧%'  
   OR PRODUCT_NAME LIKE '%�ҧ����ҧ�Һ���%')
;

UPDATE vpm_data.pm_skutagging_kids_product_master 
SET TODDLER_FLAG = 'YES'
WHERE MOM_AND_KID_PRODUCT_FLAG = 1
AND REVISED_CATEGORY_LEVEL_2 = 'KID FURNITURE'
AND COALESCE(PREG_FLAG,'N/A') = 'N/A'
AND COALESCE(INFANT_FLAG,'N/A') = 'N/A'
AND COALESCE(TODDLER_FLAG,'N/A') = 'N/A'
AND COALESCE(PRESCHOOL_FLAG,'N/A') = 'N/A'
AND COALESCE(JUNIOR_FLAG,'N/A') = 'N/A'
AND COALESCE(TEEN_FLAG,'N/A') = 'N/A'
AND TYPE_FINAL = 'CAR SEAT'
AND  (PRODUCT_NAME NOT LIKE '%��%'
      OR PRODUCT_NAME NOT LIKE '%������%'
      OR PRODUCT_NAME NOT LIKE '%��С���%'
      OR PRODUCT_NAME NOT LIKE '%�á�Դ%'
      OR PRODUCT_NAME NOT LIKE '%0+1+2%')
;

--BU PRODUCT SCHEMA
UPDATE vpm_data.pm_skutagging_kids_product_master  
SET TODDLER_FLAG = 'YES'
WHERE MOM_AND_KID_PRODUCT_FLAG = 1
AND REVISED_CATEGORY_LEVEL_2 = 'KID FURNITURE'
AND COALESCE(PREG_FLAG,'N/A') = 'N/A'
AND COALESCE(INFANT_FLAG,'N/A') = 'N/A'
AND COALESCE(TODDLER_FLAG,'N/A') = 'N/A'
AND COALESCE(PRESCHOOL_FLAG,'N/A') = 'N/A'
AND COALESCE(JUNIOR_FLAG,'N/A') = 'N/A'
AND COALESCE(TEEN_FLAG,'N/A') = 'N/A'
AND (SUBDEPT_NAME LIKE 'BABY S WEAR'
	 OR CLASS_NAME LIKE '%TODDLER%'
     OR SUBCLASS_NAME LIKE '%TODDLER%')
;

--PM PRODUCT SCHEMA
UPDATE vpm_data.pm_skutagging_kids_product_master  
SET TODDLER_FLAG = 'YES'
WHERE MOM_AND_KID_PRODUCT_FLAG = 1
AND REVISED_CATEGORY_LEVEL_2 = 'KID FURNITURE'
AND COALESCE(PREG_FLAG,'N/A') = 'N/A'
AND COALESCE(INFANT_FLAG,'N/A') = 'N/A'
AND COALESCE(TODDLER_FLAG,'N/A') = 'N/A'
AND COALESCE(PRESCHOOL_FLAG,'N/A') = 'N/A'
AND COALESCE(JUNIOR_FLAG,'N/A') = 'N/A'
AND COALESCE(TEEN_FLAG,'N/A') = 'N/A'
AND TYPE_FINAL = 'STROLLER'
AND  (PRODUCT_NAME LIKE '%�ػ�ó�����ͧ��%'
      OR PRODUCT_NAME LIKE '%ͻءó�����ͧ��%'
      OR PRODUCT_NAME LIKE '%�ش��%')
;




------------------------------- KID ACCESSORIES -------------------------------

--KEY WORD
UPDATE vpm_data.pm_skutagging_kids_product_master 
SET TODDLER_FLAG = 'YES'
WHERE MOM_AND_KID_PRODUCT_FLAG = 1
AND REVISED_CATEGORY_LEVEL_2 = 'KID ACCESSORIES'
AND COALESCE(PREG_FLAG,'N/A') = 'N/A'
AND COALESCE(INFANT_FLAG,'N/A') = 'N/A'
AND COALESCE(TODDLER_FLAG,'N/A') = 'N/A'
AND COALESCE(PRESCHOOL_FLAG,'N/A') = 'N/A'
AND COALESCE(JUNIOR_FLAG,'N/A') = 'N/A'
AND COALESCE(TEEN_FLAG,'N/A') = 'N/A'
AND TYPE_FINAL = 'THERMOMETER'
AND PRODUCT_NAME LIKE '%THERMOMETER%'
;

--GIFT SET -> ALL SUBSTAGE AND ONLY INFANT
UPDATE vpm_data.pm_skutagging_kids_product_master 
SET "STAGE_M+0_TO_M+3" = 'YES'
	,"STAGE_M+3_TO_M+6" = 'YES'
	,"STAGE_M+6_TO_M+9" = 'YES'
	,"STAGE_M+9_TO_M+12" = 'YES'
	,INFANT_FLAG = 'YES'
WHERE MOM_AND_KID_PRODUCT_FLAG = 1
AND REVISED_CATEGORY_LEVEL_2 = 'KID ACCESSORIES'
AND COALESCE(PREG_FLAG,'N/A') = 'N/A'
AND COALESCE(INFANT_FLAG,'N/A') = 'N/A'
AND COALESCE(TODDLER_FLAG,'N/A') = 'N/A'
AND COALESCE(PRESCHOOL_FLAG,'N/A') = 'N/A'
AND COALESCE(JUNIOR_FLAG,'N/A') = 'N/A'
AND COALESCE(TEEN_FLAG,'N/A') = 'N/A'
AND TYPE_FINAL = 'GIFT SET'
AND PRODUCT_NAME NOT LIKE '%8-9Y%'
AND PRODUCT_NAME NOT LIKE '%5-6Y%'
AND PRODUCT_NAME NOT LIKE '%4-5Y%'
AND PRODUCT_NAME NOT LIKE '%1-2Y%'
;

UPDATE vpm_data.pm_skutagging_kids_product_master 
SET TODDLER_FLAG = 'YES'
WHERE MOM_AND_KID_PRODUCT_FLAG = 1
AND REVISED_CATEGORY_LEVEL_2 = 'KID ACCESSORIES'
AND COALESCE(PREG_FLAG,'N/A') = 'N/A'
AND COALESCE(INFANT_FLAG,'N/A') = 'N/A'
AND COALESCE(TODDLER_FLAG,'N/A') = 'N/A'
AND COALESCE(PRESCHOOL_FLAG,'N/A') = 'N/A'
AND COALESCE(JUNIOR_FLAG,'N/A') = 'N/A'
AND COALESCE(TEEN_FLAG,'N/A') = 'N/A'
AND TYPE_FINAL = 'KID'
AND PRODUCT_NAME NOT LIKE '%3M%'
AND PRODUCT_NAME NOT LIKE '%6M%'
AND PRODUCT_NAME NOT LIKE '%12M%'
AND PRODUCT_NAME NOT LIKE '%18M%'
AND PRODUCT_NAME NOT LIKE '%6Y%'
AND PRODUCT_NAME NOT LIKE '%8Y%'
AND PRODUCT_NAME NOT LIKE '%10Y%'
;

UPDATE vpm_data.pm_skutagging_kids_product_master 
SET TODDLER_FLAG = 'YES'
WHERE MOM_AND_KID_PRODUCT_FLAG = 1
AND REVISED_CATEGORY_LEVEL_2 = 'KID ACCESSORIES'
AND COALESCE(PREG_FLAG,'N/A') = 'N/A'
AND COALESCE(INFANT_FLAG,'N/A') = 'N/A'
AND COALESCE(TODDLER_FLAG,'N/A') = 'N/A'
AND COALESCE(PRESCHOOL_FLAG,'N/A') = 'N/A'
AND COALESCE(JUNIOR_FLAG,'N/A') = 'N/A'
AND COALESCE(TEEN_FLAG,'N/A') = 'N/A'
AND TYPE_FINAL = 'WEANING'
AND PRODUCT_NAME NOT LIKE '%��͹%'
AND PRODUCT_NAME NOT LIKE '%��%'
AND PRODUCT_NAME NOT LIKE '%STEP2%'
AND PRODUCT_NAME NOT LIKE '%STEP 2%'
AND PRODUCT_NAME NOT LIKE '%�֡%'
AND PRODUCT_NAME NOT LIKE '%��á%'
AND PRODUCT_NAME NOT LIKE '%�Ѵ%'
AND PRODUCT_NAME NOT LIKE '%FEEDER%'
AND PRODUCT_NAME NOT LIKE '%TEETHER%'
AND PRODUCT_NAME NOT LIKE '%STEP1%'
AND PRODUCT_NAME NOT LIKE '%STEP 1%'
AND PRODUCT_NAME NOT LIKE '%�ҧ�Ѵ%'
AND PRODUCT_NAME NOT LIKE '%3M%'
AND PRODUCT_NAME NOT LIKE '%4M%'
AND PRODUCT_NAME NOT LIKE '%6M%'
AND PRODUCT_NAME NOT LIKE '%7M%'
AND PRODUCT_NAME NOT LIKE '%12M%'
AND PRODUCT_NAME NOT LIKE '%18M%'
;

UPDATE vpm_data.pm_skutagging_kids_product_master 
SET TODDLER_FLAG = 'YES'
WHERE MOM_AND_KID_PRODUCT_FLAG = 1
AND REVISED_CATEGORY_LEVEL_2 = 'KID ACCESSORIES'
AND COALESCE(PREG_FLAG,'N/A') = 'N/A'
AND COALESCE(INFANT_FLAG,'N/A') = 'N/A'
AND COALESCE(TODDLER_FLAG,'N/A') = 'N/A'
AND COALESCE(PRESCHOOL_FLAG,'N/A') = 'N/A'
AND COALESCE(JUNIOR_FLAG,'N/A') = 'N/A'
AND COALESCE(TEEN_FLAG,'N/A') = 'N/A'
AND TYPE_FINAL = 'OTHER'
AND PRODUCT_NAME NOT LIKE '%����͹%'
AND PRODUCT_NAME NOT LIKE '%�����%'
AND PRODUCT_NAME NOT LIKE '%�������%'
AND PRODUCT_NAME NOT LIKE '%�Ǵ��%'
AND PRODUCT_NAME NOT LIKE '%FEED%'
AND PRODUCT_NAME NOT LIKE '%BOTTLE%'
AND PRODUCT_NAME NOT LIKE '%POCKET WARMER%'
AND CLEANED_BRANDNAME NOT LIKE '%ORGANEH%'
AND CLEANED_BRANDNAME NOT LIKE '%MALADA%'
AND CLEANED_BRANDNAME NOT LIKE '%MOTHERLY LOVE%'
AND CLEANED_BRANDNAME NOT LIKE '%CANTALOOP%'
;

UPDATE vpm_data.pm_skutagging_kids_product_master 
SET TODDLER_FLAG = 'YES'
WHERE MOM_AND_KID_PRODUCT_FLAG = 1
AND REVISED_CATEGORY_LEVEL_2 = 'KID ACCESSORIES'
AND COALESCE(PREG_FLAG,'N/A') = 'N/A'
AND COALESCE(INFANT_FLAG,'N/A') = 'N/A'
AND COALESCE(TODDLER_FLAG,'N/A') = 'N/A'
AND COALESCE(PRESCHOOL_FLAG,'N/A') = 'N/A'
AND COALESCE(JUNIOR_FLAG,'N/A') = 'N/A'
AND COALESCE(TEEN_FLAG,'N/A') = 'N/A'
AND TYPE_FINAL = 'BATHING'
AND PRODUCT_NAME NOT LIKE '%����͹%'
AND PRODUCT_NAME NOT LIKE '%���1%'
AND PRODUCT_NAME NOT LIKE '%��á%'
AND PRODUCT_NAME NOT LIKE '%3��͹%'
AND PRODUCT_NAME NOT LIKE '%�Ǵ�˧�͡%'
AND PRODUCT_NAME NOT LIKE '%6-9%'
AND PRODUCT_NAME NOT LIKE '%6-8%'
AND PRODUCT_NAME NOT LIKE '%5-7%'
AND PRODUCT_NAME NOT LIKE '%�ҧ�Ѵ%'
AND PRODUCT_NAME NOT LIKE '%6-12%'
AND PRODUCT_NAME NOT LIKE '%�Ǵ��%'
AND PRODUCT_NAME NOT LIKE '%8-10%'
AND PRODUCT_NAME NOT LIKE '%����˭�%'
AND PRODUCT_NAME NOT LIKE '%15M%'
AND PRODUCT_NAME NOT LIKE '%12 M%'
AND PRODUCT_NAME NOT LIKE '%6 M%'
AND PRODUCT_NAME NOT LIKE '%�á�Դ%'
AND PRODUCT_NAME NOT LIKE '%�ء��%'
AND PRODUCT_NAME NOT LIKE '%��͵����%'
AND PRODUCT_NAME NOT LIKE '%INFANT%'
AND PRODUCT_NAME NOT LIKE '%ᵡ���%'
;

UPDATE vpm_data.pm_skutagging_kids_product_master 
SET TODDLER_FLAG = 'YES'
	,REVISED_CATEGORY_LEVEL_1 = 'KIDS'
	,REVISED_CATEGORY_LEVEL_2 = 'KID ACCESSORIES'
	,REVISED_CATEGORY_LEVEL_3 = 'KID BATHROOM ACCESSORIES'
WHERE MOM_AND_KID_PRODUCT_FLAG = 1
AND REVISED_CATEGORY_LEVEL_2 = 'KID ACCESSORIES'
AND COALESCE(PREG_FLAG,'N/A') = 'N/A'
AND COALESCE(INFANT_FLAG,'N/A') = 'N/A'
AND COALESCE(TODDLER_FLAG,'N/A') = 'N/A'
AND COALESCE(PRESCHOOL_FLAG,'N/A') = 'N/A'
AND COALESCE(JUNIOR_FLAG,'N/A') = 'N/A'
AND COALESCE(TEEN_FLAG,'N/A') = 'N/A'
AND (PRODUCT_NAME LIKE '%�ç�տѹ%' 
	OR PRODUCT_NAME LIKE '%TOOTHBRUSH%'
	OR PRODUCT_NAME LIKE '%0-3%YEARS%'
	OR PRODUCT_NAME LIKE '%0-5��%'
	OR PRODUCT_NAME LIKE '%18-36%MONTH%'
	OR PRODUCT_NAME LIKE '%���տѹ%')
;

--BU PRODUCT SCHEMA
UPDATE vpm_data.pm_skutagging_kids_product_master 
SET TODDLER_FLAG = 'YES'
WHERE MOM_AND_KID_PRODUCT_FLAG = 1
AND REVISED_CATEGORY_LEVEL_2 = 'KID ACCESSORIES'
AND COALESCE(PREG_FLAG,'N/A') = 'N/A'
AND COALESCE(INFANT_FLAG,'N/A') = 'N/A'
AND COALESCE(TODDLER_FLAG,'N/A') = 'N/A'
AND COALESCE(PRESCHOOL_FLAG,'N/A') = 'N/A'
AND COALESCE(JUNIOR_FLAG,'N/A') = 'N/A'
AND COALESCE(TEEN_FLAG,'N/A') = 'N/A'
AND (SUBDEPT_NAME LIKE 'BABY S WEAR'
     OR CLASS_NAME LIKE '%TODDLER%'
     OR SUBCLASS_NAME LIKE '%TODDLER%')
;

--PM PRODUCT SCHEMA
UPDATE vpm_data.pm_skutagging_kids_product_master 
SET TODDLER_FLAG = 'YES'
WHERE MOM_AND_KID_PRODUCT_FLAG = 1
AND REVISED_CATEGORY_LEVEL_2 = 'KID ACCESSORIES'
AND COALESCE(PREG_FLAG,'N/A') = 'N/A'
AND COALESCE(INFANT_FLAG,'N/A') = 'N/A'
AND COALESCE(TODDLER_FLAG,'N/A') = 'N/A'
AND COALESCE(PRESCHOOL_FLAG,'N/A') = 'N/A'
AND COALESCE(JUNIOR_FLAG,'N/A') = 'N/A'
AND COALESCE(TEEN_FLAG,'N/A') = 'N/A'
AND (TYPE_FINAL = 'PERSONAL CARE'
	OR TYPE_FINAL = 'CAR SEAT'
	OR TYPE_FINAL = 'STROLLER'
	OR TYPE_FINAL = 'FABRIC'
	OR TYPE_FINAL = 'TOY'
	OR TYPE_FINAL = 'ACCESSORIES'
	OR (TYPE_FINAL = 'MILK POWDER' AND PRODUCT_NAME LIKE '%�ٵ�2%')
	OR (TYPE_FINAL = 'BEDDING' AND PRODUCT_NAME NOT LIKE '%����%' AND PRODUCT_NAME NOT LIKE '%��ͧ%')
	OR (TYPE_FINAL = 'SAFETY' AND PRODUCT_NAME LIKE '%������ҹ����%')
	OR (TYPE_FINAL = 'BABY BOUNCER' AND PRODUCT_NAME LIKE '%��ྐྵ%')
	)
;

------------------------------- DIAPER -------------------------------

--KEY WORD
UPDATE vpm_data.pm_skutagging_kids_product_master 
SET TODDLER_FLAG = 'YES'
WHERE MOM_AND_KID_PRODUCT_FLAG = 1
AND REVISED_CATEGORY_LEVEL_2 = 'DIAPERS'
AND COALESCE(PREG_FLAG,'N/A') = 'N/A'
AND COALESCE(INFANT_FLAG,'N/A') = 'N/A'
AND COALESCE(TODDLER_FLAG,'N/A') = 'N/A'
AND COALESCE(PRESCHOOL_FLAG,'N/A') = 'N/A'
AND COALESCE(JUNIOR_FLAG,'N/A') = 'N/A'
AND COALESCE(TEEN_FLAG,'N/A') = 'N/A'
AND PRODUCT_NAME LIKE '%18M%'
;

------------------------------- KID FOOD -------------------------------

--KEY WORD
UPDATE vpm_data.pm_skutagging_kids_product_master  
SET TODDLER_FLAG = 'YES'
WHERE MOM_AND_KID_PRODUCT_FLAG = 1
AND REVISED_CATEGORY_LEVEL_2 = 'KID FOOD'
AND COALESCE(PREG_FLAG,'N/A') = 'N/A'
AND COALESCE(INFANT_FLAG,'N/A') = 'N/A'
AND COALESCE(TODDLER_FLAG,'N/A') = 'N/A'
AND COALESCE(PRESCHOOL_FLAG,'N/A') = 'N/A'
AND COALESCE(JUNIOR_FLAG,'N/A') = 'N/A'
AND COALESCE(TEEN_FLAG,'N/A') = 'N/A'
AND (PRODUCT_NAME LIKE '%ENFALAC 3%'
	OR PRODUCT_NAME LIKE '%SIMILAC 3%'
	OR PRODUCT_NAME LIKE '%SIMILAC COMFORT3%'
	OR PRODUCT_NAME LIKE '%DG3%'
	OR PRODUCT_NAME LIKE '%1 PLUS%'
	OR PRODUCT_NAME LIKE '%1PLUS%'
	OR PRODUCT_NAME LIKE '%DUMILK 3%'
	OR PRODUCT_NAME LIKE '%DUMILK3%'
	OR PRODUCT_NAME LIKE '%CARNATION%1%PLUS%'
	OR PRODUCT_NAME LIKE '%ENFAGROW%A%PLUS4%'
	OR PRODUCT_NAME LIKE '%ENFAGROW%3%'
	OR PRODUCT_NAME LIKE '%DUMEX%3%'
	OR PRODUCT_NAME LIKE '%S26%PROGRES%'
	OR PRODUCT_NAME LIKE '%S26%PE%'
	OR PRODUCT_NAME LIKE '%HI%Q%1%PLUS%'
	OR PRODUCT_NAME LIKE '%BEAR%BRAND%3%'
	OR PRODUCT_NAME LIKE '%NANKID%3%'
	OR PRODUCT_NAME LIKE '%NANKID%1PLUS%'
	OR PRODUCT_NAME LIKE '%DUGRO%3%'
	OR PRODUCT_NAME LIKE '%NUTRICIA%MILNUTRISURE%'
	OR PRODUCT_NAME LIKE '%ISOMIL PLUS AI Q PLUS%'
	OR PRODUCT_NAME LIKE '%DUTCHMILL%3%'
	OR PRODUCT_NAME LIKE '%LACTOGEN3%'
	OR PRODUCT_NAME LIKE '%DUMILK4%')
;



------------------------------- TOYS -------------------------------


--KEY WORD
UPDATE vpm_data.pm_skutagging_kids_product_master 
SET TODDLER_FLAG = 'YES'
WHERE MOM_AND_KID_PRODUCT_FLAG = 1
AND REVISED_CATEGORY_LEVEL_2 = 'KID FOOD' --TOYS
AND COALESCE(PREG_FLAG,'N/A') = 'N/A'
AND COALESCE(INFANT_FLAG,'N/A') = 'N/A'
AND COALESCE(TODDLER_FLAG,'N/A') = 'N/A'
AND COALESCE(PRESCHOOL_FLAG,'N/A') = 'N/A'
AND COALESCE(JUNIOR_FLAG,'N/A') = 'N/A'
AND COALESCE(TEEN_FLAG,'N/A') = 'N/A'
AND PRODUCT_NAME NOT LIKE '%�ҧ�Ѵ%'
AND PRODUCT_NAME NOT LIKE '%�Ѵ�Թ%'
AND PRODUCT_NAME NOT LIKE '%�ç�տѹ����⤹%'
AND PRODUCT_NAME NOT LIKE '%�Ѵ%'
AND PRODUCT_NAME NOT LIKE '%�Ǵ�˧�͡%'
AND PRODUCT_NAME NOT LIKE '%WALK%'
AND PRODUCT_NAME NOT LIKE '%TEETHER%'
AND PRODUCT_NAME NOT LIKE '%STROLLER%'
AND PRODUCT_NAME NOT LIKE '%����%'
AND PRODUCT_NAME NOT LIKE '%�����%'
AND PRODUCT_NAME NOT LIKE '%�ء��͡%'
AND PRODUCT_NAME NOT LIKE '%�Ǵ��%'
AND PRODUCT_NAME NOT LIKE '%BOUNCER%'
AND PRODUCT_NAME NOT LIKE '%BABY CARRIER%'
AND PRODUCT_NAME NOT LIKE '%TEETHING%'
AND PRODUCT_NAME NOT LIKE '%RATTLE%'
AND PRODUCT_NAME NOT LIKE '%BABY BOOSTER%'
AND PRODUCT_NAME NOT LIKE '%��ͤ����%'
AND PRODUCT_NAME NOT LIKE '%ROCKER%'
AND PRODUCT_NAME NOT LIKE '%��������%'
AND PRODUCT_NAME NOT LIKE '%��%'
AND PRODUCT_NAME NOT LIKE '%�ѹ%'
AND PRODUCT_NAME NOT LIKE '%STACKING%FUN%CUPS'
AND PRODUCT_NAME NOT LIKE '%SHAKE%'
AND PRODUCT_NAME NOT LIKE '%��ҹ%'
AND PRODUCT_NAME NOT LIKE '%FILING DISC%'
AND CLEANED_BRANDNAME NOT LIKE 'BDUCK'
AND CLEANED_BRANDNAME NOT LIKE 'BLACK SHEEP'
AND CLEANED_BRANDNAME NOT LIKE 'BOOMERANG'
AND PRODUCT_NAME NOT LIKE '%CARRIER%'
AND PRODUCT_NAME NOT LIKE '%BABY CARRY%'
AND PRODUCT_NAME NOT LIKE '%BABY HOLDER%'
AND PRODUCT_NAME NOT LIKE '%INFANT%'
AND PRODUCT_NAME NOT LIKE '%STACK%'
AND CLEANED_BRANDNAME NOT LIKE 'HOLLYWOOD'
AND PRODUCT_NAME NOT LIKE '%����͹%' 
AND PRODUCT_NAME NOT LIKE '%SWING%'
AND CLEANED_BRANDNAME NOT LIKE 'KING SPORT'
AND CLEANED_BRANDNAME NOT LIKE 'MTR'
AND CLEANED_BRANDNAME NOT LIKE 'TRIAMORN'
AND CLEANED_BRANDNAME NOT LIKE 'VINTAGE'
;

------------------------------- BOOK -------------------------------

--KEYWORD
UPDATE vpm_data.pm_skutagging_kids_product_master 
SET TODDLER_FLAG = 'YES'
WHERE MOM_AND_KID_PRODUCT_FLAG = 1
AND REVISED_CATEGORY_LEVEL_2 = 'BOOK'
AND COALESCE(PREG_FLAG,'N/A') = 'N/A'
AND COALESCE(INFANT_FLAG,'N/A') = 'N/A'
AND COALESCE(TODDLER_FLAG,'N/A') = 'N/A'
AND COALESCE(PRESCHOOL_FLAG,'N/A') = 'N/A'
AND COALESCE(JUNIOR_FLAG,'N/A') = 'N/A'
AND COALESCE(TEEN_FLAG,'N/A') = 'N/A'
AND PRODUCT_NAME NOT LIKE '%��á%'
AND PRODUCT_NAME NOT LIKE '%�á�Դ%'
AND PRODUCT_NAME NOT LIKE '%�Ѵ�Թ%'
AND PRODUCT_NAME NOT LIKE '%��ͧ%'
AND PRODUCT_NAME NOT LIKE '%�����%'
AND PRODUCT_NAME NOT LIKE '%�Թ�յ�͹�Ѻ����š%'
AND PRODUCT_NAME NOT LIKE '%����١��͹%'
AND PRODUCT_NAME NOT LIKE '%�����á%'
AND subclass_name NOT LIKE 'PREGNANCY'
;


------------------------------- KID FASHION -------------------------------

--KEY WORD
UPDATE vpm_data.pm_skutagging_kids_product_master
SET TODDLER_KEYWORD = CASE WHEN PRODUCT_NAME LIKE '%3Y%' THEN '3Y'
                           WHEN PRODUCT_NAME LIKE '%4Y%' THEN '4Y'
                           WHEN PRODUCT_NAME LIKE '%24M%' THEN '24M'
                           WHEN PRODUCT_NAME LIKE '%36M%' THEN '36M'
                           WHEN PRODUCT_NAME LIKE '%12-18%' THEN '12-18M'
                           WHEN PRODUCT_NAME LIKE '%2-3%' THEN '2-3Y'
                           WHEN PRODUCT_NAME LIKE '%3-4%' THEN '3-4Y'
						   ELSE TODDLER_KEYWORD
						   END
WHERE MOM_AND_KID_PRODUCT_FLAG = 1
AND REVISED_CATEGORY_LEVEL_2 = 'KID FASHION'
AND TODDLER_KEYWORD IS NULL
AND TODDLER_FLAG = 'YES'
;

--BU PRODUCT SCHEMA
UPDATE vpm_data.pm_skutagging_kids_product_master 
SET TODDLER_KEYWORD = 'TODDLER FASHION'
WHERE MOM_AND_KID_PRODUCT_FLAG = 1
AND REVISED_CATEGORY_LEVEL_2 = 'KID FASHION'
AND TODDLER_KEYWORD IS NULL
AND (SUBDEPT_NAME LIKE 'BABY S WEAR'
     OR CLASS_NAME LIKE '%TODDLER%'
     OR SUBCLASS_NAME LIKE '%TODDLER%')
AND TODDLER_FLAG = 'YES'
;

--FINAL
UPDATE vpm_data.pm_skutagging_kids_product_master
SET TODDLER_KEYWORD = 'TODDLER FASHION'
WHERE MOM_AND_KID_PRODUCT_FLAG = 1
AND REVISED_CATEGORY_LEVEL_2 = 'KID FASHION'
AND TODDLER_KEYWORD IS NULL
AND TODDLER_FLAG = 'YES'
;

------------------------------- MATERNITY -------------------------------

--BU PRODUCT SCHEMA
UPDATE vpm_data.pm_skutagging_kids_product_master 
SET TODDLER_KEYWORD = 'TODDLER MATERNITY'
WHERE MOM_AND_KID_PRODUCT_FLAG = 1
AND REVISED_CATEGORY_LEVEL_2 = 'MATERNITY'
AND TODDLER_KEYWORD IS NULL
AND (SUBDEPT_NAME LIKE '%TODDLER%'
	 OR CLASS_NAME LIKE '%TODDLER%'
	 OR SUBCLASS_NAME LIKE '%TODDLER%')
AND TODDLER_FLAG = 'YES'
;

UPDATE vpm_data.pm_skutagging_kids_product_master 
SET TODDLER_KEYWORD = 'TOOTHBRUSH'
WHERE MOM_AND_KID_PRODUCT_FLAG = 1
AND REVISED_CATEGORY_LEVEL_2 = 'MATERNITY'
AND TODDLER_KEYWORD IS NULL
AND (PRODUCT_NAME LIKE '%�ç�տѹ%' 
	OR PRODUCT_NAME LIKE '%TOOTHBRUSH%'
    OR PRODUCT_NAME LIKE '%0-3%YEARS%'
	OR PRODUCT_NAME LIKE '%0-5��%'
	OR PRODUCT_NAME LIKE '%18-36%MONTH%'
	OR PRODUCT_NAME LIKE '%���տѹ%')
AND TODDLER_FLAG = 'YES'
;

------------------------------- KID FURNITURE -------------------------------

--KEY WORD
UPDATE vpm_data.pm_skutagging_kids_product_master 
SET TODDLER_KEYWORD = 'STROLLER'
WHERE MOM_AND_KID_PRODUCT_FLAG = 1
AND REVISED_CATEGORY_LEVEL_2 = 'KID FURNITURE'
AND TODDLER_KEYWORD IS NULL
AND (PRODUCT_NAME LIKE '%MODES TRAVEL SYSTEM DAVIS%'
   OR PRODUCT_NAME LIKE '%ö��%'
   OR PRODUCT_NAME LIKE '%STROLLER%' 
   OR PRODUCT_NAME LIKE '%EVFö��%'
   OR PRODUCT_NAME LIKE '%MECHACAL%'
   OR PRODUCT_NAME LIKE 'CITY MINI%'
   OR PRODUCT_NAME LIKE '%BRAVO TRAVEL%'
   OR PRODUCT_NAME LIKE '%MODES TS SNUGRIDE SNUGLOG%'  
   OR PRODUCT_NAME LIKE '%CORTINA CX TS WITH ISOFIX%'  
   OR PRODUCT_NAME LIKE '%CORTINA TS W%'  
   OR PRODUCT_NAME LIKE '%LITERIDER%'  
   OR PRODUCT_NAME LIKE '%BABYZEN YOYO FRAME%'  
   OR PRODUCT_NAME LIKE '%CUP HOLDER%'  
   OR PRODUCT_NAME LIKE '%BABY JOGGER%'  
   OR PRODUCT_NAME LIKE '%ö�������%'  
   OR PRODUCT_NAME LIKE '%PACE TRAVEL SYSTEM%'  
   OR PRODUCT_NAME LIKE '%BRAVO PLUS%'  
   OR PRODUCT_NAME LIKE '%TRAVEL MUZE%'
   OR PRODUCT_NAME LIKE '%COMFY CRUISER TRAVEL%'  
   OR PRODUCT_NAME LIKE '%SIMPLICITY%PLUS%'  
   OR PRODUCT_NAME LIKE '%STORLLER%'  
   OR PRODUCT_NAME LIKE '%BABYZEN YOYO%'  
   OR PRODUCT_NAME LIKE '%BABY JOG%'  
   OR PRODUCT_NAME LIKE '%MECHACAL HANDY%'  
   OR PRODUCT_NAME LIKE '%CITILITE-R%'  
   OR PRODUCT_NAME LIKE '%GRACO MODES%'  
   OR PRODUCT_NAME LIKE '%TRAVEL SYSTERM AIRE STEP%'  
   OR PRODUCT_NAME LIKE '%TRAVEL SYSTEM%'  
   OR PRODUCT_NAME LIKE '%TRAVE SYSTEM%'  
   OR PRODUCT_NAME LIKE '%BRAVO LE 3IN1%'  
   OR PRODUCT_NAME LIKE '%AIRE STEP%'  
   OR PRODUCT_NAME LIKE '%QUEST MEDIEVAL%'  
   OR PRODUCT_NAME LIKE '%LITEWAY BASIC%'  
   OR PRODUCT_NAME LIKE '%GB POCKIT%'  
   OR PRODUCT_NAME LIKE '%GB QBIT PLUS%'  
   OR PRODUCT_NAME LIKE '%BABYZEN%'  
   OR PRODUCT_NAME LIKE '%PEG PEREGO%'  
   OR PRODUCT_NAME LIKE '%PACT LITE%'  
   OR PRODUCT_NAME LIKE '%PRIM LUXE%'  
   OR PRODUCT_NAME LIKE '%PRIAM LUXE%'  
   OR PRODUCT_NAME LIKE '%MT ELEGANT%'  
   OR PRODUCT_NAME LIKE '%EEZY%'  
   OR PRODUCT_NAME LIKE '%MARK II%'  
   OR PRODUCT_NAME LIKE '%MEGA RIDE%'  
   OR PRODUCT_NAME LIKE '%BUGGY%'  
   OR PRODUCT_NAME LIKE '%MEGERIDE DX%')  
AND TODDLER_FLAG = 'YES'
;

UPDATE vpm_data.pm_skutagging_kids_product_master 
SET TODDLER_KEYWORD = 'CAR SEAT'
WHERE MOM_AND_KID_PRODUCT_FLAG = 1
AND REVISED_CATEGORY_LEVEL_2 = 'KID FURNITURE'
AND TODDLER_KEYWORD IS NULL
AND (PRODUCT_NAME LIKE '%NEXTFIT%'
   OR PRODUCT_NAME LIKE '%CARSEAT%'  
   OR PRODUCT_NAME LIKE '%CAR SEAT%'
   OR PRODUCT_NAME LIKE '%�Ҫշ%' 
   OR PRODUCT_NAME LIKE '%����շ%'
   OR PRODUCT_NAME LIKE '%��еԴö%'
   OR PRODUCT_NAME LIKE '%�ҫշ%'
   OR PRODUCT_NAME LIKE '%������%'
   OR PRODUCT_NAME LIKE '%GRACO COMFY CRUISER%'  
   OR PRODUCT_NAME LIKE '%WEGO LONG NAVY%'  
   OR PRODUCT_NAME LIKE '%BUON JUNIOR%'  
   OR PRODUCT_NAME LIKE '%�������շ%'  
   OR PRODUCT_NAME LIKE '%MEDIEVAL QUEST%'  
   OR PRODUCT_NAME LIKE '%LATCH HARRIS%'  
   OR PRODUCT_NAME LIKE '%SNACK BOOSTER SEAT%'  
   OR PRODUCT_NAME LIKE '%NEMULTLA BEDIAT%'  
   OR PRODUCT_NAME LIKE '%EMBIX C81%'  
   OR PRODUCT_NAME LIKE '4EVER%'
   OR PRODUCT_NAME LIKE '%KID FIT%') 
AND TODDLER_FLAG = 'YES'
;

UPDATE vpm_data.pm_skutagging_kids_product_master 
SET TODDLER_KEYWORD = 'TRICYCLE'
WHERE MOM_AND_KID_PRODUCT_FLAG = 1
AND REVISED_CATEGORY_LEVEL_2 = 'KID FURNITURE'
AND TODDLER_KEYWORD IS NULL
AND (PRODUCT_NAME LIKE '%������%'
   OR PRODUCT_NAME LIKE '%TRICYCLE%'  
   OR PRODUCT_NAME LIKE '%���%'  
   OR PRODUCT_NAME LIKE '%MICKEY PLANE ACT RIDE ON%'  
   OR PRODUCT_NAME LIKE '%SCOOTER%'  
   OR PRODUCT_NAME LIKE '%DISNEY CARS LIGHT & SOUND%'  
   OR PRODUCT_NAME LIKE '%TRIKE COMPLETE SET%')  
AND TODDLER_FLAG = 'YES'
;

UPDATE vpm_data.pm_skutagging_kids_product_master 
SET TODDLER_KEYWORD = 'PLAYPEN'
WHERE MOM_AND_KID_PRODUCT_FLAG = 1
AND REVISED_CATEGORY_LEVEL_2 = 'KID FURNITURE'
AND TODDLER_KEYWORD IS NULL
AND (PRODUCT_NAME LIKE '%LULLABY%PLAYARD%'
   OR PRODUCT_NAME LIKE '%��ྐྵ%'
   OR PRODUCT_NAME LIKE '%PLAYARD COMMUTER CHANGE%'  
   OR PRODUCT_NAME LIKE '%PNP%NAPPER CHANGER%'
   OR PRODUCT_NAME LIKE '%LITE WAY BASIC WITH BUMPER%'  
   OR PRODUCT_NAME LIKE '%PACK NPLAY CUDDLE COVE%'  
   OR PRODUCT_NAME LIKE '%COMMUTER CHANGE%')  
AND TODDLER_FLAG = 'YES'
;
   
UPDATE vpm_data.pm_skutagging_kids_product_master 
SET TODDLER_KEYWORD = 'AUTO SWING'
WHERE MOM_AND_KID_PRODUCT_FLAG = 1
AND REVISED_CATEGORY_LEVEL_2 = 'KID FURNITURE'
AND TODDLER_KEYWORD IS NULL
AND (PRODUCT_NAME LIKE '%FEALETTO AUTO SWING%'  
   OR PRODUCT_NAME LIKE '%NEMULILA AUTO SWING%'  
   OR PRODUCT_NAME LIKE '%HYBRID CRADLE%'  
   OR PRODUCT_NAME LIKE '%RIDE ON%')  
AND TODDLER_FLAG = 'YES'
;

UPDATE vpm_data.pm_skutagging_kids_product_master 
SET TODDLER_KEYWORD = CASE WHEN PRODUCT_NAME LIKE '%��٧%' THEN 'LEAD BACKPACK'
                           WHEN PRODUCT_NAME LIKE '%�ٴ����١%' THEN 'SNOT SUCKER'
						   WHEN PRODUCT_NAME LIKE '%�ҧ����ҧ�Һ���%' THEN 'BATHING MAT'
						   ELSE TODDLER_KEYWORD
						   END
WHERE MOM_AND_KID_PRODUCT_FLAG = 1
AND REVISED_CATEGORY_LEVEL_2 = 'KID FURNITURE'
AND TODDLER_KEYWORD IS NULL
AND TODDLER_FLAG = 'YES'
;

UPDATE vpm_data.pm_skutagging_kids_product_master 
SET TODDLER_KEYWORD = 'HIGH CHAIR'
WHERE MOM_AND_KID_PRODUCT_FLAG = 1
AND REVISED_CATEGORY_LEVEL_2 = 'KID FURNITURE'
AND TODDLER_KEYWORD IS NULL
AND (PRODUCT_NAME LIKE '%HIGHCHAIR%'  
   OR PRODUCT_NAME LIKE '%������ҹ����%'  
   OR PRODUCT_NAME LIKE '%��͹����%')
AND TODDLER_FLAG = 'YES'
;

UPDATE vpm_data.pm_skutagging_kids_product_master 
SET TODDLER_KEYWORD = 'CAR SEAT'
WHERE MOM_AND_KID_PRODUCT_FLAG = 1
AND REVISED_CATEGORY_LEVEL_2 = 'KID FURNITURE'
AND TODDLER_KEYWORD IS NULL
AND TYPE_FINAL = 'CAR SEAT'
AND  (PRODUCT_NAME NOT LIKE '%��%'
      OR PRODUCT_NAME NOT LIKE '%������%'
      OR PRODUCT_NAME NOT LIKE '%��С���%'
      OR PRODUCT_NAME NOT LIKE '%�á�Դ%'
      OR PRODUCT_NAME NOT LIKE '%0+1+2%')
AND TODDLER_FLAG = 'YES'
;

--PM PRODUCT SCHEMA
UPDATE vpm_data.pm_skutagging_kids_product_master  
SET TODDLER_KEYWORD = 'STROLLER'
WHERE MOM_AND_KID_PRODUCT_FLAG = 1
AND REVISED_CATEGORY_LEVEL_2 = 'KID FURNITURE'
AND TODDLER_KEYWORD IS NULL
AND TYPE_FINAL = 'STROLLER'
AND  (PRODUCT_NAME LIKE '%�ػ�ó�����ͧ��%'
      OR PRODUCT_NAME LIKE '%ͻءó�����ͧ��%'
      OR PRODUCT_NAME LIKE '%�ش��%')
AND TODDLER_FLAG = 'YES'
;

--BU PRODUCT SCHEMA
UPDATE vpm_data.pm_skutagging_kids_product_master 
SET TODDLER_KEYWORD = 'TODDLER FURNITURE'
WHERE MOM_AND_KID_PRODUCT_FLAG = 1
AND REVISED_CATEGORY_LEVEL_2 = 'KID FURNITURE'
AND TODDLER_KEYWORD IS NULL
AND (SUBDEPT_NAME LIKE 'BABY S WEAR'
     OR CLASS_NAME LIKE '%TODDLER%'
     OR SUBCLASS_NAME LIKE '%TODDLER%'
	 OR PRODUCT_NAME LIKE '%TODLR%'
	 OR PRODUCT_NAME LIKE '%TODDLER%')
AND TODDLER_FLAG = 'YES'
;

--FINAL 
UPDATE vpm_data.pm_skutagging_kids_product_master
SET TODDLER_KEYWORD = 'TODDLER FURNITURE'
WHERE MOM_AND_KID_PRODUCT_FLAG = 1
AND REVISED_CATEGORY_LEVEL_2 = 'KID FURNITURE'
AND TODDLER_KEYWORD IS NULL
AND TODDLER_FLAG = 'YES'
;

------------------------------- DIAPER -------------------------------

--KEY WORD
UPDATE vpm_data.pm_skutagging_kids_product_master 
SET TODDLER_KEYWORD = CASE WHEN PRODUCT_NAME LIKE '%XL%' THEN 'DIAPER SIZE XL'
                           WHEN PRODUCT_NAME LIKE '%XXL%' THEN 'DIAPER SIZE XXL'
						   WHEN PRODUCT_NAME LIKE '%18M%' THEN '18M'
						   ELSE TODDLER_KEYWORD
						   END
WHERE MOM_AND_KID_PRODUCT_FLAG = 1
AND REVISED_CATEGORY_LEVEL_2 = 'DIAPERS'
AND TODDLER_KEYWORD IS NULL
AND TODDLER_FLAG = 'YES'
;

--FINAL 
UPDATE vpm_data.pm_skutagging_kids_product_master
SET TODDLER_KEYWORD = 'TODDLER DIAPERS'
WHERE MOM_AND_KID_PRODUCT_FLAG = 1
AND REVISED_CATEGORY_LEVEL_2 = 'DIAPERS'
AND TODDLER_KEYWORD IS NULL
AND TODDLER_FLAG = 'YES'
;

------------------------------- KID FOOD -------------------------------

--KEY WORD
UPDATE vpm_data.pm_skutagging_kids_product_master 
SET TODDLER_KEYWORD = 'MILK POWDER FORMULA 3'
WHERE MOM_AND_KID_PRODUCT_FLAG = 1
AND REVISED_CATEGORY_LEVEL_2 = 'KID FOOD'
AND TODDLER_KEYWORD IS NULL
AND TYPE_FINAL = 'CAR SEAT'
AND (PRODUCT_NAME LIKE '%ENFALAC 3%'
	OR PRODUCT_NAME LIKE '%SIMILAC 3%'
	OR PRODUCT_NAME LIKE '%SIMILAC COMFORT3%'
	OR PRODUCT_NAME LIKE '%DG3%'
	OR PRODUCT_NAME LIKE '%DUMILK 3%'
	OR PRODUCT_NAME LIKE '%DUMILK3%'	
	OR PRODUCT_NAME LIKE '%DUTCHMILL%3%'
	OR PRODUCT_NAME LIKE '%LACTOGEN3%'	
	OR PRODUCT_NAME LIKE '%BEAR%BRAND%3%'
	OR PRODUCT_NAME LIKE '%NANKID%3%'	
    OR PRODUCT_NAME LIKE '%DUGRO%3%'
	OR PRODUCT_NAME LIKE '%ENFAGROW%3%'
	OR PRODUCT_NAME LIKE '%DUMEX%3%'
	OR PRODUCT_NAME LIKE '%S26%PROGRES%'
	OR PRODUCT_NAME LIKE '%S26%PE%'
	OR PRODUCT_NAME LIKE '%1 PLUS%'
	OR PRODUCT_NAME LIKE '%1PLUS%'
	OR PRODUCT_NAME LIKE '%CARNATION%1%PLUS%'
	OR PRODUCT_NAME LIKE '%HI%Q%1%PLUS%'
	OR PRODUCT_NAME LIKE '%NUTRICIA%MILNUTRISURE%'
	OR PRODUCT_NAME LIKE '%ISOMIL PLUS AI Q PLUS%')
AND TODDLER_FLAG = 'YES'
;

UPDATE vpm_data.pm_skutagging_kids_product_master 
SET TODDLER_KEYWORD = 'MILK POWDER FORMULA 4'
WHERE MOM_AND_KID_PRODUCT_FLAG = 1
AND REVISED_CATEGORY_LEVEL_2 = 'KID FOOD'
AND TODDLER_KEYWORD IS NULL
AND TYPE_FINAL = 'CAR SEAT'
AND (PRODUCT_NAME LIKE '%ENFAGROW%A%PLUS4%'
	OR PRODUCT_NAME LIKE '%DUMILK4%')
AND TODDLER_FLAG = 'YES'
;

--FINAL 
UPDATE vpm_data.pm_skutagging_kids_product_master
SET TODDLER_KEYWORD = 'TODDLER MILK POWDER'
WHERE MOM_AND_KID_PRODUCT_FLAG = 1
AND REVISED_CATEGORY_LEVEL_2 = 'KID FOOD'
AND TODDLER_KEYWORD IS NULL
AND TODDLER_FLAG = 'YES'
;

------------------------------- KID ACCESSORIES -------------------------------

--PM PRODUCT SCHEMA
UPDATE vpm_data.pm_skutagging_kids_product_master 
SET TODDLER_KEYWORD = CASE WHEN TYPE_FINAL = 'PERSONAL CARE' THEN 'PERSONAL CARE'
                           WHEN TYPE_FINAL = 'CAR SEAT' THEN 'CAR SEAT'
						   WHEN TYPE_FINAL = 'STROLLER' THEN 'STROLLER'
						   WHEN (TYPE_FINAL = 'MILK POWDER' AND PRODUCT_NAME LIKE '%�ٵ�2%') THEN 'STEP2 MILK POWDER'
						   WHEN TYPE_FINAL = 'FABRIC' THEN 'TODDLER TOWEL'
						   WHEN TYPE_FINAL = 'TOY' THEN 'TODDLER TOY'
						   WHEN (TYPE_FINAL = 'BEDDING' AND PRODUCT_NAME NOT LIKE '%����%' AND PRODUCT_NAME NOT LIKE '%��ͧ%') THEN 'TODDLER BEDDING'
						   WHEN TYPE_FINAL = 'ACCESSORIES' THEN 'TODDLER ACCESSORIES'
						   WHEN (TYPE_FINAL = 'SAFETY' AND PRODUCT_NAME LIKE '%������ҹ����%') THEN 'HIGH CHAIR'
						   WHEN (TYPE_FINAL = 'BABY BOUNCER' AND PRODUCT_NAME LIKE '%��ྐྵ%') THEN 'PLAYPEN'
						   WHEN (TYPE_FINAL = 'THERMOMETER' AND PRODUCT_NAME LIKE '%THERMOMETER%') THEN 'THERMOMETER'
						   ELSE TODDLER_KEYWORD
						   END
WHERE MOM_AND_KID_PRODUCT_FLAG = 1
AND REVISED_CATEGORY_LEVEL_2 = 'KID ACCESSORIES'
AND TODDLER_KEYWORD IS NULL
AND TODDLER_FLAG = 'YES'
;

UPDATE vpm_data.pm_skutagging_kids_product_master 
SET TODDLER_KEYWORD = 'TOOTHBRUSH'
WHERE MOM_AND_KID_PRODUCT_FLAG = 1
AND REVISED_CATEGORY_LEVEL_2 = 'KID ACCESSORIES'
AND (PRODUCT_NAME LIKE '%�ç�տѹ%' 
	OR PRODUCT_NAME LIKE '%TOOTHBRUSH%'
	OR PRODUCT_NAME LIKE '%0-3%YEARS%'
	OR PRODUCT_NAME LIKE '%0-5��%'
	OR PRODUCT_NAME LIKE '%18-36%MONTH%'
	OR PRODUCT_NAME LIKE '%���տѹ%')
AND TODDLER_KEYWORD IS NULL
AND TODDLER_FLAG = 'YES'
;

UPDATE vpm_data.pm_skutagging_kids_product_master 
SET TODDLER_KEYWORD = CASE WHEN PRODUCT_NAME LIKE '%��еԡ���%' THEN '��еԡ���'
                           WHEN PRODUCT_NAME LIKE '%��͹����%' THEN '�ػ�ó�ҹ����'
                           WHEN PRODUCT_NAME LIKE '%���ⶹ%' THEN '���ⶹ'
                           WHEN PRODUCT_NAME LIKE '%�ҹ%' THEN '�ػ�ó�ҹ����'
                           WHEN PRODUCT_NAME LIKE '%���%' THEN '�ػ�ó�ҹ����'
                           WHEN PRODUCT_NAME LIKE '%�ѡ���%' THEN '�ѡ���'
                           WHEN PRODUCT_NAME LIKE '%�ҹ�����%' THEN '�ػ�ó�ҹ����'
                           WHEN PRODUCT_NAME LIKE '%TABLE%' THEN '�ػ�ó�ҹ����'
                           WHEN PRODUCT_NAME LIKE '%BOWL%' THEN '�ػ�ó�ҹ����'
                           WHEN PRODUCT_NAME LIKE '%BOTTLE%' THEN '��еԡ���'
                           WHEN PRODUCT_NAME LIKE '%��ҡѹ���͹%' THEN '��ҡѹ���͹'
                           WHEN PRODUCT_NAME LIKE '%BATH%' THEN '�Һ���'
                           WHEN PRODUCT_NAME LIKE '%�Һ���%' THEN '�Һ���'
                           WHEN PRODUCT_NAME LIKE '%ʺ��%' THEN '�Һ���'
                           WHEN PRODUCT_NAME LIKE '%����%' THEN '�Һ���'
                           WHEN PRODUCT_NAME LIKE '%SHAMPOO%' THEN '�Һ���'
                           WHEN PRODUCT_NAME LIKE '%���%' THEN '�ػ�ó�ҹ����'
                           WHEN PRODUCT_NAME LIKE '%����%' THEN '�ػ�ó�ҹ����'
                           WHEN PRODUCT_NAME LIKE '%UTENSILS%' THEN '�ػ�ó�ҹ����'
                           WHEN PRODUCT_NAME LIKE '%TODDLER%' THEN 'TODDLER ACCESSORIES'
                           WHEN PRODUCT_NAME LIKE '%���ͧ%�����%' THEN '�ػ�ó�ҹ����'
                           WHEN PRODUCT_NAME LIKE '%FORK%' THEN '�ػ�ó�ҹ����'
                           WHEN PRODUCT_NAME LIKE '%SPOON%' THEN '�ػ�ó�ҹ����'
                           WHEN PRODUCT_NAME LIKE '%�����͵��%' THEN 'TODDLER TOWEL'
                           WHEN PRODUCT_NAME LIKE '%��ҧ�ҹ%' THEN '�������ҧ�ҹ'
                           WHEN PRODUCT_NAME LIKE '%��Ѻ��ҹ���%' THEN '�ѡ���'
                           WHEN PRODUCT_NAME LIKE '%���¹��%' THEN 'SWIMMING'
                           WHEN PRODUCT_NAME LIKE '%��µ��%' THEN 'SWIMMING'
						   ELSE TODDLER_KEYWORD
						   END
WHERE MOM_AND_KID_PRODUCT_FLAG = 1
AND REVISED_CATEGORY_LEVEL_2 = 'KID ACCESSORIES'
AND TODDLER_KEYWORD IS NULL
AND TODDLER_FLAG = 'YES'
;

--BU PRODUCT SCHEMA
UPDATE vpm_data.pm_skutagging_kids_product_master 
SET TODDLER_KEYWORD = 'TODDLER ACCESSORIES'
WHERE MOM_AND_KID_PRODUCT_FLAG = 1
AND REVISED_CATEGORY_LEVEL_2 = 'KID ACCESSORIES'
AND TODDLER_KEYWORD IS NULL
AND (SUBDEPT_NAME LIKE 'BABY S WEAR'
     OR CLASS_NAME LIKE '%TODDLER%'
     OR SUBCLASS_NAME LIKE '%TODDLER%')
AND TODDLER_FLAG = 'YES'

;
--FINAL 
UPDATE vpm_data.pm_skutagging_kids_product_master
SET TODDLER_KEYWORD = 'TODDLER ACCESSORIES'
WHERE MOM_AND_KID_PRODUCT_FLAG = 1
AND REVISED_CATEGORY_LEVEL_2 = 'KID ACCESSORIES'
AND TODDLER_KEYWORD IS NULL
AND TODDLER_FLAG = 'YES'
;

------------------------------- BOOK -------------------------------

UPDATE vpm_data.pm_skutagging_kids_product_master
SET TODDLER_KEYWORD = CASE WHEN CLASS_NAME = 'BABY - PRESCHOOL/BASIC LE' THEN 'BABY-PRESCHOOL BOOK'
                           WHEN PRODUCT_NAME LIKE '%�١%' THEN '�١'
                           WHEN PRODUCT_NAME LIKE '%�ء%' THEN '�١'
                           WHEN PRODUCT_NAME LIKE '%���%' THEN '�س���'
                           WHEN PRODUCT_NAME LIKE '%��%' THEN '��'
                           WHEN PRODUCT_NAME LIKE '%͹غ��%' THEN '͹غ��'
						   WHEN PRODUCT_NAME LIKE '%�Էҹ%' THEN '�Էҹ'
						   WHEN PRODUCT_NAME LIKE '%3-4��%' THEN '3-4��'
						   WHEN PRODUCT_NAME LIKE '%���%' THEN '���'
						   WHEN PRODUCT_NAME LIKE '%�Ѳ�ҡ��%' THEN '�Ѳ�ҡ��'
						   WHEN PRODUCT_NAME LIKE '%�Թ��%' THEN '�Թ��'
						   WHEN PRODUCT_NAME LIKE '%˹�%' THEN '��'
						   ELSE TODDLER_KEYWORD
						   END
WHERE MOM_AND_KID_PRODUCT_FLAG = 1
AND REVISED_CATEGORY_LEVEL_2 = 'BOOK'
AND TODDLER_FLAG = 'YES'
AND TODDLER_KEYWORD IS NULL
AND PRODUCT_NAME NOT LIKE '%���%'
;

--FINAL 
UPDATE vpm_data.pm_skutagging_kids_product_master
SET TODDLER_KEYWORD = 'TODDLER BOOK'
WHERE MOM_AND_KID_PRODUCT_FLAG = 1
AND REVISED_CATEGORY_LEVEL_2 = 'BOOK'
AND TODDLER_KEYWORD IS NULL
AND TODDLER_FLAG = 'YES'
AND CLASS_NAME NOT LIKE 'STUDY AND EXAMINATION GUI'
AND SUBDEPT_NAME NOT LIKE 'EXAMINATION GUIDE'
AND PRODUCT_NAME NOT LIKE '%���%'
;

------------------------------- TOYS -------------------------------

UPDATE vpm_data.pm_skutagging_kids_product_master
SET TODDLER_KEYWORD = CASE WHEN PRODUCT_NAME LIKE '%3Y%' THEN '3Y'
						   WHEN PRODUCT_NAME LIKE '%18M%' THEN '18M'
						   WHEN PRODUCT_NAME LIKE '%2Y%' THEN '2Y'
						   WHEN PRODUCT_NAME LIKE '%��꡵�%' THEN '��꡵�'
                           WHEN PRODUCT_NAME LIKE '%DOLL%' THEN '��꡵�'
                           WHEN PRODUCT_NAME LIKE '%DOUGH%' THEN '�Թ����ѹ'
                           WHEN PRODUCT_NAME LIKE '%���%' THEN '�Թ����ѹ'
                           WHEN PRODUCT_NAME LIKE '%�Թ����ѹ%' THEN '�Թ����ѹ'
						   WHEN PRODUCT_NAME LIKE '%���%' THEN '���'
						   WHEN PRODUCT_NAME LIKE '%�Ѵ%' THEN '�����Ѵ����'
						   WHEN PRODUCT_NAME LIKE '%�ͧ���%' THEN '�ͧ���'
						   WHEN PRODUCT_NAME LIKE '%�Ԩ����%' THEN '�Ԩ����'
						   WHEN PRODUCT_NAME LIKE '%CARTOON%' THEN 'CARTOON'
						   WHEN PRODUCT_NAME LIKE '%˹�%' THEN '��'
						   WHEN PRODUCT_NAME LIKE '%CARD%' THEN 'CARD'
						   WHEN PRODUCT_NAME LIKE '%BLOCK%' THEN 'BLOCK'
						   WHEN PRODUCT_NAME LIKE '%KITCHEN%' THEN 'KITCHEN'
						   WHEN PRODUCT_NAME LIKE '%ROBO%' THEN 'ROBOT'
						   WHEN PRODUCT_NAME LIKE '%BUBBLE%' THEN 'BUBBLE'
						   WHEN PRODUCT_NAME LIKE '%BALL%' THEN '���'
						   WHEN PRODUCT_NAME LIKE '%DRAWING%' THEN 'DRAWING'
						   WHEN PRODUCT_NAME LIKE '%TROLLEY%' THEN 'TROLLEY'
						   WHEN PRODUCT_NAME LIKE '%MUSIC%' THEN 'MUSIC'
						   WHEN PRODUCT_NAME LIKE '%JIGSAW%' THEN 'JIGSAW'
						   WHEN PRODUCT_NAME LIKE '%GAME%' THEN 'GAME'
						   WHEN PRODUCT_NAME LIKE '%MUSIC%' THEN 'MUSIC'
						   WHEN PRODUCT_NAME LIKE '%PLAYPEN%' THEN 'PLAYPEN'
						   WHEN CLASS_NAME LIKE 'EDUCATIONAL%' THEN 'EDUCATIONAL TOY'
						   WHEN CLASS_NAME LIKE 'TOYS' THEN 'TODDLER TOY'
						   WHEN PRODUCT_NAME LIKE '%TOY%' THEN 'TODDLER TOY'
						   ELSE TODDLER_KEYWORD
						   END
WHERE MOM_AND_KID_PRODUCT_FLAG = 1
AND REVISED_CATEGORY_LEVEL_2 = 'TOYS'
AND TODDLER_FLAG = 'YES'
AND TODDLER_KEYWORD IS NULL
;

--FINAL 
UPDATE vpm_data.pm_skutagging_kids_product_master
SET TODDLER_KEYWORD = 'TODDLER TOYS'
WHERE MOM_AND_KID_PRODUCT_FLAG = 1
AND REVISED_CATEGORY_LEVEL_2 = 'TOYS'
AND TODDLER_FLAG = 'YES'
AND TODDLER_KEYWORD IS NULL
;









-----VERY LAST STEP
--NOT MOM AND KID PRODUCT
UPDATE vpm_data.pm_skutagging_kids_product_master
SET MOM_AND_KID_PRODUCT_FLAG = 0
WHERE MOM_AND_KID_PRODUCT_FLAG = 1
AND COALESCE(PREG_FLAG,'N/A') = 'N/A'
AND COALESCE(INFANT_FLAG,'N/A') = 'N/A'
AND COALESCE(TODDLER_FLAG,'N/A') = 'N/A'
AND COALESCE(PRESCHOOL_FLAG,'N/A') = 'N/A'
AND COALESCE(JUNIOR_FLAG,'N/A') = 'N/A'
AND COALESCE(TEEN_FLAG,'N/A') = 'N/A'
AND (REVISED_CATEGORY_LEVEL_2 IN ('MAKE UP','TREATMENT','OTHER','STORAGE AND MISCELLANEOUS','KITCHENWARE','FURNITURE AND DECORATION','BEDDING','BATHING','WOMEN','UNISEX','STATIONERY')
	OR REVISED_CATEGORY_LEVEL_2 IS NULL
	OR (REVISED_CATEGORY_LEVEL_2 = 'BOOK' AND DEPT_NAME = 'BOOK' AND SUBDEPT_NAME LIKE '%����ٹ%')
	OR (DEPT_NAME = 'BOOK' AND SUBDEPT_NAME IN ('����ٹ BOY LOVE / YAOI','����� GIRL LOVE/YURI','�������� BOY LOVE / YAOI'))
	OR PRODUCT_NAME LIKE '%��д����%'
	OR PRODUCT_NAME LIKE '%������硵Դ������%'
	OR PRODUCT_NAME LIKE '%�ǧ�ح�%'
	OR PRODUCT_NAME LIKE '%����Ѵ������%'
	OR PRODUCT_NAME LIKE '%�١��%'
	OR PRODUCT_NAME LIKE '%MAGNET%'
	OR PRODUCT_NAME LIKE '%�׹�մ���%'
	OR PRODUCT_NAME LIKE '%��մ���%'
	OR PRODUCT_NAME LIKE '%BOOKMARK%'
	OR PRODUCT_NAME LIKE 'ZIPPER CHARM%'
	OR PRODUCT_NAME LIKE 'ZIPPER KEY%'
	OR PRODUCT_NAME LIKE '%KEYCHAIN%'
	OR PRODUCT_NAME LIKE '%KEYRING%'
	OR PRODUCT_NAME LIKE '%�����������%'
	OR PRODUCT_NAME LIKE '%���������%'
	OR PRODUCT_NAME LIKE '%���������%'
	OR PRODUCT_NAME LIKE '%�������ѧ��%'
	OR PRODUCT_NAME LIKE '%BOTTLE OPENER%'
	OR PRODUCT_NAME LIKE '%���ͧ���µԴ��Ͷ��ἧ%'
	OR PRODUCT_NAME LIKE '%HOLDER KEY%'
	OR PRODUCT_NAME LIKE '%���PVC%'
	OR PRODUCT_NAME LIKE '%PLIERS%'
	OR PRODUCT_NAME LIKE '%���ԡ� D.I.Y%'
	OR PRODUCT_NAME LIKE '%�������ҵ�ҧ�%'
	OR CLEANED_BRANDNAME LIKE 'CHATCHAI WIKAIKUL'
	OR CLEANED_BRANDNAME LIKE 'METALEARTH'
	OR CLEANED_BRANDNAME LIKE 'ORIGINAL MAGNET'
	OR CLEANED_BRANDNAME LIKE 'ROYAL&LANGNICKEL'
	OR CLEANED_BRANDNAME LIKE 'STUFF2COLOR')
;

UPDATE vpm_data.pm_skutagging_kids_product_master
SET MOM_AND_KID_PRODUCT_FLAG = 0
WHERE MOM_AND_KID_PRODUCT_FLAG = 1
AND COALESCE(PREG_FLAG,'N/A') = 'N/A'
AND COALESCE(INFANT_FLAG,'N/A') = 'N/A'
AND COALESCE(TODDLER_FLAG,'N/A') = 'N/A'
AND COALESCE(PRESCHOOL_FLAG,'N/A') = 'N/A'
AND COALESCE(JUNIOR_FLAG,'N/A') = 'N/A'
AND COALESCE(TEEN_FLAG,'N/A') = 'N/A'
AND (DEPT_NAME = 'BOOK' AND SUBDEPT_NAME IN ('����ٹ BOY LOVE / YAOI','����� GIRL LOVE/YURI','�������� BOY LOVE / YAOI'))
;

--INFANT UNKNOWN SUBSTAGE
UPDATE vpm_data.pm_skutagging_kids_product_master
SET INFANT_FLAG = 'YES'
	,UNKNOWN_INFANT_SUBSTAGE = 'YES'
	,INFANT_UNKNOWN_KEYWORD = '0-2 Y'
WHERE MOM_AND_KID_PRODUCT_FLAG = 1
AND COALESCE(PREG_FLAG,'N/A') = 'N/A'
AND COALESCE(INFANT_FLAG,'N/A') = 'N/A'
AND COALESCE(TODDLER_FLAG,'N/A') = 'N/A'
AND COALESCE(PRESCHOOL_FLAG,'N/A') = 'N/A'
AND COALESCE(JUNIOR_FLAG,'N/A') = 'N/A'
AND COALESCE(TEEN_FLAG,'N/A') = 'N/A'
AND REVISED_CATEGORY_LEVEL_2 = 'BOOK'
AND PRODUCT_NAME LIKE '%��á-2�Ǻ%'
;

--OLDER INFANT AND TODDLER
UPDATE vpm_data.pm_skutagging_kids_product_master
SET INFANT_FLAG = 'N/A'
	,TODDLER_FLAG = 'YES'
	,TODDLER_KEYWORD = (CASE WHEN PRODUCT_NAME LIKE '%�Ҿ�Ѵ���Ό��%' THEN '�Ҿ�Ѵ���Ό��'
									WHEN PRODUCT_NAME LIKE '%�йҶ����� 8 ���§%' THEN '�йҴ 8 ���§'
									ELSE TODDLER_KEYWORD END)
WHERE MOM_AND_KID_PRODUCT_FLAG = 1
AND COALESCE(PREG_FLAG,'N/A') = 'N/A'
AND COALESCE(INFANT_FLAG,'N/A') = 'N/A'
AND COALESCE(TODDLER_FLAG,'N/A') = 'N/A'
AND COALESCE(PRESCHOOL_FLAG,'N/A') = 'N/A'
AND COALESCE(JUNIOR_FLAG,'N/A') = 'N/A'
AND COALESCE(TEEN_FLAG,'N/A') = 'N/A'
AND REVISED_CATEGORY_LEVEL_2 = 'BOOK'
AND (PRODUCT_NAME LIKE '%�Ҿ�Ѵ���Ό��%'
	OR PRODUCT_NAME LIKE '%�йҶ����� 8 ���§%')
;

--INFANT AND TODDLER UNKNOWN SUBSTAGE
UPDATE vpm_data.pm_skutagging_kids_product_master
SET INFANT_FLAG = 'YES'
	,UNKNOWN_INFANT_SUBSTAGE = 'YES'
	,TODDLER_FLAG = 'YES'
	,INFANT_UNKNOWN_KEYWORD = (CASE WHEN REVISED_CATEGORY_LEVEL_2 = 'GAME AND TOYS' THEN SUBCLASS_NAME
									WHEN (REVISED_CATEGORY_LEVEL_2 = 'UNISEX' AND PRODUCT_NAME LIKE '%ö����%') THEN 'STROLLER'
									WHEN (REVISED_CATEGORY_LEVEL_2 = 'BOOK' AND SUBCLASS_NAME = 'BABY FOOD') THEN 'BABY FOOD COOKBOOK'
									ELSE INFANT_UNKNOWN_KEYWORD END)
	,TODDLER_KEYWORD = (CASE WHEN REVISED_CATEGORY_LEVEL_2 = 'GAME AND TOYS' THEN SUBCLASS_NAME
									WHEN (REVISED_CATEGORY_LEVEL_2 = 'UNISEX' AND PRODUCT_NAME LIKE '%ö����%') THEN 'STROLLER'
									WHEN (REVISED_CATEGORY_LEVEL_2 = 'BOOK' AND SUBCLASS_NAME = 'BABY FOOD') THEN 'BABY FOOD COOKBOOK'
									ELSE TODDLER_KEYWORD END)
WHERE MOM_AND_KID_PRODUCT_FLAG = 1
AND COALESCE(PREG_FLAG,'N/A') = 'N/A'
AND COALESCE(INFANT_FLAG,'N/A') = 'N/A'
AND COALESCE(TODDLER_FLAG,'N/A') = 'N/A'
AND COALESCE(PRESCHOOL_FLAG,'N/A') = 'N/A'
AND COALESCE(JUNIOR_FLAG,'N/A') = 'N/A'
AND COALESCE(TEEN_FLAG,'N/A') = 'N/A'
AND (SUBDEPT_NAME = 'BABY S FURNITURES'
	OR REVISED_CATEGORY_LEVEL_2 = 'GAME AND TOYS'
	OR (REVISED_CATEGORY_LEVEL_2 = 'UNISEX' AND PRODUCT_NAME LIKE '%ö����%')
	OR (REVISED_CATEGORY_LEVEL_2 = 'BOOK' AND SUBCLASS_NAME = 'BABY FOOD'))
;

--TODDLER AND PRESCHOOL
UPDATE vpm_data.pm_skutagging_kids_product_master
SET INFANT_FLAG = 'N/A'
	,TODDLER_FLAG = 'YES'
	,PRESCHOOL_FLAG = 'YES'
	,TODDLER_KEYWORD = (CASE WHEN PRODUCT_NAME LIKE '4DCUBE %' THEN '4D CUBE'
							WHEN SUBCLASS_NAME = 'WRONG OR RIGHT GAME' THEN 'WRONG OR RIGHT GAME'
							WHEN SUBCLASS_NAME = 'WORD CARD' THEN 'WORD CARD'
							WHEN PRODUCT_NAME LIKE '% 3Y' THEN '3Y+'
							WHEN PRODUCT_NAME LIKE '%�Ф��ç��硡�맡��%' THEN '��맡��'
							WHEN PRODUCT_NAME LIKE '%��ͧ�鹡Ѻ����ͧ���%' THEN '��ͧ�鹡Ѻ����ͧ���'
							WHEN SUBCLASS_NAME = 'WIT PRACTICE' THEN 'WIT PRACTICE'
							WHEN PRODUCT_NAME LIKE '%��ǵ��%' THEN '��ǵ��'
							WHEN PRODUCT_NAME LIKE '%�Ѻ���%' THEN '�Ѻ���'
							WHEN PRODUCT_NAME LIKE '%MATCHING%GAME%' THEN 'MATCHING GAME'
							WHEN PRODUCT_NAME LIKE '%���ͤ%' THEN 'BLOCK'
							WHEN PRODUCT_NAME LIKE '%���͡%' THEN 'BLOCK'
							WHEN PRODUCT_NAME LIKE '%BLOCK%' THEN 'BLOCK'
							WHEN PRODUCT_NAME LIKE '%����Ţ%' THEN '�Ѻ�Ţ'
							WHEN PRODUCT_NAME LIKE '%�Ѻ�Ţ%' THEN '�Ѻ�Ţ'
							WHEN PRODUCT_NAME LIKE '%A-Z%' THEN 'A-Z'
							WHEN PRODUCT_NAME LIKE '%ABC%' THEN 'A-Z'
							WHEN PRODUCT_NAME LIKE '%�Ѳ�ҡ��%' THEN '�Ѳ�ҡ��'
							WHEN PRODUCT_NAME LIKE 'SHAPE SORTING%' THEN 'SHAPE SORTING'
							WHEN PRODUCT_NAME LIKE '%��ѭ�����%' THEN '��ѭ�����'
							WHEN PRODUCT_NAME LIKE '%�.��%' THEN  '��ѭ�����'
							WHEN PRODUCT_NAME LIKE '%� ��%' THEN  '��ѭ�����'
							WHEN PRODUCT_NAME LIKE '%�-�%' THEN  '��ѭ�����'
							WHEN PRODUCT_NAME LIKE '%���%��ó�ء��%' THEN '�����'
							WHEN PRODUCT_NAME LIKE '%��ҹ��¹���¹�����%' THEN '�����'
							WHEN PRODUCT_NAME LIKE '%1-10%'  THEN '�Ѻ�Ţ'
							WHEN PRODUCT_NAME LIKE '%�-��%' THEN '�Ѻ�Ţ'
							WHEN PRODUCT_NAME LIKE '%���¹%123%' THEN '�Ѻ�Ţ'
							WHEN PRODUCT_NAME LIKE '�ش�Ҵ�ٻ�ç�âҤ�Ե%' THEN '�ٻ�ç�âҤ�Ե'
							WHEN PRODUCT_NAME LIKE '%��鹴Թ%' THEN '��鹴Թ'
							WHEN PRODUCT_NAME LIKE '%�к����%' THEN '�к����'
							WHEN PRODUCT_NAME LIKE '%�ѵä�%' THEN 'WORD CARD'
							WHEN PRODUCT_NAME LIKE '%�ѵ��Ҿ%' THEN 'WORD CARD'
							WHEN PRODUCT_NAME LIKE '%˹ѧ��;ٴ��%' THEN '˹ѧ��;ٴ��'
							WHEN PRODUCT_NAME LIKE '%�ҡ�Ҿٴ��%' THEN '�ҡ�Ҿٴ��'
							WHEN CLEANED_BRANDNAME LIKE 'PELANGI PUBLISHING' THEN '˹ѧ��������'
							WHEN CLEANED_BRANDNAME LIKE '��������� �.�.�.' THEN '˹ѧ��������'
							WHEN CLEANED_BRANDNAME LIKE '���ѧ�� ��' THEN '˹ѧ��������'
							WHEN CLEANED_BRANDNAME LIKE '���ǿ�ҧ 35' THEN '˹ѧ��������'
							WHEN CLEANED_BRANDNAME LIKE 'IHEAD' THEN '˹ѧ��������'
							ELSE TODDLER_KEYWORD END)
WHERE MOM_AND_KID_PRODUCT_FLAG = 1
AND COALESCE(PREG_FLAG,'N/A') = 'N/A'
AND COALESCE(INFANT_FLAG,'N/A') = 'N/A'
AND COALESCE(TODDLER_FLAG,'N/A') = 'N/A'
AND COALESCE(PRESCHOOL_FLAG,'N/A') = 'N/A'
AND COALESCE(JUNIOR_FLAG,'N/A') = 'N/A'
AND COALESCE(TEEN_FLAG,'N/A') = 'N/A'
AND REVISED_CATEGORY_LEVEL_2 = 'BOOK'
AND (PRODUCT_NAME LIKE '4DCUBE %'
	OR SUBCLASS_NAME = 'WRONG OR RIGHT GAME'
	OR SUBCLASS_NAME = 'WORD CARD'
	OR PRODUCT_NAME LIKE '% 3Y'
	OR PRODUCT_NAME LIKE '%�Ф��ç��硡�맡��%'
	OR PRODUCT_NAME LIKE '%��ͧ�鹡Ѻ����ͧ���%'
	OR SUBCLASS_NAME = 'WIT PRACTICE'
	OR PRODUCT_NAME LIKE '%��ǵ��%'
	OR PRODUCT_NAME LIKE '%�Ѻ���%'
	OR PRODUCT_NAME LIKE '%MATCHING%GAME%'
	OR PRODUCT_NAME LIKE '%���ͤ%'
	OR PRODUCT_NAME LIKE '%���ͤ%'
	OR PRODUCT_NAME LIKE '%���͡%'
	OR PRODUCT_NAME LIKE '%BLOCK%'
	OR PRODUCT_NAME LIKE '%����Ţ%'
	OR PRODUCT_NAME LIKE '%�Ѻ�Ţ%'
	OR PRODUCT_NAME LIKE '%A-Z%'
	OR PRODUCT_NAME LIKE '%ABC%'
	OR PRODUCT_NAME LIKE '%�Ѳ�ҡ��%'
	OR PRODUCT_NAME LIKE 'SHAPE SORTING%'
	OR PRODUCT_NAME LIKE '%��ѭ�����%'
	OR PRODUCT_NAME LIKE '%�.��%'
	OR PRODUCT_NAME LIKE '%� ��%'
	OR PRODUCT_NAME LIKE '%�-�%'
	OR PRODUCT_NAME LIKE '%���%��ó�ء��%'
	OR PRODUCT_NAME LIKE '%��ҹ��¹���¹�����%'
	OR PRODUCT_NAME LIKE '%1-10%'
	OR PRODUCT_NAME LIKE '%�-��%'
	OR PRODUCT_NAME LIKE '%���¹%123%'
	OR PRODUCT_NAME LIKE '�ش�Ҵ�ٻ�ç�âҤ�Ե%'
	OR PRODUCT_NAME LIKE '%��鹴Թ%'
	OR PRODUCT_NAME LIKE '%�к����%'
	OR PRODUCT_NAME LIKE '%�ѵä�%'
	OR PRODUCT_NAME LIKE '%�ѵ��Ҿ%'
	OR PRODUCT_NAME LIKE '%˹ѧ��;ٴ��%'
	OR PRODUCT_NAME LIKE '%�ҡ�Ҿٴ��%'
	OR CLEANED_BRANDNAME LIKE 'PELANGI PUBLISHING'
	OR CLEANED_BRANDNAME LIKE '��������� �.�.�.'
	OR CLEANED_BRANDNAME LIKE '���ѧ�� ��'
	OR CLEANED_BRANDNAME LIKE '���ǿ�ҧ 35'
	OR CLEANED_BRANDNAME LIKE 'IHEAD')
;

--PRESCHOOL
UPDATE vpm_data.pm_skutagging_kids_product_master
SET INFANT_FLAG = 'N/A'
	,TODDLER_FLAG = 'N/A'
	,PRESCHOOL_FLAG = 'YES'
WHERE MOM_AND_KID_PRODUCT_FLAG = 1
AND COALESCE(PREG_FLAG,'N/A') = 'N/A'
AND COALESCE(INFANT_FLAG,'N/A') = 'N/A'
AND COALESCE(TODDLER_FLAG,'N/A') = 'N/A'
AND COALESCE(PRESCHOOL_FLAG,'N/A') = 'N/A'
AND COALESCE(JUNIOR_FLAG,'N/A') = 'N/A'
AND COALESCE(TEEN_FLAG,'N/A') = 'N/A'
AND REVISED_CATEGORY_LEVEL_2 = 'BOOK'
AND (PRODUCT_NAME LIKE '%�ǡ�Ţ%'
	OR PRODUCT_NAME LIKE '%��ҧ��¢ͧ�ѹ%'
	OR PRODUCT_NAME LIKE '%���Ѿ��%'
	OR PRODUCT_NAME LIKE '%FLASHCARD%'
	OR CLEANED_BRANDNAME LIKE '���� ��紴�')
;

--NOT INFANT OR TODDLER, BUT KID (MAYBE JUNIOR+)
UPDATE vpm_data.pm_skutagging_kids_product_master
SET INFANT_FLAG = 'N/A'
	,TODDLER_FLAG = 'N/A'
	,JUNIOR_FLAG = 'YES'
WHERE MOM_AND_KID_PRODUCT_FLAG = 1
AND COALESCE(PREG_FLAG,'N/A') = 'N/A'
AND COALESCE(INFANT_FLAG,'N/A') = 'N/A'
AND COALESCE(TODDLER_FLAG,'N/A') = 'N/A'
AND COALESCE(PRESCHOOL_FLAG,'N/A') = 'N/A'
AND COALESCE(JUNIOR_FLAG,'N/A') = 'N/A'
AND COALESCE(TEEN_FLAG,'N/A') = 'N/A'
AND ((SUBDEPT_NAME = 'BABY S FURNITURES' AND CLEANED_BRANDNAME LIKE 'AERA' AND (PRODUCT_NAME LIKE '%��������ͧ%' OR PRODUCT_NAME LIKE '%���ͧ������%'))
	OR REVISED_CATEGORY_LEVEL_2 = 'MEN'
	OR (REVISED_CATEGORY_LEVEL_2 = 'BOOK' AND SUBCLASS_NAME = 'JIGSAW')
	OR (REVISED_CATEGORY_LEVEL_2 = 'BOOK' AND PRODUCT_NAME LIKE '�Ѩ���������١�͹�Ҵ����ٹ%')
	OR (REVISED_CATEGORY_LEVEL_2 = 'BOOK' AND SUBCLASS_NAME = 'OTHERS')
	OR (REVISED_CATEGORY_LEVEL_2 = 'BOOK' AND SUBCLASS_NAME = 'TOY')
	OR PRODUCT_NAME LIKE '%�ٵäٳ%'
	OR PRODUCT_NAME LIKE '%Ẻ�֡��ҹ%'
	OR PRODUCT_NAME LIKE '%���������%'
	OR PRODUCT_NAME LIKE '%�ӡ����%'
	OR PRODUCT_NAME LIKE '%���ʷ%'
	OR PRODUCT_NAME LIKE '%���ʡ%'
	OR PRODUCT_NAME LIKE '%��ǹ�٧%'
	OR PRODUCT_NAME LIKE '�Թ��� TOYART'
	OR PRODUCT_NAME LIKE '�ʡ���乴������%'
	OR CLEANED_BRANDNAME LIKE 'DINO KIDS'
	OR CLEANED_BRANDNAME LIKE 'POLAR PLAY'
	OR CLEANED_BRANDNAME LIKE 'NANYUAN'
	OR CLEANED_BRANDNAME LIKE 'NASS'
	OR CLEANED_BRANDNAME LIKE 'LOZ'
	OR CLEANED_BRANDNAME LIKE 'TOYBIES �ӹѡ��'
	OR CLEANED_BRANDNAME LIKE 'TOYBIES 38'
	OR CLEANED_BRANDNAME LIKE '�պի� �.�.�.'
	OR CLEANED_BRANDNAME LIKE 'ʶҾ� ����� �.�.�.'
	OR CLEANED_BRANDNAME LIKE '������ ����')
;

--TEEN BOOK
UPDATE vpm_data.pm_skutagging_kids_product_master
SET INFANT_FLAG = 'N/A'
	,TODDLER_FLAG = 'N/A'
	,PRESCHOOL_FLAG = 'N/A'
	,JUNIOR_FLAG = 'N/A'
	,TEEN_FLAG = 'YES'
WHERE MOM_AND_KID_PRODUCT_FLAG = 1
AND REVISED_CATEGORY_LEVEL_2 = 'BOOK'
AND (PRODUCT_NAME LIKE '%BIOLOGY%'
	OR PRODUCT_NAME LIKE '%����Է��%'
	OR (PRODUCT_NAME LIKE '%���%' AND PRODUCT_NAME NOT LIKE '%�.1-3%')
	OR PRODUCT_NAME LIKE '%���ԡ%'
	OR PRODUCT_NAME LIKE '%�Ѹ��%'
	OR PRODUCT_NAME LIKE '%�.����%'
	OR PRODUCT_NAME LIKE '%�.��%')
;






------------------------------------------------------------------------------------------------------

------------ CLEAN FASHION FROM TODDLER ------------

UPDATE vpm_data.pm_skutagging_kids_product_master
SET PRESCHOOL_FLAG = 'YES'
WHERE MOM_AND_KID_PRODUCT_FLAG = 1
AND COALESCE(PREG_FLAG,'N/A') = 'N/A'
AND COALESCE(INFANT_FLAG,'N/A') = 'N/A'
AND COALESCE(TODDLER_FLAG,'N/A') = 'N/A'
AND COALESCE(PRESCHOOL_FLAG,'N/A') = 'N/A'
AND COALESCE(JUNIOR_FLAG,'N/A') = 'N/A'
AND COALESCE(TEEN_FLAG,'N/A') = 'N/A'
AND (PRODUCT_NAME LIKE '%5Y%' 
   OR PRODUCT_NAME LIKE '%6Y%' 
   OR PRODUCT_NAME LIKE '%6-7Y%')
;

UPDATE vpm_data.pm_skutagging_kids_product_master
SET JUNIOR_FLAG = 'YES'
WHERE MOM_AND_KID_PRODUCT_FLAG = 1
AND COALESCE(PREG_FLAG,'N/A') = 'N/A'
AND COALESCE(INFANT_FLAG,'N/A') = 'N/A'
AND COALESCE(TODDLER_FLAG,'N/A') = 'N/A'
AND COALESCE(PRESCHOOL_FLAG,'N/A') = 'N/A'
AND COALESCE(JUNIOR_FLAG,'N/A') = 'N/A'
AND COALESCE(TEEN_FLAG,'N/A') = 'N/A'
AND (PRODUCT_NAME LIKE '%6-7Y%'
   OR PRODUCT_NAME LIKE '%7-8Y%'
   OR PRODUCT_NAME LIKE '%8Y%'
   OR PRODUCT_NAME LIKE '%8-9Y%'
   OR PRODUCT_NAME LIKE '%10Y%'
   OR PRODUCT_NAME LIKE '%11-12Y%')
;

-------------------------------- CLEAN BY REVISED_CATEGORY_LEVEL_1 --------------------------------

------------ NULL ------------
UPDATE vpm_data.pm_skutagging_kids_product_master
SET PRESCHOOL_FLAG = 'YES'
   ,JUNIOR_FLAG = 'YES'
WHERE MOM_AND_KID_PRODUCT_FLAG = 1
AND COALESCE(PREG_FLAG,'N/A') = 'N/A'
AND COALESCE(INFANT_FLAG,'N/A') = 'N/A'
AND COALESCE(TODDLER_FLAG,'N/A') = 'N/A'
AND COALESCE(PRESCHOOL_FLAG,'N/A') = 'N/A'
AND COALESCE(JUNIOR_FLAG,'N/A') = 'N/A'
AND COALESCE(TEEN_FLAG,'N/A') = 'N/A'
AND REVISED_CATEGORY_LEVEL_1 IS NULL
AND (PRODUCT_NAME NOT LIKE '%�Ѵ�Թ%')
;

UPDATE vpm_data.pm_skutagging_kids_product_master
SET TEEN_FLAG = 'YES'
WHERE MOM_AND_KID_PRODUCT_FLAG = 1
AND COALESCE(PREG_FLAG,'N/A') = 'N/A'
AND COALESCE(INFANT_FLAG,'N/A') = 'N/A'
AND COALESCE(TODDLER_FLAG,'N/A') = 'N/A'
AND COALESCE(PRESCHOOL_FLAG,'N/A') = 'N/A'
AND COALESCE(JUNIOR_FLAG,'N/A') = 'N/A'
AND COALESCE(TEEN_FLAG,'N/A') = 'N/A'
AND REVISED_CATEGORY_LEVEL_1 IS NULL
AND PRODUCT_NAME LIKE '%�%'
;

------------ OTHER ------------
UPDATE vpm_data.pm_skutagging_kids_product_master
SET PRESCHOOL_FLAG = 'YES'
   ,JUNIOR_FLAG = 'YES'
WHERE MOM_AND_KID_PRODUCT_FLAG = 1
AND COALESCE(PREG_FLAG,'N/A') = 'N/A'
AND COALESCE(INFANT_FLAG,'N/A') = 'N/A'
AND COALESCE(TODDLER_FLAG,'N/A') = 'N/A'
AND COALESCE(PRESCHOOL_FLAG,'N/A') = 'N/A'
AND COALESCE(JUNIOR_FLAG,'N/A') = 'N/A'
AND COALESCE(TEEN_FLAG,'N/A') = 'N/A'
AND REVISED_CATEGORY_LEVEL_1 = 'OTHER'
AND DEPT_NAME = 'NON-FOOD'
;

UPDATE vpm_data.pm_skutagging_kids_product_master
SET TEEN_FLAG = 'YES'
WHERE MOM_AND_KID_PRODUCT_FLAG = 1
AND COALESCE(PREG_FLAG,'N/A') = 'N/A'
AND COALESCE(INFANT_FLAG,'N/A') = 'N/A'
AND COALESCE(TODDLER_FLAG,'N/A') = 'N/A'
AND COALESCE(PRESCHOOL_FLAG,'N/A') = 'N/A'
AND COALESCE(JUNIOR_FLAG,'N/A') = 'N/A'
AND COALESCE(TEEN_FLAG,'N/A') = 'N/A'
AND REVISED_CATEGORY_LEVEL_1 = 'OTHER'
AND DEPT_NAME = 'FOREIGN BOOKS'
;

------------ HOME ELECTRONICS ------------
UPDATE vpm_data.pm_skutagging_kids_product_master
SET PRESCHOOL_FLAG = 'YES'
   ,JUNIOR_FLAG = 'YES'
WHERE MOM_AND_KID_PRODUCT_FLAG = 1
AND COALESCE(PREG_FLAG,'N/A') = 'N/A'
AND COALESCE(INFANT_FLAG,'N/A') = 'N/A'
AND COALESCE(TODDLER_FLAG,'N/A') = 'N/A'
AND COALESCE(PRESCHOOL_FLAG,'N/A') = 'N/A'
AND COALESCE(JUNIOR_FLAG,'N/A') = 'N/A'
AND COALESCE(TEEN_FLAG,'N/A') = 'N/A'
AND REVISED_CATEGORY_LEVEL_1 = 'HOME ELECTRONICS'
;

------------ HOME NON-ELECTRONICS ------------

--ACCESSORIES
UPDATE vpm_data.pm_skutagging_kids_product_master
SET PRESCHOOL_FLAG = 'YES'
WHERE MOM_AND_KID_PRODUCT_FLAG = 1
AND COALESCE(PREG_FLAG,'N/A') = 'N/A'
AND COALESCE(INFANT_FLAG,'N/A') = 'N/A'
AND COALESCE(TODDLER_FLAG,'N/A') = 'N/A'
AND COALESCE(PRESCHOOL_FLAG,'N/A') = 'N/A'
AND COALESCE(JUNIOR_FLAG,'N/A') = 'N/A'
AND COALESCE(TEEN_FLAG,'N/A') = 'N/A'
AND REVISED_CATEGORY_LEVEL_1 = 'HOME NON-ELECTRONICS'
AND SUBCLASS_NAME = 'ACCESSORIES'
AND PRODUCT_NAME LIKE '%4-7Y%'
;

UPDATE vpm_data.pm_skutagging_kids_product_master
SET JUNIOR_FLAG = 'YES'
WHERE MOM_AND_KID_PRODUCT_FLAG = 1
AND COALESCE(PREG_FLAG,'N/A') = 'N/A'
AND COALESCE(INFANT_FLAG,'N/A') = 'N/A'
AND COALESCE(TODDLER_FLAG,'N/A') = 'N/A'
AND COALESCE(PRESCHOOL_FLAG,'N/A') = 'N/A'
AND COALESCE(JUNIOR_FLAG,'N/A') = 'N/A'
AND COALESCE(TEEN_FLAG,'N/A') = 'N/A'
AND REVISED_CATEGORY_LEVEL_1 = 'HOME NON-ELECTRONICS'
AND SUBCLASS_NAME = 'ACCESSORIES'
AND PRODUCT_NAME LIKE '%7-10Y%'
;

--MODEL
UPDATE vpm_data.pm_skutagging_kids_product_master
SET PRESCHOOL_FLAG = 'YES'
   ,JUNIOR_FLAG = 'YES'
WHERE MOM_AND_KID_PRODUCT_FLAG = 1
AND COALESCE(PREG_FLAG,'N/A') = 'N/A'
AND COALESCE(INFANT_FLAG,'N/A') = 'N/A'
AND COALESCE(TODDLER_FLAG,'N/A') = 'N/A'
AND COALESCE(PRESCHOOL_FLAG,'N/A') = 'N/A'
AND COALESCE(JUNIOR_FLAG,'N/A') = 'N/A'
AND COALESCE(TEEN_FLAG,'N/A') = 'N/A'
AND REVISED_CATEGORY_LEVEL_1 = 'HOME NON-ELECTRONICS'
AND SUBCLASS_NAME = 'MODEL'
;

--ANIMAL/GAMES
UPDATE vpm_data.pm_skutagging_kids_product_master
SET PRESCHOOL_FLAG = 'YES'
WHERE MOM_AND_KID_PRODUCT_FLAG = 1
AND COALESCE(PREG_FLAG,'N/A') = 'N/A'
AND COALESCE(INFANT_FLAG,'N/A') = 'N/A'
AND COALESCE(TODDLER_FLAG,'N/A') = 'N/A'
AND COALESCE(PRESCHOOL_FLAG,'N/A') = 'N/A'
AND COALESCE(JUNIOR_FLAG,'N/A') = 'N/A'
AND COALESCE(TEEN_FLAG,'N/A') = 'N/A'
AND REVISED_CATEGORY_LEVEL_1 = 'HOME NON-ELECTRONICS'
AND SUBCLASS_NAME IN ('ANIMAL','GAMES')
AND (PRODUCT_NAME LIKE '%���%'
     OR PRODUCT_NAME LIKE '%FISHING%'
     OR PRODUCT_NAME LIKE '%JIGSAW%'
     OR (PRODUCT_NAME LIKE '%PUZZLE%' AND PRODUCT_NAME NOT LIKE '%150%' AND PRODUCT_NAME NOT LIKE '%100%')
     OR PRODUCT_NAME LIKE '%ABC%')
;

UPDATE vpm_data.pm_skutagging_kids_product_master
SET PRESCHOOL_FLAG = 'YES'
   ,JUNIOR_FLAG = 'YES'
WHERE MOM_AND_KID_PRODUCT_FLAG = 1
AND COALESCE(PREG_FLAG,'N/A') = 'N/A'
AND COALESCE(INFANT_FLAG,'N/A') = 'N/A'
AND COALESCE(TODDLER_FLAG,'N/A') = 'N/A'
AND COALESCE(PRESCHOOL_FLAG,'N/A') = 'N/A'
AND COALESCE(JUNIOR_FLAG,'N/A') = 'N/A'
AND COALESCE(TEEN_FLAG,'N/A') = 'N/A'
AND REVISED_CATEGORY_LEVEL_1 = 'HOME NON-ELECTRONICS'
AND SUBCLASS_NAME IN ('ANIMAL','GAMES')
AND (PRODUCT_NAME LIKE '%��%'
     OR PRODUCT_NAME LIKE '%�ѹ�%'
     OR PRODUCT_NAME LIKE '%HIPPO%'
     OR PRODUCT_NAME LIKE '%�ԧ�%'
     OR PRODUCT_NAME LIKE '%MINECRAFT%'
     OR PRODUCT_NAME LIKE '%PIE FACE%'
	 OR PRODUCT_NAME LIKE '%�غ����%'
     OR PRODUCT_NAME LIKE '%TOILET TROUBLE%'
     OR PRODUCT_NAME LIKE '%��ꡫ�%'
     OR PRODUCT_NAME LIKE '%BINGO%'
     OR PRODUCT_NAME LIKE '%��Ž֡�ѡ��%'
     OR PRODUCT_NAME LIKE '%ʺ��%'
	 OR PRODUCT_NAME LIKE '%PUZZLE%'
	 OR PRODUCT_NAME LIKE '%RING UP%')
;

UPDATE vpm_data.pm_skutagging_kids_product_master
SET PRESCHOOL_FLAG = 'YES'
   ,JUNIOR_FLAG = 'YES'
   ,TEEN_FLAG = 'YES'
WHERE MOM_AND_KID_PRODUCT_FLAG = 1
AND COALESCE(PREG_FLAG,'N/A') = 'N/A'
AND COALESCE(INFANT_FLAG,'N/A') = 'N/A'
AND COALESCE(TODDLER_FLAG,'N/A') = 'N/A'
AND COALESCE(PRESCHOOL_FLAG,'N/A') = 'N/A'
AND COALESCE(JUNIOR_FLAG,'N/A') = 'N/A'
AND COALESCE(TEEN_FLAG,'N/A') = 'N/A'
AND REVISED_CATEGORY_LEVEL_1 = 'HOME NON-ELECTRONICS'
AND SUBCLASS_NAME IN ('ANIMAL','GAMES')
AND (PRODUCT_NAME LIKE '%DOMINO%'
     OR PRODUCT_NAME LIKE '%�����%'
	 OR PRODUCT_NAME LIKE '%����%'
     OR PRODUCT_NAME LIKE '%I SPY DIG IN%'
	 OR PRODUCT_NAME LIKE '%�¹��ǧ%'
	 OR PRODUCT_NAME LIKE '%TWISTER%')
;

UPDATE vpm_data.pm_skutagging_kids_product_master
SET JUNIOR_FLAG = 'YES'
   ,TEEN_FLAG = 'YES'
WHERE MOM_AND_KID_PRODUCT_FLAG = 1
AND COALESCE(PREG_FLAG,'N/A') = 'N/A'
AND COALESCE(INFANT_FLAG,'N/A') = 'N/A'
AND COALESCE(TODDLER_FLAG,'N/A') = 'N/A'
AND COALESCE(PRESCHOOL_FLAG,'N/A') = 'N/A'
AND COALESCE(JUNIOR_FLAG,'N/A') = 'N/A'
AND COALESCE(TEEN_FLAG,'N/A') = 'N/A'
AND REVISED_CATEGORY_LEVEL_1 = 'HOME NON-ELECTRONICS'
AND SUBCLASS_NAME IN ('ANIMAL','GAMES')
AND (PRODUCT_NAME LIKE '%UNO%'
     OR PRODUCT_NAME LIKE '%��ҡ%'
     OR PRODUCT_NAME LIKE '%JENGA%'
	 OR PRODUCT_NAME LIKE '%MONOPOLY%'
     OR PRODUCT_NAME LIKE '%MOUSETRAP%'
     OR PRODUCT_NAME LIKE '%CARDINAL%'
     OR PRODUCT_NAME LIKE '%IQ PUZZLE%'
     OR PRODUCT_NAME LIKE '%CHES%'
     OR PRODUCT_NAME LIKE '%IQ%'
     OR PRODUCT_NAME LIKE '%SUDOKU%'
	 OR PRODUCT_NAME LIKE '%�ٺԤ%'
	 OR PRODUCT_NAME LIKE '%RUBIC%'
	 OR PRODUCT_NAME LIKE '%RUBIK%'
     OR PRODUCT_NAME LIKE '%��⤴�%'
     OR PRODUCT_NAME LIKE '%����������ѡ��%'
	 OR PRODUCT_NAME LIKE '%GRAB AND ;%'
	 OR PRODUCT_NAME LIKE '%PINBALL%'
	 OR PRODUCT_NAME LIKE '%SCRABBLE%'
	 OR PRODUCT_NAME LIKE '%SPEED CUPS%'
	 OR PRODUCT_NAME LIKE '%STACK%'
	 OR PRODUCT_NAME LIKE '%QUAKE TOWER%'
	 OR PRODUCT_NAME LIKE '%MONOPONY%')
;

UPDATE vpm_data.pm_skutagging_kids_product_master
SET JUNIOR_FLAG = 'YES'
WHERE MOM_AND_KID_PRODUCT_FLAG = 1
AND COALESCE(PREG_FLAG,'N/A') = 'N/A'
AND COALESCE(INFANT_FLAG,'N/A') = 'N/A'
AND COALESCE(TODDLER_FLAG,'N/A') = 'N/A'
AND COALESCE(PRESCHOOL_FLAG,'N/A') = 'N/A'
AND COALESCE(JUNIOR_FLAG,'N/A') = 'N/A'
AND COALESCE(TEEN_FLAG,'N/A') = 'N/A'
AND REVISED_CATEGORY_LEVEL_1 = 'HOME NON-ELECTRONICS'
AND SUBCLASS_NAME IN ('ANIMAL','GAMES')
AND (CLEANED_BRANDNAME LIKE '%LEGO%'
     OR CLEANED_BRANDNAME LIKE '%CARDINAL%'
	 OR PRODUCT_NAME LIKE '%���ʷ%'
	 OR PRODUCT_NAME LIKE '%TUMBLIN%'
	 OR PRODUCT_NAME LIKE '%�������%')
;

UPDATE vpm_data.pm_skutagging_kids_product_master
SET PRESCHOOL_FLAG = 'YES'
WHERE MOM_AND_KID_PRODUCT_FLAG = 1
AND COALESCE(PREG_FLAG,'N/A') = 'N/A'
AND COALESCE(INFANT_FLAG,'N/A') = 'N/A'
AND COALESCE(TODDLER_FLAG,'N/A') = 'N/A'
AND COALESCE(PRESCHOOL_FLAG,'N/A') = 'N/A'
AND COALESCE(JUNIOR_FLAG,'N/A') = 'N/A'
AND COALESCE(TEEN_FLAG,'N/A') = 'N/A'
AND REVISED_CATEGORY_LEVEL_1 = 'HOME NON-ELECTRONICS'
AND SUBCLASS_NAME IN ('ANIMAL','GAMES')
;

------------ FASHION ------------
UPDATE vpm_data.pm_skutagging_kids_product_master
SET PRESCHOOL_FLAG = 'YES'
WHERE MOM_AND_KID_PRODUCT_FLAG = 1
AND COALESCE(PREG_FLAG,'N/A') = 'N/A'
AND COALESCE(INFANT_FLAG,'N/A') = 'N/A'
AND COALESCE(TODDLER_FLAG,'N/A') = 'N/A'
AND COALESCE(PRESCHOOL_FLAG,'N/A') = 'N/A'
AND COALESCE(JUNIOR_FLAG,'N/A') = 'N/A'
AND COALESCE(TEEN_FLAG,'N/A') = 'N/A'
AND REVISED_CATEGORY_LEVEL_1 = 'FASHION'
AND CLEANED_BRANDNAME NOT IN ('SANRIO','SANRIO LOCAL','THAI TIME','TSURUYA')
AND (PRODUCT_NAME LIKE '%25%'
     OR PRODUCT_NAME LIKE '%26%'
     OR PRODUCT_NAME LIKE '%27%'
     OR PRODUCT_NAME LIKE '%28%'
     OR PRODUCT_NAME LIKE '%29%'
     OR PRODUCT_NAME LIKE '%30%'
     OR PRODUCT_NAME LIKE '%31%')
;

UPDATE vpm_data.pm_skutagging_kids_product_master
SET JUNIOR_FLAG = 'YES'
WHERE MOM_AND_KID_PRODUCT_FLAG = 1
AND COALESCE(PREG_FLAG,'N/A') = 'N/A'
AND COALESCE(INFANT_FLAG,'N/A') = 'N/A'
AND COALESCE(TODDLER_FLAG,'N/A') = 'N/A'
AND COALESCE(PRESCHOOL_FLAG,'N/A') = 'N/A'
AND COALESCE(JUNIOR_FLAG,'N/A') = 'N/A'
AND COALESCE(TEEN_FLAG,'N/A') = 'N/A'
AND REVISED_CATEGORY_LEVEL_1 = 'FASHION'
AND CLEANED_BRANDNAME NOT IN ('SANRIO','SANRIO LOCAL','THAI TIME','TSURUYA')
AND (PRODUCT_NAME LIKE '%32%'
     OR PRODUCT_NAME LIKE '%33%'
     OR PRODUCT_NAME LIKE '%34%'
     OR PRODUCT_NAME LIKE '%35%'
     OR PRODUCT_NAME LIKE '%36%')
;

UPDATE vpm_data.pm_skutagging_kids_product_master
SET TEEN_FLAG = 'YES'
WHERE MOM_AND_KID_PRODUCT_FLAG = 1
AND COALESCE(PREG_FLAG,'N/A') = 'N/A'
AND COALESCE(INFANT_FLAG,'N/A') = 'N/A'
AND COALESCE(TODDLER_FLAG,'N/A') = 'N/A'
AND COALESCE(PRESCHOOL_FLAG,'N/A') = 'N/A'
AND COALESCE(JUNIOR_FLAG,'N/A') = 'N/A'
AND COALESCE(TEEN_FLAG,'N/A') = 'N/A'
AND REVISED_CATEGORY_LEVEL_1 = 'FASHION'
AND CLEANED_BRANDNAME NOT IN ('SANRIO','SANRIO LOCAL','THAI TIME','TSURUYA')
AND (PRODUCT_NAME LIKE '%37%'
     OR PRODUCT_NAME LIKE '%38%'
     OR PRODUCT_NAME LIKE '%39%'
     OR PRODUCT_NAME LIKE '%40%'
     OR PRODUCT_NAME LIKE '%41%'
     OR PRODUCT_NAME LIKE '%42%')
;

UPDATE vpm_data.pm_skutagging_kids_product_master
SET MOM_AND_KID_PRODUCT_FLAG = 0
WHERE MOM_AND_KID_PRODUCT_FLAG = 1
AND COALESCE(PREG_FLAG,'N/A') = 'N/A'
AND COALESCE(INFANT_FLAG,'N/A') = 'N/A'
AND COALESCE(TODDLER_FLAG,'N/A') = 'N/A'
AND COALESCE(PRESCHOOL_FLAG,'N/A') = 'N/A'
AND COALESCE(JUNIOR_FLAG,'N/A') = 'N/A'
AND COALESCE(TEEN_FLAG,'N/A') = 'N/A'
AND REVISED_CATEGORY_LEVEL_1 = 'FASHION'
AND CLEANED_BRANDNAME IN ('SANRIO','SANRIO LOCAL','THAI TIME','TSURUYA')
;

------------ SPORT ------------
UPDATE vpm_data.pm_skutagging_kids_product_master
SET JUNIOR_FLAG = 'YES'
   ,TEEN_FLAG = 'YES'
WHERE MOM_AND_KID_PRODUCT_FLAG = 1
AND COALESCE(PREG_FLAG,'N/A') = 'N/A'
AND COALESCE(INFANT_FLAG,'N/A') = 'N/A'
AND COALESCE(TODDLER_FLAG,'N/A') = 'N/A'
AND COALESCE(PRESCHOOL_FLAG,'N/A') = 'N/A'
AND COALESCE(JUNIOR_FLAG,'N/A') = 'N/A'
AND COALESCE(TEEN_FLAG,'N/A') = 'N/A'
AND REVISED_CATEGORY_LEVEL_1 = 'SPORT'
;

------------ KID FURNITURE ------------
--AGE_RANGE 
UPDATE vpm_data.pm_skutagging_kids_product_master
SET PRESCHOOL_FLAG = 'YES'
WHERE MOM_AND_KID_PRODUCT_FLAG = 1
AND COALESCE(PREG_FLAG,'N/A') = 'N/A'
AND COALESCE(INFANT_FLAG,'N/A') = 'N/A'
AND COALESCE(TODDLER_FLAG,'N/A') = 'N/A'
AND COALESCE(PRESCHOOL_FLAG,'N/A') = 'N/A'
AND COALESCE(JUNIOR_FLAG,'N/A') = 'N/A'
AND COALESCE(TEEN_FLAG,'N/A') = 'N/A'
AND REVISED_CATEGORY_LEVEL_2 = 'TOYS'
AND AGE_RANGE  = '3 TO 6 YEARS'
;

UPDATE vpm_data.pm_skutagging_kids_product_master
SET PRESCHOOL_FLAG = 'YES'
   ,JUNIOR_FLAG = 'YES'
WHERE MOM_AND_KID_PRODUCT_FLAG = 1
AND COALESCE(PREG_FLAG,'N/A') = 'N/A'
AND COALESCE(INFANT_FLAG,'N/A') = 'N/A'
AND COALESCE(TODDLER_FLAG,'N/A') = 'N/A'
AND COALESCE(PRESCHOOL_FLAG,'N/A') = 'N/A'
AND COALESCE(JUNIOR_FLAG,'N/A') = 'N/A'
AND COALESCE(TEEN_FLAG,'N/A') = 'N/A'
AND REVISED_CATEGORY_LEVEL_2 = 'TOYS'
AND (AGE_RANGE  = '3 TO 9 YEARS'
	 OR AGE_RANGE  = '5 TO 9 YEARS'
	 OR AGE_RANGE  = '6 TO 9 YEARS')
;

UPDATE vpm_data.pm_skutagging_kids_product_master
SET JUNIOR_FLAG = 'YES'
WHERE MOM_AND_KID_PRODUCT_FLAG = 1
AND COALESCE(PREG_FLAG,'N/A') = 'N/A'
AND COALESCE(INFANT_FLAG,'N/A') = 'N/A'
AND COALESCE(TODDLER_FLAG,'N/A') = 'N/A'
AND COALESCE(PRESCHOOL_FLAG,'N/A') = 'N/A'
AND COALESCE(JUNIOR_FLAG,'N/A') = 'N/A'
AND COALESCE(TEEN_FLAG,'N/A') = 'N/A'
AND REVISED_CATEGORY_LEVEL_2 = 'TOYS'
AND (AGE_RANGE  = '6 TO 12 YEARS')
;

--FROM HOME NON-ELECTRONICS
UPDATE vpm_data.pm_skutagging_kids_product_master
SET PRESCHOOL_FLAG = 'YES'
WHERE MOM_AND_KID_PRODUCT_FLAG = 1
AND COALESCE(PREG_FLAG,'N/A') = 'N/A'
AND COALESCE(INFANT_FLAG,'N/A') = 'N/A'
AND COALESCE(TODDLER_FLAG,'N/A') = 'N/A'
AND COALESCE(PRESCHOOL_FLAG,'N/A') = 'N/A'
AND COALESCE(JUNIOR_FLAG,'N/A') = 'N/A'
AND COALESCE(TEEN_FLAG,'N/A') = 'N/A'
AND REVISED_CATEGORY_LEVEL_2 = 'TOYS'
AND (PRODUCT_NAME LIKE '%���%'
     OR PRODUCT_NAME LIKE '%FISHING%'
     OR PRODUCT_NAME LIKE '%JIGSAW%'
     OR (PRODUCT_NAME LIKE '%PUZZLE%' AND PRODUCT_NAME NOT LIKE '%150%' AND PRODUCT_NAME NOT LIKE '%100%')
     OR PRODUCT_NAME LIKE '%ABC%')
;

UPDATE vpm_data.pm_skutagging_kids_product_master
SET PRESCHOOL_FLAG = 'YES'
   ,JUNIOR_FLAG = 'YES'
WHERE MOM_AND_KID_PRODUCT_FLAG = 1
AND COALESCE(PREG_FLAG,'N/A') = 'N/A'
AND COALESCE(INFANT_FLAG,'N/A') = 'N/A'
AND COALESCE(TODDLER_FLAG,'N/A') = 'N/A'
AND COALESCE(PRESCHOOL_FLAG,'N/A') = 'N/A'
AND COALESCE(JUNIOR_FLAG,'N/A') = 'N/A'
AND COALESCE(TEEN_FLAG,'N/A') = 'N/A'
AND REVISED_CATEGORY_LEVEL_2 = 'TOYS'
AND (PRODUCT_NAME LIKE '%��%'
     OR PRODUCT_NAME LIKE '%�ѹ�%'
     OR PRODUCT_NAME LIKE '%HIPPO%'
     OR PRODUCT_NAME LIKE '%�ԧ�%'
     OR PRODUCT_NAME LIKE '%MINECRAFT%'
     OR PRODUCT_NAME LIKE '%PIE FACE%'
	 OR PRODUCT_NAME LIKE '%�غ����%'
     OR PRODUCT_NAME LIKE '%TOILET TROUBLE%'
     OR PRODUCT_NAME LIKE '%��ꡫ�%'
     OR PRODUCT_NAME LIKE '%BINGO%'
     OR PRODUCT_NAME LIKE '%��Ž֡�ѡ��%'
     OR PRODUCT_NAME LIKE '%ʺ��%'
	 OR PRODUCT_NAME LIKE '%PUZZLE%'
	 OR PRODUCT_NAME LIKE '%RING UP%')
;

UPDATE vpm_data.pm_skutagging_kids_product_master
SET PRESCHOOL_FLAG = 'YES'
   ,JUNIOR_FLAG = 'YES'
   ,TEEN_FLAG = 'YES'
WHERE MOM_AND_KID_PRODUCT_FLAG = 1
AND COALESCE(PREG_FLAG,'N/A') = 'N/A'
AND COALESCE(INFANT_FLAG,'N/A') = 'N/A'
AND COALESCE(TODDLER_FLAG,'N/A') = 'N/A'
AND COALESCE(PRESCHOOL_FLAG,'N/A') = 'N/A'
AND COALESCE(JUNIOR_FLAG,'N/A') = 'N/A'
AND COALESCE(TEEN_FLAG,'N/A') = 'N/A'
AND REVISED_CATEGORY_LEVEL_2 = 'TOYS'
AND (PRODUCT_NAME LIKE '%DOMINO%'
     OR PRODUCT_NAME LIKE '%�����%'
	 OR PRODUCT_NAME LIKE '%����%'
     OR PRODUCT_NAME LIKE '%I SPY DIG IN%'
	 OR PRODUCT_NAME LIKE '%�¹��ǧ%'
	 OR PRODUCT_NAME LIKE '%TWISTER%')
;

UPDATE vpm_data.pm_skutagging_kids_product_master
SET JUNIOR_FLAG = 'YES'
   ,TEEN_FLAG = 'YES'
WHERE MOM_AND_KID_PRODUCT_FLAG = 1
AND COALESCE(PREG_FLAG,'N/A') = 'N/A'
AND COALESCE(INFANT_FLAG,'N/A') = 'N/A'
AND COALESCE(TODDLER_FLAG,'N/A') = 'N/A'
AND COALESCE(PRESCHOOL_FLAG,'N/A') = 'N/A'
AND COALESCE(JUNIOR_FLAG,'N/A') = 'N/A'
AND COALESCE(TEEN_FLAG,'N/A') = 'N/A'
AND REVISED_CATEGORY_LEVEL_2 = 'TOYS'
AND (PRODUCT_NAME LIKE '%UNO%'
     OR PRODUCT_NAME LIKE '%��ҡ%'
     OR PRODUCT_NAME LIKE '%JENGA%'
	 OR PRODUCT_NAME LIKE '%MONOPOLY%'
     OR PRODUCT_NAME LIKE '%MOUSETRAP%'
     OR PRODUCT_NAME LIKE '%CARDINAL%'
     OR PRODUCT_NAME LIKE '%IQ PUZZLE%'
     OR PRODUCT_NAME LIKE '%CHES%'
     OR PRODUCT_NAME LIKE '%IQ%'
     OR PRODUCT_NAME LIKE '%SUDOKU%'
	 OR PRODUCT_NAME LIKE '%�ٺԤ%'
	 OR PRODUCT_NAME LIKE '%RUBIC%'
	 OR PRODUCT_NAME LIKE '%RUBIK%'
     OR PRODUCT_NAME LIKE '%��⤴�%'
     OR PRODUCT_NAME LIKE '%����������ѡ��%'
	 OR PRODUCT_NAME LIKE '%GRAB AND ;%'
	 OR PRODUCT_NAME LIKE '%PINBALL%'
	 OR PRODUCT_NAME LIKE '%SCRABBLE%'
	 OR PRODUCT_NAME LIKE '%SPEED CUPS%'
	 OR PRODUCT_NAME LIKE '%STACK%'
	 OR PRODUCT_NAME LIKE '%QUAKE TOWER%'
	 OR PRODUCT_NAME LIKE '%MONOPONY%')
;

UPDATE vpm_data.pm_skutagging_kids_product_master
SET JUNIOR_FLAG = 'YES'
WHERE MOM_AND_KID_PRODUCT_FLAG = 1
AND COALESCE(PREG_FLAG,'N/A') = 'N/A'
AND COALESCE(INFANT_FLAG,'N/A') = 'N/A'
AND COALESCE(TODDLER_FLAG,'N/A') = 'N/A'
AND COALESCE(PRESCHOOL_FLAG,'N/A') = 'N/A'
AND COALESCE(JUNIOR_FLAG,'N/A') = 'N/A'
AND COALESCE(TEEN_FLAG,'N/A') = 'N/A'
AND REVISED_CATEGORY_LEVEL_2 = 'TOYS'
AND (CLEANED_BRANDNAME LIKE '%LEGO%'
     OR CLEANED_BRANDNAME LIKE '%CARDINAL%'
	 OR PRODUCT_NAME LIKE '%���ʷ%'
	 OR PRODUCT_NAME LIKE '%TUMBLIN%'
	 OR PRODUCT_NAME LIKE '%�������%')
;

UPDATE vpm_data.pm_skutagging_kids_product_master
SET PRESCHOOL_FLAG = 'YES'
WHERE MOM_AND_KID_PRODUCT_FLAG = 1
AND COALESCE(PREG_FLAG,'N/A') = 'N/A'
AND COALESCE(INFANT_FLAG,'N/A') = 'N/A'
AND COALESCE(TODDLER_FLAG,'N/A') = 'N/A'
AND COALESCE(PRESCHOOL_FLAG,'N/A') = 'N/A'
AND COALESCE(JUNIOR_FLAG,'N/A') = 'N/A'
AND COALESCE(TEEN_FLAG,'N/A') = 'N/A'
AND REVISED_CATEGORY_LEVEL_2 = 'TOYS'
;

--START CLEANING HERE
UPDATE vpm_data.pm_skutagging_kids_product_master
SET PRESCHOOL_FLAG = 'YES'
   ,JUNIOR_FLAG = 'YES'
   ,TEEN_FLAG = 'YES'
WHERE MOM_AND_KID_PRODUCT_FLAG = 1
AND COALESCE(PREG_FLAG,'N/A') = 'N/A'
AND COALESCE(INFANT_FLAG,'N/A') = 'N/A'
AND COALESCE(TODDLER_FLAG,'N/A') = 'N/A'
AND COALESCE(PRESCHOOL_FLAG,'N/A') = 'N/A'
AND COALESCE(JUNIOR_FLAG,'N/A') = 'N/A'
AND COALESCE(TEEN_FLAG,'N/A') = 'N/A'
AND REVISED_CATEGORY_LEVEL_2 = 'TOYS'
AND (SUBDEPT_NAME = 'BUILD-A-BEAR WORKSHOP' OR PRODUCT_NAME LIKE '%��꡵�%')
;

UPDATE vpm_data.pm_skutagging_kids_product_master
SET PRESCHOOL_FLAG = 'YES'
   ,JUNIOR_FLAG = 'YES'
WHERE MOM_AND_KID_PRODUCT_FLAG = 1
AND COALESCE(PREG_FLAG,'N/A') = 'N/A'
AND COALESCE(INFANT_FLAG,'N/A') = 'N/A'
AND COALESCE(TODDLER_FLAG,'N/A') = 'N/A'
AND COALESCE(PRESCHOOL_FLAG,'N/A') = 'N/A'
AND COALESCE(JUNIOR_FLAG,'N/A') = 'N/A'
AND COALESCE(TEEN_FLAG,'N/A') = 'N/A'
AND REVISED_CATEGORY_LEVEL_2 = 'TOYS'
AND (SUBDEPT_NAME = 'GM/NON FMCG'
     OR SUBDEPT_NAME = 'EDUCATION TOY' 
	 OR (SUBDEPT_NAME = 'GAME & BUILDING' AND SUBCLASS_NAME NOT IN ('BOARD GAME','CARD GAME'))	     
	 OR (SUBDEPT_NAME = 'TOYS' AND SUBCLASS_NAME != 'SPORT'))
;

UPDATE vpm_data.pm_skutagging_kids_product_master
SET PRESCHOOL_FLAG = NULL
   ,JUNIOR_FLAG = 'YES'
   ,TEEN_FLAG = 'YES'
WHERE MOM_AND_KID_PRODUCT_FLAG = 1
AND COALESCE(PREG_FLAG,'N/A') = 'N/A'
AND COALESCE(INFANT_FLAG,'N/A') = 'N/A'
AND COALESCE(TODDLER_FLAG,'N/A') = 'N/A'
AND COALESCE(PRESCHOOL_FLAG,'N/A') = 'N/A'
AND COALESCE(JUNIOR_FLAG,'N/A') = 'N/A'
AND COALESCE(TEEN_FLAG,'N/A') = 'N/A'
AND REVISED_CATEGORY_LEVEL_2 = 'TOYS'
AND SUBDEPT_NAME = 'GAME & BUILDING' AND SUBCLASS_NAME = 'BOARD GAME'
;

UPDATE vpm_data.pm_skutagging_kids_product_master
SET PRESCHOOL_FLAG = NULL 
   ,JUNIOR_FLAG = 'YES'
WHERE MOM_AND_KID_PRODUCT_FLAG = 1
AND COALESCE(PREG_FLAG,'N/A') = 'N/A'
AND COALESCE(INFANT_FLAG,'N/A') = 'N/A'
AND COALESCE(TODDLER_FLAG,'N/A') = 'N/A'
AND COALESCE(PRESCHOOL_FLAG,'N/A') = 'N/A'
AND COALESCE(JUNIOR_FLAG,'N/A') = 'N/A'
AND COALESCE(TEEN_FLAG,'N/A') = 'N/A'
AND REVISED_CATEGORY_LEVEL_2 = 'TOYS'
AND SUBDEPT_NAME = 'GAME & BUILDING' AND SUBCLASS_NAME = 'CARD GAME'
;

UPDATE vpm_data.pm_skutagging_kids_product_master
SET PRESCHOOL_FLAG = 'YES'
WHERE MOM_AND_KID_PRODUCT_FLAG = 1
AND COALESCE(PREG_FLAG,'N/A') = 'N/A'
AND COALESCE(INFANT_FLAG,'N/A') = 'N/A'
AND COALESCE(TODDLER_FLAG,'N/A') = 'N/A'
AND COALESCE(PRESCHOOL_FLAG,'N/A') = 'N/A'
AND COALESCE(JUNIOR_FLAG,'N/A') = 'N/A'
AND COALESCE(TEEN_FLAG,'N/A') = 'N/A'
AND REVISED_CATEGORY_LEVEL_2 = 'TOYS'
AND SUBDEPT_NAME = 'TOYS' AND SUBCLASS_NAME = 'SPORT'
AND (PRODUCT_NAME LIKE '%��ǧ��%'
     OR PRODUCT_NAME LIKE '%������ᢹ%'
	 OR PRODUCT_NAME LIKE '%��͡ᢹ%')
;

UPDATE vpm_data.pm_skutagging_kids_product_master
SET TEEN_FLAG = 'YES'
WHERE MOM_AND_KID_PRODUCT_FLAG = 1
AND COALESCE(PREG_FLAG,'N/A') = 'N/A'
AND COALESCE(INFANT_FLAG,'N/A') = 'N/A'
AND COALESCE(TODDLER_FLAG,'N/A') = 'N/A'
AND COALESCE(PRESCHOOL_FLAG,'N/A') = 'N/A'
AND COALESCE(JUNIOR_FLAG,'N/A') = 'N/A'
AND COALESCE(TEEN_FLAG,'N/A') = 'N/A'
AND REVISED_CATEGORY_LEVEL_2 = 'TOYS'
AND SUBDEPT_NAME = 'TOYS' AND SUBCLASS_NAME = 'SPORT'
AND (PRODUCT_NAME LIKE '%�%'
     OR PRODUCT_NAME LIKE '%��蹵�%'
	 OR PRODUCT_NAME LIKE '%��ǧ�ҧ%'
     OR PRODUCT_NAME LIKE '%���;��%'
     OR PRODUCT_NAME LIKE '%�ٺ%')
;

UPDATE vpm_data.pm_skutagging_kids_product_master
SET PRESCHOOL_FLAG = 'YES'
   ,JUNIOR_FLAG = 'YES'
WHERE MOM_AND_KID_PRODUCT_FLAG = 1
AND COALESCE(PREG_FLAG,'N/A') = 'N/A'
AND COALESCE(INFANT_FLAG,'N/A') = 'N/A'
AND COALESCE(TODDLER_FLAG,'N/A') = 'N/A'
AND COALESCE(PRESCHOOL_FLAG,'N/A') = 'N/A'
AND COALESCE(JUNIOR_FLAG,'N/A') = 'N/A'
AND COALESCE(TEEN_FLAG,'N/A') = 'N/A'
AND REVISED_CATEGORY_LEVEL_2 = 'TOYS'
AND SUBDEPT_NAME = 'TOYS' AND SUBCLASS_NAME = 'SPORT'
;

------------ BOOK AND STATIONERY ------------
--STATIONERY
UPDATE vpm_data.pm_skutagging_kids_product_master
SET JUNIOR_FLAG = 'YES'
   ,TEEN_FLAG = 'YES'
WHERE MOM_AND_KID_PRODUCT_FLAG = 1
AND COALESCE(PREG_FLAG,'N/A') = 'N/A'
AND COALESCE(INFANT_FLAG,'N/A') = 'N/A'
AND COALESCE(TODDLER_FLAG,'N/A') = 'N/A'
AND COALESCE(PRESCHOOL_FLAG,'N/A') = 'N/A'
AND COALESCE(JUNIOR_FLAG,'N/A') = 'N/A'
AND COALESCE(TEEN_FLAG,'N/A') = 'N/A'
AND REVISED_CATEGORY_LEVEL_2 = 'STATIONERY'
;

--BOOKS
UPDATE vpm_data.pm_skutagging_kids_product_master --343
SET PRESCHOOL_FLAG = 'YES'
WHERE MOM_AND_KID_PRODUCT_FLAG = 1
AND COALESCE(PREG_FLAG,'N/A') = 'N/A'
AND COALESCE(INFANT_FLAG,'N/A') = 'N/A'
AND COALESCE(TODDLER_FLAG,'N/A') = 'N/A'
AND COALESCE(PRESCHOOL_FLAG,'N/A') = 'N/A'
AND COALESCE(JUNIOR_FLAG,'N/A') = 'N/A'
AND COALESCE(TEEN_FLAG,'N/A') = 'N/A'
AND REVISED_CATEGORY_LEVEL_3 = 'CHILDREN BOOKS'
AND (AGE_RANGE  = '3 TO 5 YEARS' 
     OR AGE_RANGE  = '3 TO 6 YEARS'
     OR AGE_RANGE  = '3 TO 9 YEARS'
     OR AGE_RANGE  = '4 TO 8 YEARS'
	 OR CLASS_NAME LIKE '%AGES 4-8%'
	 OR CLASS_NAME LIKE '%BABY - PRESCHOOL%'
	 OR CLASS_NAME LIKE '%KINDERGARDEN%'
	 OR CLASS_NAME LIKE '%PRE-SCHOOL%')
;

UPDATE vpm_data.pm_skutagging_kids_product_master --6881
SET JUNIOR_FLAG = 'YES'
WHERE MOM_AND_KID_PRODUCT_FLAG = 1
AND COALESCE(PREG_FLAG,'N/A') = 'N/A'
AND COALESCE(INFANT_FLAG,'N/A') = 'N/A'
AND COALESCE(TODDLER_FLAG,'N/A') = 'N/A'
AND COALESCE(PRESCHOOL_FLAG,'N/A') = 'N/A'
AND COALESCE(JUNIOR_FLAG,'N/A') = 'N/A'
AND COALESCE(TEEN_FLAG,'N/A') = 'N/A'
AND REVISED_CATEGORY_LEVEL_3 = 'CHILDREN BOOKS'
AND (AGE_RANGE  = '3 TO 9 YEARS' 
     OR AGE_RANGE  = '4 TO 8 YEARS'
     OR AGE_RANGE  = '6 TO 9 YEARS'
     OR AGE_RANGE  = '6 TO 12 YEARS'
     OR AGE_RANGE  = '9 TO 12 YEARS'
	 OR AGE_RANGE  = '12 TO 13 YEARS'
	 OR AGE_RANGE  = '12 TO 15 YEARS'
	 OR AGE_RANGE  = '12 YEARS AND OLDER'
	 OR CLASS_NAME LIKE '%AGES 4-8%'
	 OR CLASS_NAME LIKE '%KINDERGARDEN - PRIMARY%'
	 OR CLASS_NAME LIKE '%PRIMARY SCHOOL%'
	 OR CLASS_NAME LIKE '%AGES 9-12%')
;

UPDATE vpm_data.pm_skutagging_kids_product_master --6599
SET TEEN_FLAG = 'YES'
WHERE MOM_AND_KID_PRODUCT_FLAG = 1
AND COALESCE(PREG_FLAG,'N/A') = 'N/A'
AND COALESCE(INFANT_FLAG,'N/A') = 'N/A'
AND COALESCE(TODDLER_FLAG,'N/A') = 'N/A'
AND COALESCE(PRESCHOOL_FLAG,'N/A') = 'N/A'
AND COALESCE(JUNIOR_FLAG,'N/A') = 'N/A'
AND COALESCE(TEEN_FLAG,'N/A') = 'N/A'
AND REVISED_CATEGORY_LEVEL_3 = 'CHILDREN BOOKS'
AND (AGE_RANGE  = '12 TO 15 YEARS'
	 OR AGE_RANGE  = '12 YEARS AND OLDER'
	 OR AGE_RANGE  = '15 TO 18 YEARS'
	 OR CLASS_NAME LIKE '%JUNIOR HIGH SCHOOL(M1-3)%'
	 OR CLASS_NAME LIKE '%SENIOR HIGH SCHOOL(M4-6)%'
	 OR SUBDEPT_NAME LIKE '%TEENAGE%')
;

UPDATE vpm_data.pm_skutagging_kids_product_master --2585
SET PRESCHOOL_FLAG = 'YES'
WHERE MOM_AND_KID_PRODUCT_FLAG = 1
AND COALESCE(PREG_FLAG,'N/A') = 'N/A'
AND COALESCE(INFANT_FLAG,'N/A') = 'N/A'
AND COALESCE(TODDLER_FLAG,'N/A') = 'N/A'
AND COALESCE(PRESCHOOL_FLAG,'N/A') = 'N/A'
AND COALESCE(JUNIOR_FLAG,'N/A') = 'N/A'
AND COALESCE(TEEN_FLAG,'N/A') = 'N/A'
AND REVISED_CATEGORY_LEVEL_3 = 'CHILDREN BOOKS'
AND (PRODUCT_NAME LIKE '%˹ٹԴ%'
     OR PRODUCT_NAME LIKE '%�ҡ���%'
	 OR PRODUCT_NAME LIKE '%�� �� ��%'
	 OR PRODUCT_NAME LIKE '%�.%'
	 OR PRODUCT_NAME LIKE '%� ��%'
	 OR PRODUCT_NAME LIKE '%��맡��%'
	 OR PRODUCT_NAME LIKE '%�-�%'
	 OR (PRODUCT_NAME LIKE '%�ͺ���%' AND PRODUCT_NAME LIKE '%�.%')
	 OR PRODUCT_NAME LIKE '%ABC%'
	 OR PRODUCT_NAME LIKE '%���%'
	 OR PRODUCT_NAME LIKE '%A-Z%'
	 OR PRODUCT_NAME LIKE '%�ѵ�%'
	 OR PRODUCT_NAME LIKE '%��ѭ���%'
	 OR PRODUCT_NAME LIKE '%�ͺ���%�%'
	 OR PRODUCT_NAME LIKE '%CARD%'
	 OR PRODUCT_NAME LIKE '%ALPHABETS%'
	 OR PRODUCT_NAME LIKE '%3 �Ǻ%'
	 OR PRODUCT_NAME LIKE '%4 �Ǻ%'
	 OR PRODUCT_NAME LIKE '%5 �Ǻ%'
	 OR PRODUCT_NAME LIKE '%6 �Ǻ%'
	 OR PRODUCT_NAME LIKE '%5-7 �Ǻ%'
	 OR PRODUCT_NAME LIKE '%͹غ��%'
	 OR PRODUCT_NAME LIKE '%�ѡ����¹��%'
	 OR PRODUCT_NAME LIKE '%4 ��%'
	 OR PRODUCT_NAME LIKE '%4��%'
	 OR PRODUCT_NAME LIKE '%5 ��%'
	 OR PRODUCT_NAME LIKE '%5��%'
	 OR PRODUCT_NAME LIKE '%6 ��%'
	 OR PRODUCT_NAME LIKE '%6��%'
	 OR PRODUCT_NAME LIKE '%123%'
	 OR PRODUCT_NAME LIKE '%���%'
	 OR PRODUCT_NAME LIKE '%������%')
;

UPDATE vpm_data.pm_skutagging_kids_product_master --434
SET JUNIOR_FLAG = 'YES'
WHERE MOM_AND_KID_PRODUCT_FLAG = 1
AND COALESCE(PREG_FLAG,'N/A') = 'N/A'
AND COALESCE(INFANT_FLAG,'N/A') = 'N/A'
AND COALESCE(TODDLER_FLAG,'N/A') = 'N/A'
AND COALESCE(PRESCHOOL_FLAG,'N/A') = 'N/A'
AND COALESCE(JUNIOR_FLAG,'N/A') = 'N/A'
AND COALESCE(TEEN_FLAG,'N/A') = 'N/A'
AND REVISED_CATEGORY_LEVEL_3 = 'CHILDREN BOOKS'
AND (PRODUCT_NAME LIKE '%�.%'
     OR PRODUCT_NAME LIKE '%��ж�%'
	 OR (PRODUCT_NAME LIKE '%�ͺ���%' AND PRODUCT_NAME LIKE '%�.1%')
	 OR PRODUCT_NAME LIKE '%7 ��%'
	 OR PRODUCT_NAME LIKE '%�ǡ%'
	 OR PRODUCT_NAME LIKE '%ź%'
	 OR PRODUCT_NAME LIKE '%�ٳ%'
	 OR PRODUCT_NAME LIKE '%������%')
;

UPDATE vpm_data.pm_skutagging_kids_product_master --155
SET TEEN_FLAG = 'YES'
WHERE MOM_AND_KID_PRODUCT_FLAG = 1
AND COALESCE(PREG_FLAG,'N/A') = 'N/A'
AND COALESCE(INFANT_FLAG,'N/A') = 'N/A'
AND COALESCE(TODDLER_FLAG,'N/A') = 'N/A'
AND COALESCE(PRESCHOOL_FLAG,'N/A') = 'N/A'
AND COALESCE(JUNIOR_FLAG,'N/A') = 'N/A'
AND COALESCE(TEEN_FLAG,'N/A') = 'N/A'
AND REVISED_CATEGORY_LEVEL_3 = 'CHILDREN BOOKS'
AND (PRODUCT_NAME LIKE '%BIOLOGY%'
     OR PRODUCT_NAME LIKE '%�Ѹ��%'
	 OR PRODUCT_NAME LIKE '%�.����%'
	 OR PRODUCT_NAME LIKE '%�.��%'
	 OR PRODUCT_NAME LIKE '%���ԡ%'
	 OR PRODUCT_NAME LIKE '%����Է��%'
	 OR PRODUCT_NAME LIKE '%�.%'
	 OR PRODUCT_NAME LIKE '%O-NET%'
	 OR PRODUCT_NAME LIKE '%A-NET%'
	 OR PRODUCT_NAME LIKE '%���%'
	 OR PRODUCT_NAME LIKE '%GAT2%'
	 OR PRODUCT_NAME LIKE '%9 �Ԫ����ѭ%'
	 OR PRODUCT_NAME LIKE '%TCAS%'
	 OR PRODUCT_NAME LIKE '%�ѭ��%'
	 OR PRODUCT_NAME LIKE '%������%'
	 OR PRODUCT_NAME LIKE '%��Դ�%'
	 OR PRODUCT_NAME LIKE '%GRAMMAR%'
	 )
;

UPDATE vpm_data.pm_skutagging_kids_product_master --12037
SET PRESCHOOL_FLAG = 'YES'
   ,JUNIOR_FLAG = 'YES'
WHERE MOM_AND_KID_PRODUCT_FLAG = 1
AND COALESCE(PREG_FLAG,'N/A') = 'N/A'
AND COALESCE(INFANT_FLAG,'N/A') = 'N/A'
AND COALESCE(TODDLER_FLAG,'N/A') = 'N/A'
AND COALESCE(PRESCHOOL_FLAG,'N/A') = 'N/A'
AND COALESCE(JUNIOR_FLAG,'N/A') = 'N/A'
AND COALESCE(TEEN_FLAG,'N/A') = 'N/A'
AND REVISED_CATEGORY_LEVEL_3 = 'CHILDREN BOOKS'
;

------------ CLEAN FASHION FROM TODDLER ------------

UPDATE vpm_data.pm_skutagging_kids_product_master
SET PRESCHOOL_KEYWORD = CASE WHEN PRODUCT_NAME LIKE '%5Y%' THEN '5Y'
                             WHEN PRODUCT_NAME LIKE '%6Y%' THEN '6Y'
							 WHEN PRODUCT_NAME LIKE '%6-7Y%' THEN '6-7Y'
							 ELSE PRESCHOOL_KEYWORD END
WHERE MOM_AND_KID_PRODUCT_FLAG = 1
AND PRESCHOOL_FLAG = 'YES'
;

UPDATE vpm_data.pm_skutagging_kids_product_master
SET JUNIOR_KEYWORD = CASE WHEN PRODUCT_NAME LIKE '%6-7Y%' THEN '6-7Y'
                          WHEN PRODUCT_NAME LIKE '%7-8Y%' THEN '7-8Y'
                          WHEN PRODUCT_NAME LIKE '%8Y%' THEN '8Y'
						  WHEN PRODUCT_NAME LIKE '%8-9Y%'THEN '8-9Y'
						  WHEN PRODUCT_NAME LIKE '%10Y%' THEN '10Y'
						  WHEN PRODUCT_NAME LIKE '%11-12Y%' THEN '11-12Y'
						  ELSE JUNIOR_KEYWORD END
WHERE MOM_AND_KID_PRODUCT_FLAG = 1
AND JUNIOR_FLAG = 'YES'
;

-------------------------------- CLEAN BY REVISED_CATEGORY_LEVEL_1 --------------------------------

UPDATE vpm_data.pm_skutagging_kids_product_master
SET PRESCHOOL_KEYWORD = --NULL & OTHER
                     CASE WHEN PRODUCT_NAME LIKE '%���%' THEN '���'
					      WHEN PRODUCT_NAME LIKE '%�%' THEN '�'
                          WHEN PRODUCT_NAME LIKE '%�������%' THEN '�������'
						  WHEN PRODUCT_NAME LIKE '%��ǧ�ҧ%' THEN '��ǧ�ҧ' 
						  WHEN PRODUCT_NAME LIKE '%����ͧ�Թ%' THEN '����ͧ�Թ'
						  WHEN PRODUCT_NAME LIKE '%�����%' THEN '�����'
						  WHEN PRODUCT_NAME LIKE '%���Ԥͻ��%' THEN 'HELICOPTER'
						  WHEN PRODUCT_NAME LIKE '%ö�%' THEN 'ö�'
						  WHEN PRODUCT_NAME LIKE '%ʡ������%' THEN 'ʡ������'
						  WHEN PRODUCT_NAME LIKE '%���%' THEN '���'
						  WHEN PRODUCT_NAME LIKE '%��蹹��%' THEN '��蹹��'
						  WHEN PRODUCT_NAME LIKE '%�ǧ�ҧ%' THEN '��ǧ�ҧ'
						  WHEN PRODUCT_NAME LIKE '%�ԧ���%' THEN '�ԧ���'
						  WHEN PRODUCT_NAME LIKE '%ö�ҡ%' THEN 'ö�ҡ'
						  WHEN PRODUCT_NAME LIKE '%�ͧ���%' THEN '�ͧ���'
						  WHEN PRODUCT_NAME LIKE '%ö�ҡ%' THEN 'ö�ҡ'
						  WHEN PRODUCT_NAME LIKE '%�����ҧ%' THEN '�����ҧ'
						  WHEN PRODUCT_NAME LIKE '%ʹѺ%' THEN 'ʹѺ'
						  WHEN PRODUCT_NAME LIKE '%��ҹ���%' THEN '��ҹ���'
						  WHEN PRODUCT_NAME LIKE '%���%' THEN '���'
						  WHEN PRODUCT_NAME LIKE '%��ҹ%' THEN '��ҹ'
						  WHEN PRODUCT_NAME LIKE '%GUN%' THEN 'GUN'
						  --HOME ELECTRONICS
						  WHEN PRODUCT_NAME LIKE '%ö������ҧ%' THEN 'ö������ҧ'
						  WHEN PRODUCT_NAME LIKE '%�׹%' THEN '�׹'
						  WHEN PRODUCT_NAME LIKE '%����%' THEN 'MODEL'
						  WHEN PRODUCT_NAME LIKE '%�Һ%' THEN '�Һ'
						  WHEN PRODUCT_NAME LIKE '%�ѧ�ѹ%' THEN '�ѧ�ѹ'
						  WHEN PRODUCT_NAME LIKE '%�ѹ����%' THEN 'GUNDUM'
						  WHEN PRODUCT_NAME LIKE '%˹�ҡҡ%' THEN '˹�ҡҡ'
						  WHEN PRODUCT_NAME LIKE '%ö��%' THEN 'ö��'
						  WHEN PRODUCT_NAME LIKE '%����%' THEN '�Է��'
						  WHEN PRODUCT_NAME LIKE '%�Է��%' THEN '�Է��'
						  WHEN PRODUCT_NAME LIKE '%�����%' THEN '�����'
						  WHEN PRODUCT_NAME LIKE '%�����%' THEN '�����'
						  WHEN PRODUCT_NAME LIKE '%�����%' THEN '�����'
						  WHEN PRODUCT_NAME LIKE '%������ʵҧ%' THEN '������ʵҧ��'
						  WHEN PRODUCT_NAME LIKE '%����%' THEN '����'
						  WHEN PRODUCT_NAME LIKE '%������%' THEN '������'
						  WHEN PRODUCT_NAME LIKE '%��ŵ���%' THEN '��ŵ������'
						  WHEN PRODUCT_NAME LIKE '%����­%' THEN '����­��ѧ'
						  WHEN PRODUCT_NAME LIKE '%GUNDUM%' THEN 'GUNDUM'
						  WHEN PRODUCT_NAME LIKE '%�������%' THEN 'COSPLAY'
						  WHEN PRODUCT_NAME LIKE '%������%' THEN '������'
						  WHEN PRODUCT_NAME LIKE '%������%' THEN '������'
						  --HOME NON-ELECTRONICS (ACCESSORIES)
						  WHEN PRODUCT_NAME LIKE '%4-7Y%' THEN '4-7Y'
						  --HOME NON-ELECTRONICS (MODEL)
						  WHEN PRODUCT_NAME LIKE '%GAMER%' THEN 'GAMER'
						  WHEN PRODUCT_NAME LIKE '%FIGURE%' THEN 'FIGURE'
						  WHEN PRODUCT_NAME LIKE '%�ش����%' THEN '�ش����'
						  WHEN PRODUCT_NAME LIKE '%�ش�س���%' THEN '�ش�س���'
						  WHEN PRODUCT_NAME LIKE '%ROBOT%' THEN 'ROBOTS'
						  WHEN PRODUCT_NAME LIKE '%DARTH VADER%' THEN 'DARTH VADER'
						  WHEN PRODUCT_NAME LIKE '%KAMEN RIDER%' THEN 'KAMEN RIDER'
						  WHEN PRODUCT_NAME LIKE '%KNIGHT%' THEN 'KNIGHT'
						  WHEN PRODUCT_NAME LIKE '%UNICORN%' THEN 'UNICORN'
						  WHEN PRODUCT_NAME LIKE '%ACTION%' THEN 'ACTION'
						  WHEN PRODUCT_NAME LIKE '%YODA%' THEN 'YODA'
						  WHEN PRODUCT_NAME LIKE '%��ǻ���%' THEN '��ǻ���'
						  WHEN PRODUCT_NAME LIKE '%GOKU%' THEN 'GOKU'
						  WHEN PRODUCT_NAME LIKE '%DIY%' THEN 'DIY'
						  WHEN PRODUCT_NAME LIKE '%YOUKAI%' THEN 'YOUKAI'
						  WHEN PRODUCT_NAME LIKE '%TROOPER%' THEN 'TROOPER'
						  WHEN PRODUCT_NAME LIKE '%GUNDA%' THEN 'GUNDUM'
						  WHEN PRODUCT_NAME LIKE '%GUNDAM%' THEN 'GUNDUM'
						  WHEN PRODUCT_NAME LIKE '%COMBAT%' THEN 'COMBAT'
						  WHEN PRODUCT_NAME LIKE '%CHOPPER%' THEN 'CHOPPER'
						  WHEN PRODUCT_NAME LIKE '%ONE PIECE%' THEN 'ONE PIECE'
						  WHEN PRODUCT_NAME LIKE '%DRIVER%' THEN 'DRIVER'
						  WHEN PRODUCT_NAME LIKE '%UNICORN%' THEN 'UNICORN'
						  WHEN PRODUCT_NAME LIKE '%GUMDAM%' THEN 'GUNDUM'
						  WHEN PRODUCT_NAME LIKE '%CUBE%' THEN 'CUBE'
						  WHEN PRODUCT_NAME LIKE '%DX %' THEN 'DX'
						  WHEN PRODUCT_NAME LIKE '%DRAGON%' THEN 'DRAGON'
						  WHEN PRODUCT_NAME LIKE '%�ԡ����%' THEN 'FIGURE'
						  WHEN PRODUCT_NAME LIKE '%MARVEL%' THEN 'MARVEL'
						  WHEN PRODUCT_NAME LIKE '%VEHICLE%' THEN 'VEHICLE'
						  WHEN PRODUCT_NAME LIKE '%MODEL%' THEN 'MODEL'
						  WHEN CLEANED_BRANDNAME LIKE '%BANDAI%' THEN 'BANDAI'
						  WHEN PRODUCT_NAME LIKE '%HONDA%' THEN 'HONDA'
						  WHEN PRODUCT_NAME LIKE '%MINI COOPER%' THEN 'MINI COOPER'
						  WHEN PRODUCT_NAME LIKE '%BENZ%' THEN 'MERCEDES BENZ'
						  WHEN PRODUCT_NAME LIKE '%FERRARI%' THEN 'FERRARI'
						  WHEN PRODUCT_NAME LIKE '%BMW%' THEN 'BMW'
						  WHEN PRODUCT_NAME LIKE '%NISSAN%' THEN 'NISSAN'
						  WHEN PRODUCT_NAME LIKE '%YAMAHA%' THEN 'YAMAHA'
						  WHEN PRODUCT_NAME LIKE '%MISSUBISHI%' THEN 'MITSUBISHI'
						  WHEN PRODUCT_NAME LIKE '%MITSUBISHI%' THEN 'MITSUBISHI'
						  WHEN PRODUCT_NAME LIKE '%POLICE%' THEN 'POLICE'
						  WHEN PRODUCT_NAME LIKE '%���Ǩ%' THEN 'POLICE'
						  WHEN PRODUCT_NAME LIKE '%HELICOPTER%' THEN 'HELICOPTER'
						  WHEN PRODUCT_NAME LIKE '%TOYATA%' THEN 'TOYOTA'
						  WHEN PRODUCT_NAME LIKE '%TOYOTA%' THEN 'TOYOTA'
						  WHEN PRODUCT_NAME LIKE '%POKEMON%' THEN 'POKEMON'
						  WHEN PRODUCT_NAME LIKE '%SPACE%' THEN 'SPACE'
						  WHEN PRODUCT_NAME LIKE '%�ǡ��%' THEN 'SPACE'
						  WHEN PRODUCT_NAME LIKE '%BATTLE%' THEN 'BATTLE'
						  WHEN PRODUCT_NAME LIKE '%���%' THEN 'BATTLE'
						  WHEN PRODUCT_NAME LIKE '%ARMY%' THEN 'ARMY'
						  WHEN PRODUCT_NAME LIKE '%NAVY%' THEN 'NAVY'
						  WHEN PRODUCT_NAME LIKE '%TANK%' THEN 'TANK'
						  WHEN PRODUCT_NAME LIKE '%DESTROY%' THEN 'DESTROY'
						  WHEN PRODUCT_NAME LIKE '%FIGHT%' THEN 'FIGHTING'
						  WHEN PRODUCT_NAME LIKE '%AERO%' THEN 'AERO'
						  WHEN PRODUCT_NAME LIKE '%VOLKSWAGEN%' THEN 'VOLKSWAGEN'
						  WHEN PRODUCT_NAME LIKE '%AMBULANCE%' THEN 'AMBULANCE'
						  WHEN PRODUCT_NAME LIKE '%MILITARY%' THEN 'MILITARY'
						  WHEN PRODUCT_NAME LIKE '%TRUCK%' THEN 'TRUCK'
						  WHEN PRODUCT_NAME LIKE '%DUCATI%' THEN 'DUCATI'
						  WHEN PRODUCT_NAME LIKE '%AVENGER%' THEN 'AVENGER'
						  WHEN PRODUCT_NAME LIKE '%SUZUKI%' THEN 'SUZUKI'
						  WHEN PRODUCT_NAME LIKE '%HYUNDAI%' THEN 'HYUNDAI'
						  WHEN PRODUCT_NAME LIKE '%LEXUS%' THEN 'LEXUS'
						  WHEN PRODUCT_NAME LIKE '%FORD%' THEN 'FORD'
						  WHEN PRODUCT_NAME LIKE '%SUBMARINE%' THEN 'SUBMARINE'
						  WHEN PRODUCT_NAME LIKE '%MARINE%' THEN 'MARINE'
						  WHEN PRODUCT_NAME LIKE '%NINJA%' THEN 'NINJA'
						  WHEN PRODUCT_NAME LIKE '%BOAT%' THEN 'BOAT'
						  WHEN PRODUCT_NAME LIKE '%SHIP%' THEN 'SHIP'
						  WHEN PRODUCT_NAME LIKE '%KUMAMON%' THEN 'KUMAMON'
						  WHEN PRODUCT_NAME LIKE '%WEAPON%' THEN 'WEAPON'
						  WHEN PRODUCT_NAME LIKE '%ARMOUR%' THEN 'ARMOUR'
						  --HOME NON-ELECTRONICS (ANIMAL/GAMES): PRESCHOOL ONLY
						  WHEN PRODUCT_NAME LIKE '%���%' THEN '���'
						  WHEN PRODUCT_NAME LIKE '%FISHING%' THEN 'FISHING'
						  WHEN PRODUCT_NAME LIKE '%JIGSAW%' THEN 'JIGSAW'
						  WHEN (PRODUCT_NAME LIKE '%PUZZLE%' AND PRODUCT_NAME NOT LIKE '%150%' AND PRODUCT_NAME NOT LIKE '%100%') THEN 'PUZZLE'
						  WHEN PRODUCT_NAME LIKE '%ABC%' THEN 'ABC'
						  --HOME NON-ELECTRONICS (ANIMAL/GAMES): PRESCHOOL,JUNIOR
						  WHEN PRODUCT_NAME LIKE '%��%' THEN '��'
                          WHEN PRODUCT_NAME LIKE '%�ѹ�%' THEN '�ѹ�'
						  WHEN PRODUCT_NAME LIKE '%HIPPO%' THEN 'HIPPO'
					  	  WHEN PRODUCT_NAME LIKE '%�ԧ�%' THEN 'BINGO'
						  WHEN PRODUCT_NAME LIKE '%MINECRAFT%' THEN 'MINECRAFT'
						  WHEN PRODUCT_NAME LIKE '%PIE FACE%' THEN 'PIE FACE'
						  WHEN PRODUCT_NAME LIKE '%�غ����%' THEN '�غ����'
						  WHEN PRODUCT_NAME LIKE '%��ꡫ�%' THEN 'JIGSAW'
						  WHEN PRODUCT_NAME LIKE '%BINGO%' THEN 'BINGO'
						  WHEN PRODUCT_NAME LIKE '%��Ž֡�ѡ��%' THEN 'BALL'
						  WHEN PRODUCT_NAME LIKE '%ʺ��%' THEN 'BUBBLE'
						  WHEN PRODUCT_NAME LIKE '%PUZZLE%' THEN 'PUZZLE'
						  WHEN PRODUCT_NAME LIKE '%RING UP%' THEN 'RING UP'
						  --HOME NON-ELECTRONICS (ANIMAL/GAMES): PRESCHOOL,JUNIOR, TEEN
						  WHEN PRODUCT_NAME LIKE '%DOMINO%' THEN 'DOMINO'
						  WHEN PRODUCT_NAME LIKE '%����%' THEN 'DOMINO'
						  WHEN PRODUCT_NAME LIKE '%�����%' THEN 'DOMINO'
						  WHEN PRODUCT_NAME LIKE '%I SPY DIG IN%' THEN 'I SPY DIG IN'
						  WHEN PRODUCT_NAME LIKE '%�¹��ǧ%' THEN '�¹��ǧ'
						  WHEN PRODUCT_NAME LIKE '%TWISTER%' THEN 'TWISTER'
						  WHEN PRODUCT_NAME LIKE '%ö%' THEN 'ö'
						  WHEN PRODUCT_NAME LIKE '%CAR%' THEN 'CARS'
   					      ELSE PRESCHOOL_KEYWORD END
WHERE MOM_AND_KID_PRODUCT_FLAG = 1
AND PRESCHOOL_FLAG = 'YES'
AND PRESCHOOL_KEYWORD IS NULL
;

UPDATE vpm_data.pm_skutagging_kids_product_master
SET JUNIOR_KEYWORD = --NULL & OTHER
                     CASE WHEN PRODUCT_NAME LIKE '%���%' THEN '���'
					      WHEN PRODUCT_NAME LIKE '%�%' THEN '�'
                          WHEN PRODUCT_NAME LIKE '%�������%' THEN '�������'
						  WHEN PRODUCT_NAME LIKE '%��ǧ�ҧ%' THEN '��ǧ�ҧ' 
						  WHEN PRODUCT_NAME LIKE '%����ͧ�Թ%' THEN '����ͧ�Թ'
						  WHEN PRODUCT_NAME LIKE '%�����%' THEN '�����'
						  WHEN PRODUCT_NAME LIKE '%���Ԥͻ��%' THEN '���Ԥͻ����'
						  WHEN PRODUCT_NAME LIKE '%ö�%' THEN 'ö�'
						  WHEN PRODUCT_NAME LIKE '%ʡ������%' THEN 'ʡ������'
						  WHEN PRODUCT_NAME LIKE '%���%' THEN '���'
						  WHEN PRODUCT_NAME LIKE '%��蹹��%' THEN '��蹹��'
						  WHEN PRODUCT_NAME LIKE '%�ǧ�ҧ%' THEN '��ǧ�ҧ'
						  WHEN PRODUCT_NAME LIKE '%�ԧ���%' THEN '�ԧ���'
						  WHEN PRODUCT_NAME LIKE '%ö�ҡ%' THEN 'ö�ҡ'
						  WHEN PRODUCT_NAME LIKE '%�ͧ���%' THEN '�ͧ���'
						  WHEN PRODUCT_NAME LIKE '%ö�ҡ%' THEN 'ö�ҡ'
						  WHEN PRODUCT_NAME LIKE '%�����ҧ%' THEN '�����ҧ'
						  WHEN PRODUCT_NAME LIKE '%ʹѺ%' THEN 'ʹѺ'
						  WHEN PRODUCT_NAME LIKE '%��ҹ���%' THEN '��ҹ���'
						  WHEN PRODUCT_NAME LIKE '%���%' THEN '���'
						  WHEN PRODUCT_NAME LIKE '%��ҹ%' THEN '��ҹ'
						  WHEN PRODUCT_NAME LIKE '%GUN%' THEN 'GUN'
						  --HOME ELECTRONICS
						  WHEN PRODUCT_NAME LIKE '%ö������ҧ%' THEN 'ö������ҧ'
						  WHEN PRODUCT_NAME LIKE '%�׹%' THEN '�׹'
						  WHEN PRODUCT_NAME LIKE '%����%' THEN 'MODEL'
						  WHEN PRODUCT_NAME LIKE '%�Һ%' THEN '�Һ'
						  WHEN PRODUCT_NAME LIKE '%�ѧ�ѹ%' THEN '�ѧ�ѹ'
						  WHEN PRODUCT_NAME LIKE '%�ѹ����%' THEN 'GUNDUM'
						  WHEN PRODUCT_NAME LIKE '%˹�ҡҡ%' THEN '˹�ҡҡ'
						  WHEN PRODUCT_NAME LIKE '%ö��%' THEN 'ö��'
						  WHEN PRODUCT_NAME LIKE '%����%' THEN '�Է��'
						  WHEN PRODUCT_NAME LIKE '%�Է��%' THEN '�Է��'
						  WHEN PRODUCT_NAME LIKE '%�����%' THEN '�����'
						  WHEN PRODUCT_NAME LIKE '%�����%' THEN '�����'
						  WHEN PRODUCT_NAME LIKE '%�����%' THEN '�����'
						  WHEN PRODUCT_NAME LIKE '%������ʵҧ%' THEN '������ʵҧ��'
						  WHEN PRODUCT_NAME LIKE '%����%' THEN '����'
						  WHEN PRODUCT_NAME LIKE '%������%' THEN '������'
						  WHEN PRODUCT_NAME LIKE '%��ŵ���%' THEN '��ŵ������'
						  WHEN PRODUCT_NAME LIKE '%����­%' THEN '����­��ѧ'
						  WHEN PRODUCT_NAME LIKE '%GUNDUM%' THEN 'GUNDUM'
						  WHEN PRODUCT_NAME LIKE '%�������%' THEN 'COSPLAY'
						  WHEN PRODUCT_NAME LIKE '%������%' THEN '������'
						  WHEN PRODUCT_NAME LIKE '%������%' THEN '������'
						  --HOME NON-ELECTRONICS (ACCESSORIES)
						  WHEN PRODUCT_NAME LIKE '%7-10Y%' THEN '7-10Y'
						  --HOME NON-ELECTRONICS (MODEL)
						  WHEN PRODUCT_NAME LIKE '%GAMER%' THEN 'GAMER'
						  WHEN PRODUCT_NAME LIKE '%FIGURE%' THEN 'FIGURE'
						  WHEN PRODUCT_NAME LIKE '%�ش����%' THEN '�ش����'
						  WHEN PRODUCT_NAME LIKE '%�ش�س���%' THEN '�ش�س���'
						  WHEN PRODUCT_NAME LIKE '%ROBOT%' THEN 'ROBOTS'
						  WHEN PRODUCT_NAME LIKE '%DARTH VADER%' THEN 'DARTH VADER'
						  WHEN PRODUCT_NAME LIKE '%KAMEN RIDER%' THEN 'KAMEN RIDER'
						  WHEN PRODUCT_NAME LIKE '%KNIGHT%' THEN 'KNIGHT'
						  WHEN PRODUCT_NAME LIKE '%UNICORN%' THEN 'UNICORN'
						  WHEN PRODUCT_NAME LIKE '%ACTION%' THEN 'ACTION'
						  WHEN PRODUCT_NAME LIKE '%YODA%' THEN 'YODA'
						  WHEN PRODUCT_NAME LIKE '%��ǻ���%' THEN '��ǻ���'
						  WHEN PRODUCT_NAME LIKE '%GOKU%' THEN 'GOKU'
						  WHEN PRODUCT_NAME LIKE '%DIY%' THEN 'DIY'
						  WHEN PRODUCT_NAME LIKE '%YOUKAI%' THEN 'YOUKAI'
						  WHEN PRODUCT_NAME LIKE '%TROOPER%' THEN 'TROOPER'
						  WHEN PRODUCT_NAME LIKE '%GUNDA%' THEN 'GUNDUM'
						  WHEN PRODUCT_NAME LIKE '%GUNDAM%' THEN 'GUNDUM'
						  WHEN PRODUCT_NAME LIKE '%COMBAT%' THEN 'COMBAT'
						  WHEN PRODUCT_NAME LIKE '%CHOPPER%' THEN 'CHOPPER'
						  WHEN PRODUCT_NAME LIKE '%ONE PIECE%' THEN 'ONE PIECE'
						  WHEN PRODUCT_NAME LIKE '%DRIVER%' THEN 'DRIVER'
						  WHEN PRODUCT_NAME LIKE '%UNICORN%' THEN 'UNICORN'
						  WHEN PRODUCT_NAME LIKE '%GUMDAM%' THEN 'GUNDUM'
						  WHEN PRODUCT_NAME LIKE '%CUBE%' THEN 'CUBE'
						  WHEN PRODUCT_NAME LIKE '%DX %' THEN 'DX'
						  WHEN PRODUCT_NAME LIKE '%DRAGON%' THEN 'DRAGON'
						  WHEN PRODUCT_NAME LIKE '%�ԡ����%' THEN 'FIGURE'
						  WHEN PRODUCT_NAME LIKE '%MARVEL%' THEN 'MARVEL'
						  WHEN PRODUCT_NAME LIKE '%VEHICLE%' THEN 'VEHICLE'
						  WHEN PRODUCT_NAME LIKE '%MODEL%' THEN 'MODEL'
						  WHEN CLEANED_BRANDNAME LIKE '%BANDAI%' THEN 'BANDAI'
						  WHEN PRODUCT_NAME LIKE '%HONDA%' THEN 'HONDA'
						  WHEN PRODUCT_NAME LIKE '%MINI COOPER%' THEN 'MINI COOPER'
						  WHEN PRODUCT_NAME LIKE '%BENZ%' THEN 'MERCEDES BENZ'
						  WHEN PRODUCT_NAME LIKE '%FERRARI%' THEN 'FERRARI'
						  WHEN PRODUCT_NAME LIKE '%BMW%' THEN 'BMW'
						  WHEN PRODUCT_NAME LIKE '%NISSAN%' THEN 'NISSAN'
						  WHEN PRODUCT_NAME LIKE '%YAMAHA%' THEN 'YAMAHA'
						  WHEN PRODUCT_NAME LIKE '%MISSUBISHI%' THEN 'MITSUBISHI'
						  WHEN PRODUCT_NAME LIKE '%MITSUBISHI%' THEN 'MITSUBISHI'
						  WHEN PRODUCT_NAME LIKE '%POLICE%' THEN 'POLICE'
						  WHEN PRODUCT_NAME LIKE '%���Ǩ%' THEN 'POLICE'
						  WHEN PRODUCT_NAME LIKE '%HELICOPTER%' THEN 'HELICOPTER'
						  WHEN PRODUCT_NAME LIKE '%TOYATA%' THEN 'TOYOTA'
						  WHEN PRODUCT_NAME LIKE '%TOYOTA%' THEN 'TOYOTA'
						  WHEN PRODUCT_NAME LIKE '%POKEMON%' THEN 'POKEMON'
						  WHEN PRODUCT_NAME LIKE '%SPACE%' THEN 'SPACE'
						  WHEN PRODUCT_NAME LIKE '%�ǡ��%' THEN 'SPACE'
						  WHEN PRODUCT_NAME LIKE '%BATTLE%' THEN 'BATTLE'
						  WHEN PRODUCT_NAME LIKE '%���%' THEN 'BATTLE'
						  WHEN PRODUCT_NAME LIKE '%ARMY%' THEN 'ARMY'
						  WHEN PRODUCT_NAME LIKE '%NAVY%' THEN 'NAVY'
						  WHEN PRODUCT_NAME LIKE '%TANK%' THEN 'TANK'
						  WHEN PRODUCT_NAME LIKE '%DESTROY%' THEN 'DESTROY'
						  WHEN PRODUCT_NAME LIKE '%FIGHT%' THEN 'FIGHTING'
						  WHEN PRODUCT_NAME LIKE '%AERO%' THEN 'AERO'
						  WHEN PRODUCT_NAME LIKE '%VOLKSWAGEN%' THEN 'VOLKSWAGEN'
						  WHEN PRODUCT_NAME LIKE '%AMBULANCE%' THEN 'AMBULANCE'
						  WHEN PRODUCT_NAME LIKE '%MILITARY%' THEN 'MILITARY'
						  WHEN PRODUCT_NAME LIKE '%TRUCK%' THEN 'TRUCK'
						  WHEN PRODUCT_NAME LIKE '%DUCATI%' THEN 'DUCATI'
						  WHEN PRODUCT_NAME LIKE '%AVENGER%' THEN 'AVENGER'
						  WHEN PRODUCT_NAME LIKE '%SUZUKI%' THEN 'SUZUKI'
						  WHEN PRODUCT_NAME LIKE '%HYUNDAI%' THEN 'HYUNDAI'
						  WHEN PRODUCT_NAME LIKE '%LEXUS%' THEN 'LEXUS'
						  WHEN PRODUCT_NAME LIKE '%FORD%' THEN 'FORD'
						  WHEN PRODUCT_NAME LIKE '%SUBMARINE%' THEN 'SUBMARINE'
						  WHEN PRODUCT_NAME LIKE '%MARINE%' THEN 'MARINE'
						  WHEN PRODUCT_NAME LIKE '%NINJA%' THEN 'NINJA'
						  WHEN PRODUCT_NAME LIKE '%BOAT%' THEN 'BOAT'
						  WHEN PRODUCT_NAME LIKE '%SHIP%' THEN 'SHIP'
						  WHEN PRODUCT_NAME LIKE '%KUMAMON%' THEN 'KUMAMON'
						  WHEN PRODUCT_NAME LIKE '%WEAPON%' THEN 'WEAPON'
						  WHEN PRODUCT_NAME LIKE '%ARMOUR%' THEN 'ARMOUR'
						  --HOME NON-ELECTRONICS (ANIMAL/GAMES): PRESCHOOL,JUNIOR
						  WHEN PRODUCT_NAME LIKE '%��%' THEN '��'
                          WHEN PRODUCT_NAME LIKE '%�ѹ�%' THEN '�ѹ�'
						  WHEN PRODUCT_NAME LIKE '%HIPPO%' THEN 'HIPPO'
					  	  WHEN PRODUCT_NAME LIKE '%�ԧ�%' THEN 'BINGO'
						  WHEN PRODUCT_NAME LIKE '%MINECRAFT%' THEN 'MINECRAFT'
						  WHEN PRODUCT_NAME LIKE '%PIE FACE%' THEN 'PIE FACE'
						  WHEN PRODUCT_NAME LIKE '%�غ����%' THEN '�غ����'
						  WHEN PRODUCT_NAME LIKE '%��ꡫ�%' THEN 'JIGSAW'
						  WHEN PRODUCT_NAME LIKE '%BINGO%' THEN 'BINGO'
						  WHEN PRODUCT_NAME LIKE '%��Ž֡�ѡ��%' THEN 'BALL'
						  WHEN PRODUCT_NAME LIKE '%ʺ��%' THEN 'BUBBLE'
						  WHEN PRODUCT_NAME LIKE '%PUZZLE%' THEN 'PUZZLE'
						  WHEN PRODUCT_NAME LIKE '%RING UP%' THEN 'RING UP'
						  --HOME NON-ELECTRONICS (ANIMAL/GAMES): PRESCHOOL,JUNIOR, TEEN
						  WHEN PRODUCT_NAME LIKE '%DOMINO%' THEN 'DOMINO'
						  WHEN PRODUCT_NAME LIKE '%����%' THEN 'DOMINO'
						  WHEN PRODUCT_NAME LIKE '%�����%' THEN 'DOMINO'
						  WHEN PRODUCT_NAME LIKE '%I SPY DIG IN%' THEN 'I SPY DIG IN'
						  WHEN PRODUCT_NAME LIKE '%�¹��ǧ%' THEN '�¹��ǧ'
						  WHEN PRODUCT_NAME LIKE '%TWISTER%' THEN 'TWISTER'
						  --HOME NON-ELECTRONICS (ANIMAL/GAMES): JUNIOR, TEEN
						  WHEN PRODUCT_NAME LIKE '%UNO%' THEN 'UNO'
						  WHEN PRODUCT_NAME LIKE '%��ҡ%' THEN '��ҡ�ء'
						  WHEN PRODUCT_NAME LIKE '%JENGA%' THEN 'JENGA'
						  WHEN PRODUCT_NAME LIKE '%MONOPOLY%' THEN 'MONOPOLY'
						  WHEN PRODUCT_NAME LIKE '%MOUSETRAP%' THEN 'MOUSETRAP'
						  WHEN PRODUCT_NAME LIKE '%MOUSE TRAP%' THEN 'MOUSETRAP'
						  WHEN PRODUCT_NAME LIKE '%CARDINAL%' THEN 'CARDINAL'
						  WHEN PRODUCT_NAME LIKE '%IQ PUZZLE%' THEN 'IQ PUZZLE'
						  WHEN PRODUCT_NAME LIKE '%CHES%' THEN 'CHESS'
						  WHEN PRODUCT_NAME LIKE '%IQ%' THEN 'IQ'
						  WHEN PRODUCT_NAME LIKE '%SUDOKU%' THEN 'SUDOKU'
						  WHEN PRODUCT_NAME LIKE '%��⤴�%' THEN 'SUDOKU'
						  WHEN PRODUCT_NAME LIKE '%�ٺԤ%' THEN 'RUBIC'
						  WHEN PRODUCT_NAME LIKE '%RUBIC%' THEN 'RUBIC'
						  WHEN PRODUCT_NAME LIKE '%RUBIK%' THEN 'RUBIC'
						  WHEN PRODUCT_NAME LIKE '%����������ѡ��%' THEN '����������ѡ��'
						  WHEN PRODUCT_NAME LIKE '%GRAB AND ;%' THEN 'GRAB AND ;'
						  WHEN PRODUCT_NAME LIKE '%PINBALL%' THEN 'PINBALL'
						  WHEN PRODUCT_NAME LIKE '%SCRABBLE%' THEN 'SCRABBLE'
						  WHEN PRODUCT_NAME LIKE '%SPEED CUPS%' THEN 'SPEED CUPS'
						  WHEN PRODUCT_NAME LIKE '%STACK%' THEN 'STACK'
						  WHEN PRODUCT_NAME LIKE '%QUAKE TOWER%' THEN 'QUAKE TOWER'
						  WHEN PRODUCT_NAME LIKE '%DETECTIVE GAME%' THEN 'DETECTIVE GAME'
						  WHEN PRODUCT_NAME LIKE '%MONOPONY%' THEN 'MONOPOLY'
						  --HOME NON-ELECTRONICS (ANIMAL/GAMES): JUNIOR ONLY
						  WHEN CLEANED_BRANDNAME LIKE '%LEGO%' THEN 'LEGO'
						  WHEN CLEANED_BRANDNAME LIKE '%CARDINAL%' THEN 'CARDINAL'
						  WHEN PRODUCT_NAME LIKE '%���ʷ%' THEN 'MOSAIC'
						  WHEN PRODUCT_NAME LIKE '%TUMBLIN%' THEN 'TUMBLIN'
						  WHEN PRODUCT_NAME LIKE '%�������%' THEN '�������'
						  WHEN PRODUCT_NAME LIKE '%ö%' THEN 'ö'
						  WHEN PRODUCT_NAME LIKE '%CAR%' THEN 'CARS'
   					      ELSE JUNIOR_KEYWORD END
WHERE MOM_AND_KID_PRODUCT_FLAG = 1
AND JUNIOR_FLAG = 'YES'
AND JUNIOR_KEYWORD IS NULL
;

UPDATE vpm_data.pm_skutagging_kids_product_master
SET TEEN_KEYWORD = CASE WHEN PRODUCT_NAME LIKE '%�%' THEN '�'
                        WHEN PRODUCT_NAME LIKE '%J.K. ROWLIN%' THEN 'J.K. ROWLIN'
	                    WHEN PRODUCT_NAME LIKE '%HARRY POTTER%' THEN 'HARRY POTTER'
						--HOME NON-ELECTRONICS (ANIMAL/GAMES): PRESCHOOL,JUNIOR, TEEN
						WHEN PRODUCT_NAME LIKE '%DOMINO%' THEN 'DOMINO'
						WHEN PRODUCT_NAME LIKE '%����%' THEN 'DOMINO'
						WHEN PRODUCT_NAME LIKE '%�����%' THEN 'DOMINO'
						--HOME NON-ELECTRONICS (ANIMAL/GAMES): JUNIOR, TEEN
						WHEN PRODUCT_NAME LIKE '%UNO%' THEN 'UNO'
						WHEN PRODUCT_NAME LIKE '%��ҡ%' THEN '��ҡ�ء'
						WHEN PRODUCT_NAME LIKE '%JENGA%' THEN 'JENGA'
						WHEN PRODUCT_NAME LIKE '%MONOPOLY%' THEN 'MONOPOLY'
						WHEN PRODUCT_NAME LIKE '%MOUSETRAP%' THEN 'MOUSETRAP'
						WHEN PRODUCT_NAME LIKE '%MOUSE TRAP%' THEN 'MOUSETRAP'
						WHEN PRODUCT_NAME LIKE '%CARDINAL%' THEN 'CARDINAL'
						WHEN PRODUCT_NAME LIKE '%IQ PUZZLE%' THEN 'IQ PUZZLE'
						WHEN PRODUCT_NAME LIKE '%CHES%' THEN 'CHESS'
						WHEN PRODUCT_NAME LIKE '%IQ%' THEN 'IQ'
						WHEN PRODUCT_NAME LIKE '%SUDOKU%' THEN 'SUDOKU'
						WHEN PRODUCT_NAME LIKE '%��⤴�%' THEN 'SUDOKU'
						WHEN PRODUCT_NAME LIKE '%�ٺԤ%' THEN 'RUBIC'
						WHEN PRODUCT_NAME LIKE '%RUBIC%' THEN 'RUBIC'
						WHEN PRODUCT_NAME LIKE '%RUBIK%' THEN 'RUBIC'
						WHEN PRODUCT_NAME LIKE '%����������ѡ��%' THEN '����������ѡ��'
						WHEN PRODUCT_NAME LIKE '%GRAB AND ;%' THEN 'GRAB AND ;'
						WHEN PRODUCT_NAME LIKE '%PINBALL%' THEN 'PINBALL'
						WHEN PRODUCT_NAME LIKE '%SCRABBLE%' THEN 'SCRABBLE'
						WHEN PRODUCT_NAME LIKE '%SPEED CUPS%' THEN 'SPEED CUPS'
						WHEN PRODUCT_NAME LIKE '%STACK%' THEN 'STACK'
						WHEN PRODUCT_NAME LIKE '%QUAKE TOWER%' THEN 'QUAKE TOWER'
						WHEN PRODUCT_NAME LIKE '%MONOPONY%' THEN 'MONOPOLY'
						WHEN PRODUCT_NAME LIKE '%DETECTIVE GAME%' THEN 'DETECTIVE GAME'
						ELSE TEEN_KEYWORD END
WHERE MOM_AND_KID_PRODUCT_FLAG = 1
AND TEEN_FLAG = 'YES'
AND TEEN_KEYWORD IS NULL
;

------------ NULL ------------
UPDATE vpm_data.pm_skutagging_kids_product_master
SET PRESCHOOL_KEYWORD = 'PRESCHOOL TOY'
WHERE MOM_AND_KID_PRODUCT_FLAG = 1
AND REVISED_CATEGORY_LEVEL_1 IS NULL
AND PRESCHOOL_FLAG = 'YES'
AND PRESCHOOL_KEYWORD IS NULL
;

UPDATE vpm_data.pm_skutagging_kids_product_master
SET JUNIOR_KEYWORD = 'JUNIOR TOY'
WHERE MOM_AND_KID_PRODUCT_FLAG = 1
AND REVISED_CATEGORY_LEVEL_1 IS NULL
AND JUNIOR_FLAG = 'YES'
AND JUNIOR_KEYWORD IS NULL
;

------------ OTHER ------------
UPDATE vpm_data.pm_skutagging_kids_product_master
SET TEEN_KEYWORD = 'FOREIGN BOOKS'
WHERE MOM_AND_KID_PRODUCT_FLAG = 1
AND REVISED_CATEGORY_LEVEL_1 = 'OTHER'
AND DEPT_NAME = 'FOREIGN BOOKS'
AND TEEN_FLAG = 'YES'
AND TEEN_KEYWORD IS NULL
;

UPDATE vpm_data.pm_skutagging_kids_product_master
SET PRESCHOOL_KEYWORD = 'PRESCHOOL TOY'
WHERE MOM_AND_KID_PRODUCT_FLAG = 1
AND REVISED_CATEGORY_LEVEL_1 = 'OTHER'
AND DEPT_NAME = 'NON-FOOD'
AND PRESCHOOL_FLAG = 'YES'
;

UPDATE vpm_data.pm_skutagging_kids_product_master
SET JUNIOR_KEYWORD = 'JUNIOR TOY'
WHERE MOM_AND_KID_PRODUCT_FLAG = 1
AND REVISED_CATEGORY_LEVEL_1 = 'OTHER'
AND DEPT_NAME = 'NON-FOOD'
AND JUNIOR_FLAG = 'YES'
;

------------ HOME ELECTRONICS ------------
UPDATE vpm_data.pm_skutagging_kids_product_master
SET PRESCHOOL_KEYWORD = 'PRESCHOOL TOY'
WHERE MOM_AND_KID_PRODUCT_FLAG = 1
AND REVISED_CATEGORY_LEVEL_1 = 'HOME ELECTRONICS'
AND PRESCHOOL_FLAG = 'YES'
AND PRESCHOOL_KEYWORD IS NULL
;

UPDATE vpm_data.pm_skutagging_kids_product_master
SET JUNIOR_KEYWORD = 'JUNIOR TOY'
WHERE MOM_AND_KID_PRODUCT_FLAG = 1
AND REVISED_CATEGORY_LEVEL_1 = 'HOME ELECTRONICS'
AND JUNIOR_FLAG = 'YES'
AND JUNIOR_KEYWORD IS NULL
;

------------ HOME NON-ELECTRONICS ------------
--MODEL
UPDATE vpm_data.pm_skutagging_kids_product_master
SET PRESCHOOL_KEYWORD = 'PRESCHOOL MODEL'
   ,JUNIOR_KEYWORD = 'JUNIOR MODEL'
WHERE MOM_AND_KID_PRODUCT_FLAG = 1
AND REVISED_CATEGORY_LEVEL_1 = 'HOME NON-ELECTRONICS'
AND SUBCLASS_NAME = 'MODEL'
AND PRESCHOOL_FLAG = 'YES'
AND PRESCHOOL_KEYWORD IS NULL 
;

--ANIMAL/GAMES
UPDATE vpm_data.pm_skutagging_kids_product_master
SET PRESCHOOL_KEYWORD = 'PRESCHOOL TOY'
WHERE MOM_AND_KID_PRODUCT_FLAG = 1
AND REVISED_CATEGORY_LEVEL_1 = 'HOME NON-ELECTRONICS'
AND SUBCLASS_NAME IN ('ANIMAL','GAMES')
AND PRESCHOOL_FLAG = 'YES'
AND PRESCHOOL_KEYWORD IS NULL
;

------------ FASHION ------------
UPDATE vpm_data.pm_skutagging_kids_product_master
SET PRESCHOOL_KEYWORD = CASE WHEN PRODUCT_NAME LIKE '%25%' THEN 'SIZE 25'
                             WHEN PRODUCT_NAME LIKE '%26%' THEN 'SIZE 26'
							 WHEN PRODUCT_NAME LIKE '%27%' THEN 'SIZE 27'
							 WHEN PRODUCT_NAME LIKE '%28%' THEN 'SIZE 28'
							 WHEN PRODUCT_NAME LIKE '%29%' THEN 'SIZE 29'
							 WHEN PRODUCT_NAME LIKE '%30%' THEN 'SIZE 30'
							 WHEN PRODUCT_NAME LIKE '%31%' THEN 'SIZE 31'
							 ELSE PRESCHOOL_KEYWORD END
WHERE MOM_AND_KID_PRODUCT_FLAG = 1
AND REVISED_CATEGORY_LEVEL_1 = 'FASHION'
AND CLEANED_BRANDNAME NOT IN ('SANRIO','SANRIO LOCAL','THAI TIME','TSURUYA')
AND PRESCHOOL_FLAG = 'YES'
;

UPDATE vpm_data.pm_skutagging_kids_product_master
SET JUNIOR_KEYWORD = CASE WHEN PRODUCT_NAME LIKE '%32%' THEN 'SIZE 32'
                             WHEN PRODUCT_NAME LIKE '%33%' THEN 'SIZE 33'
							 WHEN PRODUCT_NAME LIKE '%34%' THEN 'SIZE 34'
							 WHEN PRODUCT_NAME LIKE '%35%' THEN 'SIZE 35'
							 WHEN PRODUCT_NAME LIKE '%36%' THEN 'SIZE 36'
							 ELSE JUNIOR_KEYWORD END
WHERE MOM_AND_KID_PRODUCT_FLAG = 1
AND REVISED_CATEGORY_LEVEL_1 = 'FASHION'
AND CLEANED_BRANDNAME NOT IN ('SANRIO','SANRIO LOCAL','THAI TIME','TSURUYA')
AND JUNIOR_FLAG = 'YES'
;

UPDATE vpm_data.pm_skutagging_kids_product_master
SET TEEN_KEYWORD = CASE WHEN PRODUCT_NAME LIKE '%37%' THEN 'SIZE 37'
                        WHEN PRODUCT_NAME LIKE '%38%' THEN 'SIZE 38'
                        WHEN PRODUCT_NAME LIKE '%39%' THEN 'SIZE 39'
                        WHEN PRODUCT_NAME LIKE '%40%' THEN 'SIZE 40'
                        WHEN PRODUCT_NAME LIKE '%41%' THEN 'SIZE 41'
                        WHEN PRODUCT_NAME LIKE '%42%' THEN 'SIZE 42'
						ELSE TEEN_KEYWORD END
WHERE MOM_AND_KID_PRODUCT_FLAG = 1
AND REVISED_CATEGORY_LEVEL_1 = 'FASHION'
AND CLEANED_BRANDNAME NOT IN ('SANRIO','SANRIO LOCAL','THAI TIME','TSURUYA')
AND TEEN_FLAG = 'YES'
;

------------ SPORT ------------
UPDATE vpm_data.pm_skutagging_kids_product_master
SET JUNIOR_KEYWORD = CASE WHEN SUBCLASS_NAME LIKE '%SHORT%' THEN 'SHORT'
                       WHEN SUBCLASS_NAME LIKE '%SKIRT%' THEN 'SKIRT'
                       WHEN SUBCLASS_NAME LIKE '%TANK%' THEN 'TANK'
                       WHEN SUBCLASS_NAME LIKE '%T-SHIRT%' THEN 'T-SHIRT'
                       WHEN SUBCLASS_NAME LIKE '%SOCCER SHORT%' THEN 'SHORT'
                       WHEN CLASS_NAME LIKE '%CASUAL%' THEN 'CASUAL SHOES'
                       WHEN CLASS_NAME LIKE '%RUNNING%' THEN 'RUNNING SHOES'
                       WHEN CLASS_NAME LIKE '%SANDAL%' THEN 'SANDALS'
                       WHEN CLASS_NAME LIKE '%SOCCER%' THEN 'SOCCER SHOES'
                       WHEN CLASS_NAME LIKE '%TENNIS%' THEN 'TENNIS SHOES'
                       WHEN CLASS_NAME LIKE '%SPORT WEAR%' THEN 'SPORTWEAR'
                       ELSE 'JUNIOR SPORT SHOES' END
WHERE MOM_AND_KID_PRODUCT_FLAG = 1
AND REVISED_CATEGORY_LEVEL_1 = 'SPORT'
AND JUNIOR_FLAG = 'YES'
;

UPDATE vpm_data.pm_skutagging_kids_product_master
SET TEEN_KEYWORD = CASE WHEN SUBCLASS_NAME LIKE '%SHORT%' THEN 'SHORT'
                       WHEN SUBCLASS_NAME LIKE '%SKIRT%' THEN 'SKIRT'
                       WHEN SUBCLASS_NAME LIKE '%TANK%' THEN 'TANK'
                       WHEN SUBCLASS_NAME LIKE '%T-SHIRT%' THEN 'T-SHIRT'
                       WHEN SUBCLASS_NAME LIKE '%SOCCER SHORT%' THEN 'SHORT'
                       WHEN CLASS_NAME LIKE '%CASUAL%' THEN 'CASUAL SHOES'
                       WHEN CLASS_NAME LIKE '%RUNNING%' THEN 'RUNNING SHOES'
                       WHEN CLASS_NAME LIKE '%SANDAL%' THEN 'SANDALS'
                       WHEN CLASS_NAME LIKE '%SOCCER%' THEN 'SOCCER SHOES'
                       WHEN CLASS_NAME LIKE '%TENNIS%' THEN 'TENNIS SHOES'
                       WHEN CLASS_NAME LIKE '%SPORT WEAR%' THEN 'SPORTWEAR'
                       ELSE 'TEEN SPORT SHOES' END
WHERE MOM_AND_KID_PRODUCT_FLAG = 1
AND REVISED_CATEGORY_LEVEL_1 = 'SPORT'
AND TEEN_FLAG = 'YES'
;

------------ BOOK AND STATIONERY ------------
--STATIONERY
UPDATE vpm_data.pm_skutagging_kids_product_master
SET JUNIOR_KEYWORD = CASE WHEN SUBCLASS_NAME = 'FILES&BINDERS' THEN 'FILES&BINDERS'
                          WHEN SUBCLASS_NAME = 'NOTE PAD' THEN 'NOTE PAD'
						  WHEN SUBCLASS_NAME = 'PEN&PENCIL' THEN 'PEN&PENCIL'
						  WHEN SUBCLASS_NAME = 'STATIONERY' THEN 'OTHER STATIONERY'
						  ELSE JUNIOR_KEYWORD END
WHERE MOM_AND_KID_PRODUCT_FLAG = 1
AND REVISED_CATEGORY_LEVEL_2 = 'STATIONERY'
AND JUNIOR_FLAG = 'YES'
;

UPDATE vpm_data.pm_skutagging_kids_product_master
SET TEEN_KEYWORD = CASE WHEN SUBCLASS_NAME = 'FILES&BINDERS' THEN 'FILES&BINDERS'
                          WHEN SUBCLASS_NAME = 'NOTE PAD' THEN 'NOTE PAD'
						  WHEN SUBCLASS_NAME = 'PEN&PENCIL' THEN 'PEN&PENCIL'
						  WHEN SUBCLASS_NAME = 'STATIONERY' THEN 'OTHER STATIONERY'
						  ELSE TEEN_KEYWORD END
WHERE MOM_AND_KID_PRODUCT_FLAG = 1
AND REVISED_CATEGORY_LEVEL_2 = 'STATIONERY'
AND TEEN_FLAG = 'YES'
;

--BOOKS 
/*(AND OTHER CAT THAT HAS AGE_RANGE )*/
UPDATE vpm_data.pm_skutagging_kids_product_master
SET PRESCHOOL_KEYWORD = CASE WHEN AGE_RANGE  = '3 TO 5 YEARS'  THEN '3-5Y'
                             WHEN AGE_RANGE  = '3 TO 6 YEARS' THEN '3-6Y'
							 WHEN AGE_RANGE  = '3 TO 9 YEARS' THEN '3-9Y'
							 WHEN AGE_RANGE  = '4 TO 8 YEARS' THEN '4-8Y'
							 WHEN AGE_RANGE  = '5 TO 9 YEARS' THEN '5-9Y'
							 WHEN AGE_RANGE  = '6 TO 9 YEARS' THEN '6-9Y'
							 ELSE PRESCHOOL_KEYWORD END
WHERE MOM_AND_KID_PRODUCT_FLAG = 1
AND PRESCHOOL_FLAG = 'YES'
AND PRESCHOOL_KEYWORD IS NULL
;

UPDATE vpm_data.pm_skutagging_kids_product_master
SET JUNIOR_KEYWORD = CASE WHEN AGE_RANGE  = '3 TO 9 YEARS'   THEN '3-9Y'
						  WHEN AGE_RANGE  = '4 TO 8 YEARS' THEN '4-8Y'
						  WHEN AGE_RANGE  = '5 TO 9 YEARS' THEN '5-9Y'
						  WHEN AGE_RANGE  = '6 TO 9 YEARS' THEN '6-9Y'
						  WHEN AGE_RANGE  = '6 TO 12 YEARS' THEN '6-12Y'
						  WHEN AGE_RANGE  = '9 TO 12 YEARS' THEN '9-12Y'
						  WHEN AGE_RANGE  = '12 TO 13 YEARS' THEN '12-13Y'
						  WHEN AGE_RANGE  = '12 TO 15 YEARS' THEN '12-15Y'
						  WHEN AGE_RANGE  = '12 YEARS AND OLDER' THEN '12Y+'
						  ELSE JUNIOR_KEYWORD END
WHERE MOM_AND_KID_PRODUCT_FLAG = 1
AND JUNIOR_FLAG = 'YES'
AND JUNIOR_KEYWORD IS NULL
;

UPDATE vpm_data.pm_skutagging_kids_product_master
SET TEEN_KEYWORD = CASE WHEN AGE_RANGE  = '12 TO 15 YEARS'   THEN '12-15Y'
						WHEN AGE_RANGE  = '12 YEARS AND OLDER' THEN '12Y+'
						WHEN AGE_RANGE  = '15 TO 18 YEARS' THEN '15-18Y'
						ELSE TEEN_KEYWORD END
WHERE MOM_AND_KID_PRODUCT_FLAG = 1
AND TEEN_FLAG = 'YES'
AND TEEN_KEYWORD IS NULL
;

UPDATE vpm_data.pm_skutagging_kids_product_master
SET PRESCHOOL_KEYWORD = CASE WHEN PRODUCT_NAME LIKE '%˹ٹԴ%' THEN '˹ٹԴ'
                             WHEN PRODUCT_NAME LIKE '%�ҡ���%' THEN '�ҡ���'
							 WHEN PRODUCT_NAME LIKE '%�� �� ��%' THEN 'ABC'
							 WHEN PRODUCT_NAME LIKE '%�.%' THEN '�-�'
							 WHEN PRODUCT_NAME LIKE '%��맡��%' THEN '��맡��'
							 WHEN PRODUCT_NAME LIKE '%�-�%' THEN '�-�'
							 WHEN (PRODUCT_NAME LIKE '%�ͺ���%' AND PRODUCT_NAME LIKE '%�.%') THEN '�ͺ��һ�ж�'
							 WHEN PRODUCT_NAME LIKE '%ABC%' THEN 'ABC'
							 WHEN PRODUCT_NAME LIKE '%���%' THEN '�-�'
							 WHEN PRODUCT_NAME LIKE '%A-Z%' THEN 'A-Z'
							 WHEN PRODUCT_NAME LIKE '%�ѵ�%' THEN 'FLASH CARDS'
							 WHEN PRODUCT_NAME LIKE '%��ѭ���%' THEN '��ѭ���'
							 WHEN PRODUCT_NAME LIKE '%�ͺ���%�%' THEN '�ͺ��һ�ж�'
							 WHEN PRODUCT_NAME LIKE '%CARD%' THEN 'FLASH CARDS'
							 WHEN PRODUCT_NAME LIKE '%ALPHABETS%' THEN 'ALPHABETS'
							 WHEN PRODUCT_NAME LIKE '%3 �Ǻ%' THEN '3Y'
							 WHEN PRODUCT_NAME LIKE '%4 �Ǻ%' THEN '4Y'
							 WHEN PRODUCT_NAME LIKE '%5 �Ǻ%' THEN '5Y'
							 WHEN PRODUCT_NAME LIKE '%6 �Ǻ%' THEN '6Y'
							 WHEN PRODUCT_NAME LIKE '%5-7 �Ǻ%' THEN '5-7Y'
							 WHEN PRODUCT_NAME LIKE '%͹غ��%' THEN 'KINDERGARTEN'
							 WHEN PRODUCT_NAME LIKE '%�ѡ����¹��%' THEN '�ѡ����¹��'
							 WHEN PRODUCT_NAME LIKE '%4 ��%' THEN '4Y'
							 WHEN PRODUCT_NAME LIKE '%5 ��%' THEN '5Y'
							 WHEN PRODUCT_NAME LIKE '%5��%' THEN '5Y'
							 WHEN PRODUCT_NAME LIKE '%6 ��%' THEN '6Y'
							 WHEN PRODUCT_NAME LIKE '%123%' THEN '123'
							 WHEN PRODUCT_NAME LIKE '%���%' THEN '123'
							 WHEN PRODUCT_NAME LIKE '%������%' THEN '������'
							 -- PRESCHOOL AND JUNIOR
							 WHEN PRODUCT_NAME LIKE '%����%' THEN 'POSTER'
							 WHEN PRODUCT_NAME LIKE '%POSTER%' THEN 'POSTER'
							 WHEN PRODUCT_NAME LIKE '%�к����%' THEN 'COLORING'
							 WHEN PRODUCT_NAME LIKE '%ú�%' THEN 'COLORING'
							 WHEN PRODUCT_NAME LIKE '%COLOUR%' THEN 'COLORING'
							 WHEN PRODUCT_NAME LIKE '%STICKER%' THEN 'STICKER'
							 WHEN PRODUCT_NAME LIKE '%ʵԡ��%' THEN 'STICKER'
							 WHEN PRODUCT_NAME LIKE '%ʵ������%' THEN 'STICKER'
							 WHEN PRODUCT_NAME LIKE '%����%' THEN 'FLASH CARD'
							 WHEN PRODUCT_NAME LIKE '%�ѵ��%' THEN '�ѵ��'
							 WHEN PRODUCT_NAME LIKE '%�ѡ��%' THEN '�ѡ��'
						     WHEN PRODUCT_NAME LIKE '%�ѧ���%' THEN '�����ѧ���'
						     WHEN PRODUCT_NAME LIKE '%�Ѿ��%' THEN '���Ѿ��'
						     WHEN PRODUCT_NAME LIKE '%��ͧ��%' THEN '��ͧ��'
						     WHEN PRODUCT_NAME LIKE '%��ꡨ��%' THEN '��ꡨ��'
						     WHEN PRODUCT_NAME LIKE '%��%' THEN '������'
						     WHEN PRODUCT_NAME LIKE '%��Ե%' THEN '��Ե��ʵ��'
						     WHEN PRODUCT_NAME LIKE '%���%' THEN '�١���'
						     WHEN PRODUCT_NAME LIKE '%��%' THEN '��'
						     WHEN PRODUCT_NAME LIKE '%��%' THEN '��'
						     WHEN PRODUCT_NAME LIKE '%��駨͡%' THEN '��駨͡'
						     WHEN PRODUCT_NAME LIKE '%��ҧ%' THEN '��ҧ'
						     WHEN PRODUCT_NAME LIKE '%��֡%' THEN '��֡'
						     WHEN PRODUCT_NAME LIKE '%���%' THEN '���'
						     WHEN PRODUCT_NAME LIKE '%���%' THEN '�١���'
						     WHEN PRODUCT_NAME LIKE '%���%' THEN '�١���'
						     WHEN PRODUCT_NAME LIKE '%�����%' THEN '�����'
						     WHEN PRODUCT_NAME LIKE '%��ʻ%' THEN '��ʻ'
						     WHEN PRODUCT_NAME LIKE '%KUMON%' THEN 'KUMON'
						     WHEN PRODUCT_NAME LIKE '%WORD%' THEN '���Ѿ��'
						     WHEN PRODUCT_NAME LIKE '%Ẻ���¹%' THEN 'Ẻ���¹'
						     WHEN PRODUCT_NAME LIKE '%�Ҵ�%' THEN '�Ҵ�'
							 WHEN PRODUCT_NAME LIKE '%�������%' THEN '������õ��'
						     WHEN PRODUCT_NAME LIKE '%NUMBER%' THEN 'NUMBER'
						     WHEN PRODUCT_NAME LIKE '%LITTLE PONY%' THEN 'LITTLE PONY'
						     WHEN PRODUCT_NAME LIKE '%��е���%' THEN '��е���'
						     WHEN PRODUCT_NAME LIKE '%���˭ԧ%' THEN '���˭ԧ'
						     WHEN PRODUCT_NAME LIKE '%��Ҫ��%' THEN '��Ҫ��'
						     WHEN PRODUCT_NAME LIKE '%��%' THEN '�١��'
						     WHEN PRODUCT_NAME LIKE '%��ҧ���%' THEN '��ҧ���'
						     WHEN PRODUCT_NAME LIKE '%���ԡ�%' THEN '���ԡ�'
						     WHEN PRODUCT_NAME LIKE '%˹�%' THEN '˹ٹ���'
						     WHEN PRODUCT_NAME LIKE '%���Ǩ%' THEN '���Ǩ'
						     WHEN PRODUCT_NAME LIKE '%�������%' THEN '�������'
						     WHEN PRODUCT_NAME LIKE '%�������%' THEN '�������'
						     WHEN PRODUCT_NAME LIKE '%��Ե��ʵ��%' THEN '��Ե��ʵ��'
						     WHEN PRODUCT_NAME LIKE '%����%' THEN '����'
						     WHEN PRODUCT_NAME LIKE '%�ԧ%' THEN '�ԧ'
						     WHEN PRODUCT_NAME LIKE '%�ԧ�%' THEN '�ԧ�'
						     WHEN PRODUCT_NAME LIKE '%��%' THEN '��'
						     WHEN PRODUCT_NAME LIKE '%����%' THEN '����'
						     WHEN PRODUCT_NAME LIKE '%���ҿ%' THEN '���ҿ'
						     WHEN PRODUCT_NAME LIKE '%��%' THEN '��'
						     WHEN PRODUCT_NAME LIKE '%��%' THEN '��'
						     WHEN PRODUCT_NAME LIKE '%PEPPA PIG%' THEN 'PEPPA PIG'
						     WHEN PRODUCT_NAME LIKE '%PAPPA PIG%' THEN 'PEPPA PIG'
						     WHEN PRODUCT_NAME LIKE '%������%' THEN '������'
						     WHEN PRODUCT_NAME LIKE '%�Ѳ��%' THEN '�Ѳ��'
						     WHEN PRODUCT_NAME LIKE '%DISNEY%' THEN 'DISNEY'
						     WHEN PRODUCT_NAME LIKE '%FROZEN%' THEN 'FROZEN'
						     WHEN PRODUCT_NAME LIKE '%IQ%' THEN 'IQ'
						     WHEN PRODUCT_NAME LIKE '%�ͤ��%' THEN 'IQ'
						     WHEN PRODUCT_NAME LIKE '%�Ҵ�ٻ%' THEN '�Ҵ�ٻ'
						     WHEN PRODUCT_NAME LIKE '%�ء����ѹ%' THEN '�ء����ѹ'
						     WHEN PRODUCT_NAME LIKE '%������ѡ��%' THEN '������ѡ��'
						     WHEN PRODUCT_NAME LIKE '%�Էҹ%' THEN '�Էҹ'
						     WHEN PRODUCT_NAME LIKE '%����ͧ����%' THEN '����ͧ����'
							 ELSE PRESCHOOL_KEYWORD END
WHERE MOM_AND_KID_PRODUCT_FLAG = 1
AND REVISED_CATEGORY_LEVEL_3 = 'CHILDREN BOOKS' 
AND PRESCHOOL_FLAG = 'YES'
AND PRESCHOOL_KEYWORD IS NULL
;

UPDATE vpm_data.pm_skutagging_kids_product_master
SET JUNIOR_KEYWORD = CASE WHEN PRODUCT_NAME LIKE '%�.%' THEN '��ж�'
                          WHEN PRODUCT_NAME LIKE '%��ж�%' THEN '��ж�'
						  WHEN (PRODUCT_NAME LIKE '%�ͺ���%' AND PRODUCT_NAME LIKE '%�.1%') THEN '�ͺ����.1'
						  WHEN PRODUCT_NAME LIKE '%7 ��%' THEN '7Y'
						  WHEN PRODUCT_NAME LIKE '%�ǡ%' THEN '�ǡ'
						  WHEN PRODUCT_NAME LIKE '%ź%' THEN 'ź'
						  WHEN PRODUCT_NAME LIKE '%�ٳ%' THEN '�ٳ'
						  WHEN PRODUCT_NAME LIKE '%������%' THEN '���'
						  -- PRESCHOOL AND JUNIOR
						  WHEN PRODUCT_NAME LIKE '%����%' THEN 'POSTER'
						  WHEN PRODUCT_NAME LIKE '%POSTER%' THEN 'POSTER'
						  WHEN PRODUCT_NAME LIKE '%�к����%' THEN 'COLORING'
						  WHEN PRODUCT_NAME LIKE '%ú�%' THEN 'COLORING'
						  WHEN PRODUCT_NAME LIKE '%COLOUR%' THEN 'COLORING'
						  WHEN PRODUCT_NAME LIKE '%STICKER%' THEN 'STICKER'
						  WHEN PRODUCT_NAME LIKE '%ʵԡ��%' THEN 'STICKER'
						  WHEN PRODUCT_NAME LIKE '%ʵ������%' THEN 'STICKER'
						  WHEN PRODUCT_NAME LIKE '%����%' THEN 'FLASH CARD'
						  WHEN PRODUCT_NAME LIKE '%�ѵ��%' THEN '�ѵ��'
						  WHEN PRODUCT_NAME LIKE '%�ѡ��%' THEN '�ѡ��'
						  WHEN PRODUCT_NAME LIKE '%�ѧ���%' THEN '�����ѧ���'
						  WHEN PRODUCT_NAME LIKE '%�Ѿ��%' THEN '���Ѿ��'
						  WHEN PRODUCT_NAME LIKE '%��ͧ��%' THEN '��ͧ��'
						  WHEN PRODUCT_NAME LIKE '%��ꡨ��%' THEN '��ꡨ��'
						  WHEN PRODUCT_NAME LIKE '%��%' THEN '������'
						  WHEN PRODUCT_NAME LIKE '%��Ե%' THEN '��Ե��ʵ��'
						  WHEN PRODUCT_NAME LIKE '%���%' THEN '�١���'
						  WHEN PRODUCT_NAME LIKE '%��%' THEN '��'
						  WHEN PRODUCT_NAME LIKE '%��%' THEN '��'
						  WHEN PRODUCT_NAME LIKE '%��駨͡%' THEN '��駨͡'
						  WHEN PRODUCT_NAME LIKE '%��ҧ%' THEN '��ҧ'
						  WHEN PRODUCT_NAME LIKE '%��֡%' THEN '��֡'
						  WHEN PRODUCT_NAME LIKE '%���%' THEN '���'
						  WHEN PRODUCT_NAME LIKE '%���%' THEN '�١���'
						  WHEN PRODUCT_NAME LIKE '%���%' THEN '�١���'
						  WHEN PRODUCT_NAME LIKE '%�����%' THEN '�����'
						  WHEN PRODUCT_NAME LIKE '%��ʻ%' THEN '��ʻ'
						  WHEN PRODUCT_NAME LIKE '%KUMON%' THEN 'KUMON'
						  WHEN PRODUCT_NAME LIKE '%WORD%' THEN '���Ѿ��'
						  WHEN PRODUCT_NAME LIKE '%Ẻ���¹%' THEN 'Ẻ���¹'
						  WHEN PRODUCT_NAME LIKE '%�Ҵ�%' THEN '�Ҵ�'
						  WHEN PRODUCT_NAME LIKE '%�������%' THEN '������õ��'
						  WHEN PRODUCT_NAME LIKE '%NUMBER%' THEN 'NUMBER'
						  WHEN PRODUCT_NAME LIKE '%LITTLE PONY%' THEN 'LITTLE PONY'
						  WHEN PRODUCT_NAME LIKE '%��е���%' THEN '��е���'
						  WHEN PRODUCT_NAME LIKE '%���˭ԧ%' THEN '���˭ԧ'
						  WHEN PRODUCT_NAME LIKE '%��Ҫ��%' THEN '��Ҫ��'
						  WHEN PRODUCT_NAME LIKE '%��%' THEN '�١��'
						  WHEN PRODUCT_NAME LIKE '%��ҧ���%' THEN '��ҧ���'
						  WHEN PRODUCT_NAME LIKE '%���ԡ�%' THEN '���ԡ�'
						  WHEN PRODUCT_NAME LIKE '%˹�%' THEN '˹ٹ���'
						  WHEN PRODUCT_NAME LIKE '%���Ǩ%' THEN '���Ǩ'
						  WHEN PRODUCT_NAME LIKE '%�������%' THEN '�������'
						  WHEN PRODUCT_NAME LIKE '%�������%' THEN '�������'
						  WHEN PRODUCT_NAME LIKE '%��Ե��ʵ��%' THEN '��Ե��ʵ��'
						  WHEN PRODUCT_NAME LIKE '%����%' THEN '����'
						  WHEN PRODUCT_NAME LIKE '%�ԧ%' THEN '�ԧ'
						  WHEN PRODUCT_NAME LIKE '%�ԧ�%' THEN '�ԧ�'
						  WHEN PRODUCT_NAME LIKE '%��%' THEN '��'
						  WHEN PRODUCT_NAME LIKE '%����%' THEN '����'
						  WHEN PRODUCT_NAME LIKE '%���ҿ%' THEN '���ҿ'
						  WHEN PRODUCT_NAME LIKE '%��%' THEN '��'
						  WHEN PRODUCT_NAME LIKE '%��%' THEN '��'
						  WHEN PRODUCT_NAME LIKE '%PEPPA PIG%' THEN 'PEPPA PIG'
						  WHEN PRODUCT_NAME LIKE '%PAPPA PIG%' THEN 'PEPPA PIG'
						  WHEN PRODUCT_NAME LIKE '%������%' THEN '������'
						  WHEN PRODUCT_NAME LIKE '%�Ѳ��%' THEN '�Ѳ��'
						  WHEN PRODUCT_NAME LIKE '%DISNEY%' THEN 'DISNEY'
						  WHEN PRODUCT_NAME LIKE '%FROZEN%' THEN 'FROZEN'
						  WHEN PRODUCT_NAME LIKE '%IQ%' THEN 'IQ'
						  WHEN PRODUCT_NAME LIKE '%�ͤ��%' THEN 'IQ'
						  WHEN PRODUCT_NAME LIKE '%�Ҵ�ٻ%' THEN '�Ҵ�ٻ'
						  WHEN PRODUCT_NAME LIKE '%�ء����ѹ%' THEN '�ء����ѹ'
						  WHEN PRODUCT_NAME LIKE '%������ѡ��%' THEN '������ѡ��'
						  WHEN PRODUCT_NAME LIKE '%�Էҹ%' THEN '�Էҹ'
						  WHEN PRODUCT_NAME LIKE '%����ͧ����%' THEN '����ͧ����'
						  ELSE JUNIOR_KEYWORD END
WHERE MOM_AND_KID_PRODUCT_FLAG = 1
AND REVISED_CATEGORY_LEVEL_3 = 'CHILDREN BOOKS' 
AND JUNIOR_FLAG = 'YES'
AND JUNIOR_KEYWORD IS NULL
;

UPDATE vpm_data.pm_skutagging_kids_product_master 
SET TEEN_KEYWORD = CASE WHEN PRODUCT_NAME LIKE '%BIOLOGY%' THEN 'BIOLOGY'
                        WHEN PRODUCT_NAME LIKE '%�Ѹ��%' THEN '�Ѹ��'
						WHEN PRODUCT_NAME LIKE '%�.����%' THEN '�.����'
						WHEN PRODUCT_NAME LIKE '%�.��%' THEN '�.��'
						WHEN PRODUCT_NAME LIKE '%���ԡ%' THEN '���ԡ'
						WHEN PRODUCT_NAME LIKE '%����Է��%' THEN 'BIOLOGY'
						WHEN PRODUCT_NAME LIKE '%�.%' THEN '�Ѹ��'
						WHEN PRODUCT_NAME LIKE '%O-NET%' THEN 'O-NET'
						WHEN PRODUCT_NAME LIKE '%A-NET%' THEN 'A-NET'
						WHEN PRODUCT_NAME LIKE '%���%' THEN 'CHEMISTRY'
						WHEN PRODUCT_NAME LIKE '%GAT2%' THEN 'GAT'
						WHEN PRODUCT_NAME LIKE '%9 �Ԫ����ѭ%' THEN '9 �Ԫ����ѭ'
						WHEN PRODUCT_NAME LIKE '%TCAS%' THEN 'TCAS'
						WHEN PRODUCT_NAME LIKE '%�ѭ��%' THEN '�ѭ��'
						WHEN PRODUCT_NAME LIKE '%������%' THEN '������'
						WHEN PRODUCT_NAME LIKE '%GRAMMAR%' THEN 'GRAMMAR'
						ELSE TEEN_KEYWORD END
WHERE MOM_AND_KID_PRODUCT_FLAG = 1
AND REVISED_CATEGORY_LEVEL_3 = 'CHILDREN BOOKS' 
AND TEEN_FLAG = 'YES'
AND TEEN_KEYWORD IS NULL
;

UPDATE vpm_data.pm_skutagging_kids_product_master
SET PRESCHOOL_KEYWORD = CASE WHEN CLASS_NAME LIKE '%AGES 4-8%' THEN '4-8Y'
                             WHEN CLASS_NAME LIKE '%BABY - PRESCHOOL%' THEN 'BABY - PRESCHOOL BOOKS'
							 WHEN CLASS_NAME LIKE '%KINDERGARDEN%' THEN 'KINDERGARDEN - PRIMARY BOOKS'
							 WHEN CLASS_NAME LIKE '%PRE-SCHOOL%' THEN 'PRESCHOOL BOOKS'
							 ELSE PRESCHOOL_KEYWORD END
WHERE MOM_AND_KID_PRODUCT_FLAG = 1
AND REVISED_CATEGORY_LEVEL_3 = 'CHILDREN BOOKS'
AND PRESCHOOL_FLAG = 'YES'
AND PRESCHOOL_KEYWORD IS NULL
;

UPDATE vpm_data.pm_skutagging_kids_product_master
SET JUNIOR_KEYWORD = CASE WHEN CLASS_NAME LIKE '%AGES 4-8%' THEN '4-8Y'
						  WHEN CLASS_NAME LIKE '%KINDERGARDEN - PRIMARY%' THEN 'KINDERGARDEN - PRIMARY BOOKS'
						  WHEN CLASS_NAME LIKE '%PRIMARY SCHOOL%' THEN 'PRIMARY SCHOOL BOOKS'
						  WHEN CLASS_NAME LIKE '%AGES 9-12%' THEN '9-12Y'
					      ELSE JUNIOR_KEYWORD END
WHERE MOM_AND_KID_PRODUCT_FLAG = 1
AND REVISED_CATEGORY_LEVEL_3 = 'CHILDREN BOOKS'
AND JUNIOR_FLAG = 'YES'
AND JUNIOR_KEYWORD IS NULL
;

UPDATE vpm_data.pm_skutagging_kids_product_master
SET TEEN_KEYWORD = CASE WHEN CLASS_NAME LIKE '%JUNIOR HIGH SCHOOL(M1-3)%' THEN 'JUNIOR HIGH SCHOOL BOOKS'
                        WHEN CLASS_NAME LIKE '%SENIOR HIGH SCHOOL(M4-6)%' THEN 'SENIOR HIGH SCHOOL BOOKS'
						WHEN CLASS_NAME LIKE '%SENIOR HIGH SCHOOL(M4-6)%' THEN 'TEENAGE BOOKS'
						WHEN SUBDEPT_NAME LIKE '%TEENAGE%' THEN 'TEENAGE BOOKS'
						ELSE TEEN_KEYWORD END
WHERE MOM_AND_KID_PRODUCT_FLAG = 1
AND REVISED_CATEGORY_LEVEL_3 = 'CHILDREN BOOKS'
AND TEEN_FLAG = 'YES'
AND TEEN_KEYWORD IS NULL
;

UPDATE vpm_data.pm_skutagging_kids_product_master
SET PRESCHOOL_KEYWORD = 'PRESCHOOL BOOKS'
   ,JUNIOR_KEYWORD = 'JUNIOR BOOKS'
WHERE MOM_AND_KID_PRODUCT_FLAG = 1
AND REVISED_CATEGORY_LEVEL_3 = 'CHILDREN BOOKS' 
AND ((PRESCHOOL_FLAG = 'YES' AND PRESCHOOL_KEYWORD IS NULL) 
      OR (JUNIOR_FLAG = 'YES' AND JUNIOR_KEYWORD IS NULL))
;

------------ TOYS ------------
--BUILD-A-BEAR WORKSHOP
UPDATE vpm_data.pm_skutagging_kids_product_master
SET PRESCHOOL_KEYWORD = 'DOLL'
   ,JUNIOR_KEYWORD = 'DOLL'
   ,TEEN_KEYWORD = 'DOLL'
WHERE MOM_AND_KID_PRODUCT_FLAG = 1
AND REVISED_CATEGORY_LEVEL_2 = 'TOYS'
AND (PRESCHOOL_FLAG = 'YES' OR JUNIOR_FLAG = 'YES' OR TEEN_FLAG = 'YES')
AND (SUBDEPT_NAME = 'BUILD-A-BEAR WORKSHOP' 
     OR PRODUCT_NAME LIKE '%��꡵�%'
	 OR PRODUCT_NAME LIKE '%DOLL%'
	 OR PRODUCT_NAME LIKE '%TEDDY%')
;

--GM/NON FMCG
UPDATE vpm_data.pm_skutagging_kids_product_master
SET PRESCHOOL_KEYWORD = CASE WHEN PRODUCT_NAME LIKE '%TOYS SET%' THEN 'TOY SET'
                             WHEN PRODUCT_NAME LIKE '%BOY TOYS%' THEN 'BOY TOYS'
                             WHEN PRODUCT_NAME LIKE '%GIRL TOY%' THEN 'GIRL TOYS'
                             WHEN PRODUCT_NAME LIKE '%TRICYCLE%' THEN 'TRICYCLE'
                             WHEN PRODUCT_NAME LIKE '%BEN10%' THEN 'BEN10'
                             WHEN PRODUCT_NAME LIKE '%SOLDIER%' THEN 'SOLDIER'
                             WHEN PRODUCT_NAME LIKE '%PLANE%' THEN 'PLANE'
                             WHEN PRODUCT_NAME LIKE '%SPIDERMAN%' THEN 'SPIDERMAN'
                             WHEN PRODUCT_NAME LIKE '%GUN%' THEN 'GUN'
                             WHEN PRODUCT_NAME LIKE '%PIGGY BANK%' THEN 'PIGGYBANK'
                             WHEN PRODUCT_NAME LIKE '%WEAPON%' THEN 'WEAPON'
                             WHEN PRODUCT_NAME LIKE '%DOCTOR%' THEN 'DOCTOR'
                             WHEN PRODUCT_NAME LIKE '%BLOCK%' THEN 'BLOCKS'
                             WHEN PRODUCT_NAME LIKE '%DOUGH%' THEN 'DOUGH'
                             WHEN PRODUCT_NAME LIKE '%DOH%' THEN 'DOUGH'
                             WHEN PRODUCT_NAME LIKE '%ROBOT%' THEN 'ROBOT'
                             WHEN PRODUCT_NAME LIKE '%FIGURE%' THEN 'FIGURE'
                             WHEN PRODUCT_NAME LIKE '%KITCHEN%' THEN 'KITCHEN'
                             WHEN PRODUCT_NAME LIKE '%MAGIC%' THEN 'MAGIC'
                             WHEN PRODUCT_NAME LIKE '%CD%' THEN 'CD'
                             WHEN PRODUCT_NAME LIKE '%POOL%' THEN 'POOL'
                             WHEN PRODUCT_NAME LIKE '%MAGIC%' THEN 'MAGIC'
                             WHEN PRODUCT_NAME LIKE '%GLASS%' THEN 'GLASSES'
                             WHEN PRODUCT_NAME LIKE '%MAKE UP%' THEN 'MAKE UP'
                             WHEN PRODUCT_NAME LIKE '%MAKEUP%' THEN 'MAKE UP'
                             WHEN PRODUCT_NAME LIKE '%GOLF%' THEN 'GOLF'
                             WHEN PRODUCT_NAME LIKE '%BALL%' THEN 'BALL'
                             WHEN PRODUCT_NAME LIKE '%PONY%' THEN 'PONY'
                             WHEN PRODUCT_NAME LIKE '%GAME BOX%' THEN 'GAME BOX'
                             WHEN PRODUCT_NAME LIKE '%MOBILE%' THEN 'MOBILE'
                             WHEN PRODUCT_NAME LIKE '%SWORD%' THEN 'SWORD'
                             WHEN PRODUCT_NAME LIKE '%ANGRY%' THEN 'ANGRY BIRD'
                             WHEN PRODUCT_NAME LIKE '%SHOOTER%' THEN 'SHOOTER'
                             WHEN PRODUCT_NAME LIKE '%DORAMON%' THEN 'DORAEMON'
                             WHEN PRODUCT_NAME LIKE '%DORAEMON%' THEN 'DORAEMON'
                             WHEN PRODUCT_NAME LIKE '%DOLL%' THEN 'DOLL'
                             WHEN PRODUCT_NAME LIKE '%BOXING%' THEN 'BOXING'
                             WHEN PRODUCT_NAME LIKE '%ANIMAL%' THEN 'ANIMAL'
                             WHEN PRODUCT_NAME LIKE '%TEA SET%' THEN 'TEA SET'
                             WHEN PRODUCT_NAME LIKE '%PET%' THEN 'PETS'
                             WHEN PRODUCT_NAME LIKE '%MUSIC%' THEN 'MUSIC'
                             WHEN PRODUCT_NAME LIKE '%MICKEY%' THEN 'MICKEY'
                             WHEN PRODUCT_NAME LIKE '%AIRCRAFT%' THEN 'AIRCRAFT'
                             WHEN PRODUCT_NAME LIKE '%FRISBEE%' THEN 'FRISBEE'
                             WHEN PRODUCT_NAME LIKE '%ROCKING%' THEN 'ROCKING'
                             WHEN PRODUCT_NAME LIKE '%STAMP%' THEN 'STAMP'
                             WHEN PRODUCT_NAME LIKE '%SAND%' THEN 'SAND'
                             WHEN PRODUCT_NAME LIKE '%CONSTRUCT%' THEN 'CONSTRUCTION'
                             WHEN PRODUCT_NAME LIKE '%GAME%' THEN 'GAME'
							 ELSE 'PRESCHOOL TOYS' END
WHERE MOM_AND_KID_PRODUCT_FLAG = 1
AND REVISED_CATEGORY_LEVEL_2 = 'TOYS'
AND SUBDEPT_NAME = 'GM/NON FMCG'
AND PRESCHOOL_FLAG = 'YES'
AND PRESCHOOL_KEYWORD IS NULL
;

UPDATE vpm_data.pm_skutagging_kids_product_master
SET JUNIOR_KEYWORD = CASE WHEN PRODUCT_NAME LIKE '%TOYS SET%' THEN 'TOY SET'
                             WHEN PRODUCT_NAME LIKE '%BOY TOYS%' THEN 'BOY TOYS'
                             WHEN PRODUCT_NAME LIKE '%GIRL TOY%' THEN 'GIRL TOYS'
                             WHEN PRODUCT_NAME LIKE '%TRICYCLE%' THEN 'TRICYCLE'
                             WHEN PRODUCT_NAME LIKE '%BEN10%' THEN 'BEN10'
                             WHEN PRODUCT_NAME LIKE '%SOLDIER%' THEN 'SOLDIER'
                             WHEN PRODUCT_NAME LIKE '%PLANE%' THEN 'PLANE'
                             WHEN PRODUCT_NAME LIKE '%SPIDERMAN%' THEN 'SPIDERMAN'
                             WHEN PRODUCT_NAME LIKE '%GUN%' THEN 'GUN'
                             WHEN PRODUCT_NAME LIKE '%PIGGY BANK%' THEN 'PIGGYBANK'
                             WHEN PRODUCT_NAME LIKE '%WEAPON%' THEN 'WEAPON'
                             WHEN PRODUCT_NAME LIKE '%DOCTOR%' THEN 'DOCTOR'
                             WHEN PRODUCT_NAME LIKE '%BLOCK%' THEN 'BLOCKS'
                             WHEN PRODUCT_NAME LIKE '%DOUGH%' THEN 'DOUGH'
                             WHEN PRODUCT_NAME LIKE '%DOH%' THEN 'DOUGH'
                             WHEN PRODUCT_NAME LIKE '%ROBOT%' THEN 'ROBOT'
                             WHEN PRODUCT_NAME LIKE '%FIGURE%' THEN 'FIGURE'
                             WHEN PRODUCT_NAME LIKE '%KITCHEN%' THEN 'KITCHEN'
                             WHEN PRODUCT_NAME LIKE '%MAGIC%' THEN 'MAGIC'
                             WHEN PRODUCT_NAME LIKE '%CD%' THEN 'CD'
                             WHEN PRODUCT_NAME LIKE '%POOL%' THEN 'POOL'
                             WHEN PRODUCT_NAME LIKE '%MAGIC%' THEN 'MAGIC'
                             WHEN PRODUCT_NAME LIKE '%GLASS%' THEN 'GLASSES'
                             WHEN PRODUCT_NAME LIKE '%MAKE UP%' THEN 'MAKE UP'
                             WHEN PRODUCT_NAME LIKE '%MAKEUP%' THEN 'MAKE UP'
                             WHEN PRODUCT_NAME LIKE '%GOLF%' THEN 'GOLF'
                             WHEN PRODUCT_NAME LIKE '%BALL%' THEN 'BALL'
                             WHEN PRODUCT_NAME LIKE '%PONY%' THEN 'PONY'
                             WHEN PRODUCT_NAME LIKE '%GAME BOX%' THEN 'GAME BOX'
                             WHEN PRODUCT_NAME LIKE '%MOBILE%' THEN 'MOBILE'
                             WHEN PRODUCT_NAME LIKE '%SWORD%' THEN 'SWORD'
                             WHEN PRODUCT_NAME LIKE '%ANGRY%' THEN 'ANGRY BIRD'
                             WHEN PRODUCT_NAME LIKE '%SHOOTER%' THEN 'SHOOTER'
                             WHEN PRODUCT_NAME LIKE '%DORAMON%' THEN 'DORAEMON'
                             WHEN PRODUCT_NAME LIKE '%DORAEMON%' THEN 'DORAEMON'
                             WHEN PRODUCT_NAME LIKE '%DOLL%' THEN 'DOLL'
                             WHEN PRODUCT_NAME LIKE '%BOXING%' THEN 'BOXING'
                             WHEN PRODUCT_NAME LIKE '%ANIMAL%' THEN 'ANIMAL'
                             WHEN PRODUCT_NAME LIKE '%TEA SET%' THEN 'TEA SET'
                             WHEN PRODUCT_NAME LIKE '%PET%' THEN 'PETS'
                             WHEN PRODUCT_NAME LIKE '%MUSIC%' THEN 'MUSIC'
                             WHEN PRODUCT_NAME LIKE '%MICKEY%' THEN 'MICKEY'
                             WHEN PRODUCT_NAME LIKE '%AIRCRAFT%' THEN 'AIRCRAFT'
                             WHEN PRODUCT_NAME LIKE '%FRISBEE%' THEN 'FRISBEE'
                             WHEN PRODUCT_NAME LIKE '%ROCKING%' THEN 'ROCKING'
                             WHEN PRODUCT_NAME LIKE '%STAMP%' THEN 'STAMP'
                             WHEN PRODUCT_NAME LIKE '%SAND%' THEN 'SAND'
                             WHEN PRODUCT_NAME LIKE '%CONSTRUCT%' THEN 'CONSTRUCTION'
                             WHEN PRODUCT_NAME LIKE '%GAME%' THEN 'GAME'
							 ELSE 'JUNIOR TOYS' END
WHERE MOM_AND_KID_PRODUCT_FLAG = 1
AND REVISED_CATEGORY_LEVEL_2 = 'TOYS'
AND SUBDEPT_NAME = 'GM/NON FMCG'
AND JUNIOR_FLAG = 'YES'
AND JUNIOR_KEYWORD IS NULL
;

--EDUCATIONAL TOY
UPDATE vpm_data.pm_skutagging_kids_product_master
SET PRESCHOOL_KEYWORD = 'EDUCATIONAL TOY'
   ,JUNIOR_KEYWORD = 'EDUCATIONAL TOY'
WHERE MOM_AND_KID_PRODUCT_FLAG = 1
AND REVISED_CATEGORY_LEVEL_2 = 'TOYS'
AND SUBDEPT_NAME = 'EDUCATION TOY' 
AND ((PRESCHOOL_FLAG = 'YES' AND PRESCHOOL_KEYWORD IS NULL) 
      OR (JUNIOR_FLAG = 'YES' AND JUNIOR_KEYWORD IS NULL))
;

--GAME & BUILDING (SUBCLASS NOT IN ('BOARD GAME','CARD GAME'))
UPDATE vpm_data.pm_skutagging_kids_product_master
SET PRESCHOOL_KEYWORD = CASE WHEN SUBCLASS_NAME LIKE '%BUILDING SET%' THEN 'BUILDING SET'
                             WHEN SUBCLASS_NAME LIKE '%BLOCK SET%' THEN 'BLOCK SET'
                             WHEN SUBCLASS_NAME LIKE '%CHILDREN GAME%' THEN 'PRESCHOOL GAME'
							 ELSE PRESCHOOL_KEYWORD END
WHERE MOM_AND_KID_PRODUCT_FLAG = 1
AND REVISED_CATEGORY_LEVEL_2 = 'TOYS'
AND SUBDEPT_NAME = 'GAME & BUILDING'
AND SUBCLASS_NAME NOT IN ('BOARD GAME','CARD GAME')
AND PRESCHOOL_FLAG = 'YES'
AND PRESCHOOL_KEYWORD IS NULL
;

UPDATE vpm_data.pm_skutagging_kids_product_master
SET JUNIOR_KEYWORD = CASE WHEN SUBCLASS_NAME LIKE '%BUILDING SET%' THEN 'BUILDING SET'
                          WHEN SUBCLASS_NAME LIKE '%BLOCK SET%' THEN 'BLOCK SET'
                          WHEN SUBCLASS_NAME LIKE '%CHILDREN GAME%' THEN 'PRESCHOOL GAME'
					      ELSE JUNIOR_KEYWORD END
WHERE MOM_AND_KID_PRODUCT_FLAG = 1
AND REVISED_CATEGORY_LEVEL_2 = 'TOYS'
AND SUBDEPT_NAME = 'GAME & BUILDING'
AND SUBCLASS_NAME NOT IN ('BOARD GAME','CARD GAME')
AND JUNIOR_FLAG = 'YES'
AND JUNIOR_FLAG IS NULL 
;

--SUBDEPT_NAME = 'TOYS' AND SUBCLASS_NAME != 'SPORT'

UPDATE vpm_data.pm_skutagging_kids_product_master
SET PRESCHOOL_KEYWORD = CASE WHEN PRODUCT_NAME LIKE '%TOYS SET%' THEN 'TOY SET'
                             WHEN PRODUCT_NAME LIKE '%BOY TOYS%' THEN 'BOY TOYS'
                             WHEN PRODUCT_NAME LIKE '%GIRL TOY%' THEN 'GIRL TOYS'
                             WHEN PRODUCT_NAME LIKE '%TRICYCLE%' THEN 'TRICYCLE'
                             WHEN PRODUCT_NAME LIKE '%BEN10%' THEN 'BEN10'
                             WHEN PRODUCT_NAME LIKE '%SOLDIER%' THEN 'SOLDIER'
                             WHEN PRODUCT_NAME LIKE '%PLANE%' THEN 'PLANE'
                             WHEN PRODUCT_NAME LIKE '%GUN%' THEN 'GUN'
                             WHEN PRODUCT_NAME LIKE '%PIGGY BANK%' THEN 'PIGGYBANK'
                             WHEN PRODUCT_NAME LIKE '%WEAPON%' THEN 'WEAPON'
                             WHEN PRODUCT_NAME LIKE '%DOCTOR%' THEN 'DOCTOR'
                             WHEN PRODUCT_NAME LIKE '%BLOCK%' THEN 'BLOCKS'
                             WHEN PRODUCT_NAME LIKE '%DOUGH%' THEN 'DOUGH'
                             WHEN PRODUCT_NAME LIKE '%DOH%' THEN 'DOUGH'
                             WHEN PRODUCT_NAME LIKE '%ROBOT%' THEN 'ROBOT'
                             WHEN PRODUCT_NAME LIKE '%FIGURE%' THEN 'FIGURE'
                             WHEN PRODUCT_NAME LIKE '%KITCHEN%' THEN 'KITCHEN'
                             WHEN PRODUCT_NAME LIKE '%MAGIC%' THEN 'MAGIC'
                             WHEN PRODUCT_NAME LIKE '%CD%' THEN 'CD'
                             WHEN PRODUCT_NAME LIKE '%POOL%' THEN 'POOL'
                             WHEN PRODUCT_NAME LIKE '%MAGIC%' THEN 'MAGIC'
                             WHEN PRODUCT_NAME LIKE '%GLASS%' THEN 'GLASSES'
                             WHEN PRODUCT_NAME LIKE '%MAKE UP%' THEN 'MAKE UP'
                             WHEN PRODUCT_NAME LIKE '%MAKEUP%' THEN 'MAKE UP'
                             WHEN PRODUCT_NAME LIKE '%GOLF%' THEN 'GOLF'
                             WHEN PRODUCT_NAME LIKE '%BALL%' THEN 'BALL'
                             WHEN PRODUCT_NAME LIKE '%PONY%' THEN 'PONY'
                             WHEN PRODUCT_NAME LIKE '%GAME BOX%' THEN 'GAME BOX'
                             WHEN PRODUCT_NAME LIKE '%MOBILE%' THEN 'MOBILE'
                             WHEN PRODUCT_NAME LIKE '%SWORD%' THEN 'SWORD'
                             WHEN PRODUCT_NAME LIKE '%ANGRY%' THEN 'ANGRY BIRD'
                             WHEN PRODUCT_NAME LIKE '%SHOOTER%' THEN 'SHOOTER'
                             WHEN PRODUCT_NAME LIKE '%DORAMON%' THEN 'DORAEMON'
                             WHEN PRODUCT_NAME LIKE '%DORAEMON%' THEN 'DORAEMON'
                             WHEN PRODUCT_NAME LIKE '%DOLL%' THEN 'DOLL'
                             WHEN PRODUCT_NAME LIKE '%BOXING%' THEN 'BOXING'
                             WHEN PRODUCT_NAME LIKE '%ANIMAL%' THEN 'ANIMAL'
                             WHEN PRODUCT_NAME LIKE '%TEA SET%' THEN 'TEA SET'
                             WHEN PRODUCT_NAME LIKE '%PET%' THEN 'PETS'
                             WHEN PRODUCT_NAME LIKE '%MUSIC%' THEN 'MUSIC'
                             WHEN PRODUCT_NAME LIKE '%MICKEY%' THEN 'MICKEY'
                             WHEN PRODUCT_NAME LIKE '%AIRCRAFT%' THEN 'AIRCRAFT'
                             WHEN PRODUCT_NAME LIKE '%FRISBEE%' THEN 'FRISBEE'
                             WHEN PRODUCT_NAME LIKE '%ROCKING%' THEN 'ROCKING'
                             WHEN PRODUCT_NAME LIKE '%STAMP%' THEN 'STAMP'
                             WHEN PRODUCT_NAME LIKE '%SAND%' THEN 'SAND'
                             WHEN PRODUCT_NAME LIKE '%CONSTRUCT%' THEN 'CONSTRUCTION'
                             WHEN PRODUCT_NAME LIKE '%GAME%' THEN 'GAME'
							 WHEN SUBCLASS_NAME = 'FIGURES' THEN 'FIGURE'
                             WHEN SUBCLASS_NAME = 'GUN' THEN 'GUN'
                             WHEN SUBCLASS_NAME = 'BRICK' THEN 'BRICK'
                             WHEN SUBCLASS_NAME = 'WOODEN' THEN 'WOODEN'
							 WHEN PRODUCT_NAME LIKE '%���ԡ�%' THEN '���ԡ�'
							 WHEN CLEANED_BRANDNAME LIKE '%DOH%' THEN 'DOUGH'
							 WHEN CLEANED_BRANDNAME LIKE '%MARVEL%' THEN 'MARVEL'
							 WHEN CLEANED_BRANDNAME LIKE '%PEPPA PIG%' THEN 'PEPPA PIG'
							 WHEN CLEANED_BRANDNAME LIKE '%BARBIE%' THEN 'BARBIE'
							 WHEN CLEANED_BRANDNAME LIKE '%TRANSFORMER%' THEN 'TRANSFORMER'
							 WHEN CLEANED_BRANDNAME LIKE '%����%' THEN 'CARD'
							 WHEN CLEANED_BRANDNAME LIKE '%����%' THEN 'CARD'
							 WHEN CLEANED_BRANDNAME LIKE '%PONY%' THEN 'MY LITTLE PONY'
							 WHEN CLEANED_BRANDNAME LIKE '%POKEMON%' THEN 'POKEMON'
							 WHEN CLEANED_BRANDNAME LIKE '%CRAYOLA%' THEN 'CRAYOLA'
							 WHEN CLEANED_BRANDNAME LIKE '%BANDAI%' THEN 'BANDAI'
							 WHEN CLEANED_BRANDNAME LIKE '%ANGRY BIRD%' THEN 'ANGRY BIRD'
							 WHEN CLEANED_BRANDNAME LIKE '%SUPERMAN%' THEN 'SUPERMAN'
							 WHEN CLEANED_BRANDNAME LIKE '%BEN10%' THEN 'BEN10'
							 WHEN CLEANED_BRANDNAME LIKE '%CARTOON%' THEN 'CARTOON'
							 WHEN CLEANED_BRANDNAME LIKE '%DISNEY%' THEN 'DISNEY'
							 WHEN CLEANED_BRANDNAME LIKE '%CLAY%' THEN 'CLAY'
							 WHEN CLEANED_BRANDNAME LIKE '%KITTY%' THEN 'KITTY'
							 WHEN CLEANED_BRANDNAME LIKE '%THOMAS%' THEN 'THOMAS'
							 WHEN CLEANED_BRANDNAME LIKE '%RILAK%' THEN 'RILAKKUMA'
							 WHEN CLEANED_BRANDNAME LIKE '%PUFF%' THEN 'POWERPUFF GIRL'
							 WHEN CLEANED_BRANDNAME LIKE '%ROBOT%' THEN 'ROBOT'
							 WHEN CLEANED_BRANDNAME LIKE '%ROBO%' THEN 'ROBOT'
							 WHEN PRODUCT_NAME LIKE '%ROBO%' THEN 'ROBOT'
							 WHEN CLEANED_BRANDNAME LIKE '%STAR WARS%' THEN 'STAR WARS'
							 WHEN CLEANED_BRANDNAME LIKE '%SPIDERMAN%' THEN 'SPIDERMAN'
							 WHEN CLEANED_BRANDNAME LIKE '%���������%' THEN 'SPIDERMAN'
							 WHEN CLEANED_BRANDNAME LIKE '%DOLL%' THEN 'DOLL'
							 WHEN CLEANED_BRANDNAME LIKE '%DINO%' THEN 'DINO'
							 WHEN CLEANED_BRANDNAME LIKE '%BUBBLE%' THEN 'BUBBLE'
							 WHEN CLEANED_BRANDNAME LIKE '%HOTWHEELS%' THEN 'HOTWHEELS'
							 WHEN CLEANED_BRANDNAME LIKE '%PLAY%' THEN 'PLAY'
							 WHEN PRODUCT_NAME LIKE '%BUS%' THEN 'BUS'
							 WHEN PRODUCT_NAME LIKE '%�������%' THEN '�������'
							 WHEN PRODUCT_NAME LIKE '%�ѵ��%' THEN '�ѵ��'
							 WHEN PRODUCT_NAME LIKE '%��дҹ%' THEN '��дҹ'
							 WHEN PRODUCT_NAME LIKE '%CLAY%' THEN 'CLAY'
							 WHEN PRODUCT_NAME LIKE '%BLADE%' THEN 'BEYBLADE'
							 WHEN PRODUCT_NAME LIKE '%TRANSFORMER%' THEN 'TRANSFORMER'
							 WHEN PRODUCT_NAME LIKE '%HAMMER%' THEN 'HAMMER'
							 WHEN PRODUCT_NAME LIKE '%LAMBORGHINI%' THEN 'LAMBORGHINI'
							 WHEN PRODUCT_NAME LIKE '%FIREMAN%' THEN 'FIREMAN'
							 WHEN PRODUCT_NAME LIKE '%GLIDER%' THEN 'GLIDER'
							 WHEN PRODUCT_NAME LIKE '%COPTER%' THEN 'COPTER'
							 WHEN PRODUCT_NAME LIKE '%����%' THEN 'FROZEN'
							 WHEN PRODUCT_NAME LIKE '%FROZEN%' THEN 'FROZEN'
							 WHEN PRODUCT_NAME LIKE '%STAR WAR%' THEN 'STAR WARS'
							 WHEN PRODUCT_NAME LIKE '%����%' THEN 'SAND'
							 WHEN PRODUCT_NAME LIKE '%ZOMBIE%' THEN 'ZOMBIE'
							 WHEN PRODUCT_NAME LIKE '%FARM%' THEN 'FARM'
							 WHEN PRODUCT_NAME LIKE '%ZOMBIE%' THEN 'ZOMBIE'
							 WHEN PRODUCT_NAME LIKE '%���¹%' THEN 'ROBOT'
							 WHEN PRODUCT_NAME LIKE '%PLAYSET%' THEN 'PLAYSET'
							 WHEN PRODUCT_NAME LIKE '%ACCESSORIE%' THEN 'ACCESSORIES'
							 WHEN PRODUCT_NAME LIKE '%ACCESSORY%' THEN 'ACCESSORIES'
							 WHEN PRODUCT_NAME LIKE '%��͹%' THEN '��͹'
							 WHEN PRODUCT_NAME LIKE '%BAG%' THEN 'BAG'
							 WHEN PRODUCT_NAME LIKE '%BACKPACK%' THEN 'BACKPACK'
							 WHEN PRODUCT_NAME LIKE '%��%' THEN 'BACKPACK'
							 WHEN PRODUCT_NAME LIKE '%MARIO%' THEN 'MARIO'
							 WHEN PRODUCT_NAME LIKE '%����%' THEN 'MOTOR'
							 WHEN PRODUCT_NAME LIKE '%SHOOT%' THEN 'SHOOTING'
							 WHEN PRODUCT_NAME LIKE '%STICKER%' THEN 'STICKER'
							 WHEN PRODUCT_NAME LIKE '%WALLET%' THEN 'WALLET'
							 WHEN PRODUCT_NAME LIKE '%POUCH%' THEN 'POUCH'
							 WHEN PRODUCT_NAME LIKE '%LEGO%' THEN 'LEGO'
							 WHEN PRODUCT_NAME LIKE '%RACER%' THEN 'RACER'
							 WHEN PRODUCT_NAME LIKE '%BOOK%' THEN 'BOOK'
							 WHEN PRODUCT_NAME LIKE '%COOK%' THEN 'COOKING'
							 WHEN PRODUCT_NAME LIKE '%TROLLEY%' THEN 'TROLLEY'
							 WHEN PRODUCT_NAME LIKE '%DESPICABLE ME%' THEN 'DESPICABLE ME'
							 WHEN PRODUCT_NAME LIKE '%�Թ����ѹ%' THEN '�Թ����ѹ'
							 WHEN PRODUCT_NAME LIKE '%��%' THEN 'DINO'
							 WHEN PRODUCT_NAME LIKE '%PIGLET%' THEN 'PIGLET'
							 WHEN PRODUCT_NAME LIKE '%BUILD%' THEN 'BUILDER'
							 WHEN PRODUCT_NAME LIKE '%TAXI%' THEN 'TAXI'
							 WHEN PRODUCT_NAME LIKE '%TUKTUK%' THEN 'TUKTUK'
							 WHEN PRODUCT_NAME LIKE '%EXPLORE%' THEN 'EXPLORER'
							 WHEN PRODUCT_NAME LIKE '%VIKING%' THEN 'VIKING'
							 WHEN PRODUCT_NAME LIKE '%NSTRIKE%' THEN 'NSTRIKE'
							 WHEN PRODUCT_NAME LIKE '%GAME%' THEN 'GAME'
							 WHEN PRODUCT_NAME LIKE '%PATROL%' THEN 'PATROL'
							 WHEN PRODUCT_NAME LIKE '%JURASSIC%' THEN 'JURASSIC'
							 WHEN PRODUCT_NAME LIKE '%COOPER%' THEN 'COOPER'
							 ELSE 'PRESCHOOL TOY' END
WHERE MOM_AND_KID_PRODUCT_FLAG = 1
AND REVISED_CATEGORY_LEVEL_2 = 'TOYS' 
AND SUBDEPT_NAME = 'TOYS'
AND SUBCLASS_NAME != 'SPORT'
AND PRESCHOOL_FLAG = 'YES'
AND PRESCHOOL_KEYWORD IS NULL
;

UPDATE vpm_data.pm_skutagging_kids_product_master
SET JUNIOR_KEYWORD = CASE WHEN PRODUCT_NAME LIKE '%TOYS SET%' THEN 'TOY SET'
                             WHEN PRODUCT_NAME LIKE '%BOY TOYS%' THEN 'BOY TOYS'
                             WHEN PRODUCT_NAME LIKE '%GIRL TOY%' THEN 'GIRL TOYS'
                             WHEN PRODUCT_NAME LIKE '%TRICYCLE%' THEN 'TRICYCLE'
                             WHEN PRODUCT_NAME LIKE '%BEN10%' THEN 'BEN10'
                             WHEN PRODUCT_NAME LIKE '%SOLDIER%' THEN 'SOLDIER'
                             WHEN PRODUCT_NAME LIKE '%PLANE%' THEN 'PLANE'
                             WHEN PRODUCT_NAME LIKE '%GUN%' THEN 'GUN'
                             WHEN PRODUCT_NAME LIKE '%PIGGY BANK%' THEN 'PIGGYBANK'
                             WHEN PRODUCT_NAME LIKE '%WEAPON%' THEN 'WEAPON'
                             WHEN PRODUCT_NAME LIKE '%DOCTOR%' THEN 'DOCTOR'
                             WHEN PRODUCT_NAME LIKE '%BLOCK%' THEN 'BLOCKS'
                             WHEN PRODUCT_NAME LIKE '%DOUGH%' THEN 'DOUGH'
                             WHEN PRODUCT_NAME LIKE '%DOH%' THEN 'DOUGH'
                             WHEN PRODUCT_NAME LIKE '%ROBOT%' THEN 'ROBOT'
                             WHEN PRODUCT_NAME LIKE '%FIGURE%' THEN 'FIGURE'
                             WHEN PRODUCT_NAME LIKE '%KITCHEN%' THEN 'KITCHEN'
                             WHEN PRODUCT_NAME LIKE '%MAGIC%' THEN 'MAGIC'
                             WHEN PRODUCT_NAME LIKE '%CD%' THEN 'CD'
                             WHEN PRODUCT_NAME LIKE '%POOL%' THEN 'POOL'
                             WHEN PRODUCT_NAME LIKE '%MAGIC%' THEN 'MAGIC'
                             WHEN PRODUCT_NAME LIKE '%GLASS%' THEN 'GLASSES'
                             WHEN PRODUCT_NAME LIKE '%MAKE UP%' THEN 'MAKE UP'
                             WHEN PRODUCT_NAME LIKE '%MAKEUP%' THEN 'MAKE UP'
                             WHEN PRODUCT_NAME LIKE '%GOLF%' THEN 'GOLF'
                             WHEN PRODUCT_NAME LIKE '%BALL%' THEN 'BALL'
                             WHEN PRODUCT_NAME LIKE '%PONY%' THEN 'PONY'
                             WHEN PRODUCT_NAME LIKE '%GAME BOX%' THEN 'GAME BOX'
                             WHEN PRODUCT_NAME LIKE '%MOBILE%' THEN 'MOBILE'
                             WHEN PRODUCT_NAME LIKE '%SWORD%' THEN 'SWORD'
                             WHEN PRODUCT_NAME LIKE '%ANGRY%' THEN 'ANGRY BIRD'
                             WHEN PRODUCT_NAME LIKE '%SHOOTER%' THEN 'SHOOTER'
                             WHEN PRODUCT_NAME LIKE '%DORAMON%' THEN 'DORAEMON'
                             WHEN PRODUCT_NAME LIKE '%DORAEMON%' THEN 'DORAEMON'
                             WHEN PRODUCT_NAME LIKE '%DOLL%' THEN 'DOLL'
                             WHEN PRODUCT_NAME LIKE '%BOXING%' THEN 'BOXING'
                             WHEN PRODUCT_NAME LIKE '%ANIMAL%' THEN 'ANIMAL'
                             WHEN PRODUCT_NAME LIKE '%TEA SET%' THEN 'TEA SET'
                             WHEN PRODUCT_NAME LIKE '%PET%' THEN 'PETS'
                             WHEN PRODUCT_NAME LIKE '%MUSIC%' THEN 'MUSIC'
                             WHEN PRODUCT_NAME LIKE '%MICKEY%' THEN 'MICKEY'
                             WHEN PRODUCT_NAME LIKE '%AIRCRAFT%' THEN 'AIRCRAFT'
                             WHEN PRODUCT_NAME LIKE '%FRISBEE%' THEN 'FRISBEE'
                             WHEN PRODUCT_NAME LIKE '%ROCKING%' THEN 'ROCKING'
                             WHEN PRODUCT_NAME LIKE '%STAMP%' THEN 'STAMP'
                             WHEN PRODUCT_NAME LIKE '%SAND%' THEN 'SAND'
                             WHEN PRODUCT_NAME LIKE '%CONSTRUCT%' THEN 'CONSTRUCTION'
                             WHEN PRODUCT_NAME LIKE '%GAME%' THEN 'GAME'
							 WHEN SUBCLASS_NAME = 'FIGURES' THEN 'FIGURE'
                             WHEN SUBCLASS_NAME = 'GUN' THEN 'GUN'
                             WHEN SUBCLASS_NAME = 'BRICK' THEN 'BRICK'
                             WHEN SUBCLASS_NAME = 'WOODEN' THEN 'WOODEN'
							 WHEN PRODUCT_NAME LIKE '%���ԡ�%' THEN '���ԡ�'
							 WHEN CLEANED_BRANDNAME LIKE '%DOH%' THEN 'DOUGH'
							 WHEN CLEANED_BRANDNAME LIKE '%MARVEL%' THEN 'MARVEL'
							 WHEN CLEANED_BRANDNAME LIKE '%PEPPA PIG%' THEN 'PEPPA PIG'
							 WHEN CLEANED_BRANDNAME LIKE '%BARBIE%' THEN 'BARBIE'
							 WHEN CLEANED_BRANDNAME LIKE '%TRANSFORMER%' THEN 'TRANSFORMER'
							 WHEN CLEANED_BRANDNAME LIKE '%����%' THEN 'CARD'
							 WHEN CLEANED_BRANDNAME LIKE '%����%' THEN 'CARD'
							 WHEN CLEANED_BRANDNAME LIKE '%PONY%' THEN 'MY LITTLE PONY'
							 WHEN CLEANED_BRANDNAME LIKE '%POKEMON%' THEN 'POKEMON'
							 WHEN CLEANED_BRANDNAME LIKE '%CRAYOLA%' THEN 'CRAYOLA'
							 WHEN CLEANED_BRANDNAME LIKE '%BANDAI%' THEN 'BANDAI'
							 WHEN CLEANED_BRANDNAME LIKE '%ANGRY BIRD%' THEN 'ANGRY BIRD'
							 WHEN CLEANED_BRANDNAME LIKE '%SUPERMAN%' THEN 'SUPERMAN'
							 WHEN CLEANED_BRANDNAME LIKE '%BEN10%' THEN 'BEN10'
							 WHEN CLEANED_BRANDNAME LIKE '%CARTOON%' THEN 'CARTOON'
							 WHEN CLEANED_BRANDNAME LIKE '%DISNEY%' THEN 'DISNEY'
							 WHEN CLEANED_BRANDNAME LIKE '%CLAY%' THEN 'CLAY'
							 WHEN CLEANED_BRANDNAME LIKE '%KITTY%' THEN 'KITTY'
							 WHEN CLEANED_BRANDNAME LIKE '%THOMAS%' THEN 'THOMAS'
							 WHEN CLEANED_BRANDNAME LIKE '%RILAK%' THEN 'RILAKKUMA'
							 WHEN CLEANED_BRANDNAME LIKE '%PUFF%' THEN 'POWERPUFF GIRL'
							 WHEN CLEANED_BRANDNAME LIKE '%ROBOT%' THEN 'ROBOT'
							 WHEN CLEANED_BRANDNAME LIKE '%ROBO%' THEN 'ROBOT'
							 WHEN PRODUCT_NAME LIKE '%ROBO%' THEN 'ROBOT'
							 WHEN CLEANED_BRANDNAME LIKE '%STAR WARS%' THEN 'STAR WARS'
							 WHEN CLEANED_BRANDNAME LIKE '%SPIDERMAN%' THEN 'SPIDERMAN'
							 WHEN CLEANED_BRANDNAME LIKE '%���������%' THEN 'SPIDERMAN'
							 WHEN CLEANED_BRANDNAME LIKE '%DOLL%' THEN 'DOLL'
							 WHEN CLEANED_BRANDNAME LIKE '%DINO%' THEN 'DINO'
							 WHEN CLEANED_BRANDNAME LIKE '%BUBBLE%' THEN 'BUBBLE'
							 WHEN CLEANED_BRANDNAME LIKE '%HOTWHEELS%' THEN 'HOTWHEELS'
							 WHEN CLEANED_BRANDNAME LIKE '%PLAY%' THEN 'PLAY'
							 WHEN PRODUCT_NAME LIKE '%BUS%' THEN 'BUS'
							 WHEN PRODUCT_NAME LIKE '%�������%' THEN '�������'
							 WHEN PRODUCT_NAME LIKE '%�ѵ��%' THEN '�ѵ��'
							 WHEN PRODUCT_NAME LIKE '%��дҹ%' THEN '��дҹ'
							 WHEN PRODUCT_NAME LIKE '%CLAY%' THEN 'CLAY'
							 WHEN PRODUCT_NAME LIKE '%BLADE%' THEN 'BEYBLADE'
							 WHEN PRODUCT_NAME LIKE '%TRANSFORMER%' THEN 'TRANSFORMER'
							 WHEN PRODUCT_NAME LIKE '%HAMMER%' THEN 'HAMMER'
							 WHEN PRODUCT_NAME LIKE '%LAMBORGHINI%' THEN 'LAMBORGHINI'
							 WHEN PRODUCT_NAME LIKE '%FIREMAN%' THEN 'FIREMAN'
							 WHEN PRODUCT_NAME LIKE '%GLIDER%' THEN 'GLIDER'
							 WHEN PRODUCT_NAME LIKE '%COPTER%' THEN 'COPTER'
							 WHEN PRODUCT_NAME LIKE '%����%' THEN 'FROZEN'
							 WHEN PRODUCT_NAME LIKE '%FROZEN%' THEN 'FROZEN'
							 WHEN PRODUCT_NAME LIKE '%STAR WAR%' THEN 'STAR WARS'
							 WHEN PRODUCT_NAME LIKE '%����%' THEN 'SAND'
							 WHEN PRODUCT_NAME LIKE '%ZOMBIE%' THEN 'ZOMBIE'
							 WHEN PRODUCT_NAME LIKE '%FARM%' THEN 'FARM'
							 WHEN PRODUCT_NAME LIKE '%ZOMBIE%' THEN 'ZOMBIE'
							 WHEN PRODUCT_NAME LIKE '%���¹%' THEN 'ROBOT'
							 WHEN PRODUCT_NAME LIKE '%PLAYSET%' THEN 'PLAYSET'
							 WHEN PRODUCT_NAME LIKE '%ACCESSORIE%' THEN 'ACCESSORIES'
							 WHEN PRODUCT_NAME LIKE '%ACCESSORY%' THEN 'ACCESSORIES'
							 WHEN PRODUCT_NAME LIKE '%��͹%' THEN '��͹'
							 WHEN PRODUCT_NAME LIKE '%BAG%' THEN 'BAG'
							 WHEN PRODUCT_NAME LIKE '%BACKPACK%' THEN 'BACKPACK'
							 WHEN PRODUCT_NAME LIKE '%��%' THEN 'BACKPACK'
							 WHEN PRODUCT_NAME LIKE '%MARIO%' THEN 'MARIO'
							 WHEN PRODUCT_NAME LIKE '%����%' THEN 'MOTOR'
							 WHEN PRODUCT_NAME LIKE '%SHOOT%' THEN 'SHOOTING'
							 WHEN PRODUCT_NAME LIKE '%STICKER%' THEN 'STICKER'
							 WHEN PRODUCT_NAME LIKE '%WALLET%' THEN 'WALLET'
							 WHEN PRODUCT_NAME LIKE '%POUCH%' THEN 'POUCH'
							 WHEN PRODUCT_NAME LIKE '%LEGO%' THEN 'LEGO'
							 WHEN PRODUCT_NAME LIKE '%RACER%' THEN 'RACER'
							 WHEN PRODUCT_NAME LIKE '%BOOK%' THEN 'BOOK'
							 WHEN PRODUCT_NAME LIKE '%COOK%' THEN 'COOKING'
							 WHEN PRODUCT_NAME LIKE '%TROLLEY%' THEN 'TROLLEY'
							 WHEN PRODUCT_NAME LIKE '%DESPICABLE ME%' THEN 'DESPICABLE ME'
							 WHEN PRODUCT_NAME LIKE '%�Թ����ѹ%' THEN '�Թ����ѹ'
							 WHEN PRODUCT_NAME LIKE '%��%' THEN 'DINO'
							 WHEN PRODUCT_NAME LIKE '%PIGLET%' THEN 'PIGLET'
							 WHEN PRODUCT_NAME LIKE '%BUILD%' THEN 'BUILDER'
							 WHEN PRODUCT_NAME LIKE '%TAXI%' THEN 'TAXI'
							 WHEN PRODUCT_NAME LIKE '%TUKTUK%' THEN 'TUKTUK'
							 WHEN PRODUCT_NAME LIKE '%EXPLORE%' THEN 'EXPLORER'
							 WHEN PRODUCT_NAME LIKE '%VIKING%' THEN 'VIKING'
							 WHEN PRODUCT_NAME LIKE '%NSTRIKE%' THEN 'NSTRIKE'
							 WHEN PRODUCT_NAME LIKE '%GAME%' THEN 'GAME'
							 WHEN PRODUCT_NAME LIKE '%PATROL%' THEN 'PATROL'
							 WHEN PRODUCT_NAME LIKE '%JURASSIC%' THEN 'JURASSIC'
							 WHEN PRODUCT_NAME LIKE '%COOPER%' THEN 'COOPER'
							 ELSE 'JUNIOR TOY' END
WHERE MOM_AND_KID_PRODUCT_FLAG = 1
AND REVISED_CATEGORY_LEVEL_2 = 'TOYS'
AND SUBDEPT_NAME = 'TOYS'
AND SUBCLASS_NAME != 'SPORT'
AND JUNIOR_FLAG = 'YES'
AND JUNIOR_KEYWORD IS NULL
;

--SUBDEPT_NAME = 'GAME & BUILDING' AND SUBCLASS_NAME = 'BOARD GAME'
UPDATE vpm_data.pm_skutagging_kids_product_master
SET JUNIOR_KEYWORD = 'BOARD GAME'
   ,TEEN_KEYWORD = 'BOARD GAME'
WHERE MOM_AND_KID_PRODUCT_FLAG = 1
AND REVISED_CATEGORY_LEVEL_2 = 'TOYS'
AND JUNIOR_FLAG = 'YES'
AND SUBDEPT_NAME = 'GAME & BUILDING'
AND SUBCLASS_NAME = 'BOARD GAME'
;

--SUBDEPT_NAME = 'GAME & BUILDING' AND SUBCLASS_NAME = 'CARD GAME'
UPDATE vpm_data.pm_skutagging_kids_product_master
SET JUNIOR_KEYWORD = 'CARD GAME'
WHERE MOM_AND_KID_PRODUCT_FLAG = 1
AND REVISED_CATEGORY_LEVEL_2 = 'TOYS'
AND JUNIOR_FLAG = 'YES'
AND SUBDEPT_NAME = 'GAME & BUILDING'
AND SUBCLASS_NAME = 'CARD GAME'
;

--SUBDEPT_NAME = 'TOYS' AND SUBCLASS_NAME = 'SPORT'

UPDATE vpm_data.pm_skutagging_kids_product_master
SET PRESCHOOL_KEYWORD = CASE WHEN PRODUCT_NAME LIKE '%��ǧ��%' THEN '��ǧ��'
                             WHEN PRODUCT_NAME LIKE '%������ᢹ%' THEN '��͡ᢹ'
							 WHEN PRODUCT_NAME LIKE '%��͡ᢹ%' THEN '��͡ᢹ'
							 ELSE PRESCHOOL_KEYWORD END
WHERE MOM_AND_KID_PRODUCT_FLAG = 1
AND REVISED_CATEGORY_LEVEL_2 = 'TOYS'
AND SUBDEPT_NAME = 'TOYS'
AND SUBCLASS_NAME = 'SPORT'
AND PRESCHOOL_FLAG = 'YES'
;

UPDATE vpm_data.pm_skutagging_kids_product_master
SET TEEN_KEYWORD = CASE WHEN PRODUCT_NAME LIKE '%�%' THEN '�'
                        WHEN PRODUCT_NAME LIKE '%��蹵�%' THEN '��蹵�'
						WHEN PRODUCT_NAME LIKE '%��ǧ�ҧ%' THEN '��ǧ�ҧ'
						WHEN PRODUCT_NAME LIKE '%���;��%' THEN '���;��'
						WHEN PRODUCT_NAME LIKE '%�ٺ%' THEN '����ٺ��'
						ELSE TEEN_KEYWORD END
WHERE MOM_AND_KID_PRODUCT_FLAG = 1
AND REVISED_CATEGORY_LEVEL_2 = 'TOYS'
AND SUBDEPT_NAME = 'TOYS'
AND SUBCLASS_NAME = 'SPORT'
AND TEEN_FLAG = 'YES'
;

UPDATE vpm_data.pm_skutagging_kids_product_master
SET PRESCHOOL_KEYWORD = 'SWIMMING'
   ,JUNIOR_KEYWORD = 'SWIMMING'
WHERE MOM_AND_KID_PRODUCT_FLAG = 1
AND REVISED_CATEGORY_LEVEL_2 = 'TOYS'
AND SUBDEPT_NAME = 'TOYS'
AND SUBCLASS_NAME = 'SPORT'
AND ((PRESCHOOL_FLAG = 'YES' AND PRESCHOOL_KEYWORD IS NULL) 
      OR (JUNIOR_FLAG = 'YES' AND JUNIOR_KEYWORD IS NULL))
;

--NOT KID PRODUCT
UPDATE vpm_data.pm_skutagging_kids_product_master
SET MOM_AND_KID_PRODUCT_FLAG = 0
WHERE MOM_AND_KID_PRODUCT_FLAG = 1
AND COALESCE(PREG_FLAG,'N/A') = 'N/A'
AND COALESCE(INFANT_FLAG,'N/A') = 'N/A'
AND COALESCE(TODDLER_FLAG,'N/A') = 'N/A'
AND COALESCE(PRESCHOOL_FLAG,'N/A') = 'N/A'
AND COALESCE(JUNIOR_FLAG,'N/A') = 'N/A'
AND COALESCE(TEEN_FLAG,'N/A') = 'N/A'
AND (REVISED_CATEGORY_LEVEL_1 = 'BEAUTY'
	OR CLEANED_BRANDNAME IN ('BOBINO','S SPORTS','CARRY ALL')
	OR SUBCLASS_NAME = 'CHARACTER WATCH'
	OR PRODUCT_NAME LIKE '%����ͧ���ҧ%')
;

/* KID ACCESSORIES */
-----OTHER KID ACCESSORIES
--NEW INFANT: GUCCI GIFTSET AND SLEEPSUIT FOR 0-3/3-6/6-9/9-12 MTH
UPDATE vpm_data.pm_skutagging_kids_product_master
SET INFANT_FLAG = 'YES'
	,UNKNOWN_INFANT_SUBSTAGE = 'N/A'
	,"STAGE_M+0_TO_M+3" = (CASE WHEN PRODUCT_NAME LIKE '% 0-3' THEN 'YES' ELSE 'N/A' END)
	,"STAGE_M+3_TO_M+6" = (CASE WHEN PRODUCT_NAME LIKE '% 3-6' THEN 'YES' ELSE 'N/A' END)
	,"STAGE_M+6_TO_M+9" = (CASE WHEN PRODUCT_NAME LIKE '% 6-9' THEN 'YES' ELSE 'N/A' END)
	,"STAGE_M+9_TO_M+12" = (CASE WHEN PRODUCT_NAME LIKE '% 9-12' THEN 'YES' ELSE 'N/A' END)
	,INFANT_0_3M_KEYWORD = (CASE WHEN PRODUCT_NAME LIKE '% 0-3' THEN '0-3 MTH' ELSE 'N/A' END)
	,INFANT_4_6M_KEYWORD = (CASE WHEN PRODUCT_NAME LIKE '% 3-6' THEN '0-3 MTH' ELSE 'N/A' END)
	,INFANT_7_9M_KEYWORD = (CASE WHEN PRODUCT_NAME LIKE '% 6-9' THEN '0-3 MTH' ELSE 'N/A' END)
	,INFANT_10_12M_KEYWORD = (CASE WHEN PRODUCT_NAME LIKE '% 9-12' THEN '0-3 MTH' ELSE 'N/A' END)
WHERE MOM_AND_KID_PRODUCT_FLAG = 1
AND COALESCE(PREG_FLAG,'N/A') = 'N/A'
AND COALESCE(INFANT_FLAG,'N/A') = 'N/A'
AND COALESCE(TODDLER_FLAG,'N/A') = 'N/A'
AND COALESCE(PRESCHOOL_FLAG,'N/A') = 'N/A'
AND COALESCE(JUNIOR_FLAG,'N/A') = 'N/A'
AND COALESCE(TEEN_FLAG,'N/A') = 'N/A'
AND REVISED_CATEGORY_LEVEL_1 = 'KIDS'
AND REVISED_CATEGORY_LEVEL_2 = 'KID ACCESSORIES'
AND REVISED_CATEGORY_LEVEL_3 = 'OTHER KID ACCESSORIES'
AND (CLEANED_BRANDNAME LIKE 'GUCCI KIDS%'
	OR (PRODUCT_NAME LIKE '%GIFTSET%' OR PRODUCT_NAME LIKE '%SLEEPSUIT%')
	OR CLEANED_BRANDNAME IN ('BABITO','BRUSTA','CAMERA','CANTALOOP'))
;

--NEW TODDLER
UPDATE vpm_data.pm_skutagging_kids_product_master
SET TODDLER_FLAG = 'YES'
	,TODDLER_KEYWORD = (CASE WHEN PRODUCT_NAME LIKE '%3TO5%' THEN '3-5 Y'
							WHEN PRODUCT_NAME LIKE '%18-36' THEN '18-36 MTH'
							WHEN PRODUCT_NAME LIKE '%24-36 MOS' THEN '24-36 MTH'
							WHEN PRODUCT_NAME LIKE '%36M' THEN '36 MTH'
							WHEN PRODUCT_NAME LIKE '%2-3Y%' THEN '2-3 Y'
							WHEN PRODUCT_NAME LIKE '%3-4Y%' THEN '3-4 Y'
							WHEN PRODUCT_NAME LIKE '%4Y%' THEN '4Y'
							WHEN PRODUCT_NAME LIKE '%15.5%' THEN 'SHOES 15.5 CM'
							WHEN PRODUCT_NAME LIKE '%15 5%' THEN 'SHOES 15.5 CM'
							WHEN PRODUCT_NAME LIKE '%16 5%' THEN 'SHOES 16.5 CM'
							WHEN PRODUCT_NAME LIKE 'NEW FLY RAIN COAT FOR CHILD' THEN 'NEW FLY RAIN COAT'
							ELSE TODDLER_KEYWORD END)
WHERE MOM_AND_KID_PRODUCT_FLAG = 1
AND COALESCE(PREG_FLAG,'N/A') = 'N/A'
AND COALESCE(INFANT_FLAG,'N/A') = 'N/A'
AND COALESCE(TODDLER_FLAG,'N/A') = 'N/A'
AND COALESCE(PRESCHOOL_FLAG,'N/A') = 'N/A'
AND COALESCE(JUNIOR_FLAG,'N/A') = 'N/A'
AND COALESCE(TEEN_FLAG,'N/A') = 'N/A'
AND REVISED_CATEGORY_LEVEL_1 = 'KIDS'
AND REVISED_CATEGORY_LEVEL_2 = 'KID ACCESSORIES'
AND (PRODUCT_NAME LIKE '%3TO5%'
	OR PRODUCT_NAME LIKE 'NEW FLY RAIN COAT FOR CHILD'
	OR PRODUCT_NAME LIKE '%18-36'
	OR PRODUCT_NAME LIKE '%24-36 MOS'
	OR PRODUCT_NAME LIKE '%36'
	OR PRODUCT_NAME LIKE '%2-3Y%'
	OR PRODUCT_NAME LIKE '%3-4Y%'
	OR PRODUCT_NAME LIKE '%4Y%'
	OR PRODUCT_NAME LIKE '%15.5%'
	OR PRODUCT_NAME LIKE '%15 5%'
	OR PRODUCT_NAME LIKE '%16 5%')
;

--NEW INFANT/TODDLER
UPDATE vpm_data.pm_skutagging_kids_product_master
SET INFANT_FLAG = 'YES'
	,UNKNOWN_INFANT_SUBSTAGE = 'YES'
	,TODDLER_FLAG = 'YES'
	,INFANT_UNKNOWN_KEYWORD = 'BABY MUJI CLOTHING'
	,TODDLER_KEYWORD = 'MUJI BABY CLOTHES'
WHERE MOM_AND_KID_PRODUCT_FLAG = 1
AND COALESCE(PREG_FLAG,'N/A') = 'N/A'
AND COALESCE(INFANT_FLAG,'N/A') = 'N/A'
AND COALESCE(TODDLER_FLAG,'N/A') = 'N/A'
AND COALESCE(PRESCHOOL_FLAG,'N/A') = 'N/A'
AND COALESCE(JUNIOR_FLAG,'N/A') = 'N/A'
AND COALESCE(TEEN_FLAG,'N/A') = 'N/A'
AND REVISED_CATEGORY_LEVEL_1 = 'KIDS'
AND REVISED_CATEGORY_LEVEL_2 = 'KID ACCESSORIES'
AND REVISED_CATEGORY_LEVEL_3 = 'OTHER KID ACCESSORIES'
AND (CLEANED_BRANDNAME = 'MUJI' AND PRODUCT_NAME LIKE '%BABY%')
;

--TODDLER/PRESCHOOL/JUNIOR
UPDATE vpm_data.pm_skutagging_kids_product_master
SET TODDLER_FLAG = 'YES'
	,PRESCHOOL_FLAG = 'YES'
	,JUNIOR_FLAG = 'YES'
	,TODDLER_KEYWORD = '3-12 Y'
	,PRESCHOOL_KEYWORD = '3-12 Y'
	,JUNIOR_KEYWORD = '3-12 Y'
WHERE MOM_AND_KID_PRODUCT_FLAG = 1
AND COALESCE(PREG_FLAG,'N/A') = 'N/A'
AND COALESCE(INFANT_FLAG,'N/A') = 'N/A'
AND COALESCE(TODDLER_FLAG,'N/A') = 'N/A'
AND COALESCE(PRESCHOOL_FLAG,'N/A') = 'N/A'
AND COALESCE(JUNIOR_FLAG,'N/A') = 'N/A'
AND COALESCE(TEEN_FLAG,'N/A') = 'N/A'
AND REVISED_CATEGORY_LEVEL_1 = 'KIDS'
AND REVISED_CATEGORY_LEVEL_2 = 'KID ACCESSORIES'
AND PRODUCT_NAME LIKE '%3-12Y%'
;

--PRESCHOOL/JUNIOR/TEEN
UPDATE vpm_data.pm_skutagging_kids_product_master
SET PRESCHOOL_FLAG = 'YES'
	,JUNIOR_FLAG = 'YES'
	,TEEN_FLAG = 'YES'
	,PRESCHOOL_KEYWORD = (CASE WHEN CLEANED_BRANDNAME LIKE 'MUJI' THEN 'MUJI KID CLOTHES'
								WHEN CLEANED_BRANDNAME LIKE 'SMIGGLE' THEN CONCAT('SMIGGLE ',SUBCLASS_NAME)
								ELSE PRESCHOOL_KEYWORD END)
	,JUNIOR_KEYWORD = (CASE WHEN CLEANED_BRANDNAME LIKE 'MUJI' THEN 'MUJI KID CLOTHES'
								WHEN CLEANED_BRANDNAME LIKE 'SMIGGLE' THEN CONCAT('SMIGGLE ',SUBCLASS_NAME)
								ELSE JUNIOR_KEYWORD END)
	,TEEN_KEYWORD = (CASE WHEN CLEANED_BRANDNAME LIKE 'MUJI' THEN 'MUJI KID CLOTHES'
								WHEN CLEANED_BRANDNAME LIKE 'SMIGGLE' THEN CONCAT('SMIGGLE ',SUBCLASS_NAME)
								ELSE TEEN_KEYWORD END)
WHERE MOM_AND_KID_PRODUCT_FLAG = 1
AND COALESCE(PREG_FLAG,'N/A') = 'N/A'
AND COALESCE(INFANT_FLAG,'N/A') = 'N/A'
AND COALESCE(TODDLER_FLAG,'N/A') = 'N/A'
AND COALESCE(PRESCHOOL_FLAG,'N/A') = 'N/A'
AND COALESCE(JUNIOR_FLAG,'N/A') = 'N/A'
AND COALESCE(TEEN_FLAG,'N/A') = 'N/A'
AND REVISED_CATEGORY_LEVEL_1 = 'KIDS'
AND REVISED_CATEGORY_LEVEL_2 = 'KID ACCESSORIES'
AND REVISED_CATEGORY_LEVEL_3 = 'OTHER KID ACCESSORIES'
AND (CLEANED_BRANDNAME LIKE 'SMIGGLE'
	OR (SUBCLASS_NAME IN ('PAJAMA WOVEN S/S','PAJAMA WOVEN L/S','PAJAMA C&S L/S') AND PRODUCT_NAME NOT LIKE '%BABY%' AND PRODUCT_NAME NOT LIKE '%KID%')
	OR (CLEANED_BRANDNAME LIKE 'MUJI' AND PRODUCT_NAME NOT LIKE '%BABY%'))
;

--PRESCHOOL
UPDATE vpm_data.pm_skutagging_kids_product_master
SET PRESCHOOL_FLAG = 'YES'
	,PRESCHOOL_KEYWORD = (CASE WHEN PRODUCT_NAME LIKE '%3TO5%' THEN '3-5 Y'
								WHEN PRODUCT_NAME LIKE '%4TO6%' THEN '4-6 Y'
								WHEN PRODUCT_NAME LIKE '%5TO7%' THEN '5-7 Y'
								WHEN PRODUCT_NAME LIKE '����������ç���¹%' AND CLEANED_BRANDNAME LIKE 'MAMAS&PAPAS' THEN '����������ç���¹'
								ELSE PRESCHOOL_KEYWORD END)
WHERE MOM_AND_KID_PRODUCT_FLAG = 1
AND COALESCE(PREG_FLAG,'N/A') = 'N/A'
AND COALESCE(INFANT_FLAG,'N/A') = 'N/A'
AND COALESCE(TODDLER_FLAG,'N/A') = 'N/A'
AND COALESCE(PRESCHOOL_FLAG,'N/A') = 'N/A'
AND COALESCE(JUNIOR_FLAG,'N/A') = 'N/A'
AND COALESCE(TEEN_FLAG,'N/A') = 'N/A'
AND (PRODUCT_NAME LIKE '%3TO5%'
	OR PRODUCT_NAME LIKE '%4TO6%'
	OR PRODUCT_NAME LIKE '%5TO7%'
	OR (PRODUCT_NAME LIKE '����������ç���¹%' AND CLEANED_BRANDNAME LIKE 'MAMAS&PAPAS'))
;

--JUNIOR 6-13Y
UPDATE vpm_data.pm_skutagging_kids_product_master
SET JUNIOR_FLAG = 'YES'
	,JUNIOR_KEYWORD = (CASE WHEN PRODUCT_NAME LIKE '%5TO7%' THEN '5-7 Y'
							WHEN PRODUCT_NAME LIKE '%6-7Y%' THEN '6-7 Y'
							WHEN PRODUCT_NAME LIKE '%7-8Y%' THEN '7-8 Y'
							WHEN PRODUCT_NAME LIKE '%7TO9%' THEN '7-9 Y'
							WHEN PRODUCT_NAME LIKE '%8Y%' THEN '8Y'
							WHEN PRODUCT_NAME LIKE '%8-9Y%' THEN '8-9 Y'
							WHEN PRODUCT_NAME LIKE '%9-10Y%' THEN '9-10 Y'
							WHEN PRODUCT_NAME LIKE '%9TO12%' THEN '9-12 Y'
							WHEN PRODUCT_NAME LIKE '%10-11Y%' THEN '10-11 Y'
							WHEN PRODUCT_NAME LIKE '%11-12Y%' THEN '11-12 Y'
							WHEN PRODUCT_NAME LIKE '%12-13%' THEN '12-13 Y'
							ELSE JUNIOR_KEYWORD END)
WHERE MOM_AND_KID_PRODUCT_FLAG = 1
AND COALESCE(PREG_FLAG,'N/A') = 'N/A'
AND COALESCE(INFANT_FLAG,'N/A') = 'N/A'
AND COALESCE(TODDLER_FLAG,'N/A') = 'N/A'
AND COALESCE(PRESCHOOL_FLAG,'N/A') = 'N/A'
AND COALESCE(JUNIOR_FLAG,'N/A') = 'N/A'
AND COALESCE(TEEN_FLAG,'N/A') = 'N/A'
AND REVISED_CATEGORY_LEVEL_1 = 'KIDS'
AND REVISED_CATEGORY_LEVEL_2 = 'KID ACCESSORIES'
AND REVISED_CATEGORY_LEVEL_3 = 'OTHER KID ACCESSORIES'
AND (PRODUCT_NAME LIKE '%5TO7%'
	OR PRODUCT_NAME LIKE '%6-7Y%'
	OR PRODUCT_NAME LIKE '%7-8Y%'
	OR PRODUCT_NAME LIKE '%7TO9%'
	OR PRODUCT_NAME LIKE '%8Y%'
	OR PRODUCT_NAME LIKE '%8-9Y%'
	OR PRODUCT_NAME LIKE '%9-10Y%'
	OR PRODUCT_NAME LIKE '%9TO12%'
	OR PRODUCT_NAME LIKE '%10-11Y%'
	OR PRODUCT_NAME LIKE '%11-12Y%'
	OR PRODUCT_NAME LIKE '%12-13%')
;

--JUNIOR AND TEEN ACC
UPDATE vpm_data.pm_skutagging_kids_product_master
SET JUNIOR_FLAG = 'YES'
	,TEEN_FLAG = 'YES'
	,JUNIOR_KEYWORD = (CASE WHEN PRODUCT_NAME LIKE '%STUDENT%' THEN 'STUDENT'
							WHEN CLEANED_BRANDNAME LIKE 'CARSON' THEN 'CARSON SOCKS'
							WHEN CLEANED_BRANDNAME LIKE 'BLACKKIWI' THEN 'KID SLIPPERS'
							ELSE JUNIOR_KEYWORD END)
	,TEEN_KEYWORD = (CASE WHEN PRODUCT_NAME LIKE '%STUDENT%' THEN 'STUDENT'
							WHEN CLEANED_BRANDNAME LIKE 'CARSON' THEN 'CARSON SOCKS'
							WHEN CLEANED_BRANDNAME LIKE 'BLACKKIWI' THEN 'KID SLIPPERS'
							ELSE TEEN_KEYWORD END)
WHERE MOM_AND_KID_PRODUCT_FLAG = 1
AND COALESCE(PREG_FLAG,'N/A') = 'N/A'
AND COALESCE(INFANT_FLAG,'N/A') = 'N/A'
AND COALESCE(TODDLER_FLAG,'N/A') = 'N/A'
AND COALESCE(PRESCHOOL_FLAG,'N/A') = 'N/A'
AND COALESCE(JUNIOR_FLAG,'N/A') = 'N/A'
AND COALESCE(TEEN_FLAG,'N/A') = 'N/A'
AND REVISED_CATEGORY_LEVEL_1 = 'KIDS'
AND REVISED_CATEGORY_LEVEL_2 = 'KID ACCESSORIES'
AND REVISED_CATEGORY_LEVEL_3 = 'OTHER KID ACCESSORIES'
AND (PRODUCT_NAME LIKE '%STUDENT%'
	OR CLEANED_BRANDNAME IN ('BLACKKIWI','CARSON'))
;

--TEEN 13-14Y
UPDATE vpm_data.pm_skutagging_kids_product_master
SET TEEN_FLAG = 'YES'
	,TEEN_KEYWORD = (CASE WHEN PRODUCT_NAME LIKE '%13-14Y%' THEN '13-14 Y'
							WHEN CLEANED_BRANDNAME LIKE 'SWEET SUMMER' THEN 'KID STATIONERY'
							ELSE TEEN_KEYWORD END)
WHERE MOM_AND_KID_PRODUCT_FLAG = 1
AND COALESCE(PREG_FLAG,'N/A') = 'N/A'
AND COALESCE(INFANT_FLAG,'N/A') = 'N/A'
AND COALESCE(TODDLER_FLAG,'N/A') = 'N/A'
AND COALESCE(PRESCHOOL_FLAG,'N/A') = 'N/A'
AND COALESCE(JUNIOR_FLAG,'N/A') = 'N/A'
AND COALESCE(TEEN_FLAG,'N/A') = 'N/A'
AND REVISED_CATEGORY_LEVEL_1 = 'KIDS'
AND REVISED_CATEGORY_LEVEL_2 = 'KID ACCESSORIES'
AND REVISED_CATEGORY_LEVEL_3 = 'OTHER KID ACCESSORIES'
AND (PRODUCT_NAME LIKE '%13-14Y%'
	OR CLEANED_BRANDNAME LIKE 'SWEET SUMMER' --STATIONERY
	)
;

/* KID FASHION */
-----KID FASHION ACCESSORIES
--NEW INFANT
UPDATE vpm_data.pm_skutagging_kids_product_master
SET INFANT_FLAG = 'YES'
	,"STAGE_M+3_TO_M+6" = (CASE WHEN CLEANED_BRANDNAME LIKE 'I PLAY' AND PRODUCT_NAME LIKE '%6 MON' THEN 'YES' ELSE "STAGE_M+3_TO_M+6" END)
	,TODDLER_FLAG = (CASE WHEN CLEANED_BRANDNAME LIKE 'I PLAY' AND PRODUCT_NAME LIKE '%18 MON' THEN 'YES'
									WHEN SUBCLASS_NAME = 'SKIRTS' AND PRODUCT_NAME LIKE '%1T' THEN 'YES'
									WHEN SUBDEPT_NAME = 'CHILDREN LUXE FASHION' AND CLEANED_BRANDNAME = 'BABY DIOR' AND PRODUCT_NAME LIKE '% 2' THEN 'YES'
									ELSE TODDLER_FLAG END)
	,UNKNOWN_INFANT_SUBSTAGE = (CASE WHEN PRODUCT_NAME LIKE 'INFANT%CARE%SET%' THEN 'YES'
									WHEN PRODUCT_NAME LIKE '%CHANGING%BAG%' THEN 'YES'
									WHEN PRODUCT_NAME LIKE '%0-2Y' THEN 'YES'
									WHEN PRODUCT_NAME LIKE '%0 2Y' THEN 'YES'
									WHEN PRODUCT_NAME LIKE '%0 2 Y' THEN 'YES'
									WHEN PRODUCT_NAME LIKE '%INFANT%' THEN 'YES'
									WHEN PRODUCT_NAME LIKE '% TU' THEN 'YES'
									ELSE UNKNOWN_INFANT_SUBSTAGE END)
	,INFANT_4_6M_KEYWORD = (CASE WHEN CLEANED_BRANDNAME LIKE 'I PLAY' AND PRODUCT_NAME LIKE '%6 MON' THEN '6 MTH' ELSE INFANT_4_6M_KEYWORD END)
	,TODDLER_KEYWORD = (CASE WHEN CLEANED_BRANDNAME LIKE 'I PLAY' AND PRODUCT_NAME LIKE '%18 MON' THEN '18 MTH'
									WHEN SUBCLASS_NAME = 'SKIRTS' AND PRODUCT_NAME LIKE '%1T' THEN '1Y'
									WHEN SUBDEPT_NAME = 'CHILDREN LUXE FASHION' AND CLEANED_BRANDNAME = 'BABY DIOR' AND PRODUCT_NAME LIKE '% 2' THEN '2Y'
									ELSE TODDLER_KEYWORD END)
	,INFANT_UNKNOWN_KEYWORD = (CASE WHEN PRODUCT_NAME LIKE 'INFANT%CARE%SET%' THEN 'INFANT CARE SET'
									WHEN PRODUCT_NAME LIKE '%CHANGING%BAG%' THEN 'CHANGING BAG'
									WHEN PRODUCT_NAME LIKE '%0-2Y' THEN '0-2 Y'
									WHEN PRODUCT_NAME LIKE '%0 2Y' THEN '0-2 Y'
									WHEN PRODUCT_NAME LIKE '%0 2 Y' THEN '0-2 Y'
									WHEN PRODUCT_NAME LIKE '%INFANT%' THEN 'INFANT'
									WHEN PRODUCT_NAME LIKE '% TU' THEN 'CHANGING BAG'
									ELSE INFANT_UNKNOWN_KEYWORD END)
WHERE MOM_AND_KID_PRODUCT_FLAG = 1
AND COALESCE(PREG_FLAG,'N/A') = 'N/A'
AND COALESCE(INFANT_FLAG,'N/A') = 'N/A'
AND COALESCE(TODDLER_FLAG,'N/A') = 'N/A'
AND COALESCE(PRESCHOOL_FLAG,'N/A') = 'N/A'
AND COALESCE(JUNIOR_FLAG,'N/A') = 'N/A'
AND COALESCE(TEEN_FLAG,'N/A') = 'N/A'
AND REVISED_CATEGORY_LEVEL_1 = 'KIDS'
AND REVISED_CATEGORY_LEVEL_2 = 'KID FASHION'
AND REVISED_CATEGORY_LEVEL_3 = 'KID FASHION ACCESSORIES'
AND (PRODUCT_NAME LIKE 'INFANT%CARE%SET%'
	OR PRODUCT_NAME LIKE '%CHANGING%BAG%'
	OR PRODUCT_NAME LIKE '% TU'
	OR (SUBCLASS_NAME = 'SKIRTS' AND PRODUCT_NAME LIKE '%1T')
	OR PRODUCT_NAME LIKE '%0-2Y'
	OR PRODUCT_NAME LIKE '%0 2Y'
	OR PRODUCT_NAME LIKE '%0 2 Y'
	OR PRODUCT_NAME LIKE '%INFANT%'
	OR (CLEANED_BRANDNAME LIKE 'I PLAY' AND (PRODUCT_NAME LIKE '%18 MON' OR PRODUCT_NAME LIKE '%6 MON'))
	OR (SUBDEPT_NAME = 'CHILDREN LUXE FASHION' AND CLEANED_BRANDNAME = 'BABY DIOR' AND PRODUCT_NAME LIKE '% 2')
	)
;

--NEW INFANT/TODDLER
UPDATE vpm_data.pm_skutagging_kids_product_master
SET INFANT_FLAG = 'YES'
	,UNKNOWN_INFANT_SUBSTAGE = 'YES'
	,TODDLER_FLAG = 'YES'
	,INFANT_UNKNOWN_KEYWORD = (CASE WHEN PRODUCT_NAME LIKE '%0-3 Y' THEN '0-3 Y'
									WHEN PRODUCT_NAME LIKE '%0-3Y' THEN '0-3 Y'
									WHEN PRODUCT_NAME LIKE '%0 3 Y%' THEN '0-3 Y'
									WHEN (CLEANED_BRANDNAME LIKE 'AILEBEBE' AND PRODUCT_NAME NOT LIKE '%4Y') THEN 'BABY ACCESSORIES'
									WHEN (CLEANED_BRANDNAME LIKE 'I PLAY' AND PRODUCT_NAME NOT LIKE '%[0-9]%') THEN 'BABY SWIMWEAR'
									ELSE INFANT_UNKNOWN_KEYWORD END)
	,TODDLER_KEYWORD = (CASE WHEN PRODUCT_NAME LIKE '%0-3 Y' THEN '0-3 Y'
									WHEN PRODUCT_NAME LIKE '%0-3Y' THEN '0-3 Y'
									WHEN PRODUCT_NAME LIKE '%0 3 Y%' THEN '0-3 Y'
									WHEN (CLEANED_BRANDNAME LIKE 'AILEBEBE' AND PRODUCT_NAME NOT LIKE '%4Y') THEN 'BABY ACCESSORIES'
									WHEN (CLEANED_BRANDNAME LIKE 'I PLAY' AND PRODUCT_NAME NOT LIKE '%[0-9]%') THEN 'BABY SWIMWEAR'
									ELSE TODDLER_KEYWORD END)
WHERE MOM_AND_KID_PRODUCT_FLAG = 1
AND COALESCE(PREG_FLAG,'N/A') = 'N/A'
AND COALESCE(INFANT_FLAG,'N/A') = 'N/A'
AND COALESCE(TODDLER_FLAG,'N/A') = 'N/A'
AND COALESCE(PRESCHOOL_FLAG,'N/A') = 'N/A'
AND COALESCE(JUNIOR_FLAG,'N/A') = 'N/A'
AND COALESCE(TEEN_FLAG,'N/A') = 'N/A'
AND REVISED_CATEGORY_LEVEL_1 = 'KIDS'
AND REVISED_CATEGORY_LEVEL_2 = 'KID FASHION'
AND REVISED_CATEGORY_LEVEL_3 = 'KID FASHION ACCESSORIES'
AND (PRODUCT_NAME LIKE '%0-3 Y'
	OR PRODUCT_NAME LIKE '%0-3Y'
	OR PRODUCT_NAME LIKE '%0 3 Y%'
	OR (CLEANED_BRANDNAME LIKE 'AILEBEBE' AND PRODUCT_NAME NOT LIKE '%4Y')
	OR (CLEANED_BRANDNAME LIKE 'I PLAY' AND PRODUCT_NAME NOT LIKE '%[0-9]%'))
;

--NEW TODDLER
UPDATE vpm_data.pm_skutagging_kids_product_master
SET TODDLER_FLAG = 'YES'
	,TODDLER_KEYWORD = (CASE WHEN CLEANED_BRANDNAME IN ('PAUL SMITH JUNIOR','KENZO KIDS') AND PRODUCT_NAME LIKE '%24A' THEN '2-4 Y'
							WHEN CLEANED_BRANDNAME IN ('PAUL SMITH JUNIOR','KENZO KIDS') AND PRODUCT_NAME LIKE '%26A' THEN '2-6 Y'
							WHEN CLEANED_BRANDNAME IN ('PAUL SMITH JUNIOR','KENZO KIDS') AND PRODUCT_NAME LIKE '%3A' THEN '3Y'
							WHEN CLEANED_BRANDNAME IN ('PAUL SMITH JUNIOR','KENZO KIDS') AND (PRODUCT_NAME LIKE '%4A' AND PRODUCT_NAME NOT LIKE '%14A') THEN '4Y'
							WHEN CLEANED_BRANDNAME IN ('PAUL SMITH JUNIOR','KENZO KIDS') AND PRODUCT_NAME LIKE '% 2326' THEN 'EU SIZE 23-26'
							WHEN CLEANED_BRANDNAME IN ('PAUL SMITH JUNIOR','KENZO KIDS') AND PRODUCT_NAME LIKE '%23 26' THEN 'EU SIZE 23-26'
							WHEN CLEANED_BRANDNAME IN ('PAUL SMITH JUNIOR','KENZO KIDS') AND PRODUCT_NAME LIKE 'SOCK%23' THEN 'EU SIZE 23'
							WHEN SUBDEPT_NAME = 'CHILDREN LUXE FASHION' AND CLEANED_BRANDNAME = 'BABY DIOR' AND PRODUCT_NAME LIKE '% 3' THEN '3Y'
							WHEN SUBDEPT_NAME = 'CHILDREN LUXE FASHION' AND CLEANED_BRANDNAME = 'BABY DIOR' AND PRODUCT_NAME LIKE '% 4' THEN '4Y'
							WHEN SUBCLASS_NAME = 'SKIRTS' AND PRODUCT_NAME LIKE '%3T' THEN '3Y'
							WHEN SUBCLASS_NAME = 'SKIRTS' AND PRODUCT_NAME LIKE '%4T' THEN '4Y'
							WHEN SUBDEPT_NAME = 'INFANT/TODDLERS' AND PRODUCT_NAME LIKE '%36' THEN '3Y'
							WHEN SUBDEPT_NAME = '4 - 8 BOYS' AND PRODUCT_NAME LIKE '%�ͧ���%26' THEN 'EU SIZE 26'
							WHEN SUBDEPT_NAME = '4 - 8 GIRLS' AND PRODUCT_NAME LIKE '%SHOE%26' THEN 'EU SIZE 26'
							WHEN SUBDEPT_NAME = '4 - 8 GIRLS' AND PRODUCT_NAME LIKE '%�ͧ���%27' THEN 'EU SIZE 27'
							WHEN SUBDEPT_NAME = '4 - 8 GIRLS' AND PRODUCT_NAME LIKE '%SHOE%27' THEN 'EU SIZE 27'
							WHEN SUBDEPT_NAME = '4 - 8 GIRLS' AND PRODUCT_NAME LIKE '%�ا��ͧ2-4%' THEN '2-4 Y'
							WHEN SUBDEPT_NAME = '4 - 8 GIRLS' AND PRODUCT_NAME LIKE '% 4' THEN '4Y'
							WHEN SUBDEPT_NAME = '4 - 8 GIRLS' AND PRODUCT_NAME LIKE '%36' THEN '3Y'
							WHEN SUBDEPT_NAME = 'CHILDREN S ACCESSORIES' AND PRODUCT_NAME LIKE '%3-5Y' THEN '3-5 Y'
							WHEN SUBDEPT_NAME = 'CHILDREN S ACCESSORIES' AND PRODUCT_NAME LIKE '%2 6Y' THEN '2-6 Y'
							WHEN SUBDEPT_NAME = 'CHILDREN S ACCESSORIES' AND PRODUCT_NAME LIKE '%2 5Y' THEN '2-5 Y'
							WHEN SUBDEPT_NAME = 'CHILDREN S ACCESSORIES' AND PRODUCT_NAME LIKE '%3 6Y%' THEN '3-6 Y'
							WHEN SUBDEPT_NAME = 'CHILDREN S ACCESSORIES' AND PRODUCT_NAME LIKE '%4Y' THEN '4Y'
							WHEN SUBDEPT_NAME = 'CHILDREN S ACCESSORIES' AND PRODUCT_NAME LIKE '%4 Y' THEN '4Y'
							WHEN SUBDEPT_NAME = 'CHILDREN S ACCESSORIES' AND PRODUCT_NAME LIKE '��ǡ%48%' THEN 'CAP/HAT 48 CM'
							WHEN SUBDEPT_NAME = 'CHILDREN S ACCESSORIES' AND PRODUCT_NAME LIKE '��ǡ%50%' THEN 'CAP/HAT 50 CM'
							WHEN SUBDEPT_NAME = 'CHILDREN S ACCESSORIES' AND PRODUCT_NAME LIKE '����%50%' THEN 'CAP/HAT 50 CM'
							WHEN SUBDEPT_NAME = 'CHILDREN S ACCESSORIES' AND PRODUCT_NAME LIKE '%��٧%' THEN '��٧'
							WHEN SUBDEPT_NAME = 'CHILDREN S ACCESSORIES' AND PRODUCT_NAME LIKE '%���ا%' THEN '��٧'
							WHEN SUBDEPT_NAME = 'CHILDREN S ACCESSORIES' AND PRODUCT_NAME LIKE '%�ͧ��Һ�����%160%' THEN '�ͧ��Һ���ŵ� 16 CM'
							WHEN SUBDEPT_NAME = 'CHILDREN S ACCESSORIES' AND PRODUCT_NAME LIKE '%�ͧ��Һ�����%170%' THEN '�ͧ��Һ���ŵ� 17 CM'
							WHEN SUBDEPT_NAME = 'CHILDREN S ACCESSORIES' AND PRODUCT_NAME LIKE '%�ͧ��Һ�����%180%' THEN '�ͧ��Һ���ŵ� 18 CM'
							WHEN SUBDEPT_NAME = 'CHILDREN S ACCESSORIES' AND (CLEANED_BRANDNAME LIKE 'I PLAY' AND PRODUCT_NAME LIKE '%4 Y') THEN '4Y'
							WHEN SUBDEPT_NAME = 'CHILDREN S ACCESSORIES' AND CLEANED_BRANDNAME LIKE 'GLITZ' THEN 'GLITZ ACCESSORIES'
							WHEN (CLEANED_BRANDNAME LIKE 'AILEBEBE' AND PRODUCT_NAME LIKE '%4Y') THEN '4Y'
							ELSE TODDLER_KEYWORD END)
WHERE MOM_AND_KID_PRODUCT_FLAG = 1
AND COALESCE(PREG_FLAG,'N/A') = 'N/A'
AND COALESCE(INFANT_FLAG,'N/A') = 'N/A'
AND COALESCE(TODDLER_FLAG,'N/A') = 'N/A'
AND COALESCE(PRESCHOOL_FLAG,'N/A') = 'N/A'
AND COALESCE(JUNIOR_FLAG,'N/A') = 'N/A'
AND COALESCE(TEEN_FLAG,'N/A') = 'N/A'
AND REVISED_CATEGORY_LEVEL_1 = 'KIDS'
AND REVISED_CATEGORY_LEVEL_2 = 'KID FASHION'
AND REVISED_CATEGORY_LEVEL_3 = 'KID FASHION ACCESSORIES'
AND (CLEANED_BRANDNAME IN ('PAUL SMITH JUNIOR','KENZO KIDS') AND (PRODUCT_NAME LIKE '%24A'
														OR PRODUCT_NAME LIKE '%26A'
														OR PRODUCT_NAME LIKE '%3A'
														OR (PRODUCT_NAME LIKE '%4A' AND PRODUCT_NAME NOT LIKE '%14A')
														OR PRODUCT_NAME LIKE '% 2326'
														OR PRODUCT_NAME LIKE '%23 26'
														OR PRODUCT_NAME LIKE 'SOCK%23')
	OR (SUBDEPT_NAME = 'CHILDREN LUXE FASHION' AND CLEANED_BRANDNAME = 'BABY DIOR' AND (PRODUCT_NAME LIKE '% 3'
																				OR PRODUCT_NAME LIKE '% 4'))
	OR (SUBCLASS_NAME = 'SKIRTS' AND (PRODUCT_NAME LIKE '%3T'
									OR PRODUCT_NAME LIKE '%4T'))
	OR (SUBDEPT_NAME = 'INFANT/TODDLERS' AND PRODUCT_NAME LIKE '%36')
	OR (SUBDEPT_NAME = '4 - 8 BOYS' AND PRODUCT_NAME LIKE '%36')
	OR (SUBDEPT_NAME = '4 - 8 GIRLS' AND (PRODUCT_NAME LIKE '%�ͧ���%26'
										OR PRODUCT_NAME LIKE '%SHOE%26'
										OR PRODUCT_NAME LIKE '%�ͧ���%27'
										OR PRODUCT_NAME LIKE '%SHOE%27'
										OR PRODUCT_NAME LIKE '%�ا��ͧ2-4%'
										OR PRODUCT_NAME LIKE '% 4'))
	OR (SUBDEPT_NAME = 'CHILDREN S ACCESSORIES' AND (PRODUCT_NAME LIKE '%3-5Y'
													OR PRODUCT_NAME LIKE '%2 6Y'
													OR PRODUCT_NAME LIKE '%2 5Y'
													OR PRODUCT_NAME LIKE '%3 6Y%'
													OR PRODUCT_NAME LIKE '%4Y'
													OR PRODUCT_NAME LIKE '%4 Y'
													OR PRODUCT_NAME LIKE '��ǡ%48%'
													OR PRODUCT_NAME LIKE '��ǡ%50%'
													OR PRODUCT_NAME LIKE '����%50%'
													OR PRODUCT_NAME LIKE '%��٧%'
													OR PRODUCT_NAME LIKE '%���ا%'
													OR PRODUCT_NAME LIKE '%�ͧ��Һ�����%160%'
													OR PRODUCT_NAME LIKE '%�ͧ��Һ�����%170%'
													OR PRODUCT_NAME LIKE '%�ͧ��Һ�����%180%'
													OR (CLEANED_BRANDNAME LIKE 'I PLAY' AND PRODUCT_NAME LIKE '%4 Y')
													OR CLEANED_BRANDNAME LIKE 'GLITZ'))
	OR (CLEANED_BRANDNAME LIKE 'AILEBEBE' AND PRODUCT_NAME LIKE '%4Y')
	)
;
	
--PRESCHOOL
UPDATE vpm_data.pm_skutagging_kids_product_master
SET PRESCHOOL_FLAG = 'YES'
	,PRESCHOOL_KEYWORD = (CASE WHEN SUBCLASS_NAME = 'BABY' THEN 'BABY'
								WHEN SUBCLASS_NAME = 'JUNIOR' AND PRODUCT_NAME LIKE '% S' THEN 'JUNIOR SIZE S'
								WHEN SUBCLASS_NAME = 'JUNIOR' AND PRODUCT_NAME LIKE '% 6' THEN '6Y'
								WHEN CLEANED_BRANDNAME = 'EMPORIO ARMANI' AND PRODUCT_NAME LIKE '% 6A' THEN '6Y'
								WHEN CLEANED_BRANDNAME = 'EMPORIO ARMANI' AND PRODUCT_NAME LIKE '% 28' THEN 'EU SIZE 28'
								WHEN CLEANED_BRANDNAME = 'EMPORIO ARMANI' AND PRODUCT_NAME LIKE '% 29' THEN 'EU SIZE 29'
								WHEN CLEANED_BRANDNAME = 'EMPORIO ARMANI' AND PRODUCT_NAME LIKE '% 30' THEN 'EU SIZE 30'
								WHEN CLEANED_BRANDNAME = 'EMPORIO ARMANI' AND PRODUCT_NAME LIKE '% 31' THEN 'EU SIZE 31'
								WHEN SUBDEPT_NAME = 'CHILDREN LUXE FASHION' AND CLEANED_BRANDNAME = 'BABY DIOR' AND PRODUCT_NAME LIKE '% 5' THEN '5Y'
								WHEN SUBDEPT_NAME = 'CHILDREN LUXE FASHION' AND CLEANED_BRANDNAME = 'BABY DIOR' AND PRODUCT_NAME LIKE '% 6' THEN '6Y'
								WHEN SUBDEPT_NAME = 'CHILDREN LUXE FASHION' AND CLEANED_BRANDNAME = 'BABY DIOR' AND PRODUCT_NAME LIKE 'SCARF%' THEN 'BABY DIOR SCARF'
								WHEN CLEANED_BRANDNAME IN ('PAUL SMITH JUNIOR','KENZO KIDS') AND PRODUCT_NAME LIKE '%5A' THEN '5Y'
								WHEN CLEANED_BRANDNAME IN ('PAUL SMITH JUNIOR','KENZO KIDS') AND PRODUCT_NAME LIKE '%6A' THEN '6Y'
								WHEN CLEANED_BRANDNAME IN ('PAUL SMITH JUNIOR','KENZO KIDS') AND PRODUCT_NAME LIKE 'CAP%56CM' THEN 'CAP 56 CM'
								WHEN CLEANED_BRANDNAME IN ('PAUL SMITH JUNIOR','KENZO KIDS') AND PRODUCT_NAME LIKE '% 2730' THEN 'EU SIZE 27-30'
								WHEN CLEANED_BRANDNAME IN ('PAUL SMITH JUNIOR','KENZO KIDS') AND PRODUCT_NAME LIKE 'SOCK%27 30' THEN 'EU SIZE 27-30'
								WHEN CLEANED_BRANDNAME IN ('PAUL SMITH JUNIOR','KENZO KIDS') AND PRODUCT_NAME LIKE 'SOCK%27' THEN 'EU SIZE 27'
								WHEN CLEANED_BRANDNAME IN ('PAUL SMITH JUNIOR','KENZO KIDS') AND PRODUCT_NAME LIKE 'SOCK%31' THEN 'EU SIZE 31'
								WHEN SUBCLASS_NAME = 'SKIRTS' AND PRODUCT_NAME LIKE '%5T' THEN '5Y'
								WHEN SUBCLASS_NAME = 'SKIRTS' AND PRODUCT_NAME LIKE '%6T' THEN '6Y'
								WHEN SUBCLASS_NAME = 'SKIRTS' AND PRODUCT_NAME LIKE '% 6' THEN '6Y'
								WHEN SUBDEPT_NAME = '4 - 8 GIRLS' AND PRODUCT_NAME LIKE '%BOOT%28' THEN 'BOOTS EU SIZE 28'
								WHEN SUBDEPT_NAME = '4 - 8 GIRLS' AND PRODUCT_NAME LIKE '%S HOE%28' THEN 'SHOES EU SIZE 28'
								WHEN SUBDEPT_NAME = '4 - 8 GIRLS' AND PRODUCT_NAME LIKE '%SANDAL%28' THEN 'SANDALS EU SIZE 28'
								WHEN SUBDEPT_NAME = '4 - 8 GIRLS' AND PRODUCT_NAME LIKE '%SHOE%28' THEN 'SHOES EU SIZE 28'
								WHEN SUBDEPT_NAME = '4 - 8 GIRLS' AND PRODUCT_NAME LIKE '%SNEAKER%28' THEN 'SNEAKERS EU SIZE 28'
								WHEN SUBDEPT_NAME = '4 - 8 GIRLS' AND PRODUCT_NAME LIKE '%�ͧ���%28' THEN 'SHOES EU SIZE 28'
								WHEN SUBDEPT_NAME = '4 - 8 GIRLS' AND PRODUCT_NAME LIKE '%BOOT%29' THEN 'BOOTS EU SIZE 29'
								WHEN SUBDEPT_NAME = '4 - 8 GIRLS' AND PRODUCT_NAME LIKE '%S HOE%29' THEN 'SHOES EU SIZE 29'
								WHEN SUBDEPT_NAME = '4 - 8 GIRLS' AND PRODUCT_NAME LIKE '%SANDAL%29' THEN 'SANDALS EU SIZE 29'
								WHEN SUBDEPT_NAME = '4 - 8 GIRLS' AND PRODUCT_NAME LIKE '%SHOE%29' THEN 'SHOES EU SIZE 29'
								WHEN SUBDEPT_NAME = '4 - 8 GIRLS' AND PRODUCT_NAME LIKE '%SNEAKER%29' THEN 'SNEAKERS EU SIZE 29'
								WHEN SUBDEPT_NAME = '4 - 8 GIRLS' AND PRODUCT_NAME LIKE '%�ͧ���%29' THEN 'SHOES EU SIZE 29'
								WHEN SUBDEPT_NAME = '4 - 8 GIRLS' AND PRODUCT_NAME LIKE '%BOOT%30' THEN 'BOOTS EU SIZE 30'
								WHEN SUBDEPT_NAME = '4 - 8 GIRLS' AND PRODUCT_NAME LIKE '%S HOE%30' THEN 'SHOES EU SIZE 30'
								WHEN SUBDEPT_NAME = '4 - 8 GIRLS' AND PRODUCT_NAME LIKE '%SANDAL%30' THEN 'SANDALS EU SIZE 30'
								WHEN SUBDEPT_NAME = '4 - 8 GIRLS' AND PRODUCT_NAME LIKE '%SHOE%30' THEN 'SHOES EU SIZE 30'
								WHEN SUBDEPT_NAME = '4 - 8 GIRLS' AND PRODUCT_NAME LIKE '%SNEAKER%30' THEN 'SNEAKERS EU SIZE 30'
								WHEN SUBDEPT_NAME = '4 - 8 GIRLS' AND PRODUCT_NAME LIKE '%�ͧ���%30' THEN 'SHOES EU SIZE 30'
								WHEN SUBDEPT_NAME = '4 - 8 GIRLS' AND PRODUCT_NAME LIKE '%BOOT%31' THEN 'BOOTS EU SIZE 31'
								WHEN SUBDEPT_NAME = '4 - 8 GIRLS' AND PRODUCT_NAME LIKE '%S HOE%31' THEN 'SHOES EU SIZE 31'
								WHEN SUBDEPT_NAME = '4 - 8 GIRLS' AND PRODUCT_NAME LIKE '%SANDAL%31' THEN 'SANDALS EU SIZE 31'
								WHEN SUBDEPT_NAME = '4 - 8 GIRLS' AND PRODUCT_NAME LIKE '%SHOE%31' THEN 'SHOES EU SIZE 31'
								WHEN SUBDEPT_NAME = '4 - 8 GIRLS' AND PRODUCT_NAME LIKE '%SNEAKER%31' THEN 'SNEAKERS EU SIZE 31'
								WHEN SUBDEPT_NAME = '4 - 8 GIRLS' AND PRODUCT_NAME LIKE '%�ͧ���%31' THEN 'SHOES EU SIZE 31'
								WHEN SUBDEPT_NAME = '4 - 8 GIRLS' AND PRODUCT_NAME LIKE '%T SHIRT%6' THEN 'T-SHIRT 6Y'
								WHEN SUBDEPT_NAME = '4 - 8 GIRLS' AND (PRODUCT_NAME LIKE '% 6' AND PRODUCT_NAME NOT LIKE '%HAT%6' AND PRODUCT_NAME NOT LIKE '%CAP%6') THEN '6Y'
								WHEN SUBDEPT_NAME = '4 - 8 GIRLS' AND PRODUCT_NAME LIKE '%CAP%3' THEN 'CAP/HAT SFERA SIZE 3 (3-6 Y)'
								WHEN SUBDEPT_NAME = '4 - 8 GIRLS' AND PRODUCT_NAME LIKE '%HAT%3' THEN 'CAP/HAT SFERA SIZE 3 (3-6 Y)'
								WHEN SUBDEPT_NAME = '4 - 8 GIRLS' AND PRODUCT_NAME LIKE 'CAP%S' THEN 'CAP/HAT SFERA SIZE S'
								WHEN SUBDEPT_NAME = '4 - 8 GIRLS' AND PRODUCT_NAME LIKE 'SET%5' THEN '5Y'
								WHEN SUBDEPT_NAME = '4 - 8 GIRLS' AND PRODUCT_NAME LIKE '�ҧࡧ%�%60' THEN '�ҧࡧ� 60'
								WHEN SUBDEPT_NAME = '4 - 8 GIRLS' AND PRODUCT_NAME LIKE '�ҧࡧ%�%65' THEN '�ҧࡧ� 65'
								WHEN SUBDEPT_NAME = '4 - 8 GIRLS' AND PRODUCT_NAME LIKE '����͡����%60' THEN '����͡���� 60'
								WHEN SUBDEPT_NAME = '4 - 8 GIRLS' AND PRODUCT_NAME LIKE '����͡����%65' THEN '����͡���� 65'
								WHEN SUBDEPT_NAME = '4 - 8 GIRLS' AND PRODUCT_NAME LIKE '�ا���%3-5' THEN '3-5 Y'
								WHEN SUBDEPT_NAME = '4 - 8 GIRLS' AND PRODUCT_NAME LIKE '�ا���%4-6' THEN '4-6 Y'
								WHEN SUBDEPT_NAME = '4 - 8 GIRLS' AND PRODUCT_NAME LIKE '�ا���%3-5' THEN '3-5 Y'
								WHEN SUBDEPT_NAME = 'CHILDREN S ACCESSORIES' AND PRODUCT_NAME LIKE '%3-5Y' THEN '3-5 Y'
								WHEN SUBDEPT_NAME = 'CHILDREN S ACCESSORIES' AND PRODUCT_NAME LIKE '%2 5Y' THEN '2-5 Y'
								WHEN SUBDEPT_NAME = 'CHILDREN S ACCESSORIES' AND PRODUCT_NAME LIKE '%2 6Y' THEN '2-6 Y'
								WHEN SUBDEPT_NAME = 'CHILDREN S ACCESSORIES' AND PRODUCT_NAME LIKE '%3 5Y%' THEN '3-5 Y'
								WHEN SUBDEPT_NAME = 'CHILDREN S ACCESSORIES' AND PRODUCT_NAME LIKE '%3 6Y%' THEN '3-6 Y'
								WHEN SUBDEPT_NAME = 'CHILDREN S ACCESSORIES' AND PRODUCT_NAME LIKE '%��ǡ%52' THEN 'CAP/HAT 52 CM'
								WHEN SUBDEPT_NAME = 'CHILDREN S ACCESSORIES' AND PRODUCT_NAME LIKE '%��ǡ%54' THEN 'CAP/HAT 54 CM'
								WHEN SUBDEPT_NAME = 'CHILDREN S ACCESSORIES' AND PRODUCT_NAME LIKE '%��٧%' THEN '��٧'
								WHEN SUBDEPT_NAME = 'CHILDREN S ACCESSORIES' AND PRODUCT_NAME LIKE '%��ا%' THEN '��٧'
								WHEN SUBDEPT_NAME = 'CHILDREN S ACCESSORIES' AND PRODUCT_NAME LIKE '%110' THEN '�ش����ŵ� 110 CM'
								WHEN SUBDEPT_NAME = 'CHILDREN S ACCESSORIES' AND PRODUCT_NAME LIKE '�ش��������%120' THEN '�ش����ŵ� 120 CM'
								WHEN SUBDEPT_NAME = 'CHILDREN S ACCESSORIES' AND PRODUCT_NAME LIKE '%������%S%' THEN '�ش����ŵ� SIZE S'
								WHEN SUBDEPT_NAME = 'CHILDREN S ACCESSORIES' AND PRODUCT_NAME LIKE '%�ͧ��Һ�����%190%' THEN '�ͧ��Һ���ŵ� 19 CM'
								WHEN SUBDEPT_NAME = 'CHILDREN S ACCESSORIES' AND PRODUCT_NAME LIKE '%�ͧ��Һ�����%200%' THEN '�ͧ��Һ���ŵ� 20 CM'
								WHEN SUBDEPT_NAME = 'CHILDREN S ACCESSORIES' AND CLEANED_BRANDNAME LIKE 'PLATYPUS' AND PRODUCT_NAME LIKE '%S' THEN 'HAT PLATYPUS SIZE S'
								WHEN SUBDEPT_NAME = 'CHILDREN S ACCESSORIES' AND CLEANED_BRANDNAME IN ('JASMINE','MESUCA','GLITZ','KEED') THEN 'KID ACCESSORIES'
								ELSE PRESCHOOL_KEYWORD END)
WHERE MOM_AND_KID_PRODUCT_FLAG = 1
AND COALESCE(PREG_FLAG,'N/A') = 'N/A'
AND COALESCE(INFANT_FLAG,'N/A') = 'N/A'
AND COALESCE(TODDLER_FLAG,'N/A') = 'N/A'
AND COALESCE(PRESCHOOL_FLAG,'N/A') = 'N/A'
AND COALESCE(JUNIOR_FLAG,'N/A') = 'N/A'
AND COALESCE(TEEN_FLAG,'N/A') = 'N/A'
AND REVISED_CATEGORY_LEVEL_1 = 'KIDS'
AND REVISED_CATEGORY_LEVEL_2 = 'KID FASHION'
AND REVISED_CATEGORY_LEVEL_3 = 'KID FASHION ACCESSORIES'
AND (SUBCLASS_NAME = 'BABY'
	OR (SUBCLASS_NAME = 'JUNIOR' AND PRODUCT_NAME LIKE '% S')
	OR (SUBCLASS_NAME = 'JUNIOR' AND PRODUCT_NAME LIKE '% 6')
	OR (CLEANED_BRANDNAME = 'EMPORIO ARMANI' AND (PRODUCT_NAME LIKE '% 6A'
										OR PRODUCT_NAME LIKE '% 28'
										OR PRODUCT_NAME LIKE '% 29'
										OR PRODUCT_NAME LIKE '% 30'
										OR PRODUCT_NAME LIKE '% 31'))
	OR (SUBDEPT_NAME = 'CHILDREN LUXE FASHION' AND CLEANED_BRANDNAME = 'BABY DIOR' AND (PRODUCT_NAME LIKE '% 5'
																				OR PRODUCT_NAME LIKE '% 6'
																				OR PRODUCT_NAME LIKE 'SCARF%'))
	OR (CLEANED_BRANDNAME IN ('PAUL SMITH JUNIOR','KENZO KIDS') AND (PRODUCT_NAME LIKE '%5A'
															OR PRODUCT_NAME LIKE '%6A'
															OR PRODUCT_NAME LIKE 'CAP%56CM'
															OR PRODUCT_NAME LIKE '% 2730'
															OR PRODUCT_NAME LIKE 'SOCK%27 30'
															OR PRODUCT_NAME LIKE 'SOCK%27'
															OR PRODUCT_NAME LIKE 'SOCK%31'))
	OR (SUBCLASS_NAME = 'SKIRTS' AND (PRODUCT_NAME LIKE '%5T'
									OR PRODUCT_NAME LIKE '%6T'
									OR PRODUCT_NAME LIKE '% 6'))
	OR (SUBDEPT_NAME = '4 - 8 GIRLS' AND (PRODUCT_NAME LIKE '%BOOT%28'
										OR PRODUCT_NAME LIKE '%S HOE%28'
										OR PRODUCT_NAME LIKE '%SANDAL%28'
										OR PRODUCT_NAME LIKE '%SHOE%28'
										OR PRODUCT_NAME LIKE '%SNEAKER%28'
										OR PRODUCT_NAME LIKE '%�ͧ���%28'
										OR PRODUCT_NAME LIKE '%BOOT%29'
										OR PRODUCT_NAME LIKE '%S HOE%29'
										OR PRODUCT_NAME LIKE '%SANDAL%29'
										OR PRODUCT_NAME LIKE '%SHOE%29'
										OR PRODUCT_NAME LIKE '%SNEAKER%29'
										OR PRODUCT_NAME LIKE '%�ͧ���%29'
										OR PRODUCT_NAME LIKE '%BOOT%30'
										OR PRODUCT_NAME LIKE '%S HOE%30'
										OR PRODUCT_NAME LIKE '%SANDAL%30'
										OR PRODUCT_NAME LIKE '%SHOE%30'
										OR PRODUCT_NAME LIKE '%SNEAKER%30'
										OR PRODUCT_NAME LIKE '%�ͧ���%30'
										OR PRODUCT_NAME LIKE '%BOOT%31'
										OR PRODUCT_NAME LIKE '%S HOE%31'
										OR PRODUCT_NAME LIKE '%SANDAL%31'
										OR PRODUCT_NAME LIKE '%SHOE%31'
										OR PRODUCT_NAME LIKE '%SNEAKER%31'
										OR PRODUCT_NAME LIKE '%�ͧ���%31'
										OR PRODUCT_NAME LIKE '%T SHIRT%6'
										OR (PRODUCT_NAME LIKE '% 6' AND PRODUCT_NAME NOT LIKE '%HAT%6' AND PRODUCT_NAME NOT LIKE '%CAP%6')
										OR PRODUCT_NAME LIKE '%CAP%3'
										OR PRODUCT_NAME LIKE '%HAT%3'
										OR PRODUCT_NAME LIKE 'CAP%S'
										OR PRODUCT_NAME LIKE 'SET%5'
										OR PRODUCT_NAME LIKE '�ҧࡧ%�%60'
										OR PRODUCT_NAME LIKE '�ҧࡧ%�%65'
										OR PRODUCT_NAME LIKE '����͡����%60'
										OR PRODUCT_NAME LIKE '����͡����%65'
										OR PRODUCT_NAME LIKE '�ا���%3-5'
										OR PRODUCT_NAME LIKE '�ا���%4-6'
										OR PRODUCT_NAME LIKE '�ا���%3-5'))
	OR (SUBDEPT_NAME = 'CHILDREN S ACCESSORIES' AND (PRODUCT_NAME LIKE '%3-5Y'
													OR PRODUCT_NAME LIKE '%2 5Y'
													OR PRODUCT_NAME LIKE '%2 6Y'
													OR PRODUCT_NAME LIKE '%3 5Y%'
													OR PRODUCT_NAME LIKE '%3 6Y%'
													OR PRODUCT_NAME LIKE '%��ǡ%52'
													OR PRODUCT_NAME LIKE '%��ǡ%54'
													OR PRODUCT_NAME LIKE '%��٧%'
													OR PRODUCT_NAME LIKE '%��ا%'
													OR PRODUCT_NAME LIKE '%110'
													OR PRODUCT_NAME LIKE '�ش��������%120'
													OR PRODUCT_NAME LIKE '%������%S%'
													OR PRODUCT_NAME LIKE '%�ͧ��Һ�����%190%'
													OR PRODUCT_NAME LIKE '%�ͧ��Һ�����%200%'
													OR (CLEANED_BRANDNAME LIKE 'PLATYPUS' AND PRODUCT_NAME LIKE '%S')
													OR CLEANED_BRANDNAME IN ('JASMINE','MESUCA','GLITZ','KEED')))
	)
;

--PRESCHOOL/JUNIOR
UPDATE vpm_data.pm_skutagging_kids_product_master
SET PRESCHOOL_FLAG = 'YES'
	,JUNIOR_FLAG = 'YES'
	,PRESCHOOL_KEYWORD = (CASE WHEN SUBCLASS_NAME = 'JUNIOR' THEN 'GUCCI JUNIOR'
								WHEN SUBCLASS_NAME IN ('HANDBAG','LUGGAGE') THEN 'GUCCI KIDS BAG'
								WHEN CLEANED_BRANDNAME IN ('PAUL SMITH JUNIOR','KENZO KIDS') AND PRODUCT_NAME LIKE '%5A 8A' THEN '5-8 Y'
								WHEN CLEANED_BRANDNAME IN ('PAUL SMITH JUNIOR','KENZO KIDS') AND PRODUCT_NAME LIKE '%5A/8A' THEN '5-8 Y'
								WHEN CLEANED_BRANDNAME IN ('PAUL SMITH JUNIOR','KENZO KIDS') AND PRODUCT_NAME LIKE '%58A' THEN '5-8 Y'
								WHEN CLEANED_BRANDNAME IN ('PAUL SMITH JUNIOR','KENZO KIDS') AND PRODUCT_NAME LIKE '%5A8A' THEN '5-8 Y'
								WHEN CLEANED_BRANDNAME IN ('RAINFLOWER','POOH') THEN 'CHILDREN BAG'
								WHEN SUBDEPT_NAME = '4 - 8 GIRLS' AND PRODUCT_NAME LIKE '%BELT%60' THEN 'BELT SIZE 60'
								WHEN SUBDEPT_NAME = '4 - 8 GIRLS' AND PRODUCT_NAME LIKE '%4 12L' THEN '4-12 Y'
								WHEN SUBDEPT_NAME = '4 - 8 GIRLS' AND CLEANED_BRANDNAME LIKE 'SFERA' AND PRODUCT_NAME LIKE '% U' THEN 'SFERA GIRL ACCESSORIES'
								WHEN SUBDEPT_NAME = '4 - 8 GIRLS' AND PRODUCT_NAME LIKE '�ا��ͧ%4-7' THEN '4-7 Y'
								WHEN SUBDEPT_NAME = '4 - 8 GIRLS' AND PRODUCT_NAME LIKE '�ا���%5-7' THEN '5-7 Y'
								WHEN SUBDEPT_NAME = '4 - 8 GIRLS' AND PRODUCT_NAME LIKE '�ا���%5-8' THEN '5-8 Y'
								WHEN SUBDEPT_NAME = 'CHILDREN S ACCESSORIES' AND PRODUCT_NAME LIKE '%5 7Y' THEN '5-7 Y'
								WHEN SUBDEPT_NAME = 'CHILDREN S ACCESSORIES' AND PRODUCT_NAME LIKE '%3 7YR%' THEN '3-7 Y'
								WHEN SUBDEPT_NAME = 'CHILDREN S ACCESSORIES' AND PRODUCT_NAME LIKE '%3-7 Y' THEN '3-7 Y'
								WHEN SUBDEPT_NAME = 'CHILDREN S ACCESSORIES' AND PRODUCT_NAME LIKE '%AGES 0-7' THEN '0-7 Y'
								WHEN SUBDEPT_NAME = 'CHILDREN S ACCESSORIES' AND PRODUCT_NAME LIKE '%5 9Y' THEN '5-9 Y'
								WHEN SUBDEPT_NAME = 'CHILDREN S ACCESSORIES' AND (CLEANED_BRANDNAME LIKE 'SIMPLE&CHIC' AND PRODUCT_NAME NOT LIKE '%�ش��������%') THEN 'KID ACCESSORIES'
								WHEN SUBDEPT_NAME = 'CHILDREN S ACCESSORIES' AND PRODUCT_NAME LIKE '%����������ҡ%' THEN '����������ҡ'
								WHEN SUBDEPT_NAME = 'CHILDREN S ACCESSORIES' AND CLEANED_BRANDNAME LIKE 'AMELIE'
																			AND PRODUCT_NAME NOT LIKE '%S'
																			AND PRODUCT_NAME NOT LIKE '%M'
																			AND PRODUCT_NAME NOT LIKE '%L'
																			AND PRODUCT_NAME NOT LIKE '%[0-9]%' THEN '�ش/�ͧ��Һ���ŵ�'
								WHEN SUBDEPT_NAME = 'CHILDREN S ACCESSORIES' AND CLEANED_BRANDNAME IN ('ROSSO','THAI TIME','DOJO','STEPHEN JOSEPH','HUGGER','KIDLAND','BECKMANN NORWAY','MYSELF BELTS','POMO'
																							,'SONS AND DAUGHTERS','BEWELL','PUMPKIN PIE','SPIDERMAN 2 KIDS','BARBIE KIDS','BARBIE','BARBIE KIDS PRO') THEN 'KID ACCESSORIES'
																							
								ELSE PRESCHOOL_KEYWORD END)
	,JUNIOR_KEYWORD = (CASE WHEN SUBCLASS_NAME = 'JUNIOR' THEN 'GUCCI JUNIOR'
								WHEN SUBCLASS_NAME IN ('HANDBAG','LUGGAGE') THEN 'GUCCI KIDS BAG'
								WHEN CLEANED_BRANDNAME IN ('PAUL SMITH JUNIOR','KENZO KIDS') AND PRODUCT_NAME LIKE '%5A 8A' THEN '5-8 Y'
								WHEN CLEANED_BRANDNAME IN ('PAUL SMITH JUNIOR','KENZO KIDS') AND PRODUCT_NAME LIKE '%5A/8A' THEN '5-8 Y'
								WHEN CLEANED_BRANDNAME IN ('PAUL SMITH JUNIOR','KENZO KIDS') AND PRODUCT_NAME LIKE '%58A' THEN '5-8 Y'
								WHEN CLEANED_BRANDNAME IN ('PAUL SMITH JUNIOR','KENZO KIDS') AND PRODUCT_NAME LIKE '%5A8A' THEN '5-8 Y'
								WHEN CLEANED_BRANDNAME IN ('RAINFLOWER','POOH') THEN 'CHILDREN BAG'
								WHEN SUBDEPT_NAME = '4 - 8 GIRLS' AND PRODUCT_NAME LIKE '%BELT%60' THEN 'BELT SIZE 60'
								WHEN SUBDEPT_NAME = '4 - 8 GIRLS' AND PRODUCT_NAME LIKE '%4 12L' THEN '4-12 Y'
								WHEN SUBDEPT_NAME = '4 - 8 GIRLS' AND CLEANED_BRANDNAME LIKE 'SFERA' AND PRODUCT_NAME LIKE '% U' THEN 'SFERA GIRL ACCESSORIES'
								WHEN SUBDEPT_NAME = '4 - 8 GIRLS' AND PRODUCT_NAME LIKE '�ا��ͧ%4-7' THEN '4-7 Y'
								WHEN SUBDEPT_NAME = '4 - 8 GIRLS' AND PRODUCT_NAME LIKE '�ا���%5-7' THEN '5-7 Y'
								WHEN SUBDEPT_NAME = '4 - 8 GIRLS' AND PRODUCT_NAME LIKE '�ا���%5-8' THEN '5-8 Y'
								WHEN SUBDEPT_NAME = 'CHILDREN S ACCESSORIES' AND PRODUCT_NAME LIKE '%5 7Y' THEN '5-7 Y'
								WHEN SUBDEPT_NAME = 'CHILDREN S ACCESSORIES' AND PRODUCT_NAME LIKE '%3 7YR%' THEN '3-7 Y'
								WHEN SUBDEPT_NAME = 'CHILDREN S ACCESSORIES' AND PRODUCT_NAME LIKE '%3-7 Y' THEN '3-7 Y'
								WHEN SUBDEPT_NAME = 'CHILDREN S ACCESSORIES' AND PRODUCT_NAME LIKE '%AGES 0-7' THEN '0-7 Y'
								WHEN SUBDEPT_NAME = 'CHILDREN S ACCESSORIES' AND PRODUCT_NAME LIKE '%5 9Y' THEN '5-9 Y'
								WHEN SUBDEPT_NAME = 'CHILDREN S ACCESSORIES' AND (CLEANED_BRANDNAME LIKE 'SIMPLE&CHIC' AND PRODUCT_NAME NOT LIKE '%�ش��������%') THEN 'KID ACCESSORIES'
								WHEN SUBDEPT_NAME = 'CHILDREN S ACCESSORIES' AND PRODUCT_NAME LIKE '%����������ҡ%' THEN '����������ҡ'
								WHEN SUBDEPT_NAME = 'CHILDREN S ACCESSORIES' AND CLEANED_BRANDNAME LIKE 'AMELIE'
																			AND PRODUCT_NAME NOT LIKE '%S'
																			AND PRODUCT_NAME NOT LIKE '%M'
																			AND PRODUCT_NAME NOT LIKE '%L'
																			AND PRODUCT_NAME NOT LIKE '%[0-9]%' THEN '�ش/�ͧ��Һ���ŵ�'
								WHEN SUBDEPT_NAME = 'CHILDREN S ACCESSORIES' AND CLEANED_BRANDNAME IN ('ROSSO','THAI TIME','DOJO','STEPHEN JOSEPH','HUGGER','KIDLAND','BECKMANN NORWAY','MYSELF BELTS','POMO'
																							,'SONS AND DAUGHTERS','BEWELL','PUMPKIN PIE','SPIDERMAN 2 KIDS','BARBIE KIDS','BARBIE','BARBIE KIDS PRO') THEN 'KID ACCESSORIES'
																							
								ELSE JUNIOR_KEYWORD END)
WHERE MOM_AND_KID_PRODUCT_FLAG = 1
AND COALESCE(PREG_FLAG,'N/A') = 'N/A'
AND COALESCE(INFANT_FLAG,'N/A') = 'N/A'
AND COALESCE(TODDLER_FLAG,'N/A') = 'N/A'
AND COALESCE(PRESCHOOL_FLAG,'N/A') = 'N/A'
AND COALESCE(JUNIOR_FLAG,'N/A') = 'N/A'
AND COALESCE(TEEN_FLAG,'N/A') = 'N/A'
AND REVISED_CATEGORY_LEVEL_1 = 'KIDS'
AND REVISED_CATEGORY_LEVEL_2 = 'KID FASHION'
AND REVISED_CATEGORY_LEVEL_3 = 'KID FASHION ACCESSORIES'
AND ((SUBCLASS_NAME = 'JUNIOR' AND PRODUCT_NAME NOT LIKE '% S'
							AND PRODUCT_NAME NOT LIKE '% M'
							AND PRODUCT_NAME NOT LIKE '% L'
							AND PRODUCT_NAME NOT LIKE '% 6'
							AND PRODUCT_NAME NOT LIKE '% 8'
							AND PRODUCT_NAME NOT LIKE '% 10'
							AND PRODUCT_NAME NOT LIKE '% 12')
	OR SUBCLASS_NAME IN ('HANDBAG','LUGGAGE')
	OR (CLEANED_BRANDNAME IN ('PAUL SMITH JUNIOR','KENZO KIDS') AND (PRODUCT_NAME LIKE '%5A 8A'
															OR PRODUCT_NAME LIKE '%5A/8A'
															OR PRODUCT_NAME LIKE '%58A'
															OR PRODUCT_NAME LIKE '%5A8A'))
	OR CLEANED_BRANDNAME IN ('RAINFLOWER','POOH')
	OR (SUBDEPT_NAME = '4 - 8 GIRLS' AND (PRODUCT_NAME LIKE '%BELT%60'
										OR PRODUCT_NAME LIKE '%4 12L'
										OR (CLEANED_BRANDNAME LIKE 'SFERA' AND PRODUCT_NAME LIKE '% U')
										OR PRODUCT_NAME LIKE '�ا���%5-7'
										OR PRODUCT_NAME LIKE '�ا��ͧ%4-7'
										OR PRODUCT_NAME LIKE '�ا���%5-8'))
	OR (SUBDEPT_NAME = 'CHILDREN S ACCESSORIES' AND (PRODUCT_NAME LIKE '%5 7Y'
													OR PRODUCT_NAME LIKE '%3 7YR%'
													OR PRODUCT_NAME LIKE '%3-7 Y'
													OR PRODUCT_NAME LIKE '%AGES 0-7'
													OR PRODUCT_NAME LIKE '%5 9Y'
													OR (CLEANED_BRANDNAME LIKE 'SIMPLE&CHIC' AND PRODUCT_NAME NOT LIKE '%�ش��������%')
													OR PRODUCT_NAME LIKE '%����������ҡ%'
													OR (CLEANED_BRANDNAME LIKE 'AMELIE' AND PRODUCT_NAME NOT LIKE '%S' AND PRODUCT_NAME NOT LIKE '%M' AND PRODUCT_NAME NOT LIKE '%L' AND PRODUCT_NAME NOT LIKE '%[0-9]%')
													OR CLEANED_BRANDNAME IN ('ROSSO','THAI TIME','DOJO','STEPHEN JOSEPH','HUGGER','KIDLAND','BECKMANN NORWAY','MYSELF BELTS','SONS AND DAUGHTERS','BEWELL','PUMPKIN PIE'
																	,'SPIDERMAN 2 KIDS','POMO','BARBIE KIDS','BARBIE','BARBIE KIDS PRO')
													))
	)
;

--PRESCHOOL/JUNIOR/TEEN
UPDATE vpm_data.pm_skutagging_kids_product_master
SET PRESCHOOL_FLAG = 'YES'
	,JUNIOR_FLAG = 'YES'
	,TEEN_FLAG = 'YES'
	,PRESCHOOL_KEYWORD = (CASE WHEN SUBDEPT_NAME LIKE 'BUILD-A-BEAR WORKSHOP' THEN 'BUILD-A-BEAR WORKSHOP'
								WHEN CLEANED_BRANDNAME IN ('CARTOON STUDIOS','DISNEY LIU','GOMU FAMILY','OCEAN TOYS','PRINCESS') THEN 'CHILDREN BAG'
								WHEN SUBDEPT_NAME = '4 - 8 GIRLS' AND PRODUCT_NAME LIKE '%BELT%'
																 AND PRODUCT_NAME NOT LIKE '%BELT%60'
																 AND PRODUCT_NAME NOT LIKE '%BELT%70'
																 AND PRODUCT_NAME NOT LIKE '%BELT%80' THEN 'KID BELT'
								WHEN SUBDEPT_NAME = 'CHILDREN S ACCESSORIES' AND CLEANED_BRANDNAME IN ('SATI','CARSON','TEDDY STAR','BUDDYPHONES','MERY JANE','JULBO','KIDS&TEENS','TOEZONE KIDS'
																								,'ZOGGS','NOBRAND' ,'HERSCHEL','CHERILON','WINGHOUSE ACCESSORIES','MY WINTER','HIP PEAS') THEN 'KID ACCESSORIES'	
								WHEN SUBDEPT_NAME = 'CHILDREN S ACCESSORIES' AND CLEANED_BRANDNAME IN ('JUST GENTLE') THEN 'KID PERSONAL CARE'
								WHEN SUBDEPT_NAME = 'CHILDREN S ACCESSORIES' AND CLEANED_BRANDNAME IN ('LITTAL LADY','PUNARA','TOWNLEY') THEN 'KID COSMETICS'
								WHEN SUBDEPT_NAME = 'CHILDREN S ACCESSORIES' AND CLEANED_BRANDNAME IN ('WINGHOUSE','BABIATORS','SHADEZ') AND PRODUCT_NAME NOT LIKE '%[0-9]%' THEN CONCAT('KID ',SUBCLASS_NAME)
								WHEN SUBDEPT_NAME = 'CHILDREN S ACCESSORIES' AND CLEANED_BRANDNAME LIKE 'PLATYPUS'
																			AND PRODUCT_NAME NOT LIKE '%S'
																			AND PRODUCT_NAME NOT LIKE '%M'
																			AND PRODUCT_NAME NOT LIKE '%L' THEN 'KID HAT'
								ELSE PRESCHOOL_KEYWORD END)
	,JUNIOR_KEYWORD = (CASE WHEN SUBDEPT_NAME LIKE 'BUILD-A-BEAR WORKSHOP' THEN 'BUILD-A-BEAR WORKSHOP'
								WHEN CLEANED_BRANDNAME IN ('CARTOON STUDIOS','DISNEY LIU','GOMU FAMILY','OCEAN TOYS','PRINCESS') THEN 'CHILDREN BAG'
								WHEN SUBDEPT_NAME = '4 - 8 GIRLS' AND PRODUCT_NAME LIKE '%BELT%'
																 AND PRODUCT_NAME NOT LIKE '%BELT%60'
																 AND PRODUCT_NAME NOT LIKE '%BELT%70'
																 AND PRODUCT_NAME NOT LIKE '%BELT%80' THEN 'KID BELT'
								WHEN SUBDEPT_NAME = 'CHILDREN S ACCESSORIES' AND CLEANED_BRANDNAME IN ('SATI','CARSON','TEDDY STAR','BUDDYPHONES','MERY JANE','JULBO','KIDS&TEENS','TOEZONE KIDS'
																								,'ZOGGS','NOBRAND' ,'HERSCHEL','CHERILON','WINGHOUSE ACCESSORIES','MY WINTER','HIP PEAS') THEN 'KID ACCESSORIES'	
								WHEN SUBDEPT_NAME = 'CHILDREN S ACCESSORIES' AND CLEANED_BRANDNAME IN ('JUST GENTLE') THEN 'KID PERSONAL CARE'
								WHEN SUBDEPT_NAME = 'CHILDREN S ACCESSORIES' AND CLEANED_BRANDNAME IN ('LITTAL LADY','PUNARA','TOWNLEY') THEN 'KID COSMETICS'
								WHEN SUBDEPT_NAME = 'CHILDREN S ACCESSORIES' AND CLEANED_BRANDNAME IN ('WINGHOUSE','BABIATORS','SHADEZ') AND PRODUCT_NAME NOT LIKE '%[0-9]%' THEN CONCAT('KID ',SUBCLASS_NAME)
								WHEN SUBDEPT_NAME = 'CHILDREN S ACCESSORIES' AND CLEANED_BRANDNAME LIKE 'PLATYPUS'
																			AND PRODUCT_NAME NOT LIKE '%S'
																			AND PRODUCT_NAME NOT LIKE '%M'
																			AND PRODUCT_NAME NOT LIKE '%L' THEN 'KID HAT'
								ELSE JUNIOR_KEYWORD END)
	,TEEN_KEYWORD = (CASE WHEN SUBDEPT_NAME LIKE 'BUILD-A-BEAR WORKSHOP' THEN 'BUILD-A-BEAR WORKSHOP'
								WHEN CLEANED_BRANDNAME IN ('CARTOON STUDIOS','DISNEY LIU','GOMU FAMILY','OCEAN TOYS','PRINCESS') THEN 'CHILDREN BAG'
								WHEN SUBDEPT_NAME = '4 - 8 GIRLS' AND PRODUCT_NAME LIKE '%BELT%'
																 AND PRODUCT_NAME NOT LIKE '%BELT%60'
																 AND PRODUCT_NAME NOT LIKE '%BELT%70'
																 AND PRODUCT_NAME NOT LIKE '%BELT%80' THEN 'KID BELT'
								WHEN SUBDEPT_NAME = 'CHILDREN S ACCESSORIES' AND CLEANED_BRANDNAME IN ('SATI','CARSON','TEDDY STAR','BUDDYPHONES','MERY JANE','JULBO','KIDS&TEENS','TOEZONE KIDS'
																								,'ZOGGS','NOBRAND' ,'HERSCHEL','CHERILON','WINGHOUSE ACCESSORIES','MY WINTER','HIP PEAS') THEN 'KID ACCESSORIES'	
								WHEN SUBDEPT_NAME = 'CHILDREN S ACCESSORIES' AND CLEANED_BRANDNAME IN ('JUST GENTLE') THEN 'KID PERSONAL CARE'
								WHEN SUBDEPT_NAME = 'CHILDREN S ACCESSORIES' AND CLEANED_BRANDNAME IN ('LITTAL LADY','PUNARA','TOWNLEY') THEN 'KID COSMETICS'
								WHEN SUBDEPT_NAME = 'CHILDREN S ACCESSORIES' AND CLEANED_BRANDNAME IN ('WINGHOUSE','BABIATORS','SHADEZ') AND PRODUCT_NAME NOT LIKE '%[0-9]%' THEN CONCAT('KID ',SUBCLASS_NAME)
								WHEN SUBDEPT_NAME = 'CHILDREN S ACCESSORIES' AND CLEANED_BRANDNAME LIKE 'PLATYPUS'
																			AND PRODUCT_NAME NOT LIKE '%S'
																			AND PRODUCT_NAME NOT LIKE '%M'
																			AND PRODUCT_NAME NOT LIKE '%L' THEN 'KID HAT'
								ELSE TEEN_KEYWORD END)
WHERE MOM_AND_KID_PRODUCT_FLAG = 1
AND COALESCE(PREG_FLAG,'N/A') = 'N/A'
AND COALESCE(INFANT_FLAG,'N/A') = 'N/A'
AND COALESCE(TODDLER_FLAG,'N/A') = 'N/A'
AND COALESCE(PRESCHOOL_FLAG,'N/A') = 'N/A'
AND COALESCE(JUNIOR_FLAG,'N/A') = 'N/A'
AND COALESCE(TEEN_FLAG,'N/A') = 'N/A'
AND REVISED_CATEGORY_LEVEL_1 = 'KIDS'
AND REVISED_CATEGORY_LEVEL_2 = 'KID FASHION'
AND REVISED_CATEGORY_LEVEL_3 = 'KID FASHION ACCESSORIES'
AND (SUBDEPT_NAME LIKE 'BUILD-A-BEAR WORKSHOP'
	OR CLEANED_BRANDNAME IN ('CARTOON STUDIOS','DISNEY LIU','GOMU FAMILY','OCEAN TOYS','PRINCESS')
	OR (SUBDEPT_NAME = '4 - 8 GIRLS' AND (PRODUCT_NAME LIKE '%BELT%'
										AND PRODUCT_NAME NOT LIKE '%BELT%60'
										AND PRODUCT_NAME NOT LIKE '%BELT%70'
										AND PRODUCT_NAME NOT LIKE '%BELT%80'))
	OR (SUBDEPT_NAME = 'CHILDREN S ACCESSORIES' AND (CLEANED_BRANDNAME IN ('SATI','CARSON','TEDDY STAR','JUST GENTLE','LITTAL LADY','PUNARA','TOWNLEY','BUDDYPHONES','MERY JANE','JULBO','KIDS&TEENS','ZOGGS','NOBRAND'
																  ,'HERSCHEL','CHERILON','WINGHOUSE ACCESSORIES','TOEZONE KIDS','MY WINTER','HIP PEAS')
													OR (CLEANED_BRANDNAME IN ('WINGHOUSE','BABIATORS','SHADEZ') AND PRODUCT_NAME NOT LIKE '%[0-9]%')
													OR (CLEANED_BRANDNAME LIKE 'PLATYPUS' AND PRODUCT_NAME NOT LIKE '%S' AND PRODUCT_NAME NOT LIKE '%M' AND PRODUCT_NAME NOT LIKE '%L')
													))
	)
;

--JUNIOR
UPDATE vpm_data.pm_skutagging_kids_product_master
SET JUNIOR_FLAG = 'YES'
	,JUNIOR_KEYWORD = (CASE WHEN DEPT_NAME = 'BABYSHOP' THEN '7-8 Y'
							WHEN CLEANED_BRANDNAME LIKE 'KIDS & TEENS%' AND PRODUCT_NAME LIKE '%������%' THEN 'BARBIE BAG'
							WHEN CLEANED_BRANDNAME LIKE 'KIDS & TEENS%' AND PRODUCT_NAME LIKE '%BARBIE%' THEN 'BARBIE BAG'
							WHEN CLEANED_BRANDNAME LIKE 'KIDS & TEENS%' AND PRODUCT_NAME LIKE '%FROZEN%' THEN 'FROZEN BAG'
							WHEN CLEANED_BRANDNAME LIKE 'KIDS & TEENS%' AND PRODUCT_NAME LIKE '%FZ%' THEN 'FROZEN BAG'
							WHEN CLEANED_BRANDNAME LIKE 'KIDS & TEENS%' AND PRODUCT_NAME LIKE '%�����%'  THEN 'FROZEN BAG'
							WHEN CLEANED_BRANDNAME LIKE 'KIDS & TEENS%' AND PRODUCT_NAME LIKE '%����%' THEN 'FROZEN BAG'
							WHEN CLEANED_BRANDNAME LIKE 'KIDS & TEENS%' AND PRODUCT_NAME LIKE '%MY LITTLE%' THEN 'MY LITTLE PONY BAG'
							WHEN CLEANED_BRANDNAME LIKE 'KIDS & TEENS%' AND PRODUCT_NAME LIKE '%PONY%' THEN 'MY LITTLE PONY BAG'
							WHEN CLEANED_BRANDNAME LIKE 'KIDS & TEENS%' AND PRODUCT_NAME LIKE '%⾹��%' THEN 'MY LITTLE PONY BAG'
							WHEN CLEANED_BRANDNAME LIKE 'KIDS & TEENS%' AND PRODUCT_NAME LIKE '%����%' THEN 'BEN 10 BAG'
							WHEN CLEANED_BRANDNAME LIKE 'KIDS & TEENS%' AND PRODUCT_NAME LIKE '%BEN 10%' THEN 'BEN 10 BAG'
							WHEN CLEANED_BRANDNAME LIKE 'KIDS & TEENS%' AND PRODUCT_NAME LIKE '%������%' THEN 'PRINCESS BAG'
							WHEN CLEANED_BRANDNAME LIKE 'KIDS & TEENS%' AND PRODUCT_NAME LIKE '%��ҹ���������%' THEN 'TRANSFORMER BAG'
							WHEN CLEANED_BRANDNAME LIKE 'KIDS & TEENS%' AND PRODUCT_NAME LIKE '%TRANSFORMER' THEN 'TRANSFORMER BAG'
							WHEN CLEANED_BRANDNAME LIKE 'KIDS & TEENS%' AND PRODUCT_NAME LIKE '%TFM%' THEN 'TRANSFORMER BAG'
							WHEN CLEANED_BRANDNAME LIKE 'KIDS & TEENS%' AND PRODUCT_NAME LIKE '%POKEMON%' THEN 'POKEMON BAG'
							WHEN CLEANED_BRANDNAME LIKE 'KIDS & TEENS%' AND PRODUCT_NAME LIKE '%SPIDER MAN%' THEN 'SPIDER MAN BAG'
							WHEN CLEANED_BRANDNAME LIKE 'KIDS & TEENS%' AND PRODUCT_NAME LIKE '%SPM%' THEN 'SPIDER MAN BAG'
							WHEN CLEANED_BRANDNAME LIKE 'KIDS & TEENS%' AND PRODUCT_NAME LIKE '%���������%' THEN 'SPIDER MAN BAG'
							WHEN CLEANED_BRANDNAME LIKE 'KIDS & TEENS%' AND PRODUCT_NAME LIKE '%��������%' THEN 'LOL BAG'
							WHEN CLEANED_BRANDNAME LIKE 'KIDS & TEENS%' AND PRODUCT_NAME LIKE '%TROLLS%' THEN 'TROLLS BAG'
							WHEN CLEANED_BRANDNAME LIKE 'SPIDER MAN%' THEN 'SPIDER MAN BAG'
							WHEN SUBCLASS_NAME = 'JUNIOR' AND PRODUCT_NAME LIKE '% M' THEN 'GUCCI JUNIOR SIZE M'
							WHEN SUBCLASS_NAME = 'JUNIOR' AND PRODUCT_NAME LIKE '% L' THEN 'GUCCI JUNIOR SIZE L'
							WHEN SUBCLASS_NAME = 'JUNIOR' AND PRODUCT_NAME LIKE '% 8' THEN '8Y'
							WHEN SUBCLASS_NAME = 'JUNIOR' AND PRODUCT_NAME LIKE '% 10' THEN '10Y'
							WHEN SUBCLASS_NAME = 'JUNIOR' AND PRODUCT_NAME LIKE '% 12' THEN '12Y'
							WHEN SUBDEPT_NAME = 'CHILDREN LUXE FASHION' AND CLEANED_BRANDNAME = 'EMPORIO ARMANI'
																		AND PRODUCT_NAME NOT LIKE '%INFANT%CARE%SET%'
																		AND PRODUCT_NAME NOT LIKE '%CHANGING%BAG%'
																		AND PRODUCT_NAME NOT LIKE '% TU'
																		AND PRODUCT_NAME NOT LIKE '% 6A'
																		AND PRODUCT_NAME NOT LIKE '% 16A' THEN 'EMPORIO ARMANI JUNIOR'
							WHEN CLEANED_BRANDNAME IN ('PAUL SMITH JUNIOR','KENZO KIDS') AND PRODUCT_NAME LIKE '%812A' THEN '8-12 Y'
							WHEN CLEANED_BRANDNAME IN ('PAUL SMITH JUNIOR','KENZO KIDS') AND PRODUCT_NAME LIKE '%8A12A' THEN '8-12 Y'
							WHEN CLEANED_BRANDNAME IN ('PAUL SMITH JUNIOR','KENZO KIDS') AND PRODUCT_NAME LIKE '% 8A' AND PRODUCT_NAME NOT LIKE '%5A 8A' THEN '8Y'
							WHEN CLEANED_BRANDNAME IN ('PAUL SMITH JUNIOR','KENZO KIDS') AND PRODUCT_NAME LIKE '%1012' THEN '10-12 Y'
							WHEN CLEANED_BRANDNAME IN ('PAUL SMITH JUNIOR','KENZO KIDS') AND PRODUCT_NAME LIKE '%10A' THEN '10Y'
							WHEN CLEANED_BRANDNAME IN ('PAUL SMITH JUNIOR','KENZO KIDS') AND PRODUCT_NAME LIKE '% 10' THEN '10Y'
							WHEN CLEANED_BRANDNAME IN ('PAUL SMITH JUNIOR','KENZO KIDS') AND PRODUCT_NAME LIKE '%12A' THEN '12Y'
							WHEN CLEANED_BRANDNAME IN ('PAUL SMITH JUNIOR','KENZO KIDS') AND PRODUCT_NAME LIKE 'SOCK%3134' THEN 'EU SIZE 31-34'
							WHEN CLEANED_BRANDNAME IN ('PAUL SMITH JUNIOR','KENZO KIDS') AND PRODUCT_NAME LIKE 'SOCK%35' THEN 'EU SIZE 35'
							WHEN CLEANED_BRANDNAME IN ('PAUL SMITH JUNIOR','KENZO KIDS') AND PRODUCT_NAME LIKE 'SOCK%35 38' THEN 'EU SIZE 38'
							WHEN SUBCLASS_NAME = 'SKIRTS' AND PRODUCT_NAME LIKE '% 8' THEN '8Y'
							WHEN SUBCLASS_NAME = 'SKIRTS' AND  PRODUCT_NAME LIKE '% 10' THEN '10Y'
							WHEN SUBDEPT_NAME = '4 - 8 GIRLS' AND PRODUCT_NAME LIKE '%BOOT%32' THEN 'BOOTS EU SIZE 32'
							WHEN SUBDEPT_NAME = '4 - 8 GIRLS' AND PRODUCT_NAME LIKE '%S HOE%32' THEN 'SHOES EU SIZE 32'
							WHEN SUBDEPT_NAME = '4 - 8 GIRLS' AND PRODUCT_NAME LIKE '%SANDAL%32' THEN 'SANDALS EU SIZE 32'
							WHEN SUBDEPT_NAME = '4 - 8 GIRLS' AND PRODUCT_NAME LIKE '%SHOE%32' THEN 'SHOES EU SIZE 32'
							WHEN SUBDEPT_NAME = '4 - 8 GIRLS' AND PRODUCT_NAME LIKE '%SNEAKER%32' THEN 'SNEAKERS EU SIZE 32'
							WHEN SUBDEPT_NAME = '4 - 8 GIRLS' AND PRODUCT_NAME LIKE '%�ͧ���%32' THEN 'SHOES EU SIZE 32'
							WHEN SUBDEPT_NAME = '4 - 8 GIRLS' AND PRODUCT_NAME LIKE '%BOOT%33' THEN 'BOOTS EU SIZE 33'
							WHEN SUBDEPT_NAME = '4 - 8 GIRLS' AND PRODUCT_NAME LIKE '%S HOE%33' THEN 'SHOES EU SIZE 33'
							WHEN SUBDEPT_NAME = '4 - 8 GIRLS' AND PRODUCT_NAME LIKE '%SHOE%33' THEN 'SHOES EU SIZE 33'
							WHEN SUBDEPT_NAME = '4 - 8 GIRLS' AND PRODUCT_NAME LIKE '%�ͧ���%33' THEN 'SHOES EU SIZE 33'
							WHEN SUBDEPT_NAME = '4 - 8 GIRLS' AND PRODUCT_NAME LIKE '%SANDAL%33' THEN 'SANDALS EU SIZE 33'
							WHEN SUBDEPT_NAME = '4 - 8 GIRLS' AND PRODUCT_NAME LIKE '%SNEAKER%33' THEN 'SNEAKERS EU SIZE 33'
							WHEN SUBDEPT_NAME = '4 - 8 GIRLS' AND PRODUCT_NAME LIKE '%BOOT%34' THEN 'BOOTS EU SIZE 34'
							WHEN SUBDEPT_NAME = '4 - 8 GIRLS' AND PRODUCT_NAME LIKE '%S HOE%34' THEN 'SHOES EU SIZE 34'
							WHEN SUBDEPT_NAME = '4 - 8 GIRLS' AND PRODUCT_NAME LIKE '%SHOE%34' THEN 'SHOES EU SIZE 34'
							WHEN SUBDEPT_NAME = '4 - 8 GIRLS' AND PRODUCT_NAME LIKE '%�ͧ���%34' THEN 'SHOES EU SIZE 34'
							WHEN SUBDEPT_NAME = '4 - 8 GIRLS' AND PRODUCT_NAME LIKE '%SANDAL%34' THEN 'SANDALS EU SIZE 34'
							WHEN SUBDEPT_NAME = '4 - 8 GIRLS' AND PRODUCT_NAME LIKE '%SNEAKER%34' THEN 'SNEAKERS EU SIZE 34'
							WHEN SUBDEPT_NAME = '4 - 8 GIRLS' AND PRODUCT_NAME LIKE '%BOOT%35' THEN 'BOOTS EU SIZE 35'
							WHEN SUBDEPT_NAME = '4 - 8 GIRLS' AND PRODUCT_NAME LIKE '%S HOE%35' THEN 'SHOES EU SIZE 35'
							WHEN SUBDEPT_NAME = '4 - 8 GIRLS' AND PRODUCT_NAME LIKE '%SHOE%35' THEN 'SHOES EU SIZE 35'
							WHEN SUBDEPT_NAME = '4 - 8 GIRLS' AND PRODUCT_NAME LIKE '%�ͧ���%35' THEN 'SHOES EU SIZE 35'
							WHEN SUBDEPT_NAME = '4 - 8 GIRLS' AND PRODUCT_NAME LIKE '%SANDAL%35' THEN 'SANDALS EU SIZE 35'
							WHEN SUBDEPT_NAME = '4 - 8 GIRLS' AND PRODUCT_NAME LIKE '%SNEAKER%35' THEN 'SNEAKERS EU SIZE 35'
							WHEN SUBDEPT_NAME = '4 - 8 GIRLS' AND PRODUCT_NAME LIKE '%BOOT%36' THEN 'BOOTS EU SIZE 36'
							WHEN SUBDEPT_NAME = '4 - 8 GIRLS' AND PRODUCT_NAME LIKE '%S HOE%36' THEN 'SHOES EU SIZE 36'
							WHEN SUBDEPT_NAME = '4 - 8 GIRLS' AND PRODUCT_NAME LIKE '%SHOE%36' THEN 'SHOES EU SIZE 36'
							WHEN SUBDEPT_NAME = '4 - 8 GIRLS' AND PRODUCT_NAME LIKE '%�ͧ���%36' THEN 'SHOES EU SIZE 36'
							WHEN SUBDEPT_NAME = '4 - 8 GIRLS' AND PRODUCT_NAME LIKE '%SANDAL%36' THEN 'SANDALS EU SIZE 36'
							WHEN SUBDEPT_NAME = '4 - 8 GIRLS' AND PRODUCT_NAME LIKE '%SNEAKER%36' THEN 'SNEAKERS EU SIZE 36'
							WHEN SUBDEPT_NAME = '4 - 8 GIRLS' AND PRODUCT_NAME LIKE '%BOOT%37' THEN 'BOOTS EU SIZE 37'
							WHEN SUBDEPT_NAME = '4 - 8 GIRLS' AND PRODUCT_NAME LIKE '%S HOE%37' THEN 'SHOES EU SIZE 37'
							WHEN SUBDEPT_NAME = '4 - 8 GIRLS' AND PRODUCT_NAME LIKE '%SHOE%37' THEN 'SHOES EU SIZE 37'
							WHEN SUBDEPT_NAME = '4 - 8 GIRLS' AND PRODUCT_NAME LIKE '%�ͧ���%37' THEN 'SHOES EU SIZE 37'
							WHEN SUBDEPT_NAME = '4 - 8 GIRLS' AND PRODUCT_NAME LIKE '%SANDAL%37' THEN 'SANDALS EU SIZE 37'
							WHEN SUBDEPT_NAME = '4 - 8 GIRLS' AND PRODUCT_NAME LIKE '%SNEAKER%37' THEN 'SNEAKERS EU SIZE 37'
							WHEN SUBDEPT_NAME = '4 - 8 GIRLS' AND PRODUCT_NAME LIKE '%��ǡ%54' THEN 'CAP/HAT 54 CM'
							WHEN SUBDEPT_NAME = '4 - 8 GIRLS' AND PRODUCT_NAME LIKE '%��ǡ%56' THEN 'CAP/HAT 56 CM'
							WHEN SUBDEPT_NAME = '4 - 8 GIRLS' AND PRODUCT_NAME LIKE '%T SHIRT%8' THEN '8Y'
							WHEN SUBDEPT_NAME = '4 - 8 GIRLS' AND PRODUCT_NAME LIKE '%T SHIRT%10' THEN '10Y'
							WHEN SUBDEPT_NAME = '4 - 8 GIRLS' AND PRODUCT_NAME LIKE '%T SHIRT%12' THEN '12Y'
							WHEN SUBDEPT_NAME = '4 - 8 GIRLS' AND PRODUCT_NAME LIKE '%CAP% 6' THEN 'CAP/HAT SIZE 6 (6-10Y)'
							WHEN SUBDEPT_NAME = '4 - 8 GIRLS' AND PRODUCT_NAME LIKE 'CAP%M' THEN 'CAP SIZE M'
							WHEN SUBDEPT_NAME = '4 - 8 GIRLS' AND PRODUCT_NAME LIKE '%HAT% 6' THEN 'CAP/HAT SIZE 6 (6-10Y)'
							WHEN SUBDEPT_NAME = '4 - 8 GIRLS' AND PRODUCT_NAME LIKE '% 8' THEN '8Y'
							WHEN SUBDEPT_NAME = '4 - 8 GIRLS' AND PRODUCT_NAME LIKE '%10' THEN '10Y'
							WHEN SUBDEPT_NAME = '4 - 8 GIRLS' AND PRODUCT_NAME LIKE '%12' THEN '12Y'
							WHEN SUBDEPT_NAME = '4 - 8 GIRLS' AND PRODUCT_NAME LIKE '%12L' AND PRODUCT_NAME NOT LIKE '%4 12L' THEN '12Y'
							WHEN SUBDEPT_NAME = '4 - 8 GIRLS' AND PRODUCT_NAME LIKE 'SET%7' THEN '7Y'
							WHEN SUBDEPT_NAME = '4 - 8 GIRLS' AND PRODUCT_NAME LIKE 'SET%9' THEN '9Y'
							WHEN SUBDEPT_NAME = '4 - 8 GIRLS' AND PRODUCT_NAME LIKE 'SET%11' THEN '11Y'
							WHEN SUBDEPT_NAME = '4 - 8 GIRLS' AND PRODUCT_NAME LIKE '�ҧࡧ%�%70' THEN '�ҧࡧ� 70'
							WHEN SUBDEPT_NAME = '4 - 8 GIRLS' AND PRODUCT_NAME LIKE '�ҧࡧ%�%75' THEN '�ҧࡧ� 75'
							WHEN SUBDEPT_NAME = '4 - 8 GIRLS' AND PRODUCT_NAME LIKE '�ҧࡧ%�%80' THEN '�ҧࡧ� 80'
							WHEN SUBDEPT_NAME = '4 - 8 GIRLS' AND PRODUCT_NAME LIKE '����͡����%70' THEN '����͡���� 70'
							WHEN SUBDEPT_NAME = '4 - 8 GIRLS' AND PRODUCT_NAME LIKE '����͡����%75' THEN '����͡���� 75'
							WHEN SUBDEPT_NAME = '4 - 8 GIRLS' AND PRODUCT_NAME LIKE '����͡����%80' THEN '����͡���� 80'
							WHEN SUBDEPT_NAME = '4 - 8 GIRLS' AND PRODUCT_NAME LIKE '�ا���%7-9' THEN '7-9 Y'
							WHEN SUBDEPT_NAME = 'CHILDREN S ACCESSORIES' AND PRODUCT_NAME LIKE '�ا���%10 15%' THEN '10-15 Y'
							WHEN SUBDEPT_NAME = 'CHILDREN S ACCESSORIES' AND PRODUCT_NAME LIKE '% 8 12' THEN '8-12 Y'
							WHEN SUBDEPT_NAME = 'CHILDREN S ACCESSORIES' AND PRODUCT_NAME LIKE '% 8 12Y' THEN '8-12 Y'
							WHEN SUBDEPT_NAME = 'CHILDREN S ACCESSORIES' AND PRODUCT_NAME LIKE '%��ǡ%54' THEN 'CAP/HAT 54 CM'
							WHEN SUBDEPT_NAME = 'CHILDREN S ACCESSORIES' AND PRODUCT_NAME LIKE '%��ǡ%56' THEN 'CAP/HAT 56 CM'
							WHEN SUBDEPT_NAME = 'CHILDREN S ACCESSORIES' AND PRODUCT_NAME LIKE '�ش��������%130' THEN '�ش����ŵ� 130 CM'
							WHEN SUBDEPT_NAME = 'CHILDREN S ACCESSORIES' AND PRODUCT_NAME LIKE '�ش��������%140' THEN '�ش����ŵ� 140 CM'
							WHEN SUBDEPT_NAME = 'CHILDREN S ACCESSORIES' AND PRODUCT_NAME LIKE '�ش��������%150' THEN '�ش����ŵ� 150 CM'
							WHEN SUBDEPT_NAME = 'CHILDREN S ACCESSORIES' AND PRODUCT_NAME LIKE '%������%M%' THEN '�ش����ŵ�'
							WHEN SUBDEPT_NAME = 'CHILDREN S ACCESSORIES' AND PRODUCT_NAME LIKE '%������% L' THEN '�ش����ŵ� SIZE L'
							WHEN SUBDEPT_NAME = 'CHILDREN S ACCESSORIES' AND PRODUCT_NAME LIKE '%������%XL' THEN '�ش����ŵ� SIZE XL'
							WHEN SUBDEPT_NAME = 'CHILDREN S ACCESSORIES' AND PRODUCT_NAME LIKE '%�ͧ��Һ�����%210%' THEN '�ͧ��Һ���ŵ� 21 CM'
							WHEN SUBDEPT_NAME = 'CHILDREN S ACCESSORIES' AND PRODUCT_NAME LIKE '%�ͧ��Һ�����%220%' THEN '�ͧ��Һ���ŵ� 22 CM'
							WHEN SUBDEPT_NAME = 'CHILDREN S ACCESSORIES' AND PRODUCT_NAME LIKE '%�ͧ��Һ�����%230%' THEN '�ͧ��Һ���ŵ� 23 CM'
							WHEN SUBDEPT_NAME = 'CHILDREN S ACCESSORIES' AND PRODUCT_NAME LIKE '%�ͧ��Һ�����%240%' THEN '�ͧ��Һ���ŵ� 24 CM'
							WHEN SUBDEPT_NAME = 'CHILDREN S ACCESSORIES' AND CLEANED_BRANDNAME LIKE 'PLATYPUS' AND PRODUCT_NAME LIKE '%M' THEN 'PLATYPUS HAT SIZE M'
							WHEN SUBDEPT_NAME = 'CHILDREN S ACCESSORIES' AND CLEANED_BRANDNAME IN ('SHU SHU','PICCOLO HARDLINE') THEN 'KID COSMETICS'
							WHEN SUBDEPT_NAME = '4 - 8 GIRLS' AND CLEANED_BRANDNAME LIKE 'SHU SHU' THEN 'KID COSMETICS'
							ELSE JUNIOR_KEYWORD END)
WHERE MOM_AND_KID_PRODUCT_FLAG = 1
AND COALESCE(PREG_FLAG,'N/A') = 'N/A'
AND COALESCE(INFANT_FLAG,'N/A') = 'N/A'
AND COALESCE(TODDLER_FLAG,'N/A') = 'N/A'
AND COALESCE(PRESCHOOL_FLAG,'N/A') = 'N/A'
AND COALESCE(JUNIOR_FLAG,'N/A') = 'N/A'
AND COALESCE(TEEN_FLAG,'N/A') = 'N/A'
AND REVISED_CATEGORY_LEVEL_1 = 'KIDS'
AND REVISED_CATEGORY_LEVEL_2 = 'KID FASHION'
AND REVISED_CATEGORY_LEVEL_3 = 'KID FASHION ACCESSORIES'
AND (DEPT_NAME = 'BABYSHOP'
	OR (CLEANED_BRANDNAME LIKE 'KIDS & TEENS%' AND (PRODUCT_NAME LIKE '%������%'
										OR PRODUCT_NAME LIKE '%BARBIE%'
										OR PRODUCT_NAME LIKE '%FROZEN%'
										OR PRODUCT_NAME LIKE '%FZ%'
										OR PRODUCT_NAME LIKE '%�����%'
										OR PRODUCT_NAME LIKE '%����%'
										OR PRODUCT_NAME LIKE '%MY LITTLE%'
										OR PRODUCT_NAME LIKE '%PONY%'
										OR PRODUCT_NAME LIKE '%⾹��%'
										OR PRODUCT_NAME LIKE '%����%'
										OR PRODUCT_NAME LIKE '%BEN 10%'
										OR PRODUCT_NAME LIKE '%������%'
										OR PRODUCT_NAME LIKE '%��ҹ���������%'
										OR PRODUCT_NAME LIKE '%TRANSFORMER'
										OR PRODUCT_NAME LIKE '%TFM%'
										OR PRODUCT_NAME LIKE '%POKEMON%'
										OR PRODUCT_NAME LIKE '%SPIDER MAN%'
										OR PRODUCT_NAME LIKE '%SPM%'
										OR PRODUCT_NAME LIKE '%���������%'
										OR PRODUCT_NAME LIKE '%��������%'
										OR PRODUCT_NAME LIKE '%TROLLS%'))
	OR CLEANED_BRANDNAME LIKE 'SPIDER MAN%'
	OR (SUBCLASS_NAME = 'JUNIOR' AND (PRODUCT_NAME LIKE '% M'
									OR PRODUCT_NAME LIKE '% L'
									OR PRODUCT_NAME LIKE '% 8'
									OR PRODUCT_NAME LIKE '% 10'
									OR PRODUCT_NAME LIKE '% 12'))
	OR (SUBDEPT_NAME = 'CHILDREN LUXE FASHION' AND CLEANED_BRANDNAME = 'EMPORIO ARMANI'
											  AND PRODUCT_NAME NOT LIKE '%INFANT%CARE%SET%'
											  AND PRODUCT_NAME NOT LIKE '%CHANGING%BAG%'
											  AND PRODUCT_NAME NOT LIKE '% TU'
											  AND PRODUCT_NAME NOT LIKE '% 6A')
	OR (CLEANED_BRANDNAME IN ('PAUL SMITH JUNIOR','KENZO KIDS') AND (PRODUCT_NAME LIKE '%812A'
															OR PRODUCT_NAME LIKE '%8A12A'
															OR (PRODUCT_NAME LIKE '% 8A' AND PRODUCT_NAME NOT LIKE '%5A 8A')
															OR PRODUCT_NAME LIKE '%1012'
															OR PRODUCT_NAME LIKE '%10A'
															OR PRODUCT_NAME LIKE '% 10'
															OR PRODUCT_NAME LIKE '%12A'
															OR PRODUCT_NAME LIKE 'SOCK%3134'
															OR PRODUCT_NAME LIKE 'SOCK%35'
															OR PRODUCT_NAME LIKE 'SOCK%35 38'))
	OR (SUBCLASS_NAME = 'SKIRTS' AND (PRODUCT_NAME LIKE '% 8'
									OR PRODUCT_NAME LIKE '% 10'))
	OR (SUBDEPT_NAME = '4 - 8 GIRLS' AND (PRODUCT_NAME LIKE '%BOOT%32'
										OR PRODUCT_NAME LIKE '%S HOE%32'
										OR PRODUCT_NAME LIKE '%SANDAL%32'
										OR PRODUCT_NAME LIKE '%SHOE%32'
										OR PRODUCT_NAME LIKE '%SNEAKER%32'
										OR PRODUCT_NAME LIKE '%�ͧ���%32'
										OR PRODUCT_NAME LIKE '%BOOT%33'
										OR PRODUCT_NAME LIKE '%S HOE%33'
										OR PRODUCT_NAME LIKE '%SANDAL%33'
										OR PRODUCT_NAME LIKE '%SHOE%33'
										OR PRODUCT_NAME LIKE '%SNEAKER%33'
										OR PRODUCT_NAME LIKE '%�ͧ���%33'
										OR PRODUCT_NAME LIKE '%BOOT%34'
										OR PRODUCT_NAME LIKE '%S HOE%34'
										OR PRODUCT_NAME LIKE '%SANDAL%34'
										OR PRODUCT_NAME LIKE '%SHOE%34'
										OR PRODUCT_NAME LIKE '%SNEAKER%34'
										OR PRODUCT_NAME LIKE '%�ͧ���%34'
										OR PRODUCT_NAME LIKE '%BOOT%35'
										OR PRODUCT_NAME LIKE '%S HOE%35'
										OR PRODUCT_NAME LIKE '%SANDAL%35'
										OR PRODUCT_NAME LIKE '%SHOE%35'
										OR PRODUCT_NAME LIKE '%SNEAKER%35'
										OR PRODUCT_NAME LIKE '%�ͧ���%35'
										OR PRODUCT_NAME LIKE '%BOOT%36'
										OR PRODUCT_NAME LIKE '%S HOE%36'
										OR PRODUCT_NAME LIKE '%SANDAL%36'
										OR PRODUCT_NAME LIKE '%SHOE%36'
										OR PRODUCT_NAME LIKE '%SNEAKER%36'
										OR PRODUCT_NAME LIKE '%�ͧ���%36'
										OR PRODUCT_NAME LIKE '%BOOT%37'
										OR PRODUCT_NAME LIKE '%S HOE%37'
										OR PRODUCT_NAME LIKE '%SANDAL%37'
										OR PRODUCT_NAME LIKE '%SHOE%37'
										OR PRODUCT_NAME LIKE '%SNEAKER%37'
										OR PRODUCT_NAME LIKE '%�ͧ���%37'
										OR PRODUCT_NAME LIKE '%��ǡ%54'
										OR PRODUCT_NAME LIKE '%��ǡ%56'
										OR PRODUCT_NAME LIKE '%T SHIRT%8'
										OR PRODUCT_NAME LIKE '%T SHIRT%10'
										OR PRODUCT_NAME LIKE '%T SHIRT%12'
										OR PRODUCT_NAME LIKE '%CAP% 6'
										OR PRODUCT_NAME LIKE 'CAP%M'
										OR PRODUCT_NAME LIKE '%HAT% 6'
										OR PRODUCT_NAME LIKE '% 8'
										OR PRODUCT_NAME LIKE '%10'
										OR PRODUCT_NAME LIKE '%12'
										OR (PRODUCT_NAME LIKE '%12L' AND PRODUCT_NAME NOT LIKE '%4 12L')
										OR PRODUCT_NAME LIKE 'SET%7'
										OR PRODUCT_NAME LIKE 'SET%9'
										OR PRODUCT_NAME LIKE 'SET%11'
										OR PRODUCT_NAME LIKE '�ҧࡧ%�%70'
										OR PRODUCT_NAME LIKE '�ҧࡧ%�%75'
										OR PRODUCT_NAME LIKE '�ҧࡧ%�%80'
										OR PRODUCT_NAME LIKE '����͡����%70'
										OR PRODUCT_NAME LIKE '����͡����%75'
										OR PRODUCT_NAME LIKE '����͡����%80'
										OR PRODUCT_NAME LIKE '�ا���%7-9')
										OR CLEANED_BRANDNAME LIKE 'SHU SHU')
	OR (SUBDEPT_NAME = 'CHILDREN S ACCESSORIES' AND (PRODUCT_NAME LIKE '�ا���%10 15%'
													OR PRODUCT_NAME LIKE '% 8 12'
													OR PRODUCT_NAME LIKE '% 8 12Y'
													OR PRODUCT_NAME LIKE '%��ǡ%54'
													OR PRODUCT_NAME LIKE '%��ǡ%56'
													OR PRODUCT_NAME LIKE '�ش��������%130'
													OR PRODUCT_NAME LIKE '�ش��������%140'
													OR PRODUCT_NAME LIKE '�ش��������%150'
													OR PRODUCT_NAME LIKE '%������%M%'
													OR PRODUCT_NAME LIKE '%������% L'
													OR PRODUCT_NAME LIKE '%������%XL'
													OR PRODUCT_NAME LIKE '%�ͧ��Һ�����%210%'
													OR PRODUCT_NAME LIKE '%�ͧ��Һ�����%220%'
													OR PRODUCT_NAME LIKE '%�ͧ��Һ�����%230%'
													OR PRODUCT_NAME LIKE '%�ͧ��Һ�����%240%'
													OR (CLEANED_BRANDNAME LIKE 'PLATYPUS' AND PRODUCT_NAME LIKE '%M')
													OR CLEANED_BRANDNAME IN ('SHU SHU','PICCOLO HARDLINE')))
	)
;

--JUNIOR/TEEN
UPDATE vpm_data.pm_skutagging_kids_product_master
SET JUNIOR_FLAG = 'YES'
	,TEEN_FLAG = 'YES'
	,JUNIOR_KEYWORD = (CASE WHEN CLEANED_BRANDNAME LIKE 'KIDS & TEENS%' AND PRODUCT_NAME LIKE '%MOOMIN%' THEN 'MOOMIN'
							WHEN CLEANED_BRANDNAME LIKE 'KIDS & TEENS%' AND PRODUCT_NAME LIKE '%���Թ%' THEN 'MOOMIN'
							WHEN CLEANED_BRANDNAME LIKE 'KIDS & TEENS%' AND PRODUCT_NAME LIKE '%PAUL FRANK%' THEN 'PAUL FRANK'
							WHEN CLEANED_BRANDNAME LIKE 'KIDS & TEENS%' AND PRODUCT_NAME LIKE '%PUAL FRANK%' THEN 'PAUL FRANK'
							WHEN CLEANED_BRANDNAME LIKE 'KIDS & TEENS%' AND PRODUCT_NAME LIKE '%������%' THEN 'TSUM TSUM'
							WHEN CLEANED_BRANDNAME LIKE 'KIDS & TEENS%' AND PRODUCT_NAME LIKE '%TSUM%TSUM%' THEN 'TSUM TSUM'
							WHEN CLEANED_BRANDNAME LIKE 'KIDS & TEENS%' AND PRODUCT_NAME LIKE '%FAIRY TAIL%' THEN 'FAIRY TAIL'
							WHEN CLEANED_BRANDNAME LIKE 'KIDS & TEENS%' AND PRODUCT_NAME LIKE '%WE BARS BEAR%' THEN 'WE BARE BEARS'
							WHEN CLEANED_BRANDNAME LIKE 'KIDS & TEENS%' AND PRODUCT_NAME LIKE '%��ǵ������%' THEN 'CUTIE MARK'
							WHEN CLEANED_BRANDNAME LIKE 'KIDS & TEENS%' AND PRODUCT_NAME LIKE '%MINION%' THEN 'MINION'
							WHEN CLEANED_BRANDNAME LIKE 'KIDS & TEENS%' AND PRODUCT_NAME LIKE '%YOKAI WATCH%' THEN 'YOKAI WATCH'
							WHEN CLEANED_BRANDNAME LIKE 'KIDS & TEENS%' AND PRODUCT_NAME LIKE '%FURBY%' THEN 'FURBY BAG'
							WHEN CLEANED_BRANDNAME LIKE 'KIDS & TEENS%' AND PRODUCT_NAME LIKE '%DISNEY%' THEN 'DISNEY'
							WHEN CLEANED_BRANDNAME LIKE 'KIDS & TEENS%' AND PRODUCT_NAME LIKE '%���ի�%' THEN 'PVC WALLET'
							WHEN CLEANED_BRANDNAME LIKE 'KIDS & TEENS%' AND PRODUCT_NAME LIKE '%�����ҾѺ��%' THEN 'WALLET'
							WHEN CLEANED_BRANDNAME LIKE 'KIDS & TEENS%' AND PRODUCT_NAME LIKE '%������KIDS&TEENS%' THEN 'KID AND TEEN BAG'
							WHEN CLEANED_BRANDNAME LIKE 'PAUL FRANK%' THEN 'PAUL FRANK'
							WHEN CLEANED_BRANDNAME IN ('RIDAZ','RILAKKUMA','SANRIO IMPORT','SANRIO LOCAL','SANRIO PRO','TSUM TSUM','WEBARE BEARS'
												,'WEBARE BEARS PRO1','KUMAMON','KIDS & TEENS PRO1','KAKAO FRIENDS','LIU DE SAC') THEN 'KID AND TEEN BAG'
							WHEN CLEANED_BRANDNAME LIKE 'CLAIRES' THEN 'CLAIRES ACCESSORIES'
							WHEN CLEANED_BRANDNAME IN ('PAUL SMITH JUNIOR','KENZO KIDS') AND PRODUCT_NAME LIKE '%8A 14' THEN '8-14 Y'
							WHEN CLEANED_BRANDNAME IN ('PAUL SMITH JUNIOR','KENZO KIDS') AND PRODUCT_NAME LIKE '%816A' THEN '8-16 Y'
							WHEN CLEANED_BRANDNAME IN ('PAUL SMITH JUNIOR','KENZO KIDS') AND PRODUCT_NAME LIKE '%8 16A' THEN '8-16 Y'
							WHEN CLEANED_BRANDNAME IN ('PAUL SMITH JUNIOR','KENZO KIDS') AND PRODUCT_NAME LIKE '%8 16A' THEN '8-16 Y'
							WHEN CLEANED_BRANDNAME IN ('PAUL SMITH JUNIOR','KENZO KIDS') AND PRODUCT_NAME LIKE '%1014' THEN '10-14 Y'
							WHEN CLEANED_BRANDNAME IN ('PAUL SMITH JUNIOR','KENZO KIDS') AND PRODUCT_NAME LIKE '%10 14' THEN '10-14 Y'
							WHEN CLEANED_BRANDNAME IN ('PAUL SMITH JUNIOR','KENZO KIDS') AND PRODUCT_NAME LIKE '%10/14' THEN '10-14 Y'
							WHEN CLEANED_BRANDNAME IN ('PAUL SMITH JUNIOR','KENZO KIDS') AND PRODUCT_NAME LIKE '%1016' THEN '10-16 Y'
							WHEN CLEANED_BRANDNAME IN ('PAUL SMITH JUNIOR','KENZO KIDS') AND PRODUCT_NAME LIKE '%10 16' THEN '10-16 Y'
							WHEN CLEANED_BRANDNAME IN ('PAUL SMITH JUNIOR','KENZO KIDS') AND PRODUCT_NAME LIKE '%10/16' THEN '10-16 Y'
							WHEN SUBDEPT_NAME = '4 - 8 GIRLS' AND PRODUCT_NAME LIKE '%BELT%70' THEN 'BELT 70 CM'
							WHEN SUBDEPT_NAME = 'CHILDREN S ACCESSORIES' AND PRODUCT_NAME LIKE '%7 14' THEN '7-14 Y'
							WHEN SUBDEPT_NAME = 'CHILDREN S ACCESSORIES' AND PRODUCT_NAME LIKE '%7-16 Y' THEN '7-16 Y'
							WHEN SUBDEPT_NAME = 'CHILDREN S ACCESSORIES' AND PRODUCT_NAME LIKE '%9 17Y' THEN '9-17 Y'
							WHEN SUBDEPT_NAME = 'CHILDREN S ACCESSORIES' AND (CLEANED_BRANDNAME LIKE 'ANGEL KIDS' AND PRODUCT_NAME NOT LIKE '%13 18%' AND PRODUCT_NAME NOT LIKE '%10 15%') THEN 'KID SOCKS FREESIZE'
							WHEN SUBDEPT_NAME = 'CHILDREN S ACCESSORIES' AND CLEANED_BRANDNAME IN ('FJALL RAVEN') THEN '����������'
							WHEN SUBDEPT_NAME = 'CHILDREN S ACCESSORIES' AND CLEANED_BRANDNAME IN ('JACOB PRO') THEN '�����ҹѡ���¹'
							ELSE JUNIOR_KEYWORD END)
	,TEEN_KEYWORD = (CASE WHEN CLEANED_BRANDNAME LIKE 'KIDS & TEENS%' AND PRODUCT_NAME LIKE '%MOOMIN%' THEN 'MOOMIN'
							WHEN CLEANED_BRANDNAME LIKE 'KIDS & TEENS%' AND PRODUCT_NAME LIKE '%���Թ%' THEN 'MOOMIN'
							WHEN CLEANED_BRANDNAME LIKE 'KIDS & TEENS%' AND PRODUCT_NAME LIKE '%PAUL FRANK%' THEN 'PAUL FRANK'
							WHEN CLEANED_BRANDNAME LIKE 'KIDS & TEENS%' AND PRODUCT_NAME LIKE '%PUAL FRANK%' THEN 'PAUL FRANK'
							WHEN CLEANED_BRANDNAME LIKE 'KIDS & TEENS%' AND PRODUCT_NAME LIKE '%������%' THEN 'TSUM TSUM'
							WHEN CLEANED_BRANDNAME LIKE 'KIDS & TEENS%' AND PRODUCT_NAME LIKE '%TSUM%TSUM%' THEN 'TSUM TSUM'
							WHEN CLEANED_BRANDNAME LIKE 'KIDS & TEENS%' AND PRODUCT_NAME LIKE '%FAIRY TAIL%' THEN 'FAIRY TAIL'
							WHEN CLEANED_BRANDNAME LIKE 'KIDS & TEENS%' AND PRODUCT_NAME LIKE '%WE BARS BEAR%' THEN 'WE BARE BEARS'
							WHEN CLEANED_BRANDNAME LIKE 'KIDS & TEENS%' AND PRODUCT_NAME LIKE '%��ǵ������%' THEN 'CUTIE MARK'
							WHEN CLEANED_BRANDNAME LIKE 'KIDS & TEENS%' AND PRODUCT_NAME LIKE '%MINION%' THEN 'MINION'
							WHEN CLEANED_BRANDNAME LIKE 'KIDS & TEENS%' AND PRODUCT_NAME LIKE '%YOKAI WATCH%' THEN 'YOKAI WATCH'
							WHEN CLEANED_BRANDNAME LIKE 'KIDS & TEENS%' AND PRODUCT_NAME LIKE '%FURBY%' THEN 'FURBY BAG'
							WHEN CLEANED_BRANDNAME LIKE 'KIDS & TEENS%' AND PRODUCT_NAME LIKE '%DISNEY%' THEN 'DISNEY'
							WHEN CLEANED_BRANDNAME LIKE 'KIDS & TEENS%' AND PRODUCT_NAME LIKE '%���ի�%' THEN 'PVC WALLET'
							WHEN CLEANED_BRANDNAME LIKE 'KIDS & TEENS%' AND PRODUCT_NAME LIKE '%�����ҾѺ��%' THEN 'WALLET'
							WHEN CLEANED_BRANDNAME LIKE 'KIDS & TEENS%' AND PRODUCT_NAME LIKE '%������KIDS&TEENS%' THEN 'KID AND TEEN BAG'
							WHEN CLEANED_BRANDNAME LIKE 'PAUL FRANK%' THEN 'PAUL FRANK'
							WHEN CLEANED_BRANDNAME IN ('RIDAZ','RILAKKUMA','SANRIO IMPORT','SANRIO LOCAL','SANRIO PRO','TSUM TSUM','WEBARE BEARS'
												,'WEBARE BEARS PRO1','KUMAMON','KIDS & TEENS PRO1','KAKAO FRIENDS','LIU DE SAC') THEN 'KID AND TEEN BAG'
							WHEN CLEANED_BRANDNAME LIKE 'CLAIRES' THEN 'CLAIRES ACCESSORIES'
							WHEN CLEANED_BRANDNAME IN ('PAUL SMITH JUNIOR','KENZO KIDS') AND PRODUCT_NAME LIKE '%8A 14' THEN '8-14 Y'
							WHEN CLEANED_BRANDNAME IN ('PAUL SMITH JUNIOR','KENZO KIDS') AND PRODUCT_NAME LIKE '%816A' THEN '8-16 Y'
							WHEN CLEANED_BRANDNAME IN ('PAUL SMITH JUNIOR','KENZO KIDS') AND PRODUCT_NAME LIKE '%8 16A' THEN '8-16 Y'
							WHEN CLEANED_BRANDNAME IN ('PAUL SMITH JUNIOR','KENZO KIDS') AND PRODUCT_NAME LIKE '%8 16A' THEN '8-16 Y'
							WHEN CLEANED_BRANDNAME IN ('PAUL SMITH JUNIOR','KENZO KIDS') AND PRODUCT_NAME LIKE '%1014' THEN '10-14 Y'
							WHEN CLEANED_BRANDNAME IN ('PAUL SMITH JUNIOR','KENZO KIDS') AND PRODUCT_NAME LIKE '%10 14' THEN '10-14 Y'
							WHEN CLEANED_BRANDNAME IN ('PAUL SMITH JUNIOR','KENZO KIDS') AND PRODUCT_NAME LIKE '%10/14' THEN '10-14 Y'
							WHEN CLEANED_BRANDNAME IN ('PAUL SMITH JUNIOR','KENZO KIDS') AND PRODUCT_NAME LIKE '%1016' THEN '10-16 Y'
							WHEN CLEANED_BRANDNAME IN ('PAUL SMITH JUNIOR','KENZO KIDS') AND PRODUCT_NAME LIKE '%10 16' THEN '10-16 Y'
							WHEN CLEANED_BRANDNAME IN ('PAUL SMITH JUNIOR','KENZO KIDS') AND PRODUCT_NAME LIKE '%10/16' THEN '10-16 Y'
							WHEN SUBDEPT_NAME = '4 - 8 GIRLS' AND PRODUCT_NAME LIKE '%BELT%70' THEN 'BELT 70 CM'
							WHEN SUBDEPT_NAME = 'CHILDREN S ACCESSORIES' AND PRODUCT_NAME LIKE '%7 14' THEN '7-14 Y'
							WHEN SUBDEPT_NAME = 'CHILDREN S ACCESSORIES' AND PRODUCT_NAME LIKE '%7-16 Y' THEN '7-16 Y'
							WHEN SUBDEPT_NAME = 'CHILDREN S ACCESSORIES' AND PRODUCT_NAME LIKE '%9 17Y' THEN '9-17 Y'
							WHEN SUBDEPT_NAME = 'CHILDREN S ACCESSORIES' AND (CLEANED_BRANDNAME LIKE 'ANGEL KIDS' AND PRODUCT_NAME NOT LIKE '%13 18%' AND PRODUCT_NAME NOT LIKE '%10 15%') THEN 'KID SOCKS FREESIZE'
							WHEN SUBDEPT_NAME = 'CHILDREN S ACCESSORIES' AND CLEANED_BRANDNAME IN ('FJALL RAVEN') THEN '����������'
							WHEN SUBDEPT_NAME = 'CHILDREN S ACCESSORIES' AND CLEANED_BRANDNAME IN ('JACOB PRO') THEN '�����ҹѡ���¹'
							ELSE TEEN_KEYWORD END)
WHERE MOM_AND_KID_PRODUCT_FLAG = 1
AND COALESCE(PREG_FLAG,'N/A') = 'N/A'
AND COALESCE(INFANT_FLAG,'N/A') = 'N/A'
AND COALESCE(TODDLER_FLAG,'N/A') = 'N/A'
AND COALESCE(PRESCHOOL_FLAG,'N/A') = 'N/A'
AND COALESCE(JUNIOR_FLAG,'N/A') = 'N/A'
AND COALESCE(TEEN_FLAG,'N/A') = 'N/A'
AND REVISED_CATEGORY_LEVEL_1 = 'KIDS'
AND REVISED_CATEGORY_LEVEL_2 = 'KID FASHION'
AND REVISED_CATEGORY_LEVEL_3 = 'KID FASHION ACCESSORIES'
AND (CLEANED_BRANDNAME LIKE 'KIDS & TEENS%' AND (PRODUCT_NAME LIKE '%MOOMIN%'
										OR PRODUCT_NAME LIKE '%���Թ%'
										OR PRODUCT_NAME LIKE '%PAUL FRANK%'
										OR PRODUCT_NAME LIKE '%PUAL FRANK%'
										OR PRODUCT_NAME LIKE '%������%'
										OR PRODUCT_NAME LIKE '%TSUM%TSUM%'
										OR PRODUCT_NAME LIKE '%FAIRY TAIL%'
										OR PRODUCT_NAME LIKE '%WE BARS BEAR%'
										OR PRODUCT_NAME LIKE '%��ǵ������%'
										OR PRODUCT_NAME LIKE '%MINION%'
										OR PRODUCT_NAME LIKE '%YOKAI WATCH%'
										OR PRODUCT_NAME LIKE '%FURBY%'
										OR PRODUCT_NAME LIKE '%DISNEY%'
										OR PRODUCT_NAME LIKE '%���ի�%'
										OR PRODUCT_NAME LIKE '%�����ҾѺ��%'
										OR PRODUCT_NAME LIKE '%������KIDS&TEENS%'))
	OR CLEANED_BRANDNAME LIKE 'PAUL FRANK%'
	OR CLEANED_BRANDNAME IN ('RIDAZ','RILAKKUMA','SANRIO IMPORT','SANRIO LOCAL','SANRIO PRO','TSUM TSUM','WEBARE BEARS','WEBARE BEARS PRO1','KUMAMON','KIDS & TEENS PRO1','KAKAO FRIENDS','CLAIRES')
	OR (CLEANED_BRANDNAME IN ('PAUL SMITH JUNIOR','KENZO KIDS') AND (PRODUCT_NAME LIKE '%8A 14'
															OR PRODUCT_NAME LIKE '%816A'
															OR PRODUCT_NAME LIKE '%8 16A'
															OR PRODUCT_NAME LIKE '%8 16A'
															OR PRODUCT_NAME LIKE '%1014'
															OR PRODUCT_NAME LIKE '%10 14'
															OR PRODUCT_NAME LIKE '%10/14'
															OR PRODUCT_NAME LIKE '%1016'
															OR PRODUCT_NAME LIKE '%10 16'
															OR PRODUCT_NAME LIKE '%10/16'))
	OR (SUBDEPT_NAME = '4 - 8 GIRLS' AND PRODUCT_NAME LIKE '%BELT%70')
	OR (SUBDEPT_NAME = 'CHILDREN S ACCESSORIES' AND (PRODUCT_NAME LIKE '%7 14'
													OR PRODUCT_NAME LIKE '%7-16 Y'
													OR PRODUCT_NAME LIKE '%9 17Y'
													OR (CLEANED_BRANDNAME LIKE 'ANGEL KIDS' AND PRODUCT_NAME NOT LIKE '%13 18%' AND PRODUCT_NAME NOT LIKE '%10 15%')
													OR CLEANED_BRANDNAME IN ('JACOB PRO','FJALL RAVEN')))
;

--TEEN
UPDATE vpm_data.pm_skutagging_kids_product_master
SET TEEN_FLAG = 'YES'
	,TEEN_KEYWORD = (CASE WHEN CLEANED_BRANDNAME IN ('PAUL SMITH JUNIOR','KENZO KIDS') AND PRODUCT_NAME LIKE '%1416' THEN '14-16 Y'
						  WHEN CLEANED_BRANDNAME IN ('PAUL SMITH JUNIOR','KENZO KIDS') AND PRODUCT_NAME LIKE '%14A' THEN '14Y'
						  WHEN SUBDEPT_NAME = '4 - 8 GIRLS' AND PRODUCT_NAME LIKE '%BELT%80' THEN 'BELT 80 CM'
						  WHEN SUBDEPT_NAME = '4 - 8 GIRLS' AND PRODUCT_NAME LIKE '%CAP%11' THEN 'CAP SIZE 11'
						  WHEN SUBDEPT_NAME = '4 - 8 GIRLS' AND PRODUCT_NAME LIKE 'CAP%L' THEN 'CAP SIZE L'
						  WHEN SUBDEPT_NAME = '4 - 8 GIRLS' AND PRODUCT_NAME LIKE 'SET%13' THEN '13Y'
						  WHEN SUBDEPT_NAME = '4 - 8 GIRLS' AND PRODUCT_NAME LIKE '�ҧࡧ%�%85' THEN '�ҧࡧ� 85'
						  WHEN SUBDEPT_NAME = 'CHILDREN S ACCESSORIES' AND PRODUCT_NAME LIKE '%�ا���%13 18%' THEN '13-18 Y'
						  WHEN SUBDEPT_NAME = 'CHILDREN S ACCESSORIES' AND CLEANED_BRANDNAME LIKE 'PLATYPUS' AND PRODUCT_NAME LIKE '%L' THEN 'PLATYPUS HAT SIZE L'
						  WHEN SUBDEPT_NAME = 'CHILDREN LUXE FASHION' AND PRODUCT_NAME NOT LIKE '% 16A' THEN '16Y'
						  ELSE TEEN_KEYWORD END)
WHERE MOM_AND_KID_PRODUCT_FLAG = 1
AND COALESCE(PREG_FLAG,'N/A') = 'N/A'
AND COALESCE(INFANT_FLAG,'N/A') = 'N/A'
AND COALESCE(TODDLER_FLAG,'N/A') = 'N/A'
AND COALESCE(PRESCHOOL_FLAG,'N/A') = 'N/A'
AND COALESCE(JUNIOR_FLAG,'N/A') = 'N/A'
AND COALESCE(TEEN_FLAG,'N/A') = 'N/A'
AND REVISED_CATEGORY_LEVEL_1 = 'KIDS'
AND REVISED_CATEGORY_LEVEL_2 = 'KID FASHION'
AND REVISED_CATEGORY_LEVEL_3 = 'KID FASHION ACCESSORIES'
AND ((CLEANED_BRANDNAME IN ('PAUL SMITH JUNIOR','KENZO KIDS') AND (PRODUCT_NAME LIKE '%1416'
										OR PRODUCT_NAME LIKE '%14A'))
	OR CLEANED_BRANDNAME IN ('LIU DE SAC')
	OR (SUBDEPT_NAME = '4 - 8 GIRLS' AND (PRODUCT_NAME LIKE '%BELT%80'
										OR PRODUCT_NAME LIKE '%CAP%11'
										OR PRODUCT_NAME LIKE 'CAP%L'
										OR PRODUCT_NAME LIKE 'SET%13'
										OR PRODUCT_NAME LIKE '�ҧࡧ%�%85'))
	OR (SUBDEPT_NAME = 'CHILDREN S ACCESSORIES' AND (PRODUCT_NAME LIKE '%�ا���%13 18%'
													OR (CLEANED_BRANDNAME LIKE 'PLATYPUS' AND PRODUCT_NAME LIKE '%L')))
	OR (SUBDEPT_NAME = 'CHILDREN LUXE FASHION' AND PRODUCT_NAME NOT LIKE '% 16A')
	)
;

--ELSE PRESCHOOL/JUNIOR 4-8 GIRLS
UPDATE vpm_data.pm_skutagging_kids_product_master
SET PRESCHOOL_FLAG = 'YES'
	,JUNIOR_FLAG = 'YES'
	,PRESCHOOL_KEYWORD = '4 - 8 GIRLS'
	,JUNIOR_KEYWORD = '4 - 8 GIRLS'
WHERE MOM_AND_KID_PRODUCT_FLAG = 1
AND COALESCE(PREG_FLAG,'N/A') = 'N/A'
AND COALESCE(INFANT_FLAG,'N/A') = 'N/A'
AND COALESCE(TODDLER_FLAG,'N/A') = 'N/A'
AND COALESCE(PRESCHOOL_FLAG,'N/A') = 'N/A'
AND COALESCE(JUNIOR_FLAG,'N/A') = 'N/A'
AND COALESCE(TEEN_FLAG,'N/A') = 'N/A'
AND REVISED_CATEGORY_LEVEL_1 = 'KIDS'
AND REVISED_CATEGORY_LEVEL_2 = 'KID FASHION'
AND REVISED_CATEGORY_LEVEL_3 = 'KID FASHION ACCESSORIES'
AND SUBDEPT_NAME = '4 - 8 GIRLS'
;


-----KID SHOES
UPDATE vpm_data.pm_skutagging_kids_product_master
SET MOM_AND_KID_PRODUCT_FLAG = 0
WHERE CLEANED_BRANDNAME LIKE 'O&B%'
OR CLEANED_BRANDNAME LIKE 'HOLSTER%'
;

--NEW INFANT
UPDATE vpm_data.pm_skutagging_kids_product_master
SET INFANT_FLAG = 'YES'
	,"STAGE_M+0_TO_M+3" = (CASE WHEN CLEANED_BRANDNAME IN ('AERA PRO','BARBIE','BARBIE SHOES','BARBIE SHOES PRO','BE FUN','DISNEY','DISNEY PRINCESS','DISNEY PRO'
													,'HELLO KITTY','HELLO KITTY PRO','HERO','HERO KIDS','HERO KIDS PRO','MICKEY MOUSE','MICKEY MOUSE PRO','SUNSANG')
													 AND PRODUCT_NAME NOT LIKE '�ͧ���������������%' AND PRODUCT_NAME LIKE '%10' THEN 'YES'
								 WHEN CLEANED_BRANDNAME IN ('ANGEL KIDS','BABY DIOR','BIBI','BIBI PRO','BLOCH','EMPORIO ARMANI','FILA','GOLD CITY','BREAKER'
													,'GRENDENE KIDS','GUCCI KIDS GIRL','HAVAIANAS','HAVAIANAS PRO1','IPANEMA','KENZO KIDS','KITO','KOOTS'
													,'KOU KOU','LICENSES SHOES','LICENSES SHOES PRO','LITTLE STEPS','LITTLE STEPS PRO','LULU','MIMI SHOES'
													,'MIMI SHOES PRO','MIMI SHOES PROMO','MINI MELISSA','PAUL SMITH JUNIOR','POPGIRL','POPMAN','POPTEEN'
													,'REPLAY','SALTWATER','SANRIO','SANRIO BABY','SCS','SCS BREAKER','SMALLTIME','SNAP','SPORT FLEX BIBI'
													,'SPORT FLEX BIBI PRO','SPORT KIDS','SPORTFLEX BIBI','TROISINCH','UNITED NUDE','VICTORIA')
													AND PRODUCT_NAME NOT LIKE '%[0-9][0-9][0-9]' AND PRODUCT_NAME LIKE '%15' THEN 'YES'
								 WHEN CLEANED_BRANDNAME IN ('ANGEL KIDS','BABY DIOR','BIBI','BIBI PRO','BLOCH','EMPORIO ARMANI','FILA','GOLD CITY','BREAKER'
													,'GRENDENE KIDS','GUCCI KIDS GIRL','HAVAIANAS','HAVAIANAS PRO1','IPANEMA','KENZO KIDS','KITO','KOOTS'
													,'KOU KOU','LICENSES SHOES','LICENSES SHOES PRO','LITTLE STEPS','LITTLE STEPS PRO','LULU','MIMI SHOES'
													,'MIMI SHOES PRO','MIMI SHOES PROMO','MINI MELISSA','PAUL SMITH JUNIOR','POPGIRL','POPMAN','POPTEEN'
													,'REPLAY','SALTWATER','SANRIO','SANRIO BABY','SCS','SCS BREAKER','SMALLTIME','SNAP','SPORT FLEX BIBI'
													,'SPORT FLEX BIBI PRO','SPORT KIDS','SPORTFLEX BIBI','TROISINCH','UNITED NUDE','VICTORIA')
													AND PRODUCT_NAME NOT LIKE '%[0-9][0-9][0-9]' AND PRODUCT_NAME LIKE '%16' THEN 'YES'
								 WHEN CLEANED_BRANDNAME LIKE 'HERO' AND PRODUCT_NAME LIKE '%40-60' THEN 'YES'
								 ELSE "STAGE_M+0_TO_M+3" END)
	,"STAGE_M+3_TO_M+6" = (CASE WHEN CLEANED_BRANDNAME IN ('AERA PRO','BARBIE','BARBIE SHOES','BARBIE SHOES PRO','BE FUN','DISNEY','DISNEY PRINCESS','DISNEY PRO'
													,'HELLO KITTY','HELLO KITTY PRO','HERO','HERO KIDS','HERO KIDS PRO','MICKEY MOUSE','MICKEY MOUSE PRO','SUNSANG')
													 AND PRODUCT_NAME NOT LIKE '�ͧ���������������%' AND PRODUCT_NAME LIKE '%11' THEN 'YES'
								 WHEN CLEANED_BRANDNAME IN ('ANGEL KIDS','BABY DIOR','BIBI','BIBI PRO','BLOCH','EMPORIO ARMANI','FILA','GOLD CITY','BREAKER'
													,'GRENDENE KIDS','GUCCI KIDS GIRL','HAVAIANAS','HAVAIANAS PRO1','IPANEMA','KENZO KIDS','KITO','KOOTS'
													,'KOU KOU','LICENSES SHOES','LICENSES SHOES PRO','LITTLE STEPS','LITTLE STEPS PRO','LULU','MIMI SHOES'
													,'MIMI SHOES PRO','MIMI SHOES PROMO','MINI MELISSA','PAUL SMITH JUNIOR','POPGIRL','POPMAN','POPTEEN'
													,'REPLAY','SALTWATER','SANRIO','SANRIO BABY','SCS','SCS BREAKER','SMALLTIME','SNAP','SPORT FLEX BIBI'
													,'SPORT FLEX BIBI PRO','SPORT KIDS','SPORTFLEX BIBI','TROISINCH','UNITED NUDE','VICTORIA')
													AND PRODUCT_NAME NOT LIKE '%[0-9][0-9][0-9]' AND PRODUCT_NAME LIKE '%17' THEN 'YES'
								 WHEN CLEANED_BRANDNAME IN ('CROCS','CROCS CREDIT','NEW BALANCE','SKECHERS','ADDA','OSH KOSH BGOSH','OSH KOSH BGOSH PRO1','OSHKOSH','OSHKOSH PRO'
													,'TOEZONE','TOEZONE PRO','TOEZONE PRO1') AND PRODUCT_NAME LIKE '% 1' THEN 'YES'
								 WHEN CLEANED_BRANDNAME LIKE 'FREDDIE THE FROG' AND PRODUCT_NAME LIKE '%3' THEN 'YES'
								 WHEN CLEANED_BRANDNAME LIKE 'FREDDIE THE FROG' AND PRODUCT_NAME NOT LIKE '%[0-9]' THEN 'YES'
								 WHEN CLEANED_BRANDNAME LIKE 'HERO' AND PRODUCT_NAME LIKE '%40-60' THEN 'YES'
								 ELSE "STAGE_M+3_TO_M+6" END)
	,"STAGE_M+6_TO_M+9" = (CASE WHEN CLEANED_BRANDNAME IN ('AERA PRO','BARBIE','BARBIE SHOES','BARBIE SHOES PRO','BE FUN','DISNEY','DISNEY PRINCESS','DISNEY PRO'
													,'HELLO KITTY','HELLO KITTY PRO','HERO','HERO KIDS','HERO KIDS PRO','MICKEY MOUSE','MICKEY MOUSE PRO','SUNSANG')
													 AND PRODUCT_NAME NOT LIKE '�ͧ���������������%' AND PRODUCT_NAME LIKE '%11' THEN 'YES'
								 WHEN CLEANED_BRANDNAME IN ('ANGEL KIDS','BABY DIOR','BIBI','BIBI PRO','BLOCH','EMPORIO ARMANI','FILA','GOLD CITY','BREAKER'
													,'GRENDENE KIDS','GUCCI KIDS GIRL','HAVAIANAS','HAVAIANAS PRO1','IPANEMA','KENZO KIDS','KITO','KOOTS'
													,'KOU KOU','LICENSES SHOES','LICENSES SHOES PRO','LITTLE STEPS','LITTLE STEPS PRO','LULU','MIMI SHOES'
													,'MIMI SHOES PRO','MIMI SHOES PROMO','MINI MELISSA','PAUL SMITH JUNIOR','POPGIRL','POPMAN','POPTEEN'
													,'REPLAY','SALTWATER','SANRIO','SANRIO BABY','SCS','SCS BREAKER','SMALLTIME','SNAP','SPORT FLEX BIBI'
													,'SPORT FLEX BIBI PRO','SPORT KIDS','SPORTFLEX BIBI','TROISINCH','UNITED NUDE','VICTORIA')
													AND PRODUCT_NAME NOT LIKE '%[0-9][0-9][0-9]' AND PRODUCT_NAME LIKE '%18' THEN 'YES'
								 WHEN CLEANED_BRANDNAME IN ('CROCS','CROCS CREDIT','NEW BALANCE','SKECHERS','ADDA','OSH KOSH BGOSH','OSH KOSH BGOSH PRO1','OSHKOSH','OSHKOSH PRO'
													,'TOEZONE','TOEZONE PRO','TOEZONE PRO1') AND PRODUCT_NAME LIKE '% 2' THEN 'YES'
								 WHEN CLEANED_BRANDNAME LIKE 'FREDDIE THE FROG' AND PRODUCT_NAME LIKE '%4' THEN 'YES'
								 WHEN CLEANED_BRANDNAME LIKE 'FREDDIE THE FROG' AND PRODUCT_NAME NOT LIKE '%[0-9]' THEN 'YES'
								 ELSE "STAGE_M+6_TO_M+9" END)
	,"STAGE_M+9_TO_M+12" = (CASE WHEN CLEANED_BRANDNAME IN ('AERA PRO','BARBIE','BARBIE SHOES','BARBIE SHOES PRO','BE FUN','DISNEY','DISNEY PRINCESS','DISNEY PRO'
													,'HELLO KITTY','HELLO KITTY PRO','HERO','HERO KIDS','HERO KIDS PRO','MICKEY MOUSE','MICKEY MOUSE PRO','SUNSANG')
													 AND PRODUCT_NAME NOT LIKE '�ͧ���������������%' AND PRODUCT_NAME LIKE '%12' THEN 'YES'
								 WHEN CLEANED_BRANDNAME IN ('ANGEL KIDS','BABY DIOR','BIBI','BIBI PRO','BLOCH','EMPORIO ARMANI','FILA','GOLD CITY','BREAKER'
													,'GRENDENE KIDS','GUCCI KIDS GIRL','HAVAIANAS','HAVAIANAS PRO1','IPANEMA','KENZO KIDS','KITO','KOOTS'
													,'KOU KOU','LICENSES SHOES','LICENSES SHOES PRO','LITTLE STEPS','LITTLE STEPS PRO','LULU','MIMI SHOES'
													,'MIMI SHOES PRO','MIMI SHOES PROMO','MINI MELISSA','PAUL SMITH JUNIOR','POPGIRL','POPMAN','POPTEEN'
													,'REPLAY','SALTWATER','SANRIO','SANRIO BABY','SCS','SCS BREAKER','SMALLTIME','SNAP','SPORT FLEX BIBI'
													,'SPORT FLEX BIBI PRO','SPORT KIDS','SPORTFLEX BIBI','TROISINCH','UNITED NUDE','VICTORIA')
													AND PRODUCT_NAME NOT LIKE '%[0-9][0-9][0-9]' AND PRODUCT_NAME LIKE '%19' THEN 'YES'
								 WHEN CLEANED_BRANDNAME IN ('CROCS','CROCS CREDIT','NEW BALANCE','SKECHERS','ADDA','OSH KOSH BGOSH','OSH KOSH BGOSH PRO1','OSHKOSH','OSHKOSH PRO'
													,'TOEZONE','TOEZONE PRO','TOEZONE PRO1') AND PRODUCT_NAME LIKE '% 3' THEN 'YES'
								 WHEN CLEANED_BRANDNAME LIKE 'ADIDAS KIDS' AND PRODUCT_NAME NOT LIKE '%[0-9][0-9][0-9]' AND PRODUCT_NAME LIKE '%74' THEN 'YES'
								 WHEN CLEANED_BRANDNAME LIKE 'FREDDIE THE FROG' AND PRODUCT_NAME LIKE '%5' THEN 'YES'
								 WHEN CLEANED_BRANDNAME LIKE 'FREDDIE THE FROG' AND PRODUCT_NAME NOT LIKE '%[0-9]' THEN 'YES'
								 ELSE "STAGE_M+9_TO_M+12" END)
	,TODDLER_FLAG = (CASE WHEN CLEANED_BRANDNAME LIKE 'SKIPPON' AND PRODUCT_NAME LIKE '%13' THEN 'YES'
									WHEN CLEANED_BRANDNAME LIKE 'SKIPPON' AND PRODUCT_NAME LIKE '%14' THEN 'YES'
									WHEN CLEANED_BRANDNAME LIKE 'SKIPPON' AND PRODUCT_NAME LIKE '%15' THEN 'YES'
									WHEN CLEANED_BRANDNAME IN ('AERA PRO','BARBIE','BARBIE SHOES','BARBIE SHOES PRO','BE FUN','DISNEY','DISNEY PRINCESS','DISNEY PRO'
													,'HELLO KITTY','HELLO KITTY PRO','HERO','HERO KIDS','HERO KIDS PRO','MICKEY MOUSE','MICKEY MOUSE PRO','SUNSANG')
													 AND PRODUCT_NAME NOT LIKE '�ͧ���������������%' AND PRODUCT_NAME LIKE '%13' THEN 'YES'
									WHEN CLEANED_BRANDNAME IN ('AERA PRO','BARBIE','BARBIE SHOES','BARBIE SHOES PRO','BE FUN','DISNEY','DISNEY PRINCESS','DISNEY PRO'
													,'HELLO KITTY','HELLO KITTY PRO','HERO','HERO KIDS','HERO KIDS PRO','MICKEY MOUSE','MICKEY MOUSE PRO','SUNSANG')
													 AND PRODUCT_NAME NOT LIKE '�ͧ���������������%' AND PRODUCT_NAME LIKE '%14' THEN 'YES'
									WHEN CLEANED_BRANDNAME IN ('AERA PRO','BARBIE','BARBIE SHOES','BARBIE SHOES PRO','BE FUN','DISNEY','DISNEY PRINCESS','DISNEY PRO'
													,'HELLO KITTY','HELLO KITTY PRO','HERO','HERO KIDS','HERO KIDS PRO','MICKEY MOUSE','MICKEY MOUSE PRO','SUNSANG')
													 AND PRODUCT_NAME NOT LIKE '�ͧ���������������%' AND PRODUCT_NAME LIKE '%15' THEN 'YES'
									WHEN CLEANED_BRANDNAME IN ('ANGEL KIDS','BABY DIOR','BIBI','BIBI PRO','BLOCH','EMPORIO ARMANI','FILA','GOLD CITY','BREAKER'
													,'GRENDENE KIDS','GUCCI KIDS GIRL','HAVAIANAS','HAVAIANAS PRO1','IPANEMA','KENZO KIDS','KITO','KOOTS'
													,'KOU KOU','LICENSES SHOES','LICENSES SHOES PRO','LITTLE STEPS','LITTLE STEPS PRO','LULU','MIMI SHOES'
													,'MIMI SHOES PRO','MIMI SHOES PROMO','MINI MELISSA','PAUL SMITH JUNIOR','POPGIRL','POPMAN','POPTEEN'
													,'REPLAY','SALTWATER','SANRIO','SANRIO BABY','SCS','SCS BREAKER','SMALLTIME','SNAP','SPORT FLEX BIBI'
													,'SPORT FLEX BIBI PRO','SPORT KIDS','SPORTFLEX BIBI','TROISINCH','UNITED NUDE','VICTORIA')
													AND PRODUCT_NAME NOT LIKE '%[0-9][0-9][0-9]' AND PRODUCT_NAME LIKE '%20' THEN 'YES'
									WHEN CLEANED_BRANDNAME IN ('ANGEL KIDS','BABY DIOR','BIBI','BIBI PRO','BLOCH','EMPORIO ARMANI','FILA','GOLD CITY','BREAKER'
													,'GRENDENE KIDS','GUCCI KIDS GIRL','HAVAIANAS','HAVAIANAS PRO1','IPANEMA','KENZO KIDS','KITO','KOOTS'
													,'KOU KOU','LICENSES SHOES','LICENSES SHOES PRO','LITTLE STEPS','LITTLE STEPS PRO','LULU','MIMI SHOES'
													,'MIMI SHOES PRO','MIMI SHOES PROMO','MINI MELISSA','PAUL SMITH JUNIOR','POPGIRL','POPMAN','POPTEEN'
													,'REPLAY','SALTWATER','SANRIO','SANRIO BABY','SCS','SCS BREAKER','SMALLTIME','SNAP','SPORT FLEX BIBI'
													,'SPORT FLEX BIBI PRO','SPORT KIDS','SPORTFLEX BIBI','TROISINCH','UNITED NUDE','VICTORIA')
													AND PRODUCT_NAME NOT LIKE '%[0-9][0-9][0-9]' AND PRODUCT_NAME LIKE '%21' THEN 'YES'
									WHEN CLEANED_BRANDNAME IN ('ANGEL KIDS','BABY DIOR','BIBI','BIBI PRO','BLOCH','EMPORIO ARMANI','FILA','GOLD CITY','BREAKER'
													,'GRENDENE KIDS','GUCCI KIDS GIRL','HAVAIANAS','HAVAIANAS PRO1','IPANEMA','KENZO KIDS','KITO','KOOTS'
													,'KOU KOU','LICENSES SHOES','LICENSES SHOES PRO','LITTLE STEPS','LITTLE STEPS PRO','LULU','MIMI SHOES'
													,'MIMI SHOES PRO','MIMI SHOES PROMO','MINI MELISSA','PAUL SMITH JUNIOR','POPGIRL','POPMAN','POPTEEN'
													,'REPLAY','SALTWATER','SANRIO','SANRIO BABY','SCS','SCS BREAKER','SMALLTIME','SNAP','SPORT FLEX BIBI'
													,'SPORT FLEX BIBI PRO','SPORT KIDS','SPORTFLEX BIBI','TROISINCH','UNITED NUDE','VICTORIA')
													AND PRODUCT_NAME NOT LIKE '%[0-9][0-9][0-9]' AND PRODUCT_NAME LIKE '%22' THEN 'YES'
									WHEN CLEANED_BRANDNAME IN ('CROCS','CROCS CREDIT','NEW BALANCE','SKECHERS','ADDA','OSH KOSH BGOSH','OSH KOSH BGOSH PRO1','OSHKOSH','OSHKOSH PRO'
													,'TOEZONE','TOEZONE PRO','TOEZONE PRO1') AND PRODUCT_NAME LIKE '% 4' THEN 'YES'
									WHEN CLEANED_BRANDNAME IN ('CROCS','CROCS CREDIT','NEW BALANCE','SKECHERS','ADDA','OSH KOSH BGOSH','OSH KOSH BGOSH PRO1','OSHKOSH','OSHKOSH PRO'
													,'TOEZONE','TOEZONE PRO','TOEZONE PRO1') AND PRODUCT_NAME LIKE '% 5' THEN 'YES'
									WHEN PRODUCT_NAME LIKE '%20' THEN 'YES'
									WHEN PRODUCT_NAME LIKE '%21' THEN 'YES'
									WHEN PRODUCT_NAME LIKE '%22' AND PRODUCT_NAME NOT LIKE '%122' THEN 'YES'
									WHEN CLEANED_BRANDNAME LIKE 'ADIDAS KIDS' AND PRODUCT_NAME NOT LIKE '%[0-9][0-9][0-9]' AND PRODUCT_NAME LIKE '%80' THEN 'YES'
									WHEN CLEANED_BRANDNAME LIKE 'ADIDAS KIDS' AND PRODUCT_NAME NOT LIKE '%[0-9][0-9][0-9]' AND PRODUCT_NAME LIKE '%86' THEN 'YES'
									ELSE TODDLER_FLAG END)
	,UNKNOWN_INFANT_SUBSTAGE = (CASE WHEN CLEANED_BRANDNAME LIKE 'FOF' AND PRODUCT_NAME LIKE '�ͧ���% 1' THEN 'YES'
									WHEN CLEANED_BRANDNAME LIKE 'FOF' AND PRODUCT_NAME LIKE '�ͧ���% 2' THEN 'YES'
									WHEN CLEANED_BRANDNAME LIKE 'FOF' AND PRODUCT_NAME LIKE '�ѷ��% 2' THEN 'YES'
									WHEN CLEANED_BRANDNAME LIKE 'FOF' AND PRODUCT_NAME LIKE '�ѷ��% 3' THEN 'YES'
									ELSE UNKNOWN_INFANT_SUBSTAGE END)
	,INFANT_0_3M_KEYWORD = (CASE WHEN CLEANED_BRANDNAME IN ('AERA PRO','BARBIE','BARBIE SHOES','BARBIE SHOES PRO','BE FUN','DISNEY','DISNEY PRINCESS','DISNEY PRO'
													,'HELLO KITTY','HELLO KITTY PRO','HERO','HERO KIDS','HERO KIDS PRO','MICKEY MOUSE','MICKEY MOUSE PRO','SUNSANG')
													 AND PRODUCT_NAME NOT LIKE '�ͧ���������������%' AND PRODUCT_NAME LIKE '%10' THEN 'SHOES 10 CM'
								 WHEN CLEANED_BRANDNAME IN ('ANGEL KIDS','BABY DIOR','BIBI','BIBI PRO','BLOCH','EMPORIO ARMANI','FILA','GOLD CITY','BREAKER'
													,'GRENDENE KIDS','GUCCI KIDS GIRL','HAVAIANAS','HAVAIANAS PRO1','IPANEMA','KENZO KIDS','KITO','KOOTS'
													,'KOU KOU','LICENSES SHOES','LICENSES SHOES PRO','LITTLE STEPS','LITTLE STEPS PRO','LULU','MIMI SHOES'
													,'MIMI SHOES PRO','MIMI SHOES PROMO','MINI MELISSA','PAUL SMITH JUNIOR','POPGIRL','POPMAN','POPTEEN'
													,'REPLAY','SALTWATER','SANRIO','SANRIO BABY','SCS','SCS BREAKER','SMALLTIME','SNAP','SPORT FLEX BIBI'
													,'SPORT FLEX BIBI PRO','SPORT KIDS','SPORTFLEX BIBI','TROISINCH','UNITED NUDE','VICTORIA')
													AND PRODUCT_NAME NOT LIKE '%[0-9][0-9][0-9]' AND PRODUCT_NAME LIKE '%15' THEN 'SHOES EU SIZE 15'
								 WHEN CLEANED_BRANDNAME IN ('ANGEL KIDS','BABY DIOR','BIBI','BIBI PRO','BLOCH','EMPORIO ARMANI','FILA','GOLD CITY','BREAKER'
													,'GRENDENE KIDS','GUCCI KIDS GIRL','HAVAIANAS','HAVAIANAS PRO1','IPANEMA','KENZO KIDS','KITO','KOOTS'
													,'KOU KOU','LICENSES SHOES','LICENSES SHOES PRO','LITTLE STEPS','LITTLE STEPS PRO','LULU','MIMI SHOES'
													,'MIMI SHOES PRO','MIMI SHOES PROMO','MINI MELISSA','PAUL SMITH JUNIOR','POPGIRL','POPMAN','POPTEEN'
													,'REPLAY','SALTWATER','SANRIO','SANRIO BABY','SCS','SCS BREAKER','SMALLTIME','SNAP','SPORT FLEX BIBI'
													,'SPORT FLEX BIBI PRO','SPORT KIDS','SPORTFLEX BIBI','TROISINCH','UNITED NUDE','VICTORIA')
													AND PRODUCT_NAME NOT LIKE '%[0-9][0-9][0-9]' AND PRODUCT_NAME LIKE '%16' THEN 'SHOES EU SIZE 16'
								 WHEN CLEANED_BRANDNAME LIKE 'HERO' AND PRODUCT_NAME LIKE '%40-60' THEN '40-60 CM'
								 ELSE INFANT_0_3M_KEYWORD END)
	,INFANT_4_6M_KEYWORD = (CASE WHEN CLEANED_BRANDNAME IN ('AERA PRO','BARBIE','BARBIE SHOES','BARBIE SHOES PRO','BE FUN','DISNEY','DISNEY PRINCESS','DISNEY PRO'
													,'HELLO KITTY','HELLO KITTY PRO','HERO','HERO KIDS','HERO KIDS PRO','MICKEY MOUSE','MICKEY MOUSE PRO','SUNSANG')
													 AND PRODUCT_NAME NOT LIKE '�ͧ���������������%' AND PRODUCT_NAME LIKE '%11' THEN 'SHOES 11 CM'
								 WHEN CLEANED_BRANDNAME IN ('ANGEL KIDS','BABY DIOR','BIBI','BIBI PRO','BLOCH','EMPORIO ARMANI','FILA','GOLD CITY','BREAKER'
													,'GRENDENE KIDS','GUCCI KIDS GIRL','HAVAIANAS','HAVAIANAS PRO1','IPANEMA','KENZO KIDS','KITO','KOOTS'
													,'KOU KOU','LICENSES SHOES','LICENSES SHOES PRO','LITTLE STEPS','LITTLE STEPS PRO','LULU','MIMI SHOES'
													,'MIMI SHOES PRO','MIMI SHOES PROMO','MINI MELISSA','PAUL SMITH JUNIOR','POPGIRL','POPMAN','POPTEEN'
													,'REPLAY','SALTWATER','SANRIO','SANRIO BABY','SCS','SCS BREAKER','SMALLTIME','SNAP','SPORT FLEX BIBI'
													,'SPORT FLEX BIBI PRO','SPORT KIDS','SPORTFLEX BIBI','TROISINCH','UNITED NUDE','VICTORIA')
													AND PRODUCT_NAME NOT LIKE '%[0-9][0-9][0-9]' AND PRODUCT_NAME LIKE '%17' THEN 'SHOES EU SIZE 17'
								 WHEN CLEANED_BRANDNAME IN ('CROCS','CROCS CREDIT','NEW BALANCE','SKECHERS','ADDA','OSH KOSH BGOSH','OSH KOSH BGOSH PRO1','OSHKOSH','OSHKOSH PRO'
													,'TOEZONE','TOEZONE PRO','TOEZONE PRO1') AND PRODUCT_NAME LIKE '% 1' THEN 'SHOES UK SIZE 1'
								 WHEN CLEANED_BRANDNAME LIKE 'FREDDIE THE FROG' AND PRODUCT_NAME LIKE '%3' THEN 'FREDDIE THE FROG SIZE 3'
								 WHEN CLEANED_BRANDNAME LIKE 'FREDDIE THE FROG' AND PRODUCT_NAME NOT LIKE '%[0-9]' THEN 'FREDDIE THE FROG SHOES'
								 WHEN CLEANED_BRANDNAME LIKE 'HERO' AND PRODUCT_NAME LIKE '%40-60' THEN '40-60 CM'
								 ELSE INFANT_4_6M_KEYWORD END)
	,INFANT_7_9M_KEYWORD = (CASE WHEN CLEANED_BRANDNAME IN ('AERA PRO','BARBIE','BARBIE SHOES','BARBIE SHOES PRO','BE FUN','DISNEY','DISNEY PRINCESS','DISNEY PRO'
													,'HELLO KITTY','HELLO KITTY PRO','HERO','HERO KIDS','HERO KIDS PRO','MICKEY MOUSE','MICKEY MOUSE PRO','SUNSANG')
													 AND PRODUCT_NAME NOT LIKE '�ͧ���������������%' AND PRODUCT_NAME LIKE '%11' THEN 'SHOES 11 CM'
								 WHEN CLEANED_BRANDNAME IN ('ANGEL KIDS','BABY DIOR','BIBI','BIBI PRO','BLOCH','EMPORIO ARMANI','FILA','GOLD CITY','BREAKER'
													,'GRENDENE KIDS','GUCCI KIDS GIRL','HAVAIANAS','HAVAIANAS PRO1','IPANEMA','KENZO KIDS','KITO','KOOTS'
													,'KOU KOU','LICENSES SHOES','LICENSES SHOES PRO','LITTLE STEPS','LITTLE STEPS PRO','LULU','MIMI SHOES'
													,'MIMI SHOES PRO','MIMI SHOES PROMO','MINI MELISSA','PAUL SMITH JUNIOR','POPGIRL','POPMAN','POPTEEN'
													,'REPLAY','SALTWATER','SANRIO','SANRIO BABY','SCS','SCS BREAKER','SMALLTIME','SNAP','SPORT FLEX BIBI'
													,'SPORT FLEX BIBI PRO','SPORT KIDS','SPORTFLEX BIBI','TROISINCH','UNITED NUDE','VICTORIA')
													AND PRODUCT_NAME NOT LIKE '%[0-9][0-9][0-9]' AND PRODUCT_NAME LIKE '%18' THEN 'SHOES EU SIZE 18'
								 WHEN CLEANED_BRANDNAME IN ('CROCS','CROCS CREDIT','NEW BALANCE','SKECHERS','ADDA','OSH KOSH BGOSH','OSH KOSH BGOSH PRO1','OSHKOSH','OSHKOSH PRO'
													,'TOEZONE','TOEZONE PRO','TOEZONE PRO1') AND PRODUCT_NAME LIKE '% 2' THEN 'SHOES UK SIZE 2'
								 WHEN CLEANED_BRANDNAME LIKE 'FREDDIE THE FROG' AND PRODUCT_NAME LIKE '%4' THEN 'FREDDIE THE FROG SIZE 4'
								 WHEN CLEANED_BRANDNAME LIKE 'FREDDIE THE FROG' AND PRODUCT_NAME NOT LIKE '%[0-9]' THEN 'FREDDIE THE FROG SHOES'
								 ELSE INFANT_7_9M_KEYWORD END)
	,INFANT_10_12M_KEYWORD = (CASE WHEN CLEANED_BRANDNAME IN ('AERA PRO','BARBIE','BARBIE SHOES','BARBIE SHOES PRO','BE FUN','DISNEY','DISNEY PRINCESS','DISNEY PRO'
													,'HELLO KITTY','HELLO KITTY PRO','HERO','HERO KIDS','HERO KIDS PRO','MICKEY MOUSE','MICKEY MOUSE PRO','SUNSANG')
													 AND PRODUCT_NAME NOT LIKE '�ͧ���������������%' AND PRODUCT_NAME LIKE '%12' THEN 'SHOES 12 CM'
								 WHEN CLEANED_BRANDNAME IN ('ANGEL KIDS','BABY DIOR','BIBI','BIBI PRO','BLOCH','EMPORIO ARMANI','FILA','GOLD CITY','BREAKER'
													,'GRENDENE KIDS','GUCCI KIDS GIRL','HAVAIANAS','HAVAIANAS PRO1','IPANEMA','KENZO KIDS','KITO','KOOTS'
													,'KOU KOU','LICENSES SHOES','LICENSES SHOES PRO','LITTLE STEPS','LITTLE STEPS PRO','LULU','MIMI SHOES'
													,'MIMI SHOES PRO','MIMI SHOES PROMO','MINI MELISSA','PAUL SMITH JUNIOR','POPGIRL','POPMAN','POPTEEN'
													,'REPLAY','SALTWATER','SANRIO','SANRIO BABY','SCS','SCS BREAKER','SMALLTIME','SNAP','SPORT FLEX BIBI'
													,'SPORT FLEX BIBI PRO','SPORT KIDS','SPORTFLEX BIBI','TROISINCH','UNITED NUDE','VICTORIA')
													AND PRODUCT_NAME NOT LIKE '%[0-9][0-9][0-9]' AND PRODUCT_NAME LIKE '%19' THEN 'SHOES EU SIZE 19'
								 WHEN CLEANED_BRANDNAME IN ('CROCS','CROCS CREDIT','NEW BALANCE','SKECHERS','ADDA','OSH KOSH BGOSH','OSH KOSH BGOSH PRO1','OSHKOSH','OSHKOSH PRO'
													,'TOEZONE','TOEZONE PRO','TOEZONE PRO1') AND PRODUCT_NAME LIKE '% 3' THEN 'SHOES UK SIZE 3'
								 WHEN CLEANED_BRANDNAME LIKE 'ADIDAS KIDS' AND PRODUCT_NAME NOT LIKE '%[0-9][0-9][0-9]' AND PRODUCT_NAME LIKE '%74' THEN 'CLOTHING 74 CM'
								 WHEN CLEANED_BRANDNAME LIKE 'FREDDIE THE FROG' AND PRODUCT_NAME LIKE '%5' THEN 'FREDDIE THE FROG SIZE 5'
								 WHEN CLEANED_BRANDNAME LIKE 'FREDDIE THE FROG' AND PRODUCT_NAME NOT LIKE '%[0-9]' THEN 'FREDDIE THE FROG SHOES'
								 ELSE INFANT_10_12M_KEYWORD END)
	,TODDLER_KEYWORD = (CASE WHEN CLEANED_BRANDNAME LIKE 'SKIPPON' AND PRODUCT_NAME LIKE '%13' THEN 'SHOES 13 CM'
									WHEN CLEANED_BRANDNAME LIKE 'SKIPPON' AND PRODUCT_NAME LIKE '%14' THEN 'SHOES 14 CM'
									WHEN CLEANED_BRANDNAME LIKE 'SKIPPON' AND PRODUCT_NAME LIKE '%15' THEN 'SHOES 15 CM'
									WHEN CLEANED_BRANDNAME IN ('AERA PRO','BARBIE','BARBIE SHOES','BARBIE SHOES PRO','BE FUN','DISNEY','DISNEY PRINCESS','DISNEY PRO'
													,'HELLO KITTY','HELLO KITTY PRO','HERO','HERO KIDS','HERO KIDS PRO','MICKEY MOUSE','MICKEY MOUSE PRO','SUNSANG')
													 AND PRODUCT_NAME NOT LIKE '�ͧ���������������%' AND PRODUCT_NAME LIKE '%13' THEN 'SHOES 13 CM'
									WHEN CLEANED_BRANDNAME IN ('AERA PRO','BARBIE','BARBIE SHOES','BARBIE SHOES PRO','BE FUN','DISNEY','DISNEY PRINCESS','DISNEY PRO'
													,'HELLO KITTY','HELLO KITTY PRO','HERO','HERO KIDS','HERO KIDS PRO','MICKEY MOUSE','MICKEY MOUSE PRO','SUNSANG')
													 AND PRODUCT_NAME NOT LIKE '�ͧ���������������%' AND PRODUCT_NAME LIKE '%14' THEN 'SHOES 14 CM'
									WHEN CLEANED_BRANDNAME IN ('AERA PRO','BARBIE','BARBIE SHOES','BARBIE SHOES PRO','BE FUN','DISNEY','DISNEY PRINCESS','DISNEY PRO'
													,'HELLO KITTY','HELLO KITTY PRO','HERO','HERO KIDS','HERO KIDS PRO','MICKEY MOUSE','MICKEY MOUSE PRO','SUNSANG')
													 AND PRODUCT_NAME NOT LIKE '�ͧ���������������%' AND PRODUCT_NAME LIKE '%15' THEN 'SHOES 15 CM'
									WHEN CLEANED_BRANDNAME IN ('ANGEL KIDS','BABY DIOR','BIBI','BIBI PRO','BLOCH','EMPORIO ARMANI','FILA','GOLD CITY','BREAKER'
													,'GRENDENE KIDS','GUCCI KIDS GIRL','HAVAIANAS','HAVAIANAS PRO1','IPANEMA','KENZO KIDS','KITO','KOOTS'
													,'KOU KOU','LICENSES SHOES','LICENSES SHOES PRO','LITTLE STEPS','LITTLE STEPS PRO','LULU','MIMI SHOES'
													,'MIMI SHOES PRO','MIMI SHOES PROMO','MINI MELISSA','PAUL SMITH JUNIOR','POPGIRL','POPMAN','POPTEEN'
													,'REPLAY','SALTWATER','SANRIO','SANRIO BABY','SCS','SCS BREAKER','SMALLTIME','SNAP','SPORT FLEX BIBI'
													,'SPORT FLEX BIBI PRO','SPORT KIDS','SPORTFLEX BIBI','TROISINCH','UNITED NUDE','VICTORIA')
													AND PRODUCT_NAME NOT LIKE '%[0-9][0-9][0-9]' AND PRODUCT_NAME LIKE '%20' THEN 'SHOES EU SIZE 20'
									WHEN CLEANED_BRANDNAME IN ('ANGEL KIDS','BABY DIOR','BIBI','BIBI PRO','BLOCH','EMPORIO ARMANI','FILA','GOLD CITY','BREAKER'
													,'GRENDENE KIDS','GUCCI KIDS GIRL','HAVAIANAS','HAVAIANAS PRO1','IPANEMA','KENZO KIDS','KITO','KOOTS'
													,'KOU KOU','LICENSES SHOES','LICENSES SHOES PRO','LITTLE STEPS','LITTLE STEPS PRO','LULU','MIMI SHOES'
													,'MIMI SHOES PRO','MIMI SHOES PROMO','MINI MELISSA','PAUL SMITH JUNIOR','POPGIRL','POPMAN','POPTEEN'
													,'REPLAY','SALTWATER','SANRIO','SANRIO BABY','SCS','SCS BREAKER','SMALLTIME','SNAP','SPORT FLEX BIBI'
													,'SPORT FLEX BIBI PRO','SPORT KIDS','SPORTFLEX BIBI','TROISINCH','UNITED NUDE','VICTORIA')
													AND PRODUCT_NAME NOT LIKE '%[0-9][0-9][0-9]' AND PRODUCT_NAME LIKE '%21' THEN 'SHOES EU SIZE 21'
									WHEN CLEANED_BRANDNAME IN ('ANGEL KIDS','BABY DIOR','BIBI','BIBI PRO','BLOCH','EMPORIO ARMANI','FILA','GOLD CITY','BREAKER'
													,'GRENDENE KIDS','GUCCI KIDS GIRL','HAVAIANAS','HAVAIANAS PRO1','IPANEMA','KENZO KIDS','KITO','KOOTS'
													,'KOU KOU','LICENSES SHOES','LICENSES SHOES PRO','LITTLE STEPS','LITTLE STEPS PRO','LULU','MIMI SHOES'
													,'MIMI SHOES PRO','MIMI SHOES PROMO','MINI MELISSA','PAUL SMITH JUNIOR','POPGIRL','POPMAN','POPTEEN'
													,'REPLAY','SALTWATER','SANRIO','SANRIO BABY','SCS','SCS BREAKER','SMALLTIME','SNAP','SPORT FLEX BIBI'
													,'SPORT FLEX BIBI PRO','SPORT KIDS','SPORTFLEX BIBI','TROISINCH','UNITED NUDE','VICTORIA')
													AND PRODUCT_NAME NOT LIKE '%[0-9][0-9][0-9]' AND PRODUCT_NAME LIKE '%22' THEN 'SHOES EU SIZE 22'
									WHEN CLEANED_BRANDNAME IN ('CROCS','CROCS CREDIT','NEW BALANCE','SKECHERS','ADDA','OSH KOSH BGOSH','OSH KOSH BGOSH PRO1','OSHKOSH','OSHKOSH PRO'
													,'TOEZONE','TOEZONE PRO','TOEZONE PRO1') AND PRODUCT_NAME LIKE '% 4' THEN 'SHOES UK SIZE 4'
									WHEN CLEANED_BRANDNAME IN ('CROCS','CROCS CREDIT','NEW BALANCE','SKECHERS','ADDA','OSH KOSH BGOSH','OSH KOSH BGOSH PRO1','OSHKOSH','OSHKOSH PRO'
													,'TOEZONE','TOEZONE PRO','TOEZONE PRO1') AND PRODUCT_NAME LIKE '% 5' THEN 'SHOES UK SIZE 5'
									WHEN PRODUCT_NAME LIKE '%20' THEN 'SHOES EU SIZE 20'
									WHEN PRODUCT_NAME LIKE '%21' THEN 'SHOES EU SIZE 21'
									WHEN PRODUCT_NAME LIKE '%22' AND PRODUCT_NAME NOT LIKE '%122' THEN 'SHOES EU SIZE 22'
									WHEN CLEANED_BRANDNAME LIKE 'ADIDAS KIDS' AND PRODUCT_NAME NOT LIKE '%[0-9][0-9][0-9]' AND PRODUCT_NAME LIKE '%80' THEN 'CLOTHING 80 CM'
									WHEN CLEANED_BRANDNAME LIKE 'ADIDAS KIDS' AND PRODUCT_NAME NOT LIKE '%[0-9][0-9][0-9]' AND PRODUCT_NAME LIKE '%86' THEN 'CLOTHING 86 CM'
									ELSE TODDLER_KEYWORD END)
	,INFANT_UNKNOWN_KEYWORD = (CASE WHEN CLEANED_BRANDNAME LIKE 'FOF' AND PRODUCT_NAME LIKE '�ͧ���% 1' THEN 'FOF BABY SHOES'
									WHEN CLEANED_BRANDNAME LIKE 'FOF' AND PRODUCT_NAME LIKE '�ͧ���% 2' THEN 'FOF BABY SHOES'
									WHEN CLEANED_BRANDNAME LIKE 'FOF' AND PRODUCT_NAME LIKE '�ѷ��% 2' THEN 'FOF BABY SHOES'
									WHEN CLEANED_BRANDNAME LIKE 'FOF' AND PRODUCT_NAME LIKE '�ѷ��% 3' THEN 'FOF BABY SHOES'
									ELSE INFANT_UNKNOWN_KEYWORD END)
WHERE MOM_AND_KID_PRODUCT_FLAG = 1
AND COALESCE(PREG_FLAG,'N/A') = 'N/A'
AND COALESCE(INFANT_FLAG,'N/A') = 'N/A'
AND COALESCE(TODDLER_FLAG,'N/A') = 'N/A'
AND COALESCE(PRESCHOOL_FLAG,'N/A') = 'N/A'
AND COALESCE(JUNIOR_FLAG,'N/A') = 'N/A'
AND COALESCE(TEEN_FLAG,'N/A') = 'N/A'
AND REVISED_CATEGORY_LEVEL_1 = 'KIDS'
AND REVISED_CATEGORY_LEVEL_2 = 'KID FASHION'
AND REVISED_CATEGORY_LEVEL_3 = 'KID SHOES'
AND ((CLEANED_BRANDNAME LIKE 'SKIPPON' AND (PRODUCT_NAME LIKE '%13'
									OR PRODUCT_NAME LIKE '%14'
									OR PRODUCT_NAME LIKE '%15')
	OR (CLEANED_BRANDNAME IN ('AERA PRO','BARBIE','BARBIE SHOES','BARBIE SHOES PRO','BE FUN','DISNEY','DISNEY PRINCESS','DISNEY PRO','HELLO KITTY','HELLO KITTY PRO'
					,'HERO','HERO KIDS','HERO KIDS PRO','MICKEY MOUSE','MICKEY MOUSE PRO','SUNSANG') AND PRODUCT_NAME NOT LIKE '�ͧ���������������%'
																									 AND (PRODUCT_NAME LIKE '%10'
																											OR PRODUCT_NAME LIKE '%11'
																											OR PRODUCT_NAME LIKE '%12'
																											OR PRODUCT_NAME LIKE '%13'
																											OR PRODUCT_NAME LIKE '%14'	
																											OR PRODUCT_NAME LIKE '%15')))
	OR (CLEANED_BRANDNAME IN ('ANGEL KIDS','BABY DIOR','BIBI','BIBI PRO','BLOCH','EMPORIO ARMANI','FILA','GOLD CITY','BREAKER'
					,'GRENDENE KIDS','GUCCI KIDS GIRL','HAVAIANAS','HAVAIANAS PRO1','IPANEMA','KENZO KIDS','KITO','KOOTS'
					,'KOU KOU','LICENSES SHOES','LICENSES SHOES PRO','LITTLE STEPS','LITTLE STEPS PRO','LULU','MIMI SHOES'
					,'MIMI SHOES PRO','MIMI SHOES PROMO','MINI MELISSA','PAUL SMITH JUNIOR','POPGIRL','POPMAN','POPTEEN'
					,'REPLAY','SALTWATER','SANRIO','SANRIO BABY','SCS','SCS BREAKER','SMALLTIME','SNAP','SPORT FLEX BIBI'
					,'SPORT FLEX BIBI PRO','SPORT KIDS','SPORTFLEX BIBI','TROISINCH','UNITED NUDE','VICTORIA') AND PRODUCT_NAME NOT LIKE '%[0-9][0-9][0-9]'
																													AND (PRODUCT_NAME LIKE '%15'
																													OR PRODUCT_NAME LIKE '%16'
																													OR PRODUCT_NAME LIKE '%17'
																													OR PRODUCT_NAME LIKE '%18'
																													OR PRODUCT_NAME LIKE '%19'
																													OR PRODUCT_NAME LIKE '%20'
																													OR PRODUCT_NAME LIKE '%21'
																													OR PRODUCT_NAME LIKE '%22'))
	OR (CLEANED_BRANDNAME IN ('CROCS','CROCS CREDIT','NEW BALANCE','SKECHERS','ADDA','OSH KOSH BGOSH','OSH KOSH BGOSH PRO1'
						,'OSHKOSH','OSHKOSH PRO','TOEZONE','TOEZONE PRO','TOEZONE PRO1') AND (PRODUCT_NAME LIKE '% 1'
																							OR PRODUCT_NAME LIKE '% 2'
																							OR PRODUCT_NAME LIKE '% 3'
																							OR PRODUCT_NAME LIKE '% 4'
																							OR PRODUCT_NAME LIKE '% 5'))
	OR PRODUCT_NAME LIKE '%20'
	OR PRODUCT_NAME LIKE '%21'
	OR (PRODUCT_NAME LIKE '%22' AND PRODUCT_NAME NOT LIKE '%122')
	OR (CLEANED_BRANDNAME LIKE 'ADIDAS KIDS' AND PRODUCT_NAME NOT LIKE '%[0-9][0-9][0-9]' AND PRODUCT_NAME LIKE '%74')
	OR (CLEANED_BRANDNAME LIKE 'ADIDAS KIDS' AND PRODUCT_NAME NOT LIKE '%[0-9][0-9][0-9]' AND PRODUCT_NAME LIKE '%80')
	OR (CLEANED_BRANDNAME LIKE 'ADIDAS KIDS' AND PRODUCT_NAME NOT LIKE '%[0-9][0-9][0-9]' AND PRODUCT_NAME LIKE '%86')
	OR CLEANED_BRANDNAME LIKE 'FREDDIE THE FROG'
	OR (CLEANED_BRANDNAME LIKE 'FOF' AND (PRODUCT_NAME LIKE '�ͧ���% 1'
								OR PRODUCT_NAME LIKE '�ͧ���% 2'
								OR PRODUCT_NAME LIKE '�ѷ��% 2'
								OR PRODUCT_NAME LIKE '�ѷ��% 3'))
	OR (CLEANED_BRANDNAME LIKE 'HERO' AND PRODUCT_NAME LIKE '%40-60')
	)
;

--NEW INFANT, TODDLER
UPDATE vpm_data.pm_skutagging_kids_product_master
SET INFANT_FLAG = 'YES'
	,UNKNOWN_INFANT_SUBSTAGE = 'YES'
	,TODDLER_FLAG = 'YES'
	,INFANT_UNKNOWN_KEYWORD = '�ͧ��������'
	,TODDLER_KEYWORD = '�ͧ��������'
WHERE MOM_AND_KID_PRODUCT_FLAG = 1
AND COALESCE(PREG_FLAG,'N/A') = 'N/A'
AND COALESCE(INFANT_FLAG,'N/A') = 'N/A'
AND COALESCE(TODDLER_FLAG,'N/A') = 'N/A'
AND COALESCE(PRESCHOOL_FLAG,'N/A') = 'N/A'
AND COALESCE(JUNIOR_FLAG,'N/A') = 'N/A'
AND COALESCE(TEEN_FLAG,'N/A') = 'N/A'
AND REVISED_CATEGORY_LEVEL_1 = 'KIDS'
AND REVISED_CATEGORY_LEVEL_2 = 'KID FASHION'
AND REVISED_CATEGORY_LEVEL_3 = 'KID SHOES'
AND ((CLEANED_BRANDNAME IN ('AERA PRO','BARBIE','BARBIE SHOES','BARBIE SHOES PRO','BE FUN','DISNEY','DISNEY PRINCESS','DISNEY PRO','HELLO KITTY','HELLO KITTY PRO'
					,'HERO','HERO KIDS','HERO KIDS PRO','MICKEY MOUSE','MICKEY MOUSE PRO','SUNSANG') AND PRODUCT_NAME NOT LIKE '%[0-9][0-9][0-9]'
																									 AND (PRODUCT_NAME LIKE '%16'
																											OR PRODUCT_NAME LIKE '%17'
																											OR PRODUCT_NAME LIKE '%18'
																											OR PRODUCT_NAME LIKE '%19'
																											OR PRODUCT_NAME LIKE '%20'
																											OR PRODUCT_NAME LIKE '%21'
																											OR PRODUCT_NAME LIKE '%22'))
	)
;

--NEW TODDLER
UPDATE vpm_data.pm_skutagging_kids_product_master
SET TODDLER_FLAG = 'YES'
	,TODDLER_KEYWORD = (CASE WHEN PRODUCT_NAME LIKE '����ͼ��%110' THEN 'CLOTHING 110'
							 WHEN CLEANED_BRANDNAME LIKE 'SKIPPON' AND PRODUCT_NAME LIKE '%16' THEN 'SHOES 16 CM'
							 WHEN CLEANED_BRANDNAME LIKE 'SKIPPON' AND PRODUCT_NAME LIKE '%17' THEN 'SHOES 17 CM'
							 WHEN CLEANED_BRANDNAME LIKE 'SKIPPON' AND PRODUCT_NAME LIKE '%18' THEN 'SHOES 18 CM'
							 WHEN CLEANED_BRANDNAME LIKE 'BESSON JOUJOU' AND PRODUCT_NAME LIKE '%160' THEN 'SHOES 16 CM'
							 WHEN CLEANED_BRANDNAME LIKE 'BESSON JOUJOU' AND PRODUCT_NAME LIKE '%170' THEN 'SHOES 17 CM'
							 WHEN CLEANED_BRANDNAME LIKE 'BESSON JOUJOU' AND PRODUCT_NAME LIKE '%180' THEN 'SHOES 18 CM'
							 WHEN CLEANED_BRANDNAME IN ('ANGEL KIDS','BABY DIOR','BIBI','BIBI PRO','BLOCH','EMPORIO ARMANI','FILA','GOLD CITY','BREAKER'
												,'GRENDENE KIDS','GUCCI KIDS GIRL','HAVAIANAS','HAVAIANAS PRO1','IPANEMA','KENZO KIDS','KITO','KOOTS'
												,'KOU KOU','LICENSES SHOES','LICENSES SHOES PRO','LITTLE STEPS','LITTLE STEPS PRO','LULU','MIMI SHOES'
												,'MIMI SHOES PRO','MIMI SHOES PROMO','MINI MELISSA','PAUL SMITH JUNIOR','POPGIRL','POPMAN','POPTEEN'
												,'REPLAY','SALTWATER','SANRIO','SANRIO BABY','SCS','SCS BREAKER','SMALLTIME','SNAP','SPORT FLEX BIBI'
												,'SPORT FLEX BIBI PRO','SPORT KIDS','SPORTFLEX BIBI','TROISINCH','UNITED NUDE','VICTORIA','AERA PRO'
												,'BARBIE','BARBIE SHOES','BARBIE SHOES PRO','BE FUN','DISNEY','DISNEY PRINCESS','DISNEY PRO','HELLO KITTY'
												,'HELLO KITTY PRO','HERO','HERO KIDS','HERO KIDS PRO','MICKEY MOUSE','MICKEY MOUSE PRO','SUNSANG') 
												AND PRODUCT_NAME NOT LIKE '%[0-9][0-9][0-9]' AND PRODUCT_NAME LIKE '%23' THEN 'SHOES EU SIZE 23'
							 WHEN CLEANED_BRANDNAME IN ('ANGEL KIDS','BABY DIOR','BIBI','BIBI PRO','BLOCH','EMPORIO ARMANI','FILA','GOLD CITY','BREAKER'
												,'GRENDENE KIDS','GUCCI KIDS GIRL','HAVAIANAS','HAVAIANAS PRO1','IPANEMA','KENZO KIDS','KITO','KOOTS'
												,'KOU KOU','LICENSES SHOES','LICENSES SHOES PRO','LITTLE STEPS','LITTLE STEPS PRO','LULU','MIMI SHOES'
												,'MIMI SHOES PRO','MIMI SHOES PROMO','MINI MELISSA','PAUL SMITH JUNIOR','POPGIRL','POPMAN','POPTEEN'
												,'REPLAY','SALTWATER','SANRIO','SANRIO BABY','SCS','SCS BREAKER','SMALLTIME','SNAP','SPORT FLEX BIBI'
												,'SPORT FLEX BIBI PRO','SPORT KIDS','SPORTFLEX BIBI','TROISINCH','UNITED NUDE','VICTORIA','AERA PRO'
												,'BARBIE','BARBIE SHOES','BARBIE SHOES PRO','BE FUN','DISNEY','DISNEY PRINCESS','DISNEY PRO','HELLO KITTY'
												,'HELLO KITTY PRO','HERO','HERO KIDS','HERO KIDS PRO','MICKEY MOUSE','MICKEY MOUSE PRO','SUNSANG') 
												AND PRODUCT_NAME NOT LIKE '%[0-9][0-9][0-9]' AND PRODUCT_NAME LIKE '%24' THEN 'SHOES EU SIZE 24'
							 WHEN CLEANED_BRANDNAME IN ('ANGEL KIDS','BABY DIOR','BIBI','BIBI PRO','BLOCH','EMPORIO ARMANI','FILA','GOLD CITY','BREAKER'
												,'GRENDENE KIDS','GUCCI KIDS GIRL','HAVAIANAS','HAVAIANAS PRO1','IPANEMA','KENZO KIDS','KITO','KOOTS'
												,'KOU KOU','LICENSES SHOES','LICENSES SHOES PRO','LITTLE STEPS','LITTLE STEPS PRO','LULU','MIMI SHOES'
												,'MIMI SHOES PRO','MIMI SHOES PROMO','MINI MELISSA','PAUL SMITH JUNIOR','POPGIRL','POPMAN','POPTEEN'
												,'REPLAY','SALTWATER','SANRIO','SANRIO BABY','SCS','SCS BREAKER','SMALLTIME','SNAP','SPORT FLEX BIBI'
												,'SPORT FLEX BIBI PRO','SPORT KIDS','SPORTFLEX BIBI','TROISINCH','UNITED NUDE','VICTORIA','AERA PRO'
												,'BARBIE','BARBIE SHOES','BARBIE SHOES PRO','BE FUN','DISNEY','DISNEY PRINCESS','DISNEY PRO','HELLO KITTY'
												,'HELLO KITTY PRO','HERO','HERO KIDS','HERO KIDS PRO','MICKEY MOUSE','MICKEY MOUSE PRO','SUNSANG') 
												AND PRODUCT_NAME NOT LIKE '%[0-9][0-9][0-9]' AND PRODUCT_NAME LIKE '%25' THEN 'SHOES EU SIZE 25'
							 WHEN CLEANED_BRANDNAME IN ('ANGEL KIDS','BABY DIOR','BIBI','BIBI PRO','BLOCH','EMPORIO ARMANI','FILA','GOLD CITY','BREAKER'
												,'GRENDENE KIDS','GUCCI KIDS GIRL','HAVAIANAS','HAVAIANAS PRO1','IPANEMA','KENZO KIDS','KITO','KOOTS'
												,'KOU KOU','LICENSES SHOES','LICENSES SHOES PRO','LITTLE STEPS','LITTLE STEPS PRO','LULU','MIMI SHOES'
												,'MIMI SHOES PRO','MIMI SHOES PROMO','MINI MELISSA','PAUL SMITH JUNIOR','POPGIRL','POPMAN','POPTEEN'
												,'REPLAY','SALTWATER','SANRIO','SANRIO BABY','SCS','SCS BREAKER','SMALLTIME','SNAP','SPORT FLEX BIBI'
												,'SPORT FLEX BIBI PRO','SPORT KIDS','SPORTFLEX BIBI','TROISINCH','UNITED NUDE','VICTORIA','AERA PRO'
												,'BARBIE','BARBIE SHOES','BARBIE SHOES PRO','BE FUN','DISNEY','DISNEY PRINCESS','DISNEY PRO','HELLO KITTY'
												,'HELLO KITTY PRO','HERO','HERO KIDS','HERO KIDS PRO','MICKEY MOUSE','MICKEY MOUSE PRO','SUNSANG') 
												AND PRODUCT_NAME NOT LIKE '%[0-9][0-9][0-9]' AND PRODUCT_NAME LIKE '%26' THEN 'SHOES EU SIZE 26'
							 WHEN CLEANED_BRANDNAME IN ('ANGEL KIDS','BABY DIOR','BIBI','BIBI PRO','BLOCH','EMPORIO ARMANI','FILA','GOLD CITY','BREAKER'
												,'GRENDENE KIDS','GUCCI KIDS GIRL','HAVAIANAS','HAVAIANAS PRO1','IPANEMA','KENZO KIDS','KITO','KOOTS'
												,'KOU KOU','LICENSES SHOES','LICENSES SHOES PRO','LITTLE STEPS','LITTLE STEPS PRO','LULU','MIMI SHOES'
												,'MIMI SHOES PRO','MIMI SHOES PROMO','MINI MELISSA','PAUL SMITH JUNIOR','POPGIRL','POPMAN','POPTEEN'
												,'REPLAY','SALTWATER','SANRIO','SANRIO BABY','SCS','SCS BREAKER','SMALLTIME','SNAP','SPORT FLEX BIBI'
												,'SPORT FLEX BIBI PRO','SPORT KIDS','SPORTFLEX BIBI','TROISINCH','UNITED NUDE','VICTORIA','AERA PRO'
												,'BARBIE','BARBIE SHOES','BARBIE SHOES PRO','BE FUN','DISNEY','DISNEY PRINCESS','DISNEY PRO','HELLO KITTY'
												,'HELLO KITTY PRO','HERO','HERO KIDS','HERO KIDS PRO','MICKEY MOUSE','MICKEY MOUSE PRO','SUNSANG') 
												AND PRODUCT_NAME NOT LIKE '%[0-9][0-9][0-9]' AND PRODUCT_NAME LIKE '%27' THEN 'SHOES EU SIZE 27'
							 WHEN CLEANED_BRANDNAME LIKE 'HERO%' AND PRODUCT_NAME LIKE '�ͧ���������������%10' THEN '�ͧ�������� HERO'
							 WHEN CLEANED_BRANDNAME LIKE 'HERO%' AND PRODUCT_NAME LIKE '�ͧ���������������%11' THEN '�ͧ�������� HERO'
							 WHEN CLEANED_BRANDNAME LIKE 'HERO%' AND PRODUCT_NAME LIKE '�ͧ���������������%12' THEN '�ͧ�������� HERO'
							 WHEN CLEANED_BRANDNAME LIKE 'HERO%' AND PRODUCT_NAME LIKE '�ͧ���������������%13' THEN '�ͧ�������� HERO'
							 WHEN CLEANED_BRANDNAME IN ('CROCS','CROCS CREDIT','NEW BALANCE','SKECHERS','ADDA','OSH KOSH BGOSH','OSH KOSH BGOSH PRO1'
												,'OSHKOSH','OSHKOSH PRO','TOEZONE','TOEZONE PRO','TOEZONE PRO1') AND PRODUCT_NAME LIKE '% 6' THEN 'SHOES US SIZE 6'
							 WHEN CLEANED_BRANDNAME IN ('CROCS','CROCS CREDIT','NEW BALANCE','SKECHERS','ADDA','OSH KOSH BGOSH','OSH KOSH BGOSH PRO1'
												,'OSHKOSH','OSHKOSH PRO','TOEZONE','TOEZONE PRO','TOEZONE PRO1') AND PRODUCT_NAME LIKE '% 7' THEN 'SHOES US SIZE 7'
							 WHEN CLEANED_BRANDNAME IN ('CROCS','CROCS CREDIT','NEW BALANCE','SKECHERS','ADDA','OSH KOSH BGOSH','OSH KOSH BGOSH PRO1'
												,'OSHKOSH','OSHKOSH PRO','TOEZONE','TOEZONE PRO','TOEZONE PRO1') AND PRODUCT_NAME LIKE '% 8' THEN 'SHOES US SIZE 8'
							 WHEN CLEANED_BRANDNAME IN ('CROCS','CROCS CREDIT','NEW BALANCE','SKECHERS','ADDA','OSH KOSH BGOSH','OSH KOSH BGOSH PRO1'
												,'OSHKOSH','OSHKOSH PRO','TOEZONE','TOEZONE PRO','TOEZONE PRO1') AND PRODUCT_NAME LIKE '% 9' THEN 'SHOES US SIZE 9'
							 WHEN PRODUCT_NAME LIKE '%23' THEN 'EU SIZE 23'
							 WHEN PRODUCT_NAME LIKE '%24' THEN 'EU SIZE 24'
							 WHEN PRODUCT_NAME LIKE '%25' THEN 'EU SIZE 25'
							 WHEN PRODUCT_NAME LIKE '%26' THEN 'EU SIZE 26'
							 WHEN PRODUCT_NAME LIKE '%27' THEN 'EU SIZE 27'
							 WHEN CLEANED_BRANDNAME LIKE 'ADIDAS KIDS' AND PRODUCT_NAME NOT LIKE '%[0-9][0-9][0-9]' AND PRODUCT_NAME LIKE '%92' THEN 'CLOTHING 92 CM'
							 WHEN CLEANED_BRANDNAME LIKE 'ADIDAS KIDS' AND PRODUCT_NAME NOT LIKE '%[0-9][0-9][0-9]' AND PRODUCT_NAME LIKE '%98' THEN 'CLOTHING 98 CM'
							 WHEN CLEANED_BRANDNAME LIKE 'MUFFIN BITE PRO1' AND PRODUCT_NAME LIKE '% 3' THEN '3Y'
							 ELSE TODDLER_KEYWORD END)
WHERE MOM_AND_KID_PRODUCT_FLAG = 1
AND COALESCE(PREG_FLAG,'N/A') = 'N/A'
AND COALESCE(INFANT_FLAG,'N/A') = 'N/A'
AND COALESCE(TODDLER_FLAG,'N/A') = 'N/A'
AND COALESCE(PRESCHOOL_FLAG,'N/A') = 'N/A'
AND COALESCE(JUNIOR_FLAG,'N/A') = 'N/A'
AND COALESCE(TEEN_FLAG,'N/A') = 'N/A'
AND REVISED_CATEGORY_LEVEL_1 = 'KIDS'
AND REVISED_CATEGORY_LEVEL_2 = 'KID FASHION'
AND REVISED_CATEGORY_LEVEL_3 = 'KID SHOES'
AND (PRODUCT_NAME LIKE '����ͼ��%110'
	OR (CLEANED_BRANDNAME LIKE 'SKIPPON' AND (PRODUCT_NAME LIKE '%16'
									OR PRODUCT_NAME LIKE '%17'
									OR PRODUCT_NAME LIKE '%18'))
	OR (CLEANED_BRANDNAME LIKE 'BESSON JOUJOU' AND (PRODUCT_NAME LIKE '%160'
											OR PRODUCT_NAME LIKE '%170'
											OR PRODUCT_NAME LIKE '%180'))
	OR (CLEANED_BRANDNAME IN ('ANGEL KIDS','BABY DIOR','BIBI','BIBI PRO','BLOCH','EMPORIO ARMANI','FILA','GOLD CITY','BREAKER'
					,'GRENDENE KIDS','GUCCI KIDS GIRL','HAVAIANAS','HAVAIANAS PRO1','IPANEMA','KENZO KIDS','KITO','KOOTS'
					,'KOU KOU','LICENSES SHOES','LICENSES SHOES PRO','LITTLE STEPS','LITTLE STEPS PRO','LULU','MIMI SHOES'
					,'MIMI SHOES PRO','MIMI SHOES PROMO','MINI MELISSA','PAUL SMITH JUNIOR','POPGIRL','POPMAN','POPTEEN'
					,'REPLAY','SALTWATER','SANRIO','SANRIO BABY','SCS','SCS BREAKER','SMALLTIME','SNAP','SPORT FLEX BIBI'
					,'SPORT FLEX BIBI PRO','SPORT KIDS','SPORTFLEX BIBI','TROISINCH','UNITED NUDE','VICTORIA','AERA PRO'
					,'BARBIE','BARBIE SHOES','BARBIE SHOES PRO','BE FUN','DISNEY','DISNEY PRINCESS','DISNEY PRO','HELLO KITTY'
					,'HELLO KITTY PRO','HERO','HERO KIDS','HERO KIDS PRO','MICKEY MOUSE','MICKEY MOUSE PRO','SUNSANG') AND (PRODUCT_NAME LIKE '%23'
																															OR PRODUCT_NAME LIKE '%24'
																															OR PRODUCT_NAME LIKE '%25'
																															OR PRODUCT_NAME LIKE '%26'
																															OR PRODUCT_NAME LIKE '%27')
																													   AND PRODUCT_NAME NOT LIKE '%[0-9][0-9][0-9]')
	OR (CLEANED_BRANDNAME LIKE 'HERO%' AND (PRODUCT_NAME LIKE '�ͧ���������������%10'
									OR PRODUCT_NAME LIKE '�ͧ���������������%11'
									OR PRODUCT_NAME LIKE '�ͧ���������������%12'
									OR PRODUCT_NAME LIKE '�ͧ���������������%13'))
	OR (CLEANED_BRANDNAME IN ('CROCS','CROCS CREDIT','NEW BALANCE','SKECHERS','ADDA','OSH KOSH BGOSH','OSH KOSH BGOSH PRO1'
						,'OSHKOSH','OSHKOSH PRO','TOEZONE','TOEZONE PRO','TOEZONE PRO1') AND (PRODUCT_NAME LIKE '% 6'
																							OR PRODUCT_NAME LIKE '% 7'
																							OR PRODUCT_NAME LIKE '% 8'
																							OR PRODUCT_NAME LIKE '% 9'))
	OR PRODUCT_NAME LIKE '%23'
	OR PRODUCT_NAME LIKE '%24'
	OR PRODUCT_NAME LIKE '%25'
	OR PRODUCT_NAME LIKE '%26'
	OR PRODUCT_NAME LIKE '%27'
	OR (CLEANED_BRANDNAME LIKE 'ADIDAS KIDS' AND PRODUCT_NAME NOT LIKE '%[0-9][0-9][0-9]' AND PRODUCT_NAME LIKE '%92')
	OR (CLEANED_BRANDNAME LIKE 'ADIDAS KIDS' AND PRODUCT_NAME NOT LIKE '%[0-9][0-9][0-9]' AND PRODUCT_NAME LIKE '%98')
	OR (CLEANED_BRANDNAME LIKE 'MUFFIN BITE PRO1' AND PRODUCT_NAME LIKE '% 3')
	)
;

--PRESCHOOL
UPDATE vpm_data.pm_skutagging_kids_product_master
SET PRESCHOOL_FLAG = 'YES'
	,PRESCHOOL_KEYWORD = (CASE WHEN PRODUCT_NAME LIKE '%͹غ��%' THEN '͹غ��'
							   WHEN CLEANED_BRANDNAME = 'BESSON JOUJOU' AND PRODUCT_NAME LIKE '%190' THEN 'CLOTHING 190 CM'
							   WHEN CLEANED_BRANDNAME = 'BESSON JOUJOU' AND PRODUCT_NAME LIKE '%200' THEN 'CLOTHING 200 CM'
							   WHEN CLEANED_BRANDNAME = 'BESSON JOUJOU' AND PRODUCT_NAME LIKE '%210' THEN 'CLOTHING 210 CM'
							   WHEN CLEANED_BRANDNAME IN ('ANGEL KIDS','BABY DIOR','BIBI','BIBI PRO','BLOCH','EMPORIO ARMANI','FILA','GOLD CITY','BREAKER'
												,'GRENDENE KIDS','GUCCI KIDS GIRL','HAVAIANAS','HAVAIANAS PRO1','IPANEMA','KENZO KIDS','KITO','KOOTS'
												,'KOU KOU','LICENSES SHOES','LICENSES SHOES PRO','LITTLE STEPS','LITTLE STEPS PRO','LULU','MIMI SHOES'
												,'MIMI SHOES PRO','MIMI SHOES PROMO','MINI MELISSA','PAUL SMITH JUNIOR','POPGIRL','POPMAN','POPTEEN'
												,'REPLAY','SALTWATER','SANRIO','SANRIO BABY','SCS','SCS BREAKER','SMALLTIME','SNAP','SPORT FLEX BIBI'
												,'SPORT FLEX BIBI PRO','SPORT KIDS','SPORTFLEX BIBI','TROISINCH','UNITED NUDE','VICTORIA','AERA PRO'
												,'BARBIE','BARBIE SHOES','BARBIE SHOES PRO','BE FUN','DISNEY','DISNEY PRINCESS','DISNEY PRO','HELLO KITTY'
												,'HELLO KITTY PRO','HERO','HERO KIDS','HERO KIDS PRO','MICKEY MOUSE','MICKEY MOUSE PRO','SUNSANG') 
												AND PRODUCT_NAME NOT LIKE '%[0-9][0-9][0-9]' AND PRODUCT_NAME LIKE '%28' THEN 'SHOES EU SIZE 28'
							   WHEN CLEANED_BRANDNAME IN ('ANGEL KIDS','BABY DIOR','BIBI','BIBI PRO','BLOCH','EMPORIO ARMANI','FILA','GOLD CITY','BREAKER'
												,'GRENDENE KIDS','GUCCI KIDS GIRL','HAVAIANAS','HAVAIANAS PRO1','IPANEMA','KENZO KIDS','KITO','KOOTS'
												,'KOU KOU','LICENSES SHOES','LICENSES SHOES PRO','LITTLE STEPS','LITTLE STEPS PRO','LULU','MIMI SHOES'
												,'MIMI SHOES PRO','MIMI SHOES PROMO','MINI MELISSA','PAUL SMITH JUNIOR','POPGIRL','POPMAN','POPTEEN'
												,'REPLAY','SALTWATER','SANRIO','SANRIO BABY','SCS','SCS BREAKER','SMALLTIME','SNAP','SPORT FLEX BIBI'
												,'SPORT FLEX BIBI PRO','SPORT KIDS','SPORTFLEX BIBI','TROISINCH','UNITED NUDE','VICTORIA','AERA PRO'
												,'BARBIE','BARBIE SHOES','BARBIE SHOES PRO','BE FUN','DISNEY','DISNEY PRINCESS','DISNEY PRO','HELLO KITTY'
												,'HELLO KITTY PRO','HERO','HERO KIDS','HERO KIDS PRO','MICKEY MOUSE','MICKEY MOUSE PRO','SUNSANG') 
												AND PRODUCT_NAME NOT LIKE '%[0-9][0-9][0-9]' AND PRODUCT_NAME LIKE '%29' THEN 'SHOES EU SIZE 29'
							   WHEN CLEANED_BRANDNAME IN ('ANGEL KIDS','BABY DIOR','BIBI','BIBI PRO','BLOCH','EMPORIO ARMANI','FILA','GOLD CITY','BREAKER'
												,'GRENDENE KIDS','GUCCI KIDS GIRL','HAVAIANAS','HAVAIANAS PRO1','IPANEMA','KENZO KIDS','KITO','KOOTS'
												,'KOU KOU','LICENSES SHOES','LICENSES SHOES PRO','LITTLE STEPS','LITTLE STEPS PRO','LULU','MIMI SHOES'
												,'MIMI SHOES PRO','MIMI SHOES PROMO','MINI MELISSA','PAUL SMITH JUNIOR','POPGIRL','POPMAN','POPTEEN'
												,'REPLAY','SALTWATER','SANRIO','SANRIO BABY','SCS','SCS BREAKER','SMALLTIME','SNAP','SPORT FLEX BIBI'
												,'SPORT FLEX BIBI PRO','SPORT KIDS','SPORTFLEX BIBI','TROISINCH','UNITED NUDE','VICTORIA','AERA PRO'
												,'BARBIE','BARBIE SHOES','BARBIE SHOES PRO','BE FUN','DISNEY','DISNEY PRINCESS','DISNEY PRO','HELLO KITTY'
												,'HELLO KITTY PRO','HERO','HERO KIDS','HERO KIDS PRO','MICKEY MOUSE','MICKEY MOUSE PRO','SUNSANG') 
												AND PRODUCT_NAME NOT LIKE '%[0-9][0-9][0-9]' AND PRODUCT_NAME LIKE '%30' THEN 'SHOES EU SIZE 30'
							   WHEN CLEANED_BRANDNAME IN ('ANGEL KIDS','BABY DIOR','BIBI','BIBI PRO','BLOCH','EMPORIO ARMANI','FILA','GOLD CITY','BREAKER'
												,'GRENDENE KIDS','GUCCI KIDS GIRL','HAVAIANAS','HAVAIANAS PRO1','IPANEMA','KENZO KIDS','KITO','KOOTS'
												,'KOU KOU','LICENSES SHOES','LICENSES SHOES PRO','LITTLE STEPS','LITTLE STEPS PRO','LULU','MIMI SHOES'
												,'MIMI SHOES PRO','MIMI SHOES PROMO','MINI MELISSA','PAUL SMITH JUNIOR','POPGIRL','POPMAN','POPTEEN'
												,'REPLAY','SALTWATER','SANRIO','SANRIO BABY','SCS','SCS BREAKER','SMALLTIME','SNAP','SPORT FLEX BIBI'
												,'SPORT FLEX BIBI PRO','SPORT KIDS','SPORTFLEX BIBI','TROISINCH','UNITED NUDE','VICTORIA','AERA PRO'
												,'BARBIE','BARBIE SHOES','BARBIE SHOES PRO','BE FUN','DISNEY','DISNEY PRINCESS','DISNEY PRO','HELLO KITTY'
												,'HELLO KITTY PRO','HERO','HERO KIDS','HERO KIDS PRO','MICKEY MOUSE','MICKEY MOUSE PRO','SUNSANG') 
												AND PRODUCT_NAME NOT LIKE '%[0-9][0-9][0-9]' AND PRODUCT_NAME LIKE '%31' THEN 'SHOES EU SIZE 31'
							   WHEN CLEANED_BRANDNAME = 'SALTWATER' AND PRODUCT_NAME LIKE '%C11' THEN 'SALTWATER SIZE C11'
							   WHEN CLEANED_BRANDNAME IN ('CROCS','CROCS CREDIT','NEW BALANCE','SKECHERS','ADDA','OSH KOSH BGOSH','OSH KOSH BGOSH PRO1'
												,'OSHKOSH','OSHKOSH PRO','TOEZONE','TOEZONE PRO','TOEZONE PRO1') AND PRODUCT_NAME LIKE '%10' THEN 'SHOES US SIZE 10'
							   WHEN CLEANED_BRANDNAME IN ('CROCS','CROCS CREDIT','NEW BALANCE','SKECHERS','ADDA','OSH KOSH BGOSH','OSH KOSH BGOSH PRO1'
												,'OSHKOSH','OSHKOSH PRO','TOEZONE','TOEZONE PRO','TOEZONE PRO1') AND PRODUCT_NAME LIKE '%11' THEN 'SHOES US SIZE 11'
							   WHEN CLEANED_BRANDNAME IN ('CROCS','CROCS CREDIT','NEW BALANCE','SKECHERS','ADDA','OSH KOSH BGOSH','OSH KOSH BGOSH PRO1'
												,'OSHKOSH','OSHKOSH PRO','TOEZONE','TOEZONE PRO','TOEZONE PRO1') AND PRODUCT_NAME LIKE '%12' THEN 'SHOES US SIZE 12'
							   WHEN CLEANED_BRANDNAME IN ('CROCS','CROCS CREDIT','NEW BALANCE','SKECHERS','ADDA','OSH KOSH BGOSH','OSH KOSH BGOSH PRO1'
												,'OSHKOSH','OSHKOSH PRO','TOEZONE','TOEZONE PRO','TOEZONE PRO1') AND PRODUCT_NAME LIKE '%13' THEN 'SHOES US SIZE 13'
							   WHEN PRODUCT_NAME LIKE '%28' THEN 'SHOES EU SIZE 28'
							   WHEN PRODUCT_NAME LIKE '%29' THEN 'SHOES EU SIZE 29'
							   WHEN PRODUCT_NAME LIKE '%30' THEN 'SHOES EU SIZE 30'
							   WHEN PRODUCT_NAME LIKE '%31' THEN 'SHOES EU SIZE 31'
							   WHEN CLEANED_BRANDNAME LIKE 'ADIDAS KIDS'AND PRODUCT_NAME LIKE '%104' THEN 'CLOTHING 104'
							   WHEN CLEANED_BRANDNAME LIKE 'ADIDAS KIDS'AND PRODUCT_NAME LIKE '%110' THEN 'CLOTHING 110'
							   WHEN CLEANED_BRANDNAME LIKE 'ADIDAS KIDS'AND PRODUCT_NAME LIKE '%116' THEN 'CLOTHING 116'
							   WHEN CLEANED_BRANDNAME LIKE 'ADIDAS KIDS'AND PRODUCT_NAME LIKE '%122' THEN 'CLOTHING 122'
							   WHEN PRODUCT_NAME LIKE '%�����%' THEN '�����'
							   ELSE PRESCHOOL_KEYWORD END)
WHERE MOM_AND_KID_PRODUCT_FLAG = 1
AND COALESCE(PREG_FLAG,'N/A') = 'N/A'
AND COALESCE(INFANT_FLAG,'N/A') = 'N/A'
AND COALESCE(TODDLER_FLAG,'N/A') = 'N/A'
AND COALESCE(PRESCHOOL_FLAG,'N/A') = 'N/A'
AND COALESCE(JUNIOR_FLAG,'N/A') = 'N/A'
AND COALESCE(TEEN_FLAG,'N/A') = 'N/A'
AND REVISED_CATEGORY_LEVEL_1 = 'KIDS'
AND REVISED_CATEGORY_LEVEL_2 = 'KID FASHION'
AND REVISED_CATEGORY_LEVEL_3 = 'KID SHOES'
AND (PRODUCT_NAME LIKE '%͹غ��%'
	OR (CLEANED_BRANDNAME = 'BESSON JOUJOU' AND (PRODUCT_NAME LIKE '%190'
										OR PRODUCT_NAME LIKE '%200'
										OR PRODUCT_NAME LIKE '%210'))
	OR (CLEANED_BRANDNAME IN ('ANGEL KIDS','BABY DIOR','BIBI','BIBI PRO','BLOCH','EMPORIO ARMANI','FILA','GOLD CITY','BREAKER'
					,'GRENDENE KIDS','GUCCI KIDS GIRL','HAVAIANAS','HAVAIANAS PRO1','IPANEMA','KENZO KIDS','KITO','KOOTS'
					,'KOU KOU','LICENSES SHOES','LICENSES SHOES PRO','LITTLE STEPS','LITTLE STEPS PRO','LULU','MIMI SHOES'
					,'MIMI SHOES PRO','MIMI SHOES PROMO','MINI MELISSA','PAUL SMITH JUNIOR','POPGIRL','POPMAN','POPTEEN'
					,'REPLAY','SALTWATER','SANRIO','SANRIO BABY','SCS','SCS BREAKER','SMALLTIME','SNAP','SPORT FLEX BIBI'
					,'SPORT FLEX BIBI PRO','SPORT KIDS','SPORTFLEX BIBI','TROISINCH','UNITED NUDE','VICTORIA','AERA PRO'
					,'BARBIE','BARBIE SHOES','BARBIE SHOES PRO','BE FUN','DISNEY','DISNEY PRINCESS','DISNEY PRO','HELLO KITTY'
					,'HELLO KITTY PRO','HERO','HERO KIDS','HERO KIDS PRO','MICKEY MOUSE','MICKEY MOUSE PRO','SUNSANG') AND (PRODUCT_NAME LIKE '%28'
																															OR PRODUCT_NAME LIKE '%29'
																															OR PRODUCT_NAME LIKE '%30'
																															OR PRODUCT_NAME LIKE '%31')
																													   AND PRODUCT_NAME NOT LIKE '%[0-9][0-9][0-9]')
	OR (CLEANED_BRANDNAME = 'SALTWATER' AND PRODUCT_NAME LIKE '%C11')
	OR (CLEANED_BRANDNAME IN ('CROCS','CROCS CREDIT','NEW BALANCE','SKECHERS','ADDA','OSH KOSH BGOSH','OSH KOSH BGOSH PRO1'
						,'OSHKOSH','OSHKOSH PRO','TOEZONE','TOEZONE PRO','TOEZONE PRO1') AND (PRODUCT_NAME LIKE '%10'
																							OR PRODUCT_NAME LIKE '%11'
																							OR PRODUCT_NAME LIKE '%12'
																							OR PRODUCT_NAME LIKE '%13'))
	
	OR PRODUCT_NAME LIKE '%28'
	OR PRODUCT_NAME LIKE '%29'
	OR PRODUCT_NAME LIKE '%30'
	OR PRODUCT_NAME LIKE '%31'
	OR (CLEANED_BRANDNAME LIKE 'ADIDAS KIDS'AND PRODUCT_NAME LIKE '%104')
	OR (CLEANED_BRANDNAME LIKE 'ADIDAS KIDS'AND PRODUCT_NAME LIKE '%110')
	OR (CLEANED_BRANDNAME LIKE 'ADIDAS KIDS'AND PRODUCT_NAME LIKE '%116')
	OR (CLEANED_BRANDNAME LIKE 'ADIDAS KIDS'AND PRODUCT_NAME LIKE '%122')
	OR PRODUCT_NAME LIKE '%�����%'
	)
;

--PRESCHOOL/JUNIOR
UPDATE vpm_data.pm_skutagging_kids_product_master
SET PRESCHOOL_FLAG = 'YES'
	,JUNIOR_FLAG = 'YES'
	,PRESCHOOL_KEYWORD = '�ͧ�����'
	,JUNIOR_KEYWORD = '�ͧ�����'
WHERE MOM_AND_KID_PRODUCT_FLAG = 1
AND COALESCE(PREG_FLAG,'N/A') = 'N/A'
AND COALESCE(INFANT_FLAG,'N/A') = 'N/A'
AND COALESCE(TODDLER_FLAG,'N/A') = 'N/A'
AND COALESCE(PRESCHOOL_FLAG,'N/A') = 'N/A'
AND COALESCE(JUNIOR_FLAG,'N/A') = 'N/A'
AND COALESCE(TEEN_FLAG,'N/A') = 'N/A'
AND REVISED_CATEGORY_LEVEL_1 = 'KIDS'
AND REVISED_CATEGORY_LEVEL_2 = 'KID FASHION'
AND REVISED_CATEGORY_LEVEL_3 = 'KID SHOES'
AND (CLEANED_BRANDNAME = 'BIRKENSTOCK' AND PRODUCT_NAME LIKE '%�ͧ�����%')
;

--PRESCHOOL/JUNIOR/TEEN
UPDATE vpm_data.pm_skutagging_kids_product_master
SET PRESCHOOL_FLAG = 'YES'
	,JUNIOR_FLAG = 'YES'
	,TEEN_FLAG = 'YES'
	,PRESCHOOL_KEYWORD = (CASE WHEN PRODUCT_NAME LIKE '%����ͼ��%' THEN '����ͼ��' ELSE '�ͧ�����' END)
	,JUNIOR_KEYWORD = (CASE WHEN PRODUCT_NAME LIKE '%����ͼ��%' THEN '����ͼ��' ELSE '�ͧ�����' END)
	,TEEN_KEYWORD = (CASE WHEN PRODUCT_NAME LIKE '%����ͼ��%' THEN '����ͼ��' ELSE '�ͧ�����' END)
WHERE MOM_AND_KID_PRODUCT_FLAG = 1
AND COALESCE(PREG_FLAG,'N/A') = 'N/A'
AND COALESCE(INFANT_FLAG,'N/A') = 'N/A'
AND COALESCE(TODDLER_FLAG,'N/A') = 'N/A'
AND COALESCE(PRESCHOOL_FLAG,'N/A') = 'N/A'
AND COALESCE(JUNIOR_FLAG,'N/A') = 'N/A'
AND COALESCE(TEEN_FLAG,'N/A') = 'N/A'
AND REVISED_CATEGORY_LEVEL_1 = 'KIDS'
AND REVISED_CATEGORY_LEVEL_2 = 'KID FASHION'
AND REVISED_CATEGORY_LEVEL_3 = 'KID SHOES'
AND ((CLEANED_BRANDNAME IN ('ANGEL KIDS','BABY DIOR','BIBI','BIBI PRO','BLOCH','EMPORIO ARMANI','FILA','GOLD CITY','BREAKER'
					,'GRENDENE KIDS','GUCCI KIDS GIRL','HAVAIANAS','HAVAIANAS PRO1','IPANEMA','KENZO KIDS','KITO','KOOTS'
					,'KOU KOU','LICENSES SHOES','LICENSES SHOES PRO','LITTLE STEPS','LITTLE STEPS PRO','LULU','MIMI SHOES'
					,'MIMI SHOES PRO','MIMI SHOES PROMO','MINI MELISSA','PAUL SMITH JUNIOR','POPGIRL','POPMAN','POPTEEN'
					,'REPLAY','SALTWATER','SANRIO','SANRIO BABY','SCS','SCS BREAKER','SMALLTIME','SNAP','SPORT FLEX BIBI'
					,'SPORT FLEX BIBI PRO','SPORT KIDS','SPORTFLEX BIBI','TROISINCH','UNITED NUDE','VICTORIA','AERA PRO'
					,'BARBIE','BARBIE SHOES','BARBIE SHOES PRO','BE FUN','DISNEY','DISNEY PRINCESS','DISNEY PRO','HELLO KITTY'
					,'HELLO KITTY PRO','HERO','HERO KIDS','HERO KIDS PRO','MICKEY MOUSE','MICKEY MOUSE PRO','SUNSANG') AND PRODUCT_NAME NOT LIKE '%͹غ��%'
																													   AND PRODUCT_NAME NOT LIKE '%��ж�%'
																													   AND PRODUCT_NAME NOT LIKE '%�Ѹ��%'
																													   AND (PRODUCT_NAME LIKE '%[0-9][0-9][0-9]'
																															OR PRODUCT_NAME NOT LIKE '%[0-9][0-9]'))
	OR (CLEANED_BRANDNAME LIKE 'SUNSANG' AND PRODUCT_NAME LIKE '%99')
	OR CLEANED_BRANDNAME IN ('MELISSA','CATCHA','DEXTER ACCESSORIES','MINICCI','SUPASIN','CARTOON AGENCY','BUILDABEAR WORKSHOP')
	OR (CLEANED_BRANDNAME IN ('CROCS','CROCS CREDIT','NEW BALANCE','SKECHERS','ADDA','OSH KOSH BGOSH','OSH KOSH BGOSH PRO1'
						,'OSHKOSH','OSHKOSH PRO','TOEZONE','TOEZONE PRO','TOEZONE PRO1') AND RIGHT(PRODUCT_NAME,2) NOT IN (' 1',' 2',' 3',' 4',' 5',' 6',' 7',' 8',' 9','10','11','12','13','B1','B2','B3','B4','B5','B6')
																						 AND PRODUCT_NAME NOT LIKE '%�����%')
	OR PRODUCT_NAME LIKE '�ػ�ó�%BOTTL%0 75'
	OR (CLEANED_BRANDNAME LIKE 'ADIDAS KIDS' AND PRODUCT_NAME NOT LIKE '%[0-9][0-9]')
	OR PRODUCT_NAME NOT LIKE '%[0-9]'
	OR (CLEANED_BRANDNAME LIKE 'ADIDAS KIDS K2' AND (PRODUCT_NAME LIKE '%2.0'
											OR PRODUCT_NAME LIKE '%3.0'
											OR PRODUCT_NAME LIKE '%17.1'))
	OR (CLEANED_BRANDNAME LIKE 'CHRCTER' AND PRODUCT_NAME LIKE '%F1')
	OR (CLEANED_BRANDNAME LIKE 'NANYANG' AND PRODUCT_NAME LIKE '%P4')
	OR (CLEANED_BRANDNAME LIKE 'OLLIE' AND PRODUCT_NAME LIKE '%P6')
	OR (CLEANED_BRANDNAME LIKE 'ADIDAS KIDS K1' AND PRODUCT_NAME LIKE '%T2')
	OR PRODUCT_NAME LIKE '�ͧ�������99'
	OR PRODUCT_NAME LIKE '�ͧ����� 34164 MINI GRN05'
	)
;

--JUNIOR
UPDATE vpm_data.pm_skutagging_kids_product_master
SET JUNIOR_FLAG = 'YES'
	,JUNIOR_KEYWORD = (CASE WHEN PRODUCT_NAME LIKE '%��ж�%' THEN '��ж�'
							WHEN CLEANED_BRANDNAME LIKE 'FOF %' AND PRODUCT_NAME LIKE '% 6' THEN '6Y'
							WHEN CLEANED_BRANDNAME LIKE 'FOF %' AND PRODUCT_NAME LIKE '% 8' THEN '8Y'
							WHEN CLEANED_BRANDNAME LIKE 'FOF %' AND PRODUCT_NAME LIKE '%10' THEN '10Y'
							WHEN CLEANED_BRANDNAME LIKE 'FOF %' AND PRODUCT_NAME LIKE '%11' THEN '11Y'
							WHEN CLEANED_BRANDNAME LIKE 'FOF %' AND PRODUCT_NAME LIKE '%12' THEN '12Y'
							WHEN CLEANED_BRANDNAME LIKE 'FOF %' AND PRODUCT_NAME LIKE '%13' THEN '13Y'
							WHEN CLEANED_BRANDNAME IN ('ANGEL KIDS','BABY DIOR','BIBI','BIBI PRO','BLOCH','EMPORIO ARMANI','FILA','GOLD CITY','BREAKER'
											,'GRENDENE KIDS','GUCCI KIDS GIRL','HAVAIANAS','HAVAIANAS PRO1','IPANEMA','KENZO KIDS','KITO','KOOTS'
											,'KOU KOU','LICENSES SHOES','LICENSES SHOES PRO','LITTLE STEPS','LITTLE STEPS PRO','LULU','MIMI SHOES'
											,'MIMI SHOES PRO','MIMI SHOES PROMO','MINI MELISSA','PAUL SMITH JUNIOR','POPGIRL','POPMAN','POPTEEN'
											,'REPLAY','SALTWATER','SANRIO','SANRIO BABY','SCS','SCS BREAKER','SMALLTIME','SNAP','SPORT FLEX BIBI'
											,'SPORT FLEX BIBI PRO','SPORT KIDS','SPORTFLEX BIBI','TROISINCH','UNITED NUDE','VICTORIA','AERA PRO'
											,'BARBIE','BARBIE SHOES','BARBIE SHOES PRO','BE FUN','DISNEY','DISNEY PRINCESS','DISNEY PRO','HELLO KITTY'
											,'HELLO KITTY PRO','HERO','HERO KIDS','HERO KIDS PRO','MICKEY MOUSE','MICKEY MOUSE PRO','SUNSANG')
											AND PRODUCT_NAME NOT LIKE '%[0-9][0-9][0-9]' AND PRODUCT_NAME LIKE '%32' THEN 'SHOES EU SIZE 32'
							WHEN CLEANED_BRANDNAME IN ('ANGEL KIDS','BABY DIOR','BIBI','BIBI PRO','BLOCH','EMPORIO ARMANI','FILA','GOLD CITY','BREAKER'
											,'GRENDENE KIDS','GUCCI KIDS GIRL','HAVAIANAS','HAVAIANAS PRO1','IPANEMA','KENZO KIDS','KITO','KOOTS'
											,'KOU KOU','LICENSES SHOES','LICENSES SHOES PRO','LITTLE STEPS','LITTLE STEPS PRO','LULU','MIMI SHOES'
											,'MIMI SHOES PRO','MIMI SHOES PROMO','MINI MELISSA','PAUL SMITH JUNIOR','POPGIRL','POPMAN','POPTEEN'
											,'REPLAY','SALTWATER','SANRIO','SANRIO BABY','SCS','SCS BREAKER','SMALLTIME','SNAP','SPORT FLEX BIBI'
											,'SPORT FLEX BIBI PRO','SPORT KIDS','SPORTFLEX BIBI','TROISINCH','UNITED NUDE','VICTORIA','AERA PRO'
											,'BARBIE','BARBIE SHOES','BARBIE SHOES PRO','BE FUN','DISNEY','DISNEY PRINCESS','DISNEY PRO','HELLO KITTY'
											,'HELLO KITTY PRO','HERO','HERO KIDS','HERO KIDS PRO','MICKEY MOUSE','MICKEY MOUSE PRO','SUNSANG')
											AND PRODUCT_NAME NOT LIKE '%[0-9][0-9][0-9]' AND PRODUCT_NAME LIKE '%33' THEN 'SHOES EU SIZE 33'
							WHEN CLEANED_BRANDNAME IN ('ANGEL KIDS','BABY DIOR','BIBI','BIBI PRO','BLOCH','EMPORIO ARMANI','FILA','GOLD CITY','BREAKER'
											,'GRENDENE KIDS','GUCCI KIDS GIRL','HAVAIANAS','HAVAIANAS PRO1','IPANEMA','KENZO KIDS','KITO','KOOTS'
											,'KOU KOU','LICENSES SHOES','LICENSES SHOES PRO','LITTLE STEPS','LITTLE STEPS PRO','LULU','MIMI SHOES'
											,'MIMI SHOES PRO','MIMI SHOES PROMO','MINI MELISSA','PAUL SMITH JUNIOR','POPGIRL','POPMAN','POPTEEN'
											,'REPLAY','SALTWATER','SANRIO','SANRIO BABY','SCS','SCS BREAKER','SMALLTIME','SNAP','SPORT FLEX BIBI'
											,'SPORT FLEX BIBI PRO','SPORT KIDS','SPORTFLEX BIBI','TROISINCH','UNITED NUDE','VICTORIA','AERA PRO'
											,'BARBIE','BARBIE SHOES','BARBIE SHOES PRO','BE FUN','DISNEY','DISNEY PRINCESS','DISNEY PRO','HELLO KITTY'
											,'HELLO KITTY PRO','HERO','HERO KIDS','HERO KIDS PRO','MICKEY MOUSE','MICKEY MOUSE PRO','SUNSANG')
											AND PRODUCT_NAME NOT LIKE '%[0-9][0-9][0-9]' AND PRODUCT_NAME LIKE '%34' THEN 'SHOES EU SIZE 34'
							WHEN CLEANED_BRANDNAME IN ('ANGEL KIDS','BABY DIOR','BIBI','BIBI PRO','BLOCH','EMPORIO ARMANI','FILA','GOLD CITY','BREAKER'
											,'GRENDENE KIDS','GUCCI KIDS GIRL','HAVAIANAS','HAVAIANAS PRO1','IPANEMA','KENZO KIDS','KITO','KOOTS'
											,'KOU KOU','LICENSES SHOES','LICENSES SHOES PRO','LITTLE STEPS','LITTLE STEPS PRO','LULU','MIMI SHOES'
											,'MIMI SHOES PRO','MIMI SHOES PROMO','MINI MELISSA','PAUL SMITH JUNIOR','POPGIRL','POPMAN','POPTEEN'
											,'REPLAY','SALTWATER','SANRIO','SANRIO BABY','SCS','SCS BREAKER','SMALLTIME','SNAP','SPORT FLEX BIBI'
											,'SPORT FLEX BIBI PRO','SPORT KIDS','SPORTFLEX BIBI','TROISINCH','UNITED NUDE','VICTORIA','AERA PRO'
											,'BARBIE','BARBIE SHOES','BARBIE SHOES PRO','BE FUN','DISNEY','DISNEY PRINCESS','DISNEY PRO','HELLO KITTY'
											,'HELLO KITTY PRO','HERO','HERO KIDS','HERO KIDS PRO','MICKEY MOUSE','MICKEY MOUSE PRO','SUNSANG')
											AND PRODUCT_NAME NOT LIKE '%[0-9][0-9][0-9]' AND PRODUCT_NAME LIKE '%35' THEN 'SHOES EU SIZE 35'
							WHEN CLEANED_BRANDNAME IN ('ANGEL KIDS','BABY DIOR','BIBI','BIBI PRO','BLOCH','EMPORIO ARMANI','FILA','GOLD CITY','BREAKER'
											,'GRENDENE KIDS','GUCCI KIDS GIRL','HAVAIANAS','HAVAIANAS PRO1','IPANEMA','KENZO KIDS','KITO','KOOTS'
											,'KOU KOU','LICENSES SHOES','LICENSES SHOES PRO','LITTLE STEPS','LITTLE STEPS PRO','LULU','MIMI SHOES'
											,'MIMI SHOES PRO','MIMI SHOES PROMO','MINI MELISSA','PAUL SMITH JUNIOR','POPGIRL','POPMAN','POPTEEN'
											,'REPLAY','SALTWATER','SANRIO','SANRIO BABY','SCS','SCS BREAKER','SMALLTIME','SNAP','SPORT FLEX BIBI'
											,'SPORT FLEX BIBI PRO','SPORT KIDS','SPORTFLEX BIBI','TROISINCH','UNITED NUDE','VICTORIA','AERA PRO'
											,'BARBIE','BARBIE SHOES','BARBIE SHOES PRO','BE FUN','DISNEY','DISNEY PRINCESS','DISNEY PRO','HELLO KITTY'
											,'HELLO KITTY PRO','HERO','HERO KIDS','HERO KIDS PRO','MICKEY MOUSE','MICKEY MOUSE PRO','SUNSANG')
											AND PRODUCT_NAME NOT LIKE '%[0-9][0-9][0-9]' AND PRODUCT_NAME LIKE '%36' THEN 'SHOES EU SIZE 36'
							WHEN CLEANED_BRANDNAME IN ('ANGEL KIDS','BABY DIOR','BIBI','BIBI PRO','BLOCH','EMPORIO ARMANI','FILA','GOLD CITY','BREAKER'
											,'GRENDENE KIDS','GUCCI KIDS GIRL','HAVAIANAS','HAVAIANAS PRO1','IPANEMA','KENZO KIDS','KITO','KOOTS'
											,'KOU KOU','LICENSES SHOES','LICENSES SHOES PRO','LITTLE STEPS','LITTLE STEPS PRO','LULU','MIMI SHOES'
											,'MIMI SHOES PRO','MIMI SHOES PROMO','MINI MELISSA','PAUL SMITH JUNIOR','POPGIRL','POPMAN','POPTEEN'
											,'REPLAY','SALTWATER','SANRIO','SANRIO BABY','SCS','SCS BREAKER','SMALLTIME','SNAP','SPORT FLEX BIBI'
											,'SPORT FLEX BIBI PRO','SPORT KIDS','SPORTFLEX BIBI','TROISINCH','UNITED NUDE','VICTORIA','AERA PRO'
											,'BARBIE','BARBIE SHOES','BARBIE SHOES PRO','BE FUN','DISNEY','DISNEY PRINCESS','DISNEY PRO','HELLO KITTY'
											,'HELLO KITTY PRO','HERO','HERO KIDS','HERO KIDS PRO','MICKEY MOUSE','MICKEY MOUSE PRO','SUNSANG')
											AND PRODUCT_NAME NOT LIKE '%[0-9][0-9][0-9]' AND PRODUCT_NAME LIKE '%37' THEN 'SHOES EU SIZE 37'
							WHEN CLEANED_BRANDNAME IN ('CROCS','CROCS CREDIT','NEW BALANCE','SKECHERS','ADDA','OSH KOSH BGOSH','OSH KOSH BGOSH PRO1'
												,'OSHKOSH','OSHKOSH PRO','TOEZONE','TOEZONE PRO','TOEZONE PRO1') AND PRODUCT_NAME LIKE '%B1' THEN '�ͧ������ US 1'
							WHEN CLEANED_BRANDNAME IN ('CROCS','CROCS CREDIT','NEW BALANCE','SKECHERS','ADDA','OSH KOSH BGOSH','OSH KOSH BGOSH PRO1'
												,'OSHKOSH','OSHKOSH PRO','TOEZONE','TOEZONE PRO','TOEZONE PRO1') AND PRODUCT_NAME LIKE '%B2' THEN '�ͧ������ US 2'
							WHEN CLEANED_BRANDNAME IN ('CROCS','CROCS CREDIT','NEW BALANCE','SKECHERS','ADDA','OSH KOSH BGOSH','OSH KOSH BGOSH PRO1'
												,'OSHKOSH','OSHKOSH PRO','TOEZONE','TOEZONE PRO','TOEZONE PRO1') AND PRODUCT_NAME LIKE '%B3' THEN '�ͧ������ US 3'
							WHEN CLEANED_BRANDNAME IN ('CROCS','CROCS CREDIT','NEW BALANCE','SKECHERS','ADDA','OSH KOSH BGOSH','OSH KOSH BGOSH PRO1'
												,'OSHKOSH','OSHKOSH PRO','TOEZONE','TOEZONE PRO','TOEZONE PRO1') AND PRODUCT_NAME LIKE '%B4' THEN '�ͧ������ US 4'
							WHEN CLEANED_BRANDNAME IN ('CROCS','CROCS CREDIT','NEW BALANCE','SKECHERS','ADDA','OSH KOSH BGOSH','OSH KOSH BGOSH PRO1'
												,'OSHKOSH','OSHKOSH PRO','TOEZONE','TOEZONE PRO','TOEZONE PRO1') AND PRODUCT_NAME LIKE '%B5' THEN '�ͧ������ US 5'
							WHEN CLEANED_BRANDNAME IN ('CROCS','CROCS CREDIT','NEW BALANCE','SKECHERS','ADDA','OSH KOSH BGOSH','OSH KOSH BGOSH PRO1'
												,'OSHKOSH','OSHKOSH PRO','TOEZONE','TOEZONE PRO','TOEZONE PRO1') AND PRODUCT_NAME LIKE '%B6' THEN '�ͧ������ US 6'
							WHEN PRODUCT_NAME LIKE '%32' THEN 'SHOES EU SIZE 32'
							WHEN PRODUCT_NAME LIKE '%33' THEN 'SHOES EU SIZE 33'
							WHEN PRODUCT_NAME LIKE '%34' THEN 'SHOES EU SIZE 34'
							WHEN PRODUCT_NAME LIKE '%35' THEN 'SHOES EU SIZE 35'
							WHEN PRODUCT_NAME LIKE '%36' THEN 'SHOES EU SIZE 36'
							WHEN PRODUCT_NAME LIKE '%37' THEN 'SHOES EU SIZE 37'
							WHEN CLEANED_BRANDNAME LIKE 'ADIDAS KIDS' AND PRODUCT_NAME LIKE '%140' THEN 'CLOTHING 140 CM'
							WHEN CLEANED_BRANDNAME LIKE 'ADIDAS KIDS' AND PRODUCT_NAME LIKE '%152' THEN 'CLOTHING 152 CM'
							ELSE JUNIOR_KEYWORD END)
WHERE MOM_AND_KID_PRODUCT_FLAG = 1
AND COALESCE(PREG_FLAG,'N/A') = 'N/A'
AND COALESCE(INFANT_FLAG,'N/A') = 'N/A'
AND COALESCE(TODDLER_FLAG,'N/A') = 'N/A'
AND COALESCE(PRESCHOOL_FLAG,'N/A') = 'N/A'
AND COALESCE(JUNIOR_FLAG,'N/A') = 'N/A'
AND COALESCE(TEEN_FLAG,'N/A') = 'N/A'
AND REVISED_CATEGORY_LEVEL_1 = 'KIDS'
AND REVISED_CATEGORY_LEVEL_2 = 'KID FASHION'
AND REVISED_CATEGORY_LEVEL_3 = 'KID SHOES'
AND (PRODUCT_NAME LIKE '%��ж�%'
	OR (CLEANED_BRANDNAME LIKE 'FOF %' AND PRODUCT_NAME LIKE '% 6')
	OR (CLEANED_BRANDNAME LIKE 'FOF %' AND PRODUCT_NAME LIKE '% 8')
	OR (CLEANED_BRANDNAME LIKE 'FOF %' AND PRODUCT_NAME LIKE '%10')
	OR (CLEANED_BRANDNAME LIKE 'FOF %' AND PRODUCT_NAME LIKE '%11')
	OR (CLEANED_BRANDNAME LIKE 'FOF %' AND PRODUCT_NAME LIKE '%12')
	OR (CLEANED_BRANDNAME LIKE 'FOF %' AND PRODUCT_NAME LIKE '%13')
	OR (CLEANED_BRANDNAME IN ('ANGEL KIDS','BABY DIOR','BIBI','BIBI PRO','BLOCH','EMPORIO ARMANI','FILA','GOLD CITY','BREAKER'
					,'GRENDENE KIDS','GUCCI KIDS GIRL','HAVAIANAS','HAVAIANAS PRO1','IPANEMA','KENZO KIDS','KITO','KOOTS'
					,'KOU KOU','LICENSES SHOES','LICENSES SHOES PRO','LITTLE STEPS','LITTLE STEPS PRO','LULU','MIMI SHOES'
					,'MIMI SHOES PRO','MIMI SHOES PROMO','MINI MELISSA','PAUL SMITH JUNIOR','POPGIRL','POPMAN','POPTEEN'
					,'REPLAY','SALTWATER','SANRIO','SANRIO BABY','SCS','SCS BREAKER','SMALLTIME','SNAP','SPORT FLEX BIBI'
					,'SPORT FLEX BIBI PRO','SPORT KIDS','SPORTFLEX BIBI','TROISINCH','UNITED NUDE','VICTORIA','AERA PRO'
					,'BARBIE','BARBIE SHOES','BARBIE SHOES PRO','BE FUN','DISNEY','DISNEY PRINCESS','DISNEY PRO','HELLO KITTY'
					,'HELLO KITTY PRO','HERO','HERO KIDS','HERO KIDS PRO','MICKEY MOUSE','MICKEY MOUSE PRO','SUNSANG') AND (PRODUCT_NAME LIKE '%32'
																															OR PRODUCT_NAME LIKE '%33'
																															OR PRODUCT_NAME LIKE '%34'
																															OR PRODUCT_NAME LIKE '%35'
																															OR PRODUCT_NAME LIKE '%36'
																															OR PRODUCT_NAME LIKE '%37')
																													   AND PRODUCT_NAME NOT LIKE '%[0-9][0-9][0-9]')	
	OR (CLEANED_BRANDNAME IN ('CROCS','CROCS CREDIT','NEW BALANCE','SKECHERS','ADDA','OSH KOSH BGOSH','OSH KOSH BGOSH PRO1'
						,'OSHKOSH','OSHKOSH PRO','TOEZONE','TOEZONE PRO','TOEZONE PRO1') AND (PRODUCT_NAME LIKE '%B1'
																							OR PRODUCT_NAME LIKE '%B2'
																							OR PRODUCT_NAME LIKE '%B3'
																							OR PRODUCT_NAME LIKE '%B4'
																							OR PRODUCT_NAME LIKE '%B5'
																							OR PRODUCT_NAME LIKE '%B6'))
	OR PRODUCT_NAME LIKE '%32'
	OR PRODUCT_NAME LIKE '%33'
	OR PRODUCT_NAME LIKE '%34'
	OR PRODUCT_NAME LIKE '%35'
	OR PRODUCT_NAME LIKE '%36'
	OR PRODUCT_NAME LIKE '%37'
	OR (CLEANED_BRANDNAME LIKE 'ADIDAS KIDS' AND PRODUCT_NAME LIKE '%140')
	OR (CLEANED_BRANDNAME LIKE 'ADIDAS KIDS' AND PRODUCT_NAME LIKE '%152')
	)
;

--TEEN
UPDATE vpm_data.pm_skutagging_kids_product_master
SET TEEN_FLAG = 'YES'
	,TEEN_KEYWORD = (CASE WHEN PRODUCT_NAME LIKE '%�ͧ���ʵ��%' THEN '�ͧ���ʵ��'
						  WHEN CLEANED_BRANDNAME IN ('ANGEL KIDS','BABY DIOR','BIBI','BIBI PRO','BLOCH','EMPORIO ARMANI','FILA','GOLD CITY','BREAKER'
											,'GRENDENE KIDS','GUCCI KIDS GIRL','HAVAIANAS','HAVAIANAS PRO1','IPANEMA','KENZO KIDS','KITO','KOOTS'
											,'KOU KOU','LICENSES SHOES','LICENSES SHOES PRO','LITTLE STEPS','LITTLE STEPS PRO','LULU','MIMI SHOES'
											,'MIMI SHOES PRO','MIMI SHOES PROMO','MINI MELISSA','PAUL SMITH JUNIOR','POPGIRL','POPMAN','POPTEEN'
											,'REPLAY','SALTWATER','SANRIO','SANRIO BABY','SCS','SCS BREAKER','SMALLTIME','SNAP','SPORT FLEX BIBI'
											,'SPORT FLEX BIBI PRO','SPORT KIDS','SPORTFLEX BIBI','TROISINCH','UNITED NUDE','VICTORIA','AERA PRO'
											,'BARBIE','BARBIE SHOES','BARBIE SHOES PRO','BE FUN','DISNEY','DISNEY PRINCESS','DISNEY PRO','HELLO KITTY'
											,'HELLO KITTY PRO','HERO','HERO KIDS','HERO KIDS PRO','MICKEY MOUSE','MICKEY MOUSE PRO','SUNSANG')
											AND PRODUCT_NAME NOT LIKE '%[0-9][0-9][0-9]' AND PRODUCT_NAME LIKE '%38' THEN 'SHOES EU SIZE 38'
						  WHEN CLEANED_BRANDNAME IN ('ANGEL KIDS','BABY DIOR','BIBI','BIBI PRO','BLOCH','EMPORIO ARMANI','FILA','GOLD CITY','BREAKER'
											,'GRENDENE KIDS','GUCCI KIDS GIRL','HAVAIANAS','HAVAIANAS PRO1','IPANEMA','KENZO KIDS','KITO','KOOTS'
											,'KOU KOU','LICENSES SHOES','LICENSES SHOES PRO','LITTLE STEPS','LITTLE STEPS PRO','LULU','MIMI SHOES'
											,'MIMI SHOES PRO','MIMI SHOES PROMO','MINI MELISSA','PAUL SMITH JUNIOR','POPGIRL','POPMAN','POPTEEN'
											,'REPLAY','SALTWATER','SANRIO','SANRIO BABY','SCS','SCS BREAKER','SMALLTIME','SNAP','SPORT FLEX BIBI'
											,'SPORT FLEX BIBI PRO','SPORT KIDS','SPORTFLEX BIBI','TROISINCH','UNITED NUDE','VICTORIA','AERA PRO'
											,'BARBIE','BARBIE SHOES','BARBIE SHOES PRO','BE FUN','DISNEY','DISNEY PRINCESS','DISNEY PRO','HELLO KITTY'
											,'HELLO KITTY PRO','HERO','HERO KIDS','HERO KIDS PRO','MICKEY MOUSE','MICKEY MOUSE PRO','SUNSANG')
											AND PRODUCT_NAME NOT LIKE '%[0-9][0-9][0-9]' AND PRODUCT_NAME LIKE '%39' THEN 'SHOES EU SIZE 39'
						  WHEN CLEANED_BRANDNAME IN ('ANGEL KIDS','BABY DIOR','BIBI','BIBI PRO','BLOCH','EMPORIO ARMANI','FILA','GOLD CITY','BREAKER'
											,'GRENDENE KIDS','GUCCI KIDS GIRL','HAVAIANAS','HAVAIANAS PRO1','IPANEMA','KENZO KIDS','KITO','KOOTS'
											,'KOU KOU','LICENSES SHOES','LICENSES SHOES PRO','LITTLE STEPS','LITTLE STEPS PRO','LULU','MIMI SHOES'
											,'MIMI SHOES PRO','MIMI SHOES PROMO','MINI MELISSA','PAUL SMITH JUNIOR','POPGIRL','POPMAN','POPTEEN'
											,'REPLAY','SALTWATER','SANRIO','SANRIO BABY','SCS','SCS BREAKER','SMALLTIME','SNAP','SPORT FLEX BIBI'
											,'SPORT FLEX BIBI PRO','SPORT KIDS','SPORTFLEX BIBI','TROISINCH','UNITED NUDE','VICTORIA','AERA PRO'
											,'BARBIE','BARBIE SHOES','BARBIE SHOES PRO','BE FUN','DISNEY','DISNEY PRINCESS','DISNEY PRO','HELLO KITTY'
											,'HELLO KITTY PRO','HERO','HERO KIDS','HERO KIDS PRO','MICKEY MOUSE','MICKEY MOUSE PRO','SUNSANG')
											AND PRODUCT_NAME NOT LIKE '%[0-9][0-9][0-9]' AND PRODUCT_NAME LIKE '%40' THEN 'SHOES EU SIZE 40'
						  WHEN CLEANED_BRANDNAME IN ('ANGEL KIDS','BABY DIOR','BIBI','BIBI PRO','BLOCH','EMPORIO ARMANI','FILA','GOLD CITY','BREAKER'
											,'GRENDENE KIDS','GUCCI KIDS GIRL','HAVAIANAS','HAVAIANAS PRO1','IPANEMA','KENZO KIDS','KITO','KOOTS'
											,'KOU KOU','LICENSES SHOES','LICENSES SHOES PRO','LITTLE STEPS','LITTLE STEPS PRO','LULU','MIMI SHOES'
											,'MIMI SHOES PRO','MIMI SHOES PROMO','MINI MELISSA','PAUL SMITH JUNIOR','POPGIRL','POPMAN','POPTEEN'
											,'REPLAY','SALTWATER','SANRIO','SANRIO BABY','SCS','SCS BREAKER','SMALLTIME','SNAP','SPORT FLEX BIBI'
											,'SPORT FLEX BIBI PRO','SPORT KIDS','SPORTFLEX BIBI','TROISINCH','UNITED NUDE','VICTORIA','AERA PRO'
											,'BARBIE','BARBIE SHOES','BARBIE SHOES PRO','BE FUN','DISNEY','DISNEY PRINCESS','DISNEY PRO','HELLO KITTY'
											,'HELLO KITTY PRO','HERO','HERO KIDS','HERO KIDS PRO','MICKEY MOUSE','MICKEY MOUSE PRO','SUNSANG')
											AND PRODUCT_NAME NOT LIKE '%[0-9][0-9][0-9]' AND PRODUCT_NAME LIKE '%41' THEN 'SHOES EU SIZE 41'
						  WHEN CLEANED_BRANDNAME IN ('ANGEL KIDS','BABY DIOR','BIBI','BIBI PRO','BLOCH','EMPORIO ARMANI','FILA','GOLD CITY','BREAKER'
											,'GRENDENE KIDS','GUCCI KIDS GIRL','HAVAIANAS','HAVAIANAS PRO1','IPANEMA','KENZO KIDS','KITO','KOOTS'
											,'KOU KOU','LICENSES SHOES','LICENSES SHOES PRO','LITTLE STEPS','LITTLE STEPS PRO','LULU','MIMI SHOES'
											,'MIMI SHOES PRO','MIMI SHOES PROMO','MINI MELISSA','PAUL SMITH JUNIOR','POPGIRL','POPMAN','POPTEEN'
											,'REPLAY','SALTWATER','SANRIO','SANRIO BABY','SCS','SCS BREAKER','SMALLTIME','SNAP','SPORT FLEX BIBI'
											,'SPORT FLEX BIBI PRO','SPORT KIDS','SPORTFLEX BIBI','TROISINCH','UNITED NUDE','VICTORIA','AERA PRO'
											,'BARBIE','BARBIE SHOES','BARBIE SHOES PRO','BE FUN','DISNEY','DISNEY PRINCESS','DISNEY PRO','HELLO KITTY'
											,'HELLO KITTY PRO','HERO','HERO KIDS','HERO KIDS PRO','MICKEY MOUSE','MICKEY MOUSE PRO','SUNSANG')
											AND PRODUCT_NAME NOT LIKE '%[0-9][0-9][0-9]' AND PRODUCT_NAME LIKE '%42' THEN 'SHOES EU SIZE 42'
						  WHEN CLEANED_BRANDNAME IN ('ANGEL KIDS','BABY DIOR','BIBI','BIBI PRO','BLOCH','EMPORIO ARMANI','FILA','GOLD CITY','BREAKER'
											,'GRENDENE KIDS','GUCCI KIDS GIRL','HAVAIANAS','HAVAIANAS PRO1','IPANEMA','KENZO KIDS','KITO','KOOTS'
											,'KOU KOU','LICENSES SHOES','LICENSES SHOES PRO','LITTLE STEPS','LITTLE STEPS PRO','LULU','MIMI SHOES'
											,'MIMI SHOES PRO','MIMI SHOES PROMO','MINI MELISSA','PAUL SMITH JUNIOR','POPGIRL','POPMAN','POPTEEN'
											,'REPLAY','SALTWATER','SANRIO','SANRIO BABY','SCS','SCS BREAKER','SMALLTIME','SNAP','SPORT FLEX BIBI'
											,'SPORT FLEX BIBI PRO','SPORT KIDS','SPORTFLEX BIBI','TROISINCH','UNITED NUDE','VICTORIA','AERA PRO'
											,'BARBIE','BARBIE SHOES','BARBIE SHOES PRO','BE FUN','DISNEY','DISNEY PRINCESS','DISNEY PRO','HELLO KITTY'
											,'HELLO KITTY PRO','HERO','HERO KIDS','HERO KIDS PRO','MICKEY MOUSE','MICKEY MOUSE PRO','SUNSANG')
											AND PRODUCT_NAME NOT LIKE '%[0-9][0-9][0-9]' AND PRODUCT_NAME LIKE '%43' THEN 'SHOES EU SIZE 43'
						  WHEN CLEANED_BRANDNAME IN ('ANGEL KIDS','BABY DIOR','BIBI','BIBI PRO','BLOCH','EMPORIO ARMANI','FILA','GOLD CITY','BREAKER'
											,'GRENDENE KIDS','GUCCI KIDS GIRL','HAVAIANAS','HAVAIANAS PRO1','IPANEMA','KENZO KIDS','KITO','KOOTS'
											,'KOU KOU','LICENSES SHOES','LICENSES SHOES PRO','LITTLE STEPS','LITTLE STEPS PRO','LULU','MIMI SHOES'
											,'MIMI SHOES PRO','MIMI SHOES PROMO','MINI MELISSA','PAUL SMITH JUNIOR','POPGIRL','POPMAN','POPTEEN'
											,'REPLAY','SALTWATER','SANRIO','SANRIO BABY','SCS','SCS BREAKER','SMALLTIME','SNAP','SPORT FLEX BIBI'
											,'SPORT FLEX BIBI PRO','SPORT KIDS','SPORTFLEX BIBI','TROISINCH','UNITED NUDE','VICTORIA','AERA PRO'
											,'BARBIE','BARBIE SHOES','BARBIE SHOES PRO','BE FUN','DISNEY','DISNEY PRINCESS','DISNEY PRO','HELLO KITTY'
											,'HELLO KITTY PRO','HERO','HERO KIDS','HERO KIDS PRO','MICKEY MOUSE','MICKEY MOUSE PRO','SUNSANG')
											AND PRODUCT_NAME NOT LIKE '%[0-9][0-9][0-9]' AND PRODUCT_NAME LIKE '%44' THEN 'SHOES EU SIZE 44'
						  WHEN CLEANED_BRANDNAME IN ('ANGEL KIDS','BABY DIOR','BIBI','BIBI PRO','BLOCH','EMPORIO ARMANI','FILA','GOLD CITY','BREAKER'
											,'GRENDENE KIDS','GUCCI KIDS GIRL','HAVAIANAS','HAVAIANAS PRO1','IPANEMA','KENZO KIDS','KITO','KOOTS'
											,'KOU KOU','LICENSES SHOES','LICENSES SHOES PRO','LITTLE STEPS','LITTLE STEPS PRO','LULU','MIMI SHOES'
											,'MIMI SHOES PRO','MIMI SHOES PROMO','MINI MELISSA','PAUL SMITH JUNIOR','POPGIRL','POPMAN','POPTEEN'
											,'REPLAY','SALTWATER','SANRIO','SANRIO BABY','SCS','SCS BREAKER','SMALLTIME','SNAP','SPORT FLEX BIBI'
											,'SPORT FLEX BIBI PRO','SPORT KIDS','SPORTFLEX BIBI','TROISINCH','UNITED NUDE','VICTORIA','AERA PRO'
											,'BARBIE','BARBIE SHOES','BARBIE SHOES PRO','BE FUN','DISNEY','DISNEY PRINCESS','DISNEY PRO','HELLO KITTY'
											,'HELLO KITTY PRO','HERO','HERO KIDS','HERO KIDS PRO','MICKEY MOUSE','MICKEY MOUSE PRO','SUNSANG')
											AND PRODUCT_NAME NOT LIKE '%[0-9][0-9][0-9]' AND PRODUCT_NAME LIKE '%45' THEN 'SHOES EU SIZE 45'
						  WHEN CLEANED_BRANDNAME IN ('ANGEL KIDS','BABY DIOR','BIBI','BIBI PRO','BLOCH','EMPORIO ARMANI','FILA','GOLD CITY','BREAKER'
											,'GRENDENE KIDS','GUCCI KIDS GIRL','HAVAIANAS','HAVAIANAS PRO1','IPANEMA','KENZO KIDS','KITO','KOOTS'
											,'KOU KOU','LICENSES SHOES','LICENSES SHOES PRO','LITTLE STEPS','LITTLE STEPS PRO','LULU','MIMI SHOES'
											,'MIMI SHOES PRO','MIMI SHOES PROMO','MINI MELISSA','PAUL SMITH JUNIOR','POPGIRL','POPMAN','POPTEEN'
											,'REPLAY','SALTWATER','SANRIO','SANRIO BABY','SCS','SCS BREAKER','SMALLTIME','SNAP','SPORT FLEX BIBI'
											,'SPORT FLEX BIBI PRO','SPORT KIDS','SPORTFLEX BIBI','TROISINCH','UNITED NUDE','VICTORIA','AERA PRO'
											,'BARBIE','BARBIE SHOES','BARBIE SHOES PRO','BE FUN','DISNEY','DISNEY PRINCESS','DISNEY PRO','HELLO KITTY'
											,'HELLO KITTY PRO','HERO','HERO KIDS','HERO KIDS PRO','MICKEY MOUSE','MICKEY MOUSE PRO','SUNSANG')
											AND PRODUCT_NAME NOT LIKE '%[0-9][0-9][0-9]' AND PRODUCT_NAME LIKE '%46' THEN 'SHOES EU SIZE 46'
						  WHEN CLEANED_BRANDNAME IN ('ANGEL KIDS','BABY DIOR','BIBI','BIBI PRO','BLOCH','EMPORIO ARMANI','FILA','GOLD CITY','BREAKER'
											,'GRENDENE KIDS','GUCCI KIDS GIRL','HAVAIANAS','HAVAIANAS PRO1','IPANEMA','KENZO KIDS','KITO','KOOTS'
											,'KOU KOU','LICENSES SHOES','LICENSES SHOES PRO','LITTLE STEPS','LITTLE STEPS PRO','LULU','MIMI SHOES'
											,'MIMI SHOES PRO','MIMI SHOES PROMO','MINI MELISSA','PAUL SMITH JUNIOR','POPGIRL','POPMAN','POPTEEN'
											,'REPLAY','SALTWATER','SANRIO','SANRIO BABY','SCS','SCS BREAKER','SMALLTIME','SNAP','SPORT FLEX BIBI'
											,'SPORT FLEX BIBI PRO','SPORT KIDS','SPORTFLEX BIBI','TROISINCH','UNITED NUDE','VICTORIA','AERA PRO'
											,'BARBIE','BARBIE SHOES','BARBIE SHOES PRO','BE FUN','DISNEY','DISNEY PRINCESS','DISNEY PRO','HELLO KITTY'
											,'HELLO KITTY PRO','HERO','HERO KIDS','HERO KIDS PRO','MICKEY MOUSE','MICKEY MOUSE PRO','SUNSANG')
											AND PRODUCT_NAME NOT LIKE '%[0-9][0-9][0-9]' AND PRODUCT_NAME LIKE '%47' THEN 'SHOES EU SIZE 47'																												
						  WHEN PRODUCT_NAME LIKE '%�Ѹ��%' THEN '�Ѹ��'
						  WHEN CLEANED_BRANDNAME LIKE 'FOF %' AND PRODUCT_NAME LIKE '%14' THEN '14Y'
						  WHEN CLEANED_BRANDNAME LIKE 'ADIDAS KIDS' AND PRODUCT_NAME LIKE '%164' THEN 'CLOTHING 164 CM'
						  ELSE TEEN_KEYWORD END)
WHERE MOM_AND_KID_PRODUCT_FLAG = 1
AND COALESCE(PREG_FLAG,'N/A') = 'N/A'
AND COALESCE(INFANT_FLAG,'N/A') = 'N/A'
AND COALESCE(TODDLER_FLAG,'N/A') = 'N/A'
AND COALESCE(PRESCHOOL_FLAG,'N/A') = 'N/A'
AND COALESCE(JUNIOR_FLAG,'N/A') = 'N/A'
AND COALESCE(TEEN_FLAG,'N/A') = 'N/A'
AND REVISED_CATEGORY_LEVEL_1 = 'KIDS'
AND REVISED_CATEGORY_LEVEL_2 = 'KID FASHION'
AND REVISED_CATEGORY_LEVEL_3 = 'KID SHOES'
AND (PRODUCT_NAME LIKE '%�ͧ���ʵ��%'
	OR (CLEANED_BRANDNAME IN ('ANGEL KIDS','BABY DIOR','BIBI','BIBI PRO','BLOCH','EMPORIO ARMANI','FILA','GOLD CITY','BREAKER'
					,'GRENDENE KIDS','GUCCI KIDS GIRL','HAVAIANAS','HAVAIANAS PRO1','IPANEMA','KENZO KIDS','KITO','KOOTS'
					,'KOU KOU','LICENSES SHOES','LICENSES SHOES PRO','LITTLE STEPS','LITTLE STEPS PRO','LULU','MIMI SHOES'
					,'MIMI SHOES PRO','MIMI SHOES PROMO','MINI MELISSA','PAUL SMITH JUNIOR','POPGIRL','POPMAN','POPTEEN'
					,'REPLAY','SALTWATER','SANRIO','SANRIO BABY','SCS','SCS BREAKER','SMALLTIME','SNAP','SPORT FLEX BIBI'
					,'SPORT FLEX BIBI PRO','SPORT KIDS','SPORTFLEX BIBI','TROISINCH','UNITED NUDE','VICTORIA','AERA PRO'
					,'BARBIE','BARBIE SHOES','BARBIE SHOES PRO','BE FUN','DISNEY','DISNEY PRINCESS','DISNEY PRO','HELLO KITTY'
					,'HELLO KITTY PRO','HERO','HERO KIDS','HERO KIDS PRO','MICKEY MOUSE','MICKEY MOUSE PRO','SUNSANG') AND (PRODUCT_NAME LIKE '%38'
																															OR PRODUCT_NAME LIKE '%39'
																															OR PRODUCT_NAME LIKE '%40'
																															OR PRODUCT_NAME LIKE '%41'
																															OR PRODUCT_NAME LIKE '%42'
																															OR PRODUCT_NAME LIKE '%43'
																															OR PRODUCT_NAME LIKE '%44'
																															OR PRODUCT_NAME LIKE '%45'
																															OR PRODUCT_NAME LIKE '%46'
																															OR PRODUCT_NAME LIKE '%47')
																													   AND PRODUCT_NAME NOT LIKE '%[0-9][0-9][0-9]')
	OR PRODUCT_NAME LIKE '%�Ѹ��%'
	OR (CLEANED_BRANDNAME LIKE 'FOF %' AND PRODUCT_NAME LIKE '%14')
	OR (CLEANED_BRANDNAME LIKE 'ADIDAS KIDS' AND PRODUCT_NAME LIKE '%164')
	)
;

-----KID CLOTHING
--NEW INFANT
UPDATE vpm_data.pm_skutagging_kids_product_master
SET INFANT_FLAG = 'YES'
	,"STAGE_M+0_TO_M+3" = (CASE WHEN CLEANED_BRANDNAME LIKE 'SPLASH ABOUT%' AND PRODUCT_NAME LIKE '%HAPPY%NAPPY%S' THEN 'YES'
								WHEN CLEANED_BRANDNAME LIKE 'SPLASH ABOUT%' AND PRODUCT_NAME LIKE '%��黻��ṻ���%S' THEN 'YES'
								WHEN CLEANED_BRANDNAME LIKE 'SPLASH ABOUT%' AND PRODUCT_NAME LIKE '%�λ���ṻ���%S' THEN 'YES'
								WHEN CLEANED_BRANDNAME LIKE 'AMELIE' AND PRODUCT_NAME LIKE '%0 1' THEN 'YES'
								WHEN CLEANED_BRANDNAME LIKE 'AMELIE' AND PRODUCT_NAME LIKE '% 0' THEN 'YES'
								WHEN CLEANED_BRANDNAME LIKE 'AMELIE' AND PRODUCT_NAME LIKE '%0 3' THEN 'YES'
								WHEN CLEANED_BRANDNAME LIKE 'GUCCI KIDS%' AND PRODUCT_NAME LIKE '%0-3' THEN 'YES'
								WHEN CLEANED_BRANDNAME LIKE 'MUFFIN BITE' AND PRODUCT_NAME LIKE '%(40)' THEN 'YES'
								ELSE "STAGE_M+0_TO_M+3" END)
	,"STAGE_M+3_TO_M+6" = (CASE WHEN CLASS_NAME LIKE 'SWIMWEAR' AND PRODUCT_NAME LIKE '% A' THEN 'YES'
								WHEN CLEANED_BRANDNAME LIKE 'SPLASH ABOUT%' AND PRODUCT_NAME LIKE '%HAPPY%NAPPY%M' THEN 'YES'
								WHEN CLEANED_BRANDNAME LIKE 'SPLASH ABOUT%' AND PRODUCT_NAME LIKE '%��黻��ṻ���% M' THEN 'YES'
								WHEN CLEANED_BRANDNAME LIKE 'SPLASH ABOUT%' AND PRODUCT_NAME LIKE '%�λ���ṻ���% M' THEN 'YES'
								WHEN CLEANED_BRANDNAME LIKE 'KEED' AND PRODUCT_NAME LIKE '% 60' THEN 'YES'
								WHEN PRODUCT_NAME LIKE '%ṻ���%60' THEN 'YES'
								WHEN PRODUCT_NAME LIKE '%��ǧ%���¹��%' THEN 'YES'
								WHEN CLEANED_BRANDNAME LIKE 'CLOSE' AND PRODUCT_NAME LIKE '% M' THEN 'YES'
								WHEN CLEANED_BRANDNAME LIKE 'AMELIE' AND PRODUCT_NAME LIKE '%0 1' THEN 'YES'
								ELSE "STAGE_M+3_TO_M+6" END)
	,"STAGE_M+6_TO_M+9" = (CASE WHEN CLEANED_BRANDNAME IN ('PLATYPUS') AND PRODUCT_NAME LIKE '% 0' THEN 'YES'
								WHEN CLEANED_BRANDNAME IN ('SPEEDO') AND PRODUCT_NAME LIKE '% 0' THEN 'YES'
								WHEN PRODUCT_NAME LIKE '%70' THEN 'YES'
								WHEN CLEANED_BRANDNAME LIKE 'SPLASH ABOUT%' AND PRODUCT_NAME LIKE 'BABY WRAP%M' THEN 'YES'
								WHEN CLEANED_BRANDNAME LIKE 'SPLASH ABOUT%' AND PRODUCT_NAME LIKE '%HAPPY%NAPPY%L' AND PRODUCT_NAME NOT LIKE '%XL' THEN 'YES'
								WHEN CLEANED_BRANDNAME LIKE 'SPLASH ABOUT%' AND PRODUCT_NAME LIKE '%��黻��ṻ���% L' THEN 'YES'
								WHEN CLEANED_BRANDNAME LIKE 'SPLASH ABOUT%' AND PRODUCT_NAME LIKE '%�λ���ṻ���% L' THEN 'YES'
								WHEN PRODUCT_NAME LIKE '%��ǧ%���¹��%' THEN 'YES'
								WHEN PRODUCT_NAME LIKE '��ǧ�ʹ��%' THEN 'YES'
								WHEN CLEANED_BRANDNAME LIKE 'CLOSE' AND PRODUCT_NAME LIKE '% L' THEN 'YES'
								WHEN CLEANED_BRANDNAME LIKE 'AMELIE' AND PRODUCT_NAME LIKE '%0 1' THEN 'YES'
								WHEN CLEANED_BRANDNAME LIKE 'GUCCI KIDS%' AND PRODUCT_NAME LIKE '%6-9' THEN 'YES'
								ELSE "STAGE_M+6_TO_M+9" END)
	,"STAGE_M+9_TO_M+12" = (CASE WHEN CLEANED_BRANDNAME IN ('PLATYPUS') AND PRODUCT_NAME LIKE '% 1' THEN 'YES'
								WHEN CLEANED_BRANDNAME IN ('SPEEDO') AND PRODUCT_NAME LIKE '% 1' THEN 'YES'
								WHEN PRODUCT_NAME LIKE '%1Y' THEN 'YES'
								WHEN PRODUCT_NAME LIKE '%80' THEN 'YES'
								WHEN PRODUCT_NAME LIKE '%12 M' THEN 'YES'
								WHEN CLEANED_BRANDNAME LIKE 'SPLASH ABOUT%' AND PRODUCT_NAME LIKE 'BABY WRAP%M' THEN 'YES'
								WHEN CLEANED_BRANDNAME LIKE 'SPLASH ABOUT%' AND PRODUCT_NAME LIKE '%HAPPY%NAPPY%L' AND PRODUCT_NAME NOT LIKE '%XL' THEN 'YES'
								WHEN CLEANED_BRANDNAME LIKE 'SPLASH ABOUT%' AND PRODUCT_NAME LIKE '%��黻��ṻ���% L' THEN 'YES'
								WHEN CLEANED_BRANDNAME LIKE 'SPLASH ABOUT%' AND PRODUCT_NAME LIKE '%�λ���ṻ���% L' THEN 'YES'
								WHEN PRODUCT_NAME LIKE '%��ǧ%���¹��%' THEN 'YES'
								WHEN PRODUCT_NAME LIKE '��ǧ�ʹ��%' THEN 'YES'
								WHEN CLEANED_BRANDNAME LIKE 'CLOSE' AND PRODUCT_NAME LIKE '% L' THEN 'YES'
								WHEN CLEANED_BRANDNAME LIKE 'AMELIE' AND PRODUCT_NAME LIKE '%0 1' THEN 'YES'
								WHEN CLEANED_BRANDNAME LIKE 'AMELIE' AND PRODUCT_NAME LIKE '% 1' THEN 'YES'
								WHEN PRODUCT_NAME LIKE '%1T' THEN 'YES'
								ELSE "STAGE_M+9_TO_M+12" END)
	,TODDLER_FLAG = (CASE WHEN CLEANED_BRANDNAME IN ('PLATYPUS') AND PRODUCT_NAME LIKE '% 2' THEN 'YES'
								WHEN CLEANED_BRANDNAME IN ('SPEEDO') AND PRODUCT_NAME LIKE '% 2' THEN 'YES'
								WHEN PRODUCT_NAME LIKE '%1-2' THEN 'YES'
								WHEN PRODUCT_NAME LIKE '%2Y' THEN 'YES'
								WHEN PRODUCT_NAME LIKE '%18 M' THEN 'YES'
								WHEN PRODUCT_NAME LIKE '%24 M' THEN 'YES'
								WHEN CLEANED_BRANDNAME LIKE 'I PLAY' AND PRODUCT_NAME LIKE '%24' THEN 'YES'
								WHEN CLEANED_BRANDNAME LIKE 'SPLASH ABOUT%' AND PRODUCT_NAME LIKE '�ش���¹��% S%' THEN 'YES'
								WHEN CLEANED_BRANDNAME LIKE 'SPLASH ABOUT%' AND PRODUCT_NAME LIKE 'BABY WRAP%M' THEN 'YES'
								WHEN CLEANED_BRANDNAME LIKE 'SPLASH ABOUT%' AND PRODUCT_NAME LIKE 'BABY WRAP%L' THEN 'YES'
								WHEN CLEANED_BRANDNAME LIKE 'SPLASH ABOUT%' AND PRODUCT_NAME LIKE '%HAPPY%NAPPY%XL' THEN 'YES'
								WHEN CLEANED_BRANDNAME LIKE 'SPLASH ABOUT%' AND PRODUCT_NAME LIKE '%��黻��ṻ���%XL' THEN 'YES'
								WHEN CLEANED_BRANDNAME LIKE 'SPLASH ABOUT%' AND PRODUCT_NAME LIKE '%�λ���ṻ���%XL' THEN 'YES'
								WHEN CLEANED_BRANDNAME LIKE 'SPLASH ABOUT%' AND PRODUCT_NAME LIKE 'JACKET% S%' THEN 'YES'
								WHEN CLEANED_BRANDNAME LIKE 'SPLASH ABOUT%' AND PRODUCT_NAME LIKE '���絽֡��µ��%S' THEN 'YES'
								WHEN CLEANED_BRANDNAME LIKE 'SPLASH ABOUT%' AND PRODUCT_NAME LIKE '�ش�֡��µ��%S%' THEN 'YES'
								WHEN PRODUCT_NAME LIKE '%��ǧ%���¹��%' THEN 'YES'
								WHEN PRODUCT_NAME LIKE '��ǧ�ʹ��%' THEN 'YES'
								WHEN CLEANED_BRANDNAME LIKE 'CLOSE' AND PRODUCT_NAME LIKE '% XL' THEN 'YES'
								WHEN CLEANED_BRANDNAME LIKE 'AMELIE' AND PRODUCT_NAME LIKE '%1 2' THEN 'YES'
								WHEN PRODUCT_NAME LIKE '%2A' AND PRODUCT_NAME NOT LIKE '%12A' THEN 'YES'
								WHEN PRODUCT_NAME LIKE '%12-18' THEN 'YES'
								WHEN PRODUCT_NAME LIKE '%18-24' THEN 'YES'
								WHEN PRODUCT_NAME LIKE '% 02' THEN 'YES'
								WHEN PRODUCT_NAME LIKE '%2T' THEN 'YES'
								WHEN CLEANED_BRANDNAME LIKE 'GUCCI KIDS%' AND PRODUCT_NAME LIKE '% 24' THEN 'YES'
								WHEN CLEANED_BRANDNAME LIKE 'GUCCI KIDS%' AND PRODUCT_NAME LIKE '% 18' THEN 'YES'
								WHEN CLEANED_BRANDNAME LIKE 'JELLY BEAN' AND PRODUCT_NAME LIKE '% 2' THEN 'YES'
								WHEN PRODUCT_NAME LIKE '%���2' THEN 'YES'
								WHEN CLEANED_BRANDNAME LIKE 'GINGERSNAPS' AND PRODUCT_NAME LIKE '%2' AND PRODUCT_NAME NOT LIKE '%12' THEN 'YES'
								WHEN CLEANED_BRANDNAME LIKE 'GINGERSNAPS' AND PRODUCT_NAME LIKE '% 2CC' THEN 'YES'
								WHEN CLEANED_BRANDNAME LIKE 'GINGERSNAPS' AND PRODUCT_NAME LIKE '% 2HD' THEN 'YES'
								WHEN CLEANED_BRANDNAME LIKE 'GINGERSNAPS' AND PRODUCT_NAME LIKE '% 2WF' THEN 'YES'
								WHEN CLEANED_BRANDNAME LIKE 'PJ KIDS' AND PRODUCT_NAME LIKE '%SIZE 2' THEN 'YES'
								ELSE TODDLER_FLAG END)
	,UNKNOWN_INFANT_SUBSTAGE = (CASE WHEN CLEANED_BRANDNAME LIKE 'SPLASH ABOUT%' AND PRODUCT_NAME LIKE '��ǡ���¹��%S' THEN 'YES'
									WHEN PRODUCT_NAME LIKE '%0-2' THEN 'YES'
									WHEN PRODUCT_NAME LIKE '%INFANTCARESET%' THEN 'YES'
									WHEN CLEANED_BRANDNAME LIKE 'EMPORIO ARMANI' AND CLASS_NAME LIKE 'NEW BORN/GIRLS' THEN 'YES'
									WHEN CLEANED_BRANDNAME LIKE 'HONEY BUNNY' AND PRODUCT_NAME LIKE '% NB' THEN 'YES'
									WHEN PRODUCT_NAME LIKE '�ا���຺�� 0 2' THEN 'YES'
									WHEN CLEANED_BRANDNAME LIKE 'SWEET BIRDS' THEN 'YES'
									WHEN CLEANED_BRANDNAME LIKE 'TEENY TOES' THEN 'YES'
									ELSE UNKNOWN_INFANT_SUBSTAGE END)
	,INFANT_0_3M_KEYWORD = (CASE WHEN CLEANED_BRANDNAME LIKE 'SPLASH ABOUT%' AND PRODUCT_NAME LIKE '%HAPPY%NAPPY%S' THEN 'SPLASH ABOUT HAPPY NAPPY SIZE S'
								WHEN CLEANED_BRANDNAME LIKE 'SPLASH ABOUT%' AND PRODUCT_NAME LIKE '%��黻��ṻ���%S' THEN 'SPLASH ABOUT HAPPY NAPPY SIZE S'
								WHEN CLEANED_BRANDNAME LIKE 'SPLASH ABOUT%' AND PRODUCT_NAME LIKE '%�λ���ṻ���%S' THEN 'SPLASH ABOUT HAPPY NAPPY SIZE S'
								WHEN CLEANED_BRANDNAME LIKE 'AMELIE' AND PRODUCT_NAME LIKE '%0 1' THEN '0-1 Y'
								WHEN CLEANED_BRANDNAME LIKE 'AMELIE' AND PRODUCT_NAME LIKE '% 0' THEN '0 MTH'
								WHEN CLEANED_BRANDNAME LIKE 'AMELIE' AND PRODUCT_NAME LIKE '%0 3' THEN '0-3 MTH'
								WHEN CLEANED_BRANDNAME LIKE 'GUCCI KIDS%' AND PRODUCT_NAME LIKE '%0-3' THEN '0-3 MTH'
								WHEN CLEANED_BRANDNAME LIKE 'MUFFIN BITE' AND PRODUCT_NAME LIKE '%(40)' THEN 'CLOTHING 40 CM'
								ELSE INFANT_0_3M_KEYWORD END)
	,INFANT_4_6M_KEYWORD = (CASE WHEN CLASS_NAME LIKE 'SWIMWEAR' AND PRODUCT_NAME LIKE '% A' THEN 'SWIMWEAR SIZE A'
								WHEN CLEANED_BRANDNAME LIKE 'SPLASH ABOUT%' AND PRODUCT_NAME LIKE '%HAPPY%NAPPY%M' THEN 'SPLASH ABOUT HAPPY NAPPY SIZE M'
								WHEN CLEANED_BRANDNAME LIKE 'SPLASH ABOUT%' AND PRODUCT_NAME LIKE '%��黻��ṻ���% M' THEN 'SPLASH ABOUT HAPPY NAPPY SIZE M'
								WHEN CLEANED_BRANDNAME LIKE 'SPLASH ABOUT%' AND PRODUCT_NAME LIKE '%�λ���ṻ���% M' THEN 'SPLASH ABOUT HAPPY NAPPY SIZE M'
								WHEN CLEANED_BRANDNAME LIKE 'KEED' AND PRODUCT_NAME LIKE '% 60' THEN 'SWIMWEAR 60 CM'
								WHEN PRODUCT_NAME LIKE '%ṻ���%60' THEN 'SWIMWEAR 60 CM'
								WHEN PRODUCT_NAME LIKE '%��ǧ%���¹��%' THEN '��ǧ�����¹��'
								WHEN CLEANED_BRANDNAME LIKE 'CLOSE' AND PRODUCT_NAME LIKE '% M' THEN 'CLOSE SWIM NAPPY SIZE M'
								WHEN CLEANED_BRANDNAME LIKE 'AMELIE' AND PRODUCT_NAME LIKE '%0 1' THEN '0-1 Y'
								ELSE INFANT_4_6M_KEYWORD END)
	,INFANT_7_9M_KEYWORD = (CASE WHEN CLEANED_BRANDNAME IN ('PLATYPUS') AND PRODUCT_NAME LIKE '% 0' THEN 'PLATYPUS SIZE 0'
								WHEN CLEANED_BRANDNAME IN ('SPEEDO') AND PRODUCT_NAME LIKE '% 0' THEN 'SPEEDO SIZE 0'
								WHEN PRODUCT_NAME LIKE '%70' THEN 'CLOTHING 70'
								WHEN CLEANED_BRANDNAME LIKE 'SPLASH ABOUT%' AND PRODUCT_NAME LIKE 'BABY WRAP%M' THEN 'SPLASH ABOUT BABY WRAP SIZE M'
								WHEN CLEANED_BRANDNAME LIKE 'SPLASH ABOUT%' AND PRODUCT_NAME LIKE '%HAPPY%NAPPY%L' AND PRODUCT_NAME NOT LIKE '%XL' THEN 'SPLASH ABOUT HAPPY NAPPY SIZE L'
								WHEN CLEANED_BRANDNAME LIKE 'SPLASH ABOUT%' AND PRODUCT_NAME LIKE '%��黻��ṻ���% L' THEN 'SPLASH ABOUT HAPPY NAPPY SIZE L'
								WHEN CLEANED_BRANDNAME LIKE 'SPLASH ABOUT%' AND PRODUCT_NAME LIKE '%�λ���ṻ���% L' THEN 'SPLASH ABOUT HAPPY NAPPY SIZE L'
								WHEN PRODUCT_NAME LIKE '%��ǧ%���¹��%' THEN '��ǧ�����¹��'
								WHEN PRODUCT_NAME LIKE '��ǧ�ʹ��%' THEN '��ǧ�ʹ�����¹��'
								WHEN CLEANED_BRANDNAME LIKE 'CLOSE' AND PRODUCT_NAME LIKE '% L' THEN 'CLOSE SWIM NAPPY SIZE L'
								WHEN CLEANED_BRANDNAME LIKE 'AMELIE' AND PRODUCT_NAME LIKE '%0 1' THEN '0-1 Y'
								WHEN CLEANED_BRANDNAME LIKE 'GUCCI KIDS%' AND PRODUCT_NAME LIKE '%6-9' THEN '6-9 MTH'
								ELSE INFANT_7_9M_KEYWORD END)
	,INFANT_10_12M_KEYWORD = (CASE WHEN CLEANED_BRANDNAME IN ('PLATYPUS') AND PRODUCT_NAME LIKE '% 1' THEN 'PLATYPUS SIZE 1'
								WHEN CLEANED_BRANDNAME IN ('SPEEDO') AND PRODUCT_NAME LIKE '% 1' THEN 'SPEEDO SIZE 1'
								WHEN PRODUCT_NAME LIKE '%1Y' THEN '1Y'
								WHEN PRODUCT_NAME LIKE '%80' THEN 'CLOTHING 80'
								WHEN PRODUCT_NAME LIKE '%12 M' THEN '12 MTH'
								WHEN CLEANED_BRANDNAME LIKE 'SPLASH ABOUT%' AND PRODUCT_NAME LIKE 'BABY WRAP%M' THEN 'SPLASH ABOUT BABY WRAP SIZE M'
								WHEN CLEANED_BRANDNAME LIKE 'SPLASH ABOUT%' AND PRODUCT_NAME LIKE '%HAPPY%NAPPY%L' AND PRODUCT_NAME NOT LIKE '%XL' THEN 'SPLASH ABOUT HAPPY NAPPY SIZE L'
								WHEN CLEANED_BRANDNAME LIKE 'SPLASH ABOUT%' AND PRODUCT_NAME LIKE '%��黻��ṻ���% L' THEN 'SPLASH ABOUT HAPPY NAPPY SIZE L'
								WHEN CLEANED_BRANDNAME LIKE 'SPLASH ABOUT%' AND PRODUCT_NAME LIKE '%�λ���ṻ���% L' THEN 'SPLASH ABOUT HAPPY NAPPY SIZE L'
								WHEN PRODUCT_NAME LIKE '%��ǧ%���¹��%' THEN '��ǧ�����¹��'
								WHEN PRODUCT_NAME LIKE '��ǧ�ʹ��%' THEN '��ǧ�ʹ�����¹��'
								WHEN CLEANED_BRANDNAME LIKE 'CLOSE' AND PRODUCT_NAME LIKE '% L' THEN 'CLOSE SWIM NAPPY SIZE L'
								WHEN CLEANED_BRANDNAME LIKE 'AMELIE' AND PRODUCT_NAME LIKE '%0 1' THEN '0-1 Y'
								WHEN CLEANED_BRANDNAME LIKE 'AMELIE' AND PRODUCT_NAME LIKE '% 1' THEN '1Y'
								WHEN PRODUCT_NAME LIKE '%1T' THEN '1Y'
								ELSE INFANT_10_12M_KEYWORD END)
	,TODDLER_KEYWORD = (CASE WHEN CLEANED_BRANDNAME IN ('PLATYPUS') AND PRODUCT_NAME LIKE '% 2' THEN 'PLATYPUS SIZE 2'
								WHEN CLEANED_BRANDNAME IN ('SPEEDO') AND PRODUCT_NAME LIKE '% 2' THEN 'SPEEDO SIZE 2'
								WHEN PRODUCT_NAME LIKE '%1-2' THEN '1-2 Y'
								WHEN PRODUCT_NAME LIKE '%2Y' THEN '2Y'
								WHEN PRODUCT_NAME LIKE '%18 M' THEN '18 MTH'
								WHEN PRODUCT_NAME LIKE '%24 M' THEN '24 MTH'
								WHEN CLEANED_BRANDNAME LIKE 'I PLAY' AND PRODUCT_NAME LIKE '%24' THEN '24 MTH'
								WHEN CLEANED_BRANDNAME LIKE 'SPLASH ABOUT%' AND PRODUCT_NAME LIKE '�ش���¹��% S%' THEN 'SPLASH ABOUT SWIMWEAR SIZE S'
								WHEN CLEANED_BRANDNAME LIKE 'SPLASH ABOUT%' AND PRODUCT_NAME LIKE 'BABY WRAP%M' THEN 'SPLASH ABOUT BABY WRAP SIZE M'
								WHEN CLEANED_BRANDNAME LIKE 'SPLASH ABOUT%' AND PRODUCT_NAME LIKE 'BABY WRAP%L' THEN 'SPLASH ABOUT BABY WRAP SIZE L'
								WHEN CLEANED_BRANDNAME LIKE 'SPLASH ABOUT%' AND PRODUCT_NAME LIKE '%HAPPY%NAPPY%XL' THEN 'SPLASH ABOUT HAPPY NAPPY SIZE XL'
								WHEN CLEANED_BRANDNAME LIKE 'SPLASH ABOUT%' AND PRODUCT_NAME LIKE '%��黻��ṻ���%XL' THEN 'SPLASH ABOUT HAPPY NAPPY SIZE XL'
								WHEN CLEANED_BRANDNAME LIKE 'SPLASH ABOUT%' AND PRODUCT_NAME LIKE '%�λ���ṻ���%XL' THEN 'SPLASH ABOUT HAPPY NAPPY SIZE XL'
								WHEN CLEANED_BRANDNAME LIKE 'SPLASH ABOUT%' AND PRODUCT_NAME LIKE 'JACKET% S%' THEN 'SPLASH ABOUT FLOAT JACKET SIZE S'
								WHEN CLEANED_BRANDNAME LIKE 'SPLASH ABOUT%' AND PRODUCT_NAME LIKE '���絽֡��µ��%S' THEN 'SPLASH ABOUT FLOAT JACKET SIZE S'
								WHEN CLEANED_BRANDNAME LIKE 'SPLASH ABOUT%' AND PRODUCT_NAME LIKE '�ش�֡��µ��%S%' THEN 'SPLASH ABOUT FLOAT JACKET SIZE S'
								WHEN PRODUCT_NAME LIKE '%��ǧ%���¹��%' THEN '��ǧ�����¹��'
								WHEN PRODUCT_NAME LIKE '��ǧ�ʹ��%' THEN '��ǧ�ʹ�����¹��'
								WHEN CLEANED_BRANDNAME LIKE 'CLOSE' AND PRODUCT_NAME LIKE '% XL' THEN 'CLOSE SWIM NAPPY SIZE XL'
								WHEN CLEANED_BRANDNAME LIKE 'AMELIE' AND PRODUCT_NAME LIKE '%1 2' THEN '1-2 Y'
								WHEN PRODUCT_NAME LIKE '%2A' AND PRODUCT_NAME NOT LIKE '%12A' THEN '2Y'
								WHEN PRODUCT_NAME LIKE '%12-18' THEN '12-18 MTH'
								WHEN PRODUCT_NAME LIKE '%18-24' THEN '18-24 MTH'
								WHEN PRODUCT_NAME LIKE '%18' THEN '18 MTH'
								WHEN PRODUCT_NAME LIKE '%24' THEN '24 MTH'
								WHEN PRODUCT_NAME LIKE '% 02' THEN '2Y'
								WHEN PRODUCT_NAME LIKE '%2T' THEN '2Y'
								WHEN CLEANED_BRANDNAME LIKE 'GUCCI KIDS%' AND PRODUCT_NAME LIKE '% 24' THEN '24 MTH'
								WHEN CLEANED_BRANDNAME LIKE 'GUCCI KIDS%' AND PRODUCT_NAME LIKE '% 18' THEN '18 MTH'
								WHEN CLEANED_BRANDNAME LIKE 'JELLY BEAN' AND PRODUCT_NAME LIKE '% 2' THEN '2Y'
								WHEN PRODUCT_NAME LIKE '%���2' THEN '2Y'
								WHEN CLEANED_BRANDNAME LIKE 'GINGERSNAPS' AND PRODUCT_NAME LIKE '%2' AND PRODUCT_NAME NOT LIKE '%12' THEN '2Y'
								WHEN CLEANED_BRANDNAME LIKE 'GINGERSNAPS' AND PRODUCT_NAME LIKE '% 2CC' THEN '2Y'
								WHEN CLEANED_BRANDNAME LIKE 'GINGERSNAPS' AND PRODUCT_NAME LIKE '% 2HD' THEN '2Y'
								WHEN CLEANED_BRANDNAME LIKE 'GINGERSNAPS' AND PRODUCT_NAME LIKE '% 2WF' THEN '2Y'
								WHEN CLEANED_BRANDNAME LIKE 'PJ KIDS' AND PRODUCT_NAME LIKE '%SIZE 2' THEN '2Y'
								ELSE TODDLER_KEYWORD END)
	,INFANT_UNKNOWN_KEYWORD = (CASE WHEN CLEANED_BRANDNAME LIKE 'SPLASH ABOUT%' AND PRODUCT_NAME LIKE '��ǡ���¹��%S' THEN 'SPLASH ABOUT ��ǡ���¹�� S'
									WHEN PRODUCT_NAME LIKE '%0-2' THEN '0-2 Y'
									WHEN PRODUCT_NAME LIKE '%INFANTCARESET%' THEN 'INFANT CARE SET'
									WHEN CLEANED_BRANDNAME LIKE 'EMPORIO ARMANI' AND CLASS_NAME LIKE 'NEW BORN/GIRLS' THEN 'NEW BORN/GIRLS'
									WHEN CLEANED_BRANDNAME LIKE 'HONEY BUNNY' AND PRODUCT_NAME LIKE '% NB' THEN 'NEWBORN'
									WHEN PRODUCT_NAME LIKE '�ا���຺�� 0 2' THEN '0-2 Y'
									WHEN CLEANED_BRANDNAME LIKE 'SWEET BIRDS' THEN '�ش������ SWEET BIRDS'
									WHEN CLEANED_BRANDNAME LIKE 'TEENY TOES' THEN 'TEENY TOES PREWALK'
									WHEN PRODUCT_NAME LIKE 'B BLENDSET%' THEN 'BABY BLEND SET'
									WHEN PRODUCT_NAME LIKE 'B GIFTSET%' THEN 'BABY GIFTSET'
									WHEN PRODUCT_NAME LIKE 'B JERSEY%' THEN 'BABY JERSEEY'
									WHEN PRODUCT_NAME LIKE 'B JRSEY%' THEN 'BABY JERSEEY'
									ELSE INFANT_UNKNOWN_KEYWORD END)
WHERE MOM_AND_KID_PRODUCT_FLAG = 1
AND COALESCE(PREG_FLAG,'N/A') = 'N/A'
AND COALESCE(INFANT_FLAG,'N/A') = 'N/A'
AND COALESCE(TODDLER_FLAG,'N/A') = 'N/A'
AND COALESCE(PRESCHOOL_FLAG,'N/A') = 'N/A'
AND COALESCE(JUNIOR_FLAG,'N/A') = 'N/A'
AND COALESCE(TEEN_FLAG,'N/A') = 'N/A'
AND REVISED_CATEGORY_LEVEL_1 = 'KIDS'
AND REVISED_CATEGORY_LEVEL_2 = 'KID FASHION'
AND REVISED_CATEGORY_LEVEL_3 = 'KID CLOTHING'
AND ((CLASS_NAME LIKE 'SWIMWEAR' AND PRODUCT_NAME LIKE '% A')
	OR (CLEANED_BRANDNAME IN ('PLATYPUS','SPEEDO') AND PRODUCT_NAME LIKE '% 0')
	OR (CLEANED_BRANDNAME IN ('PLATYPUS','SPEEDO') AND PRODUCT_NAME LIKE '%1' AND PRODUCT_NAME NOT LIKE '%SHOES%31')
	OR (CLEANED_BRANDNAME IN ('PLATYPUS','SPEEDO') AND PRODUCT_NAME LIKE '%2' AND PRODUCT_NAME NOT LIKE '%[0-9][0-9]')
	OR PRODUCT_NAME LIKE '%1-2'
	OR PRODUCT_NAME LIKE '%1Y'
	OR PRODUCT_NAME LIKE '%2Y'
	OR PRODUCT_NAME LIKE '%70'
	OR PRODUCT_NAME LIKE '%80'
	OR PRODUCT_NAME LIKE '%12 M'
	OR PRODUCT_NAME LIKE '%18 M'
	OR PRODUCT_NAME LIKE '%24 M'
	OR (CLEANED_BRANDNAME LIKE 'I PLAY' AND PRODUCT_NAME LIKE '%24')
	OR (CLEANED_BRANDNAME LIKE 'SPLASH ABOUT%' AND PRODUCT_NAME LIKE '�ش���¹��% S%')
	OR (CLEANED_BRANDNAME LIKE 'SPLASH ABOUT%' AND PRODUCT_NAME LIKE 'BABY WRAP%')
	OR (CLEANED_BRANDNAME LIKE 'SPLASH ABOUT%' AND PRODUCT_NAME LIKE '%HAPPY%NAPPY%M')
	OR (CLEANED_BRANDNAME LIKE 'SPLASH ABOUT%' AND PRODUCT_NAME LIKE '%HAPPY%NAPPY% L')
	OR (CLEANED_BRANDNAME LIKE 'SPLASH ABOUT%' AND PRODUCT_NAME LIKE '%��黻��ṻ���% M')
	OR (CLEANED_BRANDNAME LIKE 'SPLASH ABOUT%' AND PRODUCT_NAME LIKE '%�λ���ṻ���% M')
	OR (CLEANED_BRANDNAME LIKE 'SPLASH ABOUT%' AND PRODUCT_NAME LIKE '%��黻��ṻ���% L')
	OR (CLEANED_BRANDNAME LIKE 'SPLASH ABOUT%' AND PRODUCT_NAME LIKE '%�λ���ṻ���% L')
	OR (CLEANED_BRANDNAME LIKE 'SPLASH ABOUT%' AND PRODUCT_NAME LIKE 'JACKET% S%')
	OR (CLEANED_BRANDNAME LIKE 'SPLASH ABOUT%' AND PRODUCT_NAME LIKE '���絽֡��µ��%S')
	OR (CLEANED_BRANDNAME LIKE 'SPLASH ABOUT%' AND PRODUCT_NAME LIKE '�ش�֡��µ��%S%')
	OR (CLEANED_BRANDNAME LIKE 'SPLASH ABOUT%' AND PRODUCT_NAME LIKE '��ǡ���¹��%S')
	OR (CLEANED_BRANDNAME LIKE 'KEED' AND PRODUCT_NAME LIKE '% 60')
	OR PRODUCT_NAME LIKE '%��ǧ%���¹��%'
	OR PRODUCT_NAME LIKE '%ṻ���%60'
	OR PRODUCT_NAME LIKE '��ǧ�ʹ��%'
	OR (CLEANED_BRANDNAME LIKE 'CLOSE' AND (PRODUCT_NAME LIKE '% M'
									OR PRODUCT_NAME LIKE '% L'))
	OR (CLEANED_BRANDNAME LIKE 'AMELIE' AND (PRODUCT_NAME LIKE '%0 1'
									OR PRODUCT_NAME LIKE '% 0'
									OR PRODUCT_NAME LIKE '%0 3'
									OR PRODUCT_NAME LIKE '% 1'
									OR PRODUCT_NAME LIKE '%1 2'))
	OR (PRODUCT_NAME LIKE '%2A' AND PRODUCT_NAME NOT LIKE '%12A')
	OR PRODUCT_NAME LIKE '%0-2'
	OR PRODUCT_NAME LIKE '%18'
	OR PRODUCT_NAME LIKE '%24'
	OR PRODUCT_NAME LIKE '%12-18'
	OR PRODUCT_NAME LIKE '%18-24'
	OR PRODUCT_NAME LIKE '% 02'
	OR PRODUCT_NAME LIKE '%1T'
	OR PRODUCT_NAME LIKE '%2T'
	OR (CLEANED_BRANDNAME LIKE 'GUCCI KIDS%' AND PRODUCT_NAME LIKE '%6-9')
	OR PRODUCT_NAME LIKE '%INFANTCARESET%'
	OR (CLEANED_BRANDNAME LIKE 'EMPORIO ARMANI' AND CLASS_NAME LIKE 'NEW BORN/GIRLS')
	OR (CLEANED_BRANDNAME LIKE 'GUCCI KIDS%' AND (PRODUCT_NAME LIKE '%0-3'
										  OR PRODUCT_NAME LIKE '% 24'
										  OR PRODUCT_NAME LIKE '% 18'))
	OR (CLEANED_BRANDNAME LIKE 'HONEY BUNNY' AND PRODUCT_NAME LIKE '% NB')
	OR (CLEANED_BRANDNAME LIKE 'JELLY BEAN' AND PRODUCT_NAME LIKE '% 2')
	OR PRODUCT_NAME LIKE '%���2'
	OR (CLEANED_BRANDNAME LIKE 'MUFFIN BITE' AND PRODUCT_NAME LIKE '%(40)')
	OR PRODUCT_NAME LIKE '�ا���຺�� 0 2'
	OR CLEANED_BRANDNAME IN ('SWEET BIRDS','TEENY TOES')
	OR (CLEANED_BRANDNAME LIKE 'GINGERSNAPS' AND ((PRODUCT_NAME LIKE '%2' AND PRODUCT_NAME NOT LIKE '%12')
										OR PRODUCT_NAME LIKE '% 2CC'
										OR PRODUCT_NAME LIKE '% 2HD'
										OR PRODUCT_NAME LIKE '% 2WF'))
	OR (CLEANED_BRANDNAME LIKE 'PJ KIDS' AND PRODUCT_NAME LIKE '%SIZE 2')
	OR PRODUCT_NAME LIKE 'B BLENDSET%'
	OR PRODUCT_NAME LIKE 'B GIFTSET%'
	OR PRODUCT_NAME LIKE 'B JERSEY%'
	OR PRODUCT_NAME LIKE 'B JRSEY%'
	)
;

--NEW INFANT/TODDLER
UPDATE vpm_data.pm_skutagging_kids_product_master
SET INFANT_FLAG = 'YES'
	,UNKNOWN_INFANT_SUBSTAGE = (CASE WHEN CLEANED_BRANDNAME LIKE 'CLOSE' AND PRODUCT_NAME LIKE '%RASH%VEST%' THEN NULL
									WHEN CLEANED_BRANDNAME LIKE 'CLOSE' AND PRODUCT_NAME LIKE '%SUN%HAT%' THEN NULL
									ELSE 'YES' END)
	,TODDLER_FLAG = 'YES'
	,INFANT_UNKNOWN_KEYWORD = (CASE WHEN CLEANED_BRANDNAME LIKE 'CLOSE' AND PRODUCT_NAME LIKE 'SWIM NAPPY%' THEN 'CLOSE SWIM NAPPY UNKNOWN SIZE'
									WHEN CLEANED_BRANDNAME LIKE 'CLOSE' AND PRODUCT_NAME NOT LIKE '%RASH%VEST%' AND PRODUCT_NAME NOT LIKE '%SUN%HAT%' THEN 'CLOSE SWIM NAPPY UNKNOWN SIZE'
									WHEN CLEANED_BRANDNAME LIKE 'SPLASH ABOUT%' AND PRODUCT_NAME LIKE '�ش����ٷ �λ���ṻ���%' THEN 'SPLASH ABOUT'
									WHEN CLEANED_BRANDNAME LIKE 'KEED' AND PRODUCT_NAME NOT LIKE '% 60' AND PRODUCT_NAME NOT LIKE '%��蹵����¹��%' THEN 'KEED'
									WHEN PRODUCT_NAME LIKE '%NAPPY%' AND CLEANED_BRANDNAME NOT LIKE 'SPLASH ABOUT%' THEN 'SWIM NAPPY'
									WHEN CLEANED_BRANDNAME LIKE 'I PLAY' AND PRODUCT_NAME LIKE '%PI' THEN 'I PLAY SWIMWEAR'
									WHEN CLEANED_BRANDNAME LIKE 'AMELIE' AND PRODUCT_NAME LIKE '�ا��������������ٻ���' THEN 'AMELIE BABY SOCKS'
									WHEN CLEANED_BRANDNAME LIKE 'GUCCI KIDS%' AND PRODUCT_NAME LIKE '%SLEEPINGBAG%' THEN 'SLEEPING BAG'
									WHEN CLEANED_BRANDNAME LIKE 'JUNIORS' AND DEPT_NAME = 'BABYSHOP' THEN 'BABYSHOP'
									WHEN CLEANED_BRANDNAME LIKE 'MUFFIN BITE PRO1' THEN 'MUFFIN BITE'
									ELSE INFANT_UNKNOWN_KEYWORD END)
	,TODDLER_KEYWORD = (CASE WHEN CLEANED_BRANDNAME LIKE 'CLOSE' AND PRODUCT_NAME LIKE '%RASH%VEST%' THEN 'CLOSE RASH VEST'
								WHEN CLEANED_BRANDNAME LIKE 'CLOSE' AND PRODUCT_NAME LIKE '%SUN%HAT%' THEN 'CLOSE SUN HAT'
								WHEN CLEANED_BRANDNAME LIKE 'CLOSE' THEN 'CLOSE BABY AND TODDLER SWIMWEAR'
								WHEN CLEANED_BRANDNAME LIKE 'SPLASH ABOUT%' AND PRODUCT_NAME LIKE '�ش����ٷ �λ���ṻ���%' THEN 'SPLASH ABOUT'
								WHEN CLEANED_BRANDNAME LIKE 'KEED' AND PRODUCT_NAME NOT LIKE '% 60' AND PRODUCT_NAME NOT LIKE '%��蹵����¹��%' THEN 'KEED'
								WHEN PRODUCT_NAME LIKE '%NAPPY%' AND CLEANED_BRANDNAME NOT LIKE 'SPLASH ABOUT%' THEN 'SWIM NAPPY'
								WHEN CLEANED_BRANDNAME LIKE 'I PLAY' AND PRODUCT_NAME LIKE '%PI' THEN 'I PLAY SWIMWEAR'
								WHEN CLEANED_BRANDNAME LIKE 'AMELIE' AND PRODUCT_NAME LIKE '�ا��������������ٻ���' THEN 'AMELIE BABY SOCKS'
								WHEN CLEANED_BRANDNAME LIKE 'GUCCI KIDS%' AND PRODUCT_NAME LIKE '%SLEEPINGBAG%' THEN 'SLEEPING BAG'
								WHEN CLEANED_BRANDNAME LIKE 'JUNIORS' AND DEPT_NAME = 'BABYSHOP' THEN 'BABYSHOP'
								WHEN CLEANED_BRANDNAME LIKE 'MUFFIN BITE PRO1' THEN 'MUFFIN BITE'
								ELSE TODDLER_KEYWORD END)
WHERE MOM_AND_KID_PRODUCT_FLAG = 1
AND COALESCE(PREG_FLAG,'N/A') = 'N/A'
AND COALESCE(INFANT_FLAG,'N/A') = 'N/A'
AND COALESCE(TODDLER_FLAG,'N/A') = 'N/A'
AND COALESCE(PRESCHOOL_FLAG,'N/A') = 'N/A'
AND COALESCE(JUNIOR_FLAG,'N/A') = 'N/A'
AND COALESCE(TEEN_FLAG,'N/A') = 'N/A'
AND REVISED_CATEGORY_LEVEL_1 = 'KIDS'
AND REVISED_CATEGORY_LEVEL_2 = 'KID FASHION'
AND REVISED_CATEGORY_LEVEL_3 = 'KID CLOTHING'
AND ((CLEANED_BRANDNAME LIKE 'SPLASH ABOUT%' AND (PRODUCT_NAME LIKE '�ش����ٷ �λ���ṻ���%'))
	OR (CLEANED_BRANDNAME LIKE 'KEED' AND PRODUCT_NAME NOT LIKE '% 60' AND PRODUCT_NAME NOT LIKE '%��蹵����¹��%')
	OR (PRODUCT_NAME LIKE '%NAPPY%' AND CLEANED_BRANDNAME NOT LIKE 'SPLASH ABOUT%')
	OR (CLEANED_BRANDNAME LIKE 'I PLAY' AND PRODUCT_NAME LIKE '%PI')
	OR (CLEANED_BRANDNAME LIKE 'CLOSE' AND PRODUCT_NAME NOT LIKE '% S' AND PRODUCT_NAME NOT LIKE '% M' AND PRODUCT_NAME NOT LIKE '% L' AND PRODUCT_NAME NOT LIKE '%XL')
	OR (CLEANED_BRANDNAME LIKE 'AMELIE' AND PRODUCT_NAME LIKE '�ا��������������ٻ���')
	OR (CLEANED_BRANDNAME LIKE 'GUCCI KIDS%' AND PRODUCT_NAME LIKE '%SLEEPINGBAG%')
	OR (CLEANED_BRANDNAME LIKE 'JUNIORS' AND DEPT_NAME = 'BABYSHOP' AND (PRODUCT_NAME LIKE '%DRIBWAISTCUTSEWDENIMPANT%'
																OR PRODUCT_NAME LIKE '%CHECKSHIRTGRYMELANSLEEV%'))
	OR CLEANED_BRANDNAME LIKE 'MUFFIN BITE PRO1')
;

--NEW TODDLER
UPDATE vpm_data.pm_skutagging_kids_product_master
SET TODDLER_FLAG = 'YES'
	,TODDLER_KEYWORD = (CASE WHEN CLASS_NAME LIKE 'SWIMWEAR' AND PRODUCT_NAME LIKE '% B' THEN 'SWIMWEAR SIZE B'
							 WHEN CLASS_NAME LIKE 'SWIMWEAR' AND PRODUCT_NAME LIKE '% C' THEN 'SWIMWEAR SIZE C'
							 WHEN CLEANED_BRANDNAME IN ('PLATYPUS','SPEEDO') AND PRODUCT_NAME LIKE '%3' THEN '3Y'
							 WHEN CLEANED_BRANDNAME IN ('PLATYPUS','SPEEDO') AND PRODUCT_NAME LIKE '%4' AND PRODUCT_NAME NOT LIKE '%[0-9][0-9]' THEN '4Y'
							 WHEN PRODUCT_NAME LIKE '%2-3' THEN '2-3 Y'
							 WHEN PRODUCT_NAME LIKE '%2-3 Y' THEN '2-3 Y'
							 WHEN PRODUCT_NAME LIKE '%3-4 Y' THEN '3-4 Y'
							 WHEN PRODUCT_NAME LIKE '%YE3' THEN '3Y'
							 WHEN PRODUCT_NAME LIKE '%3Y%' THEN '3Y'
							 WHEN PRODUCT_NAME LIKE '%3 YR' THEN '3Y'
							 WHEN PRODUCT_NAME LIKE '%36' THEN '3Y'
							 WHEN PRODUCT_NAME LIKE '%4Y%' AND PRODUCT_NAME NOT LIKE '%14Y' THEN '4Y'
							 WHEN PRODUCT_NAME LIKE '%90' THEN 'SWIMWEAR 90 CM'
							 WHEN PRODUCT_NAME LIKE '%100' THEN 'SWIMWEAR 90 CM'
							 WHEN CLEANED_BRANDNAME LIKE 'SPLASH ABOUT%' AND PRODUCT_NAME LIKE '�ش���¹��% M%' THEN 'SPLASH ABOUT SWIMWEAR SIZE M'
							 WHEN CLEANED_BRANDNAME LIKE 'SPLASH ABOUT%' AND PRODUCT_NAME LIKE '�ش���¹��% L%' THEN 'SPLASH ABOUT SWIMWEAR SIZE L'
							 WHEN CLEANED_BRANDNAME LIKE 'SPLASH ABOUT%' AND PRODUCT_NAME LIKE '%HAPPY%NAPPY%XL' THEN 'SPLASH ABOUT HAPPY NAPPY SIZE XL'
							 WHEN CLEANED_BRANDNAME LIKE 'SPLASH ABOUT%' AND PRODUCT_NAME LIKE '%��黻��ṻ���%XL' THEN 'SPLASH ABOUT HAPPY NAPPY SIZE XL'
							 WHEN CLEANED_BRANDNAME LIKE 'SPLASH ABOUT%' AND PRODUCT_NAME LIKE '%�λ���ṻ���%XL' THEN 'SPLASH ABOUT HAPPY NAPPY SIZE XL'
							 WHEN CLEANED_BRANDNAME LIKE 'SPLASH ABOUT%' AND PRODUCT_NAME LIKE '%�λ���ṻ���%XX' THEN 'SPLASH ABOUT HAPPY NAPPY SIZE XL'
							 WHEN CLEANED_BRANDNAME LIKE 'SPLASH ABOUT%' AND PRODUCT_NAME LIKE 'JACKET% M%' THEN 'SPLASH ABOUT FLOAT JACKET SIZE M'
							 WHEN CLEANED_BRANDNAME LIKE 'SPLASH ABOUT%' AND PRODUCT_NAME LIKE '���絽֡��µ��%M' THEN 'SPLASH ABOUT FLOAT JACKET SIZE M'
							 WHEN CLEANED_BRANDNAME LIKE 'SPLASH ABOUT%' AND PRODUCT_NAME LIKE '�ش�֡��µ��%M%' THEN 'SPLASH ABOUT FLOAT JACKET SIZE M'
							 WHEN CLEANED_BRANDNAME LIKE 'SPLASH ABOUT%' AND PRODUCT_NAME LIKE '��ǡ���¹��%M' THEN 'SPLASH ABOUT ��ǡ���¹�� SIZE M'
							 WHEN CLEANED_BRANDNAME LIKE 'ZOGGS' AND PRODUCT_NAME LIKE '%21' THEN 'ZOGGS 21'
							 WHEN PRODUCT_NAME LIKE '%36M' THEN '36 MTH'
							 WHEN CLEANED_BRANDNAME LIKE 'KEED' AND PRODUCT_NAME LIKE '%��蹵����¹��%' THEN '��蹵����¹���� KEED'
							 WHEN PRODUCT_NAME LIKE '%������ᢹ���¹��%' THEN '��͡ᢹ���¹��'
							 WHEN PRODUCT_NAME LIKE '%��͡ᢹ%' THEN '��͡ᢹ���¹��'
							 WHEN PRODUCT_NAME LIKE '%��͡��%' THEN '��͡�����¹��'
							 WHEN PRODUCT_NAME LIKE '�ػ�ó��ا���¹��%' THEN '�ػ�ó��ا���¹��'
							 WHEN PRODUCT_NAME LIKE '�ػ�ó����¹����%' THEN '�ػ�ó��ا���¹��'
							 WHEN PRODUCT_NAME LIKE '�ػ�ó��ا㹹��%' THEN '�ػ�ó��ا���¹��'
							 WHEN CLEANED_BRANDNAME LIKE 'CLOSE' AND PRODUCT_NAME LIKE '%XL' THEN 'CLOSE SWIMWEAR SIZE XL'
							 WHEN CLEANED_BRANDNAME IN ('STEARNS','SWIMFIN') THEN CONCAT(CLEANED_BRANDNAME,' SWIMWEAR')
							 WHEN CLEANED_BRANDNAME LIKE 'AMELIE' AND PRODUCT_NAME LIKE '% 3' THEN '3Y'
							 WHEN CLEANED_BRANDNAME LIKE 'AMELIE' AND PRODUCT_NAME LIKE '%3 4' THEN '3-4 Y'
							 WHEN CLEANED_BRANDNAME LIKE 'AMELIE' AND PRODUCT_NAME LIKE '%3 5' THEN '3-5 Y'
							 WHEN PRODUCT_NAME LIKE '%04' THEN '4Y'
							 WHEN PRODUCT_NAME LIKE '% 4' THEN '4Y'
							 WHEN PRODUCT_NAME LIKE '%3A' AND PRODUCT_NAME NOT LIKE '%13A' THEN '3Y'
							 WHEN PRODUCT_NAME LIKE '%4A' AND PRODUCT_NAME NOT LIKE '%14A' THEN '4Y'
							 WHEN PRODUCT_NAME LIKE '% 03' THEN '3Y'
							 WHEN PRODUCT_NAME LIKE '% 3' THEN '3Y'
							 WHEN PRODUCT_NAME LIKE '%3T' THEN '3Y'
							 WHEN PRODUCT_NAME LIKE '%4T' THEN '3Y'
							 WHEN PRODUCT_NAME LIKE '%3-4' THEN '3-4 Y'
							 WHEN PRODUCT_NAME LIKE '%3-5' THEN '3-5 Y'
							 WHEN PRODUCT_NAME LIKE '%3-6' THEN '3-6 Y'
							 WHEN CLEANED_BRANDNAME LIKE 'BABY DIOR' AND PRODUCT_NAME LIKE '%2526' THEN 'TIGHTS 25-26'
							 WHEN PRODUCT_NAME LIKE '%2-3Y%' THEN '2-3 Y'
							 WHEN PRODUCT_NAME LIKE '%3-4Y%' THEN '3-4 Y'
							 WHEN CLEANED_BRANDNAME LIKE 'GINGERSNAPS' AND PRODUCT_NAME LIKE '%4' AND PRODUCT_NAME NOT LIKE '%14' THEN '4Y'
							 WHEN CLEANED_BRANDNAME LIKE 'GINGERSNAPS' AND PRODUCT_NAME LIKE '% 4CC' THEN '4Y'
							 WHEN CLEANED_BRANDNAME LIKE 'GINGERSNAPS' AND PRODUCT_NAME LIKE '% 4BE' THEN '4Y'
							 WHEN CLEANED_BRANDNAME LIKE 'GINGERSNAPS' AND PRODUCT_NAME LIKE '% 4WH' THEN '4Y'
							 WHEN CLEANED_BRANDNAME LIKE 'GINGERSNAPS' AND PRODUCT_NAME LIKE '% 4WF' THEN '4Y'
							 WHEN CLEANED_BRANDNAME LIKE 'HONEY BUNNY' AND PRODUCT_NAME LIKE '% 95' THEN 'HONEY BUNNY'
							 WHEN PRODUCT_NAME LIKE '%3 YRS' THEN '3Y'
							 WHEN PRODUCT_NAME LIKE '%3YRS' THEN '3Y'
							 WHEN PRODUCT_NAME LIKE '%3 Y' THEN '3Y'
							 WHEN PRODUCT_NAME LIKE '%03Y' THEN '3Y'
							 WHEN PRODUCT_NAME LIKE '% 3Y %' THEN '3Y'
							 WHEN PRODUCT_NAME LIKE '% 4Y %' THEN '4Y'
							 WHEN PRODUCT_NAME LIKE '%4YBLU' THEN '4Y'
							 WHEN PRODUCT_NAME LIKE '%4Y___' THEN '4Y'
							 WHEN PRODUCT_NAME LIKE '%���4' THEN '4Y'
							 WHEN PRODUCT_NAME LIKE '% 3A %' THEN '3Y'
							 WHEN PRODUCT_NAME LIKE '% 4A %' THEN '4Y'
							 WHEN PRODUCT_NAME LIKE '%4(C2' THEN '4Y'
							 WHEN CLEANED_BRANDNAME LIKE 'UNI FRIEND THAILAND' AND PRODUCT_NAME LIKE '% 60' THEN 'UNI FRIEND 60 (3-4 Y)'
							 ELSE TODDLER_KEYWORD END)
WHERE MOM_AND_KID_PRODUCT_FLAG = 1
AND COALESCE(PREG_FLAG,'N/A') = 'N/A'
AND COALESCE(INFANT_FLAG,'N/A') = 'N/A'
AND COALESCE(TODDLER_FLAG,'N/A') = 'N/A'
AND COALESCE(PRESCHOOL_FLAG,'N/A') = 'N/A'
AND COALESCE(JUNIOR_FLAG,'N/A') = 'N/A'
AND COALESCE(TEEN_FLAG,'N/A') = 'N/A'
AND REVISED_CATEGORY_LEVEL_1 = 'KIDS'
AND REVISED_CATEGORY_LEVEL_2 = 'KID FASHION'
AND REVISED_CATEGORY_LEVEL_3 = 'KID CLOTHING'
AND ((CLASS_NAME LIKE 'SWIMWEAR' AND PRODUCT_NAME LIKE '% B')
	OR (CLASS_NAME LIKE 'SWIMWEAR' AND PRODUCT_NAME LIKE '% C')
	OR (CLEANED_BRANDNAME IN ('PLATYPUS','SPEEDO') AND PRODUCT_NAME LIKE '%3')
	OR (CLEANED_BRANDNAME IN ('PLATYPUS','SPEEDO') AND PRODUCT_NAME LIKE '%4' AND PRODUCT_NAME NOT LIKE '%[0-9][0-9]')
	OR PRODUCT_NAME LIKE '%2-3'
	OR PRODUCT_NAME LIKE '%2-3 Y'
	OR PRODUCT_NAME LIKE '%3-4 Y'
	OR PRODUCT_NAME LIKE '%YE3'
	OR PRODUCT_NAME LIKE '%3Y%'
	OR PRODUCT_NAME LIKE '%3 YR'
	OR PRODUCT_NAME LIKE '%36'
	OR (PRODUCT_NAME LIKE '%4Y%' AND PRODUCT_NAME NOT LIKE '%14Y')
	OR PRODUCT_NAME LIKE '%90'
	OR PRODUCT_NAME LIKE '%100'
	OR (CLEANED_BRANDNAME LIKE 'SPLASH ABOUT%' AND (PRODUCT_NAME LIKE '�ش���¹��% M%'
											OR PRODUCT_NAME LIKE '�ش���¹��% L%'
											OR PRODUCT_NAME LIKE '%HAPPY%NAPPY%XL'
											OR PRODUCT_NAME LIKE '%��黻��ṻ���%XL'
											OR PRODUCT_NAME LIKE '%�λ���ṻ���%XL'
											OR PRODUCT_NAME LIKE '%�λ���ṻ���%XX'
											OR PRODUCT_NAME LIKE 'JACKET% M%'
											OR PRODUCT_NAME LIKE '���絽֡��µ��%M'
											OR PRODUCT_NAME LIKE '�ش�֡��µ��%M%'
											OR PRODUCT_NAME LIKE '��ǡ���¹��%M'))
	OR (CLEANED_BRANDNAME LIKE 'ZOGGS' AND PRODUCT_NAME LIKE '%21')
	OR PRODUCT_NAME LIKE '%36M'
	OR (CLEANED_BRANDNAME LIKE 'KEED' AND PRODUCT_NAME LIKE '%��蹵����¹��%')
	OR PRODUCT_NAME LIKE '%������ᢹ���¹��%'
	OR PRODUCT_NAME LIKE '%��͡ᢹ%'
	OR PRODUCT_NAME LIKE '%��͡��%'
	OR PRODUCT_NAME LIKE '�ػ�ó��ا���¹��%'
	OR PRODUCT_NAME LIKE '�ػ�ó����¹����%'
	OR PRODUCT_NAME LIKE '�ػ�ó��ا㹹��%'
	OR (CLEANED_BRANDNAME LIKE 'CLOSE' AND PRODUCT_NAME LIKE '%XL')
	OR CLEANED_BRANDNAME IN ('STEARNS','SWIMFIN')
	OR (CLEANED_BRANDNAME LIKE 'AMELIE' AND (PRODUCT_NAME LIKE '% 3'
									OR PRODUCT_NAME LIKE '%3 4'
									OR PRODUCT_NAME LIKE '%3 5'))
	OR PRODUCT_NAME LIKE '%04'
	OR PRODUCT_NAME LIKE '% 4'
	OR (PRODUCT_NAME LIKE '%3A' AND PRODUCT_NAME NOT LIKE '%13A')
	OR (PRODUCT_NAME LIKE '%4A' AND PRODUCT_NAME NOT LIKE '%14A')
	OR PRODUCT_NAME LIKE '% 03'
	OR PRODUCT_NAME LIKE '% 3'
	OR PRODUCT_NAME LIKE '%3T'
	OR PRODUCT_NAME LIKE '%4T'
	OR PRODUCT_NAME LIKE '%3-4'
	OR PRODUCT_NAME LIKE '%3-5'
	OR PRODUCT_NAME LIKE '%3-6'
	OR (CLEANED_BRANDNAME LIKE 'BABY DIOR' AND PRODUCT_NAME LIKE '%2526')
	OR PRODUCT_NAME LIKE '%2-3Y%'
	OR PRODUCT_NAME LIKE '%3-4Y%'
	OR (CLEANED_BRANDNAME LIKE 'GINGERSNAPS' AND ((PRODUCT_NAME LIKE '%4' AND PRODUCT_NAME NOT LIKE '%14')
										OR PRODUCT_NAME LIKE '% 4CC'
										OR PRODUCT_NAME LIKE '% 4BE'
										OR PRODUCT_NAME LIKE '% 4WH'
										OR PRODUCT_NAME LIKE '% 4WF'))
	OR (CLEANED_BRANDNAME LIKE 'HONEY BUNNY' AND PRODUCT_NAME LIKE '% 95')
	OR PRODUCT_NAME LIKE '%3 YRS'
	OR PRODUCT_NAME LIKE '%3YRS'
	OR PRODUCT_NAME LIKE '%3 Y'
	OR PRODUCT_NAME LIKE '%03Y'
	OR PRODUCT_NAME LIKE '% 3Y %'
	OR PRODUCT_NAME LIKE '% 4Y %'
	OR PRODUCT_NAME LIKE '%4YBLU'
	OR PRODUCT_NAME LIKE '%4Y___'
	OR PRODUCT_NAME LIKE '%���4'
	OR PRODUCT_NAME LIKE '% 3A %'
	OR PRODUCT_NAME LIKE '% 4A %'
	OR PRODUCT_NAME LIKE '%4(C2'
	OR (CLEANED_BRANDNAME LIKE 'UNI FRIEND THAILAND' AND PRODUCT_NAME LIKE '% 60') --3-4Y
	)
;

--PRESCHOOL
UPDATE vpm_data.pm_skutagging_kids_product_master
SET PRESCHOOL_FLAG = 'YES'
	,PRESCHOOL_KEYWORD = (CASE WHEN CLEANED_BRANDNAME IN ('ELLE PETITE','ELLE PETITE PRO','STREAM LINE','STREAM LINE PRO') AND PRODUCT_NAME LIKE '%1' THEN 'SWIMWEAR SIZE 1'
								WHEN CLEANED_BRANDNAME IN ('ELLE PETITE','ELLE PETITE PRO','STREAM LINE','STREAM LINE PRO') AND PRODUCT_NAME LIKE '%2' THEN 'SWIMWEAR SIZE 2'
								WHEN CLEANED_BRANDNAME IN ('PLATYPUS','SPEEDO') AND PRODUCT_NAME LIKE '%6' AND PRODUCT_NAME NOT LIKE '%[0-9][0-9]' THEN '6Y'
								WHEN CLEANED_BRANDNAME LIKE 'SPEEDO' AND PRODUCT_NAME LIKE '%24' THEN 'SPEEDO 24 INCH'
								WHEN CLEANED_BRANDNAME LIKE 'ZOGGS' AND PRODUCT_NAME LIKE '%19' THEN 'ZOGGS'
								WHEN PRODUCT_NAME LIKE '%4-5' THEN '4-5 Y'
								WHEN PRODUCT_NAME LIKE '%4-5YR' THEN '4-5 Y'
								WHEN PRODUCT_NAME LIKE '%4-5 Y' THEN '4-5 Y'
								WHEN PRODUCT_NAME LIKE '%5-6 Y' THEN '5-6 Y'
								WHEN PRODUCT_NAME LIKE '%5Y' THEN '5Y'
								WHEN PRODUCT_NAME LIKE '%6Y' AND PRODUCT_NAME NOT LIKE '%16Y' THEN '6Y'
								WHEN PRODUCT_NAME LIKE '%110' THEN '110 CM'
								WHEN PRODUCT_NAME LIKE '%120' THEN '110 CM'
								WHEN CLEANED_BRANDNAME LIKE 'SPLASH ABOUT%' AND PRODUCT_NAME LIKE 'JACKET% L%' THEN 'SPLASH ABOUT FLOAT JACKET SIZE L'
								WHEN CLEANED_BRANDNAME LIKE 'SPLASH ABOUT%' AND PRODUCT_NAME LIKE '�ش�֡��µ��%L%' THEN 'SPLASH ABOUT FLOAT JACKET SIZE L'
								WHEN CLEANED_BRANDNAME LIKE 'KEED' AND PRODUCT_NAME LIKE '%��蹵����¹��%' THEN '��蹵����¹��'
								WHEN CLEANED_BRANDNAME LIKE 'ZOGGS' AND PRODUCT_NAME LIKE '%23' THEN '23 INCH'
								WHEN CLEANED_BRANDNAME LIKE 'ZOGGS' AND PRODUCT_NAME LIKE '% 4' THEN '4Y'
								WHEN CLEANED_BRANDNAME LIKE 'BELUGA' THEN 'BELUGA'
								WHEN PRODUCT_NAME LIKE '%��ǧ�ҧ%' THEN '��ǧ�ҧ'
								WHEN PRODUCT_NAME LIKE '%���Ѵ���¹��%' THEN '���Ѵ���¹��'
								WHEN PRODUCT_NAME LIKE '%�͹���¹��%' THEN '�͹���¹��'
								WHEN CLEANED_BRANDNAME LIKE 'STEARNS' THEN 'STEARNS'
								WHEN CLEANED_BRANDNAME LIKE 'SWIMFIN' THEN 'SWIMFIN'
								WHEN CLEANED_BRANDNAME LIKE 'AMELIE' AND PRODUCT_NAME LIKE '%5 7' THEN '5-7 Y'
								WHEN CLEANED_BRANDNAME LIKE 'AMELIE' AND PRODUCT_NAME LIKE '%3 5' THEN '3-5 Y'
								WHEN PRODUCT_NAME LIKE '% 5' THEN '5Y'
								WHEN PRODUCT_NAME LIKE '% 6' THEN '6Y'
								WHEN PRODUCT_NAME LIKE '%06' THEN '6Y'
								WHEN PRODUCT_NAME LIKE '%5A' AND PRODUCT_NAME NOT LIKE '%15A' THEN '5Y'
								WHEN PRODUCT_NAME LIKE '%6A' AND PRODUCT_NAME NOT LIKE '%16A' THEN '6Y'
								WHEN PRODUCT_NAME LIKE '%3-5' THEN '3-5 Y'
								WHEN PRODUCT_NAME LIKE '%3-6' THEN '3-6 Y'
								WHEN PRODUCT_NAME LIKE '%5T' THEN '5Y'
								WHEN PRODUCT_NAME LIKE '%6T' THEN '6Y'
								WHEN PRODUCT_NAME LIKE '%4-6' THEN '4-6 Y'
								WHEN PRODUCT_NAME LIKE '%5-6' THEN '5-6 Y'
								WHEN PRODUCT_NAME LIKE '%120Y' THEN '120 CM'
								WHEN CLEANED_BRANDNAME LIKE 'ANGEL KIDS%' AND PRODUCT_NAME LIKE '%4' THEN '4Y'
								WHEN CLEANED_BRANDNAME LIKE 'ANGEL KIDS%' AND PRODUCT_NAME LIKE '%6' THEN '6Y'
								WHEN CLEANED_BRANDNAME LIKE 'ANGEL KIDS%' AND PRODUCT_NAME LIKE '% XS' THEN 'ANGEL KIDS XS'
								WHEN CLEANED_BRANDNAME LIKE 'ANGEL KIDS%' AND PRODUCT_NAME LIKE '% S' THEN 'ANGEL KIDS S'
								WHEN CLEANED_BRANDNAME LIKE 'ANGEL KIDS%' AND PRODUCT_NAME LIKE '%BLS' THEN 'ANGEL KIDS S'
								WHEN CLEANED_BRANDNAME LIKE 'ANGEL KIDS%' AND PRODUCT_NAME LIKE '%PIS' THEN 'ANGEL KIDS S'
								WHEN CLEANED_BRANDNAME LIKE 'ANGEL KIDS%' AND PRODUCT_NAME LIKE '%WHS' THEN 'ANGEL KIDS S'
								WHEN CLEANED_BRANDNAME LIKE 'BABY DIOR' AND PRODUCT_NAME LIKE '%2728' THEN 'TIGHTS 27-28'
								WHEN CLEANED_BRANDNAME LIKE 'BENETTON 012' THEN 'BENETTON'
								WHEN CLEANED_BRANDNAME LIKE 'CARSON%' AND PRODUCT_NAME LIKE '%-S' THEN 'CARSON S'
								WHEN CLEANED_BRANDNAME LIKE 'CARSON%' AND PRODUCT_NAME LIKE '% S' THEN 'CARSON S'
								WHEN PRODUCT_NAME LIKE '%4-5Y%' THEN '4-5 Y'
								WHEN PRODUCT_NAME LIKE '%5-6Y%' THEN '5-6 Y'
								WHEN CLEANED_BRANDNAME LIKE 'CARTEL PRO' THEN 'CARTEL PRO'
								WHEN CLEANED_BRANDNAME LIKE 'FOF %' AND PRODUCT_NAME LIKE '% S' THEN 'FOF S'
								WHEN CLEANED_BRANDNAME LIKE 'FOF %' AND PRODUCT_NAME LIKE '% M' THEN 'FOF M'
								WHEN CLEANED_BRANDNAME LIKE 'FOF %' AND PRODUCT_NAME LIKE '%1' THEN 'FOF 1'
								WHEN CLEANED_BRANDNAME LIKE 'FOF %' AND PRODUCT_NAME LIKE '%2' THEN 'FOF 2'
								WHEN CLEANED_BRANDNAME LIKE 'FOF %' AND PRODUCT_NAME LIKE '%3' THEN 'FOF 3'
								WHEN CLEANED_BRANDNAME IN ('GEDA BABY','GIGGLES') THEN CLEANED_BRANDNAME
								WHEN CLEANED_BRANDNAME LIKE 'GINGERSNAPS' AND PRODUCT_NAME LIKE '%6' THEN '6Y'
								WHEN CLEANED_BRANDNAME LIKE 'GINGERSNAPS' AND PRODUCT_NAME LIKE '%6CC' THEN '6Y'
								WHEN CLEANED_BRANDNAME LIKE 'GINGERSNAPS' AND PRODUCT_NAME LIKE '%6WF' THEN '6Y'
								WHEN PRODUCT_NAME LIKE '%4-5 Y%' THEN '4-5 Y'
								WHEN PRODUCT_NAME LIKE '% 6Y %' THEN '6Y'
								WHEN PRODUCT_NAME LIKE '%6Y___' THEN '6Y'
								WHEN CLEANED_BRANDNAME LIKE 'JUNIOR GAULTIER PRO' THEN 'JUNIOR GAULTIER'
								WHEN CLEANED_BRANDNAME LIKE 'MALINDA' AND PRODUCT_NAME LIKE '% XS' THEN 'MALINDA XS'
								WHEN CLEANED_BRANDNAME LIKE 'MALINDA' AND PRODUCT_NAME LIKE '% S' THEN 'MALINDA S'
								WHEN PRODUCT_NAME LIKE '%���6' THEN '6Y'
								WHEN CLEANED_BRANDNAME LIKE 'MUFFIN BITE PRO1' THEN 'MUFFIN BITE'
								WHEN PRODUCT_NAME LIKE '% 5A %' THEN '5Y'
								WHEN PRODUCT_NAME LIKE '% 6A %' THEN '6Y'
								WHEN CLEANED_BRANDNAME LIKE 'SOFIA' AND PRODUCT_NAME LIKE '%NO.4(C2' THEN '4Y'
								WHEN CLEANED_BRANDNAME LIKE 'SOFIA' AND PRODUCT_NAME LIKE '%NO4(C2' THEN '4Y'
								WHEN CLEANED_BRANDNAME LIKE 'SOFIA' AND PRODUCT_NAME LIKE '% 4(C2' THEN '4Y'
								WHEN CLEANED_BRANDNAME LIKE 'SOFIA' AND PRODUCT_NAME LIKE '%NO.6(C2' THEN '6Y'
								WHEN CLEANED_BRANDNAME LIKE 'SOFIA' AND PRODUCT_NAME LIKE '%NO6(C2' THEN '6Y'
								WHEN CLEANED_BRANDNAME LIKE 'SOFIA' AND PRODUCT_NAME LIKE '% 6(C2' THEN '6Y'
								WHEN PRODUCT_NAME LIKE '%6(C2' THEN '6Y'
								WHEN PRODUCT_NAME LIKE '%�����%' THEN '�����'
								ELSE PRESCHOOL_KEYWORD END)
WHERE MOM_AND_KID_PRODUCT_FLAG = 1
AND COALESCE(PREG_FLAG,'N/A') = 'N/A'
AND COALESCE(INFANT_FLAG,'N/A') = 'N/A'
AND COALESCE(TODDLER_FLAG,'N/A') = 'N/A'
AND COALESCE(PRESCHOOL_FLAG,'N/A') = 'N/A'
AND COALESCE(JUNIOR_FLAG,'N/A') = 'N/A'
AND COALESCE(TEEN_FLAG,'N/A') = 'N/A'
AND REVISED_CATEGORY_LEVEL_1 = 'KIDS'
AND REVISED_CATEGORY_LEVEL_2 = 'KID FASHION'
AND REVISED_CATEGORY_LEVEL_3 = 'KID CLOTHING'
AND ((CLEANED_BRANDNAME IN ('ELLE PETITE','ELLE PETITE PRO','STREAM LINE','STREAM LINE PRO') AND (PRODUCT_NAME LIKE '%1'
																						OR PRODUCT_NAME LIKE '%2'))
	OR (CLEANED_BRANDNAME IN ('PLATYPUS','SPEEDO') AND PRODUCT_NAME LIKE '%6' AND PRODUCT_NAME NOT LIKE '%[0-9][0-9]')
	OR (CLEANED_BRANDNAME LIKE 'SPEEDO' AND PRODUCT_NAME LIKE '%24')
	OR (CLEANED_BRANDNAME LIKE 'ZOGGS' AND PRODUCT_NAME LIKE '%19')
	OR PRODUCT_NAME LIKE '%4-5'
	OR PRODUCT_NAME LIKE '%4-5YR'
	OR PRODUCT_NAME LIKE '%4-5 Y'
	OR PRODUCT_NAME LIKE '%5-6 Y'
	OR PRODUCT_NAME LIKE '%6(C2'
	OR PRODUCT_NAME LIKE '%5Y'
	OR (PRODUCT_NAME LIKE '%6Y' AND PRODUCT_NAME NOT LIKE '%16Y')
	OR PRODUCT_NAME LIKE '%110'
	OR PRODUCT_NAME LIKE '%120'
	OR (CLEANED_BRANDNAME LIKE 'SPLASH ABOUT%' AND PRODUCT_NAME LIKE 'JACKET% L%')
	OR (CLEANED_BRANDNAME LIKE 'SPLASH ABOUT%' AND PRODUCT_NAME LIKE '�ش�֡��µ��%L%')
	OR (CLEANED_BRANDNAME LIKE 'KEED' AND PRODUCT_NAME LIKE '%��蹵����¹��%')
	OR (CLEANED_BRANDNAME LIKE 'ZOGGS' AND PRODUCT_NAME LIKE '%23')
	OR (CLEANED_BRANDNAME LIKE 'ZOGGS' AND PRODUCT_NAME LIKE '% 4')
	OR CLEANED_BRANDNAME LIKE 'BELUGA'
	OR PRODUCT_NAME LIKE '%��ǧ�ҧ%'
	OR PRODUCT_NAME LIKE '%���Ѵ���¹��%'
	OR PRODUCT_NAME LIKE '%�͹���¹��%'
	OR CLEANED_BRANDNAME LIKE 'STEARNS'
	OR CLEANED_BRANDNAME LIKE 'SWIMFIN'
	OR (CLEANED_BRANDNAME LIKE 'AMELIE' AND (PRODUCT_NAME LIKE '%5 7'
									OR PRODUCT_NAME LIKE '%3 5'))
	OR PRODUCT_NAME LIKE '% 5'
	OR PRODUCT_NAME LIKE '% 6'
	OR PRODUCT_NAME LIKE '%06'
	OR (PRODUCT_NAME LIKE '%5A' AND PRODUCT_NAME NOT LIKE '%15A')
	OR (PRODUCT_NAME LIKE '%6A' AND PRODUCT_NAME NOT LIKE '%16A')
	OR PRODUCT_NAME LIKE '%3-5'
	OR PRODUCT_NAME LIKE '%3-6'
	OR PRODUCT_NAME LIKE '%5T'
	OR PRODUCT_NAME LIKE '%6T'
	OR PRODUCT_NAME LIKE '%4-6'
	OR PRODUCT_NAME LIKE '%5-6'
	OR PRODUCT_NAME LIKE '%120Y'
	OR (CLEANED_BRANDNAME LIKE 'ANGEL KIDS%' AND PRODUCT_NAME LIKE '%4')
	OR (CLEANED_BRANDNAME LIKE 'ANGEL KIDS%' AND PRODUCT_NAME LIKE '%6')
	OR (CLEANED_BRANDNAME LIKE 'ANGEL KIDS%' AND PRODUCT_NAME LIKE '% XS')
	OR (CLEANED_BRANDNAME LIKE 'ANGEL KIDS%' AND PRODUCT_NAME LIKE '% S')
	OR (CLEANED_BRANDNAME LIKE 'ANGEL KIDS%' AND PRODUCT_NAME LIKE '%BLS')
	OR (CLEANED_BRANDNAME LIKE 'ANGEL KIDS%' AND PRODUCT_NAME LIKE '%PIS')
	OR (CLEANED_BRANDNAME LIKE 'ANGEL KIDS%' AND PRODUCT_NAME LIKE '%WHS')
	OR (CLEANED_BRANDNAME LIKE 'BABY DIOR' AND PRODUCT_NAME LIKE '%2728')
	OR CLEANED_BRANDNAME LIKE 'BENETTON 012'
	OR (CLEANED_BRANDNAME LIKE 'CARSON%' AND (PRODUCT_NAME LIKE '%-S'
									  OR PRODUCT_NAME LIKE '% S'))
	OR PRODUCT_NAME LIKE '%4-5Y%'
	OR PRODUCT_NAME LIKE '%5-6Y%'
	OR CLEANED_BRANDNAME LIKE 'CARTEL PRO'
	OR (CLEANED_BRANDNAME LIKE 'FOF %' AND (PRODUCT_NAME LIKE '% S'
								  OR PRODUCT_NAME LIKE '% M'
								  OR PRODUCT_NAME LIKE '%1'
								  OR PRODUCT_NAME LIKE '%2'
								  OR PRODUCT_NAME LIKE '%3'))
	OR CLEANED_BRANDNAME IN ('GEDA BABY','GIGGLES')
	OR (CLEANED_BRANDNAME LIKE 'GINGERSNAPS' AND (PRODUCT_NAME LIKE '%6'
										OR PRODUCT_NAME LIKE '%6CC'
										OR PRODUCT_NAME LIKE '%6WF'))
	OR PRODUCT_NAME LIKE '%4-5 Y%'
	OR PRODUCT_NAME LIKE '% 6Y %'
	OR PRODUCT_NAME LIKE '%6Y___'
	OR CLEANED_BRANDNAME LIKE 'JUNIOR GAULTIER PRO'
	OR (CLEANED_BRANDNAME LIKE 'MALINDA' AND PRODUCT_NAME LIKE '% XS')
	OR (CLEANED_BRANDNAME LIKE 'MALINDA' AND PRODUCT_NAME LIKE '% S')
	OR PRODUCT_NAME LIKE '%���6'
	OR CLEANED_BRANDNAME LIKE 'MUFFIN BITE PRO1'
	OR PRODUCT_NAME LIKE '% 5A %'
	OR PRODUCT_NAME LIKE '% 6A %'
	OR (CLEANED_BRANDNAME LIKE 'SOFIA' AND PRODUCT_NAME LIKE '%NO.4(C2')
	OR (CLEANED_BRANDNAME LIKE 'SOFIA' AND PRODUCT_NAME LIKE '%NO4(C2')
	OR (CLEANED_BRANDNAME LIKE 'SOFIA' AND PRODUCT_NAME LIKE '% 4(C2')
	OR (CLEANED_BRANDNAME LIKE 'SOFIA' AND PRODUCT_NAME LIKE '%NO.6(C2')
	OR (CLEANED_BRANDNAME LIKE 'SOFIA' AND PRODUCT_NAME LIKE '%NO6(C2')
	OR (CLEANED_BRANDNAME LIKE 'SOFIA' AND PRODUCT_NAME LIKE '% 6(C2')
	OR PRODUCT_NAME LIKE '%�����%'
	)
;

--PRESCHOOL/JUNIOR
UPDATE vpm_data.pm_skutagging_kids_product_master
SET PRESCHOOL_FLAG = 'YES'
	,JUNIOR_FLAG = 'YES'
	,PRESCHOOL_KEYWORD = (CASE WHEN CLEANED_BRANDNAME IN ('BLING2O','AIRATORS','BARBIE','BE CHARACTERS','BEAU','CHRCTER','DISNEY','DISNEY JEANS','DISNEY PRINCESS','DISNEY PRINCESS PRO') THEN '����ͼ����'
								WHEN PRODUCT_NAME LIKE '%�����¹��%' THEN '�����¹��'
								WHEN PRODUCT_NAME LIKE '%����%' THEN '�����¹��'
								WHEN PRODUCT_NAME LIKE '%�ͧ���㹹��%' THEN '�ͧ���㹹��'
								WHEN PRODUCT_NAME LIKE '%5-8' THEN '5-8 Y'
								WHEN PRODUCT_NAME LIKE '%5-7' THEN '5-7 Y'
								WHEN CLEANED_BRANDNAME LIKE 'CARSON%' AND PRODUCT_NAME NOT LIKE '%-S'
																AND PRODUCT_NAME NOT LIKE '% S'
																AND PRODUCT_NAME NOT LIKE '%-M'
																AND PRODUCT_NAME NOT LIKE '%-L'
																AND PRODUCT_NAME NOT LIKE '% M'
																AND PRODUCT_NAME NOT LIKE '% L'
																AND PRODUCT_NAME NOT LIKE '%XL'
																AND PRODUCT_NAME NOT LIKE '%2X' THEN 'CARSON'
								WHEN CLEANED_BRANDNAME LIKE 'FOF %' AND PRODUCT_NAME LIKE '% F' THEN 'FOF FREESIZE'
								WHEN CLEANED_BRANDNAME LIKE 'FOF %' AND PRODUCT_NAME LIKE '%6' THEN 'FOF 6'
								WHEN CLEANED_BRANDNAME LIKE 'FOF %' AND PRODUCT_NAME LIKE '%8' THEN 'FOF 8'
								WHEN CLEANED_BRANDNAME IN ('DREAMWORKS','KAZOKU','POOH','PUMPKIN PIE') THEN '����ͼ����'
								WHEN CLEANED_BRANDNAME LIKE 'ROSIE IN BOOTS' AND PRODUCT_NAME NOT LIKE '%[0-9]T' THEN 'ROSIE IN BOOTS'
								WHEN CLEANED_BRANDNAME LIKE 'SIMPLE&CHIC' AND PRODUCT_NAME NOT LIKE '%[0-9]' THEN 'SIMPLE&CHIC'
								WHEN CLEANED_BRANDNAME LIKE 'SOFIA' AND PRODUCT_NAME LIKE '%[0-9][0-9](C2' THEN '����ͼ����'
								ELSE PRESCHOOL_KEYWORD END)
	,JUNIOR_KEYWORD = (CASE WHEN CLEANED_BRANDNAME IN ('BLING2O','AIRATORS','BARBIE','BE CHARACTERS','BEAU','CHRCTER','DISNEY','DISNEY JEANS','DISNEY PRINCESS','DISNEY PRINCESS PRO') THEN '����ͼ����'
								WHEN PRODUCT_NAME LIKE '%�����¹��%' THEN '�����¹��'
								WHEN PRODUCT_NAME LIKE '%����%' THEN '�����¹��'
								WHEN PRODUCT_NAME LIKE '%�ͧ���㹹��%' THEN '�ͧ���㹹��'
								WHEN PRODUCT_NAME LIKE '%5-8' THEN '5-8 Y'
								WHEN PRODUCT_NAME LIKE '%5-7' THEN '5-7 Y'
								WHEN CLEANED_BRANDNAME LIKE 'CARSON%' AND PRODUCT_NAME NOT LIKE '%-S'
																AND PRODUCT_NAME NOT LIKE '% S'
																AND PRODUCT_NAME NOT LIKE '%-M'
																AND PRODUCT_NAME NOT LIKE '%-L'
																AND PRODUCT_NAME NOT LIKE '% M'
																AND PRODUCT_NAME NOT LIKE '% L'
																AND PRODUCT_NAME NOT LIKE '%XL'
																AND PRODUCT_NAME NOT LIKE '%2X' THEN 'CARSON'
								WHEN CLEANED_BRANDNAME LIKE 'FOF %' AND PRODUCT_NAME LIKE '% F' THEN 'FOF FREESIZE'
								WHEN CLEANED_BRANDNAME LIKE 'FOF %' AND PRODUCT_NAME LIKE '%6' THEN 'FOF 6'
								WHEN CLEANED_BRANDNAME LIKE 'FOF %' AND PRODUCT_NAME LIKE '%8' THEN 'FOF 8'
								WHEN CLEANED_BRANDNAME IN ('DREAMWORKS','KAZOKU','POOH','PUMPKIN PIE') THEN '����ͼ����'
								WHEN CLEANED_BRANDNAME LIKE 'ROSIE IN BOOTS' AND PRODUCT_NAME NOT LIKE '%[0-9]T' THEN 'ROSIE IN BOOTS'
								WHEN CLEANED_BRANDNAME LIKE 'SIMPLE&CHIC' AND PRODUCT_NAME NOT LIKE '%[0-9]' THEN 'SIMPLE&CHIC'
								WHEN CLEANED_BRANDNAME LIKE 'SOFIA' AND PRODUCT_NAME LIKE '%[0-9][0-9](C2' THEN '����ͼ����'
								ELSE JUNIOR_KEYWORD END)
WHERE MOM_AND_KID_PRODUCT_FLAG = 1
AND COALESCE(PREG_FLAG,'N/A') = 'N/A'
AND COALESCE(INFANT_FLAG,'N/A') = 'N/A'
AND COALESCE(TODDLER_FLAG,'N/A') = 'N/A'
AND COALESCE(PRESCHOOL_FLAG,'N/A') = 'N/A'
AND COALESCE(JUNIOR_FLAG,'N/A') = 'N/A'
AND COALESCE(TEEN_FLAG,'N/A') = 'N/A'
AND REVISED_CATEGORY_LEVEL_1 = 'KIDS'
AND REVISED_CATEGORY_LEVEL_2 = 'KID FASHION'
AND REVISED_CATEGORY_LEVEL_3 = 'KID CLOTHING'
AND (CLEANED_BRANDNAME IN ('BLING2O','AIRATORS','BARBIE','BE CHARACTERS','BEAU','CHRCTER','DISNEY','DISNEY JEANS','DISNEY PRINCESS','DISNEY PRINCESS PRO')
	OR PRODUCT_NAME LIKE '%�����¹��%'
	OR PRODUCT_NAME LIKE '%����%'
	OR PRODUCT_NAME LIKE '%�ͧ���㹹��%'
	OR PRODUCT_NAME LIKE '%5-8'
	OR PRODUCT_NAME LIKE '%5-7'
	OR (CLEANED_BRANDNAME LIKE 'CARSON%' AND (PRODUCT_NAME NOT LIKE '%-S'
									AND PRODUCT_NAME NOT LIKE '% S'
									AND PRODUCT_NAME NOT LIKE '%-M'
									AND PRODUCT_NAME NOT LIKE '%-L'
									AND PRODUCT_NAME NOT LIKE '% M'
									AND PRODUCT_NAME NOT LIKE '% L'
									AND PRODUCT_NAME NOT LIKE '%XL'
									AND PRODUCT_NAME NOT LIKE '%2X'))
	OR (CLEANED_BRANDNAME LIKE 'FOF %' AND (PRODUCT_NAME LIKE '% F'
								OR PRODUCT_NAME LIKE '%6'
								OR PRODUCT_NAME LIKE '%8'))
	OR CLEANED_BRANDNAME IN ('DREAMWORKS','KAZOKU','POOH','PUMPKIN PIE')
	OR (CLEANED_BRANDNAME LIKE 'ROSIE IN BOOTS' AND PRODUCT_NAME NOT LIKE '%[0-9]T')
	OR (CLEANED_BRANDNAME LIKE 'SIMPLE&CHIC' AND PRODUCT_NAME NOT LIKE '%[0-9]')
	OR (CLEANED_BRANDNAME LIKE 'SOFIA' AND PRODUCT_NAME LIKE '%[0-9][0-9](C2')
	)
;

--JUNIOR
UPDATE vpm_data.pm_skutagging_kids_product_master
SET JUNIOR_FLAG = 'YES'
	,JUNIOR_KEYWORD = (CASE WHEN CLEANED_BRANDNAME IN ('PLATYPUS','SPEEDO') AND PRODUCT_NAME NOT LIKE '%[0-9][0-9]' AND PRODUCT_NAME LIKE '%8' THEN '8Y'
							WHEN CLEANED_BRANDNAME IN ('PLATYPUS','SPEEDO') AND PRODUCT_NAME LIKE '%10' THEN '10Y'
							WHEN CLEANED_BRANDNAME IN ('PLATYPUS','SPEEDO') AND PRODUCT_NAME LIKE '%12' THEN '12Y'
							WHEN CLEANED_BRANDNAME LIKE 'SPEEDO' AND PRODUCT_NAME LIKE '%26' THEN 'SPEEDO 26'
							WHEN CLEANED_BRANDNAME LIKE 'SPEEDO' AND PRODUCT_NAME LIKE '%28' THEN 'SPEEDO 28'
							WHEN CLEANED_BRANDNAME LIKE 'SPEEDO' AND PRODUCT_NAME LIKE '%30' THEN 'SPEEDO 30'
							WHEN CLEANED_BRANDNAME IN ('ELLE PETITE','ELLE PETITE PRO','STREAM LINE','STREAM LINE PRO') AND PRODUCT_NAME LIKE '%3' THEN 'SWIMWEAR SIZE 3'
							WHEN CLEANED_BRANDNAME IN ('ELLE PETITE','ELLE PETITE PRO','STREAM LINE','STREAM LINE PRO') AND PRODUCT_NAME LIKE '%4' THEN 'SWIMWEAR SIZE 4'
							WHEN CLEANED_BRANDNAME IN ('ELLE PETITE','ELLE PETITE PRO','STREAM LINE','STREAM LINE PRO') AND PRODUCT_NAME LIKE '%5' THEN 'SWIMWEAR SIZE 5'
							WHEN CLEANED_BRANDNAME IN ('ELLE PETITE','ELLE PETITE PRO','STREAM LINE','STREAM LINE PRO') AND PRODUCT_NAME LIKE '%6' THEN 'SWIMWEAR SIZE 6'
							WHEN CLEANED_BRANDNAME LIKE 'ZOGGS' AND PRODUCT_NAME LIKE '%26' THEN 'ZOGGS 26'
							WHEN PRODUCT_NAME LIKE '%8Y' THEN '8Y'
							WHEN PRODUCT_NAME LIKE '%10Y' THEN '10Y'
							WHEN PRODUCT_NAME LIKE '%12Y' THEN '12Y'
							WHEN PRODUCT_NAME LIKE '%130' THEN '130 CM'
							WHEN PRODUCT_NAME LIKE '%140' THEN '140 CM'
							WHEN PRODUCT_NAME LIKE '%150' THEN '150 CM'
							WHEN PRODUCT_NAME LIKE '%8A' THEN '8Y'
							WHEN PRODUCT_NAME LIKE '%10A' THEN '10Y'
							WHEN PRODUCT_NAME LIKE '%12A' THEN '12Y'
							WHEN PRODUCT_NAME LIKE '% 7' THEN '7Y'
							WHEN PRODUCT_NAME LIKE '%08' THEN '8Y'
							WHEN PRODUCT_NAME LIKE '% 8' THEN '8Y'
							WHEN PRODUCT_NAME LIKE '% 9' THEN '9Y'
							WHEN PRODUCT_NAME LIKE '%10' THEN '10Y'
							WHEN PRODUCT_NAME LIKE '%11' THEN '11Y'
							WHEN PRODUCT_NAME LIKE '%12' THEN '12Y'
							WHEN PRODUCT_NAME LIKE '%13' THEN '13Y'
							WHEN PRODUCT_NAME LIKE '%6-7' THEN '6-7 Y'
							WHEN PRODUCT_NAME LIKE '%6-7Y%' THEN '6-7 Y'
							WHEN PRODUCT_NAME LIKE '%6-7 Y%' THEN '6-7 Y'
							WHEN PRODUCT_NAME LIKE '%7A' THEN '7Y'
							WHEN PRODUCT_NAME LIKE '%8A' THEN '8Y'
							WHEN PRODUCT_NAME LIKE '%9A' THEN '9Y'
							WHEN PRODUCT_NAME LIKE '%10A' THEN '10Y'
							WHEN PRODUCT_NAME LIKE '%11A' THEN '11Y'
							WHEN PRODUCT_NAME LIKE '%12A' THEN '12Y'
							WHEN PRODUCT_NAME LIKE '%13A' THEN '13Y'
							WHEN PRODUCT_NAME LIKE '%6-8' THEN '6-8 Y'
							WHEN PRODUCT_NAME LIKE '%7-8' THEN '7-8 Y'
							WHEN PRODUCT_NAME LIKE '%7-9' THEN '7-9 Y'
							WHEN PRODUCT_NAME LIKE '%7 9Y' THEN '7-9 Y'
							WHEN PRODUCT_NAME LIKE '%8-9Y' THEN '8-9 Y'
							WHEN PRODUCT_NAME LIKE '%130Y' THEN '130 CM'
							WHEN PRODUCT_NAME LIKE '%140Y' THEN '140 CM'
							WHEN PRODUCT_NAME LIKE '%150Y' THEN '150 CM'
							WHEN CLEANED_BRANDNAME LIKE 'ANGEL KIDS%' AND PRODUCT_NAME LIKE '%8' THEN '8Y'
							WHEN CLEANED_BRANDNAME LIKE 'ANGEL KIDS%' AND PRODUCT_NAME LIKE '% M' THEN 'ANGEL KIDS M'
							WHEN CLEANED_BRANDNAME LIKE 'ANGEL KIDS%' AND PRODUCT_NAME LIKE '% L' THEN 'ANGEL KIDS L'
							WHEN CLEANED_BRANDNAME LIKE 'ANGEL KIDS%' AND PRODUCT_NAME LIKE '% X' THEN 'ANGEL KIDS XL'
							WHEN CLEANED_BRANDNAME LIKE 'ANGEL KIDS%' AND PRODUCT_NAME LIKE '%WHM' THEN 'ANGEL KIDS M'
							WHEN CLEANED_BRANDNAME LIKE 'ANGEL KIDS%' AND PRODUCT_NAME LIKE '%WHL' THEN 'ANGEL KIDS L'
							WHEN CLEANED_BRANDNAME LIKE 'ANGEL KIDS%' AND PRODUCT_NAME LIKE '%PIM' THEN 'ANGEL KIDS M'
							WHEN CLEANED_BRANDNAME LIKE 'ANGEL KIDS%' AND PRODUCT_NAME LIKE '%PIL' THEN 'ANGEL KIDS L'
							WHEN CLEANED_BRANDNAME LIKE 'ANGEL KIDS%' AND PRODUCT_NAME LIKE '%BLM' THEN 'ANGEL KIDS M'
							WHEN CLEANED_BRANDNAME LIKE 'ANGEL KIDS%' AND PRODUCT_NAME LIKE '%BLL' THEN 'ANGEL KIDS L'
							WHEN CLEANED_BRANDNAME LIKE 'ANGEL%' AND PRODUCT_NAME LIKE '%XL' THEN 'ANGEL KIDS XL'
							WHEN CLEANED_BRANDNAME LIKE 'ANGEL%' AND PRODUCT_NAME LIKE '%XX' THEN 'ANGEL KIDS XL'
							WHEN CLEANED_BRANDNAME LIKE 'BABY DIOR' AND PRODUCT_NAME LIKE '%3334' THEN 'TIGHTS 33-34'
							WHEN CLEANED_BRANDNAME LIKE 'CARSON%' AND PRODUCT_NAME LIKE '%-M' THEN 'CARSON M'
							WHEN CLEANED_BRANDNAME LIKE 'CARSON%' AND PRODUCT_NAME LIKE '% M' THEN 'CARSON M'
							WHEN CLEANED_BRANDNAME LIKE 'CARSON%' AND PRODUCT_NAME LIKE '%-L' THEN 'CARSON L'
							WHEN CLEANED_BRANDNAME LIKE 'CARSON%' AND PRODUCT_NAME LIKE '% L' THEN 'CARSON L'
							WHEN CLEANED_BRANDNAME LIKE 'CARSON%' AND PRODUCT_NAME LIKE '%XL' THEN 'CARSON XL'
							WHEN CLEANED_BRANDNAME LIKE 'CARSON%' AND PRODUCT_NAME LIKE '%2X' THEN 'CARSON 2XL'
							WHEN PRODUCT_NAME LIKE '%6-7Y%' THEN '6-7 Y'
							WHEN PRODUCT_NAME LIKE '%7-8Y%' THEN '7-8 Y'
							WHEN PRODUCT_NAME LIKE '%12L' THEN '12Y'
							WHEN CLEANED_BRANDNAME = 'DOUBLEGOOSE' AND PRODUCT_NAME LIKE '%SIZE S%' THEN 'DOUBLEGOOSE S'
							WHEN CLEANED_BRANDNAME = 'DOUBLEGOOSE' AND PRODUCT_NAME LIKE '%26XS' THEN 'DOUBLEGOOSE 26'
							WHEN CLEANED_BRANDNAME = 'DOUBLEGOOSE' AND PRODUCT_NAME LIKE '%28S' THEN 'DOUBLEGOOSE 28'
							WHEN CLEANED_BRANDNAME = 'DOUBLEGOOSE' AND PRODUCT_NAME LIKE '% S' THEN 'DOUBLEGOOSE S'
							WHEN CLEANED_BRANDNAME = 'DOUBLEGOOSE' AND PRODUCT_NAME LIKE '% M' THEN 'DOUBLEGOOSE M'
							WHEN CLEANED_BRANDNAME = 'DOUBLEGOOSE' AND PRODUCT_NAME LIKE '%30M' THEN 'DOUBLEGOOSE M'
							WHEN CLEANED_BRANDNAME = 'DOUBLEGOOSE' AND PRODUCT_NAME LIKE '%M/30' THEN 'DOUBLEGOOSE M'
							WHEN PRODUCT_NAME LIKE '% 8Y %' THEN '8Y'
							WHEN PRODUCT_NAME LIKE '%8-9Y%' THEN '8-9 Y'
							WHEN PRODUCT_NAME LIKE '%9-10Y%' THEN '9-10 Y'
							WHEN PRODUCT_NAME LIKE '%10-11Y%' THEN '10-11 Y'
							WHEN PRODUCT_NAME LIKE '%11-12Y%' THEN '11-12 Y'
							WHEN PRODUCT_NAME LIKE '%12-13Y%' THEN '12-13 Y'
							WHEN CLEANED_BRANDNAME LIKE 'EMPORIO ARMANI' AND CLASS_NAME LIKE 'JUNIOR%' THEN 'EMPORIO ARMANI JUNIOR'
							WHEN CLEANED_BRANDNAME LIKE 'FOF %' AND PRODUCT_NAME LIKE '% L' THEN 'FOF L'
							WHEN CLEANED_BRANDNAME LIKE 'FOF %' AND PRODUCT_NAME LIKE '% XL' THEN 'FOF XL'
							WHEN CLEANED_BRANDNAME LIKE 'GINGERSNAPS' AND PRODUCT_NAME LIKE '%8' THEN '8Y'
							WHEN CLEANED_BRANDNAME LIKE 'GINGERSNAPS' AND PRODUCT_NAME LIKE '%8CC' THEN '8Y'
							WHEN CLEANED_BRANDNAME LIKE 'GINGERSNAPS' AND PRODUCT_NAME LIKE '%8AL' THEN '8Y'
							WHEN CLEANED_BRANDNAME LIKE 'GINGERSNAPS' AND PRODUCT_NAME LIKE '%8HD' THEN '8Y'
							WHEN CLEANED_BRANDNAME LIKE 'GINGERSNAPS' AND PRODUCT_NAME LIKE '%8WF' THEN '8Y'
							WHEN CLEANED_BRANDNAME LIKE 'GINGERSNAPS' AND PRODUCT_NAME LIKE '%10AL' THEN '10Y'
							WHEN CLEANED_BRANDNAME LIKE 'GINGERSNAPS' AND PRODUCT_NAME LIKE '%10HD' THEN '10Y'
							WHEN CLEANED_BRANDNAME LIKE 'GINGERSNAPS' AND PRODUCT_NAME LIKE '%10WF' THEN '10Y'
							WHEN CLEANED_BRANDNAME LIKE 'GINGERSNAPS' AND PRODUCT_NAME LIKE '%10CC' THEN '10Y'
							WHEN CLEANED_BRANDNAME LIKE 'GINGERSNAPS' AND PRODUCT_NAME LIKE '%12AL' THEN '12Y'
							WHEN CLEANED_BRANDNAME LIKE 'GINGERSNAPS' AND PRODUCT_NAME LIKE '%12WF' THEN '12Y'
							WHEN CLEANED_BRANDNAME LIKE 'GINGERSNAPS' AND PRODUCT_NAME LIKE '%12CC' THEN '12Y'
							WHEN PRODUCT_NAME LIKE '%7TO9' THEN '7-9 Y'
							WHEN PRODUCT_NAME LIKE '% 8 Y %' THEN '8Y'
							WHEN PRODUCT_NAME LIKE '%8Y%' THEN '8Y'
							WHEN CLEANED_BRANDNAME LIKE 'MALINDA' AND PRODUCT_NAME LIKE '% M' THEN 'MALINDA M'
							WHEN CLEANED_BRANDNAME LIKE 'MALINDA' AND PRODUCT_NAME LIKE '% XL' THEN 'MALINDA XL'
							WHEN CLEANED_BRANDNAME LIKE 'MARNI' AND CLASS_NAME LIKE 'JUNIOR/GIRLS' THEN 'MARNI JUNIOR'
							WHEN PRODUCT_NAME LIKE '%���8' THEN '8Y'
							WHEN PRODUCT_NAME LIKE '% 8A %' THEN '8Y'
							WHEN PRODUCT_NAME LIKE '% 10A %' THEN '10Y'
							WHEN PRODUCT_NAME LIKE '% 12A %' THEN '12Y'
							WHEN PRODUCT_NAME LIKE '% 13A %' THEN '13Y'
							WHEN CLEANED_BRANDNAME LIKE 'SOFIA' AND PRODUCT_NAME LIKE '%NO.8(C2' THEN '8Y'
							WHEN CLEANED_BRANDNAME LIKE 'SOFIA' AND PRODUCT_NAME LIKE '%NO8(C2' THEN '8Y'
							WHEN CLEANED_BRANDNAME LIKE 'SOFIA' AND PRODUCT_NAME LIKE '% 8(C2' THEN '8Y'
							WHEN PRODUCT_NAME LIKE '%8(C2' THEN '8Y'
							WHEN PRODUCT_NAME LIKE '%10(C2' THEN '10Y'
							WHEN PRODUCT_NAME LIKE '%12(C2' THEN '12Y'
							ELSE JUNIOR_KEYWORD END)
WHERE MOM_AND_KID_PRODUCT_FLAG = 1
AND COALESCE(PREG_FLAG,'N/A') = 'N/A'
AND COALESCE(INFANT_FLAG,'N/A') = 'N/A'
AND COALESCE(TODDLER_FLAG,'N/A') = 'N/A'
AND COALESCE(PRESCHOOL_FLAG,'N/A') = 'N/A'
AND COALESCE(JUNIOR_FLAG,'N/A') = 'N/A'
AND COALESCE(TEEN_FLAG,'N/A') = 'N/A'
AND REVISED_CATEGORY_LEVEL_1 = 'KIDS'
AND REVISED_CATEGORY_LEVEL_2 = 'KID FASHION'
AND REVISED_CATEGORY_LEVEL_3 = 'KID CLOTHING'
AND ((CLEANED_BRANDNAME IN ('PLATYPUS','SPEEDO') AND PRODUCT_NAME NOT LIKE '%[0-9][0-9]' AND PRODUCT_NAME LIKE '%8')
	OR (CLEANED_BRANDNAME IN ('PLATYPUS','SPEEDO') AND PRODUCT_NAME LIKE '%10')
	OR (CLEANED_BRANDNAME IN ('PLATYPUS','SPEEDO') AND PRODUCT_NAME LIKE '%12')
	OR (CLEANED_BRANDNAME LIKE 'SPEEDO' AND (PRODUCT_NAME LIKE '%26'
									OR PRODUCT_NAME LIKE '%28'
									OR PRODUCT_NAME LIKE '%30'))
	OR (CLEANED_BRANDNAME IN ('ELLE PETITE','ELLE PETITE PRO','STREAM LINE','STREAM LINE PRO') AND (PRODUCT_NAME LIKE '%3'
																						OR PRODUCT_NAME LIKE '%4'
																						OR PRODUCT_NAME LIKE '%5'
																						OR PRODUCT_NAME LIKE '%6'))
	OR (CLEANED_BRANDNAME LIKE 'ZOGGS' AND PRODUCT_NAME LIKE '%26')
	OR PRODUCT_NAME LIKE '%8(C2'
	OR PRODUCT_NAME LIKE '%10(C2'
	OR PRODUCT_NAME LIKE '%12(C2'
	OR PRODUCT_NAME LIKE '%8Y'
	OR PRODUCT_NAME LIKE '%10Y'
	OR PRODUCT_NAME LIKE '%12Y'
	OR PRODUCT_NAME LIKE '%130'
	OR PRODUCT_NAME LIKE '%140'
	OR PRODUCT_NAME LIKE '%150'
	OR PRODUCT_NAME LIKE '%8A'
	OR PRODUCT_NAME LIKE '%10A'
	OR PRODUCT_NAME LIKE '%12A'
	OR PRODUCT_NAME LIKE '% 7'
	OR PRODUCT_NAME LIKE '%08'
	OR PRODUCT_NAME LIKE '% 8'
	OR PRODUCT_NAME LIKE '% 9'
	OR PRODUCT_NAME LIKE '%10'
	OR PRODUCT_NAME LIKE '%11'
	OR PRODUCT_NAME LIKE '%12'
	OR PRODUCT_NAME LIKE '%13'
	OR PRODUCT_NAME LIKE '%6-7'
	OR PRODUCT_NAME LIKE '%6-7Y%'
	OR PRODUCT_NAME LIKE '%6-7 Y%'
	OR PRODUCT_NAME LIKE '%7A'
	OR PRODUCT_NAME LIKE '%8A'
	OR PRODUCT_NAME LIKE '%9A'
	OR PRODUCT_NAME LIKE '%10A'
	OR PRODUCT_NAME LIKE '%11A'
	OR PRODUCT_NAME LIKE '%12A'
	OR PRODUCT_NAME LIKE '%13A'
	OR PRODUCT_NAME LIKE '%6-8'
	OR PRODUCT_NAME LIKE '%7-8'
	OR PRODUCT_NAME LIKE '%7-9'
	OR PRODUCT_NAME LIKE '%8-9Y'
	OR PRODUCT_NAME LIKE '%7 9Y'
	OR PRODUCT_NAME LIKE '%130Y'
	OR PRODUCT_NAME LIKE '%140Y'
	OR PRODUCT_NAME LIKE '%150Y'
	OR (CLEANED_BRANDNAME LIKE 'ANGEL KIDS%' AND PRODUCT_NAME LIKE '%8')
	OR (CLEANED_BRANDNAME LIKE 'ANGEL KIDS%' AND PRODUCT_NAME LIKE '% M')
	OR (CLEANED_BRANDNAME LIKE 'ANGEL KIDS%' AND PRODUCT_NAME LIKE '% L')
	OR (CLEANED_BRANDNAME LIKE 'ANGEL KIDS%' AND PRODUCT_NAME LIKE '% X')
	OR (CLEANED_BRANDNAME LIKE 'ANGEL KIDS%' AND PRODUCT_NAME LIKE '%WHM')
	OR (CLEANED_BRANDNAME LIKE 'ANGEL KIDS%' AND PRODUCT_NAME LIKE '%WHL')
	OR (CLEANED_BRANDNAME LIKE 'ANGEL KIDS%' AND PRODUCT_NAME LIKE '%PIM')
	OR (CLEANED_BRANDNAME LIKE 'ANGEL KIDS%' AND PRODUCT_NAME LIKE '%PIL')
	OR (CLEANED_BRANDNAME LIKE 'ANGEL KIDS%' AND PRODUCT_NAME LIKE '%BLM')
	OR (CLEANED_BRANDNAME LIKE 'ANGEL KIDS%' AND PRODUCT_NAME LIKE '%BLL')
	OR (CLEANED_BRANDNAME LIKE 'ANGEL%' AND PRODUCT_NAME LIKE '%XL')
	OR (CLEANED_BRANDNAME LIKE 'ANGEL%' AND PRODUCT_NAME LIKE '%XX')
	OR (CLEANED_BRANDNAME LIKE 'BABY DIOR' AND PRODUCT_NAME LIKE '%3334')
	OR (CLEANED_BRANDNAME LIKE 'CARSON%' AND (PRODUCT_NAME LIKE '%-M'
									OR PRODUCT_NAME LIKE '%-L'
									OR PRODUCT_NAME LIKE '% M'
									OR PRODUCT_NAME LIKE '% L'
									OR PRODUCT_NAME LIKE '%XL'
									OR PRODUCT_NAME LIKE '%2X'))
	OR PRODUCT_NAME LIKE '%6-7Y%'
	OR PRODUCT_NAME LIKE '%7-8Y%'
	OR PRODUCT_NAME LIKE '%12L'
	OR (CLEANED_BRANDNAME = 'DOUBLEGOOSE' AND (PRODUCT_NAME LIKE '%SIZE S%'
										OR PRODUCT_NAME LIKE '%26XS'
										OR PRODUCT_NAME LIKE '%28S'
										OR PRODUCT_NAME LIKE '% S'
										OR PRODUCT_NAME LIKE '% M'
										OR PRODUCT_NAME LIKE '%30M'
										OR PRODUCT_NAME LIKE '%M/30'))
	OR PRODUCT_NAME LIKE '% 8Y %'
	OR PRODUCT_NAME LIKE '%8-9Y%'
	OR PRODUCT_NAME LIKE '%9-10Y%'
	OR PRODUCT_NAME LIKE '%10-11Y%'
	OR PRODUCT_NAME LIKE '%11-12Y%'
	OR PRODUCT_NAME LIKE '%12-13Y%'
	OR (CLEANED_BRANDNAME LIKE 'EMPORIO ARMANI' AND CLASS_NAME LIKE 'JUNIOR%')
	OR (CLEANED_BRANDNAME LIKE 'FOF %' AND (PRODUCT_NAME LIKE '% L'
								OR PRODUCT_NAME LIKE '% XL'))
	OR (CLEANED_BRANDNAME LIKE 'GINGERSNAPS' AND (PRODUCT_NAME LIKE '%8'
										OR PRODUCT_NAME LIKE '%8CC'
										OR PRODUCT_NAME LIKE '%8AL'
										OR PRODUCT_NAME LIKE '%8HD'
										OR PRODUCT_NAME LIKE '%8WF'
										OR PRODUCT_NAME LIKE '%10AL'
										OR PRODUCT_NAME LIKE '%10HD'
										OR PRODUCT_NAME LIKE '%10WF'
										OR PRODUCT_NAME LIKE '%10CC'
										OR PRODUCT_NAME LIKE '%12AL'
										OR PRODUCT_NAME LIKE '%12WF'
										OR PRODUCT_NAME LIKE '%12CC'))
	OR PRODUCT_NAME LIKE '%7TO9'
	OR PRODUCT_NAME LIKE '% 8 Y %'
	OR PRODUCT_NAME LIKE '%8Y%'
	OR (CLEANED_BRANDNAME LIKE 'MALINDA' AND PRODUCT_NAME LIKE '% M')
	OR (CLEANED_BRANDNAME LIKE 'MALINDA' AND PRODUCT_NAME LIKE '% XL')
	OR (CLEANED_BRANDNAME LIKE 'MARNI' AND CLASS_NAME LIKE 'JUNIOR/GIRLS')
	OR PRODUCT_NAME LIKE '%���8'
	OR PRODUCT_NAME LIKE '% 8A %'
	OR PRODUCT_NAME LIKE '% 10A %'
	OR PRODUCT_NAME LIKE '% 12A %'
	OR PRODUCT_NAME LIKE '% 13A %'
	OR (CLEANED_BRANDNAME LIKE 'SOFIA' AND PRODUCT_NAME LIKE '%NO.8(C2')
	OR (CLEANED_BRANDNAME LIKE 'SOFIA' AND PRODUCT_NAME LIKE '%NO8(C2')
	OR (CLEANED_BRANDNAME LIKE 'SOFIA' AND PRODUCT_NAME LIKE '% 8(C2')
	)
;

--JUNIOR/TEEN
UPDATE vpm_data.pm_skutagging_kids_product_master
SET JUNIOR_FLAG = 'YES'
	,TEEN_FLAG = 'YES'
	,JUNIOR_KEYWORD = (CASE WHEN CLEANED_BRANDNAME IN ('PLATYPUS','SPEEDO') AND PRODUCT_NAME LIKE '%6 14Y' THEN '6-14 Y'
							WHEN CLEANED_BRANDNAME IN ('PLATYPUS','SPEEDO') AND PRODUCT_NAME LIKE '%6-14Y' THEN '6-14 Y'
							WHEN PRODUCT_NAME LIKE '%˹�ҡҡ�ӹ��%' THEN '˹�ҡҡ�ӹ��'
							WHEN PRODUCT_NAME LIKE '%�������%' THEN '�������'
							WHEN PRODUCT_NAME LIKE '%���%' THEN '���'
							WHEN CLEANED_BRANDNAME IN ('A II Z XII') THEN '���'
							WHEN CLEANED_BRANDNAME LIKE 'BABY DIOR' AND PRODUCT_NAME LIKE '%12+' THEN '12Y+'
							WHEN CLEANED_BRANDNAME LIKE 'DEFRY 01 KIDS' THEN '����ͼ�����'
							WHEN CLEANED_BRANDNAME LIKE 'ONONO' THEN '����ͼ�����'
							ELSE JUNIOR_KEYWORD END)
	,TEEN_KEYWORD = (CASE WHEN CLEANED_BRANDNAME IN ('PLATYPUS','SPEEDO') AND PRODUCT_NAME LIKE '%6 14Y' THEN '6-14 Y'
							WHEN CLEANED_BRANDNAME IN ('PLATYPUS','SPEEDO') AND PRODUCT_NAME LIKE '%6-14Y' THEN '6-14 Y'
							WHEN PRODUCT_NAME LIKE '%˹�ҡҡ�ӹ��%' THEN '˹�ҡҡ�ӹ��'
							WHEN PRODUCT_NAME LIKE '%�������%' THEN '�������'
							WHEN PRODUCT_NAME LIKE '%���%' THEN '���'
							WHEN CLEANED_BRANDNAME IN ('A II Z XII') THEN '���'
							WHEN CLEANED_BRANDNAME LIKE 'BABY DIOR' AND PRODUCT_NAME LIKE '%12+' THEN '12Y+'
							WHEN CLEANED_BRANDNAME LIKE 'DEFRY 01 KIDS' THEN '����ͼ�����'
							WHEN CLEANED_BRANDNAME LIKE 'ONONO' THEN '����ͼ�����'
							ELSE TEEN_KEYWORD END)
WHERE MOM_AND_KID_PRODUCT_FLAG = 1
AND COALESCE(PREG_FLAG,'N/A') = 'N/A'
AND COALESCE(INFANT_FLAG,'N/A') = 'N/A'
AND COALESCE(TODDLER_FLAG,'N/A') = 'N/A'
AND COALESCE(PRESCHOOL_FLAG,'N/A') = 'N/A'
AND COALESCE(JUNIOR_FLAG,'N/A') = 'N/A'
AND COALESCE(TEEN_FLAG,'N/A') = 'N/A'
AND REVISED_CATEGORY_LEVEL_1 = 'KIDS'
AND REVISED_CATEGORY_LEVEL_2 = 'KID FASHION'
AND REVISED_CATEGORY_LEVEL_3 = 'KID CLOTHING'
AND ((CLEANED_BRANDNAME IN ('PLATYPUS','SPEEDO') AND PRODUCT_NAME LIKE '%6 14Y')
	OR (CLEANED_BRANDNAME IN ('PLATYPUS','SPEEDO') AND PRODUCT_NAME LIKE '%6-14Y')
	OR PRODUCT_NAME LIKE '%˹�ҡҡ�ӹ��%'
	OR PRODUCT_NAME LIKE '%�������%'
	OR PRODUCT_NAME LIKE '%���%'
	OR CLEANED_BRANDNAME IN ('A II Z XII')
	OR (CLEANED_BRANDNAME LIKE 'BABY DIOR' AND PRODUCT_NAME LIKE '%12+')
	OR CLEANED_BRANDNAME LIKE 'DEFRY 01 KIDS'
	OR CLEANED_BRANDNAME LIKE 'ONONO'
	)
;

--TEEN
UPDATE vpm_data.pm_skutagging_kids_product_master
SET TEEN_FLAG = 'YES'
	,TEEN_KEYWORD = (CASE WHEN CLEANED_BRANDNAME IN ('PLATYPUS','SPEEDO') AND PRODUCT_NAME LIKE '%14' THEN '14Y'
						  WHEN CLEANED_BRANDNAME IN ('PLATYPUS','SPEEDO') AND PRODUCT_NAME LIKE '%15' THEN '15Y'
						  WHEN CLEANED_BRANDNAME IN ('PLATYPUS','SPEEDO') AND PRODUCT_NAME LIKE '%16' THEN '16Y'
						  WHEN CLEANED_BRANDNAME LIKE 'SPEEDO' AND PRODUCT_NAME LIKE '%32' THEN 'SPEEDO 32'
						  WHEN CLEANED_BRANDNAME LIKE 'SPEEDO' AND PRODUCT_NAME LIKE '%34' THEN 'SPEEDO 34'
						  WHEN PRODUCT_NAME LIKE '%14Y' AND PRODUCT_NAME NOT LIKE '%6 14Y' AND PRODUCT_NAME NOT LIKE '%6-14Y' THEN '14Y'
						  WHEN CLEANED_BRANDNAME LIKE 'ZOGGS' AND PRODUCT_NAME LIKE '%30' THEN 'ZOGGS 30'
						  WHEN PRODUCT_NAME LIKE '%16Y' THEN '16Y'
						  WHEN PRODUCT_NAME LIKE '%160' THEN '160 CM'
						  WHEN PRODUCT_NAME LIKE '%14' THEN '14Y'
						  WHEN PRODUCT_NAME LIKE '%16' THEN '16Y'
						  WHEN PRODUCT_NAME LIKE '%14A' THEN '14Y'
						  WHEN PRODUCT_NAME LIKE '%15A' THEN '15Y'
						  WHEN PRODUCT_NAME LIKE '%16A' THEN '16Y'
						  WHEN PRODUCT_NAME LIKE '%160Y' THEN '16Y'
						  WHEN CLEANED_BRANDNAME = 'DOUBLEGOOSE' THEN 'DOUBLEGOOSE'
						  WHEN PRODUCT_NAME LIKE '%13-14Y%' THEN '13-14 Y'
						  WHEN CLEANED_BRANDNAME LIKE 'EMPORIO ARMANI' AND CLASS_NAME LIKE 'TEEN%' THEN 'EMPORIO ARMANI TEEN'
						  WHEN CLEANED_BRANDNAME LIKE 'GINGERSNAPS' AND PRODUCT_NAME LIKE '%14WF' THEN '14 Y'
						  WHEN CLEANED_BRANDNAME LIKE 'GINGERSNAPS' AND PRODUCT_NAME LIKE '%14CC' THEN '14 Y'
						  WHEN PRODUCT_NAME LIKE '% 14A %' THEN '14 Y'
						  ELSE TEEN_KEYWORD END)
WHERE MOM_AND_KID_PRODUCT_FLAG = 1
AND COALESCE(PREG_FLAG,'N/A') = 'N/A'
AND COALESCE(INFANT_FLAG,'N/A') = 'N/A'
AND COALESCE(TODDLER_FLAG,'N/A') = 'N/A'
AND COALESCE(PRESCHOOL_FLAG,'N/A') = 'N/A'
AND COALESCE(JUNIOR_FLAG,'N/A') = 'N/A'
AND COALESCE(TEEN_FLAG,'N/A') = 'N/A'
AND REVISED_CATEGORY_LEVEL_1 = 'KIDS'
AND REVISED_CATEGORY_LEVEL_2 = 'KID FASHION'
AND REVISED_CATEGORY_LEVEL_3 = 'KID CLOTHING'
AND ((CLEANED_BRANDNAME IN ('PLATYPUS','SPEEDO') AND (PRODUCT_NAME LIKE '%14'
											OR PRODUCT_NAME LIKE '%15'
											OR PRODUCT_NAME LIKE '%16'))
	OR (CLEANED_BRANDNAME LIKE 'SPEEDO' AND (PRODUCT_NAME LIKE '%32'
									OR PRODUCT_NAME LIKE '%34'))
	OR (PRODUCT_NAME LIKE '%14Y' AND PRODUCT_NAME NOT LIKE '%6 14Y' AND PRODUCT_NAME NOT LIKE '%6-14Y')
	OR (CLEANED_BRANDNAME LIKE 'ZOGGS' AND PRODUCT_NAME LIKE '%30')
	OR PRODUCT_NAME LIKE '%16Y'
	OR PRODUCT_NAME LIKE '%160'
	OR PRODUCT_NAME LIKE '%14'
	OR PRODUCT_NAME LIKE '%16'
	OR PRODUCT_NAME LIKE '%14A'
	OR PRODUCT_NAME LIKE '%15A'
	OR PRODUCT_NAME LIKE '%16A'
	OR PRODUCT_NAME LIKE '%160Y'
	OR CLEANED_BRANDNAME = 'DOUBLEGOOSE'
	OR PRODUCT_NAME LIKE '%13-14Y%'
	OR (CLEANED_BRANDNAME LIKE 'EMPORIO ARMANI' AND CLASS_NAME LIKE 'TEEN%')
	OR (CLEANED_BRANDNAME LIKE 'GINGERSNAPS' AND (PRODUCT_NAME LIKE '%14WF'
										OR PRODUCT_NAME LIKE '%14CC'))
	OR PRODUCT_NAME LIKE '% 14A %'
	)
;

--ELSE PRESCHOOL/JUNIOR/TEEN
UPDATE vpm_data.pm_skutagging_kids_product_master
SET PRESCHOOL_FLAG = 'YES'
	,JUNIOR_FLAG = 'YES'
	,TEEN_FLAG = 'YES'
	,PRESCHOOL_KEYWORD = (CASE WHEN CLEANED_BRANDNAME IN ('PLATYPUS','SPEEDO','ELLE PETITE','ELLE PETITE PRO','ELLE PRO','STREAM LINE','STREAM LINE PRO','STEPHEN JOSEPH','STREAM LINE SWIMWORLD PRO') THEN 'KID SWIMWEAR'
								WHEN PRODUCT_NAME LIKE '%��蹵����¹��%' THEN '��蹵����¹��'
								WHEN PRODUCT_NAME LIKE '%STUDENT%' THEN 'STUDENT'
								WHEN CLASS_NAME = 'SWIMWEAR' THEN 'KID SWIMWEAR'
								WHEN CLEANED_BRANDNAME IN ('AIR WALK DREAM','AIRWALK','AIRWALK CONVERT','AMERICAN EAGLE MENS','AMERICAN EAGLE WOMENS','KENZO KIDS','ANGEL KIDS BOY UNDERWEAR'
												,'ANGEL KIDS GIRL UNDERWEAR','ANGEL KIDS GIRL PYJAMAS','BRASH ROYALTY','BUILDABEAR WORKSHOP','CMG PRO','DISNEY WISELY','DKNY','ENFANT'
												,'GINGERSNAPS','GOMU FAMILY','GUESS','GUESS PRO2','GUESS PRO4','KANGAROOS','KANOKWAN','LACOSTE PRO','LEE','LITTLE RENOWN','MARAYAT'
												,'MARKS & SPENCER','MARVEL','MARVEL CHARACTER','MAVELL','MICKEY','MUJI','NICKELODEON','OTOKO','PALLY','PAUL SMITH JUNIOR','PAYLESS'
												,'PRETTY OVO','PRETTY OVO PRO','RALPH LAUREN','RALPH LAUREN PRO','RUGGED OUTBACK','SANRIO','SANRIO BASIC','SANRIO GIRLS','SANRIO NEKO'
												,'SESAME WORKSHOP','SMALL PLANET','SMARTFIT','SNAP','STAR WARS','TOTORO CHATUPORN','TRUSSARDI','TYLYNN','UNI FRIEND THAILAND','UTKIDS'
												,'ZOE&ZAC','ZOGGS','JUSTICE LEAGUE','SMARTER','SOFIA') THEN '����ͼ����'
								ELSE PRESCHOOL_KEYWORD END)
	,JUNIOR_KEYWORD = (CASE WHEN CLEANED_BRANDNAME IN ('PLATYPUS','SPEEDO','ELLE PETITE','ELLE PETITE PRO','ELLE PRO','STREAM LINE','STREAM LINE PRO','STEPHEN JOSEPH','STREAM LINE SWIMWORLD PRO') THEN 'KID SWIMWEAR'
								WHEN PRODUCT_NAME LIKE '%��蹵����¹��%' THEN '��蹵����¹��'
								WHEN PRODUCT_NAME LIKE '%STUDENT%' THEN 'STUDENT'
								WHEN CLASS_NAME = 'SWIMWEAR' THEN 'KID SWIMWEAR'
								WHEN CLEANED_BRANDNAME IN ('AIR WALK DREAM','AIRWALK','AIRWALK CONVERT','AMERICAN EAGLE MENS','AMERICAN EAGLE WOMENS','KENZO KIDS','ANGEL KIDS BOY UNDERWEAR'
												,'ANGEL KIDS GIRL UNDERWEAR','ANGEL KIDS GIRL PYJAMAS','BRASH ROYALTY','BUILDABEAR WORKSHOP','CMG PRO','DISNEY WISELY','DKNY','ENFANT'
												,'GINGERSNAPS','GOMU FAMILY','GUESS','GUESS PRO2','GUESS PRO4','KANGAROOS','KANOKWAN','LACOSTE PRO','LEE','LITTLE RENOWN','MARAYAT'
												,'MARKS & SPENCER','MARVEL','MARVEL CHARACTER','MAVELL','MICKEY','MUJI','NICKELODEON','OTOKO','PALLY','PAUL SMITH JUNIOR','PAYLESS'
												,'PRETTY OVO','PRETTY OVO PRO','RALPH LAUREN','RALPH LAUREN PRO','RUGGED OUTBACK','SANRIO','SANRIO BASIC','SANRIO GIRLS','SANRIO NEKO'
												,'SESAME WORKSHOP','SMALL PLANET','SMARTFIT','SNAP','STAR WARS','TOTORO CHATUPORN','TRUSSARDI','TYLYNN','UNI FRIEND THAILAND','UTKIDS'
												,'ZOE&ZAC','ZOGGS','JUSTICE LEAGUE','SMARTER','SOFIA') THEN '����ͼ����'
								ELSE JUNIOR_KEYWORD END)
	,TEEN_KEYWORD = (CASE WHEN CLEANED_BRANDNAME IN ('PLATYPUS','SPEEDO','ELLE PETITE','ELLE PETITE PRO','ELLE PRO','STREAM LINE','STREAM LINE PRO','STEPHEN JOSEPH','STREAM LINE SWIMWORLD PRO') THEN 'KID SWIMWEAR'
								WHEN PRODUCT_NAME LIKE '%��蹵����¹��%' THEN '��蹵����¹��'
								WHEN PRODUCT_NAME LIKE '%STUDENT%' THEN 'STUDENT'
								WHEN CLASS_NAME = 'SWIMWEAR' THEN 'KID SWIMWEAR'
								WHEN CLEANED_BRANDNAME IN ('AIR WALK DREAM','AIRWALK','AIRWALK CONVERT','AMERICAN EAGLE MENS','AMERICAN EAGLE WOMENS','KENZO KIDS','ANGEL KIDS BOY UNDERWEAR'
												,'ANGEL KIDS GIRL UNDERWEAR','ANGEL KIDS GIRL PYJAMAS','BRASH ROYALTY','BUILDABEAR WORKSHOP','CMG PRO','DISNEY WISELY','DKNY','ENFANT'
												,'GINGERSNAPS','GOMU FAMILY','GUESS','GUESS PRO2','GUESS PRO4','KANGAROOS','KANOKWAN','LACOSTE PRO','LEE','LITTLE RENOWN','MARAYAT'
												,'MARKS & SPENCER','MARVEL','MARVEL CHARACTER','MAVELL','MICKEY','MUJI','NICKELODEON','OTOKO','PALLY','PAUL SMITH JUNIOR','PAYLESS'
												,'PRETTY OVO','PRETTY OVO PRO','RALPH LAUREN','RALPH LAUREN PRO','RUGGED OUTBACK','SANRIO','SANRIO BASIC','SANRIO GIRLS','SANRIO NEKO'
												,'SESAME WORKSHOP','SMALL PLANET','SMARTFIT','SNAP','STAR WARS','TOTORO CHATUPORN','TRUSSARDI','TYLYNN','UNI FRIEND THAILAND','UTKIDS'
												,'ZOE&ZAC','ZOGGS','JUSTICE LEAGUE','SMARTER','SOFIA') THEN '����ͼ����'
								ELSE TEEN_KEYWORD END)
WHERE MOM_AND_KID_PRODUCT_FLAG = 1
AND COALESCE(PREG_FLAG,'N/A') = 'N/A'
AND COALESCE(INFANT_FLAG,'N/A') = 'N/A'
AND COALESCE(TODDLER_FLAG,'N/A') = 'N/A'
AND COALESCE(PRESCHOOL_FLAG,'N/A') = 'N/A'
AND COALESCE(JUNIOR_FLAG,'N/A') = 'N/A'
AND COALESCE(TEEN_FLAG,'N/A') = 'N/A'
AND REVISED_CATEGORY_LEVEL_1 = 'KIDS'
AND REVISED_CATEGORY_LEVEL_2 = 'KID FASHION'
AND REVISED_CATEGORY_LEVEL_3 = 'KID CLOTHING'
AND (CLEANED_BRANDNAME IN ('PLATYPUS','SPEEDO','ELLE PETITE','ELLE PETITE PRO','ELLE PRO','STREAM LINE','STREAM LINE PRO','STEPHEN JOSEPH','STREAM LINE SWIMWORLD PRO')
	OR PRODUCT_NAME LIKE '%��蹵����¹��%'
	OR CLASS_NAME = 'SWIMWEAR'
	OR PRODUCT_NAME LIKE '%STUDENT%'
	OR CLEANED_BRANDNAME IN ('AIR WALK DREAM','AIRWALK','AIRWALK CONVERT','AMERICAN EAGLE MENS','AMERICAN EAGLE WOMENS','KENZO KIDS','ANGEL KIDS BOY UNDERWEAR'
					,'ANGEL KIDS GIRL UNDERWEAR','ANGEL KIDS GIRL PYJAMAS','BRASH ROYALTY','BUILDABEAR WORKSHOP','CMG PRO','DISNEY WISELY','DKNY','ENFANT'
					,'GINGERSNAPS','GOMU FAMILY','GUESS','GUESS PRO2','GUESS PRO4','KANGAROOS','KANOKWAN','LACOSTE PRO','LEE','LITTLE RENOWN','MARAYAT'
					,'MARKS & SPENCER','MARVEL','MARVEL CHARACTER','MAVELL','MICKEY','MUJI','NICKELODEON','OTOKO','PALLY','PAUL SMITH JUNIOR','PAYLESS'
					,'PRETTY OVO','PRETTY OVO PRO','RALPH LAUREN','RALPH LAUREN PRO','RUGGED OUTBACK','SANRIO','SANRIO BASIC','SANRIO GIRLS','SANRIO NEKO'
					,'SESAME WORKSHOP','SMALL PLANET','SMARTFIT','SNAP','STAR WARS','TOTORO CHATUPORN','TRUSSARDI','TYLYNN','UNI FRIEND THAILAND','UTKIDS'
					,'ZOE&ZAC','ZOGGS','JUSTICE LEAGUE','SMARTER','SOFIA')
	)
;


/* KID FURNITURE */
-----OTHER KID FURNITURE
--NEW INFANT
UPDATE vpm_data.pm_skutagging_kids_product_master
SET INFANT_FLAG = 'YES'
	,"STAGE_M+9_TO_M+12" = (CASE WHEN PRODUCT_NAME LIKE '% 1' THEN 'YES' ELSE "STAGE_M+9_TO_M+12" END)
	,TODDLER_FLAG = (CASE WHEN PRODUCT_NAME LIKE '% 2' THEN 'YES' ELSE "STAGE_M+9_TO_M+12" END)
	,INFANT_10_12M_KEYWORD = (CASE WHEN PRODUCT_NAME LIKE '% 1' THEN '1Y' ELSE INFANT_10_12M_KEYWORD END)
	,TODDLER_KEYWORD = (CASE WHEN PRODUCT_NAME LIKE '% 2' THEN '2Y' ELSE TODDLER_KEYWORD END)
WHERE MOM_AND_KID_PRODUCT_FLAG = 1
AND COALESCE(PREG_FLAG,'N/A') = 'N/A'
AND COALESCE(INFANT_FLAG,'N/A') = 'N/A'
AND COALESCE(TODDLER_FLAG,'N/A') = 'N/A'
AND COALESCE(PRESCHOOL_FLAG,'N/A') = 'N/A'
AND COALESCE(JUNIOR_FLAG,'N/A') = 'N/A'
AND COALESCE(TEEN_FLAG,'N/A') = 'N/A'
AND REVISED_CATEGORY_LEVEL_1 = 'KIDS'
AND REVISED_CATEGORY_LEVEL_2 = 'KID FURNITURE'
AND REVISED_CATEGORY_LEVEL_3 = 'OTHER KID FURNITURE'
AND (PRODUCT_NAME LIKE '% 1'
	OR PRODUCT_NAME LIKE '% 2')
;

--NEW TODDLER
UPDATE vpm_data.pm_skutagging_kids_product_master
SET TODDLER_FLAG = 'YES'
	,TODDLER_KEYWORD = (CASE WHEN PRODUCT_NAME LIKE '% 3' THEN '3Y'
							 WHEN PRODUCT_NAME LIKE '% 4' THEN '4Y'
							 WHEN PRODUCT_NAME LIKE '%04' THEN '4Y'
							 ELSE TODDLER_KEYWORD END)
WHERE MOM_AND_KID_PRODUCT_FLAG = 1
AND COALESCE(PREG_FLAG,'N/A') = 'N/A'
AND COALESCE(INFANT_FLAG,'N/A') = 'N/A'
AND COALESCE(TODDLER_FLAG,'N/A') = 'N/A'
AND COALESCE(PRESCHOOL_FLAG,'N/A') = 'N/A'
AND COALESCE(JUNIOR_FLAG,'N/A') = 'N/A'
AND COALESCE(TEEN_FLAG,'N/A') = 'N/A'
AND REVISED_CATEGORY_LEVEL_1 = 'KIDS'
AND REVISED_CATEGORY_LEVEL_2 = 'KID FURNITURE'
AND REVISED_CATEGORY_LEVEL_3 = 'OTHER KID FURNITURE'
AND (PRODUCT_NAME LIKE '% 3'
	OR PRODUCT_NAME LIKE '% 4'
	OR PRODUCT_NAME LIKE '%04')
;

--PRESCHOOL
UPDATE vpm_data.pm_skutagging_kids_product_master
SET PRESCHOOL_FLAG = 'YES'
	,PRESCHOOL_KEYWORD = (CASE WHEN PRODUCT_NAME LIKE '% 5' THEN '5Y'
								WHEN PRODUCT_NAME LIKE '%6' AND PRODUCT_NAME NOT LIKE '%16' THEN '6Y'
								ELSE PRESCHOOL_KEYWORD END)
WHERE MOM_AND_KID_PRODUCT_FLAG = 1
AND COALESCE(PREG_FLAG,'N/A') = 'N/A'
AND COALESCE(INFANT_FLAG,'N/A') = 'N/A'
AND COALESCE(TODDLER_FLAG,'N/A') = 'N/A'
AND COALESCE(PRESCHOOL_FLAG,'N/A') = 'N/A'
AND COALESCE(JUNIOR_FLAG,'N/A') = 'N/A'
AND COALESCE(TEEN_FLAG,'N/A') = 'N/A'
AND REVISED_CATEGORY_LEVEL_1 = 'KIDS'
AND REVISED_CATEGORY_LEVEL_2 = 'KID FURNITURE'
AND REVISED_CATEGORY_LEVEL_3 = 'OTHER KID FURNITURE'
AND (PRODUCT_NAME LIKE '% 5'
	OR (PRODUCT_NAME LIKE '%6' AND PRODUCT_NAME NOT LIKE '%16'))
;

--JUNIOR
UPDATE vpm_data.pm_skutagging_kids_product_master
SET JUNIOR_FLAG = 'YES'
	,JUNIOR_KEYWORD = (CASE WHEN PRODUCT_NAME LIKE '%8' THEN '8Y'
							WHEN PRODUCT_NAME LIKE '%9' THEN '9Y'
							WHEN PRODUCT_NAME LIKE '%10' THEN '10Y'
							WHEN PRODUCT_NAME LIKE '%11' THEN '11Y'
							WHEN PRODUCT_NAME LIKE '%12' THEN  '12Y'
							WHEN PRODUCT_NAME LIKE '%13' THEN '13Y'
							WHEN CLEANED_BRANDNAME LIKE 'FUNNY KIDS' AND PRODUCT_NAME LIKE '% S' THEN 'FUNNY KIDS S'
							WHEN CLEANED_BRANDNAME LIKE 'FUNNY KIDS' AND PRODUCT_NAME LIKE '% M' THEN 'FUNNY KIDS M'
							WHEN CLEANED_BRANDNAME IN ('FOF GIRL','FOF BOY','PRETTY OVO') AND PRODUCT_NAME NOT LIKE '%[0-9]%' THEN CONCAT('����ͼ���� ',CLEANED_BRANDNAME)
							ELSE JUNIOR_KEYWORD END)
WHERE MOM_AND_KID_PRODUCT_FLAG = 1
AND COALESCE(PREG_FLAG,'N/A') = 'N/A'
AND COALESCE(INFANT_FLAG,'N/A') = 'N/A'
AND COALESCE(TODDLER_FLAG,'N/A') = 'N/A'
AND COALESCE(PRESCHOOL_FLAG,'N/A') = 'N/A'
AND COALESCE(JUNIOR_FLAG,'N/A') = 'N/A'
AND COALESCE(TEEN_FLAG,'N/A') = 'N/A'
AND REVISED_CATEGORY_LEVEL_1 = 'KIDS'
AND REVISED_CATEGORY_LEVEL_2 = 'KID FURNITURE'
AND REVISED_CATEGORY_LEVEL_3 = 'OTHER KID FURNITURE'
AND (PRODUCT_NAME LIKE '%8'
	OR PRODUCT_NAME LIKE '%9'
	OR PRODUCT_NAME LIKE '%10'
	OR PRODUCT_NAME LIKE '%11'
	OR PRODUCT_NAME LIKE '%12'
	OR PRODUCT_NAME LIKE '%13'
	OR (CLEANED_BRANDNAME LIKE 'FUNNY KIDS' AND (PRODUCT_NAME LIKE '% S'
										OR PRODUCT_NAME LIKE '% M'))
	OR (CLEANED_BRANDNAME IN ('FOF GIRL','FOF BOY','PRETTY OVO') AND PRODUCT_NAME NOT LIKE '%[0-9]%'))
;

--TEEN
UPDATE vpm_data.pm_skutagging_kids_product_master
SET TEEN_FLAG = 'YES'
	,TEEN_KEYWORD = (CASE WHEN PRODUCT_NAME LIKE '%14' THEN '14Y'
						  WHEN PRODUCT_NAME LIKE '%16' THEN '16Y'
						  ELSE TEEN_KEYWORD END)
WHERE MOM_AND_KID_PRODUCT_FLAG = 1
AND COALESCE(PREG_FLAG,'N/A') = 'N/A'
AND COALESCE(INFANT_FLAG,'N/A') = 'N/A'
AND COALESCE(TODDLER_FLAG,'N/A') = 'N/A'
AND COALESCE(PRESCHOOL_FLAG,'N/A') = 'N/A'
AND COALESCE(JUNIOR_FLAG,'N/A') = 'N/A'
AND COALESCE(TEEN_FLAG,'N/A') = 'N/A'
AND REVISED_CATEGORY_LEVEL_1 = 'KIDS'
AND REVISED_CATEGORY_LEVEL_2 = 'KID FURNITURE'
AND REVISED_CATEGORY_LEVEL_3 = 'OTHER KID FURNITURE'
AND (PRODUCT_NAME LIKE '%14'
	OR PRODUCT_NAME LIKE '%16')
;

--PRESCHOOL/JUNIOR/TEEN
UPDATE vpm_data.pm_skutagging_kids_product_master
SET PRESCHOOL_FLAG = 'YES'
	,JUNIOR_FLAG = 'YES'
	,TEEN_FLAG = 'YES'
	,PRESCHOOL_KEYWORD = (CASE WHEN CLEANED_BRANDNAME LIKE 'GUESS%' THEN '����ͼ���� GUESS'
								WHEN CLEANED_BRANDNAME LIKE 'BE CHARACTERS' THEN '����ͼ���� BE CHARACTERS'
								ELSE PRESCHOOL_KEYWORD END)
	,JUNIOR_KEYWORD = (CASE WHEN CLEANED_BRANDNAME LIKE 'GUESS%' THEN '����ͼ���� GUESS'
								WHEN CLEANED_BRANDNAME LIKE 'BE CHARACTERS' THEN '����ͼ���� BE CHARACTERS'
								ELSE JUNIOR_KEYWORD END)
	,TEEN_KEYWORD = (CASE WHEN CLEANED_BRANDNAME LIKE 'GUESS%' THEN '����ͼ���� GUESS'
								WHEN CLEANED_BRANDNAME LIKE 'BE CHARACTERS' THEN '����ͼ���� BE CHARACTERS'
								ELSE TEEN_KEYWORD END)
WHERE MOM_AND_KID_PRODUCT_FLAG = 1
AND COALESCE(PREG_FLAG,'N/A') = 'N/A'
AND COALESCE(INFANT_FLAG,'N/A') = 'N/A'
AND COALESCE(TODDLER_FLAG,'N/A') = 'N/A'
AND COALESCE(PRESCHOOL_FLAG,'N/A') = 'N/A'
AND COALESCE(JUNIOR_FLAG,'N/A') = 'N/A'
AND COALESCE(TEEN_FLAG,'N/A') = 'N/A'
AND REVISED_CATEGORY_LEVEL_1 = 'KIDS'
AND REVISED_CATEGORY_LEVEL_2 = 'KID FURNITURE'
AND REVISED_CATEGORY_LEVEL_3 = 'OTHER KID FURNITURE'
AND CLEANED_BRANDNAME IN ('GUESS','GUESS PRO2','GUESS PRO4','BE CHARACTERS')
;

--------------------------------------------------------------------------------------------------------------------------
--NOT KID PRODUCT
UPDATE vpm_data.pm_skutagging_kids_product_master
SET MOM_AND_KID_PRODUCT_FLAG = 0
WHERE MOM_AND_KID_PRODUCT_FLAG = 1
AND (PRODUCT_NAME LIKE '%��˹չá%'
	OR PRODUCT_NAME LIKE '%�����¡��Դ�ѡ%'
	OR PRODUCT_NAME LIKE '%��駪���%'
	OR PRODUCT_NAME LIKE '%���ͧ�������%'
	OR PRODUCT_NAME LIKE '%���ʹ��դ�������%'
	OR SUBCLASS_NAME LIKE 'BABY NAMES'
	OR PRODUCT_NAME LIKE '%ʧ�������%����Щ�Ѻ��������%'
	OR PRODUCT_NAME LIKE '���ͧ�Ѻ����˭�%'
	OR CLEANED_BRANDNAME LIKE 'CERTAINTY'
	OR PRODUCT_NAME LIKE '����硫��ʹ���%'
	OR PRODUCT_NAME LIKE 'ARCTIC%NECK%COOLER%'
	OR PRODUCT_NAME LIKE '%MASK ADULT%'
	)
;

--PREG 0-6M
UPDATE vpm_data.pm_skutagging_kids_product_master
SET PREG_FLAG = 'YES'
	,"STAGE_M-9_TO_M-6" = 'YES'
	,"STAGE_M-6_TO_M-3" = 'YES'
	,"STAGE_M-3_TO_M-0" = 'N/A'
	,PREG_0_3M_KEYWORD = 'PREGNANCY GUIDE'
	,PREG_4_6M_KEYWORD = 'PREGNANCY GUIDE'
WHERE MOM_AND_KID_PRODUCT_FLAG = 1
AND COALESCE(PREG_FLAG,'N/A') = 'N/A'
AND COALESCE(INFANT_FLAG,'N/A') = 'N/A'
AND COALESCE(TODDLER_FLAG,'N/A') = 'N/A'
AND COALESCE(PRESCHOOL_FLAG,'N/A') = 'N/A'
AND COALESCE(JUNIOR_FLAG,'N/A') = 'N/A'
AND COALESCE(TEEN_FLAG,'N/A') = 'N/A'
AND SUBCLASS_NAME = 'PREGNANCY'
AND REVISED_CATEGORY_LEVEL_2 = 'BOOK'
;


--PREG UNKNOWN
UPDATE vpm_data.pm_skutagging_kids_product_master
SET PREG_FLAG = 'YES'
	,UNKNOWN_PREG_SUBSTAGE = 'YES'
	,PREG_UNKNOWN_KEYWORD = (CASE WHEN PRODUCT_NAME LIKE '��͹�ͧ��ͧ%' THEN '��͹�ͧ��ͧ'
									WHEN PRODUCT_NAME LIKE '��͹��Фͧ������ҹ͹' THEN '��͹��Фͧ'
									ELSE PREG_UNKNOWN_KEYWORD END)
WHERE MOM_AND_KID_PRODUCT_FLAG = 1
AND COALESCE(PREG_FLAG,'N/A') = 'N/A'
AND COALESCE(INFANT_FLAG,'N/A') = 'N/A'
AND COALESCE(TODDLER_FLAG,'N/A') = 'N/A'
AND COALESCE(PRESCHOOL_FLAG,'N/A') = 'N/A'
AND COALESCE(JUNIOR_FLAG,'N/A') = 'N/A'
AND COALESCE(TEEN_FLAG,'N/A') = 'N/A'
AND (PRODUCT_NAME LIKE '��͹�ͧ��ͧ%'
	OR PRODUCT_NAME LIKE '��͹��Фͧ������ҹ͹')
;

--3-6M
UPDATE vpm_data.pm_skutagging_kids_product_master
SET INFANT_FLAG = 'YES'
	,"STAGE_M+3_TO_M+6" = 'YES'
	,INFANT_4_6M_KEYWORD = 'ö�Ѵ�Թ'
WHERE MOM_AND_KID_PRODUCT_FLAG = 1
AND COALESCE(PREG_FLAG,'N/A') = 'N/A'
AND COALESCE(INFANT_FLAG,'N/A') = 'N/A'
AND COALESCE(TODDLER_FLAG,'N/A') = 'N/A'
AND COALESCE(PRESCHOOL_FLAG,'N/A') = 'N/A'
AND COALESCE(JUNIOR_FLAG,'N/A') = 'N/A'
AND COALESCE(TEEN_FLAG,'N/A') = 'N/A'
AND PRODUCT_NAME LIKE '%ö�Ѵ�Թ%'
;

--INFANT 3M+
UPDATE vpm_data.pm_skutagging_kids_product_master
SET INFANT_FLAG = 'YES'
	,"STAGE_M+3_TO_M+6" = 'YES'
	,"STAGE_M+6_TO_M+9" = 'YES'
	,"STAGE_M+9_TO_M+12" = 'YES'
	,INFANT_4_6M_KEYWORD = (CASE WHEN PRODUCT_NAME LIKE '%2M+%' THEN '2M+'
								WHEN PRODUCT_NAME LIKE '%3M+%' THEN '3M+'
								WHEN PRODUCT_NAME LIKE '%4M+%' THEN '4M+'
								WHEN PRODUCT_NAME LIKE '%TEETHER%' THEN 'TEETHER'
								WHEN PRODUCT_NAME LIKE '%�Ѵ%�ѹ���%' THEN 'TEETHER'
								ELSE INFANT_4_6M_KEYWORD END)
	,INFANT_7_9M_KEYWORD = (CASE WHEN PRODUCT_NAME LIKE '%2M+%' THEN '2M+'
								WHEN PRODUCT_NAME LIKE '%3M+%' THEN '3M+'
								WHEN PRODUCT_NAME LIKE '%4M+%' THEN '4M+'
								WHEN PRODUCT_NAME LIKE '%TEETHER%' THEN 'TEETHER'
								WHEN PRODUCT_NAME LIKE '%�Ѵ%�ѹ���%' THEN 'TEETHER'
								ELSE INFANT_7_9M_KEYWORD END)
	,INFANT_10_12M_KEYWORD = (CASE WHEN PRODUCT_NAME LIKE '%2M+%' THEN '2M+'
								WHEN PRODUCT_NAME LIKE '%3M+%' THEN '3M+'
								WHEN PRODUCT_NAME LIKE '%4M+%' THEN '4M+'
								WHEN PRODUCT_NAME LIKE '%TEETHER%' THEN 'TEETHER'
								WHEN PRODUCT_NAME LIKE '%�Ѵ%�ѹ���%' THEN 'TEETHER'
								ELSE INFANT_10_12M_KEYWORD END)
WHERE MOM_AND_KID_PRODUCT_FLAG = 1
AND COALESCE(PREG_FLAG,'N/A') = 'N/A'
AND COALESCE(INFANT_FLAG,'N/A') = 'N/A'
AND COALESCE(TODDLER_FLAG,'N/A') = 'N/A'
AND COALESCE(PRESCHOOL_FLAG,'N/A') = 'N/A'
AND COALESCE(JUNIOR_FLAG,'N/A') = 'N/A'
AND COALESCE(TEEN_FLAG,'N/A') = 'N/A'
AND (PRODUCT_NAME LIKE '%3M+%'
	OR PRODUCT_NAME LIKE '%TEETHER%'
	OR PRODUCT_NAME LIKE '%4M+%'
	OR PRODUCT_NAME LIKE '%2M+%'
	OR PRODUCT_NAME LIKE '%�Ѵ%�ѹ���%')
;

--INFANT 6M+
UPDATE vpm_data.pm_skutagging_kids_product_master
SET INFANT_FLAG = 'YES'
	,"STAGE_M+6_TO_M+9" = 'YES'
	,"STAGE_M+9_TO_M+12" = 'YES'
	,TODDLER_FLAG = 'YES'
	,INFANT_7_9M_KEYWORD = '�ش�֡/��͹�����'
	,INFANT_10_12M_KEYWORD = '�ش�֡/��͹�����'
	,TODDLER_KEYWORD = '�ش�֡/��͹�����'
WHERE MOM_AND_KID_PRODUCT_FLAG = 1
AND COALESCE(PREG_FLAG,'N/A') = 'N/A'
AND COALESCE(INFANT_FLAG,'N/A') = 'N/A'
AND COALESCE(TODDLER_FLAG,'N/A') = 'N/A'
AND COALESCE(PRESCHOOL_FLAG,'N/A') = 'N/A'
AND COALESCE(JUNIOR_FLAG,'N/A') = 'N/A'
AND COALESCE(TEEN_FLAG,'N/A') = 'N/A'
AND (PRODUCT_NAME LIKE '%FEEDING%SPOON%'
	OR CLEANED_BRANDNAME LIKE 'GRACE KIDS'
	OR PRODUCT_NAME LIKE '%6M+%')
;

--INFANT 12-24M
UPDATE vpm_data.pm_skutagging_kids_product_master
SET INFANT_FLAG = 'N/A'
	,TODDLER_FLAG = 'YES'
	,TODDLER_KEYWORD = (CASE WHEN PRODUCT_NAME LIKE '%12M+%' THEN '12M+'
									WHEN PRODUCT_NAME LIKE 'CUTIES%FORK%SET' THEN 'BABY FORK SET'
									WHEN PRODUCT_NAME LIKE '%�ѡ�á%' THEN '�ѡ�á'
									WHEN PRODUCT_NAME LIKE '%����¡�ٹԤ����Ŵ�' THEN '����¡�ٹԤ���'
									WHEN PRODUCT_NAME LIKE '%POTTY%TRAINER%' THEN 'POTTY TRAINER'
									ELSE TODDLER_KEYWORD END)
WHERE MOM_AND_KID_PRODUCT_FLAG = 1
AND COALESCE(PREG_FLAG,'N/A') = 'N/A'
AND COALESCE(INFANT_FLAG,'N/A') = 'N/A'
AND COALESCE(TODDLER_FLAG,'N/A') = 'N/A'
AND COALESCE(PRESCHOOL_FLAG,'N/A') = 'N/A'
AND COALESCE(JUNIOR_FLAG,'N/A') = 'N/A'
AND COALESCE(TEEN_FLAG,'N/A') = 'N/A'
AND (PRODUCT_NAME LIKE '%12M+%'
	OR PRODUCT_NAME LIKE 'CUTIES%FORK%SET'
	OR PRODUCT_NAME LIKE '%�ѡ�á%'
	OR PRODUCT_NAME LIKE '%����¡�ٹԤ����Ŵ�'
	OR PRODUCT_NAME LIKE '%POTTY%TRAINER%')
;

--UNKNOWN INFANT
UPDATE vpm_data.pm_skutagging_kids_product_master
SET INFANT_FLAG = 'YES'
	,UNKNOWN_INFANT_SUBSTAGE = 'YES'
	,INFANT_UNKNOWN_KEYWORD = (CASE WHEN PRODUCT_NAME LIKE '%0M+%' THEN '0M+'
									WHEN PRODUCT_NAME LIKE '%BOTTLE%' THEN 'BOTTLE'
									WHEN PRODUCT_NAME LIKE '%CHANGING%BAG%' THEN 'CHANGING BAG'
									WHEN PRODUCT_NAME LIKE '%THERMO%' THEN 'THERMOMETER'
									WHEN PRODUCT_NAME LIKE '%BABY%PORTION%' THEN 'BABY PORTION'
									WHEN PRODUCT_NAME LIKE '%BIB%' THEN 'BIB'
									WHEN PRODUCT_NAME LIKE '%��ҾҴ���%' THEN '��ҾҴ���'
									WHEN  PRODUCT_NAME LIKE '%��ҾҴ����%' THEN '��ҾҴ���'
									WHEN  PRODUCT_NAME LIKE '%����ҧ%' THEN '����ҧ'
									WHEN  PRODUCT_NAME LIKE '%��ҡѹ���͹%' THEN '��ҡѹ���͹'
									WHEN  PRODUCT_NAME LIKE '%�����͵��%' THEN '�����͵��'
									WHEN PRODUCT_NAME LIKE '%BOOTEE%' THEN 'BOOTEE'
									WHEN PRODUCT_NAME LIKE '%CLEANING%BRUSH%' THEN 'CLEANING BRUSH'
									WHEN PRODUCT_NAME LIKE '%��ӹ�%' THEN '��ӹ�'
									WHEN PRODUCT_NAME LIKE '%������%' THEN '������'
									WHEN PRODUCT_NAME LIKE '%PUMP%' THEN '������'
									WHEN PRODUCT_NAME LIKE '%PVC TUBING%' THEN '������'
									WHEN PRODUCT_NAME LIKE '%����觹�%' THEN '����觹�'
									WHEN CLEANED_BRANDNAME LIKE 'MEDELA%' THEN 'MEDELA ACCESSORIES'
									WHEN PRODUCT_NAME LIKE '%MILK%POWDER%' THEN 'MILK POWDER DISPENDER'
									WHEN PRODUCT_NAME LIKE '%������%' THEN 'PLAY GYM'
									WHEN PRODUCT_NAME LIKE '%��á%' THEN '��á'
									WHEN PRODUCT_NAME LIKE '%�á�Դ%' THEN '�á�Դ'
									WHEN PRODUCT_NAME LIKE '%�������纤������%' THEN '�������纤������'
									WHEN PRODUCT_NAME LIKE '%������ҧ�Ѵ%' THEN '������ҧ�Ѵ'
									WHEN PRODUCT_NAME LIKE '%�ͧ�Һ���%' THEN '����ͧ�Һ���'
									WHEN PRODUCT_NAME LIKE '%HIP%FIT%����Ѵ��ЪѺ%' THEN '����Ѵ��ЪѺ'
									WHEN PRODUCT_NAME LIKE '%NAPPIE%CREAM%' THEN 'NAPPIE CREAM'
									WHEN PRODUCT_NAME LIKE '������ԧ���%' THEN '������ԧ���'
									WHEN PRODUCT_NAME LIKE '��俿��%' THEN '��俿��'
									WHEN PRODUCT_NAME LIKE '��ԧ��ù�� �����紽�' THEN '��俿��'
									WHEN PRODUCT_NAME LIKE '��Ե�ѳ���ا��%' THEN '��ا��'
									WHEN PRODUCT_NAME LIKE '����Ѵ%' THEN '����Ѵ��ЪѺ�س���'
									WHEN CLASS_NAME LIKE 'NEW BORN%' THEN 'NEWBORN'
									ELSE INFANT_UNKNOWN_KEYWORD END)
WHERE MOM_AND_KID_PRODUCT_FLAG = 1
AND COALESCE(PREG_FLAG,'N/A') = 'N/A'
AND COALESCE(INFANT_FLAG,'N/A') = 'N/A'
AND COALESCE(TODDLER_FLAG,'N/A') = 'N/A'
AND COALESCE(PRESCHOOL_FLAG,'N/A') = 'N/A'
AND COALESCE(JUNIOR_FLAG,'N/A') = 'N/A'
AND COALESCE(TEEN_FLAG,'N/A') = 'N/A'
AND (PRODUCT_NAME LIKE '%BOTTLE%'
	OR PRODUCT_NAME LIKE '%CHANGING%BAG%'
	OR PRODUCT_NAME LIKE '%THERMO%'
	OR PRODUCT_NAME LIKE '%BABY%PORTION%'
	OR PRODUCT_NAME LIKE '%BIB%'
	OR PRODUCT_NAME LIKE '%��ҾҴ���%'
	OR PRODUCT_NAME LIKE '%��ҾҴ����%'
	OR PRODUCT_NAME LIKE '%����ҧ%'
	OR PRODUCT_NAME LIKE '%��ҡѹ���͹%'
	OR PRODUCT_NAME LIKE '%�����͵��%'
	OR PRODUCT_NAME LIKE '%������%'
	OR PRODUCT_NAME LIKE '%��ӹ�%'
	OR PRODUCT_NAME LIKE '%����觹�%'
	OR PRODUCT_NAME LIKE '%PUMP%'
	OR PRODUCT_NAME LIKE '%PVC TUBING%'
	OR CLEANED_BRANDNAME LIKE 'MEDELA%'
	OR PRODUCT_NAME LIKE '%MILK%POWDER%DISPENSER%'
	OR PRODUCT_NAME LIKE '%0M+%'
	OR PRODUCT_NAME LIKE '%BOOTEE%'
	OR PRODUCT_NAME LIKE '%������%'
	OR PRODUCT_NAME LIKE '%��á%'
	OR PRODUCT_NAME LIKE '%�á�Դ%'
	OR PRODUCT_NAME LIKE '%�������纤������%'
	OR PRODUCT_NAME LIKE '%������ҧ�Ѵ%'
	OR PRODUCT_NAME LIKE '%�������ͧ�Һ���%'
	OR PRODUCT_NAME LIKE '%HIP%FIT%����Ѵ��ЪѺ%'
	OR PRODUCT_NAME LIKE '%NAPPIE%CREAM%'
	OR PRODUCT_NAME LIKE '������ԧ���%'
	OR PRODUCT_NAME LIKE '��俿��%'
	OR PRODUCT_NAME LIKE '��Ե�ѳ���ا��%'
	OR PRODUCT_NAME LIKE '����Ѵ%'
	OR PRODUCT_NAME LIKE '��ԧ��ù�� �����紽�'
	OR CLASS_NAME LIKE 'NEW BORN%'
	)
;

--TODDLER
UPDATE vpm_data.pm_skutagging_kids_product_master
SET TODDLER_FLAG = 'YES'
	,TODDLER_KEYWORD = (CASE WHEN PRODUCT_NAME LIKE 'DIB 2ND%' THEN '����Ѵ������� 2'
							WHEN PRODUCT_NAME LIKE 'DRINK%2ND%GENERATION%' THEN '����Ѵ������� 2'
							WHEN PRODUCT_NAME LIKE 'DRINK%IN%BOX%' THEN '����Ѵ������� 2'
							WHEN PRODUCT_NAME LIKE 'DRINK%MICKEY%' THEN '����Ѵ������� 2'
							WHEN PRODUCT_NAME LIKE 'GROWING CUP' THEN '����Ѵ������� 2'
							WHEN PRODUCT_NAME LIKE 'DIAPER%SIZE%' THEN 'XL DIAPER'
							WHEN PRODUCT_NAME LIKE '%36M%' THEN '36M'
							WHEN PRODUCT_NAME LIKE '%36 M' THEN '36M'
							WHEN PRODUCT_NAME LIKE 'AKAPOP SHOES%24' THEN 'SHOES EU SIZE 24'
							WHEN PRODUCT_NAME LIKE '%2-3Y%' THEN '2-3 Y'
							WHEN PRODUCT_NAME LIKE '%3 4Y%' THEN '3-4 Y'
							ELSE TODDLER_KEYWORD END)
WHERE MOM_AND_KID_PRODUCT_FLAG = 1
AND COALESCE(PREG_FLAG,'N/A') = 'N/A'
AND COALESCE(INFANT_FLAG,'N/A') = 'N/A'
AND COALESCE(TODDLER_FLAG,'N/A') = 'N/A'
AND COALESCE(PRESCHOOL_FLAG,'N/A') = 'N/A'
AND COALESCE(JUNIOR_FLAG,'N/A') = 'N/A'
AND COALESCE(TEEN_FLAG,'N/A') = 'N/A'
AND (PRODUCT_NAME LIKE 'DIB 2ND%'
	OR PRODUCT_NAME LIKE 'DRINK%2ND%GENERATION%'
	OR PRODUCT_NAME LIKE 'DRINK%IN%BOX%'
	OR PRODUCT_NAME LIKE 'DRINK%MICKEY%'
	OR PRODUCT_NAME LIKE 'GROWING CUP'
	OR PRODUCT_NAME LIKE 'DIAPER%SIZE%'
	OR PRODUCT_NAME LIKE '%36M%'
	OR PRODUCT_NAME LIKE '%36 M'
	OR PRODUCT_NAME LIKE 'AKAPOP SHOES%24'
	OR PRODUCT_NAME LIKE '%SHOE%24'
	OR PRODUCT_NAME LIKE '%2-3Y%'
	OR PRODUCT_NAME LIKE '%3 4Y'
	OR PRODUCT_NAME LIKE '%��¨٧�ѹ���ŧ%')
;

--INFANT/TODDLER
UPDATE vpm_data.pm_skutagging_kids_product_master
SET INFANT_FLAG = 'YES'
	,UNKNOWN_INFANT_SUBSTAGE = 'YES'
	,TODDLER_FLAG = 'YES'
	,INFANT_UNKNOWN_KEYWORD = (CASE WHEN REVISED_CATEGORY_LEVEL_2 LIKE 'KID ACCESSORIES' THEN 'BABY ACCESSORIES'
									WHEN PRODUCT_NAME LIKE '%BABY%' THEN 'BABY'
									WHEN PRODUCT_NAME LIKE '຺��' THEN 'BABY'
									WHEN CLASS_NAME LIKE '%BABY%' THEN 'BABY''S WEAR'
									WHEN SUBCLASS_NAME LIKE '%INFANT%' THEN 'INFANT/TODDLERS'
									WHEN SUBCLASS_NAME LIKE '%NEWBORN%' THEN 'NEWBORN'
									WHEN CLEANED_BRANDNAME LIKE '%BABIES%' THEN 'OVO BABIES'
									WHEN PRODUCT_NAME LIKE '%����͹%' THEN '����͹'
									WHEN PRODUCT_NAME LIKE '%�͡%��%' THEN '�͡��'
									WHEN PRODUCT_NAME LIKE '%���͡�����%' THEN '�͡��'
									WHEN PRODUCT_NAME LIKE '%��������%' THEN 'JUMPER'
									WHEN PRODUCT_NAME LIKE '%���������%' THEN '������'
									WHEN PRODUCT_NAME LIKE '%��������%' THEN '������'
									WHEN PRODUCT_NAME LIKE '%ECLEVE%TEETHING%PAD%' THEN '������'
									WHEN PRODUCT_NAME LIKE 'HIPSEAT%CARRIER%' THEN '������'
									WHEN PRODUCT_NAME LIKE 'JUMBO SEAT%' THEN '������'
									WHEN REVISED_CATEGORY_LEVEL_2 LIKE 'DIAPERS' THEN '���������'
									WHEN PRODUCT_NAME LIKE '��蹵ҡѹᴴ�� S%' THEN '��蹵ҡѹᴴ��'
									WHEN CLEANED_BRANDNAME LIKE 'PARKLON' THEN '���ͧ��ҹ'
									WHEN CLEANED_BRANDNAME LIKE 'ASTA' THEN '���ͧ��ҹ'
									WHEN PRODUCT_NAME LIKE '%�ػ�ó�����������%' THEN '�ػ�ó��������ä'
									WHEN PRODUCT_NAME LIKE '��������%�Ҵ%' THEN '������֡�ҹ'
									WHEN PRODUCT_NAME LIKE '%BOOSTER%' THEN 'BOOSTER'
									WHEN PRODUCT_NAME LIKE '��������������%' THEN '���������������Ѻ��'
									WHEN PRODUCT_NAME LIKE '������˹ѧ����Ѻ��%' THEN '���������������Ѻ��'
									WHEN PRODUCT_NAME LIKE '%���Ѵ�س�����%' THEN '���Ѵ�س�����'
									WHEN PRODUCT_NAME LIKE '%��駡ѹ�ا%' THEN '����ͧ�͹����͹'
									WHEN PRODUCT_NAME LIKE '����ͧ�͹SANRIO' THEN '����ͧ�͹����͹'
									WHEN  PRODUCT_NAME LIKE '�ش���͹�Ԥ�Ԥ%' THEN '����ͧ�͹����͹'
									WHEN CLEANED_BRANDNAME IN ('DS','BEBE CHERI','NUEBABE','PUREEN CORNER','SOFT','BABY LINK','MUMMOM','BEANIE NAP','ANGEL BABY','TOY BANK') THEN '����ͧ�͹����͹'
									ELSE INFANT_UNKNOWN_KEYWORD END)
	,TODDLER_KEYWORD = (CASE WHEN REVISED_CATEGORY_LEVEL_2 LIKE 'KID ACCESSORIES' THEN 'BABY ACCESSORIES'
									WHEN PRODUCT_NAME LIKE '%BABY%' THEN 'BABY'
									WHEN PRODUCT_NAME LIKE '຺��' THEN 'BABY'
									WHEN CLASS_NAME LIKE '%BABY%' THEN 'BABY''S WEAR'
									WHEN SUBCLASS_NAME LIKE '%INFANT%' THEN 'INFANT/TODDLERS'
									WHEN SUBCLASS_NAME LIKE '%NEWBORN%' THEN 'NEWBORN'
									WHEN CLEANED_BRANDNAME LIKE '%BABIES%' THEN 'OVO BABIES'
									WHEN PRODUCT_NAME LIKE '%����͹%' THEN '����͹'
									WHEN PRODUCT_NAME LIKE '%�͡%��%' THEN '�͡��'
									WHEN PRODUCT_NAME LIKE '%���͡�����%' THEN '�͡��'
									WHEN PRODUCT_NAME LIKE '%��������%' THEN 'JUMPER'
									WHEN PRODUCT_NAME LIKE '%���������%' THEN '������'
									WHEN PRODUCT_NAME LIKE '%��������%' THEN '������'
									WHEN PRODUCT_NAME LIKE '%ECLEVE%TEETHING%PAD%' THEN '������'
									WHEN PRODUCT_NAME LIKE '%HIP%SEAT%CARRIER%' THEN '������'
									WHEN PRODUCT_NAME LIKE 'JUMBO SEAT%' THEN '������'
									WHEN REVISED_CATEGORY_LEVEL_2 LIKE 'DIAPERS' THEN '���������'
									WHEN PRODUCT_NAME LIKE '��蹵ҡѹᴴ�� S%' THEN '��蹵ҡѹᴴ��'
									WHEN CLEANED_BRANDNAME LIKE 'PARKLON' THEN '���ͧ��ҹ'
									WHEN CLEANED_BRANDNAME LIKE 'ASTA' THEN '���ͧ��ҹ'
									WHEN PRODUCT_NAME LIKE '%�ػ�ó�����������%' THEN '�ػ�ó��������ä'
									WHEN PRODUCT_NAME LIKE '��������%�Ҵ%' THEN '������֡�ҹ'
									WHEN PRODUCT_NAME LIKE '%BOOSTER%' THEN 'BOOSTER'
									WHEN PRODUCT_NAME LIKE '��������������%' THEN '���������������Ѻ��'
									WHEN PRODUCT_NAME LIKE '������˹ѧ����Ѻ��%' THEN '���������������Ѻ��'
									WHEN PRODUCT_NAME LIKE '%���Ѵ�س�����%' THEN '���Ѵ�س�����'
									WHEN PRODUCT_NAME LIKE '%��駡ѹ�ا%' THEN '����ͧ�͹����͹'
									WHEN PRODUCT_NAME LIKE '����ͧ�͹SANRIO' THEN '����ͧ�͹����͹'
									WHEN  PRODUCT_NAME LIKE '�ش���͹�Ԥ�Ԥ%' THEN '����ͧ�͹����͹'
									WHEN CLEANED_BRANDNAME IN ('DS','BEBE CHERI','NUEBABE','PUREEN CORNER','SOFT','BABY LINK','MUMMOM','BEANIE NAP','ANGEL BABY','TOY BANK') THEN '����ͧ�͹����͹'
									ELSE TODDLER_KEYWORD END)
WHERE MOM_AND_KID_PRODUCT_FLAG = 1
AND COALESCE(PREG_FLAG,'N/A') = 'N/A'
AND COALESCE(INFANT_FLAG,'N/A') = 'N/A'
AND COALESCE(TODDLER_FLAG,'N/A') = 'N/A'
AND COALESCE(PRESCHOOL_FLAG,'N/A') = 'N/A'
AND COALESCE(JUNIOR_FLAG,'N/A') = 'N/A'
AND COALESCE(TEEN_FLAG,'N/A') = 'N/A'
AND (REVISED_CATEGORY_LEVEL_2 LIKE 'KID ACCESSORIES'
	OR PRODUCT_NAME LIKE '%BABY%'
	OR PRODUCT_NAME LIKE '຺��'
	OR CLASS_NAME LIKE '%BABY%'
	OR SUBCLASS_NAME LIKE '%INFANT%'
	OR SUBCLASS_NAME LIKE '%NEWBORN%'
	OR CLEANED_BRANDNAME LIKE '%BABIES%'
	OR PRODUCT_NAME LIKE '%����͹%'
	OR PRODUCT_NAME LIKE '%�͡%��%'
	OR PRODUCT_NAME LIKE '%���͡�����%'
	OR PRODUCT_NAME LIKE '%��������%'
	OR PRODUCT_NAME LIKE '%���������%'
	OR PRODUCT_NAME LIKE '%��������%'
	OR PRODUCT_NAME LIKE '%HIP%SEAT%CARRIER%'
	OR PRODUCT_NAME LIKE '%ECLEVE%TEETHING%PAD%'
	OR PRODUCT_NAME LIKE 'JUMBO SEAT%'
	OR REVISED_CATEGORY_LEVEL_2 LIKE 'DIAPERS'
	OR CLEANED_BRANDNAME LIKE 'PARKLON'
	OR CLEANED_BRANDNAME LIKE 'ASTA'
	OR PRODUCT_NAME LIKE '��������������%'
	OR PRODUCT_NAME LIKE '������˹ѧ����Ѻ��%'
	OR PRODUCT_NAME LIKE '��������%�Ҵ%'
	OR PRODUCT_NAME LIKE '%�ػ�ó�����������%'
	OR PRODUCT_NAME LIKE '%��駡ѹ�ا%'
	OR PRODUCT_NAME LIKE '����ͧ�͹SANRIO'
	OR PRODUCT_NAME LIKE '�ش���͹�Ԥ�Ԥ%'
	OR PRODUCT_NAME LIKE '%BOOSTER%'
	OR PRODUCT_NAME LIKE '%���Ѵ�س�����%'
	OR PRODUCT_NAME LIKE '��蹵ҡѹᴴ�� S%'
	OR CLEANED_BRANDNAME IN ('DS','BEBE CHERI','NUEBABE','PUREEN CORNER','SOFT','BABY LINK','MUMMOM','BEANIE NAP','ANGEL BABY','TOY BANK'))
;

--PRESCHOOL
UPDATE vpm_data.pm_skutagging_kids_product_master
SET PRESCHOOL_FLAG = 'YES'
	,PRESCHOOL_KEYWORD = (CASE WHEN PRODUCT_NAME LIKE '%5 6Y' THEN '5-6 Y'
								ELSE PRESCHOOL_KEYWORD END)
WHERE MOM_AND_KID_PRODUCT_FLAG = 1
AND COALESCE(PREG_FLAG,'N/A') = 'N/A'
AND COALESCE(INFANT_FLAG,'N/A') = 'N/A'
AND COALESCE(TODDLER_FLAG,'N/A') = 'N/A'
AND COALESCE(PRESCHOOL_FLAG,'N/A') = 'N/A'
AND COALESCE(JUNIOR_FLAG,'N/A') = 'N/A'
AND COALESCE(TEEN_FLAG,'N/A') = 'N/A'
AND (PRODUCT_NAME LIKE '%5 6Y')
;

--PRESCHOOL/JUNIOR
UPDATE vpm_data.pm_skutagging_kids_product_master
SET PRESCHOOL_FLAG = 'YES'
	,JUNIOR_FLAG = 'YES'
	,PRESCHOOL_KEYWORD = (CASE WHEN PRODUCT_NAME LIKE '%������������ҡ%' THEN '������������ҡ'
								WHEN PRODUCT_NAME LIKE 'ENFAGROW4%' THEN 'MILK STEP 4'
								WHEN PRODUCT_NAME LIKE 'HIQ 3PLUS%' THEN 'MILK STEP 4'
								WHEN PRODUCT_NAME LIKE 'S26 GOLD PROGRESS 4%' THEN 'MILK STEP 4'
								WHEN PRODUCT_NAME LIKE '%5 7Y' THEN '5-7 Y'
								WHEN CLEANED_BRANDNAME LIKE 'DISNEY KIDDO' THEN '����ͼ����'
								WHEN PRODUCT_NAME LIKE '�ش����Ҵ�ا�Ң���' THEN '�ͧ�����'
								WHEN PRODUCT_NAME LIKE 'ö��蹷���' THEN '�ͧ�����'
								WHEN PRODUCT_NAME LIKE '�������32����⿹���ͧ' THEN '�ͧ�����'
								WHEN PRODUCT_NAME LIKE '��Ŵ���������Ѻ��%' THEN '�ͧ�����'
								WHEN CLEANED_BRANDNAME LIKE 'AERA%' THEN '�ͧ����'
								WHEN PRODUCT_NAME LIKE 'CUTE BEAR CHAIR%' THEN '�ͧ����'
								WHEN PRODUCT_NAME LIKE '%KIDS%WATCH%��ͧ�ѹ�����%' THEN '���ԡһ�ͧ�ѹ�����'
								WHEN PRODUCT_NAME LIKE '�ѹ�����Ѻ��%' THEN '�ѹ�����Ѻ��'
								ELSE PRESCHOOL_KEYWORD END)
	,JUNIOR_KEYWORD = (CASE WHEN PRODUCT_NAME LIKE '%������������ҡ%' THEN '������������ҡ'
								WHEN PRODUCT_NAME LIKE 'ENFAGROW4%' THEN 'MILK STEP 4'
								WHEN PRODUCT_NAME LIKE 'HIQ 3PLUS%' THEN 'MILK STEP 4'
								WHEN PRODUCT_NAME LIKE 'S26 GOLD PROGRESS 4%' THEN 'MILK STEP 4'
								WHEN PRODUCT_NAME LIKE '%5 7Y' THEN '5-7 Y'
								WHEN CLEANED_BRANDNAME LIKE 'DISNEY KIDDO' THEN '����ͼ����'
								WHEN PRODUCT_NAME LIKE '�ش����Ҵ�ا�Ң���' THEN '�ͧ�����'
								WHEN PRODUCT_NAME LIKE 'ö��蹷���' THEN '�ͧ�����'
								WHEN PRODUCT_NAME LIKE '�������32����⿹���ͧ' THEN '�ͧ�����'
								WHEN PRODUCT_NAME LIKE '��Ŵ���������Ѻ��%' THEN '�ͧ�����'
								WHEN CLEANED_BRANDNAME LIKE 'AERA%' THEN '�ͧ����'
								WHEN PRODUCT_NAME LIKE 'CUTE BEAR CHAIR%' THEN '�ͧ����'
								WHEN PRODUCT_NAME LIKE '%KIDS%WATCH%��ͧ�ѹ�����%' THEN '���ԡһ�ͧ�ѹ�����'
								WHEN PRODUCT_NAME LIKE '�ѹ�����Ѻ��%' THEN '�ѹ�����Ѻ��'
								ELSE JUNIOR_KEYWORD END)
WHERE MOM_AND_KID_PRODUCT_FLAG = 1
AND COALESCE(PREG_FLAG,'N/A') = 'N/A'
AND COALESCE(INFANT_FLAG,'N/A') = 'N/A'
AND COALESCE(TODDLER_FLAG,'N/A') = 'N/A'
AND COALESCE(PRESCHOOL_FLAG,'N/A') = 'N/A'
AND COALESCE(JUNIOR_FLAG,'N/A') = 'N/A'
AND COALESCE(TEEN_FLAG,'N/A') = 'N/A'
AND (PRODUCT_NAME LIKE '%������������ҡ%'
	OR PRODUCT_NAME LIKE 'ENFAGROW4%'
	OR PRODUCT_NAME LIKE 'HIQ 3PLUS%'
	OR PRODUCT_NAME LIKE 'S26 GOLD PROGRESS 4%'
	OR PRODUCT_NAME LIKE '%5 7Y'
	OR CLEANED_BRANDNAME LIKE 'DISNEY KIDDO'
	OR PRODUCT_NAME LIKE '�ش����Ҵ�ا�Ң���'
	OR PRODUCT_NAME LIKE 'ö��蹷���'
	OR PRODUCT_NAME LIKE '�������32����⿹���ͧ'
	OR PRODUCT_NAME LIKE '��Ŵ���������Ѻ��%'
	OR CLEANED_BRANDNAME LIKE 'AERA%'
	OR PRODUCT_NAME LIKE 'CUTE BEAR CHAIR%'
	OR PRODUCT_NAME LIKE '%KIDS%WATCH%��ͧ�ѹ�����%'
	OR PRODUCT_NAME LIKE '�ѹ�����Ѻ��%')
;

--PRESCHOOL/JUNIOR/TEEN
UPDATE vpm_data.pm_skutagging_kids_product_master
SET PRESCHOOL_FLAG = 'YES'
	,JUNIOR_FLAG = 'YES'
	,TEEN_FLAG = 'YES'
	,PRESCHOOL_KEYWORD = 'STUDENT'
	,JUNIOR_KEYWORD = 'STUDENT'
	,TEEN_KEYWORD = 'STUDENT'
WHERE MOM_AND_KID_PRODUCT_FLAG = 1
AND COALESCE(PREG_FLAG,'N/A') = 'N/A'
AND COALESCE(INFANT_FLAG,'N/A') = 'N/A'
AND COALESCE(TODDLER_FLAG,'N/A') = 'N/A'
AND COALESCE(PRESCHOOL_FLAG,'N/A') = 'N/A'
AND COALESCE(JUNIOR_FLAG,'N/A') = 'N/A'
AND COALESCE(TEEN_FLAG,'N/A') = 'N/A'
AND PRODUCT_NAME LIKE '%STUDENT%'
;

--JUNIOR
UPDATE vpm_data.pm_skutagging_kids_product_master
SET JUNIOR_FLAG = 'YES'
	,JUNIOR_KEYWORD = 'SHOES EU SIZE 36'
WHERE MOM_AND_KID_PRODUCT_FLAG = 1
AND COALESCE(PREG_FLAG,'N/A') = 'N/A'
AND COALESCE(INFANT_FLAG,'N/A') = 'N/A'
AND COALESCE(TODDLER_FLAG,'N/A') = 'N/A'
AND COALESCE(PRESCHOOL_FLAG,'N/A') = 'N/A'
AND COALESCE(JUNIOR_FLAG,'N/A') = 'N/A'
AND COALESCE(TEEN_FLAG,'N/A') = 'N/A'
AND PRODUCT_NAME LIKE '%SNEAKER%36'
;

--TEEN
UPDATE vpm_data.pm_skutagging_kids_product_master
SET TEEN_FLAG = 'YES'
	,TEEN_KEYWORD = (CASE WHEN PRODUCT_NAME LIKE '%SHOE%43' THEN 'SHOES EU SIZE 43'
						WHEN PRODUCT_NAME LIKE '%SHOE%44' THEN 'SHOES EU SIZE 44'
						WHEN PRODUCT_NAME LIKE '%SHOE%45' THEN 'SHOES EU SIZE 45'
						WHEN PRODUCT_NAME LIKE '%SNEAKER%43' THEN 'SHOES EU SIZE 43'
						WHEN PRODUCT_NAME LIKE '%SNEAKER%44' THEN 'SHOES EU SIZE 44'
						WHEN PRODUCT_NAME LIKE '%SNEAKER%45' THEN 'SHOES EU SIZE 45'
						ELSE TEEN_KEYWORD END)
WHERE MOM_AND_KID_PRODUCT_FLAG = 1
AND COALESCE(PREG_FLAG,'N/A') = 'N/A'
AND COALESCE(INFANT_FLAG,'N/A') = 'N/A'
AND COALESCE(TODDLER_FLAG,'N/A') = 'N/A'
AND COALESCE(PRESCHOOL_FLAG,'N/A') = 'N/A'
AND COALESCE(JUNIOR_FLAG,'N/A') = 'N/A'
AND COALESCE(TEEN_FLAG,'N/A') = 'N/A'
AND (PRODUCT_NAME LIKE '%SHOE%43'
	OR PRODUCT_NAME LIKE '%SHOE%44'
	OR PRODUCT_NAME LIKE '%SHOE%45'
	OR PRODUCT_NAME LIKE '%SNEAKER%43'
	OR PRODUCT_NAME LIKE '%SNEAKER%44'
	OR PRODUCT_NAME LIKE '%SNEAKER%45')
;






--ALIGN PREG/INFANT STAGE AND SUB-STAGE
UPDATE vpm_data.pm_skutagging_kids_product_master
SET PREG_FLAG = 'YES'
WHERE "STAGE_M-9_TO_M-6" = 'YES'
OR "STAGE_M-6_TO_M-3" = 'YES'
OR "STAGE_M-3_TO_M-0" = 'YES'
;

UPDATE vpm_data.pm_skutagging_kids_product_master
SET INFANT_FLAG = 'YES'
WHERE "STAGE_M+0_TO_M+3" = 'YES'
OR "STAGE_M+3_TO_M+6" = 'YES'
OR "STAGE_M+6_TO_M+9" = 'YES'
OR "STAGE_M+9_TO_M+12" = 'YES'
;

UPDATE vpm_data.pm_skutagging_kids_product_master
SET UNKNOWN_PREG_SUBSTAGE = 'YES' 
WHERE COALESCE(PREG_FLAG,'N/A') = 'YES'
AND COALESCE("STAGE_M-9_TO_M-6",'N/A') = 'N/A'
AND COALESCE("STAGE_M-6_TO_M-3",'N/A') = 'N/A'
AND COALESCE("STAGE_M-3_TO_M-0",'N/A') = 'N/A'
;

UPDATE vpm_data.pm_skutagging_kids_product_master
SET UNKNOWN_INFANT_SUBSTAGE = 'YES' 
WHERE COALESCE(INFANT_FLAG,'N/A') = 'YES'
AND COALESCE("STAGE_M+0_TO_M+3",'N/A') = 'N/A'
AND COALESCE("STAGE_M+3_TO_M+6",'N/A') = 'N/A'
AND COALESCE("STAGE_M+6_TO_M+9",'N/A') = 'N/A'
AND COALESCE("STAGE_M+9_TO_M+12",'N/A') = 'N/A'
;

UPDATE vpm_data.pm_skutagging_kids_product_master
SET UNKNOWN_PREG_SUBSTAGE = 'N/A'
WHERE COALESCE(PREG_FLAG,'N/A') = 'YES'
AND (COALESCE("STAGE_M-9_TO_M-6",'N/A') = 'YES' OR COALESCE("STAGE_M-6_TO_M-3",'N/A') = 'YES' OR COALESCE("STAGE_M-3_TO_M-0",'N/A') = 'YES')
;

UPDATE vpm_data.pm_skutagging_kids_product_master
SET UNKNOWN_INFANT_SUBSTAGE = 'N/A'
WHERE COALESCE(INFANT_FLAG,'N/A') = 'YES'
AND (COALESCE("STAGE_M+0_TO_M+3",'N/A') = 'YES' OR COALESCE("STAGE_M+3_TO_M+6",'N/A') = 'YES' OR COALESCE("STAGE_M+6_TO_M+9",'N/A') = 'YES' OR COALESCE("STAGE_M+9_TO_M+12",'N/A') = 'YES')
;

UPDATE vpm_data.pm_skutagging_kids_product_master
SET UNKNOWN_PREG_SUBSTAGE = 'YES'
	,"STAGE_M-9_TO_M-6" = 'N/A'
	,"STAGE_M-6_TO_M-3" = 'N/A'
	,"STAGE_M-3_TO_M-0" = 'N/A'
WHERE COALESCE(PREG_FLAG,'N/A') = 'YES'
AND COALESCE("STAGE_M-9_TO_M-6",'N/A') = 'YES'
AND COALESCE("STAGE_M-6_TO_M-3",'N/A') = 'YES'
AND COALESCE("STAGE_M-3_TO_M-0",'N/A') = 'YES'
;

UPDATE vpm_data.pm_skutagging_kids_product_master
SET UNKNOWN_INFANT_SUBSTAGE = 'YES'
	,"STAGE_M+0_TO_M+3" = 'N/A'
	,"STAGE_M+3_TO_M+6" = 'N/A'
	,"STAGE_M+6_TO_M+9" = 'N/A'
	,"STAGE_M+9_TO_M+12" = 'N/A'
WHERE COALESCE(INFANT_FLAG,'N/A') = 'YES'
AND COALESCE("STAGE_M+0_TO_M+3",'N/A') = 'YES'
AND COALESCE("STAGE_M+3_TO_M+6",'N/A') = 'YES'
AND COALESCE("STAGE_M+6_TO_M+9",'N/A') = 'YES'
AND COALESCE("STAGE_M+9_TO_M+12",'N/A') = 'YES'
;

UPDATE vpm_data.pm_skutagging_kids_product_master
SET preg_flag = 'N/A'
WHERE "STAGE_M-9_to_M-6" = 'N/A'
AND "STAGE_M-6_to_M-3" = 'N/A'
AND "STAGE_M-3_to_M-0" = 'N/A'
AND unknown_preg_substage = 'N/A'
;

--OVERWRITE KEYWORD WITH NULL WHEN STAGE IS N/A
UPDATE vpm_data.pm_skutagging_kids_product_master
SET PREG_0_3M_KEYWORD = NULL
WHERE COALESCE("STAGE_M-9_TO_M-6",'N/A') = 'N/A'
;

UPDATE vpm_data.pm_skutagging_kids_product_master
SET PREG_4_6M_KEYWORD = NULL
WHERE COALESCE("STAGE_M-6_TO_M-3",'N/A') = 'N/A'
;

UPDATE vpm_data.pm_skutagging_kids_product_master
SET PREG_7_9M_KEYWORD = NULL
WHERE COALESCE("STAGE_M+6_TO_M+9",'N/A') = 'N/A'
;

UPDATE vpm_data.pm_skutagging_kids_product_master
SET PREG_UNKNOWN_KEYWORD = NULL
WHERE COALESCE(UNKNOWN_PREG_SUBSTAGE,'N/A') = 'N/A'
;
UPDATE vpm_data.pm_skutagging_kids_product_master
SET INFANT_0_3M_KEYWORD = NULL
WHERE COALESCE("STAGE_M+0_TO_M+3",'N/A') = 'N/A'
;

UPDATE vpm_data.pm_skutagging_kids_product_master
SET INFANT_4_6M_KEYWORD = NULL
WHERE COALESCE("STAGE_M+3_TO_M+6",'N/A') = 'N/A'
;

UPDATE vpm_data.pm_skutagging_kids_product_master
SET INFANT_7_9M_KEYWORD = NULL
WHERE COALESCE("STAGE_M+6_TO_M+9",'N/A') = 'N/A'
;

UPDATE vpm_data.pm_skutagging_kids_product_master
SET INFANT_10_12M_KEYWORD = NULL
WHERE COALESCE("STAGE_M+9_TO_M+12",'N/A') = 'N/A'
;

UPDATE vpm_data.pm_skutagging_kids_product_master
SET INFANT_UNKNOWN_KEYWORD = NULL
WHERE COALESCE(UNKNOWN_INFANT_SUBSTAGE,'N/A') = 'N/A'
;

UPDATE vpm_data.pm_skutagging_kids_product_master
SET TODDLER_KEYWORD = NULL
WHERE COALESCE(TODDLER_FLAG,'N/A') = 'N/A'
;

UPDATE vpm_data.pm_skutagging_kids_product_master
SET PRESCHOOL_KEYWORD = NULL
WHERE COALESCE(PRESCHOOL_FLAG,'N/A') = 'N/A'
;

UPDATE vpm_data.pm_skutagging_kids_product_master
SET JUNIOR_KEYWORD = NULL
WHERE COALESCE(JUNIOR_FLAG,'N/A') = 'N/A'
;

UPDATE vpm_data.pm_skutagging_kids_product_master
SET TEEN_KEYWORD = NULL
WHERE COALESCE(TEEN_FLAG,'N/A') = 'N/A'
;

--LAST STEP
UPDATE vpm_data.pm_skutagging_kids_product_master
SET "STAGE_M-9_TO_M-6" = 'N/A'
	,"STAGE_M-6_TO_M-3" = 'N/A'
	,"STAGE_M-3_TO_M-0" = 'N/A'
	,UNKNOWN_PREG_SUBSTAGE = 'N/A'
	,PREG_FLAG = 'N/A'
	,PREG_0_3M_KEYWORD = NULL
	,PREG_4_6M_KEYWORD = NULL
	,PREG_7_9M_KEYWORD = NULL
	,PREG_UNKNOWN_KEYWORD = NULL
	,"STAGE_M+0_TO_M+3" = 'N/A'
	,"STAGE_M+3_TO_M+6" = 'N/A'
	,"STAGE_M+6_TO_M+9" = 'N/A'
	,"STAGE_M+9_TO_M+12" = 'N/A'
	,UNKNOWN_INFANT_SUBSTAGE = 'N/A'
	,INFANT_FLAG = 'N/A'
	,INFANT_0_3M_KEYWORD = NULL
	,INFANT_4_6M_KEYWORD = NULL
	,INFANT_7_9M_KEYWORD = NULL
	,INFANT_10_12M_KEYWORD = NULL
	,INFANT_UNKNOWN_KEYWORD = NULL
	,TODDLER_FLAG = 'N/A'
	,TODDLER_KEYWORD = NULL
	,PRESCHOOL_FLAG = 'N/A'
	,PRESCHOOL_KEYWORD = NULL
	,JUNIOR_FLAG = 'N/A'
	,JUNIOR_KEYWORD = NULL
	,TEEN_FLAG = 'N/A'
	,TEEN_KEYWORD = NULL
WHERE MOM_AND_KID_PRODUCT_FLAG = 0
;

--OVERWRITE NULL WITH N/A
UPDATE vpm_data.pm_skutagging_kids_product_master
SET PREG_FLAG = 'N/A'
WHERE PREG_FLAG IS NULL
;

UPDATE vpm_data.pm_skutagging_kids_product_master
SET UNKNOWN_PREG_SUBSTAGE = 'N/A'
WHERE UNKNOWN_PREG_SUBSTAGE IS NULL
;

UPDATE vpm_data.pm_skutagging_kids_product_master
SET "STAGE_M-9_TO_M-6" = 'N/A'
WHERE "STAGE_M-9_TO_M-6" IS NULL
;

UPDATE vpm_data.pm_skutagging_kids_product_master
SET "STAGE_M-6_TO_M-3" = 'N/A'
WHERE "STAGE_M-6_TO_M-3" IS NULL
;

UPDATE vpm_data.pm_skutagging_kids_product_master
SET "STAGE_M-3_TO_M-0" = 'N/A'
WHERE "STAGE_M-3_TO_M-0" IS NULL
;

UPDATE vpm_data.pm_skutagging_kids_product_master
SET INFANT_FLAG = 'N/A'
WHERE INFANT_FLAG IS NULL
;

UPDATE vpm_data.pm_skutagging_kids_product_master
SET UNKNOWN_INFANT_SUBSTAGE = 'N/A'
WHERE UNKNOWN_INFANT_SUBSTAGE IS NULL
;

UPDATE vpm_data.pm_skutagging_kids_product_master
SET "STAGE_M+0_TO_M+3" = 'N/A'
WHERE "STAGE_M+0_TO_M+3" IS NULL
;

UPDATE vpm_data.pm_skutagging_kids_product_master
SET "STAGE_M+3_TO_M+6" = 'N/A'
WHERE "STAGE_M+3_TO_M+6" IS NULL
;

UPDATE vpm_data.pm_skutagging_kids_product_master
SET "STAGE_M+6_TO_M+9" = 'N/A'
WHERE "STAGE_M+6_TO_M+9" IS NULL
;

UPDATE vpm_data.pm_skutagging_kids_product_master
SET "STAGE_M+9_TO_M+12" = 'N/A'
WHERE "STAGE_M+9_TO_M+12" IS NULL
;

UPDATE vpm_data.pm_skutagging_kids_product_master
SET TODDLER_FLAG = 'N/A'
WHERE TODDLER_FLAG IS NULL
;

UPDATE vpm_data.pm_skutagging_kids_product_master
SET PRESCHOOL_FLAG = 'N/A'
WHERE PRESCHOOL_FLAG IS NULL
;

UPDATE vpm_data.pm_skutagging_kids_product_master
SET JUNIOR_FLAG = 'N/A'
WHERE JUNIOR_FLAG IS NULL
;

UPDATE vpm_data.pm_skutagging_kids_product_master
SET TEEN_FLAG = 'N/A'
WHERE TEEN_FLAG IS NULL
;

--2.0 New Category
------------------------------------------------------------BY BU SCHEMA
UPDATE vpm_data.pm_skutagging_kids_product_master
SET new_type_final = (CASE /* BOOK */
							WHEN subclass_name LIKE 'PREGNANCY & CHILDBIRTH' THEN 'PREGNANCY BOOK'
							WHEN subclass_name LIKE 'PREGNANCY' THEN 'PREGNANCY BOOK'
							WHEN subdept_name LIKE '��������' THEN 'PARENT GUIDE BOOK'
							WHEN subdept_name LIKE '�Ե������������' THEN 'PARENT GUIDE BOOK'
							WHEN class_name LIKE 'MOTHER & CHILD' AND subclass_name LIKE 'OTHERS/EXT VAT' THEN 'PARENT GUIDE BOOK'
							WHEN subclass_name IN ('MOTHER & CHILDCARE','BABY JOURNALS','CHILDCARE') THEN 'PARENT GUIDE BOOK'
							WHEN class_name LIKE 'MOTHER & CHILD' AND subclass_name LIKE 'BABY FOOD' THEN 'COOK BOOK'
							WHEN subclass_name LIKE 'COOKING FOR/WITH CHILDREN' THEN 'COOK BOOK'
							WHEN class_name IN ('KINDERGARDEN - PRIMARY SC','PRE-SCHOOL GUIDE BOOK') AND subclass_name LIKE 'ENGLISH' THEN 'PRESCHOOL ENGLISH BOOK'
							WHEN class_name LIKE 'AGES 4-8/EDUCATIONAL BOOK' AND subclass_name LIKE 'LANGUAGE' THEN 'PRESCHOOL ENGLISH BOOK'
							WHEN subclass_name IN ('ENGLISH LANGUAGE','WORD CARD','WORD CARD(IN-VAT)','SPEACKING AND CONVERSATIO') THEN 'PRESCHOOL ENGLISH BOOK'
							WHEN class_name LIKE 'PRE-SCHOOL GUIDE BOOK' AND subclass_name LIKE 'THAI' THEN 'PRESCHOOL THAI BOOK'
							WHEN subclass_name LIKE 'ALPHABET' THEN 'PRESCHOOL THAI BOOK'
							WHEN class_name LIKE 'AGES 4-8/EDUCATIONAL BOOK' AND subclass_name LIKE 'MATH' THEN 'PRESCHOOL MATH BOOK'
							WHEN class_name IN ('PRE-SCHOOL GUIDE BOOK','KINDERGARDEN - PRIMARY SC') AND subclass_name LIKE 'MATHEMATIC' THEN 'PRESCHOOL MATH BOOK'
							WHEN subclass_name IN ('ALPHABET & NUMBER','NUMBER & TIME') THEN 'PRESCHOOL MATH BOOK'
							WHEN dept_name LIKE 'FOREIGN BOOKS' AND class_name LIKE '%EDUCATIONAL%' THEN 'SCHOOL BOOK'
							WHEN subdept_name LIKE 'EXAMINATION GUIDE' THEN 'SCHOOL BOOK'
							WHEN class_name IN ('CHILDRENS REFERENCE MATER','TEENAGE REFERENCE MATERTI','JUNIOR HIGH SCHOOL(M1-3)','JUNIOR PRIMARY SCHOOL'
									,'LEARNING MEDIA HIGHSCHOOL','LEARNING MEDIA PRIMARY','PRIMARY SCHOOL','SENIOR HIGH SCHOOL(M4-6)'
									,'STUDY AND EXAMINATION GUI','TEXT BOOKS FOR PRIMARY SC','TEXT BOOKS FOR JUNIOR HIG','TEXT BOOKS FOR SENIOR HIG') THEN 'SCHOOL BOOK'
							WHEN class_name LIKE 'KINDERGARDEN - PRIMARY SC' AND subclass_name NOT IN ('ENGLISH','MATHEMATIC','ALPHABET','ENGLISH LANGUAGE'
									,'NUMBER & TIME') THEN 'SCHOOL BOOK'
							WHEN class_name IN ('BABY - PRESCHOOL/ACTIVITY','AGES 4-8/ ACTIVITY BOOKS','AGES 9-12/ ACTIVITY BOOKS','ACTIVITY') THEN 'ACTIVITY BOOK'
							WHEN subclass_name IN ('CHILDREN S ACTIVITY BOOKS','DRAWING') THEN 'ACTIVITY BOOK'
							WHEN subdept_name IN ('����ٹ��/����ٹ�������','˹ѧ����� / �Էҹ�Ҿ����Ѻ��') THEN 'TALE BOOK'
							WHEN subdept_name LIKE 'CHILDREN' AND class_name LIKE '%STORY%' THEN 'TALE BOOK'
							WHEN class_name IN ('BABY - PRESCHOOL/ STORY B','AGES 4-8/ STORY BOOKS','AGES 9-12/ STORY BOOKS','YOUNG ADULT FICTION'
									,'TEENAGE FICTION & TRUE','CHILDRENS FICTION&TRUE','TALE') THEN 'TALE BOOK'
							WHEN subclass_name IN ('THAI TALE','TALES','WALT DISNEY TALES') THEN 'TALE BOOK'
							WHEN dept_name LIKE 'FOREIGN BOOKS' AND subclass_name LIKE 'KIDS' THEN 'OTHER KID BOOK'
							WHEN class_name IN ('BOOK & AUDIO','CHILDRENS GENERAL NON-FIC','CHILDRENS PICTURE BOOKS'
												,'TEENAGE GENERAL NON-FICTI','CHILDRENS&TEENAGE POETRY','SPECIAL PROJECT') THEN 'OTHER KID BOOK'
							WHEN class_name LIKE 'EDUCATION MEDIA' AND subclass_name IN ('BOARD BOOK','CHINESE LANGUAGE','EDU MEDIA BOOK SET'
									,'I.Q TESTING','JAPANESE LANGUAGE','OTHERS/IN-VAT','POSTER') THEN 'OTHER KID BOOK'
							WHEN class_name LIKE 'CREATING MEDIA FOR KID' AND subclass_name IN ('ACTING','I.Q TESTING','JIGSAW','KID SONG','POSTER'
									,'WIT PRACTICE','WRONG OR RIGHT GAME','OTHERS/EXT VAT','OTHERS') THEN 'OTHER KID BOOK'
							WHEN class_name LIKE 'PREMIUM' AND subclass_name LIKE 'OTHERS' THEN 'OTHER KID BOOK'
							WHEN class_name LIKE 'PRE-SCHOOL' AND subclass_name LIKE '%OTHER%' THEN 'OTHER KID BOOK'
							WHEN class_name LIKE 'PRE-SCHOOL GUIDE BOOK' AND subclass_name LIKE '%OTHER%' THEN 'OTHER KID BOOK'
							WHEN class_name LIKE 'LEARNING MEDIA PRE-SCHOOL' AND subclass_name LIKE '%OTHER%' THEN 'OTHER KID BOOK'
							WHEN class_name LIKE 'BABY - PRESCHOOL/BASIC LE' AND subclass_name NOT LIKE 'ALPHABET & NUMBER' THEN 'OTHER KID BOOK'
							WHEN class_name LIKE 'REFERENCE' AND subclass_name NOT LIKE 'ENGLISH LANGUAGE' THEN 'OTHER KID BOOK'
							WHEN class_name LIKE 'BOOKS & PRINTED MEDIA/NON FMCG' THEN 'OTHER KID BOOK'
							WHEN class_name LIKE 'CHILDREN BOOK' THEN 'OTHER KID BOOK'
							WHEN class_name LIKE 'HEALTH AND HERBS' AND subclass_name LIKE 'BODY CARE' THEN 'OTHER KID BOOK'
							WHEN class_name LIKE 'HOBBIES & CRAFT' AND subclass_name LIKE 'NEEDLEPOINT' THEN 'OTHER KID BOOK'
							WHEN class_name LIKE 'PSYCHOLOGY/RELATIONSHIP' AND subclass_name LIKE 'FAMILY PSYCHOLOGY' THEN 'OTHER KID BOOK'
							WHEN class_name IN ('CARTOON  / GAME','FEATURE / DHARMA') AND subclass_name LIKE 'OTHERS/EXT VAT' THEN 'OTHER KID BOOK'
							WHEN class_name LIKE 'EDUCATION COMIC' THEN 'OTHER KID BOOK'
							WHEN subclass_name LIKE 'NEEDLECRAFT' THEN 'OTHER KID BOOK'
							WHEN dept_name LIKE 'ENTERTAINMENT MEDIA' AND subclass_name LIKE '%CHILDREN%' THEN 'OTHER KID BOOK'
							WHEN dept_name LIKE 'THAI BOOK' THEN 'OTHER KID BOOK'
							/* DIAPERS */
							WHEN class_name LIKE 'BABY DIAPERS' THEN 'OTHER DIAPERS'
							WHEN class_name LIKE 'DIAPERS' THEN 'OTHER DIAPERS'
							WHEN class_name LIKE 'BEDDING' AND subclass_name LIKE 'CLOTH DIAPER' THEN 'OTHER DIAPERS'
							WHEN subclass_name IN ('BABY DIAPERS','INACTIVE / COTTON, BABY DIAPER') THEN 'OTHER DIAPERS'
							/* KID FOOD */
							WHEN subclass_name IN ('INFANT DAIRY FORMULA','INFANT SOY/SPECIAL FORMULA') THEN 'MILK POWDER FORMULA 1'
							WHEN subclass_name IN ('INFANT FOLLOW-ON MILK POWDER','FOLLOW-ON FORMULA') THEN 'MILK POWDER FORMULA 2'
							WHEN subclass_name IN ('GROWING UP MILK POWDER','1-PLUS') THEN 'MILK POWDER FORMULA 3'
							WHEN subclass_name LIKE 'KIDS UHT MILK' THEN 'KID UHT MILK'
							WHEN subclass_name IN ('POWDER MILK','UHT MILK','3-PLUS','INACTIVE / BABY MILK POWDER') THEN 'OTHER MILK POWDER'
							WHEN class_name LIKE 'POWDER MILK' THEN 'OTHER MILK POWDER'
							WHEN class_name LIKE 'LAYETTE' AND subclass_name LIKE 'FOOD & SNACK' THEN 'TABLE FOOD'
							WHEN subclass_name IN ('BABY FOOD','INFANT CEREAL','JAR BABY FOOD') THEN 'TABLE FOOD'
							/* MATERNITY */
							WHEN dept_name LIKE 'GARMENT' AND class_name LIKE 'MATERNITY' THEN 'MATERNITY CLOTHING'
							WHEN dept_name LIKE 'WOMEN' AND class_name LIKE 'DRESS' THEN 'MATERNITY CLOTHING'
							WHEN subdept_name LIKE 'LADIES WEAR' THEN 'MATERNITY CLOTHING'
							WHEN class_name LIKE 'MATERNITY' AND subclass_name LIKE 'CLOTHING' THEN 'MATERNITY CLOTHING'
							WHEN class_name LIKE 'MATERNITY INNERWEARS&PAJA' AND subclass_name LIKE 'MT INNER/PAJAMAS HOME' THEN 'MATERNITY CLOTHING'
							WHEN dept_name LIKE 'READY TO WEAR' AND subdept_name LIKE 'MATERNITY' THEN 'MATERNITY CLOTHING'
							WHEN dept_name LIKE 'WOMEN' AND subdept_name LIKE 'LINGERIE' THEN 'MATERNITY LINGERIE'
							WHEN class_name LIKE 'MATERNITY INNERWEARS&PAJA' AND subclass_name LIKE 'MT INNER/UNDERWEAR' THEN 'MATERNITY LINGERIE'
							WHEN class_name LIKE 'MATERNITY' AND subclass_name LIKE 'LINGERIE' THEN 'MATERNITY LINGERIE'
							WHEN subclass_name LIKE 'BREAST PADS' THEN 'MATERNITY BREAST PADS'
							WHEN subclass_name LIKE 'BREAST FEEDING' THEN 'MATERNITY MILK PUMP MACHINE'
							WHEN subdept_name LIKE 'MATERNAL FOOD' THEN 'MATERNITY SUPPLEMENT'
							WHEN class_name LIKE 'MATERNAL FOOD' THEN 'MATERNITY SUPPLEMENT'
							WHEN dept_name IN ('BEAUTY','BEAUTY&PERSONAL CARE','HBC') THEN 'MATERNITY TREATMENT'
							WHEN class_name LIKE 'MATERNITY' AND subclass_name LIKE 'TOILETRY FOR MOM' THEN 'MATERNITY TREATMENT'
							WHEN subclass_name LIKE 'FACE SPA' THEN 'MATERNITY TREATMENT'
							/* KID CLOTHING */
							WHEN dept_name LIKE 'APPAREL' AND subdept_name NOT LIKE 'INNERWEAR' THEN 'KID DAYWEAR'
							WHEN dept_name LIKE 'CHILDREN S FASHION' AND class_name LIKE 'TODDLER' AND subclass_name NOT LIKE 'PYJAMAS/SWIMWEAR' THEN 'KID DAYWEAR'
							WHEN dept_name LIKE 'JUNIOR SPORTS' AND subdept_name LIKE 'APPAREL' AND subclass_name NOT LIKE 'CONSIGNMENT' THEN 'KID DAYWEAR'
							WHEN dept_name LIKE 'LUXURY' AND subclass_name IN ('BLOUSE','COAT','DRESS','JACKET','JEANS','LEATHERWEAR','PANTS/TROUSERS'
									,'POLO','SHIRT','SKIRT','SUIT','SWEAT SHIRT','SWEATER','SWEATPANTS','T-SHIRT') THEN 'KID DAYWEAR'
							WHEN dept_name LIKE 'LUXURY' AND class_name LIKE 'OTHERS' AND subclass_name LIKE 'OTHERS' THEN 'KID DAYWEAR'
							WHEN subdept_name IN ('OTHER - APPAREL') THEN 'KID DAYWEAR'
							WHEN subdept_name LIKE 'BABY S WEAR' AND class_name NOT IN ('ACCESSORIES','PYJAMAS','UNDERWEAR') THEN 'KID DAYWEAR'
							WHEN subdept_name LIKE 'KID S WEAR' AND class_name NOT IN ('ACCESSORIES','PYJAMAS','UNDERWEAR') THEN 'KID DAYWEAR'
							WHEN subdept_name LIKE 'CHILDRENS WEAR' AND class_name LIKE 'BABY S' THEN 'KID DAYWEAR'
							WHEN subdept_name LIKE 'CHILDRENS WEAR' AND class_name LIKE 'TODDLER' THEN 'KID DAYWEAR'
							WHEN subdept_name LIKE 'KIDS LUXURY' AND subclass_name IN ('CLASSIC PANTS','CUT&SEWN','FASHION PANTS','KNITTED WEAR'
									,'OVERALL','RTW BOXSET','RTW OTHER','SPORTWEAR PANTS','SUITS','VEST','KIDS T-SHIRT') THEN 'KID DAYWEAR'
							WHEN class_name IN ('BABY''S WEAR','CHILDREN FASHION') THEN 'KID DAYWEAR'
							WHEN class_name LIKE 'LAYETTE' AND subclass_name LIKE 'OTHERS' THEN 'KID DAYWEAR'
							WHEN class_name LIKE 'NEWBORN/INFANT' AND subclass_name NOT LIKE 'ACCESSORIES' THEN 'KID DAYWEAR'
							WHEN subclass_name IN ('GOLF APP KIDS BOTTOM','KIDS SPORT','KIDS T-SHIRT') THEN 'KID DAYWEAR'
							WHEN class_name IN ('PYJAMAS','CHILDREN S PAJAMAS&HOMEWE') THEN 'KID NIGHTWEAR'
							WHEN class_name LIKE 'INNERWEAR / ACCESSORIES' AND subclass_name LIKE '%PAJAMA%' THEN 'KID NIGHTWEAR'
							WHEN subdept_name LIKE 'BABY S WEAR' AND class_name LIKE 'UNDERWEAR' THEN 'KID UNDERWEAR'
							WHEN subdept_name LIKE 'KID S WEAR' AND class_name LIKE 'UNDERWEAR' THEN 'KID UNDERWEAR'
							WHEN subdept_name LIKE 'CHILDREN S ACCESSORIES' AND class_name LIKE 'UNDERWEAR' THEN 'KID UNDERWEAR'
							WHEN class_name IN ('KIDS S UNDERWEAR','CHILDREN S INNERWEARS','UNDERWARE/LINGERIES') THEN 'KID UNDERWEAR'
							WHEN class_name LIKE 'INNERWEAR / ACCESSORIES' AND subclass_name LIKE 'INNER%' THEN 'KID UNDERWEAR'
							WHEN class_name LIKE '%UNDERWEAR%' THEN 'KID UNDERWEAR'
							WHEN class_name LIKE 'CHILDREN''S INNERWEARS' THEN 'KID UNDERWEAR'
							WHEN subclass_name LIKE 'UNDERWEAR' THEN 'KID UNDERWEAR'
							WHEN dept_name LIKE 'APPAREL' AND subdept_name LIKE 'INNERWEAR' THEN 'KID SWIMWEAR'
							WHEN dept_name LIKE 'SWIMWEAR' AND subclass_name LIKE 'JUNIOR' THEN 'KID SWIMWEAR'
							WHEN subdept_name IN ('WATER SPORT','BEACHWEAR','GOGGLE','PERFORMANCE SWIM CAP','PERFORMANCE SWIMWEAR'
									,'SUN PROTECTION APPAREL','WATERSHORTS') THEN 'KID SWIMWEAR'
							WHEN subdept_name LIKE 'KIDS LUXURY' AND subclass_name LIKE 'SWIMWEAR' THEN 'KID SWIMWEAR'
							WHEN class_name LIKE 'TODDLER' AND subclass_name LIKE 'PYJAMAS/SWIMWEAR' THEN 'KID SWIMWEAR'
							WHEN dept_name LIKE 'SWIMWEAR' THEN 'KID SWIMWEAR'
							WHEN class_name LIKE 'SWIMWEAR' THEN 'KID SWIMWEAR'
							WHEN subclass_name LIKE 'SWIMWEAR' THEN 'KID SWIMWEAR'
							/* KID SHOES AND KID FASHION ACCESSORIES */
							WHEN dept_name IN ('CASUAL FOOTWEAR','PERFORMANCE FOOTWEAR') THEN 'KID SHOES'
							WHEN dept_name LIKE 'CROCS' AND subdept_name LIKE 'FOOTWEAR' THEN 'KID SHOES'
							WHEN dept_name LIKE 'LUXURY' AND subclass_name LIKE 'SHOES' THEN 'KID SHOES'
							WHEN dept_name LIKE 'SWIMWEAR' AND subdept_name LIKE 'FOOTWEAR' THEN 'KID SHOES'
							WHEN dept_name LIKE 'STANDALONE' AND subclass_name LIKE 'PLUS SLIPPER' THEN 'KID SHOES'
							WHEN dept_name LIKE 'JUNIOR SPORTS' AND subdept_name LIKE 'FOOTWEAR' THEN 'KID SHOES'
							WHEN dept_name LIKE 'JUNIOR SPORTS' AND subclass_name LIKE 'CONSIGNMENT' THEN 'KID SHOES'
							WHEN subdept_name IN ('KID S SHOES','FUNCTIONAL SHOES','FUTSAL SHOES','SOCCER SHOES','FOOTWEAR') THEN 'KID SHOES'
							WHEN subdept_name LIKE 'BAG & SHOES' AND class_name IN ('OTHER SHOES','SANDALS','SNEAKER') THEN 'KID SHOES'
							WHEN subdept_name LIKE 'KID S WEAR' AND subclass_name LIKE 'SHOES' THEN 'KID SHOES'
							WHEN subdept_name LIKE 'KIDS LUXURY' AND subclass_name LIKE 'SHOES' THEN 'KID SHOES'
							WHEN class_name LIKE 'INNERWEAR / ACCESSORIES' AND subclass_name LIKE 'SHOES' THEN 'KID SHOES'
							WHEN class_name LIKE 'SHOES' THEN 'KID SHOES'
							WHEN subclass_name LIKE 'SHOES' THEN 'KID SHOES'
							WHEN subclass_name LIKE 'GOLF SHOES KIDS CSM' THEN 'KID SHOES'
							WHEN dept_name LIKE 'FASHION' AND subdept_name LIKE 'ACCESSORIES' THEN 'KID FASHION ACCESSORIES'
							WHEN dept_name LIKE 'GARMENT' AND subdept_name LIKE 'ACCESSORIES' THEN 'KID FASHION ACCESSORIES'
							WHEN dept_name LIKE 'JUNIOR SPORTS' AND subdept_name LIKE 'ACCESSORIES' AND subclass_name NOT LIKE 'CONSIGNMENT' THEN 'KID FASHION ACCESSORIES'
							WHEN dept_name LIKE 'LUXURY' AND class_name LIKE 'ACCESSORIES' AND subclass_name NOT LIKE 'SHOES' THEN 'KID FASHION ACCESSORIES'
							WHEN subdept_name LIKE 'KID S WEAR' AND class_name LIKE 'ACCESSORIES' AND subclass_name NOT LIKE 'SHOES' THEN 'KID FASHION ACCESSORIES'
							WHEN subdept_name LIKE 'BABY S WEAR' AND class_name LIKE 'ACCESSORIES' THEN 'KID FASHION ACCESSORIES'
							WHEN subdept_name LIKE 'BAG & SHOES' AND class_name LIKE 'MEN S BAGS' THEN 'KID FASHION ACCESSORIES'
							WHEN subdept_name LIKE 'KID S ACCESSORIES' AND class_name IN ('COSTUME JEWELRY','FASHION') THEN 'KID FASHION ACCESSORIES'
							WHEN subdept_name LIKE 'KIDS LUXURY' AND subclass_name IN ('ACC BOXSET','ACC OTHER','BAG','BELT','NECKTIE') THEN 'KID FASHION ACCESSORIES'
							WHEN subdept_name LIKE 'FASHION' AND class_name LIKE 'ACCESSORIES' THEN 'KID FASHION ACCESSORIES'
							WHEN class_name IN ('BAG','SMIGGLE','SMART WATCH KIDS','FASHION & ACCESSORIES' ) THEN 'KID FASHION ACCESSORIES'
							WHEN class_name LIKE 'BIB' AND subclass_name LIKE 'BIB' THEN 'KID FASHION ACCESSORIES'
							WHEN class_name LIKE 'NEWBORN/INFANT' AND subclass_name LIKE 'ACCESSORIES' THEN 'KID FASHION ACCESSORIES'
							WHEN class_name LIKE 'INNERWEAR / ACCESSORIES' AND subclass_name IN ('ACC','BAG','RUCKSACK','HATS','RAIN','SOCKS','OTHERS') THEN 'KID FASHION ACCESSORIES'
							WHEN subclass_name IN ('SOCK','GLOVE LH KIDS','GLOVE RH KIDS','IRON SET JUNIOR','JUNIOR RACKET') THEN 'KID FASHION ACCESSORIES'
							/* KID ACCESSORIES */
							WHEN class_name LIKE 'LAYETTE' AND subclass_name LIKE 'FEEDING' THEN 'FEEDING BOTTLE AND NIPPLE'
							WHEN subclass_name IN ('BABY NIPPLE','FEEDING BOTTLE/BREAST PUMP','FEEDING BOTTLE AND NIPPLE','BOTTLE FEEDING') THEN 'FEEDING BOTTLE AND NIPPLE'
							WHEN subclass_name IN ('BABY LIQUID DISH DETERGENT','CLEANING ITEMS','SOOTHER','TRAVEL PRODUCT') THEN 'FEEDING ACCESSORIES'
							WHEN subclass_name LIKE 'STERILISER&FOOB WARMING' THEN 'BOTTLE WARMER'
							WHEN class_name LIKE 'MUGS' THEN 'WEANING'
							WHEN subclass_name IN ('WEANING','COOKING SET','FEEDING SET','CUP FEEDING','TODDLER MEAL TIME') THEN 'WEANING'
							WHEN class_name LIKE 'TOOTHBRUSH' THEN 'KID BATHROOM ACCESSORIES'
							WHEN subclass_name IN ('BATH ACCESS.','BATH ACCESSORIES','BATHING CARE','STEP UP POTTY','TOILET TRAINER'
									,'CHILDREN S TOOTHBRUSHES','KIDS TOOTHPASTE') THEN 'KID BATHROOM ACCESSORIES'
							WHEN subdept_name IN ('GOLF','RACKET','SUPPLY USE') THEN 'OTHER KID ACCESSORIES'
							WHEN subdept_name LIKE 'ACCESSORIES' AND class_name LIKE 'ACCESSORIES' AND subclass_name LIKE 'ACCESSORIES' THEN 'OTHER KID ACCESSORIES'
							WHEN class_name IN ('GIFT SET','SUNSCREEN FOR BODY','COSMETICS') THEN 'OTHER KID ACCESSORIES'
							WHEN class_name LIKE 'CLEANING' AND subclass_name LIKE 'TOILETRY' THEN 'OTHER KID ACCESSORIES'
							WHEN class_name LIKE 'CLEANING' AND subclass_name LIKE 'WASHING' THEN 'OTHER KID ACCESSORIES'
							WHEN class_name LIKE 'CLEANING' AND subclass_name LIKE 'OTHERS' THEN 'OTHER KID ACCESSORIES'
							WHEN class_name LIKE 'LAYETTE' AND subclass_name LIKE 'PERSONAL CARE' THEN 'OTHER KID ACCESSORIES'
							WHEN class_name LIKE 'SAFETY ACCESS.' AND subclass_name LIKE 'MONITOR THERMOMETER' THEN 'OTHER KID ACCESSORIES'
							WHEN class_name LIKE 'SAFETY ACCESS.' AND subclass_name LIKE 'SANTITIZER' THEN 'OTHER KID ACCESSORIES'
							WHEN subclass_name IN ('THERMOMETERS','SAFETY ACCESSORIES','GIFT SET','BABY FABRIC SOFTENER'
									,'BABY LIQUID DETERGENT','MOTHER HEALTHY ACCESSORIES','BABIES GOODS','BABY ACCESSORIES'
									,'HEALTHY CARE','INACTIVE / BABY ACCESSORIES') THEN 'OTHER KID ACCESSORIES'
							/* KID CAR SEAT AND TRAVEL GEAR */
							WHEN subclass_name IN ('STROLLER','WHEEL GOODS') THEN 'STROLLER'
							WHEN subclass_name LIKE 'BABY CARRIERS' THEN 'BABY CARRIER'
							WHEN subclass_name IN ('CARSEAT','CAR SEAT') THEN 'CAR SEAT'
							/* KID FURNITURE */
							WHEN class_name LIKE 'BEDDING' AND subclass_name NOT LIKE 'CLOTH DIAPER' THEN 'KID BEDDING'
							WHEN class_name LIKE 'PLUSH TOY' AND subclass_name LIKE 'BLANKET' THEN 'KID BEDDING'
							WHEN subclass_name IN ('BEDDING/CLOTHING','KID S FURNITURE') THEN 'KID BEDDING'
							WHEN class_name LIKE 'BABY GOODS' AND subclass_name LIKE 'BEDDING/BOOKS' THEN 'KID BEDDING'
							WHEN class_name LIKE 'WOOD FURNISH' THEN 'OTHER KID FURNITURE'
							WHEN subclass_name IN ('HIGHCHAIR','PLAY YARD','CABINET') THEN 'OTHER KID FURNITURE'
							/* TOYS */
							WHEN subclass_name LIKE 'BOUNCER/SWING' THEN 'BABY BOUNCER'
							WHEN subclass_name LIKE 'WALKER' THEN 'BABY WALKER'
							WHEN dept_name IN ('CYCLING','PLAY & LEARN') THEN 'OTHER TOYS'
							WHEN dept_name LIKE 'STANDALONE' AND class_name IN ('TOYS','COLLECTIBLES AND DIY','PLUSH') THEN 'OTHER TOYS'
							WHEN dept_name LIKE 'STANDALONE' AND class_name LIKE 'PLUSH& TOY' AND subclass_name NOT LIKE 'PLUS SLIPPER' THEN 'OTHER TOYS'
							WHEN subdept_name IN ('KIDS & TOYS','TOYS') THEN 'OTHER TOYS'
							WHEN class_name LIKE 'BABY TOYS' THEN 'OTHER TOYS'
							WHEN subclass_name LIKE 'BABY TOYS' THEN 'OTHER TOYS'
							WHEN class_name LIKE 'PLUSH TOY' AND subclass_name NOT LIKE 'BLANKET' THEN 'OTHER TOYS'
							WHEN class_name LIKE 'BABY ENTERTAINER' AND subclass_name LIKE 'OTHERS' THEN 'OTHER TOYS'
							WHEN class_name LIKE 'CREATING MEDIA FOR KID' AND subclass_name LIKE 'TOY' THEN 'OTHER TOYS'
							WHEN class_name LIKE 'LEARNING MEDIA PRE-SCHOOL' AND subclass_name LIKE 'THAI' THEN 'OTHER TOYS'
							WHEN subclass_name IN ('INFANT TOYS','TOYS','DOLL & SOFT TOY','MUSIC BOX') THEN 'OTHER TOYS'
							END)
WHERE mom_and_kid_product_flag = 1
AND COALESCE(new_type_final,'NULL') NOT LIKE '%MATERNITY%'
;

------------------------------------------------------------BY TYPE FINAL
UPDATE vpm_data.pm_skutagging_kids_product_master
SET new_type_final = (CASE /* KID BOOK */
							WHEN type_final LIKE 'PREGNANCY BOOK' THEN 'PREGNANCY BOOK'
							WHEN type_final LIKE 'CHILDCARE BOOK' THEN 'PARENT GUIDE BOOK'
							WHEN type_final LIKE 'BOOK' THEN 'COOK BOOK'
							WHEN type_final LIKE 'EDUCATION MATERIAL' THEN 'SCHOOL BOOK'
							WHEN type_final LIKE 'ACTIVITY BOOK' THEN 'ACTIVITY BOOK'
							WHEN type_final LIKE 'TALE BOOK' THEN 'TALE BOOK'
							WHEN type_final LIKE 'MEDIA&ENTERTAINMENT' THEN 'OTHER KID BOOK'
							WHEN type_final LIKE 'BOOK' THEN 'OTHER KID BOOK'
							WHEN COALESCE(type_final,'NULL') LIKE 'NULL' AND revised_category_level_1 = 'BOOK & STATIONERY' THEN 'OTHER KID BOOK'
							WHEN type_final LIKE 'STATIONERY' THEN 'KID STATIONERY'
							/* DIAPER */
							WHEN type_final LIKE 'DIAPERS' THEN 'OTHER DIAPERS'
							WHEN COALESCE(type_final,'NULL') LIKE 'NULL' AND revised_category_level_3 = 'DIAPERS' THEN 'OTHER DIAPERS'
							/* KID FOOD */
							WHEN type_final LIKE 'MILK POWDER' THEN 'OTHER MILK POWDER'
--							WHEN type_final LIKE 'FOOD' AND SubCatName LIKE 'Follow-On Formula' THEN 'MILK POWDER FORMULA 2'
--							WHEN type_final LIKE 'FOOD' AND SubCatName LIKE '1-Plus' THEN 'MILK POWDER FORMULA 3'
--							WHEN type_final LIKE 'FOOD' AND SubCatName LIKE '3-Plus' THEN 'OTHER MILK POWDER'
							WHEN COALESCE(type_final,'NULL') LIKE 'NULL' AND revised_category_level_3 = 'MILK POWDER' THEN 'OTHER MILK POWDER'
							WHEN type_final LIKE 'NURISHMENT' THEN 'TABLE FOOD'
							/* MATERNITY */
							WHEN type_final LIKE 'MATERNITY BAG' THEN 'MATERNITY BAG'
							WHEN type_final LIKE 'MATERNITY CLOTHING' THEN 'MATERNITY CLOTHING'
							WHEN COALESCE(type_final,'NULL') LIKE 'NULL' AND revised_category_level_1 = 'FASHION' AND revised_category_level_2 = 'WOMEN' AND revised_category_level_3 = 'CLOTH' THEN 'MATERNITY CLOTHING'
							WHEN type_final LIKE 'MATERNITY LINGERIE' THEN 'MATERNITY LINGERIE'
							WHEN COALESCE(type_final,'NULL') LIKE 'NULL' AND revised_category_level_1 = 'FASHION' AND revised_category_level_2 = 'WOMEN' AND revised_category_level_3 = 'LINGERIE' THEN 'MATERNITY LINGERIE'
							WHEN type_final LIKE 'MATERNITY BREAST PADS' THEN 'MATERNITY BREAST PADS'
							WHEN type_final LIKE 'BREAST PADS' THEN 'MATERNITY BREAST PADS'
							WHEN type_final LIKE 'MATERNITY MILK PUMP MACHINE' THEN 'MATERNITY MILK PUMP MACHINE'
							WHEN type_final LIKE 'MILK PUMP MACHINE' THEN 'MATERNITY MILK PUMP MACHINE'
							WHEN type_final LIKE 'MILK STORAGE' THEN 'MATERNITY MILK STORAGE'
							WHEN type_final LIKE 'MATERNITY MILK STORAGE' THEN 'MATERNITY MILK STORAGE'
							WHEN type_final LIKE 'MATERNITY BELT' THEN 'MATERNITY BELT'
							WHEN type_final LIKE 'MATERNITY PILLOW' THEN 'MATERNITY PILLOW'
							WHEN type_final LIKE 'MATERNITY SUPPLEMENT' THEN 'MATERNITY SUPPLEMENT'
							WHEN type_final LIKE 'MILK' THEN 'MATERNITY SUPPLEMENT'
							WHEN type_final LIKE 'MATERNITY TREATMENT' THEN 'MATERNITY TREATMENT'
							WHEN COALESCE(type_final,'NULL') LIKE 'NULL' AND revised_category_level_1 = 'BEAUTY' THEN 'MATERNITY TREATMENT'
							WHEN COALESCE(type_final,'NULL') LIKE 'NULL' AND revised_category_level_1 = 'HBA' THEN 'MATERNITY TREATMENT'
							WHEN COALESCE(type_final,'NULL') LIKE 'NULL' AND revised_category_level_3 = 'BATHROOM ACCESSORIES' THEN 'MATERNITY TREATMENT'
							WHEN type_final LIKE 'MATERNITY' THEN 'OTHER MATERNITY'
							WHEN COALESCE(type_final,'NULL') LIKE 'NULL' AND revised_category_level_3 = 'MATERNITY' THEN 'OTHER MATERNITY'
							/* KID FASHION */
							WHEN type_final LIKE 'DAYWEAR' THEN 'KID DAYWEAR'
							WHEN COALESCE(type_final,'NULL') LIKE 'NULL' AND revised_category_level_3 = 'KID CLOTHING' THEN 'KID DAYWEAR'
							WHEN type_final LIKE 'NIGHTWEAR' THEN 'KID NIGHTWEAR'
							WHEN type_final LIKE 'UNDERWEAR' THEN 'KID UNDERWEAR'
							WHEN type_final LIKE 'SWIMWEAR' THEN 'KID SWIMWEAR'
							WHEN type_final LIKE 'FOOTWEAR' THEN 'KID SHOES'
							WHEN COALESCE(type_final,'NULL') LIKE 'NULL' AND revised_category_level_3 = 'KID SHOES' THEN 'KID SHOES'
							WHEN type_final LIKE 'ACCESSORIES' THEN 'KID FASHION ACCESSORIES'
							WHEN COALESCE(type_final,'NULL') LIKE 'NULL' AND revised_category_level_3 = 'KID FASHION ACCESSORIES' THEN 'KID FASHION ACCESSORIES'
							WHEN COALESCE(type_final,'NULL') LIKE 'NULL' AND revised_category_level_3 = 'SMART WATCH' THEN 'KID FASHION ACCESSORIES'
							WHEN COALESCE(type_final,'NULL') LIKE 'NULL' AND revised_category_level_3 = 'OTHER' AND product_name LIKE '%?????%' THEN 'KID FASHION ACCESSORIES'
							/* KID ACC */
							WHEN type_final LIKE 'FEEDING BOTTLE AND NIPPLE' THEN 'FEEDING BOTTLE AND NIPPLE'
							WHEN COALESCE(type_final,'NULL') LIKE 'NULL' AND revised_category_level_3 = 'FEEDING BOTTLE AND ACCESSORIES' THEN 'FEEDING BOTTLE AND NIPPLE'
							WHEN type_final LIKE 'FEEDING ACCESSORIES' THEN 'FEEDING ACCESSORIES'
							WHEN type_final LIKE 'BOTTLE WARMER' THEN 'BOTTLE WARMER'
							WHEN type_final LIKE 'DINING EQUIPMENT' THEN 'WEANING'
							WHEN type_final LIKE 'WEANING' THEN 'WEANING'
							WHEN type_final LIKE 'BATHING' THEN 'KID BATHROOM ACCESSORIES'
							WHEN COALESCE(type_final,'NULL') LIKE 'NULL' AND revised_category_level_3 = 'KID BATHROOM ACCESSORIES' THEN 'KID BATHROOM ACCESSORIES'
							WHEN type_final LIKE 'BATHROOM' AND revised_category_level_3 = 'KID BATHROOM ACCESSORIES' THEN 'KID BATHROOM ACCESSORIES'
							WHEN type_final LIKE 'FABRIC' THEN 'KID FABRIC'
							WHEN type_final LIKE 'PERSONAL CARE' THEN 'OTHER KID ACCESSORIES'
							WHEN type_final LIKE 'WASHING' THEN 'OTHER KID ACCESSORIES'
							WHEN type_final LIKE 'GIFT SET' THEN 'OTHER KID ACCESSORIES'
							WHEN type_final LIKE 'THERMOMETER' THEN 'OTHER KID ACCESSORIES'
							WHEN type_final LIKE 'KID' THEN 'OTHER KID ACCESSORIES'
							WHEN type_final LIKE 'OTHER' THEN 'OTHER KID ACCESSORIES'
							WHEN COALESCE(type_final,'NULL') LIKE 'NULL' AND revised_category_level_3 = 'OTHER KID ACCESSORIES' THEN 'OTHER KID ACCESSORIES'
							WHEN COALESCE(type_final,'NULL') LIKE 'NULL' AND revised_category_level_3 = 'OTHER' THEN 'OTHER KID ACCESSORIES'
							/* KID CAR SEAT AND TRAVEL GEAR */
							WHEN type_final LIKE 'STROLLER' THEN 'STROLLER'
							WHEN type_final LIKE 'BABY CARRIER' THEN 'BABY CARRIER'
							WHEN type_final LIKE 'CAR SEAT' THEN 'CAR SEAT'
							/* KID FURNITURE */
							WHEN type_final LIKE 'BEDDING' THEN 'KID BEDDING'
							WHEN type_final LIKE 'BEDROOM' THEN 'KID BEDDING'
							WHEN type_final LIKE 'BEDLINEN' THEN 'KID BEDDING'
							WHEN COALESCE(type_final,'NULL') LIKE 'NULL' AND revised_category_level_3 = 'KID BEDDING' THEN 'KID BEDDING'
							WHEN type_final LIKE 'NET' THEN 'OTHER KID FURNITURE'
							WHEN type_final LIKE 'SAFETY' THEN 'OTHER KID FURNITURE'
							WHEN type_final LIKE 'HOT WATER BAG' THEN 'OTHER KID FURNITURE'
							WHEN COALESCE(type_final,'NULL') LIKE 'NULL' AND revised_category_level_3 = 'LAUNDRY EQUIPMENT' THEN 'OTHER KID FURNITURE'
							WHEN COALESCE(type_final,'NULL') LIKE 'NULL' AND revised_category_level_3 = 'OTHER KID FURNITURE' THEN 'OTHER KID FURNITURE'
							/* TOYS */
							WHEN type_final LIKE 'BABY BOUNCER' THEN 'BABY BOUNCER'
							WHEN type_final LIKE 'BABY WALKER' THEN 'BABY WALKER'
							WHEN type_final LIKE 'TOY' THEN 'OTHER TOYS'
							WHEN type_final LIKE 'PLUSH & STUFFED TOY' THEN 'OTHER TOYS'
							WHEN type_final LIKE 'DRESS UP & PRETEND PLAY' THEN 'OTHER TOYS'
							WHEN COALESCE(type_final,'NULL') LIKE 'NULL' AND revised_category_level_3 = 'OTHER HOME ELECTRONICS' THEN 'OTHER TOYS'
							WHEN COALESCE(type_final,'NULL') LIKE 'NULL' AND revised_category_level_3 = 'GAME AND TOYS' THEN 'OTHER TOYS'
							WHEN COALESCE(type_final,'NULL') LIKE 'NULL' AND revised_category_level_3 = 'TOYS' THEN 'OTHER TOYS'
							ELSE type_final END)
WHERE mom_and_kid_product_flag = 1
AND (new_type_final IS NULL OR new_type_final = 'STATIONERY')
;



------------------------------------------------------------BY PRODUCT
-----DIAPERS
UPDATE vpm_data.pm_skutagging_kids_product_master
SET new_type_final = (CASE --NOT DIAPERS
							WHEN product_name LIKE '%�����˹��%' THEN 'KID FABRIC'
							WHEN product_name LIKE '%��ҾҴ%' THEN 'KID FABRIC'
							WHEN product_name LIKE '%��ҡѹ���͹%' THEN 'KID FABRIC'
							WHEN product_name LIKE '%��ҫѺ������%' THEN 'KID FABRIC'
							WHEN product_name LIKE '%��Ҷٵ��%' THEN 'KID FABRIC'
							WHEN product_name LIKE '%��ҹ��%' THEN 'KID FABRIC'
							WHEN product_name LIKE '%�������Թ%' THEN 'KID FABRIC'
							WHEN product_name LIKE '%����๡���ʧ��%' THEN 'KID FABRIC'
							WHEN product_name LIKE '%�������%'	 THEN 'KID FABRIC'
							WHEN product_name LIKE '%GIFT SET%' THEN 'OTHER KID ACCESSORIES'
							WHEN product_name LIKE '%THERMOMETER%' THEN 'OTHER KID ACCESSORIES'
							WHEN product_name LIKE '%FEVER PATCH%' THEN 'OTHER KID ACCESSORIES'
							WHEN product_name LIKE '%SILICONE NASAL APIRATOR%' THEN 'OTHER KID ACCESSORIES'
							WHEN product_name LIKE '%���ٴ����١%' THEN 'OTHER KID ACCESSORIES'
							WHEN product_name LIKE '%������ͧ�ѹ��蹼��%' THEN 'OTHER KID ACCESSORIES'
							WHEN product_name LIKE 'DIAPER CREAM' THEN 'OTHER KID ACCESSORIES'
							WHEN product_name LIKE '%��ҧ�Ǵ��%' THEN 'FEEDING ACCESSORIES'
							WHEN product_name LIKE '%��лء%������%' THEN 'FEEDING ACCESSORIES'
							WHEN product_name LIKE '%�Ǵ��%' THEN 'FEEDING BOTTLE AND NIPPLE'
							WHEN product_name LIKE '%�ء��%' THEN 'FEEDING BOTTLE AND NIPPLE'
							WHEN product_name LIKE '%�ش��͹����%' THEN 'WEANING'
							WHEN product_name LIKE 'BA DOLL DIAPERS' THEN 'OTHER TOYS'
							WHEN product_name LIKE 'BA DIAPERS REFILL' THEN 'OTHER TOYS'
							WHEN product_name LIKE '�ҧ�Ѵ' THEN 'OTHER TOYS'
							WHEN product_name like '%T-SHIRT%' THEN 'KID DAYWEAR'
							WHEN product_name LIKE 'BABY DEE%SHIRT%' THEN 'KID DAYWEAR'
							WHEN product_name LIKE 'BABY DEE%WEAR%' THEN 'KID DAYWEAR'
							WHEN product_name LIKE 'BABY DEE%PANTS%' THEN 'KID DAYWEAR'
							WHEN product_name LIKE 'BABY DEE%BRIEF%' THEN 'KID DAYWEAR'
							WHEN product_name LIKE 'BABY DEE%BABY SET%' THEN 'KID DAYWEAR'
							WHEN product_name LIKE '%��͡��%' THEN 'KID UNDERWEAR'
							WHEN product_name LIKE '%����͡����%' THEN 'KID UNDERWEAR'
							WHEN product_name LIKE 'BABY DEE%HAT%GLOVE%' THEN 'KID FASHION ACCESSORIES'
							WHEN product_name LIKE 'BABY DEE%BIBS%' THEN 'KID FASHION ACCESSORIES'
							WHEN product_name LIKE '%Baby Bedsheet%' THEN 'KID BEDDING'
							WHEN product_name LIKE '%���ͧ�ѹ���͹%' THEN 'KID BEDDING'
							WHEN product_name LIKE '%���ҧ�ٹ͹%' THEN 'KID BEDDING'
							WHEN product_name LIKE '%�蹫Ѻ%' THEN 'KID BEDDING'
							WHEN product_name LIKE '%DIAPER BAG%' THEN 'MATERNITY BAG'
							--DIAPER
							WHEN product_name LIKE 'NAPPY෻ ���CIRCUS 0-5 YR' THEN 'OTHER DIAPERS'
							WHEN INFANT_0_3M_KEYWORD IN ('DIAPER SIZE NB','DIAPERS SIZE NB') THEN 'DIAPERS SIZE NB'		-- UPDATE TO DIAPERS SIZE NB
							WHEN product_name LIKE '%�������%���á�Դ%' THEN 'DIAPERS SIZE NB'
							WHEN product_name LIKE '%TAPE NB%' THEN 'DIAPERS SIZE NB'
							WHEN INFANT_7_9M_KEYWORD IN ('DIAPER SIZE M','DIAPERS SIZE M') THEN 'DIAPERS SIZE M'		-- UPDATE TO DIAPERS SIZE M
							WHEN product_name LIKE '%PANT M%' THEN 'DIAPERS SIZE M'
							WHEN product_name LIKE '% M PANT%' THEN 'DIAPERS SIZE M'
							--WHEN TODDLER_KEYWORD LIKE 'DIAPERS SIZE L' THEN 'DIAPERS SIZE L'
							WHEN TODDLER_KEYWORD IN ('DIAPER SIZE XL','DIAPERS SIZE XL') THEN 'DIAPERS SIZE XL'
							WHEN product_name LIKE '%XL%' THEN 'DIAPERS SIZE XL'
							WHEN TODDLER_KEYWORD IN ('DIAPER SIZE L','DIAPERS SIZE L') THEN 'DIAPERS SIZE L'
							WHEN product_name LIKE '%L[0-9]%' THEN 'DIAPERS SIZE L'
							WHEN product_name LIKE '% L %'  THEN 'DIAPERS SIZE L'
							WHEN product_name LIKE '%L [0-9]%'  THEN 'DIAPERS SIZE L'
							WHEN product_name LIKE '%L X[0-9]%' THEN 'DIAPERS SIZE L'
							WHEN product_name LIKE '%-L' THEN 'DIAPERS SIZE L'
							WHEN product_name LIKE '%18M%' THEN 'DIAPERS SIZE L'
							WHEN product_name LIKE '% L' THEN 'DIAPERS SIZE L'
							WHEN product_name LIKE '%12M%' THEN 'DIAPERS SIZE L'
							WHEN product_name LIKE '% L(%' THEN 'DIAPERS SIZE L'
							WHEN product_name LIKE '%DiaperL %' THEN 'DIAPERS SIZE L'
							WHEN product_name LIKE '%PANT L%' THEN 'DIAPERS SIZE L'
							WHEN product_name LIKE '% L PANT%' THEN 'DIAPERS SIZE L'
							WHEN product_name LIKE '%TAPE L%' THEN 'DIAPERS SIZE L'
							WHEN INFANT_4_6M_KEYWORD IN ('DIAPER SIZE S','DIAPERS SIZE S') AND product_name LIKE '%XL%' THEN 'DIAPERS SIZE XL'
							WHEN INFANT_4_6M_KEYWORD IN ('DIAPER SIZE S','DIAPERS SIZE S') THEN 'DIAPERS SIZE S'		-- UPDATE TO DIAPERS SIZE S
							WHEN product_name LIKE '%�ѡ���%S68%' THEN 'DIAPERS SIZE S'
							ELSE new_type_final END)
WHERE mom_and_kid_product_flag = 1
AND (new_type_final = 'OTHER DIAPERS'
	OR (new_type_final = 'KID DAYWEAR' AND INFANT_0_3M_KEYWORD IN ('DIAPER SIZE NB','DIAPERS SIZE NB'))
	OR (new_type_final IN ('KID DAYWEAR','KID UNDERWEAR') AND INFANT_4_6M_KEYWORD IN ('DIAPER SIZE S','DIAPERS SIZE S'))
	OR (new_type_final IN ('KID DAYWEAR','KID UNDERWEAR') AND INFANT_7_9M_KEYWORD IN ('DIAPER SIZE M','DIAPERS SIZE M'))
	OR (new_type_final = 'KID DAYWEAR' AND TODDLER_KEYWORD IN ('DIAPER SIZE L','DIAPERS SIZE L','DIAPER SIZE XL','DIAPERS SIZE XL')))
;

-----KID BOOKS & STATIONERY
--KID BOOKS
UPDATE vpm_data.pm_skutagging_kids_product_master
SET new_type_final = (CASE --PREGNANCY BOOK
							WHEN product_name LIKE '%���%' AND product_name NOT LIKE '%�١%' THEN 'PREGNANCY BOOK'
							--PARENT GUIDE BOOK
							WHEN product_name LIKE '%����§�١������%' THEN 'PARENT GUIDE BOOK'
							--COOK BOOK
							WHEN product_name LIKE '%��������ҵ�����Ϸ�á%' THEN 'COOK BOOK'
							WHEN product_name LIKE '%COOKING FOR BABY%' THEN 'COOK BOOK'
							--ACTIVITY BOOK
							WHEN product_name LIKE '%DOODLE%' THEN 'ACTIVITY BOOK'
							WHEN product_name LIKE '%��ش�Ҿ%' THEN 'ACTIVITY BOOK'
							WHEN product_name LIKE '%ʵԡ����%' THEN 'ACTIVITY BOOK'
							WHEN product_name LIKE '%COLORING BOOK%' THEN 'ACTIVITY BOOK'
							WHEN product_name LIKE '%�к����%' AND product_name NOT LIKE '%ABC%'
															AND product_name NOT LIKE '%A-Z%'
															AND product_name NOT LIKE '%�Ţ��Ե%'
															AND product_name NOT LIKE '%����Ţ%'
															AND product_name NOT LIKE '%123%'
															AND product_name NOT LIKE '%�Ѵ��¹%'
															AND product_name NOT LIKE '%�Ţ�%' THEN 'ACTIVITY BOOK'
							WHEN product_name LIKE '%ú�%' AND product_name NOT LIKE '%ABC%'
															AND product_name NOT LIKE '%A-Z%'
															AND product_name NOT LIKE '%1-10%'
															AND product_name NOT LIKE '%�ʹ�ѡú�з�ҹ����%'
															AND product_name NOT LIKE '%���%'
															AND product_name NOT LIKE '%�.��%'
															AND product_name NOT LIKE '%�Ţ�%' THEN 'ACTIVITY BOOK'
							WHEN product_name LIKE '%�ҡ��鹵�ͨش%' THEN 'ACTIVITY BOOK'
							WHEN product_name LIKE '%PRACTICE%DRAW%' THEN 'ACTIVITY BOOK'
							WHEN product_name LIKE '%PLAY AND LEARN%' THEN 'ACTIVITY BOOK'
							WHEN product_name IN ('˹ٹ������������� �á�Դ-B','˹ٹ����á�Դ ��� ���¹ ���B') THEN 'ACTIVITY BOOK'
							--TALE BOOK
							WHEN product_name LIKE '%�Էҹ%' AND product_name NOT LIKE '%��ǵ��%' and product_name NOT LIKE '%ʵ������%' THEN 'TALE BOOK'
							WHEN product_name LIKE '%��ʻ%' THEN 'TALE BOOK'
							WHEN product_name LIKE '%STORYBOOK%' THEN 'TALE BOOK'
							WHEN product_name LIKE '%STORY BOOK%' THEN 'TALE BOOK'
							WHEN product_name LIKE 'IN MY LITTLE%BED' THEN 'TALE BOOK'
							WHEN product_name IN ('WHERE IS BABY S PUMPKIN?�','FIVE LITTLE PUMPKINS(HARPER GR','BISCUIT VISITS THE PUMPKIN PAT'
									,'DUCK & GOOSE,FIND A PUMPKIN') THEN 'TALE BOOK'
							--PRESCHOOL THAI BOOK
							WHEN product_name LIKE '%�����¾�鹰ҹ ͹غ��%' THEN 'PRESCHOOL THAI BOOK'
							WHEN product_name LIKE '%� ��%' THEN 'PRESCHOOL THAI BOOK'
							WHEN product_name LIKE '%�.��%' THEN 'PRESCHOOL THAI BOOK'
							WHEN product_name LIKE '%� � �%' THEN 'PRESCHOOL THAI BOOK'
							WHEN product_name LIKE '%���%' THEN 'PRESCHOOL THAI BOOK'
							WHEN product_name LIKE '%��ó�ء��%' THEN 'PRESCHOOL THAI BOOK'
							WHEN product_name LIKE '%���%' THEN 'PRESCHOOL THAI BOOK'
							WHEN product_name LIKE '%PRACTICE%THAI%' THEN 'PRESCHOOL THAI BOOK'
							WHEN product_name LIKE '%PRACTICE%KORKAI%' THEN 'PRESCHOOL THAI BOOK'
							WHEN product_name LIKE '%PRACTICE%KOR_KAI%' THEN 'PRESCHOOL THAI BOOK'
							WHEN product_name LIKE '%˹��ѡ������%' THEN 'PRESCHOOL THAI BOOK'
							WHEN product_name LIKE '%�ѡ��������%' THEN 'PRESCHOOL THAI BOOK'
							WHEN product_name LIKE '%�-�%' THEN 'PRESCHOOL THAI BOOK'
							--PRESCHOOL ENGLISH BOOK
							WHEN product_name LIKE '%A B C%' THEN 'PRESCHOOL ENGLISH BOOK'
							WHEN product_name LIKE '%ABC%' THEN 'PRESCHOOL ENGLISH BOOK'
							WHEN product_name LIKE '%A-Z%' THEN 'PRESCHOOL ENGLISH BOOK'
							WHEN product_name LIKE '%A - Z%' THEN 'PRESCHOOL ENGLISH BOOK'
							WHEN product_name LIKE '%˹��ѡ�����ѧ���%' THEN 'PRESCHOOL ENGLISH BOOK'
							WHEN product_name LIKE '%�֡��ҹ�͡���§�.%' THEN 'PRESCHOOL ENGLISH BOOK'
							WHEN product_name LIKE '%�ѡ�������ѧ����ѡ�ùٹ%' THEN 'PRESCHOOL ENGLISH BOOK'
							WHEN product_name LIKE '%�ѡ�������ѧ���%' THEN 'PRESCHOOL ENGLISH BOOK'
							WHEN new_type_final = 'PRESCHOOL THAI BOOK' AND product_name LIKE '%�ѧ���%' THEN 'PRESCHOOL ENGLISH BOOK'
							--PRESCHOOL MATH BOOK
							WHEN product_name LIKE '%�Ѵ�Ţ%' THEN 'PRESCHOOL MATH BOOK'
							WHEN product_name LIKE '%��¹%�Ţ%' THEN 'PRESCHOOL MATH BOOK'
							WHEN product_name LIKE '%PRACTICE%NUMBER%' THEN 'PRESCHOOL MATH BOOK'
							WHEN product_name LIKE '%1-100%' THEN 'PRESCHOOL MATH BOOK'
							WHEN product_name LIKE '%����Ţ 1-20%' THEN 'PRESCHOOL MATH BOOK'
							WHEN product_name LIKE '%�Ţ�ٹ%' THEN 'PRESCHOOL MATH BOOK'
							WHEN product_name like '%�ٵäٳ%' THEN 'PRESCHOOL MATH BOOK'
							WHEN product_name LIKE '%�ѡ�õ���Ţ�ٹ%' THEN 'PRESCHOOL MATH BOOK'
							WHEN product_name LIKE '%�á��������¹��� 123%' THEN 'PRESCHOOL MATH BOOK'
							WHEN product_name LIKE '�ѡ����¹�� 0-9%' THEN 'PRESCHOOL MATH BOOK'
							ELSE new_type_final END)
WHERE mom_and_kid_product_flag = 1
AND new_type_final IN ('PREGNANCY BOOK','PARENT GUIDE BOOK','SCHOOL BOOK','ACTIVITY BOOK','TALE BOOK','PRESCHOOL MATH BOOK','PRESCHOOL ENGLISH BOOK','PRESCHOOL THAI BOOK','OTHER KID BOOK')
;

--KID STATIONERY
UPDATE vpm_data.pm_skutagging_kids_product_master
SET new_type_final = 'ACTIVITY BOOK'
WHERE new_type_final = 'KID STATIONERY'
AND product_name LIKE '%COLORING BOOK%'
;

-----KID FOOD
--MILK POWDER
UPDATE vpm_data.pm_skutagging_kids_product_master
SET new_type_final = (CASE --MATERNITY
							WHEN product_name LIKE '%ENFAMAMA%' THEN 'MATERNITY SUPPLEMENT'
							--MILK POWDER PRODUCT NAME
							WHEN product_name LIKE '%180%' AND product_name NOT LIKE '%1800%' THEN 'KID UHT MILK'
							WHEN product_name LIKE '%110 X4%' THEN 'KID UHT MILK'
							WHEN product_name LIKE '%200ML%' THEN 'KID UHT MILK'
							WHEN product_name LIKE '%200��%' THEN 'KID UHT MILK'
							WHEN product_name IN ('Enfalac A Plus 350g','Enfalac 800g','Hi Q Super Gold C Synbio 1800g','Hi Q Super Gold C Synbio 3000g'
									,'Hi Q Super Gold H.A 600g') THEN 'OTHER MILK POWDER'
							WHEN product_name LIKE 'PEDIASURE COMPLETE%' THEN 'OTHER MILK POWDER'
							WHEN product_name LIKE '%3PLUS%' THEN 'OTHER MILK POWDER'
							WHEN product_name LIKE '%3 PLUS%' THEN 'OTHER MILK POWDER'
							WHEN product_name LIKE '%4 %' THEN 'OTHER MILK POWDER'
							WHEN product_name LIKE 'Enfalac A Plus1 %' THEN 'MILK POWDER FORMULA 1'
							WHEN product_name LIKE 'Enfalac 1 Smart Plus%' THEN 'MILK POWDER FORMULA 1'
							WHEN product_name LIKE '%BEAR BRAND 3 %' THEN 'MILK POWDER FORMULA 3'
							--KEYWORD
							WHEN INFANT_0_3M_KEYWORD = 'MILK POWDER FORMULA 1' THEN 'MILK POWDER FORMULA 1'
							WHEN INFANT_7_9M_KEYWORD = 'MILK POWDER FORMULA 2' THEN 'MILK POWDER FORMULA 2'
							WHEN TODDLER_KEYWORD = 'MILK POWDER FORMULA 3' THEN 'MILK POWDER FORMULA 3'
							WHEN TODDLER_KEYWORD = 'STEP3 MILK POWDER' THEN 'MILK POWDER FORMULA 3'
							WHEN TODDLER_KEYWORD = 'STEP1 PLUS MILK POWDER' THEN 'MILK POWDER FORMULA 3'
							ELSE new_type_final END)
WHERE mom_and_kid_product_flag = 1
AND new_type_final IN ('MILK POWDER FORMULA 1','MILK POWDER FORMULA 2','MILK POWDER FORMULA 3','OTHER MILK POWDER','KID UHT MILK')
;

--KID NOURISHMENT
UPDATE vpm_data.pm_skutagging_kids_product_master
SET new_type_final = (CASE --STRAINED FOOD AND PUREE
						   WHEN product_name LIKE '%HEIN ORGANICBABYVEGETABLE%' THEN 'STRAINED FOOD AND PUREE'
						   WHEN product_name LIKE '%HEINZ BABY CORN CHICKEN%' THEN 'STRAINED FOOD AND PUREE'
						   WHEN product_name LIKE '%HEINZ BABY FOOD APPLE&MANGO%' THEN 'STRAINED FOOD AND PUREE'
						   WHEN product_name LIKE '%HEINZ BABY FRUITY APPLE%' THEN 'STRAINED FOOD AND PUREE'
						   WHEN product_name LIKE '%HEINZ BABY PEAR&BANANA%' THEN 'STRAINED FOOD AND PUREE'
						   WHEN product_name LIKE '%APPLE%MONKEY%GUAVA%' THEN 'STRAINED FOOD AND PUREE'
						   WHEN product_name LIKE '%BABY%NATURE%' THEN 'STRAINED FOOD AND PUREE'
						   WHEN product_name LIKE '%BABY%NATURA%' THEN 'STRAINED FOOD AND PUREE'
						   WHEN product_name LIKE '%BE%DELIGHT%' THEN 'STRAINED FOOD AND PUREE'  
						   WHEN product_name LIKE '%CERELAC%BABY%' THEN 'STRAINED FOOD AND PUREE'
						   WHEN product_name LIKE '%�����Ť%' THEN 'STRAINED FOOD AND PUREE'
						   WHEN product_name LIKE '%HAPPY%BEAR%' THEN 'STRAINED FOOD AND PUREE'
						   WHEN product_name LIKE '%HEINZ%' THEN 'STRAINED FOOD AND PUREE'
						   WHEN product_name LIKE '%HOORAY%PUREE%' THEN 'STRAINED FOOD AND PUREE'
						   WHEN product_name LIKE '%PEACHY%' AND product_name NOT LIKE '%COOKIE%' AND product_name NOT LIKE '%PUFF%' AND product_name NOT LIKE '%SMOOTHIE%' THEN 'STRAINED FOOD AND PUREE'
						   WHEN product_name LIKE '%�ժ���%' THEN 'STRAINED FOOD AND PUREE'
						   WHEN product_name LIKE '%PORRID%' THEN 'STRAINED FOOD AND PUREE'
						   --FINGER FOOD
						   WHEN product_name LIKE '%APPLE%MONKEY%' THEN 'FINGER FOOD'
						   WHEN product_name LIKE '%DOZO%BABY%' THEN 'FINGER FOOD'
						   WHEN product_name LIKE '%���຺��%' THEN 'FINGER FOOD'
						   WHEN product_name LIKE '%HAPPY%BITE%' THEN 'FINGER FOOD'
						   WHEN product_name LIKE '%HEINZ%BISCOTTI%' THEN 'FINGER FOOD'
						   WHEN product_name LIKE '%KHAO%KHUN%MHOR%30G%' THEN 'FINGER FOOD'
						   WHEN product_name LIKE '%LITTLE%FREDDIE%' THEN 'FINGER FOOD'
						   WHEN product_name LIKE '%RICELICIOUS%' THEN 'FINGER FOOD'
						   WHEN product_name LIKE '%�����ѧ���´%' THEN 'FINGER FOOD'
						   WHEN cleaned_brandname LIKE '%ORGANEH%' THEN 'FINGER FOOD'
						   --TABLE FOOD
						   WHEN product_name LIKE '%APPLE%MONKEY%CAKE%' THEN 'TABLE FOOD'
						   WHEN product_name LIKE '%APPLE%MONKEY%COOKIES%' THEN 'TABLE FOOD'
						   WHEN product_name LIKE '%APPLE%MONKEY%GRAIN%' THEN 'TABLE FOOD'
						   WHEN product_name LIKE '%APPLE%MONKEY%GRANOLA%' THEN 'TABLE FOOD'
						   WHEN product_name LIKE '%APPLE%MONKEY%PUFF%' THEN 'TABLE FOOD'
						   WHEN product_name LIKE '%APPLE%MONKEY%GRANOLA%' THEN 'TABLE FOOD'
						   WHEN product_name LIKE '%APPLE%MONKEY%SPAGHETTI' THEN 'TABLE FOOD'
						   WHEN product_name LIKE '%APPLE%MONKE%' THEN 'TABLE FOOD'
						   WHEN product_name LIKE '%BABY%NATURE%ORGANIC%' THEN 'TABLE FOOD'
						   WHEN product_name LIKE '%GREENDAY%' THEN 'TABLE FOOD'
						   WHEN product_name LIKE '%NESTLE%CERELAC%' THEN 'TABLE FOOD'
						   WHEN product_name LIKE '%PEACHY%COOKIE%' THEN 'TABLE FOOD'
						   WHEN product_name LIKE '%PEACHY%PUFF%' THEN 'TABLE FOOD'
						   WHEN product_name LIKE '%PEACHY%SMOOTHIE%' THEN 'TABLE FOOD'
						   WHEN product_name LIKE '%SWEET%PEA%DRIED PRUNE PUREE%' THEN 'TABLE FOOD'
						   WHEN product_name LIKE '%UNCLE%MARK%' THEN 'TABLE FOOD'
						   WHEN product_name LIKE '%WEL%B%BABY%' THEN 'TABLE FOOD'
						   WHEN product_name LIKE '%FOODIE%' THEN 'TABLE FOOD'
						   WHEN product_name LIKE '%HAPPY%MUNCHY%' THEN 'TABLE FOOD'
						   WHEN product_name LIKE '%HOORAY%' THEN 'TABLE FOOD'
						   WHEN product_name LIKE '%KHAO%KHUN%MHOR%' THEN 'TABLE FOOD'
						   WHEN product_name LIKE '%KIDDI%' THEN 'TABLE FOOD'
						   WHEN product_name LIKE '%LITTLE%MUNCHY%' THEN 'TABLE FOOD'
						   WHEN product_name LIKE '%MAMA%COOKS%' THEN 'TABLE FOOD'
						   WHEN product_name LIKE '%NAMCHOW%HAPPY%' THEN 'TABLE FOOD'
						   WHEN product_name LIKE '%PICNIC%BABY%' THEN 'TABLE FOOD'
						   WHEN product_name LIKE '%PicnicSuperVeggies%' THEN 'TABLE FOOD'
						   WHEN product_name LIKE '%�Ԥ�Ԥ຺��%' THEN 'TABLE FOOD'
						   WHEN product_name LIKE '%SWEET%PEA%JUICE%' THEN 'TABLE FOOD'
						   WHEN product_name LIKE '%XONGDUR%BABY%' THEN 'TABLE FOOD'
						   WHEN product_name LIKE '%�Ѻ�輧%' THEN 'TABLE FOOD'
						   --MATERNITY SUPPLEMENT
						   WHEN product_name LIKE '%��ǻ��%' THEN 'MATERNITY SUPPLEMENT'
						   ELSE new_type_final END)
WHERE mom_and_kid_product_flag = 1
AND new_type_final = 'TABLE FOOD'
;

-----MATERNITY
--product name to maternity lingerie
UPDATE vpm_data.pm_skutagging_kids_product_master
SET new_type_final = 'MATERNITY LINGERIE'
WHERE product_name IN ('�ҧࡧ�����١��͹','�ѧ�ç����١��͹','�ѧ�ç����١���','����ͺѧ�ç����١���','�����¡�ç����١��͹','ા�鹷�����١��͹')
;

--product name to maternity treatment
UPDATE vpm_data.pm_skutagging_kids_product_master
SET new_type_final = 'MATERNITY TREATMENT'
WHERE product_name LIKE '%STRETCH%MARK%'
OR product_name LIKE '%������ЪѺ���%'
;

--MATERNITY BREAST PADS
UPDATE vpm_data.pm_skutagging_kids_product_master
SET new_type_final = (CASE WHEN product_name LIKE '�ا�Ի��ͤ�๡���ʧ��' THEN 'MATERNITY MILK STORAGE'
							WHEN product_name LIKE '�ا��èع�ӹ�%' THEN 'MATERNITY MILK STORAGE'
							WHEN product_name LIKE '�ا�Ժ�纹�ӹ�%' THEN 'MATERNITY MILK STORAGE'
							WHEN product_name LIKE 'BREAST MILK BAGS%' THEN 'MATERNITY MILK STORAGE'
							WHEN product_name LIKE '%���«���⤹�纹�ӹ�%' THEN 'MATERNITY MILK PUMP MACHINE'
							WHEN product_name LIKE 'VALVE COMPLETE INC SPARE MEMBR' THEN 'MATERNITY MILK PUMP MACHINE'
							WHEN product_name LIKE '��������¤�ͺ��� 30' THEN 'MATERNITY MILK PUMP MACHINE'
							WHEN product_name LIKE 'SILICONE TUBING WITH POLYGON' THEN 'MATERNITY MILK PUMP MACHINE'
							WHEN product_name LIKE 'SET BRUSTA M2PP' THEN 'MATERNITY MILK PUMP MACHINE'
							WHEN product_name LIKE '%MIRACLE 2PLUSPAUSE SET SPECIA%' THEN 'MATERNITY MILK PUMP MACHINE'
							WHEN product_name LIKE '%COOLING FREEZE PACK%' THEN 'OTHER MATERNITY'
							WHEN product_name LIKE '%NIPPLE PROTECTORS%' THEN 'OTHER MATERNITY'
							WHEN product_name LIKE '%NIPPLE SHIELD%' THEN 'OTHER MATERNITY'
							WHEN product_name LIKE '%����⤹��ͧ�ѹ��ǹ�%' THEN 'OTHER MATERNITY'
							WHEN product_name LIKE '%BREAST%SHIELD%' THEN 'OTHER MATERNITY'
							WHEN product_name LIKE '%BREAST%SHEILD%' THEN 'OTHER MATERNITY'
							WHEN product_name LIKE '%NIPPLE%CORRECTOR%' THEN 'OTHER MATERNITY'
							WHEN product_name LIKE '%�ا��觢Ǵ��%' THEN 'OTHER MATERNITY'
							WHEN product_name LIKE '%�Ҥ�ͺ��ǹ��ʹ�����%' THEN 'OTHER MATERNITY'
							WHEN product_name LIKE '%CONNECTOR%' THEN 'OTHER MATERNITY'
							WHEN product_name LIKE '%BREAST%SHELL%' THEN 'OTHER MATERNITY'
							WHEN product_name LIKE '�Ǵ�纹�ӹ���%' THEN 'FEEDING ACCESSORIES'
							WHEN product_name LIKE '�ا��觢Ǵ��' THEN 'FEEDING ACCESSORIES'
							WHEN product_name LIKE '%BOTTLE INSULATOR%' THEN 'FEEDING ACCESSORIES'
							WHEN product_name LIKE '%WIDENECK REPLACEMENT KIT%' THEN 'FEEDING ACCESSORIES'
							WHEN product_name LIKE '%BABY TABLEWARE%' THEN 'WEANING'
							WHEN product_name LIKE 'STEP UP COOKING SET' THEN 'WEANING'
							WHEN product_name LIKE '%��ا����%' THEN 'WEANING'
							WHEN product_name LIKE 'TODDLER GIFT SET' THEN 'OTHER KID ACCESSORIES'
							WHEN product_name LIKE '����ͧ����ا俿��%' THEN 'OTHER KID ACCESSORIES'
							WHEN product_name LIKE '����ͧ��Ե������ͦ������' THEN 'OTHER KID ACCESSORIES'
							WHEN product_name LIKE '%UVC LED MINI BOX%' THEN 'OTHER KID ACCESSORIES'
							WHEN product_name LIKE 'PREMIUM ACCESSORY KIT' THEN 'OTHER KID ACCESSORIES'
							ELSE new_type_final END)
WHERE mom_and_kid_product_flag = 1
AND new_type_final = 'MATERNITY BREAST PADS'
;

--OTHER MATERNITY
UPDATE vpm_data.pm_skutagging_kids_product_master
SET new_type_final = (CASE WHEN product_name LIKE 'HIP FIH BLACK' THEN 'MATERNITY BELT'
							WHEN product_name LIKE '%BOTTLE COOLER%' THEN 'FEEDING ACCESSORIES'
							WHEN product_name LIKE '%�Ǵ�纹�ӹ�%' THEN 'FEEDING ACCESSORIES'
							WHEN product_name LIKE '%����ͧ��觢Ǵ��%' THEN 'BOTTLE WARMER'
							WHEN product_name LIKE '%���͹��ͺ���%' THEN 'BOTTLE WARMER'
							WHEN product_name LIKE '%����ͧ��触�����;����ͺ���%' THEN 'BOTTLE WARMER'
							WHEN product_name LIKE '%����ͧ��蹵������%' THEN 'BOTTLE WARMER'
							WHEN product_name LIKE '%MICROWAVE STERILIZER%' THEN 'BOTTLE WARMER'
							WHEN product_name LIKE '%����ͧ���&��������%' THEN 'WEANING'
							WHEN product_name LIKE '%����ͧ��觻��%' THEN 'WEANING'
							WHEN product_name LIKE '%������%' THEN 'MATERNITY MILK PUMP MACHINE'
							WHEN product_name LIKE '%������%' THEN 'MATERNITY MILK PUMP MACHINE'
							WHEN product_name LIKE '%����BRUSTA%' THEN 'MATERNITY MILK PUMP MACHINE'
							WHEN product_name LIKE '%��ǻ���%' THEN 'MATERNITY MILK PUMP MACHINE'
							WHEN product_name LIKE '%�ا�纹�ӹ�%' THEN 'MATERNITY MILK STORAGE'
							WHEN product_name LIKE '%��͹�ͧ����%' THEN 'MATERNITY PILLOW'
							WHEN product_name LIKE '%��͹�ʹ�س���%' THEN 'MATERNITY PILLOW'
							WHEN product_name LIKE '%�蹫Ѻ��ӹ�%' THEN 'MATERNITY BREAST PADS'
							WHEN product_name LIKE '%��Ҥ���%' THEN 'MATERNITY CLOTHING'
							WHEN product_name LIKE '�ҧࡧ M SUPPORTED SHORTS' THEN 'MATERNITY CLOTHING'
							ELSE new_type_final END)
WHERE mom_and_kid_product_flag = 1
AND new_type_final LIKE 'OTHER MATERNITY'
;

-----KID FASHION
--KID DAYWEAR 1
UPDATE vpm_data.pm_skutagging_kids_product_master
SET new_type_final = (CASE WHEN product_name LIKE 'BAMBOO BABY WATERPROOF MAT%' THEN 'KID BEDDING'
							WHEN product_name LIKE '�����%' THEN 'OTHER KID ACCESSORIES'
							ELSE new_type_final END)
WHERE mom_and_kid_product_flag = 1
AND new_type_final LIKE 'KID DAYWEAR'
;

--KID DAYWEAR 2
UPDATE vpm_data.pm_skutagging_kids_product_master
SET new_type_final = (CASE WHEN product_name LIKE '�ش��%' THEN 'KID DAYWEAR'
							WHEN product_name LIKE '%��ǻ��%' THEN 'MATERNITY SUPPLEMENT'
							WHEN product_name LIKE '%��͹��Фͧ��ͧ%' THEN 'MATERNITY PILLOW'
							WHEN product_name LIKE '%����ͪ���%' THEN 'MATERNITY LINGERIE'
							WHEN product_name LIKE '%�����%' THEN 'MATERNITY CLOTHING'
							WHEN product_name LIKE '%���%' THEN 'MATERNITY CLOTHING'
							WHEN product_name LIKE '%DRESS%' THEN 'MATERNITY CLOTHING'
							WHEN product_name LIKE '%�ҧࡧ%' THEN 'MATERNITY CLOTHING'
							WHEN product_name LIKE '%�ش�͹%' THEN 'MATERNITY CLOTHING'
							WHEN product_name LIKE '%�����ٷ%' THEN 'MATERNITY CLOTHING'
							WHEN product_name LIKE '%����ç%' THEN 'MATERNITY CLOTHING'
							WHEN UNKNOWN_PREG_SUBSTAGE = 'YES'THEN 'MATERNITY CLOTHING'
							ELSE new_type_final END)
WHERE mom_and_kid_product_flag = 1
AND new_type_final LIKE 'KID DAYWEAR'
AND ("STAGE_M-6_to_M-3" = 'YES'
	OR "STAGE_M-3_to_M-0" = 'YES'
	OR UNKNOWN_PREG_SUBSTAGE = 'YES')
;

--KID UNDERWEAR
UPDATE vpm_data.pm_skutagging_kids_product_master
SET new_type_final = 'MATERNITY LINGERIE'
WHERE mom_and_kid_product_flag = 1
AND new_type_final LIKE 'KID UNDERWEAR'
AND UNKNOWN_PREG_SUBSTAGE = 'YES'
;

--KID FASHION ACCESSORIES
UPDATE vpm_data.pm_skutagging_kids_product_master
SET new_type_final = (CASE WHEN product_name LIKE '%STRETCH MARK%' THEN 'MATERNITY TREATMENT'
							WHEN product_name LIKE '%MATERNITY BRIEF%' THEN 'MATERNITY LINGERIE'
							WHEN product_name LIKE '%�����%' THEN 'KID DAYWEAR'
							WHEN product_name LIKE '%�ҧࡧ%' THEN 'KID DAYWEAR'
							WHEN product_name LIKE '%������%' THEN 'KID DAYWEAR'
							WHEN product_name LIKE '%������%' THEN 'KID BEDDING'
							ELSE new_type_final END)
WHERE mom_and_kid_product_flag = 1
AND new_type_final LIKE 'KID FASHION ACCESSORIES'
;

-----KID ACCESSORIES
--FEEDING ACCESSORIES
UPDATE vpm_data.pm_skutagging_kids_product_master
SET new_type_final = (CASE WHEN product_name LIKE '%�蹫Ѻ��ӹ�%' THEN 'MATERNITY BREAST PADS'
							WHEN product_name LIKE '%������%' THEN 'MATERNITY MILK PUMP MACHINE'
							WHEN product_name LIKE '%������ӹ�%' THEN 'MATERNITY MILK PUMP MACHINE'
							WHEN product_name LIKE '%Pump Adapter%' THEN 'MATERNITY MILK PUMP MACHINE'
							WHEN product_name LIKE '%����⤹��ͧ�ѹ��ǹ�%' THEN 'OTHER MATERNITY'
							WHEN product_name LIKE '%�Ҥ�ͺ��ǹ�%' THEN 'OTHER MATERNITY'
							WHEN product_name LIKE '%�蹻���ͧ��ǹ�%' THEN 'OTHER MATERNITY' 
							WHEN product_name LIKE '%�ا��ӹ�%' THEN 'MATERNITY MILK STORAGE'
							WHEN product_name LIKE '%�اʵ�͡�����%' THEN 'MATERNITY MILK STORAGE'
							WHEN product_name LIKE '%STEAM STERILIZER DRY DRYCLEAN%' THEN 'BOTTLE WARMER'
							WHEN product_name LIKE '%����ͧ��觢Ǵ��%' THEN 'BOTTLE WARMER'
							WHEN product_name LIKE '%�ػ�ó�ͧ������͹%' THEN 'OTHER KID ACCESSORIES'
							WHEN product_name LIKE '%��Ի˹պ��Ҥ���ö��%' THEN 'WEANING'
							WHEN product_name LIKE '%����ͧ����������%' THEN 'WEANING'
							WHEN product_name LIKE '%����ͧ��蹹��%' THEN 'WEANING'
							WHEN product_name LIKE '%���ͧ�Ѻ%' THEN 'KID BEDDING'
							ELSE new_type_final END)
WHERE new_type_final = 'FEEDING ACCESSORIES'
;

--FEEDING ACCESSORIES and FEEDING BOTTLE AND NIPPLE by Product Name
UPDATE vpm_data.pm_skutagging_kids_product_master
SET new_type_final = (CASE WHEN product_name LIKE '%�Ǵ��%' THEN 'FEEDING BOTTLE AND NIPPLE'
						   WHEN product_name LIKE '%�Ǵ���%' THEN 'FEEDING ACCESSORIES'
						   WHEN product_name LIKE '���ͧ%BOX MINI%' THEN 'FEEDING ACCESSORIES'
						   ELSE new_type_final END)
WHERE new_type_final IS NULL
AND mom_and_kid_product_flag = 1
AND subdept_name IN ('NOVELTY','ZEENZONE')
;

--BOTTLE WARMER
UPDATE vpm_data.pm_skutagging_kids_product_master
SET new_type_final = (CASE WHEN product_name LIKE '��Ե�ѳ�좨Ѵ��Һ�Ҵ���͹��' THEN 'FEEDING ACCESSORIES'
							WHEN product_name LIKE 'BABYFOOD STEAMER AND BLENDER' THEN 'WEANING'
							WHEN product_name LIKE '%����ͧ��觾�������%' THEN 'WEANING'
							WHEN product_name LIKE '%����ͧ��觻�������%' THEN 'WEANING'
							WHEN product_name LIKE '%����ͧ��������%' THEN 'WEANING'
							ELSE new_type_final END)
WHERE mom_and_kid_product_flag = 1
AND new_type_final = 'BOTTLE WARMER'
;

--WEANING
UPDATE vpm_data.pm_skutagging_kids_product_master
SET new_type_final = (CASE WHEN product_name LIKE '%�ç��ҧ%' THEN 'FEEDING ACCESSORIES'
							WHEN product_name LIKE '%�ç�Ӥ������Ҵ%' THEN 'FEEDING ACCESSORIES'
							WHEN product_name LIKE '%����觹���%' THEN 'FEEDING ACCESSORIES'
							WHEN product_name LIKE '%���ͧ����%' THEN 'FEEDING ACCESSORIES'
							WHEN product_name LIKE '%�ǧ��%' THEN 'FEEDING ACCESSORIES'
							WHEN product_name LIKE '%MILK POWDER%' THEN 'FEEDING ACCESSORIES'
							WHEN product_name LIKE '%FORMULA DISPENSER%' THEN 'FEEDING ACCESSORIES'
							WHEN product_name LIKE '%��ҡѹ���͹%' THEN 'FEEDING ACCESSORIES'
							WHEN product_name LIKE '%��ӹ�%' THEN 'FEEDING ACCESSORIES'
							WHEN product_name LIKE '%PACIFER%' THEN 'FEEDING BOTTLE AND NIPPLE'
							WHEN product_name LIKE '�ͧ������͹ SUNSTORY' THEN 'OTHER KID ACCESSORIES'
							WHEN product_name LIKE '%�ҧ�Ѵ���%' THEN 'OTHER TOYS'
							WHEN product_name LIKE 'BREAST MILK STORAGE' THEN 'MATERNITY MILK STORAGE'
							WHEN product_name LIKE '%������ͧ��ҹ%' THEN 'OTHER KID FURNITURE'
							ELSE new_type_final END)
WHERE mom_and_kid_product_flag = 1
AND new_type_final = 'WEANING'
;

--KID BATHROOM ACCESSORIES
UPDATE vpm_data.pm_skutagging_kids_product_master
SET new_type_final = (CASE WHEN product_name LIKE '%�����������%' THEN 'OTHER KID ACCESSORIES'
							WHEN product_name LIKE '%BABY ACC CLEANSER%' THEN 'OTHER KID ACCESSORIES'
							WHEN product_name LIKE '%PLAYMAT CLEANSER%' THEN 'OTHER KID ACCESSORIES'
							WHEN product_name LIKE '%��� �������%' THEN 'OTHER KID ACCESSORIES'
							WHEN product_name LIKE '%�Ӥ������Ҵ�๡���ʧ��%' THEN 'OTHER KID ACCESSORIES'
							WHEN product_name LIKE '%�Ӥ������Ҵ����ͧ��%' THEN 'OTHER KID ACCESSORIES'
							WHEN product_name LIKE '%�索ͧ��%' THEN 'OTHER KID ACCESSORIES'
							WHEN product_name LIKE '%����ҫѡ���%' THEN 'OTHER KID ACCESSORIES'
							WHEN product_name LIKE '%����һ�Ѻ��ҹ���%' THEN 'OTHER KID ACCESSORIES'
							WHEN product_name LIKE '%������ѭ�ѹ���ا%' THEN 'OTHER KID ACCESSORIES'
							WHEN product_name LIKE '%BUG%AWAY%PATCH%' THEN 'OTHER KID ACCESSORIES'
							WHEN product_name LIKE '%MOSQUITO%' THEN 'OTHER KID ACCESSORIES'
							WHEN product_name LIKE '%�ѹ�ا%' THEN 'OTHER KID ACCESSORIES'
							WHEN product_name LIKE '%MOZZIE BLOCK%' THEN 'OTHER KID ACCESSORIES'
							WHEN product_name LIKE '%BABY POWDER%' THEN 'OTHER KID ACCESSORIES'
							WHEN product_name LIKE '%BABY KIDS SOOTHING CREAM%' THEN 'OTHER KID ACCESSORIES'
							WHEN product_name LIKE '%COOLING GEL%' THEN 'OTHER KID ACCESSORIES'
							WHEN product_name LIKE '%�����Ҽ��˹����м�ǡ��%' THEN 'OTHER KID ACCESSORIES'
							WHEN product_name LIKE '%�������ҧ���%' THEN 'OTHER KID ACCESSORIES'
							WHEN product_name LIKE '%�����ᴧ%' THEN 'OTHER KID ACCESSORIES'
							WHEN product_name LIKE '%�����ѹᴴ%' THEN 'OTHER KID ACCESSORIES'
							WHEN product_name LIKE '%��Ң�˹�%' THEN 'KID FABRIC'
							WHEN product_name LIKE '%TOWEL%' THEN 'KID FABRIC'
							WHEN product_name LIKE '�ش��С����%' THEN 'CAR SEAT'
							WHEN product_name LIKE '%ʹѺ���%' THEN 'OTHER TOYS'
							WHEN product_name LIKE '%�ѹ���%' THEN 'OTHER KID FURNITURE'
							WHEN product_name LIKE '%CORNER PROTECTOR%' THEN 'OTHER KID FURNITURE'
							WHEN product_name LIKE '%���ѹ����%' THEN 'OTHER KID FURNITURE'
							WHEN product_name LIKE '%�����%' THEN 'OTHER KID FURNITURE'
							WHEN product_name LIKE '%OUTLET PLUGS%' THEN 'OTHER KID FURNITURE'
							WHEN product_name LIKE '%��ҧ�ء����ТǴ��%' THEN 'FEEDING ACCESSORIES'
							WHEN product_name LIKE '%��Ե�ѳ����ҧ�ѡ��м����%' THEN 'FEEDING ACCESSORIES'
							WHEN product_name LIKE '%NIPPLE AND LIP BALM%' THEN 'MATERNITY TREATMENT'
							WHEN product_name LIKE '%ANTI STRETCH MARK CREAM%' THEN 'MATERNITY TREATMENT'
							WHEN product_name LIKE '%����Ф�˹��͡%' THEN 'MATERNITY TREATMENT'
							ELSE new_type_final END)
WHERE mom_and_kid_product_flag = 1
AND new_type_final = 'KID BATHROOM ACCESSORIES'
;

--OTHER KID ACCESSORIES
UPDATE vpm_data.pm_skutagging_kids_product_master
SET new_type_final = (CASE WHEN product_name LIKE '%BOTTLE BRUSH%' THEN 'FEEDING ACCESSORIES'
							WHEN product_name LIKE '%��ҧ�Ǵ��%' THEN 'FEEDING ACCESSORIES'
							WHEN product_name LIKE '%����%' THEN 'KID BATHROOM ACCESSORIES'
							WHEN product_name LIKE '%��м�%' THEN 'KID BATHROOM ACCESSORIES'
							WHEN product_name LIKE '%�ʹ���ͪ%' THEN 'KID BATHROOM ACCESSORIES'
							WHEN product_name LIKE '%ʺ������%' THEN 'KID BATHROOM ACCESSORIES'
							WHEN product_name LIKE '%����紾ѹ%' THEN 'KID BATHROOM ACCESSORIES'
							WHEN product_name LIKE '%���տѹ%' THEN 'KID BATHROOM ACCESSORIES'
							WHEN product_name LIKE '%����͡����%' THEN 'KID UNDERWEAR'
							WHEN product_name LIKE '%�ҧࡧ�%' THEN 'KID UNDERWEAR'
							WHEN product_name LIKE '%3D MASK KID%' THEN 'KID FASHION ACCESSORIES'
							WHEN product_name LIKE '%3D˹�ҡҡ͹����%' THEN 'KID FASHION ACCESSORIES'
							WHEN product_name LIKE '%��蹡ѹᴴ%' THEN 'KID FASHION ACCESSORIES'
							WHEN product_name LIKE '%����Ѵ��ا��ͧ' THEN 'MATERNITY BELT'
							WHEN product_name LIKE '��������ͧ��Ǫ�ǧ��駤����%' THEN 'MATERNITY TREATMENT'
							WHEN product_name LIKE '%BREAST%MILK%STORAGE%' THEN 'MATERNITY MILK STORAGE'
							ELSE new_type_final END)
WHERE mom_and_kid_product_flag = 1
AND new_type_final = 'OTHER KID ACCESSORIES'
;

-----KID CAR SEAT AND TRAVEL GEAR
--STROLLER
UPDATE vpm_data.pm_skutagging_kids_product_master
SET new_type_final = (CASE WHEN product_name LIKE '%BABY%CRADLE%' THEN 'KID BEDDING'
							WHEN product_name LIKE '%��ྐྵ%' THEN 'KID BEDDING'
							WHEN product_name LIKE '%BABY WALKER%' THEN 'BABY WALKER'
							WHEN product_name LIKE '%BABY CARRIER%' THEN 'BABY CARRIER'
							WHEN product_name LIKE '%����շ%' THEN 'CAR SEAT'
							ELSE new_type_final END)
WHERE mom_and_kid_product_flag = 1
AND new_type_final = 'STROLLER'
;

--BABY CARRIER
UPDATE vpm_data.pm_skutagging_kids_product_master
SET new_type_final = (CASE WHEN product_name LIKE '%��俿��%' THEN 'KID BEDDING'
							WHEN product_name LIKE '%��ྐྵ%' THEN 'KID BEDDING'
							WHEN product_name LIKE '%����ҧ˹ѧ���%' THEN 'OTHER KID FURNITURE'
							WHEN product_name LIKE '%�ѹ�����Ѻ��%' THEN 'OTHER KID FURNITURE'
							WHEN product_name LIKE '%��Ŵ�����%' THEN 'OTHER TOYS'
							ELSE new_type_final END)
WHERE mom_and_kid_product_flag = 1
AND new_type_final = 'BABY CARRIER'
;

-----KID FURNITURE
--KID BEDDING
UPDATE vpm_data.pm_skutagging_kids_product_master
SET new_type_final = (CASE WHEN product_name LIKE '%��Ң�˹�%' THEN 'KID FABRIC'
							WHEN product_name LIKE '%��Ң�˹����%' THEN 'KID BATHROOM ACCESSORIES'
							WHEN product_name LIKE '%GLOVES SET%' THEN 'KID FASHION ACCESSORIES'
							WHEN product_name LIKE '%��͹��Фͧ��ͧ%' THEN 'MATERNITY PILLOW'
							WHEN product_name LIKE '%��͹�ͧ��ͧ%' THEN 'MATERNITY PILLOW'
							WHEN product_name LIKE '%�����͵��%' THEN 'KID FABRIC'
							ELSE new_type_final END)
WHERE mom_and_kid_product_flag = 1
AND mom_and_kid_product_flag = 1
AND new_type_final = 'KID BEDDING'
;

-----OTHER TOYS
--BABY BOUNCER
UPDATE vpm_data.pm_skutagging_kids_product_master
SET new_type_final = (CASE WHEN product_name LIKE 'BABY CRIB%' THEN 'KID BEDDING'
							WHEN product_name LIKE '��§%' THEN 'KID BEDDING'
							WHEN product_name LIKE 'PNP BASE FOLDING FEET STRATUS' THEN 'KID BEDDING'
							WHEN product_name LIKE '%PNP ON THE GO%' THEN 'KID BEDDING'
							WHEN product_name LIKE '%����%' THEN 'KID BEDDING'
							WHEN product_name LIKE '%�Ź͹%' THEN 'KID BEDDING'
							WHEN product_name LIKE '%��ྐྵ%' THEN 'KID BEDDING'
							WHEN product_name LIKE '�� %' THEN 'KID BEDDING'
							WHEN product_name LIKE '%LULLABY DREAM PLAYARD%' THEN 'KID BEDDING'
							WHEN product_name LIKE '%NAPPER CHANGER%' THEN 'OTHER KID FURNITURE'
							ELSE new_type_final END)
WHERE mom_and_kid_product_flag = 1
AND new_type_final = 'BABY BOUNCER'
;

--BABY WALKER
UPDATE vpm_data.pm_skutagging_kids_product_master
SET new_type_final = (CASE WHEN product_name LIKE 'LETTER TRAIN PIANO ACTIVITY' THEN 'OTHER TOYS'
							WHEN product_name LIKE 'GROW WITH ME LION SCOOTER' THEN 'OTHER TOYS'
							WHEN product_name LIKE 'TRIKE COMPLETE SET WHITE' THEN 'OTHER TOYS'
							WHEN product_name LIKE '%RIDE%' THEN 'OTHER TOYS'
							WHEN product_name LIKE '%ö������%' THEN 'OTHER TOYS'
							ELSE new_type_final END)
WHERE mom_and_kid_product_flag = 1
AND new_type_final = 'BABY WALKER'
;

--OTHER TOYS
UPDATE vpm_data.pm_skutagging_kids_product_master
SET new_type_final = (CASE WHEN product_name LIKE '%�ا���%' THEN 'KID FASHION ACCESSORIES'
							WHEN product_name LIKE 'BACKPACK%' THEN 'KID FASHION ACCESSORIES'
							WHEN product_name LIKE '%������%' THEN 'KID FASHION ACCESSORIES'
							WHEN product_name LIKE '%������%' THEN 'KID BEDDING'
							ELSE new_type_final END)
WHERE mom_and_kid_product_flag = 1
AND new_type_final = 'OTHER TOYS'
;


------------------------------------------------------------UPDATE STAGE FLAG AND KEYWORD
-----DIAPERS
--DIAPERS SIZE NB
UPDATE vpm_data.pm_skutagging_kids_product_master
SET "STAGE_M+0_to_M+3" = 'YES'
	,"STAGE_M+3_to_M+6" = 'N/A'
	,infant_flag = 'YES'
	,unknown_infant_substage = 'N/A'
	,toddler_flag = 'N/A'
	,INFANT_0_3M_KEYWORD = 'DIAPERS SIZE NB'
	,INFANT_4_6M_KEYWORD = NULL
	,INFANT_UNKNOWN_KEYWORD = NULL
	,TODDLER_KEYWORD = NULL
WHERE mom_and_kid_product_flag = 1
AND new_type_final = 'DIAPERS SIZE NB'
;

--DIAPERS SIZE S
UPDATE vpm_data.pm_skutagging_kids_product_master
SET "STAGE_M+3_to_M+6" = 'YES'
	,INFANT_4_6M_KEYWORD = 'DIAPERS SIZE S'
	,UNKNOWN_INFANT_SUBSTAGE = 'N/A'
	,TODDLER_FLAG = 'N/A'
	,INFANT_UNKNOWN_KEYWORD = NULL
	,TODDLER_KEYWORD = NULL
WHERE mom_and_kid_product_flag = 1
AND new_type_final = 'DIAPERS SIZE S'
;

--DIAPERS SIZE M
UPDATE vpm_data.pm_skutagging_kids_product_master
SET "STAGE_M+6_to_M+9" = 'YES'
	,"STAGE_M+9_to_M+12" = 'YES'
	,INFANT_7_9M_KEYWORD = 'DIAPERS SIZE M'
	,INFANT_10_12M_KEYWORD = 'DIAPERS SIZE M'
	,"STAGE_M+3_to_M+6"= 'N/A'
	,TODDLER_FLAG = 'N/A'
	,INFANT_4_6M_KEYWORD = NULL
	,TODDLER_KEYWORD = NULL
WHERE mom_and_kid_product_flag = 1
AND new_type_final = 'DIAPERS SIZE M'
;

--DIAPERS SIZE L/XL
UPDATE vpm_data.pm_skutagging_kids_product_master
SET TODDLER_FLAG = 'YES'
	,TODDLER_KEYWORD = (CASE WHEN new_type_final = 'DIAPERS SIZE L' THEN 'DIAPERS SIZE L'
							 WHEN new_type_final = 'DIAPERS SIZE XL' THEN 'DIAPERS SIZE XL'
							 ELSE TODDLER_KEYWORD END)
	,"STAGE_M+3_to_M+6"= 'N/A'
	,UNKNOWN_INFANT_SUBSTAGE = 'N/A'
	,INFANT_4_6M_KEYWORD = NULL
	,INFANT_UNKNOWN_KEYWORD = NULL
WHERE mom_and_kid_product_flag = 1
AND new_type_final IN ('DIAPERS SIZE L','DIAPERS SIZE XL')
;

--OTHER DIAPERS
UPDATE vpm_data.pm_skutagging_kids_product_master
SET UNKNOWN_INFANT_SUBSTAGE = 'YES'
	,TODDLER_FLAG = 'YES'
	,INFANT_UNKNOWN_KEYWORD = '�������'
	,TODDLER_KEYWORD = '�������'
	,"STAGE_M+0_to_M+3" = 'N/A'
	,"STAGE_M+3_to_M+6" = 'N/A'
	,INFANT_0_3M_KEYWORD = NULL
	,INFANT_4_6M_KEYWORD = NULL
WHERE mom_and_kid_product_flag = 1
AND new_type_final LIKE 'OTHER DIAPERS'
;

-----KID BOOKS & STATIONERY
--PREGNANCY BOOK
UPDATE vpm_data.pm_skutagging_kids_product_master
SET "STAGE_M-9_to_M-6" = 'YES'
	,"STAGE_M-6_to_M-3" = 'YES'
	,PREG_0_3M_KEYWORD = 'PREGNANCY BOOK'
	,PREG_4_6M_KEYWORD = 'PREGNANCY BOOK'
	,UNKNOWN_PREG_SUBSTAGE = 'N/A'
	,UNKNOWN_INFANT_SUBSTAGE = 'N/A'
	,PREG_UNKNOWN_KEYWORD = NULL
	,INFANT_UNKNOWN_KEYWORD = NULL
WHERE mom_and_kid_product_flag = 1
AND new_type_final = 'PREGNANCY BOOK'
;

--PARENT GUIDE BOOK
--UPDATE vpm_data.pm_skutagging_kids_product_master
--SET "STAGE_M+0_to_M+3" = 'N/A'
--	,"STAGE_M+3_to_M+6" = 'N/A'
--	,"STAGE_M+6_to_M+9" = 'N/A'
--	,"STAGE_M+9_to_M+12" = 'N/A'
--	,INFANT_0_3M_KEYWORD = NULL
--	,INFANT_4_6M_KEYWORD = NULL
--	,INFANT_7_9M_KEYWORD = NULL
--	,INFANT_10_12M_KEYWORD = NULL
--	,INFANT_FLAG = 'YES'
--	,UNKNOWN_INFANT_SUBSTAGE = 'YES'
--	,TODDLER_FLAG = 'YES'
--	,PRESCHOOL_FLAG = 'YES'
--WHERE new_type_final LIKE 'PARENT GUIDE BOOK'
--AND ("STAGE_M+0_to_M+3" = 'YES'
--	OR "STAGE_M+3_to_M+6" = 'YES'
--	OR "STAGE_M+6_to_M+9" = 'YES'
--	OR "STAGE_M+9_to_M+12" = 'YES')
--;

--PARENT GUIDE BOOK
UPDATE vpm_data.pm_skutagging_kids_product_master
SET "STAGE_M+0_to_M+3" = 'N/A'
	,"STAGE_M+3_to_M+6" = 'N/A'
	,"STAGE_M+6_to_M+9" = 'N/A'
	,"STAGE_M+9_to_M+12" = 'N/A'
	,UNKNOWN_INFANT_SUBSTAGE = 'N/A'
	,INFANT_FLAG = 'N/A'
	,TODDLER_FLAG = 'N/A'
	,PRESCHOOL_FLAG = 'N/A'
	,INFANT_0_3M_KEYWORD = NULL
	,INFANT_4_6M_KEYWORD = NULL
	,INFANT_7_9M_KEYWORD = NULL
	,INFANT_10_12M_KEYWORD = NULL
	,INFANT_UNKNOWN_KEYWORD = NULL
	,TODDLER_KEYWORD = NULL
	,PRESCHOOL_KEYWORD = NULL
	,JUNIOR_FLAG = 'YES'
	,TEEN_FLAG = 'YES'
WHERE mom_and_kid_product_flag = 1
AND new_type_final LIKE 'PARENT GUIDE BOOK'
AND product_name LIKE '%������%'
;

--COOK BOOK
UPDATE vpm_data.pm_skutagging_kids_product_master
SET "STAGE_M+3_to_M+6" = 'YES'
	,"STAGE_M+6_to_M+9" = 'YES'
	,"STAGE_M+9_to_M+12" = 'YES'
	,TODDLER_FLAG = 'YES'
	,UNKNOWN_INFANT_SUBSTAGE = 'N/A'
	,INFANT_UNKNOWN_KEYWORD = NULL
WHERE mom_and_kid_product_flag = 1
AND new_type_final = 'COOK BOOK'
AND UNKNOWN_INFANT_SUBSTAGE = 'YES'
;

--SCHOOL BOOK: JUNIOR
UPDATE vpm_data.pm_skutagging_kids_product_master
SET JUNIOR_FLAG = 'YES'
	,TODDLER_FLAG = 'N/A'
	,PRESCHOOL_FLAG = 'N/A'
	,TODDLER_KEYWORD = NULL
	,PRESCHOOL_KEYWORD = NULL
WHERE mom_and_kid_product_flag = 1
AND new_type_final = 'SCHOOL BOOK'
AND (TODDLER_FLAG = 'YES' OR PRESCHOOL_FLAG = 'YES')
AND class_name IN ('AGES 4-8/EDUCATIONAL BOOK','AGES 9-12/ EDUCATIONAL BO','PRIMARY SCHOOL','JUNIOR PRIMARY SCHOOL')
;

--SCHOOL BOOK: TEEN
UPDATE vpm_data.pm_skutagging_kids_product_master
SET TEEN_FLAG = 'YES'
	,TODDLER_FLAG = 'N/A'
	,PRESCHOOL_FLAG = 'N/A'
	,TODDLER_KEYWORD = NULL
	,PRESCHOOL_KEYWORD = NULL
WHERE mom_and_kid_product_flag = 1
AND new_type_final = 'SCHOOL BOOK'
AND (TODDLER_FLAG = 'YES' OR PRESCHOOL_FLAG = 'YES')
AND class_name IN ('JUNIOR HIGH SCHOOL(M1-3)','LEARNING MEDIA HIGHSCHOOL','LEARNING MEDIA PRIMARY','SENIOR HIGH SCHOOL(M4-6)'
,'STUDY AND EXAMINATION GUI','TEENAGE REFERENCE MATERTI')
;

--TALE BOOK
UPDATE vpm_data.pm_skutagging_kids_product_master
SET PRESCHOOL_FLAG = 'YES'
	,TEEN_FLAG = 'N/A'
	,TEEN_KEYWORD = NULL
WHERE mom_and_kid_product_flag = 1
AND new_type_final = 'TALE BOOK'
AND TEEN_FLAG = 'YES'
;

--ACTIVITY BOOK
UPDATE vpm_data.pm_skutagging_kids_product_master
SET "STAGE_M+3_to_M+6" = 'N/A'
	,"STAGE_M+6_to_M+9" = 'N/A'
	,INFANT_FLAG = 'N/A'
	,INFANT_4_6M_KEYWORD = NULL
	,INFANT_7_9M_KEYWORD = NULL
	,TODDLER_FLAG = 'YES'
	,PRESCHOOL_FLAG = 'YES'
WHERE mom_and_kid_product_flag = 1
AND new_type_final LIKE 'ACTIVITY BOOK'
AND product_name LIKE '%COLORING BOOK%'
;

--PRESCHOOL MATH BOOK
UPDATE vpm_data.pm_skutagging_kids_product_master
SET TODDLER_FLAG = 'YES'
	,PRESCHOOL_FLAG = 'YES'
	,JUNIOR_FLAG = 'N/A'
WHERE mom_and_kid_product_flag = 1
AND new_type_final = 'PRESCHOOL MATH BOOK'
;

--PRESCHOOL ENGLISH BOOK
UPDATE vpm_data.pm_skutagging_kids_product_master
SET TODDLER_FLAG = 'YES'
	,PRESCHOOL_FLAG = 'YES'
	,JUNIOR_FLAG = 'N/A'
WHERE mom_and_kid_product_flag = 1
AND new_type_final = 'PRESCHOOL ENGLISH BOOK'
;

--PRESCHOOL THAI BOOK
UPDATE vpm_data.pm_skutagging_kids_product_master
SET TODDLER_FLAG = 'YES'
	,PRESCHOOL_FLAG = 'YES'
	,JUNIOR_FLAG = 'N/A'
WHERE mom_and_kid_product_flag = 1
AND new_type_final = 'PRESCHOOL THAI BOOK'
;

--OTHER KID BOOK
UPDATE vpm_data.pm_skutagging_kids_product_master
SET UNKNOWN_INFANT_SUBSTAGE = 'N/A'
	,INFANT_UNKNOWN_KEYWORD = NULL
WHERE mom_and_kid_product_flag = 1
AND new_type_final = 'OTHER KID BOOK'
AND UNKNOWN_INFANT_SUBSTAGE = 'YES'
;

--KID STATIONERY
UPDATE vpm_data.pm_skutagging_kids_product_master
SET PRESCHOOL_FLAG = 'YES'
	,JUNIOR_FLAG = 'YES'
	,TEEN_FLAG = 'YES'
	,TODDLER_FLAG = 'N/A'
	,TODDLER_KEYWORD = NULL
WHERE mom_and_kid_product_flag = 1
AND new_type_final = 'KID STATIONERY'
;

-----KID FOOD
--MILK POWDER FORMULA 1
UPDATE vpm_data.pm_skutagging_kids_product_master
SET "STAGE_M+0_to_M+3" = 'YES'
	,"STAGE_M+3_to_M+6" = 'YES'
	,"STAGE_M+6_to_M+9" = 'N/A'
	,"STAGE_M+9_to_M+12" = 'N/A'
	,TODDLER_FLAG = 'N/A'
	,INFANT_7_9M_KEYWORD = NULL
	,INFANT_10_12M_KEYWORD = NULL
	,TODDLER_KEYWORD = NULL
WHERE mom_and_kid_product_flag = 1
AND new_type_final = 'MILK POWDER FORMULA 1'
;

--MILK POWDER FORMULA 2
UPDATE vpm_data.pm_skutagging_kids_product_master
SET "STAGE_M+0_to_M+3" = 'N/A'
	,"STAGE_M+3_to_M+6" = 'N/A'
	,"STAGE_M+6_to_M+9" = 'YES'
	,"STAGE_M+9_to_M+12" = 'YES'
	,TODDLER_FLAG = 'YES'
	,INFANT_0_3M_KEYWORD = NULL
	,INFANT_4_6M_KEYWORD = NULL
WHERE mom_and_kid_product_flag = 1
AND new_type_final = 'MILK POWDER FORMULA 2'
;

--MILK POWDER FORMULA 3
UPDATE vpm_data.pm_skutagging_kids_product_master
SET "STAGE_M+0_to_M+3" = 'N/A'
	,"STAGE_M+3_to_M+6" = 'N/A'
	,"STAGE_M+6_to_M+9" = 'N/A'
	,"STAGE_M+9_to_M+12" = 'N/A'
	,UNKNOWN_INFANT_SUBSTAGE = 'N/A'
	,INFANT_FLAG = 'N/A'
	,TODDLER_FLAG = 'YES'
	,INFANT_0_3M_KEYWORD = NULL
	,INFANT_4_6M_KEYWORD = NULL
	,INFANT_7_9M_KEYWORD = NULL
	,INFANT_10_12M_KEYWORD = NULL
	,INFANT_UNKNOWN_KEYWORD = NULL
WHERE mom_and_kid_product_flag = 1
AND new_type_final = 'MILK POWDER FORMULA 3'
;

--OTHER MILK POWDER: CANNOT TELL FORMULA
UPDATE vpm_data.pm_skutagging_kids_product_master
SET "STAGE_M+0_to_M+3" = 'N/A'
	,"STAGE_M+3_to_M+6" = 'N/A'
	,"STAGE_M+6_to_M+9" = 'N/A'
	,"STAGE_M+9_to_M+12" = 'N/A'
	,UNKNOWN_INFANT_SUBSTAGE = 'YES'
	,INFANT_FLAG = 'YES'
	,TODDLER_FLAG = 'YES'
	,INFANT_0_3M_KEYWORD = NULL
	,INFANT_4_6M_KEYWORD = NULL
	,INFANT_7_9M_KEYWORD = NULL
	,INFANT_10_12M_KEYWORD = NULL
WHERE mom_and_kid_product_flag = 1
AND new_type_final = 'OTHER MILK POWDER'
;

--OTHER MILK POWDER: FORMULA 4
UPDATE vpm_data.pm_skutagging_kids_product_master
SET "STAGE_M+0_to_M+3" = 'N/A'
	,"STAGE_M+3_to_M+6" = 'N/A'
	,"STAGE_M+6_to_M+9" = 'N/A'
	,"STAGE_M+9_to_M+12" = 'N/A'
	,UNKNOWN_INFANT_SUBSTAGE = 'N/A'
	,INFANT_FLAG = 'N/A'
	,TODDLER_FLAG = 'YES'
	,PRESCHOOL_FLAG = 'YES'
	,INFANT_UNKNOWN_KEYWORD = NULL
	,TODDLER_KEYWORD = 'MILK POWDER FORMULA 4'
	,PRESCHOOL_KEYWORD = 'MILK POWDER FORMULA 4'
WHERE mom_and_kid_product_flag = 1
AND new_type_final = 'OTHER MILK POWDER'
AND (product_name LIKE '%3 Plus%'
	OR product_name LIKE '%3����%'
	OR product_name LIKE '%DUMILK4%')
;

--KID UHT MILK
UPDATE vpm_data.pm_skutagging_kids_product_master
SET "STAGE_M+0_to_M+3" = 'N/A'
	,"STAGE_M+3_to_M+6" = 'N/A'
	,"STAGE_M+6_to_M+9" = 'N/A'
	,"STAGE_M+9_to_M+12" = 'N/A'
	,UNKNOWN_INFANT_SUBSTAGE = 'N/A'
	,INFANT_FLAG = 'N/A'
	,INFANT_0_3M_KEYWORD = NULL
	,INFANT_4_6M_KEYWORD = NULL
	,INFANT_7_9M_KEYWORD = NULL
	,INFANT_10_12M_KEYWORD = NULL
	,INFANT_UNKNOWN_KEYWORD = NULL
	,TODDLER_FLAG = 'YES'
	,PRESCHOOL_FLAG = 'YES'
	,JUNIOR_FLAG = 'YES'
WHERE mom_and_kid_product_flag = 1
AND new_type_final = 'KID UHT MILK'
;

--STRAINED FOOD AND PUREE
UPDATE vpm_data.pm_skutagging_kids_product_master
SET UNKNOWN_INFANT_SUBSTAGE = 'N/A'
	,INFANT_UNKNOWN_KEYWORD = NULL
	,"STAGE_M+6_to_M+9" = 'YES'
	,"STAGE_M+9_to_M+12" = 'YES'
WHERE new_type_final LIKE 'STRAINED FOOD AND PUREE' AND product_name LIKE '%�����Ť������������%'
;

--FINGER FOOD
UPDATE vpm_data.pm_skutagging_kids_product_master
SET UNKNOWN_INFANT_SUBSTAGE = 'N/A'
	,INFANT_UNKNOWN_KEYWORD = NULL
	,"STAGE_M+6_to_M+9" = 'YES'
	,"STAGE_M+9_to_M+12" = 'YES'
WHERE mom_and_kid_product_flag = 1
AND new_type_final LIKE 'FINGER FOOD'
AND product_name LIKE '%���຺��亷�%'
;

--TABLE FOOD
UPDATE vpm_data.pm_skutagging_kids_product_master
SET UNKNOWN_INFANT_SUBSTAGE = 'N/A'
	,INFANT_UNKNOWN_KEYWORD = NULL
	,TODDLER_FLAG = 'YES'
WHERE mom_and_kid_product_flag = 1
AND new_type_final LIKE 'TABLE FOOD'
;

-----MATERNITY
--MATERNITY CLOTHING 1
UPDATE vpm_data.pm_skutagging_kids_product_master
SET UNKNOWN_PREG_SUBSTAGE = 'N/A'
	,PRESCHOOL_FLAG = 'N/A'
	,JUNIOR_FLAG = 'N/A'
	,TEEN_FLAG = 'N/A'
	,PREG_UNKNOWN_KEYWORD = NULL
	,PRESCHOOL_KEYWORD = NULL
	,JUNIOR_KEYWORD = NULL
	,TEEN_KEYWORD = NULL
	,"STAGE_M-6_to_M-3" = 'YES'
	,"STAGE_M-3_to_M-0" = 'YES'
	,INFANT_FLAG = 'YES'
	,UNKNOWN_INFANT_SUBSTAGE = 'YES'
WHERE mom_and_kid_product_flag = 1
AND new_type_final LIKE 'MATERNITY CLOTHING'
AND (UNKNOWN_PREG_SUBSTAGE = 'YES'
	OR PRESCHOOL_FLAG = 'YES'
	OR JUNIOR_FLAG = 'YES'
	OR TEEN_FLAG = 'YES')
;

--MATERNITY CLOTHING 2
UPDATE vpm_data.pm_skutagging_kids_product_master
SET UNKNOWN_INFANT_SUBSTAGE = 'YES'
	,"STAGE_M+3_to_M+6" = 'N/A'
	,INFANT_4_6M_KEYWORD = NULL
WHERE mom_and_kid_product_flag = 1
AND new_type_final = 'MATERNITY CLOTHING'
AND "STAGE_M+3_to_M+6" = 'YES'
;

--MATERNITY LINGERIE
UPDATE vpm_data.pm_skutagging_kids_product_master
SET UNKNOWN_PREG_SUBSTAGE = 'N/A'
	,"STAGE_M+3_to_M+6" = 'YES'
	,"STAGE_M+6_to_M+9" = 'YES'
	,PREG_UNKNOWN_KEYWORD = NULL
WHERE mom_and_kid_product_flag = 1
AND new_type_final = 'MATERNITY LINGERIE'
AND UNKNOWN_PREG_SUBSTAGE = 'YES'
;

--MATERNITY BREAST PADS
UPDATE vpm_data.pm_skutagging_kids_product_master
SET INFANT_FLAG = 'YES'
	,UNKNOWN_INFANT_SUBSTAGE = 'YES'
	,TODDLER_FLAG = 'YES'
WHERE mom_and_kid_product_flag = 1
AND new_type_final LIKE 'MATERNITY BREAST PADS'
;

--MATERNITY BELT
UPDATE vpm_data.pm_skutagging_kids_product_master
SET INFANT_FLAG = 'YES'
	,UNKNOWN_INFANT_SUBSTAGE = 'YES'
	,TODDLER_FLAG = 'N/A'
	,TODDLER_KEYWORD = NULL
WHERE mom_and_kid_product_flag = 1
AND new_type_final LIKE 'MATERNITY BELT'
AND TODDLER_FLAG = 'YES'
;

--MATERNITY PILLOW 1
UPDATE vpm_data.pm_skutagging_kids_product_master
SET INFANT_FLAG = 'YES'
	,UNKNOWN_INFANT_SUBSTAGE = 'YES'
	,"STAGE_M+0_to_M+3" = 'N/A'
	,"STAGE_M+3_to_M+6" = 'N/A'
	,INFANT_0_3M_KEYWORD = NULL
	,INFANT_4_6M_KEYWORD = NULL
WHERE mom_and_kid_product_flag = 1
AND new_type_final LIKE 'MATERNITY PILLOW'
AND ("STAGE_M+0_to_M+3" = 'YES'
	OR "STAGE_M+3_to_M+6" = 'YES')
;

--MATERNITY PILLOW 2
UPDATE vpm_data.pm_skutagging_kids_product_master
SET UNKNOWN_PREG_SUBSTAGE = 'N/A'
	,PREG_UNKNOWN_KEYWORD = NULL
	,"STAGE_M-6_to_M-3" = 'YES'
	,"STAGE_M-3_to_M-0" = 'YES'
WHERE mom_and_kid_product_flag = 1
AND new_type_final LIKE 'MATERNITY PILLOW'
AND UNKNOWN_PREG_SUBSTAGE = 'YES'
AND product_name LIKE '%��͹�ͧ��ͧ%'
;

--OTHER MATERNITY
UPDATE vpm_data.pm_skutagging_kids_product_master
SET UNKNOWN_INFANT_SUBSTAGE = 'YES'
	,INFANT_FLAG = 'YES'
WHERE mom_and_kid_product_flag = 1
AND new_type_final LIKE 'OTHER MATERNITY'
AND product_name LIKE '��Ф���͹ THEROMA CLASSIC'
;

-----KID FASHION
--KID DAYWEAR
UPDATE vpm_data.pm_skutagging_kids_product_master
SET "STAGE_M-9_to_M-6" = 'N/A'
	,"STAGE_M-6_to_M-3" = 'N/A'
	,"STAGE_M-3_to_M-0" = 'N/A'
	,PREG_FLAG = 'N/A'
	,PREG_0_3M_KEYWORD = NULL
	,PREG_4_6M_KEYWORD = NULL
	,PREG_7_9M_KEYWORD = NULL
	,INFANT_FLAG = 'YES'
	,UNKNOWN_INFANT_SUBSTAGE = 'YES'
WHERE mom_and_kid_product_flag = 1
AND new_type_final LIKE 'KID DAYWEAR'
AND (product_name LIKE 'BAMBOO BABY WATERPROOF MAT%'
	OR product_name LIKE '�ش��ANGEL 6-8-LIPSTICK')
;

-----KID ACCESSORIES
--WEANING
UPDATE vpm_data.pm_skutagging_kids_product_master
SET "STAGE_M+0_to_M+3" = 'N/A'
	,INFANT_0_3M_KEYWORD = NULL
	,INFANT_FLAG = 'YES'
	,UNKNOWN_INFANT_SUBSTAGE = 'YES'
WHERE mom_and_kid_product_flag = 1
AND new_type_final = 'WEANING'
AND "STAGE_M+0_to_M+3" = 'YES'
;

--KID FABRIC
UPDATE vpm_data.pm_skutagging_kids_product_master
SET JUNIOR_FLAG = 'N/A'
	,TEEN_FLAG = 'N/A'
	,JUNIOR_KEYWORD = NULL
	,TEEN_KEYWORD = NULL
	,UNKNOWN_INFANT_SUBSTAGE = 'YES'
	,INFANT_FLAG = 'YES'
WHERE mom_and_kid_product_flag = 1
AND new_type_final = 'KID FABRIC'
AND product_name LIKE '%�����͵��%'
AND (JUNIOR_FLAG = 'YES' OR TEEN_FLAG = 'YES')
;

--OTHER KID ACCESSORIES
UPDATE vpm_data.pm_skutagging_kids_product_master
SET "STAGE_M-6_to_M-3" = 'N/A'
	,"STAGE_M-3_to_M-0" = 'N/A'
	,UNKNOWN_PREG_SUBSTAGE = 'N/A'
	,PREG_FLAG = 'N/A'
	,PREG_4_6M_KEYWORD = NULL
	,PREG_7_9M_KEYWORD = NULL
	,PREG_UNKNOWN_KEYWORD = NULL
	,UNKNOWN_INFANT_SUBSTAGE = 'YES'
	,INFANT_FLAG = 'YES'
WHERE mom_and_kid_product_flag = 1
AND new_type_final = 'OTHER KID ACCESSORIES'
AND (product_name LIKE '�������ا����Ѻ�硼�������'
	OR product_name LIKE '����ҫѡ�����ŵ�����´�1000ML')
;

-----KID CAR SEAT AND TRAVEL GEAR
--STROLLER
UPDATE vpm_data.pm_skutagging_kids_product_master
SET "STAGE_M+0_to_M+3" = 'N/A'
	,"STAGE_M+3_to_M+6" = 'N/A'
	,"STAGE_M+6_to_M+9" = 'N/A'
	,"STAGE_M+9_to_M+12" = 'N/A'
	,UNKNOWN_INFANT_SUBSTAGE = 'YES'
	,INFANT_FLAG = 'YES'
	,TODDLER_FLAG = 'YES'
	,INFANT_0_3M_KEYWORD = NULL
	,INFANT_4_6M_KEYWORD = NULL
	,INFANT_7_9M_KEYWORD = NULL
	,INFANT_10_12M_KEYWORD = NULL
WHERE mom_and_kid_product_flag = 1
AND new_type_final = 'STROLLER'
;

--BABY CARRIER
UPDATE vpm_data.pm_skutagging_kids_product_master
SET "STAGE_M+0_to_M+3" = 'N/A'
	,"STAGE_M+3_to_M+6" = 'N/A'
	,"STAGE_M+6_to_M+9" = 'N/A'
	,"STAGE_M+9_to_M+12" = 'N/A'
	,UNKNOWN_INFANT_SUBSTAGE = 'YES'
	,INFANT_FLAG = 'YES'
	,TODDLER_FLAG = 'YES'
	,INFANT_0_3M_KEYWORD = NULL
	,INFANT_4_6M_KEYWORD = NULL
	,INFANT_7_9M_KEYWORD = NULL
	,INFANT_10_12M_KEYWORD = NULL
WHERE mom_and_kid_product_flag = 1
AND new_type_final = 'BABY CARRIER'
;

-----------------------------------------------Align Sub-stage Flag and Stage Flag
UPDATE vpm_data.pm_skutagging_kids_product_master
SET unknown_infant_substage = 'N/A'
WHERE mom_and_kid_product_flag = 1
AND infant_flag = 'N/A'
AND unknown_infant_substage = 'YES'
;

UPDATE vpm_data.pm_skutagging_kids_product_master
SET "STAGE_M+6_to_M+9" = 'N/A'
	,"STAGE_M+9_to_M+12" = 'N/A'
WHERE mom_and_kid_product_flag = 1
AND infant_flag = 'N/A'
AND "STAGE_M+6_to_M+9" = 'YES'
AND "STAGE_M+9_to_M+12" = 'YES'
;

UPDATE vpm_data.pm_skutagging_kids_product_master
SET infant_flag = 'N/A'
WHERE mom_and_kid_product_flag = 1
AND infant_flag = 'YES'
AND "STAGE_M+0_to_M+3" = 'N/A'
AND "STAGE_M+3_to_M+6" = 'N/A'
AND "STAGE_M+6_to_M+9" = 'N/A'
AND "STAGE_M+9_to_M+12" = 'N/A'
AND unknown_infant_substage = 'N/A'
;

UPDATE vpm_data.pm_skutagging_kids_product_master
SET unknown_infant_substage = 'N/A'
WHERE mom_and_kid_product_flag = 1
AND infant_flag = 'YES'
AND unknown_infant_substage = 'YES'
AND ("STAGE_M+0_to_M+3" = 'YES'
	OR "STAGE_M+3_to_M+6" = 'YES'
	OR "STAGE_M+6_to_M+9" = 'YES'
	OR "STAGE_M+9_to_M+12" = 'YES')
;


UPDATE vpm_data.pm_skutagging_kids_product_master
SET unknown_infant_substage = 'N/A'
WHERE mom_and_kid_product_flag = 1
AND infant_flag = 'N/A'
AND unknown_infant_substage = 'YES'
;

UPDATE vpm_data.pm_skutagging_kids_product_master
SET "STAGE_M+6_to_M+9" = 'N/A'
	,"STAGE_M+9_to_M+12" = 'N/A'
WHERE mom_and_kid_product_flag = 1
AND infant_flag = 'N/A'
AND "STAGE_M+6_to_M+9" = 'YES'
AND "STAGE_M+9_to_M+12" = 'YES'
;

UPDATE vpm_data.pm_skutagging_kids_product_master
SET infant_flag = 'N/A'
WHERE mom_and_kid_product_flag = 1
AND infant_flag = 'YES'
AND "STAGE_M+0_to_M+3" = 'N/A'
AND "STAGE_M+3_to_M+6" = 'N/A'
AND "STAGE_M+6_to_M+9" = 'N/A'
AND "STAGE_M+9_to_M+12" = 'N/A'
AND unknown_infant_substage = 'N/A'
;

UPDATE vpm_data.pm_skutagging_kids_product_master
SET unknown_infant_substage = 'N/A'
WHERE mom_and_kid_product_flag = 1
AND infant_flag = 'YES'
AND unknown_infant_substage = 'YES'
AND ("STAGE_M+0_to_M+3" = 'YES'
	OR "STAGE_M+3_to_M+6" = 'YES'
	OR "STAGE_M+6_to_M+9" = 'YES'
	OR "STAGE_M+9_to_M+12" = 'YES')
;

UPDATE vpm_data.pm_skutagging_kids_product_master
SET preg_flag = 'YES'
WHERE mom_and_kid_product_flag = 1
AND preg_flag = 'N/A'
AND "STAGE_M-9_to_M-6" = 'YES'
AND "STAGE_M-6_to_M-3" = 'YES'
;

UPDATE vpm_data.pm_skutagging_kids_product_master
SET preg_flag = 'YES'
WHERE mom_and_kid_product_flag = 1
AND preg_flag = 'N/A'
AND "STAGE_M-6_to_M-3" = 'YES'
AND "STAGE_M-3_to_M-0" = 'YES'
;

UPDATE vpm_data.pm_skutagging_kids_product_master
SET preg_flag = 'N/A'
	,unknown_preg_substage = 'N/A'
	,infant_flag = 'YES'
	,unknown_infant_substage = 'YES'
	,toddler_flag = 'YES'
WHERE mom_and_kid_product_flag = 1
AND new_type_final IN ('FEEDING BOTTLE AND NIPPLE','FEEDING ACCESSORIES','BOTTLE WARMER','WEANING','KID BATHROOM ACCESSORIES','KID FABRIC','OTHER KID ACCESSORIES')
AND product_name LIKE '�������ا����Ѻ�硼�������'
;


--update new schema
UPDATE vpm_data.pm_skutagging_kids_product_master
SET new_revised_category_level_2 = 'KID FURNITURE'
	,new_revised_category_level_3 = 'OTHER KID FURNITURE'
	,new_category_final = 'OTHER KID FURNITURE'
	,new_type_final = 'OTHER KID FURNITURE'
WHERE mom_and_kid_product_flag = 1
AND new_type_final IN ('DIAPERS SIZE NB','DIAPERS SIZE S','DIAPERS SIZE M','DIAPERS SIZE L','DIAPERS SIZE XL','OTHER DIAPERS')
AND product_name LIKE 'GOPAD TM DIAPER CHANGER'	
;
UPDATE vpm_data.pm_skutagging_kids_product_master
SET new_revised_category_level_2 = 'MATERNITY'
	,new_revised_category_level_3 = 'MATERNITY'
	,new_category_final = 'MATERNITY SELF-CARE'
	,new_type_final = 'MATERNITY BELT'
WHERE mom_and_kid_product_flag = 1
AND new_type_final IN ('KID BEDDING','OTHER KID FURNITURE')
AND product_name LIKE '%����Ѵ��ا�����%'
;
UPDATE vpm_data.pm_skutagging_kids_product_master
SET new_revised_category_level_2 = 'MATERNITY'
	,new_revised_category_level_3 = 'MATERNITY'
	,new_category_final = 'MATERNITY SELF-CARE'
	,new_type_final = 'MATERNITY SUPPLEMENT'
WHERE mom_and_kid_product_flag = 1
AND new_type_final IN ('FEEDING BOTTLE AND NIPPLE','FEEDING ACCESSORIES','BOTTLE WARMER','WEANING','KID BATHROOM ACCESSORIES','KID FABRIC','OTHER KID ACCESSORIES')
AND product_name LIKE '%�����ǻ��ʡѴ%'
;
UPDATE vpm_data.pm_skutagging_kids_product_master
SET new_revised_category_level_2 = 'MATERNITY'
	,new_revised_category_level_3 = 'MATERNITY'
	,new_category_final = 'MATERNITY SELF-CARE'
	,new_type_final = 'MATERNITY TREATMENT'
WHERE mom_and_kid_product_flag = 1
AND new_type_final IN ('FEEDING BOTTLE AND NIPPLE','FEEDING ACCESSORIES','BOTTLE WARMER','WEANING','KID BATHROOM ACCESSORIES','KID FABRIC','OTHER KID ACCESSORIES')
AND (product_name LIKE '%�������ͧ�ѹ���%'
	OR product_name LIKE '%�ѵ���������ҧ��駤����%'
	OR product_name LIKE '%�š�ЪѺ�Ѵ��ǹ%'
	OR product_name LIKE '%��Ŵ���͹���ᵡ���%'
	OR product_name LIKE '%������ЪѺ�Ѵ��ǹ%')
;

--kid bed
UPDATE vpm_data.pm_skutagging_kids_product_master
SET new_revised_category_level_1 = 'KIDS'
	,new_revised_category_level_2 = 'KID FURNITURE'
	,new_revised_category_level_3 = 'KID BEDDING'
	,new_category_final = 'KID BEDDING'
	,new_type_final = 'KID BEDDING'
WHERE mom_and_kid_product_flag = 1
AND new_type_final = 'OTHER KID FURNITURE'
AND product_name IN ('����ͧ�͹ COTTON','��§ DREAMIE BED SIDE WH','����ͧ�͹SUPER SOFT SPANDEX')
;

--other kid acc
UPDATE vpm_data.pm_skutagging_kids_product_master
SET new_revised_category_level_1 = 'KIDS'
	,new_revised_category_level_2 = 'KID ACCESSORIES'
	,new_revised_category_level_3 = 'OTHER KID ACCESSORIES'
	,new_category_final = 'OTHER KID ACCESSORIES'
	,new_type_final = 'OTHER KID ACCESSORIES'
WHERE mom_and_kid_product_flag = 1
AND new_type_final = 'OTHER KID FURNITURE'
AND (product_name IN ('�蹵Դ�ѹ�اBABY PLUS18 ���','DIGITAL THERMOMETER DIGIBABY','�ا��駼������������ٻ','GK ����ͧ�Ѵ�س�����','BABY LOTION LEMON SCENT','�ë�Դ�� ���պ����١'
					,'NAPPIE CREAM 75ML','COMFORT QUICK INFRARED EAR THE','MOBY BATH THERMOMETER CELSIUS','PROTECTIVELOTIONLAVENDER0+60ML'
					,'PROTECTIVE  (LAVENDER) 1+60ML.','����ͧ�Ѵ����')
	OR product_name LIKE '%����õѴ���%'
	OR product_name LIKE '%���ٴ����١%'
	OR product_name LIKE '%������������%'
	OR product_name LIKE '%THERMOMETER%'
	OR product_name LIKE '%MOSQUITO%VAPORIZER%'
	OR product_name LIKE '%MOSQUITOREPELLENTSPRAY%'
	OR product_name LIKE '%BUKKIEBOO%'
	OR product_name LIKE 'MOZZIE%'
	OR product_name LIKE '%�ѹ�ا%'
	OR product_name LIKE 'COMFEE COOLING GEL%'
	OR product_name LIKE '຺�� �ٷ��� �����%'
	OR product_name LIKE '%����ҧ���%'
	OR product_name LIKE '%SANITIZER%SPRAY%'
	OR product_name LIKE '%�ŷӤ�������Ҵ���%'
	OR product_name LIKE '%����¦������%'
	OR product_name LIKE '%�������š�����%')
;

--acc bath
UPDATE vpm_data.pm_skutagging_kids_product_master
SET new_revised_category_level_1 = 'KIDS'
	,new_revised_category_level_2 = 'KID ACCESSORIES'
	,new_revised_category_level_3 = 'KID BATHROOM ACCESSORIES'
	,new_category_final = 'KID BATHROOM ACCESSORIES'
	,new_type_final = 'KID BATHROOM ACCESSORIES'
WHERE mom_and_kid_product_flag = 1
AND new_type_final = 'OTHER KID FURNITURE'
AND (product_name IN ('�ç�Ǵ�˧�͡����⤹��������ͧ','���Ѵ�س�����SAFETYBATHDUCKY','ORALGEL ORGANIC 50G 6MTH','���տѹORGANICSTRAWBERRY50G 2Y')
	OR product_name LIKE '%���տѹ%'
	OR product_name LIKE '%HAND%SANITIZER%')
;

--car seat
UPDATE vpm_data.pm_skutagging_kids_product_master
SET new_revised_category_level_1 = 'KIDS'
	,new_revised_category_level_2 = 'KID CAR SEAT AND TRAVEL GEAR'
	,new_revised_category_level_3 = 'KID CAR SEAT AND TRAVEL GEAR'
	,new_category_final = 'KID CAR SEAT AND TRAVEL GEAR'
	,new_type_final = 'CAR SEAT'
WHERE mom_and_kid_product_flag = 1
AND new_type_final = 'OTHER KID FURNITURE'
AND product_name IN ('NEXTFIT ZIP MAX CAR SEAT QUANT')
;

--baby carrier
UPDATE vpm_data.pm_skutagging_kids_product_master
SET new_revised_category_level_1 = 'KIDS'
	,new_revised_category_level_2 = 'KID CAR SEAT AND TRAVEL GEAR'
	,new_revised_category_level_3 = 'KID CAR SEAT AND TRAVEL GEAR'
	,new_category_final = 'KID CAR SEAT AND TRAVEL GEAR'
	,new_type_final = 'BABY CARRIER'
WHERE mom_and_kid_product_flag = 1
AND new_type_final = 'OTHER KID FURNITURE'
AND product_name LIKE 'DIAMOND3IN1BABYSEAT%'
;

--baby bouncer
UPDATE vpm_data.pm_skutagging_kids_product_master
SET new_revised_category_level_1 = 'KIDS'
	,new_revised_category_level_2 = 'TOYS'
	,new_revised_category_level_3 = 'TOYS'
	,new_category_final = 'TOYS'
	,new_type_final = 'BABY BOUNCER'
WHERE mom_and_kid_product_flag = 1
AND new_type_final = 'OTHER KID FURNITURE'
AND (product_name LIKE 'FINCH 2N1 ROCKER%'
	OR product_name LIKE 'TOPAZBABYROCKERW/CANOPYPIK'
	OR product_name LIKE '%BABY ROCKER%'
	OR product_name LIKE 'TUFFDLXROCKER-SATURNTRIANGLE')
;

--fas acc
UPDATE vpm_data.pm_skutagging_kids_product_master
SET new_revised_category_level_1 = 'KIDS'
	,new_revised_category_level_2 = 'KID FASHION'
	,new_revised_category_level_3 = 'KID FASHION ACCESSORIES'
	,new_category_final = 'KID FASHION ACCESSORIES'
	,new_type_final = 'KID FASHION ACCESSORIES'
WHERE mom_and_kid_product_flag = 1
AND new_type_final = 'OTHER KID FURNITURE'
AND (product_name IN ('��͹ ˹�ҡҡ��ͧ�ѹ���')
	OR product_name LIKE '%MASK%'
	OR product_name LIKE '%˹�ҡҡ͹����%')
;

--mater belt
UPDATE vpm_data.pm_skutagging_kids_product_master
SET new_revised_category_level_1 = 'KIDS'
	,new_revised_category_level_2 = 'MATERNITY'
	,new_revised_category_level_3 = 'MATERNITY'
	,new_category_final = 'MATERNITY SELF-CARE'
	,new_type_final = 'MATERNITY BELT'
WHERE mom_and_kid_product_flag = 1
AND new_type_final = 'OTHER KID FURNITURE'
AND product_name LIKE '%����Ѵ��蹤����Ԥ%'
;

--mater tre
UPDATE vpm_data.pm_skutagging_kids_product_master
SET new_revised_category_level_1 = 'KIDS'
	,new_revised_category_level_2 = 'MATERNITY'
	,new_revised_category_level_3 = 'MATERNITY'
	,new_category_final = 'MATERNITY SELF-CARE'
	,new_type_final = 'MATERNITY TREATMENT'
WHERE mom_and_kid_product_flag = 1
AND new_type_final = 'OTHER KID FURNITURE'
AND product_name LIKE '%��Ф���͹%'
;

--other mater
UPDATE vpm_data.pm_skutagging_kids_product_master
SET new_revised_category_level_1 = 'KIDS'
	,new_revised_category_level_2 = 'MATERNITY'
	,new_revised_category_level_3 = 'MATERNITY'
	,new_category_final = 'MATERNITY SELF-CARE'
	,new_type_final = 'OTHER MATERNITY'
WHERE mom_and_kid_product_flag = 1
AND new_type_final = 'OTHER KID FURNITURE'
AND product_name LIKE '%���ѧ���§��%'
;

--mater lin
UPDATE vpm_data.pm_skutagging_kids_product_master
SET new_revised_category_level_1 = 'KIDS'
	,new_revised_category_level_2 = 'MATERNITY'
	,new_revised_category_level_3 = 'MATERNITY'
	,new_category_final = 'MATERNITY FASHION'
	,new_type_final = 'MATERNITY LINGERIE'
WHERE mom_and_kid_product_flag = 1
AND new_type_final = 'OTHER KID FURNITURE'
AND product_name LIKE '%���㹴����ż�Ҥ�ʹ%'
;

--other kid acc
UPDATE vpm_data.pm_skutagging_kids_product_master
SET new_revised_category_level_1 = 'KIDS'
	,new_revised_category_level_2 = 'KID ACCESSORIES'
	,new_revised_category_level_3 = 'OTHER KID ACCESSORIES'
	,new_category_final = 'OTHER KID ACCESSORIES'
	,new_type_final = 'OTHER KID ACCESSORIES'
WHERE mom_and_kid_product_flag = 1
AND new_type_final = 'KID BATHROOM ACCESSORIES'
AND (product_name LIKE '%��ҧ�Ǵ��%'
	OR product_name LIKE '%�ѡ���%'
	OR product_name LIKE '%��Ѻ��ҹ���%'
	OR product_name LIKE '%����һ�Ѻ%'
	OR product_name LIKE '%��%'
	OR product_name LIKE '������������')
;

--other kid acc2
UPDATE vpm_data.pm_skutagging_kids_product_master
SET new_revised_category_level_1 = 'KIDS'
	,new_revised_category_level_2 = 'KID ACCESSORIES'
	,new_revised_category_level_3 = 'OTHER KID ACCESSORIES'
	,new_category_final = 'OTHER KID ACCESSORIES'
	,new_type_final = 'OTHER KID ACCESSORIES'
WHERE mom_and_kid_product_flag = 1
AND new_type_final = 'KID BEDDING'
AND (product_name LIKE '%����� ORGANIC ENFANT%')
;

--fas acc
UPDATE vpm_data.pm_skutagging_kids_product_master
SET new_revised_category_level_1 = 'KIDS'
	,new_revised_category_level_2 = 'KID FASHION'
	,new_revised_category_level_3 = 'KID FASHION ACCESSORIES'
	,new_category_final = 'KID FASHION ACCESSORIES'
	,new_type_final = 'KID FASHION ACCESSORIES'
WHERE mom_and_kid_product_flag = 1
AND new_type_final = 'KID BEDDING'
AND product_name IN ('˹�ҡҡ͹������3','MITTEN')
;

--acc bath
UPDATE vpm_data.pm_skutagging_kids_product_master
SET new_revised_category_level_1 = 'KIDS'
	,new_revised_category_level_2 = 'KID ACCESSORIES'
	,new_revised_category_level_3 = 'KID BATHROOM ACCESSORIES'
	,new_category_final = 'KID BATHROOM ACCESSORIES'
	,new_type_final = 'KID BATHROOM ACCESSORIES'
WHERE mom_and_kid_product_flag = 1
AND new_type_final = 'OTHER KID ACCESSORIES'
AND product_name IN ('����ҧ��� HS NATURAL HAND GEL')
;

--new
--MATERNITY MILK PUMP MACHINE
UPDATE vpm_data.pm_skutagging_kids_product_master
SET new_type_final = 'MATERNITY MILK PUMP MACHINE'
WHERE mom_and_kid_product_flag = 1
AND new_type_final IS NULL 
AND product_name LIKE '������2������2���LAC �ǴPPSU'
;

--MATERNITY BAG
UPDATE vpm_data.pm_skutagging_kids_product_master
SET new_type_final = 'MATERNITY BAG'
WHERE mom_and_kid_product_flag = 1
AND new_type_final IS NULL 
AND product_name LIKE 'PICCOLO�����Ҥس���%'
;

--MATERNITY MILK STORAGE
UPDATE vpm_data.pm_skutagging_kids_product_master
SET new_type_final = 'MATERNITY MILK STORAGE'
WHERE mom_and_kid_product_flag = 1
AND new_type_final IS NULL 
AND product_name LIKE '%�ا�纹�ӹ�%'
;

--OTHER DIAPERS
UPDATE vpm_data.pm_skutagging_kids_product_master
SET new_type_final = 'OTHER DIAPERS'
WHERE mom_and_kid_product_flag = 1
AND new_type_final IS NULL 
AND product_name LIKE '%�������%'
;
--FEEDING ACCESSORIES
UPDATE vpm_data.pm_skutagging_kids_product_master
SET new_type_final = 'FEEDING ACCESSORIES'
WHERE mom_and_kid_product_flag = 1
AND new_type_final IS NULL 
AND product_name LIKE '%�������ҧ�Ǵ��%'
;

--MATERNITY CLOTHING
UPDATE vpm_data.pm_skutagging_kids_product_master
SET new_type_final = 'MATERNITY CLOTHING'
WHERE mom_and_kid_product_flag = 1
AND new_type_final IS NULL 
AND ((product_name LIKE '%MATERNITY%' AND class_name LIKE 'GARMENT')
	OR (dept_name LIKE 'GARMENT' AND subdept_name LIKE 'INNER WEAR'))
;

--KID DAYWEAR
UPDATE vpm_data.pm_skutagging_kids_product_master
SET new_type_final = 'KID DAYWEAR'
WHERE mom_and_kid_product_flag = 1
AND new_type_final IS NULL 
AND (subdept_name LIKE 'BABY''S WEAR'
	OR (dept_name LIKE 'CHILDREN''S FASHION')
	OR class_name LIKE 'FASHION')
;
--KID UNDERWEAR
UPDATE vpm_data.pm_skutagging_kids_product_master
SET new_type_final = 'KID UNDERWEAR'
WHERE mom_and_kid_product_flag = 1
AND new_type_final IS NULL 
AND class_name LIKE 'KIDS''S UNDERWEAR'
;

--KID BEDDING
UPDATE vpm_data.pm_skutagging_kids_product_master
SET new_type_final = 'KID BEDDING'
WHERE mom_and_kid_product_flag = 1
AND new_type_final IS NULL 
AND subclass_name LIKE 'INACTIVE / SLEEPING'
;
--OTHER TOYS
UPDATE vpm_data.pm_skutagging_kids_product_master
SET new_type_final = 'OTHER TOYS'
WHERE mom_and_kid_product_flag = 1
AND new_type_final IS NULL 
AND subclass_name LIKE 'INACTIVE / TOY''S'
;
--KID FASHION ACCESSORIES
UPDATE vpm_data.pm_skutagging_kids_product_master
SET new_type_final = 'KID FASHION ACCESSORIES'
WHERE mom_and_kid_product_flag = 1
AND new_type_final IS NULL 
AND subclass_name LIKE 'KIDS CLOTHING ACCESSORY'
;
--OTHER KID BOOK
UPDATE vpm_data.pm_skutagging_kids_product_master
SET new_type_final = 'OTHER KID BOOK'
WHERE mom_and_kid_product_flag = 1
AND new_type_final IS NULL 
AND subclass_name IN ('CHILDREN''S BOOKS','ACTIVITIES')
;

--KID UNDERWEAR
UPDATE vpm_data.pm_skutagging_kids_product_master
SET new_type_final = 'KID UNDERWEAR'
WHERE mom_and_kid_product_flag = 1
AND new_type_final IS NULL 
AND dept_name LIKE 'VARIETY LIFESTYLE'
;

--from diaper to underwear
UPDATE vpm_data.pm_skutagging_kids_product_master
SET new_type_final = 'KID UNDERWEAR'
WHERE mom_and_kid_product_flag = 1
AND new_type_final LIKE 'OTHER DIAPERS'
AND (product_name LIKE '�شᢹ���%'
	OR product_name LIKE '�ҧࡧ�����%'
	OR product_name LIKE '�ҧࡧ�%'
	OR product_name LIKE '%����%'
	OR product_name LIKE '�������������%'
	OR product_name LIKE '����ͺѧ�ç%'
	OR product_name LIKE '%BOXER%') 
;


-------------------------------------------------------LAST STEP: UPDATE NEW CATEGORY FINAL, CATEGORY LEVEL 1-3 BY TYPE FINAL
UPDATE vpm_data.pm_skutagging_kids_product_master
SET new_category_final = (CASE WHEN new_type_final LIKE 'PREGNANCY BOOK' THEN 'KID BOOKS'
							WHEN new_type_final LIKE 'PARENT GUIDE BOOK' THEN 'KID BOOKS'
							WHEN new_type_final LIKE 'COOK BOOK' THEN 'KID BOOKS'
							WHEN new_type_final LIKE 'SCHOOL BOOK' THEN 'KID BOOKS'
							WHEN new_type_final LIKE 'ACTIVITY BOOK' THEN 'KID BOOKS'
							WHEN new_type_final LIKE 'TALE BOOK' THEN 'KID BOOKS'
							WHEN new_type_final LIKE 'PRESCHOOL MATH BOOK' THEN 'KID BOOKS'
							WHEN new_type_final LIKE 'PRESCHOOL ENGLISH BOOK' THEN 'KID BOOKS'
							WHEN new_type_final LIKE 'PRESCHOOL THAI BOOK' THEN 'KID BOOKS'
							WHEN new_type_final LIKE 'OTHER KID BOOK' THEN 'KID BOOKS'
							WHEN new_type_final LIKE 'KID STATIONERY' THEN 'KID STATIONERY'
							WHEN new_type_final LIKE 'DIAPERS SIZE NB' THEN 'DIAPERS'
							WHEN new_type_final LIKE 'DIAPERS SIZE S' THEN 'DIAPERS'
							WHEN new_type_final LIKE 'DIAPERS SIZE M' THEN 'DIAPERS'
							WHEN new_type_final LIKE 'DIAPERS SIZE L' THEN 'DIAPERS'
							WHEN new_type_final LIKE 'DIAPERS SIZE XL' THEN 'DIAPERS'
							WHEN new_type_final LIKE 'OTHER DIAPERS' THEN 'DIAPERS'
							WHEN new_type_final LIKE 'MILK POWDER FORMULA 1' THEN 'MILK POWDER'
							WHEN new_type_final LIKE 'MILK POWDER FORMULA 2' THEN 'MILK POWDER'
							WHEN new_type_final LIKE 'MILK POWDER FORMULA 3' THEN 'MILK POWDER'
							WHEN new_type_final LIKE 'KID UHT MILK' THEN 'MILK POWDER'
							WHEN new_type_final LIKE 'OTHER MILK POWDER' THEN 'MILK POWDER'
							WHEN new_type_final LIKE 'STRAINED FOOD AND PUREE' THEN 'KID NOURISHMENT'
							WHEN new_type_final LIKE 'FINGER FOOD' THEN 'KID NOURISHMENT'
							WHEN new_type_final LIKE 'TABLE FOOD' THEN 'KID NOURISHMENT'
							WHEN new_type_final LIKE 'MATERNITY BAG' THEN 'MATERNITY FASHION'
							WHEN new_type_final LIKE 'MATERNITY CLOTHING' THEN 'MATERNITY FASHION'
							WHEN new_type_final LIKE 'MATERNITY LINGERIE' THEN 'MATERNITY FASHION'
							WHEN new_type_final LIKE 'MATERNITY BREAST PADS' THEN 'MATERNITY BREAST FEEDING'
							WHEN new_type_final LIKE 'MATERNITY MILK PUMP MACHINE' THEN 'MATERNITY BREAST FEEDING'
							WHEN new_type_final LIKE 'MATERNITY MILK STORAGE' THEN 'MATERNITY BREAST FEEDING'
							WHEN new_type_final LIKE 'MATERNITY BELT' THEN 'MATERNITY SELF-CARE'
							WHEN new_type_final LIKE 'MATERNITY PILLOW' THEN 'MATERNITY SELF-CARE'
							WHEN new_type_final LIKE 'MATERNITY SUPPLEMENT' THEN 'MATERNITY SELF-CARE'
							WHEN new_type_final LIKE 'MATERNITY TREATMENT' THEN 'MATERNITY SELF-CARE'
							WHEN new_type_final LIKE 'OTHER MATERNITY' THEN 'MATERNITY SELF-CARE'
							WHEN new_type_final LIKE 'KID DAYWEAR' THEN 'KID CLOTHING'
							WHEN new_type_final LIKE 'KID NIGHTWEAR' THEN 'KID CLOTHING'
							WHEN new_type_final LIKE 'KID UNDERWEAR' THEN 'KID CLOTHING'
							WHEN new_type_final LIKE 'KID SWIMWEAR' THEN 'KID CLOTHING'
							WHEN new_type_final LIKE 'KID SHOES' THEN 'KID SHOES'
							WHEN new_type_final LIKE 'KID FASHION ACCESSORIES' THEN 'KID FASHION ACCESSORIES'
							WHEN new_type_final LIKE 'FEEDING BOTTLE AND NIPPLE' THEN 'FEEDING BOTTLE AND ACCESSORIES'
							WHEN new_type_final LIKE 'FEEDING ACCESSORIES' THEN 'FEEDING BOTTLE AND ACCESSORIES'
							WHEN new_type_final LIKE 'BOTTLE WARMER' THEN 'FEEDING BOTTLE AND ACCESSORIES'
							WHEN new_type_final LIKE 'WEANING' THEN 'FEEDING BOTTLE AND ACCESSORIES'
							WHEN new_type_final LIKE 'KID BATHROOM ACCESSORIES' THEN 'KID BATHROOM ACCESSORIES'
							WHEN new_type_final LIKE 'KID FABRIC' THEN 'OTHER KID ACCESSORIES'
							WHEN new_type_final LIKE 'OTHER KID ACCESSORIES' THEN 'OTHER KID ACCESSORIES'
							WHEN new_type_final LIKE 'STROLLER' THEN 'KID CAR SEAT AND TRAVEL GEAR'
							WHEN new_type_final LIKE 'BABY CARRIER' THEN 'KID CAR SEAT AND TRAVEL GEAR'
							WHEN new_type_final LIKE 'CAR SEAT' THEN 'KID CAR SEAT AND TRAVEL GEAR'
							WHEN new_type_final LIKE 'KID BEDDING' THEN 'KID BEDDING'
							WHEN new_type_final LIKE 'OTHER KID FURNITURE' THEN 'OTHER KID FURNITURE'
							WHEN new_type_final LIKE 'BABY BOUNCER' THEN 'TOYS'
							WHEN new_type_final LIKE 'BABY WALKER' THEN 'TOYS'
							WHEN new_type_final LIKE 'OTHER TOYS' THEN 'TOYS'
							ELSE new_category_final END)
	,new_revised_category_level_3 = (CASE WHEN new_type_final LIKE 'PREGNANCY BOOK' THEN 'KID BOOKS & STATIONERY'
											WHEN new_type_final LIKE 'PARENT GUIDE BOOK' THEN 'KID BOOKS & STATIONERY'
											WHEN new_type_final LIKE 'COOK BOOK' THEN 'KID BOOKS & STATIONERY'
											WHEN new_type_final LIKE 'SCHOOL BOOK' THEN 'KID BOOKS & STATIONERY'
											WHEN new_type_final LIKE 'ACTIVITY BOOK' THEN 'KID BOOKS & STATIONERY'
											WHEN new_type_final LIKE 'TALE BOOK' THEN 'KID BOOKS & STATIONERY'
											WHEN new_type_final LIKE 'PRESCHOOL MATH BOOK' THEN 'KID BOOKS & STATIONERY'
											WHEN new_type_final LIKE 'PRESCHOOL ENGLISH BOOK' THEN 'KID BOOKS & STATIONERY'
											WHEN new_type_final LIKE 'PRESCHOOL THAI BOOK' THEN 'KID BOOKS & STATIONERY'
											WHEN new_type_final LIKE 'OTHER KID BOOK' THEN 'KID BOOKS & STATIONERY'
											WHEN new_type_final LIKE 'KID STATIONERY' THEN 'KID BOOKS & STATIONERY'
											WHEN new_type_final LIKE 'DIAPERS SIZE NB' THEN 'DIAPERS'
											WHEN new_type_final LIKE 'DIAPERS SIZE S' THEN 'DIAPERS'
											WHEN new_type_final LIKE 'DIAPERS SIZE M' THEN 'DIAPERS'
											WHEN new_type_final LIKE 'DIAPERS SIZE L' THEN 'DIAPERS'
											WHEN new_type_final LIKE 'DIAPERS SIZE XL' THEN 'DIAPERS'
											WHEN new_type_final LIKE 'OTHER DIAPERS' THEN 'DIAPERS'
											WHEN new_type_final LIKE 'MILK POWDER FORMULA 1' THEN 'MILK POWDER'
											WHEN new_type_final LIKE 'MILK POWDER FORMULA 2' THEN 'MILK POWDER'
											WHEN new_type_final LIKE 'MILK POWDER FORMULA 3' THEN 'MILK POWDER'
											WHEN new_type_final LIKE 'KID UHT MILK' THEN 'MILK POWDER'
											WHEN new_type_final LIKE 'OTHER MILK POWDER' THEN 'MILK POWDER'
											WHEN new_type_final LIKE 'STRAINED FOOD AND PUREE' THEN 'KID NOURISHMENT'
											WHEN new_type_final LIKE 'FINGER FOOD' THEN 'KID NOURISHMENT'
											WHEN new_type_final LIKE 'TABLE FOOD' THEN 'KID NOURISHMENT'
											WHEN new_type_final LIKE 'MATERNITY BAG' THEN 'MATERNITY'
											WHEN new_type_final LIKE 'MATERNITY CLOTHING' THEN 'MATERNITY'
											WHEN new_type_final LIKE 'MATERNITY LINGERIE' THEN 'MATERNITY'
											WHEN new_type_final LIKE 'MATERNITY BREAST PADS' THEN 'MATERNITY'
											WHEN new_type_final LIKE 'MATERNITY MILK PUMP MACHINE' THEN 'MATERNITY'
											WHEN new_type_final LIKE 'MATERNITY MILK STORAGE' THEN 'MATERNITY'
											WHEN new_type_final LIKE 'MATERNITY BELT' THEN 'MATERNITY'
											WHEN new_type_final LIKE 'MATERNITY PILLOW' THEN 'MATERNITY'
											WHEN new_type_final LIKE 'MATERNITY SUPPLEMENT' THEN 'MATERNITY'
											WHEN new_type_final LIKE 'MATERNITY TREATMENT' THEN 'MATERNITY'
											WHEN new_type_final LIKE 'OTHER MATERNITY' THEN 'MATERNITY'
											WHEN new_type_final LIKE 'KID DAYWEAR' THEN 'KID CLOTHING'
											WHEN new_type_final LIKE 'KID NIGHTWEAR' THEN 'KID CLOTHING'
											WHEN new_type_final LIKE 'KID UNDERWEAR' THEN 'KID CLOTHING'
											WHEN new_type_final LIKE 'KID SWIMWEAR' THEN 'KID CLOTHING'
											WHEN new_type_final LIKE 'KID SHOES' THEN 'KID SHOES'
											WHEN new_type_final LIKE 'KID FASHION ACCESSORIES' THEN 'KID FASHION ACCESSORIES'
											WHEN new_type_final LIKE 'FEEDING BOTTLE AND NIPPLE' THEN 'FEEDING BOTTLE AND ACCESSORIES'
											WHEN new_type_final LIKE 'FEEDING ACCESSORIES' THEN 'FEEDING BOTTLE AND ACCESSORIES'
											WHEN new_type_final LIKE 'BOTTLE WARMER' THEN 'FEEDING BOTTLE AND ACCESSORIES'
											WHEN new_type_final LIKE 'WEANING' THEN 'FEEDING BOTTLE AND ACCESSORIES'
											WHEN new_type_final LIKE 'KID BATHROOM ACCESSORIES' THEN 'KID BATHROOM ACCESSORIES'
											WHEN new_type_final LIKE 'KID FABRIC' THEN 'OTHER KID ACCESSORIES'
											WHEN new_type_final LIKE 'OTHER KID ACCESSORIES' THEN 'OTHER KID ACCESSORIES'
											WHEN new_type_final LIKE 'STROLLER' THEN 'KID CAR SEAT AND TRAVEL GEAR'
											WHEN new_type_final LIKE 'BABY CARRIER' THEN 'KID CAR SEAT AND TRAVEL GEAR'
											WHEN new_type_final LIKE 'CAR SEAT' THEN 'KID CAR SEAT AND TRAVEL GEAR'
											WHEN new_type_final LIKE 'KID BEDDING' THEN 'KID BEDDING'
											WHEN new_type_final LIKE 'OTHER KID FURNITURE' THEN 'OTHER KID FURNITURE'
											WHEN new_type_final LIKE 'BABY BOUNCER' THEN 'TOYS'
											WHEN new_type_final LIKE 'BABY WALKER' THEN 'TOYS'
											WHEN new_type_final LIKE 'OTHER TOYS' THEN 'TOYS'
											ELSE new_revised_category_level_3 END)
	,new_revised_category_level_2 = (CASE WHEN new_type_final LIKE 'PREGNANCY BOOK' THEN 'KID BOOKS & STATIONERY'
										WHEN new_type_final LIKE 'PARENT GUIDE BOOK' THEN 'KID BOOKS & STATIONERY'
										WHEN new_type_final LIKE 'COOK BOOK' THEN 'KID BOOKS & STATIONERY'
										WHEN new_type_final LIKE 'SCHOOL BOOK' THEN 'KID BOOKS & STATIONERY'
										WHEN new_type_final LIKE 'ACTIVITY BOOK' THEN 'KID BOOKS & STATIONERY'
										WHEN new_type_final LIKE 'TALE BOOK' THEN 'KID BOOKS & STATIONERY'
										WHEN new_type_final LIKE 'PRESCHOOL MATH BOOK' THEN 'KID BOOKS & STATIONERY'
										WHEN new_type_final LIKE 'PRESCHOOL ENGLISH BOOK' THEN 'KID BOOKS & STATIONERY'
										WHEN new_type_final LIKE 'PRESCHOOL THAI BOOK' THEN 'KID BOOKS & STATIONERY'
										WHEN new_type_final LIKE 'OTHER KID BOOK' THEN 'KID BOOKS & STATIONERY'
										WHEN new_type_final LIKE 'KID STATIONERY' THEN 'KID BOOKS & STATIONERY'
										WHEN new_type_final LIKE 'DIAPERS SIZE NB' THEN 'DIAPERS'
										WHEN new_type_final LIKE 'DIAPERS SIZE S' THEN 'DIAPERS'
										WHEN new_type_final LIKE 'DIAPERS SIZE M' THEN 'DIAPERS'
										WHEN new_type_final LIKE 'DIAPERS SIZE L' THEN 'DIAPERS'
										WHEN new_type_final LIKE 'DIAPERS SIZE XL' THEN 'DIAPERS'
										WHEN new_type_final LIKE 'OTHER DIAPERS' THEN 'DIAPERS'
										WHEN new_type_final LIKE 'MILK POWDER FORMULA 1' THEN 'KID FOOD'
										WHEN new_type_final LIKE 'MILK POWDER FORMULA 2' THEN 'KID FOOD'
										WHEN new_type_final LIKE 'MILK POWDER FORMULA 3' THEN 'KID FOOD'
										WHEN new_type_final LIKE 'KID UHT MILK' THEN 'KID FOOD'
										WHEN new_type_final LIKE 'OTHER MILK POWDER' THEN 'KID FOOD'
										WHEN new_type_final LIKE 'STRAINED FOOD AND PUREE' THEN 'KID FOOD'
										WHEN new_type_final LIKE 'FINGER FOOD' THEN 'KID FOOD'
										WHEN new_type_final LIKE 'TABLE FOOD' THEN 'KID FOOD'
										WHEN new_type_final LIKE 'MATERNITY BAG' THEN 'MATERNITY'
										WHEN new_type_final LIKE 'MATERNITY CLOTHING' THEN 'MATERNITY'
										WHEN new_type_final LIKE 'MATERNITY LINGERIE' THEN 'MATERNITY'
										WHEN new_type_final LIKE 'MATERNITY BREAST PADS' THEN 'MATERNITY'
										WHEN new_type_final LIKE 'MATERNITY MILK PUMP MACHINE' THEN 'MATERNITY'
										WHEN new_type_final LIKE 'MATERNITY MILK STORAGE' THEN 'MATERNITY'
										WHEN new_type_final LIKE 'MATERNITY BELT' THEN 'MATERNITY'
										WHEN new_type_final LIKE 'MATERNITY PILLOW' THEN 'MATERNITY'
										WHEN new_type_final LIKE 'MATERNITY SUPPLEMENT' THEN 'MATERNITY'
										WHEN new_type_final LIKE 'MATERNITY TREATMENT' THEN 'MATERNITY'
										WHEN new_type_final LIKE 'OTHER MATERNITY' THEN 'MATERNITY'
										WHEN new_type_final LIKE 'KID DAYWEAR' THEN 'KID FASHION'
										WHEN new_type_final LIKE 'KID NIGHTWEAR' THEN 'KID FASHION'
										WHEN new_type_final LIKE 'KID UNDERWEAR' THEN 'KID FASHION'
										WHEN new_type_final LIKE 'KID SWIMWEAR' THEN 'KID FASHION'
										WHEN new_type_final LIKE 'KID SHOES' THEN 'KID FASHION'
										WHEN new_type_final LIKE 'KID FASHION ACCESSORIES' THEN 'KID FASHION'
										WHEN new_type_final LIKE 'FEEDING BOTTLE AND NIPPLE' THEN 'KID ACCESSORIES'
										WHEN new_type_final LIKE 'FEEDING ACCESSORIES' THEN 'KID ACCESSORIES'
										WHEN new_type_final LIKE 'BOTTLE WARMER' THEN 'KID ACCESSORIES'
										WHEN new_type_final LIKE 'WEANING' THEN 'KID ACCESSORIES'
										WHEN new_type_final LIKE 'KID BATHROOM ACCESSORIES' THEN 'KID ACCESSORIES'
										WHEN new_type_final LIKE 'KID FABRIC' THEN 'KID ACCESSORIES'
										WHEN new_type_final LIKE 'OTHER KID ACCESSORIES' THEN 'KID ACCESSORIES'
										WHEN new_type_final LIKE 'STROLLER' THEN 'KID CAR SEAT AND TRAVEL GEAR'
										WHEN new_type_final LIKE 'BABY CARRIER' THEN 'KID CAR SEAT AND TRAVEL GEAR'
										WHEN new_type_final LIKE 'CAR SEAT' THEN 'KID CAR SEAT AND TRAVEL GEAR'
										WHEN new_type_final LIKE 'KID BEDDING' THEN 'KID FURNITURE'
										WHEN new_type_final LIKE 'OTHER KID FURNITURE' THEN 'KID FURNITURE'
										WHEN new_type_final LIKE 'BABY BOUNCER' THEN 'TOYS'
										WHEN new_type_final LIKE 'BABY WALKER' THEN 'TOYS'
										WHEN new_type_final LIKE 'OTHER TOYS' THEN 'TOYS'
										ELSE new_revised_category_level_2 END)
	,new_revised_category_level_1 = 'KIDS'
WHERE mom_and_kid_product_flag = 1
AND new_type_final IS NOT NULL
;

UPDATE vpm_data.pm_skutagging_kids_product_master A
SET new_revised_category_level_1 = revised_category_level_1
	,new_revised_category_level_2 = revised_category_level_2
	,new_revised_category_level_3 = revised_category_level_3
	,new_category_final = category_final
	,new_type_final = type_final
where mom_and_kid_product_flag = 0
;


--create table vpm_data.momkidsproj_mom_and_kid_product_master to send out
DROP TABLE IF EXISTS vpm_data.momkidsproj_mom_and_kid_product_master
;
CREATE TABLE vpm_data.momkidsproj_mom_and_kid_product_master AS
SELECT *
FROM vpm_data.pm_skutagging_kids_product_master
;

UPDATE vpm_data.pm_skutagging_kids_product_master
SET revised_category_level_1 = NULL
	,revised_category_level_2 = NULL
	,revised_category_level_3 = NULL
	,category_final = NULL
	,type_final = NULL
;

DELETE FROM vpm_data.pm_skutagging_kids_product_master
WHERE mom_and_kid_product_flag = 0
;

UPDATE vpm_data.pm_skutagging_kids_product_master
SET kids_stage_final = (CASE WHEN INFANT_FLAG = 'YES' AND TODDLER_FLAG = 'YES' AND PRESCHOOL_FLAG = 'YES' AND JUNIOR_FLAG = 'YES' AND TEEN_FLAG = 'YES' THEN 'INFANT,TODDLER,PRESCHOOLER,JUNIOR,TEENAGER'
							 WHEN INFANT_FLAG = 'YES' AND TODDLER_FLAG = 'YES' AND PRESCHOOL_FLAG = 'YES' AND JUNIOR_FLAG = 'YES' THEN 'INFANT,TODDLER,PRESCHOOLER,JUNIOR'
							 WHEN INFANT_FLAG = 'YES' AND TODDLER_FLAG = 'YES' AND PRESCHOOL_FLAG = 'YES' THEN 'NEWBORN,INFANT,TODDLER,PRESCHOOLER'
							 WHEN INFANT_FLAG = 'YES' AND TODDLER_FLAG = 'YES' THEN 'NEWBORN,INFANT,TODDLER'
							 WHEN INFANT_FLAG = 'YES' THEN 'NEWBORN,INFANT'
							 WHEN TODDLER_FLAG = 'YES' AND PRESCHOOL_FLAG = 'YES' AND JUNIOR_FLAG = 'YES' AND TEEN_FLAG = 'YES' THEN 'TODDLER,PRESCHOOLER,JUNIOR,TEENAGER'
							 WHEN TODDLER_FLAG = 'YES' AND PRESCHOOL_FLAG = 'YES' AND JUNIOR_FLAG = 'YES' THEN 'TODDLER,PRESCHOOLER,JUNIOR'
							 WHEN TODDLER_FLAG = 'YES' AND PRESCHOOL_FLAG = 'YES' THEN 'TODDLER,PRESCHOOLER'
							 WHEN TODDLER_FLAG = 'YES' THEN 'TODDLER'
							 WHEN PRESCHOOL_FLAG = 'YES' AND JUNIOR_FLAG = 'YES' AND TEEN_FLAG = 'YES' THEN 'PRESCHOOLER,JUNIOR,TEENAGER'
							 WHEN PRESCHOOL_FLAG = 'YES' AND JUNIOR_FLAG = 'YES' THEN 'PRESCHOOLER,JUNIOR'
							 WHEN PRESCHOOL_FLAG = 'YES' THEN 'PRESCHOOLER'
							 WHEN JUNIOR_FLAG = 'YES' AND TEEN_FLAG = 'YES' THEN 'JUNIOR,TEENAGER'
							 WHEN JUNIOR_FLAG = 'YES' THEN 'JUNIOR'
							 WHEN TEEN_FLAG = 'YES' THEN 'TEENAGER'
							 END)
;

--CREATE TEMP TO DELETE THE DUPLICATE PRODUCTS

DELETE FROM VPM_DATA.PM_SKUTAGGING_KIDS_PRODUCT_MASTER
WHERE EXISTS (SELECT * FROM 
				(SELECT *, ROW_NUMBER () OVER(PARTITION BY PARTNER_CODE,SKU_CODE ORDER BY PARTNER_CODE NULLS LAST) AS RN
    			FROM VPM_DATA.PM_SKUTAGGING_KIDS_PRODUCT_MASTER) B
    			WHERE RN > 1 AND VPM_DATA.PM_SKUTAGGING_KIDS_PRODUCT_MASTER.sku_code = B.SKU_CODE)
;


DELETE FROM VPM_DATA.PM_SKUTAGGING_KIDS_PRODUCT_MASTER
WHERE SKU_CODE IN ('10000085990','10000088581');

--UPDATE FROM OLD TABLE
UPDATE VPM_DATA.PM_SKUTAGGING_KIDS_PRODUCT_MASTER
SET CATEGORY_FINAL  = UPPER(CASE WHEN A.CATEGORY_FINAL IS NOT NULL THEN A.CATEGORY_FINAL ELSE VPM_DATA.PM_SKUTAGGING_KIDS_PRODUCT_MASTER.CATEGORY_FINAL END)
	,TYPE_FINAL = UPPER(A.TYPE_FINAL)
	,KID_GENDER_FINAL = UPPER(A.KID_GENDER_FINAL)
	,KIDS_STAGE_FINAL = (CASE WHEN vpm_data.pm_skutagging_kids_product_master.KIDS_STAGE_FINAL IS NOT NULL THEN vpm_data.pm_skutagging_kids_product_master.KIDS_STAGE_FINAL ELSE UPPER(A.KIDS_STAGE_FINAL) END)
FROM VPM_DATA.pm_skutagging_kids_product_master_previous A --CHANGE
WHERE A.PARTNER_CODE
= VPM_DATA.PM_SKUTAGGING_KIDS_PRODUCT_MASTER.PARTNER_CODE
AND A.SKU_CODE = VPM_DATA.PM_SKUTAGGING_KIDS_PRODUCT_MASTER.SKU_CODE;


/************************************************** TYPE FINAL **************************************************/
/*KEY WORD*/


/** UPDATE TYPE FINAL BY PRODUCT **/

UPDATE VPM_DATA.PM_SKUTAGGING_KIDS_PRODUCT_MASTER
SET TYPE_FINAL = CASE WHEN UPPER(SUBCLASS_NAME) = 'NEWBORN' AND UPPER(TRIM(CLEANED_BRANDNAME)) = 'BABY DEE' THEN 'UNDERWEAR'
						WHEN UPPER(TRIM(CLEANED_BRANDNAME)) IN ('PUREEN','PIGEON') AND UPPER(SUBCLASS_NAME) = 'FEEDING' THEN 'FEEDING BOTTLE AND NIPPLE'
						WHEN UPPER(SUBCLASS_NAME) = 'ACCESSORIES' THEN 'ACCESSORIES'	
						WHEN UPPER(SUBCLASS_NAME) IN ('BOTTOMS','TOPS','ONE PIECE') THEN 'DAYWEAR'
						WHEN UPPER(CLASS_NAME) IN ('BOTTOM','TOP','OUTERWEAR','OUTER WEAR','DRESS / JUMPSUIT','DRESS','BODY SUIT') THEN 'DAYWEAR' --CDS/RBS REVISED PRODUCT MASTER
						WHEN UPPER(TRIM(CLEANED_BRANDNAME)) = 'CAMERA THAI' AND UPPER(SUBCLASS_NAME) = 'OTHER' THEN 'BABY CRADLE'
						WHEN UPPER(TRIM(CLEANED_BRANDNAME)) = 'BABI MILD' AND UPPER(SUBCLASS_NAME) = 'TOILETRY' THEN 'PERSONAL CARE'
						WHEN UPPER(TRIM(CLEANED_BRANDNAME)) = 'FOREMOST' THEN 'UHT MILK'
						WHEN UPPER(SUBCLASS_NAME) = 'WEANING' THEN 'WEANING'
						WHEN UPPER(SUBCLASS_NAME) = 'FABRIC' THEN 'FABRIC'
						WHEN UPPER(TRIM(PRODUCT_NAME)) LIKE '%���͹%' THEN 'BEDDING'
						WHEN UPPER(TRIM(PRODUCT_NAME)) LIKE '%��͹%' THEN 'BEDDING'
						--WHEN UPPER(CLASS_NAME) = 'BEDDING' THEN 'BEDDING'
						WHEN UPPER(TRIM(CLEANED_BRANDNAME)) = 'CROCS' THEN 'FOOTWEAR' 
						WHEN UPPER(SUBDEPT_NAME) = 'TOYS' THEN 'TOY'
						WHEN UPPER(SUBCLASS_NAME) = 'BABY TOYS' THEN 'TOY' --CDS/RBS REVISED PRODUCT MASTER
						WHEN UPPER(CLASS_NAME) LIKE '%TOY%' THEN 'TOY'
						WHEN UPPER(SUBCLASS_NAME) LIKE '%TOY%' THEN 'TOY'
						WHEN UPPER(CLASS_NAME) = 'NEW BORN/BOYS' AND UPPER(SUBCLASS_NAME) IN ('SHIRT','RTW BOXSET') THEN 'DAYWEAR'
						--WHEN CATEGORY = 'BOOKS' THEN 'BOOK'
						--WHEN CATEGORY = 'TOYS' THEN 'TOY'
						WHEN UPPER(CLASS_NAME) = 'T-SHIRTS' THEN 'DAYWEAR'
						WHEN UPPER(CLASS_NAME) IN ('BLOUSE','VEST/JACKET/SWEATER') THEN 'DAYWEAR'
						WHEN UPPER(CLASS_NAME) = 'UNDERWEAR' THEN 'UNDERWEAR'
						WHEN UPPER(CLASS_NAME) = 'SHORT PANTS' THEN 'DAYWEAR'
						WHEN UPPER(SUBCLASS_NAME) IN ('B S/S TEE') THEN 'DAYWEAR'
						WHEN UPPER(SUBCLASS_NAME) = 'UNDERWEAR' THEN 'UNDERWEAR'
						WHEN UPPER(CLASS_NAME) = 'KIDS S UNDERWEAR' THEN 'UNDERWEAR' --CDS/RBS REVISED PRODUCT MASTER
						WHEN UPPER(CLASS_NAME) IN ('NIGHTWEAR','PYJAMAS/SWIMWEAR') THEN 'NIGHTWEAR'
						WHEN UPPER(CLASS_NAME) IN ('PYJAMAS') THEN 'NIGHTWEAR' --CDS/RBS REVISED PRODUCT MASTER
						WHEN UPPER(CLASS_NAME) = 'SWIMWEAR' THEN 'SWIMWEAR'
						WHEN UPPER(CLASS_NAME) IN ('BOYS SHOES','GIRLS SHOES','CASUAL') THEN 'FOOTWEAR'
						WHEN UPPER(SUBDEPT_NAME) = 'KID S SHOES' THEN 'FOOTWEAR' --CDS/RBS REVISED PRODUCT MASTER
						WHEN UPPER(CLASS_NAME) = 'KIDS ART & CRAFT' THEN 'TOY'
						WHEN UPPER(CLASS_NAME) = 'FEEDING' THEN 'FEEDING BOTTLE AND NIPPLE'
						WHEN UPPER(CLASS_NAME) = 'GIFT SET' THEN 'GIFT SET'
						WHEN UPPER(CLASS_NAME) = 'BABY FOOD' THEN 'NURISHMENT'
						WHEN UPPER(CLASS_NAME) = 'BABY DIAPERS' THEN 'DIAPERS'
						WHEN UPPER(CLASS_NAME) = 'DIAPERS' THEN 'DIAPERS' --CDS/RBS REVISED PRODUCT MASTER
						WHEN UPPER(CLASS_NAME) IN ('CHILDRENS EDUCATIONAL MAT','CHILDRENS GENERAL NON-FIC','TEENAGE FICTION & TRUE','CHILDRENS PICTURE BOOKS','CHILDRENS GENERAL NON-FIC','CHILDRENS FICTION&TRUE') THEN 'BOOK'		
						WHEN UPPER(SUBCLASS_NAME) = 'FEEDING' THEN 'FEEDING BOTTLE AND NIPPLE'
						WHEN UPPER(SUBCLASS_NAME) = 'UHT MILK' THEN 'UHT MILK'
						WHEN UPPER(SUBCLASS_NAME) = 'PREGNANCY' THEN 'PREGNANCY BOOK'
						WHEN UPPER(SUBCLASS_NAME) = 'CHILDCARE' THEN 'CHILDCARE BOOK'
						WHEN UPPER(SUBCLASS_NAME) = 'BABY NAMES' THEN 'BABY NAME BOOK'
						WHEN UPPER(SUBCLASS_NAME) = 'BABY FOOD' AND UPPER(DEPT_NAME) NOT LIKE '%BOOK%' THEN 'NURISHMENT'
						WHEN UPPER(CLASS_NAME) = 'ACTIVITY' THEN 'ACTIVITY BOOK'
						WHEN UPPER(CLASS_NAME) = 'TALE' THEN 'TALE BOOK'
						WHEN UPPER(SUBDEPT_NAME) IN ('MEDIA EDUCATION AND EDUTA',
											'CHILDREN',
											'GAME & BUILDING',
											'EXAMINATION GUIDE',
											'EDUCATION TOY',
											'SPECIAL BOOK(EVENT)',
											'CHILDRENS TEENAGE&EDUCATI') THEN 'EDUCATION MATERIAL'
						WHEN UPPER(CLASS_NAME) = 'EDUCATION' THEN 'EDUCATION MATERIAL' --CDS/RBS REVISED PRODUCT MASTER
						WHEN UPPER(DEPT_NAME) = 'CASUAL FOOTWEAR' THEN 'FOOTWEAR'
						WHEN UPPER(SUBCLASS_NAME) = 'BEDDING' THEN 'BEDDING'
						WHEN UPPER(DEPT_NAME) = 'STATIONERY' THEN 'STATIONERY'
						WHEN UPPER(SUBCLASS_NAME) = 'STATIONERY' THEN 'STATIONERY'
						WHEN UPPER(CLASS_NAME) = 'STATIONERY' THEN 'STATIONERY'
						WHEN UPPER(DEPT_NAME) = 'CHILDRENS WEAR' THEN 'DAYWEAR'
						WHEN UPPER(SUBCLASS_NAME) IN ('NEWBORN') THEN 'DAYWEAR'
						WHEN UPPER(SUBCLASS_NAME) LIKE '%SHIRT%' THEN 'DAYWEAR'
						WHEN UPPER(SUBCLASS_NAME) LIKE '%APPAREL%' THEN 'DAYWEAR'
						WHEN UPPER(SUBCLASS_NAME) LIKE '%PANTS%' THEN 'DAYWEAR'
						WHEN UPPER(SUBCLASS_NAME) LIKE '%SLEEPSUIT%' THEN 'DAYWEAR'
						WHEN UPPER(SUBCLASS_NAME) LIKE '%PYJAMA%' THEN 'DAYWEAR'
						WHEN UPPER(SUBCLASS_NAME) LIKE '%DRESS%' THEN 'DAYWEAR'
						WHEN UPPER(SUBCLASS_NAME) LIKE '%SHORT%' THEN 'DAYWEAR'
						WHEN UPPER(SUBCLASS_NAME) LIKE '%BODYSUITS%' THEN 'DAYWEAR'
						WHEN UPPER(SUBCLASS_NAME) LIKE '%WALKER%' THEN 'BABY WALKER'
						WHEN UPPER(SUBCLASS_NAME) LIKE '%BOTTOMS%' THEN 'DAYWEAR'
						WHEN UPPER(SUBCLASS_NAME) LIKE '%13_SETS%' THEN 'DAYWEAR'
						WHEN UPPER(SUBCLASS_NAME) LIKE '%LEGGING%' THEN 'DAYWEAR'
						WHEN UPPER(SUBCLASS_NAME) LIKE '%BLOUSE%' THEN 'DAYWEAR'
						WHEN UPPER(SUBCLASS_NAME) LIKE '%ROMPER%' THEN 'DAYWEAR'
						WHEN UPPER(SUBCLASS_NAME) LIKE '%FEEDING ACCESS%' THEN 'FEEDING ACCESSORIES'
						WHEN UPPER(SUBCLASS_NAME) LIKE '%TOP%' THEN 'DAYWEAR'
						WHEN UPPER(SUBCLASS_NAME) LIKE '%TANK%' THEN 'DAYWEAR'
						WHEN UPPER(SUBCLASS_NAME) LIKE '%SHOE%' THEN 'FOOTWEAR'
						WHEN UPPER(SUBCLASS_NAME) LIKE '%SANDAL%' THEN 'FOOTWEAR'
						WHEN UPPER(SUBCLASS_NAME) LIKE '%SKIRT%' THEN 'DAYWEAR'
						WHEN UPPER(SUBCLASS_NAME) LIKE '%MILK POWDER%' THEN 'MILK POWDER'
						ELSE UPPER(TRIM(TYPE_FINAL)) END
;
UPDATE VPM_DATA.PM_SKUTAGGING_KIDS_PRODUCT_MASTER
SET TYPE_FINAL = CASE WHEN UPPER(SUBCLASS_NAME) = 'TOYS/DOLLS' THEN 'TOY'
					WHEN UPPER(SUBCLASS_NAME) = 'SKIRTS' THEN 'DAYWEAR'
					WHEN UPPER(TRIM(PRODUCT_NAME)) LIKE '����ͼ��%' THEN 'DAYWEAR'
					WHEN UPPER(SUBCLASS_NAME) = 'FOOTWEAR' THEN 'FOOTWEAR'
					WHEN UPPER(TRIM(PRODUCT_NAME)) LIKE '%�ͧ���%' THEN 'FOOTWEAR'
					WHEN UPPER(DEPT_NAME) NOT LIKE '%BOOK%' AND UPPER(TRIM(PRODUCT_NAME)) LIKE '%�Һ���%' THEN 'BATHING'
					WHEN UPPER(TRIM(PRODUCT_NAME)) LIKE '%TOOTHBRUSH%' OR UPPER(TRIM(PRODUCT_NAME)) LIKE '%�ç�տѹ%' OR UPPER(TRIM(PRODUCT_NAME)) LIKE '%��ҧ�Һ���%' OR UPPER(TRIM(PRODUCT_NAME)) LIKE '%ʺ��%' THEN 'BATHING'
					WHEN UPPER(TRIM(CLEANED_BRANDNAME)) = 'PUREEN' AND UPPER(SUBCLASS_NAME) = 'BEDDING/CLOTHING' AND UPPER(TRIM(PRODUCT_NAME)) LIKE '%PILOW%' THEN 'BEDDING'
					WHEN UPPER(TRIM(CLEANED_BRANDNAME)) = 'ENFALAC' THEN 'MILK POWDER'
					WHEN UPPER(TRIM(PRODUCT_NAME)) LIKE '%����ҧ%' THEN 'BEDDING'
					WHEN UPPER(TRIM(PRODUCT_NAME)) LIKE '%��駵С��Ҥ���շ%' OR UPPER(TRIM(PRODUCT_NAME)) LIKE '%�����§����駾��%' THEN 'ACCESSORIES'
					WHEN UPPER(SUBCLASS_NAME) = 'WALKER' THEN 'BABY WALKER'
					WHEN UPPER(TRIM(PRODUCT_NAME)) LIKE '%BABY WALKER%' OR UPPER(TRIM(PRODUCT_NAME)) LIKE '%CAR WALKER%' THEN 'BABY WALKER'
					WHEN UPPER(SUBCLASS_NAME) = 'MATERNITY' THEN 'MATERNITY'
					WHEN UPPER(CLASS_NAME) = 'MATERNITY' THEN 'MATERNITY' --CDS/RBS REVISED PRODUCT MASTER
					WHEN UPPER(TRIM(PRODUCT_NAME)) LIKE '%��͹�س����͹�����ѧ��ʹ���%' THEN 'MATERNITY'
					WHEN UPPER(SUBCLASS_NAME) = '%��Ѻ��ҹ���%' OR UPPER(SUBCLASS_NAME) = '%����ҫѡ���%'THEN 'WASHING'
					WHEN UPPER(SUBCLASS_NAME) = 'WASHING' THEN 'WASHING' --CDS/RBS REVISED PRODUCT MASTER
					WHEN UPPER(TRIM(PRODUCT_NAME)) LIKE '%�����%' OR UPPER(TRIM(PRODUCT_NAME)) LIKE '%�Ū��%' THEN 'PERSONAL CARE'
					WHEN UPPER(TRIM(PRODUCT_NAME)) LIKE '%�ش�ͧ��ѭ%' THEN 'GIFT SET'
					WHEN UPPER(SUBCLASS_NAME) = 'GIFT SET' THEN 'GIFT SET'
					WHEN UPPER(TRIM(CLEANED_BRANDNAME)) = 'FOREMOST' THEN 'UHT MILK'
					WHEN UPPER(TRIM(PRODUCT_NAME)) LIKE '%�ͧ���%' AND UPPER(SUBCLASS_NAME) LIKE '%TALE%' THEN 'TALE BOOK'
					WHEN UPPER(TRIM(PRODUCT_NAME)) LIKE '%��͹%' OR UPPER(TRIM(PRODUCT_NAME)) LIKE '%DISH%' OR UPPER(TRIM(PRODUCT_NAME)) LIKE '%SPOON%' OR UPPER(TRIM(PRODUCT_NAME)) LIKE '%���%' THEN 'WEANING'
					WHEN UPPER(SUBCLASS_NAME) LIKE '%CAR%SEAT%' THEN 'CAR SEAT'
					WHEN UPPER(TRIM(PRODUCT_NAME)) LIKE '%CAR%SEAT%' OR UPPER(TRIM(PRODUCT_NAME)) LIKE '%������%' OR UPPER(TRIM(PRODUCT_NAME)) LIKE '%����շ%' THEN 'CAR SEAT'
					WHEN UPPER(SUBCLASS_NAME) = 'WHEEL GOODS' AND UPPER(TRIM(PRODUCT_NAME)) LIKE '%SNUGRID%' THEN 'STROLLER'
					WHEN UPPER(SUBCLASS_NAME) = 'STROLLER' THEN 'STROLLER' --CDS/RBS REVISED PRODUCT MASTER
					WHEN UPPER(SUBCLASS_NAME) LIKE '%STROLLER%' OR UPPER(SUBCLASS_NAME) LIKE '%STORLLER%' THEN 'STROLLER'
					WHEN UPPER(TRIM(PRODUCT_NAME)) LIKE '%STROLLER%' OR UPPER(TRIM(PRODUCT_NAME)) LIKE '%STROLER%' OR UPPER(TRIM(PRODUCT_NAME)) LIKE '%STROLL%' OR UPPER(TRIM(PRODUCT_NAME)) LIKE '%STOLLER%' OR UPPER(TRIM(PRODUCT_NAME)) LIKE '%BABY JOG%' THEN 'STROLLER'
					WHEN UPPER(TRIM(PRODUCT_NAME)) LIKE 'ö��%' AND UPPER(DEPT_NAME) LIKE '%CHILDREN%' THEN 'STROLLER'
					WHEN UPPER(SUBCLASS_NAME) = '130171354 - APP C:CB-COMBI:ACC:ACCESSORIES' THEN 'BREAST PADS'
					WHEN UPPER(TRIM(PRODUCT_NAME)) LIKE '%�ا�纹��%' THEN 'MILK STORAGE'
					WHEN UPPER(SUBCLASS_NAME)  = 'BREAST ACC/ELECTRIC' THEN 
					CASE 
						
						WHEN UPPER(TRIM(PRODUCT_NAME)) LIKE '%SWING MAXI%' OR UPPER(TRIM(PRODUCT_NAME)) LIKE '%PVC TUBING%' THEN 'MILK PUMP MACHINE'
						WHEN UPPER(TRIM(PRODUCT_NAME)) LIKE '%SPECTRA%' OR UPPER(TRIM(PRODUCT_NAME)) LIKE '%BRUSTA%MIRACLE%' THEN 'MILK PUMP MACHINE'
						WHEN UPPER(TRIM(PRODUCT_NAME)) LIKE '%����%' OR UPPER(TRIM(PRODUCT_NAME)) LIKE '%����%' OR UPPER(TRIM(PRODUCT_NAME)) LIKE '%PUMP%' THEN 'MILK PUMP MACHINE'
						WHEN UPPER(TRIM(PRODUCT_NAME)) LIKE '%ELECTRONIC STEAM STERILIZ%' OR UPPER(TRIM(PRODUCT_NAME)) LIKE '%STERRILIZER%' THEN 'BOTTLE WARMER'
						WHEN UPPER(TRIM(PRODUCT_NAME)) LIKE '%���ͺ%' OR UPPER(TRIM(PRODUCT_NAME)) LIKE '%����ͧ���%' OR UPPER(TRIM(PRODUCT_NAME)) LIKE '%���͹��%' OR UPPER(TRIM(PRODUCT_NAME)) LIKE '%����ͺ%' 
						  OR UPPER(TRIM(PRODUCT_NAME)) LIKE '%����ͧ���%' OR UPPER(TRIM(PRODUCT_NAME)) LIKE '%����ͧ���%' OR UPPER(TRIM(PRODUCT_NAME)) LIKE '%��͹��%' OR UPPER(TRIM(PRODUCT_NAME)) LIKE '%�����%'
						  OR UPPER(TRIM(PRODUCT_NAME)) LIKE '%������%' THEN 'BOTTLE WARMER'
						WHEN UPPER(TRIM(PRODUCT_NAME)) LIKE '%STEAM%' OR UPPER(TRIM(PRODUCT_NAME)) LIKE '%STREAM%' OR UPPER(TRIM(PRODUCT_NAME)) LIKE '%DELUXE ELECTRIC BOTTLE%' THEN 'BOTTLE WARMER'
						WHEN UPPER(TRIM(PRODUCT_NAME)) LIKE '%STERILNATURAL%' OR UPPER(TRIM(PRODUCT_NAME)) LIKE '%ELECTRIC STERILIZEER%' OR UPPER(TRIM(PRODUCT_NAME)) LIKE '%STERILI%' THEN 'BOTTLE WARMER'
						WHEN UPPER(TRIM(PRODUCT_NAME)) LIKE '%BOTTLE%WARMER%' OR UPPER(TRIM(PRODUCT_NAME)) LIKE '%BOTTLE%WAMER%' OR UPPER(TRIM(PRODUCT_NAME)) LIKE '%BABY%WARMER%' THEN 'BOTTLE WARMER'
						WHEN UPPER(TRIM(PRODUCT_NAME)) LIKE '%����ͧ�Ӥ������Ҵ�Ǵ%' THEN 'BOTTLE WARMER'
						WHEN UPPER(TRIM(PRODUCT_NAME)) LIKE '%�ش�纹�ӹ�%' THEN 'MILK STORAGE'
						WHEN UPPER(TRIM(PRODUCT_NAME)) LIKE '%�ا��%' THEN 'MILK STORAGE'
						WHEN UPPER(TRIM(PRODUCT_NAME)) LIKE '%������%' THEN 'ACCESSORIES'
						WHEN UPPER(TRIM(PRODUCT_NAME)) LIKE '%����ͧ�Ǵ%' OR UPPER(TRIM(PRODUCT_NAME)) LIKE '%���͵��%' OR UPPER(TRIM(PRODUCT_NAME)) LIKE '%����ͧ�Ѵ%' THEN 'MATERNITY'
						WHEN UPPER(TRIM(PRODUCT_NAME)) LIKE '%����������%' OR UPPER(TRIM(PRODUCT_NAME)) LIKE '%����ͧ�ٴ�������%' THEN 'MATERNITY'
						WHEN UPPER(TRIM(PRODUCT_NAME)) LIKE '%BREASTMILK BOTTLE%' THEN 'FEEDING BOTTLE AND NIPPLE'
						WHEN UPPER(TRIM(PRODUCT_NAME)) LIKE '%�蹫Ѻ%' THEN 'BREAST PADS'
						WHEN UPPER(TRIM(PRODUCT_NAME)) LIKE '%BREAST%PAD%' THEN 'BREAST PADS'
						WHEN UPPER(TRIM(PRODUCT_NAME)) LIKE '%SHIELD%' THEN 'BREAST PADS'
						WHEN UPPER(TRIM(PRODUCT_NAME)) LIKE '%BREAST SHELLS%' THEN 'BREAST PADS'
						WHEN UPPER(TRIM(PRODUCT_NAME)) LIKE '%BREAST%SHELL%' THEN 'BREAST PADS'
						WHEN UPPER(TRIM(PRODUCT_NAME)) LIKE '%BREASTSHEILD%' THEN 'BREAST PADS'
						ELSE 'BREAST PADS' END
					WHEN UPPER(SUBCLASS_NAME) = 'DIAPERS/WIPES' THEN 
						CASE WHEN UPPER(TRIM(PRODUCT_NAME)) LIKE '%�Ǿ�%' THEN 'WIPES'
						WHEN UPPER(TRIM(PRODUCT_NAME)) LIKE '%�Ǿ%' THEN 'WIPES'
						WHEN UPPER(TRIM(PRODUCT_NAME)) LIKE '%�����%' THEN 'WIPES'
						WHEN UPPER(TRIM(PRODUCT_NAME)) LIKE '%WIP%' THEN 'WIPES'
						WHEN UPPER(TRIM(PRODUCT_NAME)) LIKE '%຺����%' THEN 'WIPES'
						ELSE 'DIAPERS' END
					WHEN UPPER(TRIM(PRODUCT_NAME)) LIKE '%WIPE%' THEN 'WIPES'
					WHEN UPPER(TRIM(PRODUCT_NAME)) LIKE '%຺����%' THEN 'WIPES'
					WHEN UPPER(TRIM(PRODUCT_NAME)) LIKE '%DIAPER%' THEN 'DIAPERS'
					WHEN UPPER(TRIM(PRODUCT_NAME)) LIKE '%�����蹹�%' OR UPPER(TRIM(PRODUCT_NAME)) LIKE '%����觢Ǵ%' OR UPPER(TRIM(PRODUCT_NAME)) LIKE '%���͹��%' OR UPPER(TRIM(PRODUCT_NAME)) LIKE '%BOTTLE WARMER%' OR UPPER(TRIM(PRODUCT_NAME)) LIKE '%����ͧ���%' OR UPPER(TRIM(PRODUCT_NAME)) LIKE '%����ͧ���%' OR UPPER(TRIM(PRODUCT_NAME)) LIKE '%����ͺ%' THEN 'BOTTLE WARMER'
					WHEN UPPER(TRIM(PRODUCT_NAME)) LIKE '%����ͧ%����%��%' OR UPPER(TRIM(PRODUCT_NAME)) LIKE '%���%����%��%' OR UPPER(TRIM(PRODUCT_NAME)) LIKE '%���%����%��%' OR UPPER(TRIM(PRODUCT_NAME)) LIKE '%����ͧ%����%��%' OR UPPER(TRIM(PRODUCT_NAME)) LIKE '%BREAST%PUM%' OR UPPER(TRIM(PRODUCT_NAME)) LIKE '%MILK%PUM%' OR UPPER(TRIM(PRODUCT_NAME)) LIKE '%���������%' THEN 'MILK PUMP MACHINE'
					WHEN UPPER(TRIM(PRODUCT_NAME)) LIKE '%�͡��%' OR UPPER(TRIM(PRODUCT_NAME)) LIKE '%�͡�����%' OR UPPER(TRIM(PRODUCT_NAME)) LIKE '%������%' OR UPPER(TRIM(PRODUCT_NAME)) LIKE '%�͡�ѹ��%' THEN 'SAFETY'
					WHEN UPPER(TRIM(PRODUCT_NAME)) LIKE '%�����ҴԹ��%' THEN 'STATIONERY'
					WHEN UPPER(TRIM(PRODUCT_NAME)) LIKE '������%' THEN 'ACCESSORIES'
					WHEN UPPER(DEPT_NAME) LIKE 'CHILDREN%FASHION' AND UPPER(SUBDEPT_NAME) LIKE 'BABY%FURNITURE' AND UPPER(CLASS_NAME) = 'BABY ENTERTAINER' AND UPPER(SUBCLASS_NAME) = 'HIGHCHAIR' AND UPPER(TRIM(PRODUCT_NAME)) LIKE '%SWING%' THEN 'BABY BOUNCER'
					WHEN UPPER(SUBCLASS_NAME) LIKE '%PLAYPEN%' THEN 'BABY BOUNCER'
					WHEN UPPER(TRIM(PRODUCT_NAME)) LIKE '%��ྐྵ%' OR UPPER(TRIM(PRODUCT_NAME)) LIKE '%����%' OR UPPER(TRIM(PRODUCT_NAME)) LIKE '%�Ź͹%' THEN 'BABY BOUNCER'
					WHEN UPPER(SUBCLASS_NAME) = 'BOUNCER/SWING' THEN 'BABY BOUNCER'
					WHEN UPPER(TRIM(PRODUCT_NAME)) LIKE '%���¡%' OR UPPER(TRIM(PRODUCT_NAME)) LIKE '%����%' THEN 'BABY BOUNCER'
					WHEN UPPER(TRIM(PRODUCT_NAME)) LIKE '%���������%' OR UPPER(TRIM(PRODUCT_NAME)) LIKE '%�ش������%' OR UPPER(TRIM(PRODUCT_NAME)) LIKE '���������%' OR UPPER(TRIM(PRODUCT_NAME)) LIKE '%������%' THEN 'BABY CARRIER'
					WHEN UPPER(TRIM(PRODUCT_NAME)) LIKE '%�ا��ҹѡ���¹COTTON%' OR UPPER(TRIM(PRODUCT_NAME)) LIKE '%�ا��ҹѡ���¹%' OR UPPER(TRIM(PRODUCT_NAME)) LIKE '%�ا��Ң�����%' OR UPPER(TRIM(PRODUCT_NAME)) LIKE '%�ا��Ң��LOW%' OR UPPER(TRIM(PRODUCT_NAME)) LIKE '%�ا���PACK3%' OR UPPER(TRIM(PRODUCT_NAME)) LIKE '% SOCK%' OR UPPER(TRIM(PRODUCT_NAME)) LIKE 'SOCK%' THEN 'DAYWEAR'
					WHEN UPPER(CLASS_NAME) = 'UNDERWEAR' THEN 'UNDERWEAR'
					WHEN UPPER(SUBCLASS_NAME) = 'UNDERWEAR' THEN 'UNDERWEAR'
					WHEN (UPPER(TRIM(PRODUCT_NAME)) LIKE '%�ش����%' OR UPPER(TRIM(PRODUCT_NAME)) LIKE '�ҧࡧ�%') AND UPPER(DEPT_NAME) NOT LIKE '%BOOK%' THEN 'UNDERWEAR'
					ELSE TYPE_FINAL END
;

UPDATE VPM_DATA.PM_SKUTAGGING_KIDS_PRODUCT_MASTER
SET TYPE_FINAL = CASE WHEN UPPER(TRIM(PRODUCT_NAME)) LIKE '%�Ǿ�%' THEN 'WIPES'
					  WHEN UPPER(TRIM(PRODUCT_NAME)) LIKE '%�����%' THEN 'WIPES'
					  WHEN UPPER(TRIM(PRODUCT_NAME)) LIKE '%MAMY POKO WIPS%' THEN 'WIPES'
					  WHEN UPPER(TRIM(PRODUCT_NAME)) LIKE '%D-NEE%' THEN 'BATHING'
					  WHEN UPPER(TRIM(PRODUCT_NAME)) LIKE '%WIPS%' THEN 'WIPES'
					  WHEN UPPER(TRIM(PRODUCT_NAME)) LIKE '%WHITE RABBIT%' AND UPPER(CLASS_NAME) = 'COTTON' THEN 'PERSONAL CARE'
					  ELSE UPPER(TRIM(TYPE_FINAL)) END
WHERE UPPER(TRIM(TYPE_FINAL)) = 'DIAPERS'					  
;

UPDATE VPM_DATA.PM_SKUTAGGING_KIDS_PRODUCT_MASTER
SET TYPE_FINAL = 'ADULT DIAPERS'
WHERE UPPER(TRIM(TYPE_FINAL)) = 'DIAPERS' 
	AND (UPPER(TRIM(CLEANED_BRANDNAME)) LIKE '%CERTAINTY%' OR UPPER(TRIM(CLEANED_BRANDNAME)) = 'LIFREE')
;

/** UPDATE TYPE FINAL BY DEPT SUBDEPT CLASS SUBCLASS **/

UPDATE VPM_DATA.PM_SKUTAGGING_KIDS_PRODUCT_MASTER
SET TYPE_FINAL = CASE	WHEN UPPER(DEPT_NAME) LIKE 'CHILDREN%FASHION' AND UPPER(SUBDEPT_NAME) LIKE 'BABY%ACCESSORY' AND UPPER(CLASS_NAME) = 'LAYETTE' AND UPPER(SUBCLASS_NAME) = 'FABRIC' THEN 'RUBBER PAD'
					WHEN UPPER(DEPT_NAME) = 'GM/HOUSEWARES' AND UPPER(SUBDEPT_NAME) = 'GM/NON FMCG' AND UPPER(CLASS_NAME) = 'TOYS/GIFTS/NON FMCG' AND UPPER(SUBCLASS_NAME) = 'TOYS/NON FMCG' THEN 'TOY'
					WHEN UPPER(DEPT_NAME) = 'CASUAL FOOTWEAR' AND UPPER(SUBDEPT_NAME) = 'CASUAL SHOES' AND UPPER(CLASS_NAME) = 'UNISEX' AND UPPER(SUBCLASS_NAME) = 'LOCAL' THEN 'FOOTWEAR'
					WHEN UPPER(DEPT_NAME) = 'THAI BOOK' AND UPPER(SUBDEPT_NAME) = 'CHILDREN' AND UPPER(CLASS_NAME) = 'REFERENCE' AND UPPER(SUBCLASS_NAME) = 'GENERAL KNOWLEDGED' THEN 'BOOK'
					WHEN UPPER(DEPT_NAME) = 'HARDLINE' AND UPPER(SUBDEPT_NAME) = 'TOYS' AND UPPER(CLASS_NAME) = 'ACTION FIGURE' AND UPPER(SUBCLASS_NAME) = 'BOYS' THEN 'ACTIVITY'
					WHEN UPPER(DEPT_NAME) LIKE 'CHILDREN%FASHION' AND UPPER(SUBDEPT_NAME) LIKE 'BABY%ACCESSORY' AND UPPER(CLASS_NAME) = 'TOYS' AND UPPER(SUBCLASS_NAME) = 'TOYS' THEN 'TOY'
					WHEN UPPER(DEPT_NAME) LIKE 'CHILDREN%FASHION' AND UPPER(SUBDEPT_NAME) = '4 - 8 GIRLS' AND UPPER(CLASS_NAME) = 'VEST/JACKET/SWEATER' AND UPPER(SUBCLASS_NAME) = 'VEST/JACKET/SWEATER' THEN 'DAYWEAR'
					WHEN UPPER(DEPT_NAME) LIKE 'CHILDREN%FASHION' AND UPPER(SUBDEPT_NAME) LIKE 'BABY%ACCESSORY' AND UPPER(CLASS_NAME) = 'TOYS' AND UPPER(SUBCLASS_NAME) = 'PEG TOY' THEN 'BABY TOY'
					WHEN UPPER(DEPT_NAME) = 'CROCS' AND UPPER(SUBDEPT_NAME) = 'FOOTWEAR' AND UPPER(CLASS_NAME) = 'KID UNISEX' AND UPPER(SUBCLASS_NAME) = 'CLOG' THEN 'FOOTWEAR'
					WHEN UPPER(DEPT_NAME) = 'GM/HOUSEWARES' AND UPPER(SUBDEPT_NAME) = 'FASHION' AND UPPER(CLASS_NAME) = 'BABY GOODS' AND UPPER(SUBCLASS_NAME) = 'FEEDING' THEN 'FEEDING BOTTLE AND NIPPLE'
					WHEN UPPER(DEPT_NAME) = 'THAI BOOK' AND UPPER(SUBDEPT_NAME) = 'MEDIA EDUCATION AND EDUTA' AND UPPER(CLASS_NAME) = 'CREATING MEDIA FOR KID' AND UPPER(SUBCLASS_NAME) = 'OTHERS/EXT VAT' THEN 'BOOK'
					WHEN UPPER(DEPT_NAME) LIKE 'CHILDREN%FASHION' AND UPPER(SUBDEPT_NAME) = '4 - 8 BOYS' AND UPPER(CLASS_NAME) = 'ACCESSORIES' AND UPPER(SUBCLASS_NAME) = 'ACCESSORIES' THEN 'DAYWEAR'
					WHEN UPPER(DEPT_NAME) LIKE 'CHILDREN%FASHION' AND UPPER(SUBDEPT_NAME) = '4 - 8 GIRLS' AND UPPER(CLASS_NAME) = 'ACCESSORIES' AND UPPER(SUBCLASS_NAME) = 'ACCESSORIES' THEN 'DAYWEAR'
					WHEN UPPER(DEPT_NAME) LIKE 'CHILDREN%FASHION' AND UPPER(SUBDEPT_NAME) LIKE 'BABY%ACCESSORY' AND UPPER(CLASS_NAME) = 'SAFETY ACCESS.' AND UPPER(SUBCLASS_NAME) = 'OTHER' THEN 'OTHER'
					WHEN UPPER(DEPT_NAME) = 'THAI BOOK' AND UPPER(SUBDEPT_NAME) = 'CHILDREN' AND UPPER(CLASS_NAME) = 'KINDERGARDEN - PRIMARY SC' AND UPPER(SUBCLASS_NAME) = 'ENGLISH' THEN 'EDUCATION MATERIAL'
					WHEN UPPER(DEPT_NAME) LIKE 'CHILDREN%FASHION' AND UPPER(SUBDEPT_NAME) LIKE 'BABY%ACCESSORY' AND UPPER(CLASS_NAME) = 'LAYETTE' AND UPPER(SUBCLASS_NAME) = 'OTHER' THEN 'OTHER'
					WHEN UPPER(DEPT_NAME) = 'FOOD' AND UPPER(SUBDEPT_NAME) = 'MILK & BABY FOOD' AND UPPER(CLASS_NAME) = 'NON-FRESH DAIRY' AND UPPER(SUBCLASS_NAME) = 'UHT MILK' THEN 'UHT MILK'
					WHEN UPPER(DEPT_NAME) = 'THAI BOOK' AND UPPER(SUBDEPT_NAME) = 'CHILDREN' AND UPPER(CLASS_NAME) = 'ACTIVITY' AND UPPER(SUBCLASS_NAME) = 'GAMES' THEN 'BOOK'
					WHEN UPPER(DEPT_NAME) = 'THAI BOOK' AND UPPER(SUBDEPT_NAME) = 'CHILDREN' AND UPPER(CLASS_NAME) = 'KINDERGARDEN - PRIMARY SC' AND UPPER(SUBCLASS_NAME) = 'READING & WRITING FOR BEG' THEN 'EDUCATION MATERIAL'
					WHEN UPPER(DEPT_NAME) = 'GM/HOUSEWARES' AND UPPER(SUBDEPT_NAME) = 'FASHION' AND UPPER(CLASS_NAME) = 'BABY GOODS' AND UPPER(SUBCLASS_NAME) = 'HEALTHY CARE' THEN 'ACCESSORIES'
					WHEN UPPER(DEPT_NAME) LIKE 'CHILDREN%FASHION' AND UPPER(SUBDEPT_NAME) LIKE 'BABY%ACCESSORY' AND UPPER(CLASS_NAME) = 'LAYETTE' AND UPPER(SUBCLASS_NAME) = 'MATERNITY' THEN 'FEEDING ACCESSORIES'
					WHEN UPPER(DEPT_NAME) LIKE 'CHILDREN%FASHION' AND UPPER(SUBDEPT_NAME) LIKE 'BABY%ACCESSORY' AND UPPER(CLASS_NAME) = 'LAYETTE' AND UPPER(SUBCLASS_NAME) = 'BEDDING' THEN 'BEDDING'
					WHEN UPPER(DEPT_NAME) LIKE 'CHILDREN%FASHION' AND UPPER(SUBDEPT_NAME) LIKE 'BABY%ACCESSORY' AND UPPER(CLASS_NAME) = 'TOYS' AND UPPER(SUBCLASS_NAME) = 'OTHER' THEN 'BABY TOY'
					WHEN UPPER(DEPT_NAME) LIKE 'CHILDREN%FASHION' AND UPPER(SUBDEPT_NAME) LIKE 'CHILDREN%ACCESSORIES' AND UPPER(CLASS_NAME) = 'NIGHTWEAR' AND UPPER(SUBCLASS_NAME) = 'BOYS' THEN 'DAYWEAR'
					WHEN UPPER(DEPT_NAME) = 'HARDLINE' AND UPPER(SUBDEPT_NAME) = 'TOYS' AND UPPER(CLASS_NAME) = 'ACTION FIGURE' AND UPPER(SUBCLASS_NAME) = 'GIRLS' THEN 'BOOK'
					WHEN UPPER(DEPT_NAME) LIKE 'CHILDREN%FASHION' AND UPPER(SUBDEPT_NAME) = '4 - 8 GIRLS' AND UPPER(CLASS_NAME) = 'BLOUSE' AND UPPER(SUBCLASS_NAME) = 'BLOUSE' THEN 'DAYWEAR'
					WHEN UPPER(DEPT_NAME) LIKE 'CHILDREN%FASHION' AND UPPER(SUBDEPT_NAME) LIKE 'BABY%ACCESSORY' AND UPPER(CLASS_NAME) = 'BATHROOM ACCESS.' AND UPPER(SUBCLASS_NAME) = 'TOILETRY' THEN 'BATHING'
					WHEN UPPER(DEPT_NAME) LIKE 'CHILDREN%FASHION' AND UPPER(SUBDEPT_NAME) LIKE 'BABY%ACCESSORY' AND UPPER(CLASS_NAME) = 'LAYETTE' AND UPPER(SUBCLASS_NAME) = 'PERSONAL CARE' THEN 'PERSONAL CARE'
					WHEN UPPER(DEPT_NAME) LIKE 'CHILDREN%FASHION' AND UPPER(SUBDEPT_NAME) LIKE 'CHILDREN%ACCESSORIES' AND UPPER(CLASS_NAME) = 'UNDERWEAR' AND UPPER(SUBCLASS_NAME) = 'GIRLS TOP' THEN 'DAYWEAR'
					WHEN UPPER(DEPT_NAME) = 'THAI BOOK' AND UPPER(SUBDEPT_NAME) = 'LIVING' AND UPPER(CLASS_NAME) = 'MOTHER & CHILD' AND UPPER(SUBCLASS_NAME) = 'CHILDCARE' THEN 'BOOK'
					WHEN UPPER(DEPT_NAME) = 'CROCS' AND UPPER(SUBDEPT_NAME) = 'FOOTWEAR' AND UPPER(CLASS_NAME) = 'KID BOY' AND UPPER(SUBCLASS_NAME) = 'CLOG' THEN 'FOOTWEAR'
					WHEN UPPER(DEPT_NAME) LIKE 'CHILDREN%FASHION' AND UPPER(SUBDEPT_NAME) LIKE 'BABY%ACCESSORY' AND UPPER(CLASS_NAME) = 'SAFETY ACCESS.' AND UPPER(SUBCLASS_NAME) = 'SAFETY ACCESS.' THEN 'BATHING'
					WHEN UPPER(DEPT_NAME) = 'CROCS' AND UPPER(SUBDEPT_NAME) = 'FOOTWEAR' AND UPPER(CLASS_NAME) = 'KID GIRL' AND UPPER(SUBCLASS_NAME) = 'CLOG' THEN 'FOOTWEAR'
					WHEN UPPER(DEPT_NAME) LIKE 'CHILDREN%FASHION' AND UPPER(SUBDEPT_NAME) LIKE 'BABY%ACCESSORY' AND UPPER(CLASS_NAME) = 'LAYETTE' AND UPPER(SUBCLASS_NAME) = 'WEANING' THEN 'BATHING'
					WHEN UPPER(DEPT_NAME) LIKE 'CHILDREN%FASHION' AND UPPER(SUBDEPT_NAME) = '4 - 8 GIRLS' AND UPPER(CLASS_NAME) = 'SKIRTS' AND UPPER(SUBCLASS_NAME) = 'SKIRTS' THEN 'DAYWEAR'
					WHEN UPPER(DEPT_NAME) = 'THAI BOOK' AND UPPER(SUBDEPT_NAME) = 'CHILDREN' AND UPPER(CLASS_NAME) = 'TALE' AND UPPER(SUBCLASS_NAME) = 'AESOP' THEN 'BOOK'
					WHEN UPPER(DEPT_NAME) LIKE 'CHILDREN%FASHION' AND UPPER(SUBDEPT_NAME) LIKE 'BABY%ACCESSORY' AND UPPER(CLASS_NAME) = 'TOYS' AND UPPER(SUBCLASS_NAME) = 'MOBILE' THEN 'BABY TOY'
					WHEN UPPER(DEPT_NAME) LIKE 'CHILDREN%FASHION' AND UPPER(SUBDEPT_NAME) LIKE 'BABY%ACCESSORY' AND UPPER(CLASS_NAME) = 'BATHROOM ACCESS.' AND UPPER(SUBCLASS_NAME) = 'OTHER' THEN 'BATHING'
					WHEN UPPER(DEPT_NAME) = 'HARDLINE' AND UPPER(SUBDEPT_NAME) = 'TOYS' AND UPPER(CLASS_NAME) = 'EDUCATIONAL' AND UPPER(SUBCLASS_NAME) = 'SCHOOL ACTIVITY' THEN 'ACTIVITY'
					WHEN UPPER(DEPT_NAME) = 'THAI BOOK' AND UPPER(SUBDEPT_NAME) = 'LIVING' AND UPPER(CLASS_NAME) = 'MOTHER & CHILD' AND UPPER(SUBCLASS_NAME) = 'PREGNANCY' THEN 'BOOK'
					WHEN UPPER(DEPT_NAME) LIKE 'CHILDREN%FASHION' AND UPPER(SUBDEPT_NAME) LIKE 'BABY%ACCESSORY' AND UPPER(CLASS_NAME) = 'LAYETTE' AND UPPER(SUBCLASS_NAME) = 'BREAST ACC/ELECTRIC' THEN 'FEEDING ACCESSORIES'
					WHEN UPPER(DEPT_NAME) = 'THAI BOOK' AND UPPER(SUBDEPT_NAME) = 'CHILDREN' AND UPPER(CLASS_NAME) = 'REFERENCE' AND UPPER(SUBCLASS_NAME) = 'SCIENCE' THEN 'EDUCATION MATERIAL'
					WHEN UPPER(DEPT_NAME) = 'THAI BOOK' AND UPPER(SUBDEPT_NAME) = 'CHILDREN' AND UPPER(CLASS_NAME) = 'KINDERGARDEN - PRIMARY SC' AND UPPER(SUBCLASS_NAME) = 'IQ TEST' THEN 'EDUCATION MATERIAL'
					WHEN UPPER(DEPT_NAME) = 'HARDLINE' AND UPPER(SUBDEPT_NAME) = 'TOYS' AND UPPER(CLASS_NAME) = 'EDUCATIONAL' AND UPPER(SUBCLASS_NAME) = 'OTHER' THEN 'ACTIVITY'
					WHEN UPPER(DEPT_NAME) = 'THAI BOOK' AND UPPER(SUBDEPT_NAME) = 'CHILDREN' AND UPPER(CLASS_NAME) = 'KINDERGARDEN - PRIMARY SC' AND UPPER(SUBCLASS_NAME) = 'THAI' THEN 'BOOK'
					WHEN UPPER(DEPT_NAME) = 'THAI BOOK' AND UPPER(SUBDEPT_NAME) = 'CHILDREN' AND UPPER(CLASS_NAME) = 'ACTIVITY' AND UPPER(SUBCLASS_NAME) = 'OTHERS/IN-VAT' THEN 'BOOK'
					WHEN UPPER(DEPT_NAME) LIKE 'CHILDREN%FASHION' AND UPPER(SUBDEPT_NAME) = '4 - 8 BOYS' AND UPPER(CLASS_NAME) = 'T-SHIRTS' AND UPPER(SUBCLASS_NAME) = 'T-SHIRTS' THEN 'DAYWEAR'
					WHEN UPPER(DEPT_NAME) = 'THAI BOOK' AND UPPER(SUBDEPT_NAME) = 'CHILDREN' AND UPPER(CLASS_NAME) = 'EDUCATION MEDIA' AND UPPER(SUBCLASS_NAME) = 'WORD CARD' THEN 'EDUCATION MATERIAL'
					WHEN UPPER(DEPT_NAME) = 'THAI BOOK' AND UPPER(SUBDEPT_NAME) = 'CHILDREN' AND UPPER(CLASS_NAME) = 'ACTIVITY' AND UPPER(SUBCLASS_NAME) = 'OTHERS/EXT VAT' THEN 'BOOK'
					WHEN UPPER(DEPT_NAME) = 'PLAY & LEARN' AND UPPER(SUBDEPT_NAME) = 'EDUCATION TOY' AND UPPER(CLASS_NAME) = 'KIDS ART & CRAFT' AND UPPER(SUBCLASS_NAME) = 'DRAWING & PAINT' THEN 'ART&CRAFT'
					WHEN UPPER(DEPT_NAME) LIKE 'CHILDREN%FASHION' AND UPPER(SUBDEPT_NAME) LIKE 'BABY%ACCESSORY' AND UPPER(CLASS_NAME) = 'BATHROOM ACCESS.' AND UPPER(SUBCLASS_NAME) = 'BATH ACCESS.' THEN 'BATHING'
					WHEN UPPER(DEPT_NAME) LIKE 'CHILDREN%FASHION' AND UPPER(SUBDEPT_NAME) LIKE 'BABY%ACCESSORY' AND UPPER(CLASS_NAME) = 'SAFETY ACCESS.' AND UPPER(SUBCLASS_NAME) = 'MONITOR' THEN 'SAFETY'
					WHEN UPPER(DEPT_NAME) LIKE 'CHILDREN%FASHION' AND UPPER(SUBDEPT_NAME) LIKE 'BABY%ACCESSORY' AND UPPER(CLASS_NAME) = 'TOYS' AND UPPER(SUBCLASS_NAME) = 'PLAYMAT' THEN 'TOY'
					WHEN UPPER(DEPT_NAME) LIKE 'CHILDREN%FASHION' AND UPPER(SUBDEPT_NAME) LIKE 'BABY%ACCESSORY' AND UPPER(CLASS_NAME) = 'LAYETTE' AND UPPER(SUBCLASS_NAME) = 'DIAPERS/WIPES' THEN 'DIAPERS'
					WHEN UPPER(DEPT_NAME) = 'PLAY & LEARN' AND UPPER(SUBDEPT_NAME) = 'EDUCATION TOY' AND UPPER(CLASS_NAME) = 'KIDS ART & CRAFT' AND UPPER(SUBCLASS_NAME) = 'KIDS CRAFT' THEN 'ART&CRAFT'
					WHEN UPPER(DEPT_NAME) = 'HARDLINE' AND UPPER(SUBDEPT_NAME) = 'TOYS' AND UPPER(CLASS_NAME) = 'WHEEL GOODS' AND UPPER(SUBCLASS_NAME) = 'INFLATABLES' THEN 'ACTIVITY'
					WHEN UPPER(DEPT_NAME) = 'HARDLINE' AND UPPER(SUBDEPT_NAME) = 'TOYS' AND UPPER(CLASS_NAME) = 'MECHANICAL' AND UPPER(SUBCLASS_NAME) = 'BATTERY POWER' THEN 'ACTIVITY'
					WHEN UPPER(DEPT_NAME) LIKE 'CHILDREN%FASHION' AND UPPER(SUBDEPT_NAME) LIKE 'BABY%ACCESSORY' AND UPPER(CLASS_NAME) = 'SAFETY ACCESS.' AND UPPER(SUBCLASS_NAME) = 'GATE' THEN 'SAFETY'
					WHEN UPPER(DEPT_NAME) = 'HARDLINE' AND UPPER(SUBDEPT_NAME) = 'TOYS' AND UPPER(CLASS_NAME) = 'HOBBY' AND UPPER(SUBCLASS_NAME) = 'JIGSAWS/GAMES' THEN 'ACTIVITY'
					WHEN UPPER(DEPT_NAME) LIKE 'CHILDREN%FASHION' AND UPPER(SUBDEPT_NAME) LIKE 'BABY%FURNITURE' AND UPPER(CLASS_NAME) = 'BABY GEAR' AND UPPER(SUBCLASS_NAME) = 'STORLLER' THEN 'FEEDING BOTTLE AND NIPPLE'
					WHEN UPPER(DEPT_NAME) = 'PLAY & LEARN' AND UPPER(SUBDEPT_NAME) = 'GAME & BUILDING' AND UPPER(CLASS_NAME) = 'GAME' AND UPPER(SUBCLASS_NAME) = 'BOARD GAME' THEN 'STATIONERY'
					WHEN UPPER(DEPT_NAME) = 'STATIONERY' AND UPPER(SUBDEPT_NAME) = 'WRITING INSTRUMENT & SCHO' AND UPPER(CLASS_NAME) = 'SCHOOL SUPPLY' AND UPPER(SUBCLASS_NAME) = 'PENCIL CASE :BAG' THEN 'STATIONERY'
					WHEN UPPER(DEPT_NAME) LIKE 'CHILDREN%FASHION' AND UPPER(SUBDEPT_NAME) = 'BUILD-A-BEAR WORKSHOP' AND UPPER(CLASS_NAME) = 'GIRL 6.5-9' AND UPPER(SUBCLASS_NAME) = 'OUTFITTING' THEN 'DAYWEAR'
					WHEN UPPER(DEPT_NAME) = 'THAI BOOK' AND UPPER(SUBDEPT_NAME) = 'CHILDREN' AND UPPER(CLASS_NAME) = 'KINDERGARDEN - PRIMARY SC' AND UPPER(SUBCLASS_NAME) = 'MATHEMATIC' THEN 'EDUCATION MATERIAL'
					WHEN UPPER(DEPT_NAME) = 'CASUAL FOOTWEAR' AND UPPER(SUBDEPT_NAME) = 'CASUAL SHOES' AND UPPER(CLASS_NAME) = 'UNISEX' AND UPPER(SUBCLASS_NAME) = 'IMPORT' THEN 'FOOTWEAR'
					WHEN UPPER(DEPT_NAME) = 'THAI BOOK' AND UPPER(SUBDEPT_NAME) = 'CHILDREN' AND UPPER(CLASS_NAME) = 'EDUCATION MEDIA' AND UPPER(SUBCLASS_NAME) = 'NUMBER & TIME' THEN 'EDUCATION MATERIAL'
					WHEN UPPER(DEPT_NAME) LIKE 'CHILDREN%FASHION' AND UPPER(SUBDEPT_NAME) LIKE 'BABY%FURNITURE' AND UPPER(CLASS_NAME) = 'BABY GEAR' AND UPPER(SUBCLASS_NAME) = 'CAR SEAT' THEN 'CAR SEAT'
					WHEN UPPER(DEPT_NAME) = 'HARDLINE' AND UPPER(SUBDEPT_NAME) = 'TOYS' AND UPPER(CLASS_NAME) = 'HOBBY' AND UPPER(SUBCLASS_NAME) = 'MODEL KITS' THEN 'ACTIVITY'
					WHEN UPPER(DEPT_NAME) = 'THAI BOOK' AND UPPER(SUBDEPT_NAME) = 'LIVING' AND UPPER(CLASS_NAME) = 'MOTHER & CHILD' AND UPPER(SUBCLASS_NAME) = 'BABY NAMES' THEN 'BOOK'
					WHEN UPPER(DEPT_NAME) = 'THAI BOOK' AND UPPER(SUBDEPT_NAME) = 'CHILDREN' AND UPPER(CLASS_NAME) = 'KINDERGARDEN - PRIMARY SC' AND UPPER(SUBCLASS_NAME) = 'LIVING EXPERIENCES LEARNI' THEN 'EDUCATION MATERIAL'
					WHEN UPPER(DEPT_NAME) LIKE 'CHILDREN%FASHION' AND UPPER(SUBDEPT_NAME) LIKE 'BABY%FURNITURE' AND UPPER(CLASS_NAME) = 'BABY ENTERTAINER' AND UPPER(SUBCLASS_NAME) = 'WALKER' THEN 'BABY WALKER'
					WHEN UPPER(DEPT_NAME) = 'GARMENT' AND UPPER(SUBDEPT_NAME) = 'CHILDRENS WEAR' AND UPPER(CLASS_NAME) = 'INNERWEAR / ACCESSORIES' AND UPPER(SUBCLASS_NAME) = 'SOCKS' THEN 'DAYWEAR'
					WHEN UPPER(DEPT_NAME) LIKE 'CHILDREN%FASHION' AND UPPER(SUBDEPT_NAME) = '4 - 8 GIRLS' AND UPPER(CLASS_NAME) = 'SHORT PANTS' AND UPPER(SUBCLASS_NAME) = 'SHORT PANTS' THEN 'DAYWEAR'
					WHEN UPPER(DEPT_NAME) = 'PLAY & LEARN' AND UPPER(SUBDEPT_NAME) = 'EDUCATION TOY' AND UPPER(CLASS_NAME) = 'EDUCATIONAL' AND UPPER(SUBCLASS_NAME) = 'FIGURE & MODEL' THEN 'STATIONERY'
					WHEN UPPER(DEPT_NAME) = 'HARDLINE' AND UPPER(SUBDEPT_NAME) = 'STATIONERY A' AND UPPER(CLASS_NAME) = 'SCHOOL SUPPLIES' AND UPPER(SUBCLASS_NAME) = 'SHOOLS ACCESS.' THEN 'STATIONERY'
					WHEN UPPER(DEPT_NAME) = 'HARDLINE' AND UPPER(SUBDEPT_NAME) = 'TOYS' AND UPPER(CLASS_NAME) = 'ACTIVITY SET' AND UPPER(SUBCLASS_NAME) = 'OTHER' THEN 'ACTIVITY'
					WHEN UPPER(DEPT_NAME) LIKE 'CHILDREN%FASHION' AND UPPER(SUBDEPT_NAME) LIKE 'BABY%FURNITURE' AND UPPER(CLASS_NAME) = 'BABY GEAR' AND UPPER(SUBCLASS_NAME) = 'PLAYPEN' THEN 'BABY BOUNCER'
					WHEN UPPER(DEPT_NAME) LIKE 'CHILDREN%FASHION' AND UPPER(SUBDEPT_NAME) LIKE 'BABY%FURNITURE' AND UPPER(CLASS_NAME) = 'BABY ENTERTAINER' AND UPPER(SUBCLASS_NAME) = 'BOUNCER/SWING' THEN 'BABY BOUNCER'
					WHEN UPPER(DEPT_NAME) LIKE 'CHILDREN%FASHION' AND UPPER(SUBDEPT_NAME) = 'BUILD-A-BEAR WORKSHOP' AND UPPER(CLASS_NAME) = 'UNIVESAL' AND UPPER(SUBCLASS_NAME) = 'UNSTUFFED' THEN 'TOY'
					WHEN UPPER(DEPT_NAME) = 'THAI BOOK' AND UPPER(SUBDEPT_NAME) = 'MEDIA EDUCATION AND EDUTA' AND UPPER(CLASS_NAME) = 'CREATING MEDIA FOR KID' AND UPPER(SUBCLASS_NAME) = 'WORD CARD' THEN 'EDUCATION MATERIAL'
					WHEN UPPER(DEPT_NAME) = 'ENTERTAINMENT MEDIA' AND UPPER(SUBDEPT_NAME) = 'ENTERTAINMENT MERCHANDISE' AND UPPER(CLASS_NAME) = 'TOY' AND UPPER(SUBCLASS_NAME) = 'CARTOON/ANIMATION' THEN 'TOY'
					WHEN UPPER(DEPT_NAME) = 'FOREIGN BOOKS' AND UPPER(SUBDEPT_NAME) = 'CHILDRENS TEENAGE&EDUCATI' AND UPPER(CLASS_NAME) = 'CHILDRENS PICTURE BOOKS' AND UPPER(SUBCLASS_NAME) = 'EARLY LEARNING/CONCEPTS' THEN 'EDUCATION MATERIAL'
					WHEN UPPER(DEPT_NAME) = 'FOREIGN BOOKS' AND UPPER(SUBDEPT_NAME) = 'CHILDRENS TEENAGE&EDUCATI' AND UPPER(CLASS_NAME) = 'CHILDRENS PICTURE BOOKS' AND UPPER(SUBCLASS_NAME) = 'INTERATIVE&ACTIVITY BOOKS' THEN 'EDUCATION MATERIAL'
					WHEN UPPER(DEPT_NAME) = 'GARMENT' AND UPPER(SUBDEPT_NAME) = 'CHILDRENS WEAR' AND UPPER(CLASS_NAME) = 'TODDLER' AND UPPER(SUBCLASS_NAME) = 'B S/S TEE' THEN 'DAYWEAR'
					WHEN UPPER(DEPT_NAME) = 'THAI BOOK' AND UPPER(SUBDEPT_NAME) = 'CHILDREN' AND UPPER(CLASS_NAME) = 'ACTIVITY' AND UPPER(SUBCLASS_NAME) = 'CRAFTS' THEN 'BOOK'
					WHEN UPPER(DEPT_NAME) LIKE 'CHILDREN%FASHION' AND UPPER(SUBDEPT_NAME) = 'BUILD-A-BEAR WORKSHOP' AND UPPER(CLASS_NAME) = 'GIRL 6.5-9' AND UPPER(SUBCLASS_NAME) = 'UNSTUFFED' THEN 'TOY'
					WHEN UPPER(DEPT_NAME) LIKE 'CHILDREN%FASHION' AND UPPER(SUBDEPT_NAME) = 'BUILD-A-BEAR WORKSHOP' AND UPPER(CLASS_NAME) = 'GIRL 6.5-9' AND UPPER(SUBCLASS_NAME) = 'ACCESSORIES' THEN 'ACCESSORIES'
					WHEN UPPER(DEPT_NAME) LIKE 'CHILDREN%FASHION' AND UPPER(SUBDEPT_NAME) = 'BUILD-A-BEAR WORKSHOP' AND UPPER(CLASS_NAME) = 'UNIVESAL' AND UPPER(SUBCLASS_NAME) = 'FOOTWEAR' THEN 'FOOTWEAR'
					WHEN UPPER(DEPT_NAME) = 'THAI BOOK' AND UPPER(SUBDEPT_NAME) = 'PSYCHOLOGY' AND UPPER(CLASS_NAME) = 'MOTHER & CHILD' AND UPPER(SUBCLASS_NAME) = 'BABY NAMES' THEN 'BOOK'
					WHEN UPPER(DEPT_NAME) = 'ACCESSORIES' AND UPPER(SUBDEPT_NAME) = 'JUNIOR SHOES' AND UPPER(CLASS_NAME) = 'DRESSY' AND UPPER(SUBCLASS_NAME) = 'DRESSY' THEN 'FOOTWEAR'
					WHEN UPPER(DEPT_NAME) = 'THAI BOOK' AND UPPER(SUBDEPT_NAME) = 'EXAMINATION GUIDE' AND UPPER(CLASS_NAME) = 'JUNIOR HIGH SCHOOL(M1-3)' AND UPPER(SUBCLASS_NAME) = 'SCIENCE M1-3' THEN 'EDUCATION MATERIAL'
					WHEN UPPER(DEPT_NAME) LIKE 'CHILDREN%FASHION' AND UPPER(SUBDEPT_NAME) = 'BUILD-A-BEAR WORKSHOP' AND UPPER(CLASS_NAME) = 'BOY 3-9' AND UPPER(SUBCLASS_NAME) = 'FOOTWEAR' THEN 'FOOTWEAR'
					WHEN UPPER(DEPT_NAME) LIKE 'CHILDREN%FASHION' AND UPPER(SUBDEPT_NAME) = 'BUILD-A-BEAR WORKSHOP' AND UPPER(CLASS_NAME) = 'GIRL 6.5-9' AND UPPER(SUBCLASS_NAME) = 'PLAY' THEN 'FOOTWEAR'
					WHEN UPPER(DEPT_NAME) = 'GM/HOUSEWARES' AND UPPER(SUBDEPT_NAME) = 'FASHION' AND UPPER(CLASS_NAME) = 'BABY GOODS' AND UPPER(SUBCLASS_NAME) = 'BEDDING/CLOTHING' THEN 'BEDDING'
					WHEN UPPER(DEPT_NAME) LIKE 'CHILDREN%FASHION' AND UPPER(SUBDEPT_NAME) = 'BUILD-A-BEAR WORKSHOP' AND UPPER(CLASS_NAME) = 'GIRL 3-6.5' AND UPPER(SUBCLASS_NAME) = 'STUFFED' THEN 'FOOTWEAR'
					WHEN UPPER(DEPT_NAME) = 'THAI BOOK' AND UPPER(SUBDEPT_NAME) = 'CHILDREN' AND UPPER(CLASS_NAME) = 'TALE' AND UPPER(SUBCLASS_NAME) = 'POP UP BOOK' THEN 'BOOK'
					WHEN UPPER(DEPT_NAME) LIKE 'CHILDREN%FASHION' AND UPPER(SUBDEPT_NAME) LIKE 'CHILDREN%ACCESSORIES' AND UPPER(CLASS_NAME) = 'ACCESSORIES' AND UPPER(SUBCLASS_NAME) = 'ACCESSORIES' THEN 'ACCESSORIES'
					WHEN UPPER(DEPT_NAME) = 'GARMENT' AND UPPER(SUBDEPT_NAME) = 'CHILDRENS WEAR' AND UPPER(CLASS_NAME) = 'BABY S' AND UPPER(SUBCLASS_NAME) = 'B S/S TEE' THEN 'DAYWEAR'
					WHEN UPPER(DEPT_NAME) LIKE 'CHILDREN%FASHION' AND UPPER(SUBDEPT_NAME) LIKE 'CHILDREN%ACCESSORIES' AND UPPER(CLASS_NAME) = 'SWIMWEAR' AND UPPER(SUBCLASS_NAME) = 'GIRLS' THEN 'SWIMWEAR'
					WHEN UPPER(DEPT_NAME) = 'ACCESSORIES' AND UPPER(SUBDEPT_NAME) = 'JUNIOR SHOES' AND UPPER(CLASS_NAME) = 'CASUAL' AND UPPER(SUBCLASS_NAME) = 'CASUAL' THEN 'FOOTWEAR'
					WHEN UPPER(DEPT_NAME) LIKE 'CHILDREN%FASHION' AND UPPER(SUBDEPT_NAME) LIKE 'CHILDREN%ACCESSORIES' AND UPPER(CLASS_NAME) = 'SWIMWEAR' AND UPPER(SUBCLASS_NAME) = 'SWIMMING ACCESS.' THEN 'SWIMWEAR'
					WHEN UPPER(DEPT_NAME) = 'THAI BOOK' AND UPPER(SUBDEPT_NAME) = 'CHILDREN' AND UPPER(CLASS_NAME) = 'ACTIVITY' AND UPPER(SUBCLASS_NAME) = 'BOOK & DOLL' THEN 'BOOK'
					WHEN UPPER(DEPT_NAME) = 'THAI BOOK' AND UPPER(SUBDEPT_NAME) = 'CHILDREN' AND UPPER(CLASS_NAME) = 'ACTIVITY' AND UPPER(SUBCLASS_NAME) = 'DOT TO DOT' THEN 'BOOK'
					WHEN UPPER(DEPT_NAME) = 'THAI BOOK' AND UPPER(SUBDEPT_NAME) = 'CHILDREN' AND UPPER(CLASS_NAME) = 'TALE' AND UPPER(SUBCLASS_NAME) = 'OTHERS/IN-VAT' THEN 'BOOK'
					WHEN UPPER(DEPT_NAME) = 'GARMENT' AND UPPER(SUBDEPT_NAME) = 'CHILDRENS WEAR' AND UPPER(CLASS_NAME) = 'TODDLER' AND UPPER(SUBCLASS_NAME) = 'B L/S TEE' THEN 'DAYWEAR'
					WHEN UPPER(DEPT_NAME) LIKE 'CHILDREN%FASHION' AND UPPER(SUBDEPT_NAME) = 'BUILD-A-BEAR WORKSHOP' AND UPPER(CLASS_NAME) = 'UNIVESAL' AND UPPER(SUBCLASS_NAME) = 'OUTFITTING' THEN 'DAYWEAR'
					WHEN UPPER(DEPT_NAME) = 'THAI BOOK' AND UPPER(SUBDEPT_NAME) = 'CHILDREN' AND UPPER(CLASS_NAME) = 'EDUCATION MEDIA' AND UPPER(SUBCLASS_NAME) = 'POSTER' THEN 'EDUCATION MATERIAL'
					WHEN UPPER(DEPT_NAME) LIKE 'CHILDREN%FASHION' AND UPPER(SUBDEPT_NAME) LIKE 'CHILDREN%ACCESSORIES' AND UPPER(CLASS_NAME) = 'ACCESSORIES' AND UPPER(SUBCLASS_NAME) = 'BAG/BELT' THEN 'ACCESSORIES'
					WHEN UPPER(DEPT_NAME) = 'THAI BOOK' AND UPPER(SUBDEPT_NAME) = 'CHILDREN' AND UPPER(CLASS_NAME) = 'KINDERGARDEN - PRIMARY SC' AND UPPER(SUBCLASS_NAME) = 'BEGINNING TEST GUIDE BOOK' THEN 'EDUCATION MATERIAL'
					WHEN UPPER(DEPT_NAME) = 'THAI BOOK' AND UPPER(SUBDEPT_NAME) = 'CHILDREN' AND UPPER(CLASS_NAME) = 'KINDERGARDEN - PRIMARY SC' AND UPPER(SUBCLASS_NAME) = 'OTHERS/EXT VAT' THEN 'EDUCATION MATERIAL'
					WHEN UPPER(DEPT_NAME) = 'THAI BOOK' AND UPPER(SUBDEPT_NAME) = 'PSYCHOLOGY' AND UPPER(CLASS_NAME) = 'MOTHER & CHILD' AND UPPER(SUBCLASS_NAME) = 'BABY FOOD' THEN 'BOOK'
					WHEN UPPER(DEPT_NAME) LIKE 'CHILDREN%FASHION' AND UPPER(SUBDEPT_NAME) LIKE 'CHILDREN%SHOES' AND UPPER(CLASS_NAME) = 'ACCESSORIES' AND UPPER(SUBCLASS_NAME) = 'ACCESSORIES' THEN 'ACCESSORIES'
					WHEN UPPER(DEPT_NAME) = 'THAI BOOK' AND UPPER(SUBDEPT_NAME) = 'PSYCHOLOGY' AND UPPER(CLASS_NAME) = 'MOTHER & CHILD' AND UPPER(SUBCLASS_NAME) = 'PREGNANCY' THEN 'BOOK'
					WHEN UPPER(DEPT_NAME) = 'THAI BOOK' AND UPPER(SUBDEPT_NAME) = 'EXAMINATION GUIDE' AND UPPER(CLASS_NAME) = 'JUNIOR PRIMARY SCHOOL' AND UPPER(SUBCLASS_NAME) = 'THAI' THEN 'EDUCATION MATERIAL'
					WHEN UPPER(DEPT_NAME) = 'FOREIGN BOOK' AND UPPER(SUBDEPT_NAME) = 'CHILDREN' AND UPPER(CLASS_NAME) = 'BABY - PRESCHOOL/BASIC LE' AND UPPER(SUBCLASS_NAME) = 'OTHERS' THEN 'EDUCATION MATERIAL'
					WHEN UPPER(DEPT_NAME) = 'THAI BOOK' AND UPPER(SUBDEPT_NAME) = 'EXAMINATION GUIDE' AND UPPER(CLASS_NAME) = 'PRIMARY SCHOOL' AND UPPER(SUBCLASS_NAME) = 'SCIENCE' THEN 'EDUCATION MATERIAL'
					WHEN UPPER(DEPT_NAME) LIKE 'CHILDREN%FASHION' AND UPPER(SUBDEPT_NAME) LIKE 'CHILDREN%SHOES' AND UPPER(CLASS_NAME) = 'GIRLS SHOES' AND UPPER(SUBCLASS_NAME) = 'UNIFORM' THEN 'FOOTWEAR'
					WHEN UPPER(DEPT_NAME) = 'THAI BOOK' AND UPPER(SUBDEPT_NAME) = 'CHILDREN' AND UPPER(CLASS_NAME) = 'PRE-SCHOOL' AND UPPER(SUBCLASS_NAME) = 'NUMBER & TIME' THEN 'EDUCATION MATERIAL'
					WHEN UPPER(DEPT_NAME) = 'GARMENT' AND UPPER(SUBDEPT_NAME) = 'CHILDRENS WEAR' AND UPPER(CLASS_NAME) = 'TODDLER' AND UPPER(SUBCLASS_NAME) = 'G C&S TUNIC' THEN 'DAYWEAR'
					WHEN UPPER(DEPT_NAME) = 'THAI BOOK' AND UPPER(SUBDEPT_NAME) = 'EXAMINATION GUIDE' AND UPPER(CLASS_NAME) = 'SENIOR HIGH SCHOOL(M4-6)' AND UPPER(SUBCLASS_NAME) = 'MATHEMATICS M 4-6' THEN 'EDUCATION MATERIAL'
					WHEN UPPER(DEPT_NAME) = 'GM/HOUSEWARES' AND UPPER(SUBDEPT_NAME) = 'FASHION' AND UPPER(CLASS_NAME) = 'BABY''S WEAR' AND UPPER(SUBCLASS_NAME) = 'INFANT/TODDLERS' THEN 'DAYWEAR'
					WHEN UPPER(DEPT_NAME) = 'FOREIGN BOOKS' AND UPPER(SUBDEPT_NAME) = 'CHILDRENS TEENAGE&EDUCATI' AND UPPER(CLASS_NAME) = 'CHILDRENS FICTION&TRUE' AND UPPER(SUBCLASS_NAME) = 'GENERAL FICTION' THEN 'BOOK'
					WHEN UPPER(DEPT_NAME) LIKE 'CHILDREN%FASHION' AND UPPER(SUBDEPT_NAME) LIKE 'CHILDREN%SHOES' AND UPPER(CLASS_NAME) = 'INFANTS' AND UPPER(SUBCLASS_NAME) = 'INFANTS' THEN 'DAYWEAR'
					WHEN UPPER(DEPT_NAME) = 'THAI BOOK' AND UPPER(SUBDEPT_NAME) = 'EXAMINATION GUIDE' AND UPPER(CLASS_NAME) = 'PRIMARY SCHOOL' AND UPPER(SUBCLASS_NAME) = 'EXAMINATION GUIDE SENIOR' THEN 'EDUCATION MATERIAL'
					WHEN UPPER(DEPT_NAME) = 'GARMENT' AND UPPER(SUBDEPT_NAME) = 'CHILDRENS WEAR' AND UPPER(CLASS_NAME) = 'TODDLER' AND UPPER(SUBCLASS_NAME) = 'G C&S CUT LENGTHGTH PANTS' THEN 'DAYWEAR'
					WHEN UPPER(DEPT_NAME) = 'FOREIGN BOOKS' AND UPPER(SUBDEPT_NAME) = 'CHILDRENS TEENAGE&EDUCATI' AND UPPER(CLASS_NAME) = 'CHILDRENS FICTION&TRUE' AND UPPER(SUBCLASS_NAME) = 'FANTASY&MAGICAL REALISM' THEN 'BOOK'
					WHEN UPPER(DEPT_NAME) = 'THAI BOOK' AND UPPER(SUBDEPT_NAME) = 'MEDIA EDUCATION AND EDUTA' AND UPPER(CLASS_NAME) = 'CREATING MEDIA FOR KID' AND UPPER(SUBCLASS_NAME) = 'OTHERS' THEN 'BOOK'
					WHEN UPPER(DEPT_NAME) = 'GARMENT' AND UPPER(SUBDEPT_NAME) = 'CHILDRENS WEAR' AND UPPER(CLASS_NAME) = 'TODDLER' AND UPPER(SUBCLASS_NAME) = 'B C&S CUT LENGTHGTH PANTS' THEN 'DAYWEAR'
					WHEN UPPER(DEPT_NAME) LIKE 'CHILDREN%FASHION' AND UPPER(SUBDEPT_NAME) LIKE 'CHILDREN%ACCESSORIES' AND UPPER(CLASS_NAME) = 'SWIMWEAR' AND UPPER(SUBCLASS_NAME) = 'BOYS' THEN 'SWIMWEAR'
					WHEN UPPER(DEPT_NAME) = 'GM/HOUSEWARES' AND UPPER(SUBDEPT_NAME) = 'FASHION' AND UPPER(CLASS_NAME) = 'BABY GOODS' AND UPPER(SUBCLASS_NAME) = 'INFANT TOYS' THEN 'BABY TOY'
					WHEN UPPER(DEPT_NAME) = 'GARMENT' AND UPPER(SUBDEPT_NAME) = 'CHILDRENS WEAR' AND UPPER(CLASS_NAME) = 'TODDLER' AND UPPER(SUBCLASS_NAME) = 'G C&S LONG PANTS' THEN 'DAYWEAR'
					WHEN UPPER(DEPT_NAME) LIKE 'CHILDREN%FASHION' AND UPPER(SUBDEPT_NAME) LIKE 'CHILDREN%SHOES' AND UPPER(CLASS_NAME) = 'BOYS SHOES' AND UPPER(SUBCLASS_NAME) = 'CASUAL' THEN 'FOOTWEAR'
					WHEN UPPER(DEPT_NAME) = 'THAI BOOK' AND UPPER(SUBDEPT_NAME) = 'EXAMINATION GUIDE' AND UPPER(CLASS_NAME) = 'SENIOR HIGH SCHOOL(M4-6)' AND UPPER(SUBCLASS_NAME) = 'SOCIAL M 4-6' THEN 'EDUCATION MATERIAL'
					WHEN UPPER(DEPT_NAME) = 'THAI BOOK' AND UPPER(SUBDEPT_NAME) = 'CHILDREN' AND UPPER(CLASS_NAME) = 'ACTIVITY' AND UPPER(SUBCLASS_NAME) = 'ACTIVITY BOOK SET' THEN 'BOOK'
					WHEN UPPER(DEPT_NAME) = 'FOREIGN BOOKS' AND UPPER(SUBDEPT_NAME) = 'CHILDRENS TEENAGE&EDUCATI' AND UPPER(CLASS_NAME) = 'CHILDRENS PICTURE BOOKS' AND UPPER(SUBCLASS_NAME) = 'OTHERS' THEN 'BOOK'
					WHEN UPPER(DEPT_NAME) LIKE 'CHILDREN%FASHION' AND UPPER(SUBDEPT_NAME) = 'GUCCI KIDS' AND UPPER(CLASS_NAME) = 'READY TO WEAR' AND UPPER(SUBCLASS_NAME) = 'GIFT SET' THEN 'DAYWEAR'
					WHEN UPPER(DEPT_NAME) = 'THAI BOOK' AND UPPER(SUBDEPT_NAME) = 'EXAMINATION GUIDE' AND UPPER(CLASS_NAME) = 'PRIMARY SCHOOL' AND UPPER(SUBCLASS_NAME) = 'THAI LANGUAGES' THEN 'EDUCATION MATERIAL'
					WHEN UPPER(DEPT_NAME) = 'GARMENT' AND UPPER(SUBDEPT_NAME) = 'CHILDRENS WEAR' AND UPPER(CLASS_NAME) = 'TODDLER' AND UPPER(SUBCLASS_NAME) = 'B OTHER C&S' THEN 'DAYWEAR'
					WHEN UPPER(DEPT_NAME) = 'THAI BOOK' AND UPPER(SUBDEPT_NAME) = 'CHILDREN' AND UPPER(CLASS_NAME) = 'KINDERGARDEN - PRIMARY SC' AND UPPER(SUBCLASS_NAME) = 'PRIMARY TEST GUIDE BOOK' THEN 'EDUCATION MATERIAL'
					WHEN UPPER(DEPT_NAME) = 'THAI BOOK' AND UPPER(SUBDEPT_NAME) = 'EXAMINATION GUIDE' AND UPPER(CLASS_NAME) = 'PRIMARY SCHOOL' AND UPPER(SUBCLASS_NAME) = 'SOCIAL' THEN 'EDUCATION MATERIAL'
					WHEN UPPER(DEPT_NAME) = 'HARDLINE' AND UPPER(SUBDEPT_NAME) = 'TOYS' AND UPPER(CLASS_NAME) = 'ACTIVITY SET' AND UPPER(SUBCLASS_NAME) = 'KITCHEN/SAND/BEAUTY' THEN 'ACTIVITY'
					WHEN UPPER(DEPT_NAME) = 'THAI BOOK' AND UPPER(SUBDEPT_NAME) = 'EXAMINATION GUIDE' AND UPPER(CLASS_NAME) = 'SENIOR HIGH SCHOOL(M4-6)' AND UPPER(SUBCLASS_NAME) = 'BIOLOGY M 4-6' THEN 'EDUCATION MATERIAL'
					WHEN UPPER(DEPT_NAME) LIKE 'CHILDREN%FASHION' AND UPPER(SUBDEPT_NAME) = 'INFANT/TODDLERS' AND UPPER(CLASS_NAME) = 'TODDLER' AND UPPER(SUBCLASS_NAME) = 'UNDERWEAR' THEN 'SWIMWEAR'
					WHEN UPPER(DEPT_NAME) = 'GARMENT' AND UPPER(SUBDEPT_NAME) = 'CHILDRENS WEAR' AND UPPER(CLASS_NAME) = 'TODDLER' AND UPPER(SUBCLASS_NAME) = 'G OTHER C&S' THEN 'DAYWEAR'
					WHEN UPPER(DEPT_NAME) = 'THAI BOOK' AND UPPER(SUBDEPT_NAME) = 'EXAMINATION GUIDE' AND UPPER(CLASS_NAME) = 'JUNIOR HIGH SCHOOL(M1-3)' AND UPPER(SUBCLASS_NAME) = 'THAI LANGUAGES M1-3' THEN 'EDUCATION MATERIAL'
					WHEN UPPER(DEPT_NAME) = 'FOREIGN BOOKS' AND UPPER(SUBDEPT_NAME) = 'CHILDRENS TEENAGE&EDUCATI' AND UPPER(CLASS_NAME) = 'CHILDRENS PICTURE BOOKS' AND UPPER(SUBCLASS_NAME) = 'PICTURE BOOKS' THEN 'BOOK'
					WHEN UPPER(DEPT_NAME) = 'PLAY & LEARN' AND UPPER(SUBDEPT_NAME) = 'GAME & BUILDING' AND UPPER(CLASS_NAME) = 'GAME' AND UPPER(SUBCLASS_NAME) = 'CHILDREN GAME' THEN 'STATIONERY'
					WHEN UPPER(DEPT_NAME) = 'HARDLINE' AND UPPER(SUBDEPT_NAME) = 'TOYS' AND UPPER(CLASS_NAME) = 'COLLECTIBLES' AND UPPER(SUBCLASS_NAME) = 'TRAINS' THEN 'ACTIVITY'
					WHEN UPPER(DEPT_NAME) LIKE 'CHILDREN%FASHION' AND UPPER(SUBDEPT_NAME) = 'INFANT/TODDLERS' AND UPPER(CLASS_NAME) = 'TODDLER' AND UPPER(SUBCLASS_NAME) = 'PYJAMAS/SWIMWEAR' THEN 'SWIMWEAR'
					WHEN UPPER(DEPT_NAME) = 'THAI BOOK' AND UPPER(SUBDEPT_NAME) = 'EXAMINATION GUIDE' AND UPPER(CLASS_NAME) = 'SENIOR HIGH SCHOOL(M4-6)' AND UPPER(SUBCLASS_NAME) = 'CHEMICAL M 4-6' THEN 'EDUCATION MATERIAL'
					WHEN UPPER(DEPT_NAME) = 'FOREIGN BOOKS' AND UPPER(SUBDEPT_NAME) = 'CHILDRENS TEENAGE&EDUCATI' AND UPPER(CLASS_NAME) = 'CHILDRENS GENERAL NON-FIC' AND UPPER(SUBCLASS_NAME) = 'LITERATURE BOOKS&WRITERS' THEN 'EDUCATION MATERIAL'
					WHEN UPPER(DEPT_NAME) LIKE 'CHILDREN%FASHION' AND UPPER(SUBDEPT_NAME) = 'NOVELTY' AND UPPER(CLASS_NAME) = 'TOYS/DOLLS' AND UPPER(SUBCLASS_NAME) = 'TOYS/DOLLS' THEN 'BABY TOY'
					WHEN UPPER(DEPT_NAME) LIKE 'CHILDREN%FASHION' AND UPPER(SUBDEPT_NAME) = 'NOVELTY' AND UPPER(CLASS_NAME) = 'PERSONAL ACCS.' AND UPPER(SUBCLASS_NAME) = 'PERSONAL ACCS.' THEN 'PERSONAL CARE'
					WHEN UPPER(DEPT_NAME) = 'FOREIGN BOOKS' AND UPPER(SUBDEPT_NAME) = 'CHILDRENS TEENAGE&EDUCATI' AND UPPER(CLASS_NAME) = 'CHILDRENS EDUCATIONAL MAT' AND UPPER(SUBCLASS_NAME) = 'STUDY & REVISION GUIDES' THEN 'EDUCATION MATERIAL'
					WHEN UPPER(DEPT_NAME) = 'FOREIGN BOOKS' AND UPPER(SUBDEPT_NAME) = 'CHILDRENS TEENAGE&EDUCATI' AND UPPER(CLASS_NAME) = 'CHILDRENS GENERAL NON-FIC' AND UPPER(SUBCLASS_NAME) = 'HOBBIES QUIZZES&GAMES' THEN 'EDUCATION MATERIAL'
					WHEN UPPER(DEPT_NAME) = 'HARDLINE' AND UPPER(SUBDEPT_NAME) = 'TOYS' AND UPPER(CLASS_NAME) = 'WHEEL GOODS' AND UPPER(SUBCLASS_NAME) = 'OTHER' THEN 'ACTIVITY'
					WHEN UPPER(DEPT_NAME) = 'GARMENT' AND UPPER(SUBDEPT_NAME) = 'CHILDRENS WEAR' AND UPPER(CLASS_NAME) = 'TODDLER' AND UPPER(SUBCLASS_NAME) = 'B C&S LONG PANTS' THEN 'DAYWEAR'
					WHEN UPPER(DEPT_NAME) = 'FOREIGN BOOKS' AND UPPER(SUBDEPT_NAME) = 'CHILDRENS TEENAGE&EDUCATI' AND UPPER(CLASS_NAME) = 'CHILDRENS FICTION&TRUE' AND UPPER(SUBCLASS_NAME) = 'HUMOROUS STORIES' THEN 'BOOK'
					WHEN UPPER(DEPT_NAME) = 'GARMENT' AND UPPER(SUBDEPT_NAME) = 'CHILDRENS WEAR' AND UPPER(CLASS_NAME) = 'INNERWEAR / ACCESSORIES' AND UPPER(SUBCLASS_NAME) = 'SHOES' THEN 'DAYWEAR'
					WHEN UPPER(DEPT_NAME) = 'FOREIGN BOOKS' AND UPPER(SUBDEPT_NAME) = 'CHILDRENS TEENAGE&EDUCATI' AND UPPER(CLASS_NAME) = 'CHILDRENS EDUCATIONAL MAT' AND UPPER(SUBCLASS_NAME) = 'OTHERS' THEN 'EDUCATION MATERIAL'
					WHEN UPPER(DEPT_NAME) LIKE 'CHILDREN%FASHION' AND UPPER(SUBDEPT_NAME) = 'NOVELTY' AND UPPER(CLASS_NAME) = 'STATIONERY/SCHOOL SUPPLIE' AND UPPER(SUBCLASS_NAME) = 'STATIONERY/SCHOOL SUPPLIE' THEN 'STATIONERY'
					WHEN UPPER(DEPT_NAME) = 'THAI BOOK' AND UPPER(SUBDEPT_NAME) = 'EXAMINATION GUIDE' AND UPPER(CLASS_NAME) = 'JUNIOR HIGH SCHOOL(M1-3)' AND UPPER(SUBCLASS_NAME) = 'MATHEMATICS M1-3' THEN 'EDUCATION MATERIAL'
					WHEN UPPER(DEPT_NAME) LIKE 'CHILDREN%FASHION' AND UPPER(SUBDEPT_NAME) = 'NOVELTY' AND UPPER(CLASS_NAME) = 'TOON S CORNER' AND UPPER(SUBCLASS_NAME) = 'GIFT & TOY' THEN 'TOY'
					WHEN UPPER(DEPT_NAME) LIKE 'CHILDREN%FASHION' AND UPPER(SUBDEPT_NAME) LIKE 'CHILDREN%SHOES' AND UPPER(CLASS_NAME) = 'GIRLS SHOES' AND UPPER(SUBCLASS_NAME) = 'CASUAL' THEN 'FOOTWEAR'
					WHEN UPPER(DEPT_NAME) = 'STATIONERY' AND UPPER(SUBDEPT_NAME) = 'WRITING INSTRUMENT & SCHO' AND UPPER(CLASS_NAME) = 'SCHOOL SUPPLY' AND UPPER(SUBCLASS_NAME) = 'PENCIL SHARPENER:ELECTRON' THEN 'STATIONERY'
					WHEN UPPER(DEPT_NAME) = 'GARMENT' AND UPPER(SUBDEPT_NAME) = 'CHILDRENS WEAR' AND UPPER(CLASS_NAME) = 'INNERWEAR / ACCESSORIES' AND UPPER(SUBCLASS_NAME) = 'INNER S/S' THEN 'DAYWEAR'
					WHEN UPPER(DEPT_NAME) = 'FOREIGN BOOKS' AND UPPER(SUBDEPT_NAME) = 'CHILDRENS TEENAGE&EDUCATI' AND UPPER(CLASS_NAME) = 'CHILDRENS FICTION&TRUE' AND UPPER(SUBCLASS_NAME) = 'COMIC STRIP FICTION/GRAPH' THEN 'BOOK'
					WHEN UPPER(DEPT_NAME) LIKE 'CHILDREN%FASHION' AND UPPER(SUBDEPT_NAME) = 'NOVELTY' AND UPPER(CLASS_NAME) = 'TOON S CORNER' AND UPPER(SUBCLASS_NAME) = 'HOME FURNISHING' THEN 'TOY'
					WHEN UPPER(DEPT_NAME) = 'THAI BOOK' AND UPPER(SUBDEPT_NAME) = 'EXAMINATION GUIDE' AND UPPER(CLASS_NAME) = 'JUNIOR HIGH SCHOOL(M1-3)' AND UPPER(SUBCLASS_NAME) = 'SOCIAL M1-3' THEN 'EDUCATION MATERIAL'
					WHEN UPPER(DEPT_NAME) = 'PLAY & LEARN' AND UPPER(SUBDEPT_NAME) = 'GAME & BUILDING' AND UPPER(CLASS_NAME) = 'BUILDING & BLOCK' AND UPPER(SUBCLASS_NAME) = 'BLOCK SET' THEN 'STATIONERY'
					WHEN UPPER(DEPT_NAME) = 'FOREIGN BOOKS' AND UPPER(SUBDEPT_NAME) = 'CHILDRENS TEENAGE&EDUCATI' AND UPPER(CLASS_NAME) = 'CHILDRENS FICTION&TRUE' AND UPPER(SUBCLASS_NAME) = 'SHORT STORIES' THEN 'BOOK'
					WHEN UPPER(DEPT_NAME) = 'THAI BOOK' AND UPPER(SUBDEPT_NAME) = 'PSYCHOLOGY' AND UPPER(CLASS_NAME) = 'MOTHER & CHILD' AND UPPER(SUBCLASS_NAME) = 'CHILDCARE' THEN 'BOOK'
					WHEN UPPER(DEPT_NAME) = 'FOREIGN BOOKS' AND UPPER(SUBDEPT_NAME) = 'CHILDRENS TEENAGE&EDUCATI' AND UPPER(CLASS_NAME) = 'CHILDRENS FICTION&TRUE' AND UPPER(SUBCLASS_NAME) = 'CLASSIC FICTION' THEN 'BOOK'
					WHEN UPPER(DEPT_NAME) = 'PLAY & LEARN' AND UPPER(SUBDEPT_NAME) = 'GAME & BUILDING' AND UPPER(CLASS_NAME) = 'GAME' AND UPPER(SUBCLASS_NAME) = 'CARD GAME' THEN 'STATIONERY'
					WHEN UPPER(DEPT_NAME) = 'FOREIGN BOOKS' AND UPPER(SUBDEPT_NAME) = 'CHILDRENS TEENAGE&EDUCATI' AND UPPER(CLASS_NAME) = 'CHILDRENS EDUCATIONAL MAT' AND UPPER(SUBCLASS_NAME) = 'ENGLISH LANGUAGE&LITERACY' THEN 'EDUCATION MATERIAL'
					WHEN UPPER(DEPT_NAME) = 'THAI BOOK' AND UPPER(SUBDEPT_NAME) = 'MEDIA EDUCATION AND EDUTA' AND UPPER(CLASS_NAME) = 'CREATING MEDIA FOR KID' AND UPPER(SUBCLASS_NAME) = 'WIT PRACTICE' THEN 'BOOK'
					WHEN UPPER(DEPT_NAME) = 'THAI BOOK' AND UPPER(SUBDEPT_NAME) = 'CHILDREN' AND UPPER(CLASS_NAME) = 'EDUCATION MEDIA' AND UPPER(SUBCLASS_NAME) = 'EDU MEDIA BOOK SET' THEN 'EDUCATION MATERIAL'
					WHEN UPPER(DEPT_NAME) = 'GARMENT' AND UPPER(SUBDEPT_NAME) = 'CHILDRENS WEAR' AND UPPER(CLASS_NAME) = 'INNERWEAR / ACCESSORIES' AND UPPER(SUBCLASS_NAME) = 'INNER GIRLS BOTTOMS' THEN 'DAYWEAR'
					WHEN UPPER(DEPT_NAME) = 'GARMENT' AND UPPER(SUBDEPT_NAME) = 'CHILDRENS WEAR' AND UPPER(CLASS_NAME) = 'TODDLER' AND UPPER(SUBCLASS_NAME) = 'G SHIRT' THEN 'DAYWEAR'
					WHEN UPPER(DEPT_NAME) = 'THAI BOOK' AND UPPER(SUBDEPT_NAME) = 'CHILDREN' AND UPPER(CLASS_NAME) = 'KINDERGARDEN - PRIMARY SC' AND UPPER(SUBCLASS_NAME) = 'SCIENCE' THEN 'EDUCATION MATERIAL'
					WHEN UPPER(DEPT_NAME) = 'FOREIGN BOOKS' AND UPPER(SUBDEPT_NAME) = 'CHILDRENS TEENAGE&EDUCATI' AND UPPER(CLASS_NAME) = 'TEENAGE FICTION & TRUE' AND UPPER(SUBCLASS_NAME) = 'OTHERS' THEN 'BOOK'
					WHEN UPPER(DEPT_NAME) = 'FOREIGN BOOKS' AND UPPER(SUBDEPT_NAME) = 'CHILDRENS TEENAGE&EDUCATI' AND UPPER(CLASS_NAME) = 'TEENAGE FICTION & TRUE' AND UPPER(SUBCLASS_NAME) = 'FANTASY & MAGICAL REALISM' THEN 'BOOK'
					WHEN UPPER(DEPT_NAME) LIKE 'CHILDREN%FASHION' AND UPPER(SUBDEPT_NAME) = 'NOVELTY' AND UPPER(CLASS_NAME) = 'TOON S CORNER' AND UPPER(SUBCLASS_NAME) = 'STATIONERY' THEN 'STATIONERY'
					WHEN UPPER(DEPT_NAME) = 'FOREIGN BOOKS' AND UPPER(SUBDEPT_NAME) = 'CHILDRENS TEENAGE&EDUCATI' AND UPPER(CLASS_NAME) = 'TEENAGE FICTION & TRUE' AND UPPER(SUBCLASS_NAME) = 'GENERAL FICTION' THEN 'BOOK'
					WHEN UPPER(DEPT_NAME) = 'GARMENT' AND UPPER(SUBDEPT_NAME) = 'CHILDRENS WEAR' AND UPPER(CLASS_NAME) = 'INNERWEAR / ACCESSORIES' AND UPPER(SUBCLASS_NAME) = 'INNER TANK TOP' THEN 'DAYWEAR'
					WHEN UPPER(DEPT_NAME) = 'CROCS' AND UPPER(SUBDEPT_NAME) = 'FOOTWEAR' AND UPPER(CLASS_NAME) = 'KID BOY' AND UPPER(SUBCLASS_NAME) = 'SANDAL' THEN 'FOOTWEAR'
					WHEN UPPER(DEPT_NAME) = 'GARMENT' AND UPPER(SUBDEPT_NAME) = 'CHILDRENS WEAR' AND UPPER(CLASS_NAME) = 'TODDLER' AND UPPER(SUBCLASS_NAME) = 'B WOVEN CUT LENGTHGTH PANTS' THEN 'DAYWEAR'
					WHEN UPPER(DEPT_NAME) = 'GARMENT' AND UPPER(SUBDEPT_NAME) = 'CHILDRENS WEAR' AND UPPER(CLASS_NAME) = 'INNERWEAR / ACCESSORIES' AND UPPER(SUBCLASS_NAME) = 'INNER BOYS BOTTOMS' THEN 'DAYWEAR'
					WHEN UPPER(DEPT_NAME) = 'THAI BOOK' AND UPPER(SUBDEPT_NAME) = 'EXAMINATION GUIDE' AND UPPER(CLASS_NAME) = 'JUNIOR PRIMARY SCHOOL' AND UPPER(SUBCLASS_NAME) = 'SCIENCE' THEN 'EDUCATION MATERIAL'
					WHEN UPPER(DEPT_NAME) = 'FOREIGN BOOKS' AND UPPER(SUBDEPT_NAME) = 'CHILDRENS TEENAGE&EDUCATI' AND UPPER(CLASS_NAME) = 'CHILDRENS EDUCATIONAL MAT' AND UPPER(SUBCLASS_NAME) = 'SCIENCES GENERAL SCIENCE' THEN 'EDUCATION MATERIAL'
					WHEN UPPER(DEPT_NAME) = 'FOREIGN BOOK' AND UPPER(SUBDEPT_NAME) = 'CHILDREN' AND UPPER(CLASS_NAME) = 'AGES 4-8/EDUCATIONAL BOOK' AND UPPER(SUBCLASS_NAME) = 'LANGUAGE' THEN 'EDUCATION MATERIAL'
					WHEN UPPER(DEPT_NAME) = 'FOREIGN BOOKS' AND UPPER(SUBDEPT_NAME) = 'CHILDRENS TEENAGE&EDUCATI' AND UPPER(CLASS_NAME) = 'TEENAGE FICTION & TRUE' AND UPPER(SUBCLASS_NAME) = 'CLASSIC FICTION' THEN 'BOOK'
					WHEN UPPER(DEPT_NAME) = 'FOREIGN BOOKS' AND UPPER(SUBDEPT_NAME) = 'CHILDRENS TEENAGE&EDUCATI' AND UPPER(CLASS_NAME) = 'CHILDRENS FICTION&TRUE' AND UPPER(SUBCLASS_NAME) = 'ANIMAL STORIES' THEN 'BOOK'
					WHEN UPPER(DEPT_NAME) = 'FOREIGN BOOKS' AND UPPER(SUBDEPT_NAME) = 'CHILDRENS TEENAGE&EDUCATI' AND UPPER(CLASS_NAME) = 'CHILDRENS EDUCATIONAL MAT' AND UPPER(SUBCLASS_NAME) = 'GENERAL STUDIES/STUDY SKI' THEN 'EDUCATION MATERIAL'
					WHEN UPPER(DEPT_NAME) LIKE 'CHILDREN%FASHION' AND UPPER(SUBDEPT_NAME) = 'ZEENZONE' AND UPPER(CLASS_NAME) = 'PLUSH' AND UPPER(SUBCLASS_NAME) = 'STATIONERY' THEN 'STATIONERY'
					WHEN UPPER(DEPT_NAME) = 'THAI BOOK' AND UPPER(SUBDEPT_NAME) = 'SPECIAL BOOK(EVENT)' AND UPPER(CLASS_NAME) = 'BOOK PRICE CHILDREN' AND UPPER(SUBCLASS_NAME) = 'OTHERS/IN-VAT' THEN 'BOOK'
					WHEN UPPER(DEPT_NAME) = 'THAI BOOK' AND UPPER(SUBDEPT_NAME) = 'EXAMINATION GUIDE' AND UPPER(CLASS_NAME) = 'PRIMARY SCHOOL' AND UPPER(SUBCLASS_NAME) = 'ENGLISH LANGUAGES' THEN 'EDUCATION MATERIAL'
					WHEN UPPER(DEPT_NAME) = 'PLAY & LEARN' AND UPPER(SUBDEPT_NAME) = 'EDUCATION TOY' AND UPPER(CLASS_NAME) = 'EDUCATIONAL' AND UPPER(SUBCLASS_NAME) = 'PLAY SETS' THEN 'STATIONERY'
					WHEN UPPER(DEPT_NAME) = 'GARMENT' AND UPPER(SUBDEPT_NAME) = 'CHILDRENS WEAR' AND UPPER(CLASS_NAME) = 'INNERWEAR / ACCESSORIES' AND UPPER(SUBCLASS_NAME) = 'PAJAMA C&S L/S' THEN 'DAYWEAR'
					WHEN UPPER(DEPT_NAME) = 'THAI BOOK' AND UPPER(SUBDEPT_NAME) = 'EXAMINATION GUIDE' AND UPPER(CLASS_NAME) = 'SENIOR HIGH SCHOOL(M4-6)' AND UPPER(SUBCLASS_NAME) = 'ENGLISH LANGUAGES M 4-6' THEN 'EDUCATION MATERIAL'
					WHEN UPPER(DEPT_NAME) = 'FOREIGN BOOKS' AND UPPER(SUBDEPT_NAME) = 'CHILDRENS TEENAGE&EDUCATI' AND UPPER(CLASS_NAME) = 'CHILDRENS GENERAL NON-FIC' AND UPPER(SUBCLASS_NAME) = 'GENERAL KNOWLEDGE& TRIVIA' THEN 'BOOK'
					WHEN UPPER(DEPT_NAME) = 'GARMENT' AND UPPER(SUBDEPT_NAME) = 'CHILDRENS WEAR' AND UPPER(CLASS_NAME) = 'INNERWEAR / ACCESSORIES' AND UPPER(SUBCLASS_NAME) = 'PAJAMA WOVEN L/S' THEN 'DAYWEAR'
					WHEN UPPER(DEPT_NAME) = 'FOREIGN BOOKS' AND UPPER(SUBDEPT_NAME) = 'CHILDRENS TEENAGE&EDUCATI' AND UPPER(CLASS_NAME) = 'CHILDRENS FICTION&TRUE' AND UPPER(SUBCLASS_NAME) = 'ADVENTURE STORIES' THEN 'BOOK'
					WHEN UPPER(DEPT_NAME) = 'FOREIGN BOOK' AND UPPER(SUBDEPT_NAME) = 'CHILDREN' AND UPPER(CLASS_NAME) = 'AGES 4-8/EDUCATIONAL BOOK' AND UPPER(SUBCLASS_NAME) = 'REFERENCE' THEN 'EDUCATION MATERIAL'
					WHEN UPPER(DEPT_NAME) = 'FOREIGN BOOKS' AND UPPER(SUBDEPT_NAME) = 'CHILDRENS TEENAGE&EDUCATI' AND UPPER(CLASS_NAME) = 'CHILDRENS GENERAL NON-FIC' AND UPPER(SUBCLASS_NAME) = 'NATURAL HISTORY' THEN 'EDUCATION MATERIAL'
					WHEN UPPER(DEPT_NAME) = 'FOREIGN BOOKS' AND UPPER(SUBDEPT_NAME) = 'CHILDRENS TEENAGE&EDUCATI' AND UPPER(CLASS_NAME) = 'TEENAGE GENERAL NON-FICTI' AND UPPER(SUBCLASS_NAME) = 'TELEVISION & FILM' THEN 'BOOK'
					WHEN UPPER(DEPT_NAME) = 'FOREIGN BOOKS' AND UPPER(SUBDEPT_NAME) = 'CHILDRENS TEENAGE&EDUCATI' AND UPPER(CLASS_NAME) = 'CHILDRENS GENERAL NON-FIC' AND UPPER(SUBCLASS_NAME) = 'DRAMA & PERFORMING' THEN 'BOOK'
					WHEN UPPER(DEPT_NAME) LIKE 'CHILDREN%FASHION' AND UPPER(SUBDEPT_NAME) = 'ZEENZONE' AND UPPER(CLASS_NAME) = 'STATIONERY' AND UPPER(SUBCLASS_NAME) = 'PAPER SUPPLIES' THEN 'STATIONERY'
					WHEN UPPER(DEPT_NAME) = 'FOREIGN BOOKS' AND UPPER(SUBDEPT_NAME) = 'CHILDRENS TEENAGE&EDUCATI' AND UPPER(CLASS_NAME) = 'CHILDRENS EDUCATIONAL MAT' AND UPPER(SUBCLASS_NAME) = 'LANGUAGES OTHER THAN ENGL' THEN 'EDUCATION MATERIAL'
					WHEN UPPER(DEPT_NAME) = 'GARMENT' AND UPPER(SUBDEPT_NAME) = 'CHILDRENS WEAR' AND UPPER(CLASS_NAME) = 'INNERWEAR / ACCESSORIES' AND UPPER(SUBCLASS_NAME) = 'HATS' THEN 'DAYWEAR'
					WHEN UPPER(DEPT_NAME) = 'FOREIGN BOOKS' AND UPPER(SUBDEPT_NAME) = 'CHILDRENS TEENAGE&EDUCATI' AND UPPER(CLASS_NAME) = 'CHILDRENS REFERENCE MATER' AND UPPER(SUBCLASS_NAME) = 'ENCYCLOPAEDIAS' THEN 'EDUCATION MATERIAL'
					WHEN UPPER(DEPT_NAME) = 'THAI BOOK' AND UPPER(SUBDEPT_NAME) = 'EXAMINATION GUIDE' AND UPPER(CLASS_NAME) = 'SENIOR HIGH SCHOOL(M4-6)' AND UPPER(SUBCLASS_NAME) = 'PHYSIC M 4-6' THEN 'EDUCATION MATERIAL'
					WHEN UPPER(DEPT_NAME) = 'FOREIGN BOOKS' AND UPPER(SUBDEPT_NAME) = 'CHILDRENS TEENAGE&EDUCATI' AND UPPER(CLASS_NAME) = 'CHILDRENS FICTION&TRUE' AND UPPER(SUBCLASS_NAME) = 'OTHERS' THEN 'BOOK'
					WHEN UPPER(DEPT_NAME) LIKE 'CHILDREN%FASHION' AND UPPER(SUBDEPT_NAME) = 'ZEENZONE' AND UPPER(CLASS_NAME) = 'STATIONERY' AND UPPER(SUBCLASS_NAME) = 'DECORATIVE TOOLS' THEN 'STATIONERY'
					WHEN UPPER(DEPT_NAME) = 'ENTERTAINMENT MEDIA' AND UPPER(SUBDEPT_NAME) = 'ENTERTAINMENT MERCHANDISE' AND UPPER(CLASS_NAME) = 'TOY' AND UPPER(SUBCLASS_NAME) = 'MOVIE' THEN 'TOY'
					WHEN UPPER(DEPT_NAME) = 'FOREIGN BOOKS' AND UPPER(SUBDEPT_NAME) = 'CHILDRENS TEENAGE&EDUCATI' AND UPPER(CLASS_NAME) = 'CHILDRENS EDUCATIONAL MAT' AND UPPER(SUBCLASS_NAME) = 'MATHEMATICS & NUMERACY' THEN 'EDUCATION MATERIAL'
					WHEN UPPER(DEPT_NAME) = 'GARMENT' AND UPPER(SUBDEPT_NAME) = 'CHILDRENS WEAR' AND UPPER(CLASS_NAME) = 'TODDLER' AND UPPER(SUBCLASS_NAME) = 'B POLO' THEN 'DAYWEAR'
					WHEN UPPER(DEPT_NAME) = 'FOREIGN BOOKS' AND UPPER(SUBDEPT_NAME) = 'CHILDRENS TEENAGE&EDUCATI' AND UPPER(CLASS_NAME) = 'TEENAGE FICTION & TRUE' AND UPPER(SUBCLASS_NAME) = 'HORROR & GHOST STORIES' THEN 'BOOK'
					WHEN UPPER(DEPT_NAME) = 'FOREIGN BOOKS' AND UPPER(SUBDEPT_NAME) = 'CHILDRENS TEENAGE&EDUCATI' AND UPPER(CLASS_NAME) = 'TEENAGE FICTION & TRUE' AND UPPER(SUBCLASS_NAME) = 'HUMOROUS STORIES' THEN 'BOOK'
					WHEN UPPER(DEPT_NAME) = 'THAI BOOK' AND UPPER(SUBDEPT_NAME) = 'EXAMINATION GUIDE' AND UPPER(CLASS_NAME) = 'JUNIOR HIGH SCHOOL(M1-3)' AND UPPER(SUBCLASS_NAME) = 'ENTRACE BOOK GUIDE TO SEN' THEN 'BOOK'
					WHEN UPPER(DEPT_NAME) = 'THAI BOOK' AND UPPER(SUBDEPT_NAME) = 'EXAMINATION GUIDE' AND UPPER(CLASS_NAME) = 'JUNIOR PRIMARY SCHOOL' AND UPPER(SUBCLASS_NAME) = 'PRIMARY TEST GUIDE BOOK' THEN 'EDUCATION MATERIAL'
					WHEN UPPER(DEPT_NAME) = 'FOREIGN BOOKS' AND UPPER(SUBDEPT_NAME) = 'CHILDRENS TEENAGE&EDUCATI' AND UPPER(CLASS_NAME) = 'CHILDRENS GENERAL NON-FIC' AND UPPER(SUBCLASS_NAME) = 'TELEVISION& FILM' THEN 'BOOK'
					WHEN UPPER(DEPT_NAME) = 'GM/HOUSEWARES' AND UPPER(SUBDEPT_NAME) = 'FASHION' AND UPPER(CLASS_NAME) = 'BABY GOODS' AND UPPER(SUBCLASS_NAME) = 'GIFT SET' THEN 'GIFT SET'
					WHEN UPPER(DEPT_NAME) = 'STATIONERY' AND UPPER(SUBDEPT_NAME) = 'WRITING INSTRUMENT & SCHO' AND UPPER(CLASS_NAME) = 'SCHOOL SUPPLY' AND UPPER(SUBCLASS_NAME) = 'STATIONERY SET' THEN 'STATIONERY'
					WHEN UPPER(DEPT_NAME) = 'THAI BOOK' AND UPPER(SUBDEPT_NAME) = 'CHILDREN' AND UPPER(CLASS_NAME) = 'TALE' AND UPPER(SUBCLASS_NAME) = 'TALE BOOK SET' THEN 'BOOK'
					WHEN UPPER(DEPT_NAME) = 'THAI BOOK' AND UPPER(SUBDEPT_NAME) = 'EXAMINATION GUIDE' AND UPPER(CLASS_NAME) = 'JUNIOR PRIMARY SCHOOL' AND UPPER(SUBCLASS_NAME) = 'ENGLISH' THEN 'BOOK'
					WHEN UPPER(DEPT_NAME) = 'GARMENT' AND UPPER(SUBDEPT_NAME) = 'CHILDRENS WEAR' AND UPPER(CLASS_NAME) = 'TODDLER' AND UPPER(SUBCLASS_NAME) = 'G SKIRT' THEN 'DAYWEAR'
					WHEN UPPER(DEPT_NAME) = 'FOREIGN BOOKS' AND UPPER(SUBDEPT_NAME) = 'CHILDRENS TEENAGE&EDUCATI' AND UPPER(CLASS_NAME) = 'TEENAGE GENERAL NON-FICTI' AND UPPER(SUBCLASS_NAME) = 'HOBBIES QUIZZES & GAMES' THEN 'EDUCATION MATERIAL'
					WHEN UPPER(DEPT_NAME) = 'CASUAL FOOTWEAR' AND UPPER(SUBDEPT_NAME) = 'CASUAL SHOES' AND UPPER(CLASS_NAME) = 'CASUAL CHILD' AND UPPER(SUBCLASS_NAME) = 'CASUAL KIDS CR' THEN 'FOOTWEAR'
					WHEN UPPER(DEPT_NAME) = 'THAI BOOK' AND UPPER(SUBDEPT_NAME) = 'CHILDREN' AND UPPER(CLASS_NAME) = 'EDUCATION MEDIA' AND UPPER(SUBCLASS_NAME) = 'OTHERS/IN-VAT' THEN 'BOOK'
					WHEN UPPER(DEPT_NAME) = 'GARMENT' AND UPPER(SUBDEPT_NAME) = 'CHILDRENS WEAR' AND UPPER(CLASS_NAME) = 'TODDLER' AND UPPER(SUBCLASS_NAME) = 'B C&S CARDIGAN' THEN 'DAYWEAR'
					WHEN UPPER(DEPT_NAME) = 'FOREIGN BOOKS' AND UPPER(SUBDEPT_NAME) = 'CHILDRENS TEENAGE&EDUCATI' AND UPPER(CLASS_NAME) = 'CHILDRENS EDUCATIONAL MAT' AND UPPER(SUBCLASS_NAME) = 'TECHNOLOGY' THEN 'EDUCATION MATERIAL'
					WHEN UPPER(DEPT_NAME) = 'FOREIGN BOOKS' AND UPPER(SUBDEPT_NAME) = 'CHILDRENS TEENAGE&EDUCATI' AND UPPER(CLASS_NAME) = 'CHILDRENS FICTION&TRUE' AND UPPER(SUBCLASS_NAME) = 'FAMILY & HOME STORIES' THEN 'BOOK'
					WHEN UPPER(DEPT_NAME) = 'FOREIGN BOOKS' AND UPPER(SUBDEPT_NAME) = 'CHILDRENS TEENAGE&EDUCATI' AND UPPER(CLASS_NAME) = 'CHILDRENS FICTION&TRUE' AND UPPER(SUBCLASS_NAME) = 'SCIENCE FICTION' THEN 'BOOK'
					WHEN UPPER(DEPT_NAME) = 'FOREIGN BOOKS' AND UPPER(SUBDEPT_NAME) = 'CHILDRENS TEENAGE&EDUCATI' AND UPPER(CLASS_NAME) = 'TEENAGE REFERENCE MATERTI' AND UPPER(SUBCLASS_NAME) = 'REFERENCE WORKS' THEN 'EDUCATION MATERIAL'
					WHEN UPPER(DEPT_NAME) = 'THAI BOOK' AND UPPER(SUBDEPT_NAME) = 'EXAMINATION GUIDE' AND UPPER(CLASS_NAME) = 'PRIMARY SCHOOL' AND UPPER(SUBCLASS_NAME) = 'ENTRACE BOOK GUIDE TO JUN' THEN 'EDUCATION MATERIAL'
					WHEN UPPER(DEPT_NAME) = 'FOREIGN BOOKS' AND UPPER(SUBDEPT_NAME) = 'CHILDRENS TEENAGE&EDUCATI' AND UPPER(CLASS_NAME) = 'CHILDRENS FICTION&TRUE' AND UPPER(SUBCLASS_NAME) = 'HORROR & GHOST STORIES' THEN 'BOOK'
					WHEN UPPER(DEPT_NAME) = 'FOREIGN BOOKS' AND UPPER(SUBDEPT_NAME) = 'CHILDRENS TEENAGE&EDUCATI' AND UPPER(CLASS_NAME) = 'TEENAGE FICTION & TRUE' AND UPPER(SUBCLASS_NAME) = 'SPORTING STORIES' THEN 'BOOK'
					WHEN UPPER(DEPT_NAME) = 'FOREIGN BOOK' AND UPPER(SUBDEPT_NAME) = 'CHILDREN' AND UPPER(CLASS_NAME) = 'BABY - PRESCHOOL/ACTIVITY' AND UPPER(SUBCLASS_NAME) = 'PLAY&LEARN' THEN 'BOOK'
					WHEN UPPER(DEPT_NAME) = 'FOREIGN BOOKS' AND UPPER(SUBDEPT_NAME) = 'CHILDRENS TEENAGE&EDUCATI' AND UPPER(CLASS_NAME) = 'CHILDRENS EDUCATIONAL MAT' AND UPPER(SUBCLASS_NAME) = 'HISTORY' THEN 'EDUCATION MATERIAL'
					WHEN UPPER(DEPT_NAME) LIKE 'CHILDREN%FASHION' AND UPPER(SUBDEPT_NAME) = 'ZEENZONE' AND UPPER(CLASS_NAME) = 'STATIONERY' AND UPPER(SUBCLASS_NAME) = 'WRITING INSTRUMENT' THEN 'STATIONERY'
					WHEN UPPER(DEPT_NAME) = 'THAI BOOK' AND UPPER(SUBDEPT_NAME) = 'EXAMINATION GUIDE' AND UPPER(CLASS_NAME) = 'PRIMARY SCHOOL' AND UPPER(SUBCLASS_NAME) = 'LIVING EXPERIENCES LEARNI' THEN 'EDUCATION MATERIAL'
					WHEN UPPER(DEPT_NAME) = 'GARMENT' AND UPPER(SUBDEPT_NAME) = 'CHILDRENS WEAR' AND UPPER(CLASS_NAME) = 'INNERWEAR / ACCESSORIES' AND UPPER(SUBCLASS_NAME) = 'INNER CAMISOLE' THEN 'DAYWEAR'
					WHEN UPPER(DEPT_NAME) LIKE 'CHILDREN%FASHION' AND UPPER(SUBDEPT_NAME) = 'ZEENZONE' AND UPPER(CLASS_NAME) = 'FASHION' AND UPPER(SUBCLASS_NAME) = 'BAGS' THEN 'ACCESSORIES'
					WHEN UPPER(DEPT_NAME) = 'THAI BOOK' AND UPPER(SUBDEPT_NAME) = 'EXAMINATION GUIDE' AND UPPER(CLASS_NAME) = 'JUNIOR PRIMARY SCHOOL' AND UPPER(SUBCLASS_NAME) = 'IQ TEST' THEN 'EDUCATION MATERIAL'
					WHEN UPPER(DEPT_NAME) = 'HOUSEHOLD' AND UPPER(SUBDEPT_NAME) = 'STATIONERY' AND UPPER(CLASS_NAME) = 'TOY' AND UPPER(SUBCLASS_NAME) = 'ARTS AND CRAFTS GOODS' THEN 'ART&CRAFT'
					WHEN UPPER(DEPT_NAME) = 'THAI BOOK' AND UPPER(SUBDEPT_NAME) = 'CHILDREN' AND UPPER(CLASS_NAME) = 'EDUCATION MEDIA' AND UPPER(SUBCLASS_NAME) = 'CHINESE LANGUAGE' THEN 'EDUCATION MATERIAL'
					WHEN UPPER(DEPT_NAME) = 'FOREIGN BOOKS' AND UPPER(SUBDEPT_NAME) = 'CHILDRENS TEENAGE&EDUCATI' AND UPPER(CLASS_NAME) = 'CHILDRENS FICTION&TRUE' AND UPPER(SUBCLASS_NAME) = 'HISTORICAL FICTION' THEN 'BOOK'
					WHEN UPPER(DEPT_NAME) = 'FOREIGN BOOKS' AND UPPER(SUBDEPT_NAME) = 'CHILDRENS TEENAGE&EDUCATI' AND UPPER(CLASS_NAME) = 'TEENAGE FICTION & TRUE' AND UPPER(SUBCLASS_NAME) = 'FAMILY & HOME STORIES' THEN 'BOOK'
					WHEN UPPER(DEPT_NAME) = 'THAI BOOK' AND UPPER(SUBDEPT_NAME) = 'CHILDREN' AND UPPER(CLASS_NAME) = 'SPECIAL PROJECT' AND UPPER(SUBCLASS_NAME) = 'OTHERS/EXT VAT' THEN 'BOOK'
					WHEN UPPER(DEPT_NAME) = 'THAI BOOK' AND UPPER(SUBDEPT_NAME) = 'EXAMINATION GUIDE' AND UPPER(CLASS_NAME) = 'PRIMARY SCHOOL' AND UPPER(SUBCLASS_NAME) = 'MATHEMATICS' THEN 'EDUCATION MATERIAL'
					WHEN UPPER(DEPT_NAME) LIKE 'CHILDREN%FASHION' AND UPPER(SUBDEPT_NAME) = '4 - 8 BOYS' AND UPPER(CLASS_NAME) = 'SHORT PANTS' AND UPPER(SUBCLASS_NAME) = 'SHORT PANTS' THEN 'DAYWEAR'
					WHEN UPPER(DEPT_NAME) = 'THAI BOOK' AND UPPER(SUBDEPT_NAME) = 'EXAMINATION GUIDE' AND UPPER(CLASS_NAME) = 'JUNIOR PRIMARY SCHOOL' AND UPPER(SUBCLASS_NAME) = 'MATHEMATIC' THEN 'EDUCATION MATERIAL'
					WHEN UPPER(DEPT_NAME) = 'HARDLINE' AND UPPER(SUBDEPT_NAME) = 'BOOK' AND UPPER(CLASS_NAME) = 'CHILDREN' AND UPPER(SUBCLASS_NAME) = 'FAIRY TALE' THEN 'TALE BOOK'
					WHEN UPPER(DEPT_NAME) = 'HARDLINE' AND UPPER(SUBDEPT_NAME) = 'BOOK' AND UPPER(CLASS_NAME) = 'CHILDREN' AND UPPER(SUBCLASS_NAME) = 'GENERAL KNOWLEDGE' THEN 'EDUCATION MATERIAL'
					WHEN UPPER(DEPT_NAME) = 'HARDLINE' AND UPPER(SUBDEPT_NAME) = 'BOOK' AND UPPER(CLASS_NAME) = 'CHILDREN' AND UPPER(SUBCLASS_NAME) = 'SCHOOL BOOKS' THEN 'EDUCATION MATERIAL'
					WHEN UPPER(DEPT_NAME) = 'HARDLINE' AND UPPER(SUBDEPT_NAME) = 'BOOK' AND UPPER(CLASS_NAME) = 'CHILDREN' AND UPPER(SUBCLASS_NAME) = 'OTHER' THEN 'BOOK'
					WHEN UPPER(DEPT_NAME) = 'GM/HOUSEWARES' AND UPPER(SUBDEPT_NAME) = 'FASHION' AND UPPER(CLASS_NAME) = 'BABY GOODS' AND UPPER(SUBCLASS_NAME) = 'BATH ACCESSORIES' THEN 'BATHING'
					WHEN UPPER(DEPT_NAME) = 'GM/HOUSEWARES' AND UPPER(SUBDEPT_NAME) = 'FASHION' AND UPPER(CLASS_NAME) = 'BABY GOODS' AND UPPER(SUBCLASS_NAME) = 'WHEEL GOODS' AND PARTNER_SUBCATEGORY_NAME = 'CARRIERS' THEN 'BABY CARRIER'
					WHEN UPPER(DEPT_NAME) = 'GM/HOUSEWARES' AND UPPER(SUBDEPT_NAME) = 'FASHION' AND UPPER(CLASS_NAME) = 'BABY GOODS' AND UPPER(SUBCLASS_NAME) = 'WHEEL GOODS' AND PARTNER_SUBCATEGORY_NAME = 'CRADLES/BASSINET' THEN 'BABY BOUNCER'
					WHEN UPPER(DEPT_NAME) = 'GM/HOUSEWARES' AND UPPER(SUBDEPT_NAME) = 'FASHION' AND UPPER(CLASS_NAME) = 'BABY GOODS' AND UPPER(SUBCLASS_NAME) = 'WHEEL GOODS' AND (PARTNER_SUBCATEGORY_NAME = 'WALKER' OR PARTNER_SUBCATEGORY_NAME = 'OTHERS') THEN 'BABY WALKER'
					WHEN UPPER(DEPT_NAME) LIKE 'CHILDREN%FASHION' AND UPPER(SUBDEPT_NAME) LIKE 'BABY%ACCESSORY' AND (UPPER(CLASS_NAME) LIKE '%SAFETY%' OR UPPER(SUBCLASS_NAME) LIKE '%SAFETY%') THEN 'SAFETY'
					WHEN UPPER(DEPT_NAME) LIKE 'CHILDREN%FASHION' AND UPPER(SUBDEPT_NAME) LIKE 'CHILDREN%FURNITURE' AND (UPPER(CLASS_NAME) LIKE '%BATH%' OR UPPER(SUBCLASS_NAME) LIKE '%BATH%') THEN 'BATHING'
					WHEN UPPER(DEPT_NAME) = 'AEROPOSTALE' AND UPPER(SUBDEPT_NAME) = 'GIRLS' THEN 'DAYWEAR'
					WHEN UPPER(DEPT_NAME) = 'AEROPOSTALE' AND UPPER(SUBDEPT_NAME) = 'GIRLS ACCESSORIES' AND UPPER(CLASS_NAME) LIKE '%FOOTWEAR%' THEN 'FOOTWEAR'
					WHEN UPPER(DEPT_NAME) = 'AEROPOSTALE' AND UPPER(SUBDEPT_NAME) = 'GIRLS ACCESSORIES' THEN 'ACCESSORIES'
					WHEN UPPER(DEPT_NAME) = 'BABYSHOP' AND UPPER(SUBDEPT_NAME) = '61_NURSERY TRAVEL' THEN 'ACCESSORIES'
					WHEN UPPER(DEPT_NAME) = 'BABYSHOP' AND UPPER(SUBDEPT_NAME) = '46_ PERSONAL ACCESSORIES' THEN 'ACCESSORIES'
					WHEN UPPER(DEPT_NAME) = 'BABYSHOP' AND UPPER(SUBDEPT_NAME) = '25_BASICS NON FEEDING' AND UPPER(CLASS_NAME) = 'BABY CARE' THEN 'BATHING'
					WHEN UPPER(DEPT_NAME) = 'BABYSHOP' AND UPPER(SUBDEPT_NAME) = '25_BASICS NON FEEDING' AND UPPER(CLASS_NAME) = 'FEEDING & SOOTHING' AND UPPER(SUBCLASS_NAME) = 'FEEDING BOTTLES' THEN 'FEEDING BOTTLE AND NIPPLE'
					WHEN UPPER(DEPT_NAME) = 'BABYSHOP' AND UPPER(SUBDEPT_NAME) = '25_BASICS NON FEEDING' AND UPPER(CLASS_NAME) = 'FEEDING & SOOTHING' THEN 'ACCESSORIES'
					WHEN UPPER(DEPT_NAME) = 'BABYSHOP' AND UPPER(SUBDEPT_NAME) = '21_BASICS APPAREL REGULAR' AND UPPER(CLASS_NAME) = 'ACCESSORIES' AND UPPER(SUBCLASS_NAME) = 'BIB, CAP & OTHERS' THEN 'ACCESSORIES'
					WHEN UPPER(DEPT_NAME) = 'BABYSHOP' AND UPPER(SUBDEPT_NAME) = '21_BASICS APPAREL REGULAR' THEN 'DAYWEAR'
					WHEN UPPER(DEPT_NAME) = 'BABYSHOP' AND UPPER(SUBDEPT_NAME) = '23_BASICS APPAREL SEA' AND UPPER(CLASS_NAME) = 'ACCESSORIES' AND UPPER(SUBCLASS_NAME) = 'BIB, CAP & OTHERS' THEN 'ACCESSORIES'
					WHEN UPPER(DEPT_NAME) = 'BABYSHOP' AND UPPER(SUBDEPT_NAME) = '23_BASICS APPAREL SEA' THEN 'DAYWEAR'
					WHEN UPPER(DEPT_NAME) = 'BABYSHOP' AND UPPER(SUBDEPT_NAME) = '22_BASICS APPAREL NON SEA' AND UPPER(CLASS_NAME) = 'MULTIPACK BODYSUIT' AND UPPER(SUBCLASS_NAME) = 'BODYSUIT' THEN 'DAYWEAR'
					WHEN UPPER(DEPT_NAME) = 'BABYSHOP' AND UPPER(SUBDEPT_NAME) = '22_BASICS APPAREL NON SEA' AND UPPER(CLASS_NAME) = 'MULTIPACK GIFT & OTHERS' AND UPPER(SUBCLASS_NAME) = 'GIFT & OTHER SETS' THEN 'GIFT SET'
					WHEN UPPER(DEPT_NAME) = 'BABYSHOP' AND UPPER(SUBDEPT_NAME) = '24_BASICS NON BATH & BED' AND UPPER(CLASS_NAME) = 'TOWELS & ACCESSORIES' AND UPPER(SUBCLASS_NAME) = 'TOWELS' THEN 'FABRIC'
					WHEN UPPER(DEPT_NAME) = 'BABYSHOP' AND UPPER(SUBDEPT_NAME) = '24_BASICS NON BATH & BED' THEN 'BEDDING'
					WHEN UPPER(DEPT_NAME) = 'BABYSHOP' AND UPPER(SUBDEPT_NAME) IN ('35_CLOTHING BABY GIRLS','36_CLOTHING TODDLER GIRLS','37_CLOTHING YOUNGER GIRLS') THEN 'DAYWEAR'
					WHEN UPPER(DEPT_NAME) = 'BABYSHOP' AND UPPER(SUBDEPT_NAME) = '55_CLOTHING ESSENTIAL GIR' AND UPPER(CLASS_NAME) = 'HOSIERY' THEN 'DAYWEAR'
					WHEN UPPER(DEPT_NAME) = 'BABYSHOP' AND UPPER(SUBDEPT_NAME) = '55_CLOTHING ESSENTIAL GIR' AND UPPER(CLASS_NAME) = 'SLEEPWEAR' THEN 'NIGHTWEAR'
					WHEN UPPER(DEPT_NAME) = 'BABYSHOP' AND UPPER(SUBDEPT_NAME) = '55_CLOTHING ESSENTIAL GIR' AND UPPER(CLASS_NAME) = 'UNDERGARMENTS' THEN 'UNDERWEAR'
					WHEN UPPER(DEPT_NAME) = 'BABYSHOP' AND UPPER(SUBDEPT_NAME) = '62_NURSERY BEDROOM' AND UPPER(CLASS_NAME) = 'BASSINETS & CRADLES' AND UPPER(SUBCLASS_NAME) = 'BASSINETS & CRADLES' THEN 'BABY BOUNCER'
					WHEN UPPER(DEPT_NAME) = 'BABYSHOP' AND UPPER(SUBDEPT_NAME) = '62_NURSERY BEDROOM' AND UPPER(CLASS_NAME) = 'ROOM FURNITURE' AND UPPER(SUBCLASS_NAME) = 'SECURITIES' THEN 'SAFETY'
					WHEN UPPER(DEPT_NAME) = 'BABYSHOP' AND UPPER(SUBDEPT_NAME) = '62_NURSERY BEDROOM' AND UPPER(CLASS_NAME) = 'BATH' THEN 'BATHING'
					WHEN UPPER(DEPT_NAME) = 'BABYSHOP' AND UPPER(SUBDEPT_NAME) = '62_NURSERY BEDROOM' AND UPPER(CLASS_NAME) IN ('BEDS','ROOM FURNITURE') THEN 'BEDDING'
					WHEN UPPER(DEPT_NAME) = 'BABYSHOP' AND UPPER(SUBDEPT_NAME) = '63_NURSERY LIVING&DINING' AND UPPER(CLASS_NAME) = 'PLAYTIME' AND UPPER(SUBCLASS_NAME) IN ('BABY SEATS','ROCKERS') THEN 'SAFETY'
					WHEN UPPER(DEPT_NAME) = 'BABYSHOP' AND UPPER(SUBDEPT_NAME) = '63_NURSERY LIVING&DINING' AND UPPER(CLASS_NAME) = 'PLAYTIME' AND UPPER(SUBCLASS_NAME) IN ('BOUNCERS','SWINGS') THEN 'BABY BOUNCER'
					WHEN UPPER(DEPT_NAME) = 'BABYSHOP' AND UPPER(CLASS_NAME) IN ('ONE-PIECE','OUTERWEAR') THEN 'DAYWEAR'
					WHEN UPPER(DEPT_NAME) = 'BABYSHOP' AND UPPER(CLASS_NAME) IN ('SETS') THEN 'GIFT SET'
					WHEN UPPER(DEPT_NAME) = 'FASHION' AND UPPER(SUBDEPT_NAME) = 'CHILDREN FASHION' AND UPPER(CLASS_NAME) = 'GIRL FASHION' AND UPPER(SUBCLASS_NAME) = 'BOTTOM' THEN 'DAYWEAR'
					WHEN UPPER(DEPT_NAME) = 'GARMENT' AND UPPER(SUBDEPT_NAME) = 'CHILDRENS WEAR' AND UPPER(CLASS_NAME) LIKE 'BABY%' THEN 'DAYWEAR'
					WHEN UPPER(DEPT_NAME) = 'GARMENT' AND UPPER(SUBDEPT_NAME) = 'CHILDRENS WEAR' AND UPPER(CLASS_NAME) IN ('TODDLER','INNERWEAR / ACCESSORIES') THEN 'ACCESSORIES'
					WHEN UPPER(DEPT_NAME) = 'JUNIOR SPORTS' AND UPPER(SUBDEPT_NAME) = 'ACCESSORIES' THEN 'ACCESSORIES'
					WHEN UPPER(DEPT_NAME) = 'JUNIOR SPORTS' AND UPPER(SUBDEPT_NAME) = 'APPAREL' THEN 'DAYWEAR'
					WHEN UPPER(DEPT_NAME) = 'JUNIOR SPORTS' AND UPPER(SUBDEPT_NAME) = 'CONSIGNMENT' THEN 'ACCESSORIES'
					WHEN UPPER(DEPT_NAME) = 'JUNIOR SPORTS' AND UPPER(SUBDEPT_NAME) = 'FOOTWEAR' THEN 'FOOTWEAR'
					WHEN UPPER(DEPT_NAME) = 'JUNIOR SPORTS' AND UPPER(SUBDEPT_NAME) IN ('RACKET',';LF') THEN 'OUTDOOR'
					WHEN UPPER(DEPT_NAME) = 'NON MERCHADISE ITEM' THEN 'OTHER'
					WHEN UPPER(DEPT_NAME) = 'PAYLESS SHOESOURCE' AND UPPER(SUBDEPT_NAME) = '4  CHILDRENS' AND UPPER(CLASS_NAME) = '10 C ATHLETICS'THEN 'DAYWEAR'
					WHEN UPPER(DEPT_NAME) = 'PAYLESS SHOESOURCE' AND UPPER(SUBDEPT_NAME) = '4  CHILDRENS' AND UPPER(SUBCLASS_NAME) IN ('12 TOD CASUALS','02 CASUALS') THEN 'DAYWEAR'
					WHEN UPPER(DEPT_NAME) = 'PAYLESS SHOESOURCE' AND UPPER(SUBDEPT_NAME) = '4  CHILDRENS' AND UPPER(SUBCLASS_NAME) = '06 BEACH' THEN 'SWIMWEAR'
					WHEN UPPER(DEPT_NAME) = 'PAYLESS SHOESOURCE' AND UPPER(SUBDEPT_NAME) = '4  CHILDRENS' AND UPPER(SUBCLASS_NAME) = '04 BOOTS' THEN 'FOOTWEAR'
					WHEN UPPER(DEPT_NAME) LIKE 'CHILDREN%FASHION' AND UPPER(SUBDEPT_NAME) IN ('4 - 8 BOYS','4 - 8 GIRLS') THEN 'DAYWEAR'
					WHEN UPPER(DEPT_NAME) LIKE 'CHILDREN%FASHION' AND UPPER(SUBDEPT_NAME) LIKE 'CHILDREN%FURNITURE' AND UPPER(CLASS_NAME) = 'SAFETY ACCESS.' THEN 'SAFETY'
					WHEN UPPER(DEPT_NAME) LIKE 'CHILDREN%FASHION' AND UPPER(SUBDEPT_NAME) LIKE 'CHILDREN%FURNITURE' AND UPPER(CLASS_NAME) = 'LAYETTE' AND UPPER(SUBCLASS_NAME) = 'TOWELS' THEN 'FABRIC'
					WHEN UPPER(DEPT_NAME) LIKE 'CHILDREN%FASHION' AND UPPER(SUBDEPT_NAME) LIKE 'CHILDREN%FURNITURE' AND UPPER(CLASS_NAME) = 'LAYETTE' AND UPPER(SUBCLASS_NAME) IN ('SHEETS/PILLOWCASE','BLANKET') THEN 'BEDDING'
					WHEN UPPER(DEPT_NAME) LIKE 'CHILDREN%FASHION' AND UPPER(SUBDEPT_NAME) LIKE 'CHILDREN%FURNITURE' AND UPPER(CLASS_NAME) = 'LAYETTE' AND UPPER(SUBCLASS_NAME) IN ('CLOTH DIAPER','PAPER DIAPERS') THEN 'DIAPERS'
					WHEN UPPER(DEPT_NAME) LIKE 'CHILDREN%FASHION' AND UPPER(SUBDEPT_NAME) LIKE 'CHILDREN%FURNITURE' AND UPPER(CLASS_NAME) = 'LAYETTE' AND UPPER(SUBCLASS_NAME) = 'GIFT SETS' THEN 'GIFT SET'
					WHEN UPPER(DEPT_NAME) LIKE 'CHILDREN%FASHION' AND UPPER(SUBDEPT_NAME) LIKE 'CHILDREN%FURNITURE' AND UPPER(CLASS_NAME) = 'LAYETTE' AND UPPER(SUBCLASS_NAME) = 'OTHER' THEN 'OTHER'
					WHEN UPPER(DEPT_NAME) LIKE 'CHILDREN%FASHION' AND UPPER(SUBDEPT_NAME) = 'CHILDREN LUXE FASHION' AND UPPER(SUBCLASS_NAME) IN ('ACC BOXSET','ACC OTHER','BAG') THEN 'ACCESSORIES'
					WHEN UPPER(DEPT_NAME) LIKE 'CHILDREN%FASHION' AND UPPER(SUBDEPT_NAME) = 'CHILDREN LUXE FASHION' AND UPPER(SUBCLASS_NAME) IN ('COAT','CUT&SEWN','JACKET','JEANS','KNITTED WEAR','OVERALL','SUITS','NECKTIE','RTW OTHER') THEN 'DAYWEAR'
					WHEN UPPER(DEPT_NAME) LIKE 'CHILDREN%FASHION' AND UPPER(SUBDEPT_NAME) = 'CHILDREN LUXE FASHION' AND UPPER(SUBCLASS_NAME) IN ('SWIMWEAR') THEN 'SWIMWEAR'
					WHEN UPPER(DEPT_NAME) LIKE 'CHILDREN%FASHION' AND UPPER(SUBDEPT_NAME) = 'BUILD-A-BEAR WORKSHOP' AND UPPER(CLASS_NAME) = 'BOY 3-9' AND UPPER(SUBCLASS_NAME) = 'STUFFERS' THEN 'DRESS UP & PRETEND PLAY'
					WHEN UPPER(DEPT_NAME) LIKE 'CHILDREN%FASHION' AND UPPER(SUBDEPT_NAME) = 'BUILD-A-BEAR WORKSHOP' AND UPPER(CLASS_NAME) = 'SOUNDS' AND UPPER(SUBCLASS_NAME) = 'OTHERS' THEN 'DRESS UP & PRETEND PLAY'
					WHEN UPPER(DEPT_NAME) LIKE 'CHILDREN%FASHION' AND UPPER(SUBDEPT_NAME) = 'BUILD-A-BEAR WORKSHOP' AND UPPER(CLASS_NAME) = 'ACCESSORIES' THEN 'ACCESSORIES'
					WHEN UPPER(DEPT_NAME) LIKE 'CHILDREN%FASHION' AND UPPER(SUBDEPT_NAME) = 'BUILD-A-BEAR WORKSHOP' AND UPPER(CLASS_NAME) = 'CLOTHES' THEN 'DAYWEAR'
					WHEN UPPER(DEPT_NAME) LIKE 'CHILDREN%FASHION' AND UPPER(SUBDEPT_NAME) LIKE 'BABY%FURNITURE' AND UPPER(CLASS_NAME) = 'BABY ENTERTAINER' AND UPPER(SUBCLASS_NAME) = 'BATH DESK' THEN 'BATHING'
					WHEN UPPER(DEPT_NAME) LIKE 'CHILDREN%FASHION' AND UPPER(SUBDEPT_NAME) LIKE 'BABY%FURNITURE' AND UPPER(CLASS_NAME) = 'BABY ENTERTAINER' AND UPPER(SUBCLASS_NAME) = 'HIGHCHAIR' THEN 'SAFETY'
					WHEN UPPER(DEPT_NAME) LIKE 'CHILDREN%FASHION' AND UPPER(SUBDEPT_NAME) LIKE 'BABY%FURNITURE' AND UPPER(CLASS_NAME) = 'BABY ENTERTAINER' AND UPPER(SUBCLASS_NAME) = 'OTHER' THEN 'TOY'
					WHEN UPPER(DEPT_NAME) LIKE 'CHILDREN%FASHION' AND UPPER(SUBDEPT_NAME) LIKE 'BABY%FURNITURE' AND UPPER(CLASS_NAME) = 'BABY GEAR' AND UPPER(SUBCLASS_NAME) = 'OTHER' THEN 'ACCESSORIES'
					WHEN UPPER(DEPT_NAME) = 'FOOD' AND UPPER(SUBDEPT_NAME) = 'MILK & BABY FOOD' AND UPPER(CLASS_NAME) = 'NON-FRESH DAIRY' AND UPPER(SUBCLASS_NAME) = 'NUTRITIONAL SUPPLEMENTS' THEN 'MILK POWDER'
					WHEN UPPER(DEPT_NAME) LIKE 'CHILDREN%FASHION' AND UPPER(SUBDEPT_NAME) = 'MUJI' AND UPPER(CLASS_NAME) = 'GARMENT' THEN 'DAYWEAR'
					WHEN UPPER(DEPT_NAME) LIKE 'CHILDREN%FASHION' AND UPPER(SUBDEPT_NAME) = 'MUJI' AND UPPER(CLASS_NAME) = 'HOUSEHOLD' THEN 'ACCESSORIES'
					WHEN UPPER(DEPT_NAME) LIKE 'CHILDREN%FASHION' AND UPPER(SUBDEPT_NAME) = 'INFANT/TODDLERS' AND UPPER(CLASS_NAME) = 'LAYETTE' AND UPPER(SUBCLASS_NAME) = 'BLANKET' THEN 'BEDDING'
					WHEN UPPER(DEPT_NAME) LIKE 'CHILDREN%FASHION' AND UPPER(SUBDEPT_NAME) = 'INFANT/TODDLERS' AND UPPER(CLASS_NAME) = 'LAYETTE' AND UPPER(SUBCLASS_NAME) = 'CLOTH DIAPER' THEN 'DIAPERS'
					WHEN UPPER(DEPT_NAME) LIKE 'CHILDREN%FASHION' AND UPPER(SUBDEPT_NAME) = 'INFANT/TODDLERS' AND UPPER(CLASS_NAME) = 'LAYETTE' AND UPPER(SUBCLASS_NAME) = 'GIFT SETS' THEN 'GIFT SET'
					WHEN UPPER(DEPT_NAME) LIKE 'CHILDREN%FASHION' AND UPPER(SUBDEPT_NAME) = 'INFANT/TODDLERS' AND UPPER(CLASS_NAME) = 'LAYETTE' AND UPPER(SUBCLASS_NAME) = 'TOWELS' THEN 'FABRIC'
					WHEN UPPER(DEPT_NAME) LIKE 'CHILDREN%FASHION' AND UPPER(SUBDEPT_NAME) = 'INFANT/TODDLERS' AND UPPER(CLASS_NAME) = 'NEWBORN/INFANT' AND UPPER(SUBCLASS_NAME) = 'OTHER' THEN 'BEDDING'
					WHEN UPPER(DEPT_NAME) LIKE 'CHILDREN%FASHION' AND UPPER(SUBDEPT_NAME) LIKE 'CHILDREN%ACCESSORIES' AND UPPER(CLASS_NAME) = 'ACCESSORIES' AND UPPER(SUBCLASS_NAME) = 'COSTUME JEWELLERY' THEN 'DRESS UP & PRETEND PLAY'
					WHEN UPPER(DEPT_NAME) LIKE 'CHILDREN%FASHION' AND UPPER(SUBDEPT_NAME) LIKE 'CHILDREN%ACCESSORIES' AND UPPER(CLASS_NAME) = 'ACCESSORIES' AND UPPER(SUBCLASS_NAME) = 'SUNGLASSES' THEN 'ACCESSORIES'
					ELSE UPPER(TRIM(TYPE_FINAL)) END
WHERE TYPE_FINAL IS NULL
;

/*SUB CLASS*/

UPDATE VPM_DATA.PM_SKUTAGGING_KIDS_PRODUCT_MASTER
SET TYPE_SUBCLASS = TEMP.TYPE_FINAL
FROM
	(
		SELECT * 
		FROM 
			(
			SELECT A.PARTNER_CODE,A.DEPT_NAME,A.SUBDEPT_NAME,A.CLASS_NAME,A.SUBCLASS_NAME,A.CATEGORY_FINAL,A.TYPE_FINAL,TTL,COUNT(DISTINCT CASE WHEN TYPE_FINAL IS NOT NULL THEN  SKU_CODE END ) AS CNT_ITM
			,CASE WHEN TTL_ITM > 0 THEN COUNT(DISTINCT CASE WHEN TYPE_FINAL IS NOT NULL THEN  SKU_CODE END )*1.0/TTL_ITM*1.0 ELSE 0 END AS PERCENTAGE,TTL_ITM*1.0/TTL*1.0 AS RATIO,ROW_NUMBER()OVER(PARTITION BY A.PARTNER_CODE,A.DEPT_NAME,A.SUBDEPT_NAME,A.CLASS_NAME,A.SUBCLASS_NAME,A.CATEGORY_FINAL ORDER BY CASE WHEN TTL_ITM > 0 THEN COUNT(DISTINCT CASE WHEN TYPE_FINAL IS NOT NULL THEN  SKU_CODE END )*1.0/TTL_ITM*1.0 ELSE 0 END DESC NULLS LAST) RK 
			FROM VPM_DATA.PM_SKUTAGGING_KIDS_PRODUCT_MASTER A
				 INNER JOIN 
				   (
				   SELECT PARTNER_CODE,DEPT_NAME,SUBDEPT_NAME,CLASS_NAME,SUBCLASS_NAME,CATEGORY_FINAL,COUNT(*) AS TTL,COUNT(DISTINCT CASE WHEN TYPE_FINAL IS NOT NULL THEN  SKU_CODE END ) AS TTL_ITM
				   FROM VPM_DATA.PM_SKUTAGGING_KIDS_PRODUCT_MASTER
				   GROUP BY PARTNER_CODE,DEPT_NAME,SUBDEPT_NAME,CLASS_NAME,SUBCLASS_NAME,CATEGORY_FINAL
				   ) B
			ON A.PARTNER_CODE = B.PARTNER_CODE
			AND UPPER(TRIM(A.DEPT_NAME)) = UPPER(TRIM(B.DEPT_NAME))
			AND UPPER(TRIM(A.SUBDEPT_NAME)) = UPPER(TRIM(B.SUBDEPT_NAME))
			AND UPPER(TRIM(A.CLASS_NAME)) = UPPER(TRIM(B.CLASS_NAME))
			AND UPPER(TRIM(A.SUBCLASS_NAME)) = UPPER(TRIM(B.SUBCLASS_NAME))
			AND UPPER(TRIM(A.CATEGORY_FINAL)) = UPPER(TRIM(B.CATEGORY_FINAL))
			GROUP BY A.PARTNER_CODE,A.DEPT_NAME,A.SUBDEPT_NAME,A.CLASS_NAME,A.SUBCLASS_NAME,A.CATEGORY_FINAL,A.TYPE_FINAL,TTL,TTL_ITM
			) TMP 
	WHERE (CNT_ITM >= 50 OR  (RATIO >=0.5 AND CNT_ITM > 10)) AND PERCENTAGE > 0.85 AND RK = 1
	) TEMP
WHERE UPPER(TRIM(VPM_DATA.PM_SKUTAGGING_KIDS_PRODUCT_MASTER.PARTNER_CODE)) = UPPER(TRIM(TEMP.PARTNER_CODE))
AND UPPER(TRIM(VPM_DATA.PM_SKUTAGGING_KIDS_PRODUCT_MASTER.DEPT_NAME)) = UPPER(TRIM(TEMP.DEPT_NAME))
AND UPPER(TRIM(VPM_DATA.PM_SKUTAGGING_KIDS_PRODUCT_MASTER.SUBDEPT_NAME)) = UPPER(TRIM(TEMP.SUBDEPT_NAME))
AND UPPER(TRIM(VPM_DATA.PM_SKUTAGGING_KIDS_PRODUCT_MASTER.CLASS_NAME)) = UPPER(TRIM(TEMP.CLASS_NAME))
AND UPPER(TRIM(VPM_DATA.PM_SKUTAGGING_KIDS_PRODUCT_MASTER.SUBCLASS_NAME)) = UPPER(TRIM(TEMP.SUBCLASS_NAME))
AND UPPER(TRIM(VPM_DATA.PM_SKUTAGGING_KIDS_PRODUCT_MASTER.CATEGORY_FINAL)) = UPPER(TRIM(TEMP.CATEGORY_FINAL))
AND VPM_DATA.PM_SKUTAGGING_KIDS_PRODUCT_MASTER.TYPE_FINAL IS NULL 
;

/*CLASS*/
UPDATE VPM_DATA.PM_SKUTAGGING_KIDS_PRODUCT_MASTER
SET TYPE_SUBCLASS = TEMP.TYPE_FINAL
FROM
	(
	SELECT * 
	FROM 
		(
		SELECT A.PARTNER_CODE,A.DEPT_NAME,A.SUBDEPT_NAME,A.CLASS_NAME,A.CATEGORY_FINAL,A.TYPE_FINAL,TTL,COUNT(DISTINCT CASE WHEN TYPE_FINAL IS NOT NULL THEN  SKU_CODE END ) AS CNT_ITM
		,CASE WHEN TTL_ITM > 0 THEN COUNT(DISTINCT CASE WHEN TYPE_FINAL IS NOT NULL THEN  SKU_CODE END )*1.0/TTL_ITM*1.0 ELSE 0 END AS PERCENTAGE,TTL_ITM*1.0/TTL*1.0 AS RATIO,ROW_NUMBER()OVER(PARTITION BY A.PARTNER_CODE,A.DEPT_NAME,A.SUBDEPT_NAME,A.CLASS_NAME,A.CATEGORY_FINAL ORDER BY CASE WHEN TTL_ITM > 0 THEN COUNT(DISTINCT CASE WHEN TYPE_FINAL IS NOT NULL THEN  SKU_CODE END )*1.0/TTL_ITM*1.0 ELSE 0 END DESC NULLS LAST) RK 
		FROM VPM_DATA.PM_SKUTAGGING_KIDS_PRODUCT_MASTER A
		INNER JOIN 
			   (
			   SELECT PARTNER_CODE,DEPT_NAME,SUBDEPT_NAME,CLASS_NAME,CATEGORY_FINAL,COUNT(*) AS TTL,COUNT(DISTINCT CASE WHEN TYPE_FINAL IS NOT NULL THEN  SKU_CODE END ) AS TTL_ITM
			   FROM VPM_DATA.PM_SKUTAGGING_KIDS_PRODUCT_MASTER
			   GROUP BY PARTNER_CODE,DEPT_NAME,SUBDEPT_NAME,CLASS_NAME,CATEGORY_FINAL
			   ) B
		ON A.PARTNER_CODE = B.PARTNER_CODE
		AND UPPER(TRIM(A.DEPT_NAME)) = UPPER(TRIM(B.DEPT_NAME))
		AND UPPER(TRIM(A.SUBDEPT_NAME)) = UPPER(TRIM(B.SUBDEPT_NAME))
		AND UPPER(TRIM(A.CLASS_NAME)) = UPPER(TRIM(B.CLASS_NAME))
		AND UPPER(TRIM(A.CATEGORY_FINAL)) = UPPER(TRIM(B.CATEGORY_FINAL))
		GROUP BY A.PARTNER_CODE,A.DEPT_NAME,A.SUBDEPT_NAME,A.CLASS_NAME,A.CATEGORY_FINAL,A.TYPE_FINAL,TTL,TTL_ITM
		) TMP 
	WHERE (CNT_ITM >= 500 OR  (RATIO >=0.5 AND CNT_ITM > 50)) AND PERCENTAGE > 0.90 AND RK = 1
	) TEMP
WHERE  UPPER(TRIM(VPM_DATA.PM_SKUTAGGING_KIDS_PRODUCT_MASTER.PARTNER_CODE)) = UPPER(TRIM(TEMP.PARTNER_CODE))
AND UPPER(TRIM(VPM_DATA.PM_SKUTAGGING_KIDS_PRODUCT_MASTER.DEPT_NAME)) = UPPER(TRIM(TEMP.DEPT_NAME))
AND UPPER(TRIM(VPM_DATA.PM_SKUTAGGING_KIDS_PRODUCT_MASTER.SUBDEPT_NAME)) = UPPER(TRIM(TEMP.SUBDEPT_NAME))
AND UPPER(TRIM(VPM_DATA.PM_SKUTAGGING_KIDS_PRODUCT_MASTER.CATEGORY_FINAL)) = UPPER(TRIM(TEMP.CATEGORY_FINAL))
AND VPM_DATA.PM_SKUTAGGING_KIDS_PRODUCT_MASTER.TYPE_FINAL IS NULL 
AND VPM_DATA.PM_SKUTAGGING_KIDS_PRODUCT_MASTER.TYPE_SUBCLASS IS NULL
;
UPDATE VPM_DATA.PM_SKUTAGGING_KIDS_PRODUCT_MASTER
SET TYPE_FINAL = UPPER(TRIM(TYPE_SUBCLASS))
WHERE TYPE_FINAL IS NULL
;

 
/************************************************** GENDER **************************************************/
/*KEY WORD*/

UPDATE VPM_DATA.PM_SKUTAGGING_KIDS_PRODUCT_MASTER
SET KID_GENDER_KEYWORD = CASE 
								WHEN UPPER(CLASS_NAME) LIKE '%/GIRLS' THEN 'GIRL' --CDS/RBS REVISED PRODUCT MASTER
								WHEN UPPER(CLASS_NAME) LIKE '%/BOYS' THEN 'BOY' --CDS/RBS REVISED PRODUCT MASTER
								WHEN (UPPER(CLASS_NAME) LIKE '% OVERALL/DRESS' OR UPPER(CLASS_NAME) LIKE 'OVERALL/DRESS%' OR UPPER(CLASS_NAME) LIKE '% OVERALL/DRESS %') AND UPPER(TRIM(CATEGORY_FINAL)) = 'FASHION' THEN 'GIRL'
								WHEN (UPPER(CLASS_NAME) LIKE '% BLOUSE' OR UPPER(CLASS_NAME) LIKE 'BLOUSE%' OR UPPER(CLASS_NAME) LIKE '% BLOUSE %') AND UPPER(TRIM(CATEGORY_FINAL)) = 'FASHION' THEN 'GIRL'
								WHEN (UPPER(CLASS_NAME) LIKE '% SKIRTS' OR UPPER(CLASS_NAME) LIKE 'SKIRTS%' OR UPPER(CLASS_NAME) LIKE '% SKIRTS %') AND UPPER(TRIM(CATEGORY_FINAL)) = 'FASHION' THEN 'GIRL'
								WHEN (UPPER(CLASS_NAME) LIKE '% 1_APPARELS' OR UPPER(CLASS_NAME) LIKE '1_APPARELS%' OR UPPER(CLASS_NAME) LIKE '% 1_APPARELS %') AND UPPER(TRIM(CATEGORY_FINAL)) = 'FASHION' THEN 'GIRL'
								WHEN (UPPER(CLASS_NAME) LIKE '% BOY' OR UPPER(CLASS_NAME) LIKE 'BOY%' OR UPPER(CLASS_NAME) LIKE '% BOY %') AND UPPER(TRIM(CATEGORY_FINAL)) = 'FASHION' THEN 'BOY'
								WHEN (UPPER(CLASS_NAME) LIKE '% 27_UNDERWEAR-TODDLER' OR UPPER(CLASS_NAME) LIKE '27_UNDERWEAR-TODDLER%' OR UPPER(CLASS_NAME) LIKE '% 27_UNDERWEAR-TODDLER %') AND UPPER(TRIM(CATEGORY_FINAL)) = 'FASHION' THEN 'BOY'
								WHEN (UPPER(CLASS_NAME) LIKE '% 5_ACCESSORIES' OR UPPER(CLASS_NAME) LIKE '5_ACCESSORIES%' OR UPPER(CLASS_NAME) LIKE '% 5_ACCESSORIES %') AND UPPER(TRIM(CATEGORY_FINAL)) = 'FASHION' THEN 'GIRL'
								WHEN (UPPER(CLASS_NAME) LIKE '% 1_BOOTIES' OR UPPER(CLASS_NAME) LIKE '1_BOOTIES%' OR UPPER(CLASS_NAME) LIKE '% 1_BOOTIES %') AND UPPER(TRIM(CATEGORY_FINAL)) = 'FASHION' THEN 'GIRL'
								WHEN (UPPER(CLASS_NAME) LIKE '% 6_GIRLS' OR UPPER(CLASS_NAME) LIKE '6_GIRLS%' OR UPPER(CLASS_NAME) LIKE '% 6_GIRLS %') AND UPPER(TRIM(CATEGORY_FINAL)) = 'FASHION' THEN 'GIRL'
								WHEN (UPPER(CLASS_NAME) LIKE '% JUNIOR/BOYS' OR UPPER(CLASS_NAME) LIKE 'JUNIOR/BOYS%' OR UPPER(CLASS_NAME) LIKE '% JUNIOR/BOYS %') AND UPPER(TRIM(CATEGORY_FINAL)) = 'FASHION' THEN 'GIRL'
								WHEN (UPPER(CLASS_NAME) LIKE '% 25_UNDERWEAR-TODDLER' OR UPPER(CLASS_NAME) LIKE '25_UNDERWEAR-TODDLER%' OR UPPER(CLASS_NAME) LIKE '% 25_UNDERWEAR-TODDLER %') AND UPPER(TRIM(CATEGORY_FINAL)) = 'FASHION' THEN 'BOY'
								WHEN (UPPER(CLASS_NAME) LIKE '% GIRL' OR UPPER(CLASS_NAME) LIKE 'GIRL%' OR UPPER(CLASS_NAME) LIKE '% GIRL %') AND UPPER(TRIM(CATEGORY_FINAL)) = 'FASHION' THEN 'GIRL'
								WHEN (UPPER(CLASS_NAME) LIKE '% 37_BRIEF-' OR UPPER(CLASS_NAME) LIKE '37_BRIEF-%' OR UPPER(CLASS_NAME) LIKE '% 37_BRIEF- %') AND UPPER(TRIM(CATEGORY_FINAL)) = 'FASHION' THEN 'BOY'
								WHEN (UPPER(CLASS_NAME) LIKE '% HAIR' OR UPPER(CLASS_NAME) LIKE 'HAIR%' OR UPPER(CLASS_NAME) LIKE '% HAIR %') AND UPPER(TRIM(CATEGORY_FINAL)) = 'FASHION' THEN 'GIRL'
								WHEN (UPPER(CLASS_NAME) LIKE '% 11_NEWBORN' OR UPPER(CLASS_NAME) LIKE '11_NEWBORN%' OR UPPER(CLASS_NAME) LIKE '% 11_NEWBORN %') AND UPPER(TRIM(CATEGORY_FINAL)) = 'FASHION' THEN 'BOY'
								WHEN (UPPER(CLASS_NAME) LIKE '% 61_SHORTS-' OR UPPER(CLASS_NAME) LIKE '61_SHORTS-%' OR UPPER(CLASS_NAME) LIKE '% 61_SHORTS- %') AND UPPER(TRIM(CATEGORY_FINAL)) = 'FASHION' THEN 'BOY'
								WHEN (UPPER(CLASS_NAME) LIKE '% 13_SETS' OR UPPER(CLASS_NAME) LIKE '13_SETS%' OR UPPER(CLASS_NAME) LIKE '% 13_SETS %') AND UPPER(TRIM(CATEGORY_FINAL)) = 'FASHION' THEN 'BOY'
								WHEN (UPPER(CLASS_NAME) LIKE '% 4_GIRLS' OR UPPER(CLASS_NAME) LIKE '4_GIRLS%' OR UPPER(CLASS_NAME) LIKE '% 4_GIRLS %') AND UPPER(TRIM(CATEGORY_FINAL)) = 'FASHION' THEN 'GIRL'
								WHEN (UPPER(CLASS_NAME) LIKE '% 40_BOXER' OR UPPER(CLASS_NAME) LIKE '40_BOXER%' OR UPPER(CLASS_NAME) LIKE '% 40_BOXER %') AND UPPER(TRIM(CATEGORY_FINAL)) = 'FASHION' THEN 'BOY'
								WHEN (UPPER(CLASS_NAME) LIKE '% 14_LEGGING' OR UPPER(CLASS_NAME) LIKE '14_LEGGING%' OR UPPER(CLASS_NAME) LIKE '% 14_LEGGING %') AND UPPER(TRIM(CATEGORY_FINAL)) = 'FASHION' THEN 'GIRL'
								WHEN (UPPER(CLASS_NAME) LIKE '% GIRLS' OR UPPER(CLASS_NAME) LIKE 'GIRLS%' OR UPPER(CLASS_NAME) LIKE '% GIRLS %') AND UPPER(TRIM(CATEGORY_FINAL)) = 'FASHION' THEN 'GIRL'
								WHEN (UPPER(CLASS_NAME) LIKE '% GIRLS' OR UPPER(CLASS_NAME) LIKE 'GIRLS%' OR UPPER(CLASS_NAME) LIKE '% GIRLS %') AND UPPER(TRIM(CATEGORY_FINAL)) = 'FASHION' THEN 'GIRL'
								WHEN (UPPER(CLASS_NAME) LIKE '% SHIRTS' OR UPPER(CLASS_NAME) LIKE 'SHIRTS%' OR UPPER(CLASS_NAME) LIKE '% SHIRTS %') AND UPPER(TRIM(CATEGORY_FINAL)) = 'FASHION' THEN 'BOY'
								WHEN (UPPER(CLASS_NAME) LIKE '% BOYS' OR UPPER(CLASS_NAME) LIKE 'BOYS%' OR UPPER(CLASS_NAME) LIKE '% BOYS %') AND UPPER(TRIM(CATEGORY_FINAL)) = 'FASHION' THEN 'BOY'
								WHEN (UPPER(CLASS_NAME) LIKE '% GIRL' OR UPPER(CLASS_NAME) LIKE 'GIRL%' OR UPPER(CLASS_NAME) LIKE '% GIRL %') AND UPPER(TRIM(CATEGORY_FINAL)) = 'FASHION' THEN 'BOY'
								WHEN (UPPER(CLASS_NAME) LIKE '% POLO' OR UPPER(CLASS_NAME) LIKE 'POLO%' OR UPPER(CLASS_NAME) LIKE '% POLO %') AND UPPER(TRIM(CATEGORY_FINAL)) = 'FASHION' THEN 'BOY'
								WHEN (UPPER(CLASS_NAME) LIKE '% BOY' OR UPPER(CLASS_NAME) LIKE 'BOY%' OR UPPER(CLASS_NAME) LIKE '% BOY %') AND UPPER(TRIM(CATEGORY_FINAL)) = 'FASHION' THEN 'BOY'
								WHEN (UPPER(CLASS_NAME) LIKE '% FASHION' OR UPPER(CLASS_NAME) LIKE 'FASHION%' OR UPPER(CLASS_NAME) LIKE '% FASHION %') AND UPPER(TRIM(CATEGORY_FINAL)) = 'FASHION' THEN 'GIRL'
								WHEN (UPPER(CLASS_NAME) LIKE '% DRESSY' OR UPPER(CLASS_NAME) LIKE 'DRESSY%' OR UPPER(CLASS_NAME) LIKE '% DRESSY %') AND UPPER(TRIM(CATEGORY_FINAL)) = 'FASHION' THEN 'GIRL'
								WHEN (UPPER(CLASS_NAME) LIKE '% UNDERWEAR' OR UPPER(CLASS_NAME) LIKE 'UNDERWEAR%' OR UPPER(CLASS_NAME) LIKE '% UNDERWEAR %') AND UPPER(TRIM(CATEGORY_FINAL)) = 'FASHION' THEN 'GIRL'
								WHEN (UPPER(CLASS_NAME) LIKE '% BOTTOM' OR UPPER(CLASS_NAME) LIKE 'BOTTOM%' OR UPPER(CLASS_NAME) LIKE '% BOTTOM %') AND UPPER(TRIM(CATEGORY_FINAL)) = 'FASHION' THEN 'GIRL'
								WHEN (UPPER(CLASS_NAME) LIKE '% NEWBORN/INFANT' OR UPPER(CLASS_NAME) LIKE 'NEWBORN/INFANT%' OR UPPER(CLASS_NAME) LIKE '% NEWBORN/INFANT %') AND UPPER(TRIM(CATEGORY_FINAL)) = 'FASHION' THEN 'GIRL'
								WHEN (UPPER(CLASS_NAME) LIKE '% ACCESSORIES' OR UPPER(CLASS_NAME) LIKE 'ACCESSORIES%' OR UPPER(CLASS_NAME) LIKE '% ACCESSORIES %') AND UPPER(TRIM(CATEGORY_FINAL)) = 'FASHION' THEN 'GIRL'
								WHEN (UPPER(CLASS_NAME) LIKE '% VEST/JACKET/SWEATER' OR UPPER(CLASS_NAME) LIKE 'VEST/JACKET/SWEATER%' OR UPPER(CLASS_NAME) LIKE '% VEST/JACKET/SWEATER %') AND UPPER(TRIM(CATEGORY_FINAL)) = 'FASHION' THEN 'GIRL'
								WHEN (UPPER(CLASS_NAME) LIKE '% TOPS' OR UPPER(CLASS_NAME) LIKE 'TOPS%' OR UPPER(CLASS_NAME) LIKE '% TOPS %') AND UPPER(TRIM(CATEGORY_FINAL)) = 'FASHION' THEN 'GIRL'
								WHEN (UPPER(CLASS_NAME) LIKE '% GIRL' OR UPPER(CLASS_NAME) LIKE 'GIRL%' OR UPPER(CLASS_NAME) LIKE '% GIRL %') AND UPPER(TRIM(CATEGORY_FINAL)) = 'TOYS' THEN 'GIRL'
								WHEN (UPPER(CLASS_NAME) LIKE '% MODEL' OR UPPER(CLASS_NAME) LIKE 'MODEL%' OR UPPER(CLASS_NAME) LIKE '% MODEL %') AND UPPER(TRIM(CATEGORY_FINAL)) = 'TOYS' THEN 'BOY'
								WHEN (UPPER(CLASS_NAME) LIKE '% PLANES' OR UPPER(CLASS_NAME) LIKE 'PLANES%' OR UPPER(CLASS_NAME) LIKE '% PLANES %') AND UPPER(TRIM(CATEGORY_FINAL)) = 'TOYS' THEN 'BOY'
								WHEN (UPPER(CLASS_NAME) LIKE '% ACCESSORIES' OR UPPER(CLASS_NAME) LIKE 'ACCESSORIES%' OR UPPER(CLASS_NAME) LIKE '% ACCESSORIES %') AND UPPER(TRIM(CATEGORY_FINAL)) = 'TOYS' THEN 'GIRL'
								WHEN (UPPER(CLASS_NAME) LIKE '% BOY' OR UPPER(CLASS_NAME) LIKE 'BOY%' OR UPPER(CLASS_NAME) LIKE '% BOY %') AND UPPER(TRIM(CATEGORY_FINAL)) = 'TOYS' THEN 'BOY'
								WHEN (UPPER(CLASS_NAME) LIKE '% BOYS' OR UPPER(CLASS_NAME) LIKE 'BOYS%' OR UPPER(CLASS_NAME) LIKE '% BOYS %') AND UPPER(TRIM(CATEGORY_FINAL)) = 'TOYS' THEN 'BOY'
								WHEN (UPPER(CLASS_NAME) LIKE '% CARS' OR UPPER(CLASS_NAME) LIKE 'CARS%' OR UPPER(CLASS_NAME) LIKE '% CARS %') AND UPPER(TRIM(CATEGORY_FINAL)) = 'TOYS' THEN 'BOY'
								WHEN (UPPER(CLASS_NAME) LIKE '% GIRLS' OR UPPER(CLASS_NAME) LIKE 'GIRLS%' OR UPPER(CLASS_NAME) LIKE '% GIRLS %') AND UPPER(TRIM(CATEGORY_FINAL)) = 'TOYS' THEN 'GIRL'
								WHEN (UPPER(CLASS_NAME) LIKE '% POWER' OR UPPER(CLASS_NAME) LIKE 'POWER%' OR UPPER(CLASS_NAME) LIKE '% POWER %') AND UPPER(TRIM(CATEGORY_FINAL)) = 'TOYS' THEN 'BOY'
								WHEN (UPPER(CLASS_NAME) LIKE '% MECHANICAL' OR UPPER(CLASS_NAME) LIKE 'MECHANICAL%' OR UPPER(CLASS_NAME) LIKE '% MECHANICAL %') AND UPPER(TRIM(CATEGORY_FINAL)) = 'TOYS' THEN 'BOY'
								WHEN (UPPER(CLASS_NAME) LIKE '% TRAINS' OR UPPER(CLASS_NAME) LIKE 'TRAINS%' OR UPPER(CLASS_NAME) LIKE '% TRAINS %') AND UPPER(TRIM(CATEGORY_FINAL)) = 'TOYS' THEN 'BOY'
								WHEN (UPPER(CLASS_NAME) LIKE '% TOYS/DOLLS' OR UPPER(CLASS_NAME) LIKE 'TOYS/DOLLS%' OR UPPER(CLASS_NAME) LIKE '% TOYS/DOLLS %') AND UPPER(TRIM(CATEGORY_FINAL)) = 'TOYS' THEN 'GIRL'
								WHEN (UPPER(CLASS_NAME) LIKE '% BATTERY' OR UPPER(CLASS_NAME) LIKE 'BATTERY%' OR UPPER(CLASS_NAME) LIKE '% BATTERY %') AND UPPER(TRIM(CATEGORY_FINAL)) = 'TOYS' THEN 'BOY'
								WHEN (UPPER(CLASS_NAME) LIKE '% KITCHEN/SAND/BEAUTY' OR UPPER(CLASS_NAME) LIKE 'KITCHEN/SAND/BEAUTY%' OR UPPER(CLASS_NAME) LIKE '% KITCHEN/SAND/BEAUTY %') AND UPPER(TRIM(CATEGORY_FINAL)) = 'TOYS' THEN 'GIRL'
								WHEN (UPPER(SUBCLASS_NAME) LIKE '% OVERALL/DRESS' OR UPPER(SUBCLASS_NAME) LIKE 'OVERALL/DRESS%' OR UPPER(SUBCLASS_NAME) LIKE '% OVERALL/DRESS %') AND UPPER(TRIM(CATEGORY_FINAL)) = 'FASHION' THEN 'GIRL'
								WHEN (UPPER(SUBCLASS_NAME) LIKE '% TOP' OR UPPER(SUBCLASS_NAME) LIKE 'TOP%' OR UPPER(SUBCLASS_NAME) LIKE '% TOP %') AND UPPER(TRIM(CATEGORY_FINAL)) = 'FASHION' THEN 'GIRL'
								WHEN (UPPER(SUBCLASS_NAME) LIKE '% BLOUSE' OR UPPER(SUBCLASS_NAME) LIKE 'BLOUSE%' OR UPPER(SUBCLASS_NAME) LIKE '% BLOUSE %') AND UPPER(TRIM(CATEGORY_FINAL)) = 'FASHION' THEN 'GIRL'
								WHEN (UPPER(SUBCLASS_NAME) LIKE '% SKIRTS' OR UPPER(SUBCLASS_NAME) LIKE 'SKIRTS%' OR UPPER(SUBCLASS_NAME) LIKE '% SKIRTS %') AND UPPER(TRIM(CATEGORY_FINAL)) = 'FASHION' THEN 'GIRL'
								WHEN (UPPER(SUBCLASS_NAME) LIKE '% PYJAMAS/SWIMWEAR' OR UPPER(SUBCLASS_NAME) LIKE 'PYJAMAS/SWIMWEAR%' OR UPPER(SUBCLASS_NAME) LIKE '% PYJAMAS/SWIMWEAR %') AND UPPER(TRIM(CATEGORY_FINAL)) = 'FASHION' THEN 'GIRL'
								WHEN (UPPER(SUBCLASS_NAME) LIKE '% 1_TOPS' OR UPPER(SUBCLASS_NAME) LIKE '1_TOPS%' OR UPPER(SUBCLASS_NAME) LIKE '% 1_TOPS %') AND UPPER(TRIM(CATEGORY_FINAL)) = 'FASHION' THEN 'GIRL'
								WHEN (UPPER(SUBCLASS_NAME) LIKE '% BOY' OR UPPER(SUBCLASS_NAME) LIKE 'BOY%' OR UPPER(SUBCLASS_NAME) LIKE '% BOY %') AND UPPER(TRIM(CATEGORY_FINAL)) = 'FASHION' THEN 'BOY'
								WHEN (UPPER(SUBCLASS_NAME) LIKE '% 27_UNDERWEAR-TODDLER' OR UPPER(SUBCLASS_NAME) LIKE '27_UNDERWEAR-TODDLER%' OR UPPER(SUBCLASS_NAME) LIKE '% 27_UNDERWEAR-TODDLER %') AND UPPER(TRIM(CATEGORY_FINAL)) = 'FASHION' THEN 'BOY'
								WHEN (UPPER(SUBCLASS_NAME) LIKE '% 5_ACCESSORIES' OR UPPER(SUBCLASS_NAME) LIKE '5_ACCESSORIES%' OR UPPER(SUBCLASS_NAME) LIKE '% 5_ACCESSORIES %') AND UPPER(TRIM(CATEGORY_FINAL)) = 'FASHION' THEN 'GIRL'
								WHEN (UPPER(SUBCLASS_NAME) LIKE '% 1_BOOTIES' OR UPPER(SUBCLASS_NAME) LIKE '1_BOOTIES%' OR UPPER(SUBCLASS_NAME) LIKE '% 1_BOOTIES %') AND UPPER(TRIM(CATEGORY_FINAL)) = 'FASHION' THEN 'GIRL'
								WHEN (UPPER(SUBCLASS_NAME) LIKE '% 6_GIRLS' OR UPPER(SUBCLASS_NAME) LIKE '6_GIRLS%' OR UPPER(SUBCLASS_NAME) LIKE '% 6_GIRLS %') AND UPPER(TRIM(CATEGORY_FINAL)) = 'FASHION' THEN 'GIRL'
								WHEN (UPPER(SUBCLASS_NAME) LIKE '% JUNIOR/BOYS' OR UPPER(SUBCLASS_NAME) LIKE 'JUNIOR/BOYS%' OR UPPER(SUBCLASS_NAME) LIKE '% JUNIOR/BOYS %') AND UPPER(TRIM(CATEGORY_FINAL)) = 'FASHION' THEN 'GIRL'
								WHEN (UPPER(SUBCLASS_NAME) LIKE '% 25_UNDERWEAR-TODDLER' OR UPPER(SUBCLASS_NAME) LIKE '25_UNDERWEAR-TODDLER%' OR UPPER(SUBCLASS_NAME) LIKE '% 25_UNDERWEAR-TODDLER %') AND UPPER(TRIM(CATEGORY_FINAL)) = 'FASHION' THEN 'BOY'
								WHEN (UPPER(SUBCLASS_NAME) LIKE '% GIRL' OR UPPER(SUBCLASS_NAME) LIKE 'GIRL%' OR UPPER(SUBCLASS_NAME) LIKE '% GIRL %') AND UPPER(TRIM(CATEGORY_FINAL)) = 'FASHION' THEN 'GIRL'
								WHEN (UPPER(SUBCLASS_NAME) LIKE '% PANTY-SETS' OR UPPER(SUBCLASS_NAME) LIKE 'PANTY-SETS%' OR UPPER(SUBCLASS_NAME) LIKE '% PANTY-SETS %') AND UPPER(TRIM(CATEGORY_FINAL)) = 'FASHION' THEN 'BOY'
								WHEN (UPPER(SUBCLASS_NAME) LIKE '% 37_BRIEF-' OR UPPER(SUBCLASS_NAME) LIKE '37_BRIEF-%' OR UPPER(SUBCLASS_NAME) LIKE '% 37_BRIEF- %') AND UPPER(TRIM(CATEGORY_FINAL)) = 'FASHION' THEN 'BOY'
								WHEN (UPPER(SUBCLASS_NAME) LIKE '% HAIR' OR UPPER(SUBCLASS_NAME) LIKE 'HAIR%' OR UPPER(SUBCLASS_NAME) LIKE '% HAIR %') AND UPPER(TRIM(CATEGORY_FINAL)) = 'FASHION' THEN 'GIRL'
								WHEN (UPPER(SUBCLASS_NAME) LIKE '% BRIEF-' OR UPPER(SUBCLASS_NAME) LIKE 'BRIEF-%' OR UPPER(SUBCLASS_NAME) LIKE '% BRIEF- %') AND UPPER(TRIM(CATEGORY_FINAL)) = 'FASHION' THEN 'BOY'
								WHEN (UPPER(SUBCLASS_NAME) LIKE '% 4_GIRLS' OR UPPER(SUBCLASS_NAME) LIKE '4_GIRLS%' OR UPPER(SUBCLASS_NAME) LIKE '% 4_GIRLS %') AND UPPER(TRIM(CATEGORY_FINAL)) = 'FASHION' THEN 'GIRL'
								WHEN (UPPER(SUBCLASS_NAME) LIKE '% 40_BOXER' OR UPPER(SUBCLASS_NAME) LIKE '40_BOXER%' OR UPPER(SUBCLASS_NAME) LIKE '% 40_BOXER %') AND UPPER(TRIM(CATEGORY_FINAL)) = 'FASHION' THEN 'BOY'
								WHEN (UPPER(SUBCLASS_NAME) LIKE '% BAG/BELT' OR UPPER(SUBCLASS_NAME) LIKE 'BAG/BELT%' OR UPPER(SUBCLASS_NAME) LIKE '% BAG/BELT %') AND UPPER(TRIM(CATEGORY_FINAL)) = 'FASHION' THEN 'GIRL'
								WHEN (UPPER(SUBCLASS_NAME) LIKE '% GIRLS' OR UPPER(SUBCLASS_NAME) LIKE 'GIRLS%' OR UPPER(SUBCLASS_NAME) LIKE '% GIRLS %') AND UPPER(TRIM(CATEGORY_FINAL)) = 'FASHION' THEN 'GIRL'
								WHEN (UPPER(SUBCLASS_NAME) LIKE '% 16_INFANTS' OR UPPER(SUBCLASS_NAME) LIKE '16_INFANTS%' OR UPPER(SUBCLASS_NAME) LIKE '% 16_INFANTS %') AND UPPER(TRIM(CATEGORY_FINAL)) = 'FASHION' THEN 'GIRL'
								WHEN (UPPER(SUBCLASS_NAME) LIKE '% 31_BABY' OR UPPER(SUBCLASS_NAME) LIKE '31_BABY%' OR UPPER(SUBCLASS_NAME) LIKE '% 31_BABY %') AND UPPER(TRIM(CATEGORY_FINAL)) = 'FASHION' THEN 'BOY'
								WHEN (UPPER(SUBCLASS_NAME) LIKE '% 2_BOTTOMS' OR UPPER(SUBCLASS_NAME) LIKE '2_BOTTOMS%' OR UPPER(SUBCLASS_NAME) LIKE '% 2_BOTTOMS %') AND UPPER(TRIM(CATEGORY_FINAL)) = 'FASHION' THEN 'GIRL'
								WHEN (UPPER(SUBCLASS_NAME) LIKE '% GIRLS' OR UPPER(SUBCLASS_NAME) LIKE 'GIRLS%' OR UPPER(SUBCLASS_NAME) LIKE '% GIRLS %') AND UPPER(TRIM(CATEGORY_FINAL)) = 'FASHION' THEN 'GIRL'
								WHEN (UPPER(SUBCLASS_NAME) LIKE '% SHIRTS' OR UPPER(SUBCLASS_NAME) LIKE 'SHIRTS%' OR UPPER(SUBCLASS_NAME) LIKE '% SHIRTS %') AND UPPER(TRIM(CATEGORY_FINAL)) = 'FASHION' THEN 'BOY'
								WHEN (UPPER(SUBCLASS_NAME) LIKE '% BOYS' OR UPPER(SUBCLASS_NAME) LIKE 'BOYS%' OR UPPER(SUBCLASS_NAME) LIKE '% BOYS %') AND UPPER(TRIM(CATEGORY_FINAL)) = 'FASHION' THEN 'BOY'
								WHEN (UPPER(SUBCLASS_NAME) LIKE '% GIRL' OR UPPER(SUBCLASS_NAME) LIKE 'GIRL%' OR UPPER(SUBCLASS_NAME) LIKE '% GIRL %') AND UPPER(TRIM(CATEGORY_FINAL)) = 'FASHION' THEN 'BOY'
								WHEN (UPPER(SUBCLASS_NAME) LIKE '% POLO' OR UPPER(SUBCLASS_NAME) LIKE 'POLO%' OR UPPER(SUBCLASS_NAME) LIKE '% POLO %') AND UPPER(TRIM(CATEGORY_FINAL)) = 'FASHION' THEN 'BOY'
								WHEN (UPPER(SUBCLASS_NAME) LIKE '% BOY' OR UPPER(SUBCLASS_NAME) LIKE 'BOY%' OR UPPER(SUBCLASS_NAME) LIKE '% BOY %') AND UPPER(TRIM(CATEGORY_FINAL)) = 'FASHION' THEN 'BOY'
								WHEN (UPPER(SUBCLASS_NAME) LIKE '% DRESSY' OR UPPER(SUBCLASS_NAME) LIKE 'DRESSY%' OR UPPER(SUBCLASS_NAME) LIKE '% DRESSY %') AND UPPER(TRIM(CATEGORY_FINAL)) = 'FASHION' THEN 'GIRL'
								WHEN (UPPER(SUBCLASS_NAME) LIKE '% SHORT' OR UPPER(SUBCLASS_NAME) LIKE 'SHORT%' OR UPPER(SUBCLASS_NAME) LIKE '% SHORT %') AND UPPER(TRIM(CATEGORY_FINAL)) = 'FASHION' THEN 'BOY'
								WHEN (UPPER(SUBCLASS_NAME) LIKE '% UNDERWEAR' OR UPPER(SUBCLASS_NAME) LIKE 'UNDERWEAR%' OR UPPER(SUBCLASS_NAME) LIKE '% UNDERWEAR %') AND UPPER(TRIM(CATEGORY_FINAL)) = 'FASHION' THEN 'GIRL'
								WHEN (UPPER(SUBCLASS_NAME) LIKE '% BOTTOM' OR UPPER(SUBCLASS_NAME) LIKE 'BOTTOM%' OR UPPER(SUBCLASS_NAME) LIKE '% BOTTOM %') AND UPPER(TRIM(CATEGORY_FINAL)) = 'FASHION' THEN 'GIRL'
								WHEN (UPPER(SUBCLASS_NAME) LIKE '% PANTS' OR UPPER(SUBCLASS_NAME) LIKE 'PANTS%' OR UPPER(SUBCLASS_NAME) LIKE '% PANTS %') AND UPPER(TRIM(CATEGORY_FINAL)) = 'FASHION' THEN 'GIRL'
								WHEN (UPPER(SUBCLASS_NAME) LIKE '% T-SHIRTS' OR UPPER(SUBCLASS_NAME) LIKE 'T-SHIRTS%' OR UPPER(SUBCLASS_NAME) LIKE '% T-SHIRTS %') AND UPPER(TRIM(CATEGORY_FINAL)) = 'FASHION' THEN 'GIRL'
								WHEN (UPPER(SUBCLASS_NAME) LIKE '% ACCESSORIES' OR UPPER(SUBCLASS_NAME) LIKE 'ACCESSORIES%' OR UPPER(SUBCLASS_NAME) LIKE '% ACCESSORIES %') AND UPPER(TRIM(CATEGORY_FINAL)) = 'FASHION' THEN 'GIRL'
								WHEN (UPPER(SUBCLASS_NAME) LIKE '% SHOES' OR UPPER(SUBCLASS_NAME) LIKE 'SHOES%' OR UPPER(SUBCLASS_NAME) LIKE '% SHOES %') AND UPPER(TRIM(CATEGORY_FINAL)) = 'FASHION' THEN 'GIRL'
								WHEN (UPPER(SUBCLASS_NAME) LIKE '% MATERNITY' OR UPPER(SUBCLASS_NAME) LIKE 'MATERNITY%' OR UPPER(SUBCLASS_NAME) LIKE '% MATERNITY %') AND UPPER(TRIM(CATEGORY_FINAL)) = 'NECCESSITIES' THEN 'GIRL'
								WHEN (UPPER(SUBCLASS_NAME) LIKE '% HEALTHY' OR UPPER(SUBCLASS_NAME) LIKE 'HEALTHY%' OR UPPER(SUBCLASS_NAME) LIKE '% HEALTHY %') AND UPPER(TRIM(CATEGORY_FINAL)) = 'NECCESSITIES' THEN 'GIRL'
								WHEN (UPPER(SUBCLASS_NAME) LIKE '% GIRL' OR UPPER(SUBCLASS_NAME) LIKE 'GIRL%' OR UPPER(SUBCLASS_NAME) LIKE '% GIRL %') AND UPPER(TRIM(CATEGORY_FINAL)) = 'TOYS' THEN 'GIRL'
								WHEN (UPPER(SUBCLASS_NAME) LIKE '% MODEL' OR UPPER(SUBCLASS_NAME) LIKE 'MODEL%' OR UPPER(SUBCLASS_NAME) LIKE '% MODEL %') AND UPPER(TRIM(CATEGORY_FINAL)) = 'TOYS' THEN 'BOY'
								WHEN (UPPER(SUBCLASS_NAME) LIKE '% KITS' OR UPPER(SUBCLASS_NAME) LIKE 'KITS%' OR UPPER(SUBCLASS_NAME) LIKE '% KITS %') AND UPPER(TRIM(CATEGORY_FINAL)) = 'TOYS' THEN 'BOY'
								WHEN (UPPER(SUBCLASS_NAME) LIKE '% PLAY' OR UPPER(SUBCLASS_NAME) LIKE 'PLAY%' OR UPPER(SUBCLASS_NAME) LIKE '% PLAY %') AND UPPER(TRIM(CATEGORY_FINAL)) = 'TOYS' THEN 'GIRL'
								WHEN (UPPER(SUBCLASS_NAME) LIKE '% PLANES' OR UPPER(SUBCLASS_NAME) LIKE 'PLANES%' OR UPPER(SUBCLASS_NAME) LIKE '% PLANES %') AND UPPER(TRIM(CATEGORY_FINAL)) = 'TOYS' THEN 'BOY'
								WHEN (UPPER(SUBCLASS_NAME) LIKE '% BOY' OR UPPER(SUBCLASS_NAME) LIKE 'BOY%' OR UPPER(SUBCLASS_NAME) LIKE '% BOY %') AND UPPER(TRIM(CATEGORY_FINAL)) = 'TOYS' THEN 'BOY'
								WHEN (UPPER(SUBCLASS_NAME) LIKE '% BOYS' OR UPPER(SUBCLASS_NAME) LIKE 'BOYS%' OR UPPER(SUBCLASS_NAME) LIKE '% BOYS %') AND UPPER(TRIM(CATEGORY_FINAL)) = 'TOYS' THEN 'BOY'
								WHEN (UPPER(SUBCLASS_NAME) LIKE '% CARS' OR UPPER(SUBCLASS_NAME) LIKE 'CARS%' OR UPPER(SUBCLASS_NAME) LIKE '% CARS %') AND UPPER(TRIM(CATEGORY_FINAL)) = 'TOYS' THEN 'BOY'
								WHEN (UPPER(SUBCLASS_NAME) LIKE '% GIRLS' OR UPPER(SUBCLASS_NAME) LIKE 'GIRLS%' OR UPPER(SUBCLASS_NAME) LIKE '% GIRLS %') AND UPPER(TRIM(CATEGORY_FINAL)) = 'TOYS' THEN 'GIRL'
								WHEN (UPPER(SUBCLASS_NAME) LIKE '% NON' OR UPPER(SUBCLASS_NAME) LIKE 'NON%' OR UPPER(SUBCLASS_NAME) LIKE '% NON %') AND UPPER(TRIM(CATEGORY_FINAL)) = 'TOYS' THEN 'BOY'
								WHEN (UPPER(SUBCLASS_NAME) LIKE '% COLLECTIBLES' OR UPPER(SUBCLASS_NAME) LIKE 'COLLECTIBLES%' OR UPPER(SUBCLASS_NAME) LIKE '% COLLECTIBLES %') AND UPPER(TRIM(CATEGORY_FINAL)) = 'TOYS' THEN 'BOY'
								WHEN (UPPER(SUBCLASS_NAME) LIKE '% POWER' OR UPPER(SUBCLASS_NAME) LIKE 'POWER%' OR UPPER(SUBCLASS_NAME) LIKE '% POWER %') AND UPPER(TRIM(CATEGORY_FINAL)) = 'TOYS' THEN 'BOY'
								WHEN (UPPER(SUBCLASS_NAME) LIKE '% MECHANICAL' OR UPPER(SUBCLASS_NAME) LIKE 'MECHANICAL%' OR UPPER(SUBCLASS_NAME) LIKE '% MECHANICAL %') AND UPPER(TRIM(CATEGORY_FINAL)) = 'TOYS' THEN 'BOY'
								WHEN (UPPER(SUBCLASS_NAME) LIKE '% TRAINS' OR UPPER(SUBCLASS_NAME) LIKE 'TRAINS%' OR UPPER(SUBCLASS_NAME) LIKE '% TRAINS %') AND UPPER(TRIM(CATEGORY_FINAL)) = 'TOYS' THEN 'BOY'
								WHEN (UPPER(SUBCLASS_NAME) LIKE '% TOYS/DOLLS' OR UPPER(SUBCLASS_NAME) LIKE 'TOYS/DOLLS%' OR UPPER(SUBCLASS_NAME) LIKE '% TOYS/DOLLS %') AND UPPER(TRIM(CATEGORY_FINAL)) = 'TOYS' THEN 'GIRL'
								WHEN (UPPER(SUBCLASS_NAME) LIKE '% BATTERY' OR UPPER(SUBCLASS_NAME) LIKE 'BATTERY%' OR UPPER(SUBCLASS_NAME) LIKE '% BATTERY %') AND UPPER(TRIM(CATEGORY_FINAL)) = 'TOYS' THEN 'BOY'
								WHEN (UPPER(SUBCLASS_NAME) LIKE '% KITCHEN/SAND/BEAUTY' OR UPPER(SUBCLASS_NAME) LIKE 'KITCHEN/SAND/BEAUTY%' OR UPPER(SUBCLASS_NAME) LIKE '% KITCHEN/SAND/BEAUTY %') AND UPPER(TRIM(CATEGORY_FINAL)) = 'TOYS' THEN 'GIRL'
								WHEN (UPPER(SUBCLASS_NAME) LIKE '% BLOCKS' OR UPPER(SUBCLASS_NAME) LIKE 'BLOCKS%' OR UPPER(SUBCLASS_NAME) LIKE '% BLOCKS %') AND UPPER(TRIM(CATEGORY_FINAL)) = 'TOYS' THEN 'BOY'
								WHEN (UPPER(TRIM(PRODUCT_NAME)) LIKE '% ˭ԧ' OR UPPER(TRIM(PRODUCT_NAME)) LIKE '˭ԧ%' OR UPPER(TRIM(PRODUCT_NAME)) LIKE '% ˭ԧ %') AND UPPER(TRIM(CATEGORY_FINAL)) = 'NECCESSITIES' THEN 'GIRL'
								WHEN (UPPER(TRIM(PRODUCT_NAME)) LIKE '% �ѡ�����ŵ���PANTS˭ԧ' OR UPPER(TRIM(PRODUCT_NAME)) LIKE '�ѡ�����ŵ���PANTS˭ԧ%' OR UPPER(TRIM(PRODUCT_NAME)) LIKE '% �ѡ�����ŵ���PANTS˭ԧ %') AND UPPER(TRIM(CATEGORY_FINAL)) = 'NECCESSITIES' THEN 'GIRL'
								WHEN (UPPER(TRIM(PRODUCT_NAME)) LIKE '% ˭ԧL14' OR UPPER(TRIM(PRODUCT_NAME)) LIKE '˭ԧL14%' OR UPPER(TRIM(PRODUCT_NAME)) LIKE '% ˭ԧL14 %') AND UPPER(TRIM(CATEGORY_FINAL)) = 'NECCESSITIES' THEN 'GIRL'
								WHEN (UPPER(TRIM(PRODUCT_NAME)) LIKE '% ˭ԧL52' OR UPPER(TRIM(PRODUCT_NAME)) LIKE '˭ԧL52%' OR UPPER(TRIM(PRODUCT_NAME)) LIKE '% ˭ԧL52 %') AND UPPER(TRIM(CATEGORY_FINAL)) = 'NECCESSITIES' THEN 'GIRL'
								WHEN (UPPER(TRIM(PRODUCT_NAME)) LIKE '% ˭ԧM17' OR UPPER(TRIM(PRODUCT_NAME)) LIKE '˭ԧM17%' OR UPPER(TRIM(PRODUCT_NAME)) LIKE '% ˭ԧM17 %') AND UPPER(TRIM(CATEGORY_FINAL)) = 'NECCESSITIES' THEN 'GIRL'
								WHEN (UPPER(TRIM(PRODUCT_NAME)) LIKE '% ˭ԧXXL38' OR UPPER(TRIM(PRODUCT_NAME)) LIKE '˭ԧXXL38%' OR UPPER(TRIM(PRODUCT_NAME)) LIKE '% ˭ԧXXL38 %') AND UPPER(TRIM(CATEGORY_FINAL)) = 'NECCESSITIES' THEN 'GIRL'
								WHEN (UPPER(TRIM(PRODUCT_NAME)) LIKE '% GIRL' OR UPPER(TRIM(PRODUCT_NAME)) LIKE 'GIRL%' OR UPPER(TRIM(PRODUCT_NAME)) LIKE '% GIRL %') AND UPPER(TRIM(CATEGORY_FINAL)) = 'NECCESSITIES' THEN 'GIRL'
								WHEN (UPPER(TRIM(PRODUCT_NAME)) LIKE '% KITTY' OR UPPER(TRIM(PRODUCT_NAME)) LIKE 'KITTY%' OR UPPER(TRIM(PRODUCT_NAME)) LIKE '% KITTY %') AND UPPER(TRIM(CATEGORY_FINAL)) = 'NECCESSITIES' THEN 'GIRL'
								WHEN (UPPER(TRIM(PRODUCT_NAME)) LIKE '% �ѡ�����ŵ���PANTS���' OR UPPER(TRIM(PRODUCT_NAME)) LIKE '�ѡ�����ŵ���PANTS���%' OR UPPER(TRIM(PRODUCT_NAME)) LIKE '% �ѡ�����ŵ���PANTS��� %') AND UPPER(TRIM(CATEGORY_FINAL)) = 'NECCESSITIES' THEN 'BOY'
								WHEN (UPPER(TRIM(PRODUCT_NAME)) LIKE '% BOY' OR UPPER(TRIM(PRODUCT_NAME)) LIKE 'BOY%' OR UPPER(TRIM(PRODUCT_NAME)) LIKE '% BOY %') AND UPPER(TRIM(CATEGORY_FINAL)) = 'NECCESSITIES' THEN 'BOY'
								WHEN (UPPER(TRIM(PRODUCT_NAME)) LIKE '% ���' OR UPPER(TRIM(PRODUCT_NAME)) LIKE '���%' OR UPPER(TRIM(PRODUCT_NAME)) LIKE '% ��� %') AND UPPER(TRIM(CATEGORY_FINAL)) = 'NECCESSITIES' THEN 'BOY'
								WHEN (UPPER(TRIM(PRODUCT_NAME)) LIKE '% ���XXL38' OR UPPER(TRIM(PRODUCT_NAME)) LIKE '���XXL38%' OR UPPER(TRIM(PRODUCT_NAME)) LIKE '% ���XXL38 %') AND UPPER(TRIM(CATEGORY_FINAL)) = 'NECCESSITIES' THEN 'BOY'
								WHEN (UPPER(TRIM(PRODUCT_NAME)) LIKE '% ���XXL10' OR UPPER(TRIM(PRODUCT_NAME)) LIKE '���XXL10%' OR UPPER(TRIM(PRODUCT_NAME)) LIKE '% ���XXL10 %') AND UPPER(TRIM(CATEGORY_FINAL)) = 'NECCESSITIES' THEN 'BOY'
								WHEN (UPPER(TRIM(PRODUCT_NAME)) LIKE '% �M64' OR UPPER(TRIM(PRODUCT_NAME)) LIKE '�M64%' OR UPPER(TRIM(PRODUCT_NAME)) LIKE '% �M64 %') AND UPPER(TRIM(CATEGORY_FINAL)) = 'NECCESSITIES' THEN 'BOY'
								WHEN (UPPER(TRIM(PRODUCT_NAME)) LIKE '% ���XL46' OR UPPER(TRIM(PRODUCT_NAME)) LIKE '���XL46%' OR UPPER(TRIM(PRODUCT_NAME)) LIKE '% ���XL46 %') AND UPPER(TRIM(CATEGORY_FINAL)) = 'NECCESSITIES' THEN 'BOY'
								WHEN (UPPER(TRIM(PRODUCT_NAME)) LIKE '% ���XL12' OR UPPER(TRIM(PRODUCT_NAME)) LIKE '���XL12%' OR UPPER(TRIM(PRODUCT_NAME)) LIKE '% ���XL12 %') AND UPPER(TRIM(CATEGORY_FINAL)) = 'NECCESSITIES' THEN 'BOY'
								WHEN (UPPER(TRIM(PRODUCT_NAME)) LIKE '% ���L52' OR UPPER(TRIM(PRODUCT_NAME)) LIKE '���L52%' OR UPPER(TRIM(PRODUCT_NAME)) LIKE '% ���L52 %') AND UPPER(TRIM(CATEGORY_FINAL)) = 'NECCESSITIES' THEN 'BOY'
								WHEN (UPPER(TRIM(PRODUCT_NAME)) LIKE '% ���M17' OR UPPER(TRIM(PRODUCT_NAME)) LIKE '���M17%' OR UPPER(TRIM(PRODUCT_NAME)) LIKE '% ���M17 %') AND UPPER(TRIM(CATEGORY_FINAL)) = 'NECCESSITIES' THEN 'BOY'
								WHEN (UPPER(TRIM(PRODUCT_NAME)) LIKE '% GIRLZ' OR UPPER(TRIM(PRODUCT_NAME)) LIKE 'GIRLZ%' OR UPPER(TRIM(PRODUCT_NAME)) LIKE '% GIRLZ %') AND UPPER(TRIM(CATEGORY_FINAL)) = 'TOYS' THEN 'GIRL'
								WHEN (UPPER(TRIM(PRODUCT_NAME)) LIKE '% STRIKE' OR UPPER(TRIM(PRODUCT_NAME)) LIKE 'STRIKE%' OR UPPER(TRIM(PRODUCT_NAME)) LIKE '% STRIKE %') AND UPPER(TRIM(CATEGORY_FINAL)) = 'TOYS' THEN 'BOY'
								WHEN (UPPER(TRIM(PRODUCT_NAME)) LIKE '% GUNDAM' OR UPPER(TRIM(PRODUCT_NAME)) LIKE 'GUNDAM%' OR UPPER(TRIM(PRODUCT_NAME)) LIKE '% GUNDAM %') AND UPPER(TRIM(CATEGORY_FINAL)) = 'TOYS' THEN 'BOY'
								WHEN (UPPER(TRIM(PRODUCT_NAME)) LIKE '% �׹NER' OR UPPER(TRIM(PRODUCT_NAME)) LIKE '�׹NER%' OR UPPER(TRIM(PRODUCT_NAME)) LIKE '% �׹NER %') AND UPPER(TRIM(CATEGORY_FINAL)) = 'TOYS' THEN 'BOY'
								WHEN (UPPER(TRIM(PRODUCT_NAME)) LIKE '% ���TRA' OR UPPER(TRIM(PRODUCT_NAME)) LIKE '���TRA%' OR UPPER(TRIM(PRODUCT_NAME)) LIKE '% ���TRA %') AND UPPER(TRIM(CATEGORY_FINAL)) = 'TOYS' THEN 'BOY'
								WHEN (UPPER(TRIM(PRODUCT_NAME)) LIKE '% SHOPKINS' OR UPPER(TRIM(PRODUCT_NAME)) LIKE 'SHOPKINS%' OR UPPER(TRIM(PRODUCT_NAME)) LIKE '% SHOPKINS %') AND UPPER(TRIM(CATEGORY_FINAL)) = 'TOYS' THEN 'GIRL'
								WHEN (UPPER(TRIM(PRODUCT_NAME)) LIKE '% CA' OR UPPER(TRIM(PRODUCT_NAME)) LIKE 'CA%' OR UPPER(TRIM(PRODUCT_NAME)) LIKE '% CA %') AND UPPER(TRIM(CATEGORY_FINAL)) = 'TOYS' THEN 'BOY'
								WHEN (UPPER(TRIM(PRODUCT_NAME)) LIKE '% HERO' OR UPPER(TRIM(PRODUCT_NAME)) LIKE 'HERO%' OR UPPER(TRIM(PRODUCT_NAME)) LIKE '% HERO %') AND UPPER(TRIM(CATEGORY_FINAL)) = 'TOYS' THEN 'BOY'
								WHEN (UPPER(TRIM(PRODUCT_NAME)) LIKE '% ����ͧ�Թ' OR UPPER(TRIM(PRODUCT_NAME)) LIKE '����ͧ�Թ%' OR UPPER(TRIM(PRODUCT_NAME)) LIKE '% ����ͧ�Թ %') AND UPPER(TRIM(CATEGORY_FINAL)) = 'TOYS' THEN 'BOY'
								WHEN (UPPER(TRIM(PRODUCT_NAME)) LIKE '% ��꡵�BRB' OR UPPER(TRIM(PRODUCT_NAME)) LIKE '��꡵�BRB%' OR UPPER(TRIM(PRODUCT_NAME)) LIKE '% ��꡵�BRB %') AND UPPER(TRIM(CATEGORY_FINAL)) = 'TOYS' THEN 'GIRL'
								WHEN (UPPER(TRIM(PRODUCT_NAME)) LIKE '% TITAN' OR UPPER(TRIM(PRODUCT_NAME)) LIKE 'TITAN%' OR UPPER(TRIM(PRODUCT_NAME)) LIKE '% TITAN %') AND UPPER(TRIM(CATEGORY_FINAL)) = 'TOYS' THEN 'BOY'
								WHEN (UPPER(TRIM(PRODUCT_NAME)) LIKE '% ZOMBIE' OR UPPER(TRIM(PRODUCT_NAME)) LIKE 'ZOMBIE%' OR UPPER(TRIM(PRODUCT_NAME)) LIKE '% ZOMBIE %') AND UPPER(TRIM(CATEGORY_FINAL)) = 'TOYS' THEN 'BOY'
								WHEN (UPPER(TRIM(PRODUCT_NAME)) LIKE '% EXPLORE' OR UPPER(TRIM(PRODUCT_NAME)) LIKE 'EXPLORE%' OR UPPER(TRIM(PRODUCT_NAME)) LIKE '% EXPLORE %') AND UPPER(TRIM(CATEGORY_FINAL)) = 'TOYS' THEN 'GIRL'
								WHEN (UPPER(TRIM(PRODUCT_NAME)) LIKE '% PONY' OR UPPER(TRIM(PRODUCT_NAME)) LIKE 'PONY%' OR UPPER(TRIM(PRODUCT_NAME)) LIKE '% PONY %') AND UPPER(TRIM(CATEGORY_FINAL)) = 'TOYS' THEN 'GIRL'
								WHEN (UPPER(TRIM(PRODUCT_NAME)) LIKE '% �׹' OR UPPER(TRIM(PRODUCT_NAME)) LIKE '�׹%' OR UPPER(TRIM(PRODUCT_NAME)) LIKE '% �׹ %') AND UPPER(TRIM(CATEGORY_FINAL)) = 'TOYS' THEN 'BOY'
								WHEN (UPPER(TRIM(PRODUCT_NAME)) LIKE '% ��꡵�MLP' OR UPPER(TRIM(PRODUCT_NAME)) LIKE '��꡵�MLP%' OR UPPER(TRIM(PRODUCT_NAME)) LIKE '% ��꡵�MLP %') AND UPPER(TRIM(CATEGORY_FINAL)) = 'TOYS' THEN 'GIRL'
								WHEN (UPPER(TRIM(PRODUCT_NAME)) LIKE '% PET' OR UPPER(TRIM(PRODUCT_NAME)) LIKE 'PET%' OR UPPER(TRIM(PRODUCT_NAME)) LIKE '% PET %') AND UPPER(TRIM(CATEGORY_FINAL)) = 'TOYS' THEN 'GIRL'
								WHEN (UPPER(TRIM(PRODUCT_NAME)) LIKE '% ���' OR UPPER(TRIM(PRODUCT_NAME)) LIKE '���%' OR UPPER(TRIM(PRODUCT_NAME)) LIKE '% ��� %') AND UPPER(TRIM(CATEGORY_FINAL)) = 'TOYS' THEN 'BOY'
								WHEN (UPPER(TRIM(PRODUCT_NAME)) LIKE '% ����' OR UPPER(TRIM(PRODUCT_NAME)) LIKE '����%' OR UPPER(TRIM(PRODUCT_NAME)) LIKE '% ���� %') AND UPPER(TRIM(CATEGORY_FINAL)) = 'TOYS' THEN 'BOY'
								WHEN (UPPER(TRIM(PRODUCT_NAME)) LIKE '% SECRET' OR UPPER(TRIM(PRODUCT_NAME)) LIKE 'SECRET%' OR UPPER(TRIM(PRODUCT_NAME)) LIKE '% SECRET %') AND UPPER(TRIM(CATEGORY_FINAL)) = 'TOYS' THEN 'GIRL'
								WHEN (UPPER(TRIM(PRODUCT_NAME)) LIKE '% ELSA' OR UPPER(TRIM(PRODUCT_NAME)) LIKE 'ELSA%' OR UPPER(TRIM(PRODUCT_NAME)) LIKE '% ELSA %') AND UPPER(TRIM(CATEGORY_FINAL)) = 'TOYS' THEN 'GIRL'
								WHEN (UPPER(TRIM(PRODUCT_NAME)) LIKE '% EQUESTRIA' OR UPPER(TRIM(PRODUCT_NAME)) LIKE 'EQUESTRIA%' OR UPPER(TRIM(PRODUCT_NAME)) LIKE '% EQUESTRIA %') AND UPPER(TRIM(CATEGORY_FINAL)) = 'TOYS' THEN 'GIRL'
								WHEN (UPPER(TRIM(PRODUCT_NAME)) LIKE '% ���SW' OR UPPER(TRIM(PRODUCT_NAME)) LIKE '���SW%' OR UPPER(TRIM(PRODUCT_NAME)) LIKE '% ���SW %') AND UPPER(TRIM(CATEGORY_FINAL)) = 'TOYS' THEN 'BOY'
								WHEN (UPPER(TRIM(PRODUCT_NAME)) LIKE '% MINI' OR UPPER(TRIM(PRODUCT_NAME)) LIKE 'MINI%' OR UPPER(TRIM(PRODUCT_NAME)) LIKE '% MINI %') AND UPPER(TRIM(CATEGORY_FINAL)) = 'TOYS' THEN 'BOY'
								WHEN (UPPER(TRIM(PRODUCT_NAME)) LIKE '% CHANGE' OR UPPER(TRIM(PRODUCT_NAME)) LIKE 'CHANGE%' OR UPPER(TRIM(PRODUCT_NAME)) LIKE '% CHANGE %') AND UPPER(TRIM(CATEGORY_FINAL)) = 'TOYS' THEN 'BOY'
								WHEN (UPPER(TRIM(PRODUCT_NAME)) LIKE '% FASHION' OR UPPER(TRIM(PRODUCT_NAME)) LIKE 'FASHION%' OR UPPER(TRIM(PRODUCT_NAME)) LIKE '% FASHION %') AND UPPER(TRIM(CATEGORY_FINAL)) = 'TOYS' THEN 'GIRL'
								WHEN (UPPER(TRIM(PRODUCT_NAME)) LIKE '% BLASTER' OR UPPER(TRIM(PRODUCT_NAME)) LIKE 'BLASTER%' OR UPPER(TRIM(PRODUCT_NAME)) LIKE '% BLASTER %') AND UPPER(TRIM(CATEGORY_FINAL)) = 'TOYS' THEN 'BOY'
								WHEN (UPPER(TRIM(PRODUCT_NAME)) LIKE '% MAN' OR UPPER(TRIM(PRODUCT_NAME)) LIKE 'MAN%' OR UPPER(TRIM(PRODUCT_NAME)) LIKE '% MAN %') AND UPPER(TRIM(CATEGORY_FINAL)) = 'TOYS' THEN 'BOY'
								WHEN (UPPER(TRIM(PRODUCT_NAME)) LIKE '% POPPIT' OR UPPER(TRIM(PRODUCT_NAME)) LIKE 'POPPIT%' OR UPPER(TRIM(PRODUCT_NAME)) LIKE '% POPPIT %') AND UPPER(TRIM(CATEGORY_FINAL)) = 'TOYS' THEN 'GIRL'
								WHEN (UPPER(TRIM(PRODUCT_NAME)) LIKE '% öHW' OR UPPER(TRIM(PRODUCT_NAME)) LIKE 'öHW%' OR UPPER(TRIM(PRODUCT_NAME)) LIKE '% öHW %') AND UPPER(TRIM(CATEGORY_FINAL)) = 'TOYS' THEN 'BOY'
								WHEN (UPPER(TRIM(PRODUCT_NAME)) LIKE '% FROZEN' OR UPPER(TRIM(PRODUCT_NAME)) LIKE 'FROZEN%' OR UPPER(TRIM(PRODUCT_NAME)) LIKE '% FROZEN %') AND UPPER(TRIM(CATEGORY_FINAL)) = 'TOYS' THEN 'GIRL'
								WHEN (UPPER(TRIM(PRODUCT_NAME)) LIKE '% MAJORETTE' OR UPPER(TRIM(PRODUCT_NAME)) LIKE 'MAJORETTE%' OR UPPER(TRIM(PRODUCT_NAME)) LIKE '% MAJORETTE %') AND UPPER(TRIM(CATEGORY_FINAL)) = 'TOYS' THEN 'BOY'
								WHEN (UPPER(TRIM(PRODUCT_NAME)) LIKE '% ��꡵�SPARKLE' OR UPPER(TRIM(PRODUCT_NAME)) LIKE '��꡵�SPARKLE%' OR UPPER(TRIM(PRODUCT_NAME)) LIKE '% ��꡵�SPARKLE %') AND UPPER(TRIM(CATEGORY_FINAL)) = 'TOYS' THEN 'GIRL'
								WHEN (UPPER(TRIM(PRODUCT_NAME)) LIKE '% PLACES' OR UPPER(TRIM(PRODUCT_NAME)) LIKE 'PLACES%' OR UPPER(TRIM(PRODUCT_NAME)) LIKE '% PLACES %') AND UPPER(TRIM(CATEGORY_FINAL)) = 'TOYS' THEN 'GIRL'
								WHEN (UPPER(TRIM(PRODUCT_NAME)) LIKE '% MEDAL' OR UPPER(TRIM(PRODUCT_NAME)) LIKE 'MEDAL%' OR UPPER(TRIM(PRODUCT_NAME)) LIKE '% MEDAL %') AND UPPER(TRIM(CATEGORY_FINAL)) = 'TOYS' THEN 'BOY'
								WHEN (UPPER(TRIM(PRODUCT_NAME)) LIKE '% TOWN' OR UPPER(TRIM(PRODUCT_NAME)) LIKE 'TOWN%' OR UPPER(TRIM(PRODUCT_NAME)) LIKE '% TOWN %') AND UPPER(TRIM(CATEGORY_FINAL)) = 'TOYS' THEN 'GIRL'
								WHEN (UPPER(TRIM(PRODUCT_NAME)) LIKE '% GENERATIONS' OR UPPER(TRIM(PRODUCT_NAME)) LIKE 'GENERATIONS%' OR UPPER(TRIM(PRODUCT_NAME)) LIKE '% GENERATIONS %') AND UPPER(TRIM(CATEGORY_FINAL)) = 'TOYS' THEN 'BOY'
								WHEN (UPPER(TRIM(PRODUCT_NAME)) LIKE '% CLASS' OR UPPER(TRIM(PRODUCT_NAME)) LIKE 'CLASS%' OR UPPER(TRIM(PRODUCT_NAME)) LIKE '% CLASS %') AND UPPER(TRIM(CATEGORY_FINAL)) = 'TOYS' THEN 'BOY'
								WHEN (UPPER(TRIM(PRODUCT_NAME)) LIKE '% TRANSFORMING' OR UPPER(TRIM(PRODUCT_NAME)) LIKE 'TRANSFORMING%' OR UPPER(TRIM(PRODUCT_NAME)) LIKE '% TRANSFORMING %') AND UPPER(TRIM(CATEGORY_FINAL)) = 'TOYS' THEN 'BOY'
								WHEN (UPPER(TRIM(PRODUCT_NAME)) LIKE '% ROBOT' OR UPPER(TRIM(PRODUCT_NAME)) LIKE 'ROBOT%' OR UPPER(TRIM(PRODUCT_NAME)) LIKE '% ROBOT %') AND UPPER(TRIM(CATEGORY_FINAL)) = 'TOYS' THEN 'BOY'
								WHEN (UPPER(TRIM(PRODUCT_NAME)) LIKE '% ö' OR UPPER(TRIM(PRODUCT_NAME)) LIKE 'ö%' OR UPPER(TRIM(PRODUCT_NAME)) LIKE '% ö %') AND UPPER(TRIM(CATEGORY_FINAL)) = 'TOYS' THEN 'BOY'
								WHEN (UPPER(TRIM(PRODUCT_NAME)) LIKE '% �׹����NER' OR UPPER(TRIM(PRODUCT_NAME)) LIKE '�׹����NER%' OR UPPER(TRIM(PRODUCT_NAME)) LIKE '% �׹����NER %') AND UPPER(TRIM(CATEGORY_FINAL)) = 'TOYS' THEN 'BOY'
								WHEN (UPPER(TRIM(PRODUCT_NAME)) LIKE '% DRESS' OR UPPER(TRIM(PRODUCT_NAME)) LIKE 'DRESS%' OR UPPER(TRIM(PRODUCT_NAME)) LIKE '% DRESS %') AND UPPER(TRIM(CATEGORY_FINAL)) = 'TOYS' THEN 'GIRL'
								WHEN (UPPER(TRIM(PRODUCT_NAME)) LIKE '% ARIEL' OR UPPER(TRIM(PRODUCT_NAME)) LIKE 'ARIEL%' OR UPPER(TRIM(PRODUCT_NAME)) LIKE '% ARIEL %') AND UPPER(TRIM(CATEGORY_FINAL)) = 'TOYS' THEN 'GIRL'
								WHEN (UPPER(TRIM(PRODUCT_NAME)) LIKE '% LEGENDS' OR UPPER(TRIM(PRODUCT_NAME)) LIKE 'LEGENDS%' OR UPPER(TRIM(PRODUCT_NAME)) LIKE '% LEGENDS %') AND UPPER(TRIM(CATEGORY_FINAL)) = 'TOYS' THEN 'BOY'
								WHEN (UPPER(TRIM(PRODUCT_NAME)) LIKE '% DOLL' OR UPPER(TRIM(PRODUCT_NAME)) LIKE 'DOLL%' OR UPPER(TRIM(PRODUCT_NAME)) LIKE '% DOLL %') AND UPPER(TRIM(CATEGORY_FINAL)) = 'TOYS' THEN 'GIRL'
								WHEN (UPPER(TRIM(PRODUCT_NAME)) LIKE '% PRINCESS' OR UPPER(TRIM(PRODUCT_NAME)) LIKE 'PRINCESS%' OR UPPER(TRIM(PRODUCT_NAME)) LIKE '% PRINCESS %') AND UPPER(TRIM(CATEGORY_FINAL)) = 'TOYS' THEN 'GIRL'
								WHEN (UPPER(TRIM(PRODUCT_NAME)) LIKE '% PEPPA' OR UPPER(TRIM(PRODUCT_NAME)) LIKE 'PEPPA%' OR UPPER(TRIM(PRODUCT_NAME)) LIKE '% PEPPA %') AND UPPER(TRIM(CATEGORY_FINAL)) = 'TOYS' THEN 'GIRL'
								WHEN (UPPER(TRIM(PRODUCT_NAME)) LIKE '% SMALL' OR UPPER(TRIM(PRODUCT_NAME)) LIKE 'SMALL%' OR UPPER(TRIM(PRODUCT_NAME)) LIKE '% SMALL %') AND UPPER(TRIM(CATEGORY_FINAL)) = 'TOYS' THEN 'GIRL'
								WHEN (UPPER(TRIM(PRODUCT_NAME)) LIKE '% BEAR' OR UPPER(TRIM(PRODUCT_NAME)) LIKE 'BEAR%' OR UPPER(TRIM(PRODUCT_NAME)) LIKE '% BEAR %') AND UPPER(TRIM(CATEGORY_FINAL)) = 'TOYS' THEN 'GIRL'
								WHEN (UPPER(TRIM(PRODUCT_NAME)) LIKE '% FROZEN' OR UPPER(TRIM(PRODUCT_NAME)) LIKE 'FROZEN%' OR UPPER(TRIM(PRODUCT_NAME)) LIKE '% FROZEN %') AND UPPER(TRIM(CATEGORY_FINAL)) = 'TOYS' THEN 'GIRL'
								WHEN (UPPER(TRIM(PRODUCT_NAME)) LIKE '% HAPPY' OR UPPER(TRIM(PRODUCT_NAME)) LIKE 'HAPPY%' OR UPPER(TRIM(PRODUCT_NAME)) LIKE '% HAPPY %') AND UPPER(TRIM(CATEGORY_FINAL)) = 'TOYS' THEN 'GIRL'
								WHEN (UPPER(TRIM(PRODUCT_NAME)) LIKE '% �Ե���' OR UPPER(TRIM(PRODUCT_NAME)) LIKE '�Ե���%' OR UPPER(TRIM(PRODUCT_NAME)) LIKE '% �Ե��� %') AND UPPER(TRIM(CATEGORY_FINAL)) = 'TOYS' THEN 'GIRL'
								WHEN (UPPER(TRIM(PRODUCT_NAME)) LIKE '% SPARKLE' OR UPPER(TRIM(PRODUCT_NAME)) LIKE 'SPARKLE%' OR UPPER(TRIM(PRODUCT_NAME)) LIKE '% SPARKLE %') AND UPPER(TRIM(CATEGORY_FINAL)) = 'TOYS' THEN 'GIRL'
								WHEN (UPPER(TRIM(PRODUCT_NAME)) LIKE '% FIRE' OR UPPER(TRIM(PRODUCT_NAME)) LIKE 'FIRE%' OR UPPER(TRIM(PRODUCT_NAME)) LIKE '% FIRE %') AND UPPER(TRIM(CATEGORY_FINAL)) = 'TOYS' THEN 'BOY'
								WHEN (UPPER(TRIM(PRODUCT_NAME)) LIKE '% PIG' OR UPPER(TRIM(PRODUCT_NAME)) LIKE 'PIG%' OR UPPER(TRIM(PRODUCT_NAME)) LIKE '% PIG %') AND UPPER(TRIM(CATEGORY_FINAL)) = 'TOYS' THEN 'GIRL'
								WHEN (UPPER(TRIM(PRODUCT_NAME)) LIKE '% ULTIMATE' OR UPPER(TRIM(PRODUCT_NAME)) LIKE 'ULTIMATE%' OR UPPER(TRIM(PRODUCT_NAME)) LIKE '% ULTIMATE %') AND UPPER(TRIM(CATEGORY_FINAL)) = 'TOYS' THEN 'BOY'
								WHEN (UPPER(TRIM(PRODUCT_NAME)) LIKE '% CHANGE' OR UPPER(TRIM(PRODUCT_NAME)) LIKE 'CHANGE%' OR UPPER(TRIM(PRODUCT_NAME)) LIKE '% CHANGE %') AND UPPER(TRIM(CATEGORY_FINAL)) = 'TOYS' THEN 'BOY'
								WHEN (UPPER(TRIM(PRODUCT_NAME)) LIKE '% FAMILY' OR UPPER(TRIM(PRODUCT_NAME)) LIKE 'FAMILY%' OR UPPER(TRIM(PRODUCT_NAME)) LIKE '% FAMILY %') AND UPPER(TRIM(CATEGORY_FINAL)) = 'TOYS' THEN 'GIRL'
								WHEN (UPPER(TRIM(PRODUCT_NAME)) LIKE '% TRUCK' OR UPPER(TRIM(PRODUCT_NAME)) LIKE 'TRUCK%' OR UPPER(TRIM(PRODUCT_NAME)) LIKE '% TRUCK %') AND UPPER(TRIM(CATEGORY_FINAL)) = 'TOYS' THEN 'BOY'
								WHEN (UPPER(TRIM(PRODUCT_NAME)) LIKE '% CONE' OR UPPER(TRIM(PRODUCT_NAME)) LIKE 'CONE%' OR UPPER(TRIM(PRODUCT_NAME)) LIKE '% CONE %') AND UPPER(TRIM(CATEGORY_FINAL)) = 'TOYS' THEN 'GIRL'
								WHEN (UPPER(TRIM(PRODUCT_NAME)) LIKE '% RABBIT' OR UPPER(TRIM(PRODUCT_NAME)) LIKE 'RABBIT%' OR UPPER(TRIM(PRODUCT_NAME)) LIKE '% RABBIT %') AND UPPER(TRIM(CATEGORY_FINAL)) = 'TOYS' THEN 'GIRL'
								WHEN (UPPER(TRIM(PRODUCT_NAME)) LIKE '% TRACK' OR UPPER(TRIM(PRODUCT_NAME)) LIKE 'TRACK%' OR UPPER(TRIM(PRODUCT_NAME)) LIKE '% TRACK %') AND UPPER(TRIM(CATEGORY_FINAL)) = 'TOYS' THEN 'BOY'
								WHEN (UPPER(TRIM(PRODUCT_NAME)) LIKE '% GIFT' OR UPPER(TRIM(PRODUCT_NAME)) LIKE 'GIFT%' OR UPPER(TRIM(PRODUCT_NAME)) LIKE '% GIFT %') AND UPPER(TRIM(CATEGORY_FINAL)) = 'TOYS' THEN 'GIRL'
								WHEN (UPPER(TRIM(PRODUCT_NAME)) LIKE '% IRON' OR UPPER(TRIM(PRODUCT_NAME)) LIKE 'IRON%' OR UPPER(TRIM(PRODUCT_NAME)) LIKE '% IRON %') AND UPPER(TRIM(CATEGORY_FINAL)) = 'TOYS' THEN 'BOY'
								WHEN (UPPER(TRIM(PRODUCT_NAME)) LIKE '% INCH' OR UPPER(TRIM(PRODUCT_NAME)) LIKE 'INCH%' OR UPPER(TRIM(PRODUCT_NAME)) LIKE '% INCH %') AND UPPER(TRIM(CATEGORY_FINAL)) = 'TOYS' THEN 'BOY'
								WHEN (UPPER(TRIM(PRODUCT_NAME)) LIKE '% CAR' OR UPPER(TRIM(PRODUCT_NAME)) LIKE 'CAR%' OR UPPER(TRIM(PRODUCT_NAME)) LIKE '% CAR %') AND UPPER(TRIM(CATEGORY_FINAL)) = 'TOYS' THEN 'BOY'
								WHEN (UPPER(TRIM(PRODUCT_NAME)) LIKE '% ��ǵ��' OR UPPER(TRIM(PRODUCT_NAME)) LIKE '��ǵ��%' OR UPPER(TRIM(PRODUCT_NAME)) LIKE '% ��ǵ�� %') AND UPPER(TRIM(CATEGORY_FINAL)) = 'TOYS' THEN 'BOY'
								WHEN (UPPER(TRIM(PRODUCT_NAME)) LIKE '% STAR' OR UPPER(TRIM(PRODUCT_NAME)) LIKE 'STAR%' OR UPPER(TRIM(PRODUCT_NAME)) LIKE '% STAR %') AND UPPER(TRIM(CATEGORY_FINAL)) = 'TOYS' THEN 'BOY'
								WHEN (UPPER(TRIM(PRODUCT_NAME)) LIKE '% ELECTRONIC' OR UPPER(TRIM(PRODUCT_NAME)) LIKE 'ELECTRONIC%' OR UPPER(TRIM(PRODUCT_NAME)) LIKE '% ELECTRONIC %') AND UPPER(TRIM(CATEGORY_FINAL)) = 'TOYS' THEN 'BOY'
								WHEN (UPPER(TRIM(PRODUCT_NAME)) LIKE '% RAINBOW' OR UPPER(TRIM(PRODUCT_NAME)) LIKE 'RAINBOW%' OR UPPER(TRIM(PRODUCT_NAME)) LIKE '% RAINBOW %') AND UPPER(TRIM(CATEGORY_FINAL)) = 'TOYS' THEN 'GIRL'
								WHEN (UPPER(TRIM(PRODUCT_NAME)) LIKE '% FRIENDS' OR UPPER(TRIM(PRODUCT_NAME)) LIKE 'FRIENDS%' OR UPPER(TRIM(PRODUCT_NAME)) LIKE '% FRIENDS %') AND UPPER(TRIM(CATEGORY_FINAL)) = 'TOYS' THEN 'GIRL'
								WHEN (UPPER(TRIM(PRODUCT_NAME)) LIKE '% VEHICLE' OR UPPER(TRIM(PRODUCT_NAME)) LIKE 'VEHICLE%' OR UPPER(TRIM(PRODUCT_NAME)) LIKE '% VEHICLE %') AND UPPER(TRIM(CATEGORY_FINAL)) = 'TOYS' THEN 'BOY'
								WHEN (UPPER(TRIM(PRODUCT_NAME)) LIKE '% LITTLE' OR UPPER(TRIM(PRODUCT_NAME)) LIKE 'LITTLE%' OR UPPER(TRIM(PRODUCT_NAME)) LIKE '% LITTLE %') AND UPPER(TRIM(CATEGORY_FINAL)) = 'TOYS' THEN 'GIRL'
								WHEN (UPPER(TRIM(PRODUCT_NAME)) LIKE '% PLAYSET' OR UPPER(TRIM(PRODUCT_NAME)) LIKE 'PLAYSET%' OR UPPER(TRIM(PRODUCT_NAME)) LIKE '% PLAYSET %') AND UPPER(TRIM(CATEGORY_FINAL)) = 'TOYS' THEN 'GIRL'
								WHEN (UPPER(TRIM(PRODUCT_NAME)) LIKE '% CARS' OR UPPER(TRIM(PRODUCT_NAME)) LIKE 'CARS%' OR UPPER(TRIM(PRODUCT_NAME)) LIKE '% CARS %') AND UPPER(TRIM(CATEGORY_FINAL)) = 'TOYS' THEN 'BOY'
								WHEN (UPPER(TRIM(PRODUCT_NAME)) LIKE '% DISNEY' OR UPPER(TRIM(PRODUCT_NAME)) LIKE 'DISNEY%' OR UPPER(TRIM(PRODUCT_NAME)) LIKE '% DISNEY %') AND UPPER(TRIM(CATEGORY_FINAL)) = 'TOYS' THEN 'GIRL'
								WHEN (UPPER(TRIM(PRODUCT_NAME)) LIKE '% SERIES' OR UPPER(TRIM(PRODUCT_NAME)) LIKE 'SERIES%' OR UPPER(TRIM(PRODUCT_NAME)) LIKE '% SERIES %') AND UPPER(TRIM(CATEGORY_FINAL)) = 'TOYS' THEN 'BOY'
								WHEN (UPPER(TRIM(PRODUCT_NAME)) LIKE '% PACK' OR UPPER(TRIM(PRODUCT_NAME)) LIKE 'PACK%' OR UPPER(TRIM(PRODUCT_NAME)) LIKE '% PACK %') AND UPPER(TRIM(CATEGORY_FINAL)) = 'TOYS' THEN 'GIRL'
								WHEN (UPPER(TRIM(PRODUCT_NAME)) LIKE '% V29' OR UPPER(TRIM(PRODUCT_NAME)) LIKE 'V29%' OR UPPER(TRIM(PRODUCT_NAME)) LIKE '% V29 %') AND UPPER(TRIM(CATEGORY_FINAL)) = 'TOYS' THEN 'BOY'
								WHEN (UPPER(TRIM(PRODUCT_NAME)) LIKE '% FIGURE' OR UPPER(TRIM(PRODUCT_NAME)) LIKE 'FIGURE%' OR UPPER(TRIM(PRODUCT_NAME)) LIKE '% FIGURE %') AND UPPER(TRIM(CATEGORY_FINAL)) = 'TOYS' THEN 'BOY'
								WHEN (UPPER(TRIM(PRODUCT_NAME)) LIKE '% LIGHTS' OR UPPER(TRIM(PRODUCT_NAME)) LIKE 'LIGHTS%' OR UPPER(TRIM(PRODUCT_NAME)) LIKE '% LIGHTS %') AND UPPER(TRIM(CATEGORY_FINAL)) = 'TOYS' THEN 'GIRL'
								WHEN (UPPER(TRIM(PRODUCT_NAME)) LIKE '% HOUSE' OR UPPER(TRIM(PRODUCT_NAME)) LIKE 'HOUSE%' OR UPPER(TRIM(PRODUCT_NAME)) LIKE '% HOUSE %') AND UPPER(TRIM(CATEGORY_FINAL)) = 'TOYS' THEN 'GIRL'
								WHEN (UPPER(TRIM(PRODUCT_NAME)) LIKE '% �Ԥ����' OR UPPER(TRIM(PRODUCT_NAME)) LIKE '�Ԥ����%' OR UPPER(TRIM(PRODUCT_NAME)) LIKE '% �Ԥ���� %') AND UPPER(TRIM(CATEGORY_FINAL)) = 'TOYS' THEN 'BOY'
								WHEN (UPPER(TRIM(PRODUCT_NAME)) LIKE '% ˭ԧ' OR UPPER(TRIM(PRODUCT_NAME)) LIKE '˭ԧ%' OR UPPER(TRIM(PRODUCT_NAME)) LIKE '% ˭ԧ %') AND UPPER(TRIM(CATEGORY_FINAL)) = 'FASHION' THEN 'GIRL'
								WHEN (UPPER(TRIM(PRODUCT_NAME)) LIKE '% ���' OR UPPER(TRIM(PRODUCT_NAME)) LIKE '���%' OR UPPER(TRIM(PRODUCT_NAME)) LIKE '% ��� %') AND UPPER(TRIM(CATEGORY_FINAL)) = 'FASHION' THEN 'BOY'
								WHEN (UPPER(TRIM(PRODUCT_NAME)) LIKE '% �ѡ�����ŵ���PANTS˭ԧ' OR UPPER(TRIM(PRODUCT_NAME)) LIKE '�ѡ�����ŵ���PANTS˭ԧ%' OR UPPER(TRIM(PRODUCT_NAME)) LIKE '% �ѡ�����ŵ���PANTS˭ԧ %') AND UPPER(TRIM(CATEGORY_FINAL)) = 'FASHION' THEN 'GIRL'
								WHEN (UPPER(TRIM(PRODUCT_NAME)) LIKE '% �����' OR UPPER(TRIM(PRODUCT_NAME)) LIKE '�����%' OR UPPER(TRIM(PRODUCT_NAME)) LIKE '% ����� %') AND UPPER(TRIM(CATEGORY_FINAL)) = 'FASHION' THEN 'GIRL'
								WHEN (UPPER(TRIM(PRODUCT_NAME)) LIKE '% ����Ź�觡Ѻ�ԧ' OR UPPER(TRIM(PRODUCT_NAME)) LIKE '����Ź�觡Ѻ�ԧ%' OR UPPER(TRIM(PRODUCT_NAME)) LIKE '% ����Ź�觡Ѻ�ԧ %') AND UPPER(TRIM(CATEGORY_FINAL)) = 'FASHION' THEN 'GIRL'
								WHEN (UPPER(TRIM(PRODUCT_NAME)) LIKE '% �Ե���' OR UPPER(TRIM(PRODUCT_NAME)) LIKE '�Ե���%' OR UPPER(TRIM(PRODUCT_NAME)) LIKE '% �Ե��� %') AND UPPER(TRIM(CATEGORY_FINAL)) = 'FASHION' THEN 'GIRL'
								WHEN (UPPER(TRIM(PRODUCT_NAME)) LIKE '% MAXI' OR UPPER(TRIM(PRODUCT_NAME)) LIKE 'MAXI%' OR UPPER(TRIM(PRODUCT_NAME)) LIKE '% MAXI %') AND UPPER(TRIM(CATEGORY_FINAL)) = 'FASHION' THEN 'GIRL'
								WHEN (UPPER(TRIM(PRODUCT_NAME)) LIKE '% ����Ź͹�Ѻ�ԧ' OR UPPER(TRIM(PRODUCT_NAME)) LIKE '����Ź͹�Ѻ�ԧ%' OR UPPER(TRIM(PRODUCT_NAME)) LIKE '% ����Ź͹�Ѻ�ԧ %') AND UPPER(TRIM(CATEGORY_FINAL)) = 'FASHION' THEN 'GIRL'
								WHEN (UPPER(TRIM(PRODUCT_NAME)) LIKE '% ˭ԧXXL10' OR UPPER(TRIM(PRODUCT_NAME)) LIKE '˭ԧXXL10%' OR UPPER(TRIM(PRODUCT_NAME)) LIKE '% ˭ԧXXL10 %') AND UPPER(TRIM(CATEGORY_FINAL)) = 'FASHION' THEN 'GIRL'
								WHEN (UPPER(TRIM(PRODUCT_NAME)) LIKE '% �ѡ�����ŵ���PANTS˭ԧSPJ' OR UPPER(TRIM(PRODUCT_NAME)) LIKE '�ѡ�����ŵ���PANTS˭ԧSPJ%' OR UPPER(TRIM(PRODUCT_NAME)) LIKE '% �ѡ�����ŵ���PANTS˭ԧSPJ %') AND UPPER(TRIM(CATEGORY_FINAL)) = 'FASHION' THEN 'GIRL'
								WHEN (UPPER(TRIM(PRODUCT_NAME)) LIKE '% ˭ԧXL12' OR UPPER(TRIM(PRODUCT_NAME)) LIKE '˭ԧXL12%' OR UPPER(TRIM(PRODUCT_NAME)) LIKE '% ˭ԧXL12 %') AND UPPER(TRIM(CATEGORY_FINAL)) = 'FASHION' THEN 'GIRL'
								WHEN (UPPER(TRIM(PRODUCT_NAME)) LIKE '% ���������д��PANT�.' OR UPPER(TRIM(PRODUCT_NAME)) LIKE '���������д��PANT�.%' OR UPPER(TRIM(PRODUCT_NAME)) LIKE '% ���������д��PANT�. %') AND UPPER(TRIM(CATEGORY_FINAL)) = 'FASHION' THEN 'BOY'
								WHEN (UPPER(TRIM(PRODUCT_NAME)) LIKE '% ���������д��PANT�.' OR UPPER(TRIM(PRODUCT_NAME)) LIKE '���������д��PANT�.%' OR UPPER(TRIM(PRODUCT_NAME)) LIKE '% ���������д��PANT�. %') AND UPPER(TRIM(CATEGORY_FINAL)) = 'FASHION' THEN 'GIRL'
								WHEN (UPPER(TRIM(PRODUCT_NAME)) LIKE '% �ǴRPP8OZ.�Ե���+�ء�Թ�M' OR UPPER(TRIM(PRODUCT_NAME)) LIKE '�ǴRPP8OZ.�Ե���+�ء�Թ�M%' OR UPPER(TRIM(PRODUCT_NAME)) LIKE '% �ǴRPP8OZ.�Ե���+�ء�Թ�M %') AND UPPER(TRIM(CATEGORY_FINAL)) = 'FASHION' THEN 'GIRL'
								WHEN (UPPER(TRIM(PRODUCT_NAME)) LIKE '% ���XXL38' OR UPPER(TRIM(PRODUCT_NAME)) LIKE '���XXL38%' OR UPPER(TRIM(PRODUCT_NAME)) LIKE '% ���XXL38 %') AND UPPER(TRIM(CATEGORY_FINAL)) = 'FASHION' THEN 'BOY'
								WHEN (UPPER(TRIM(PRODUCT_NAME)) LIKE '% ˭ԧL14' OR UPPER(TRIM(PRODUCT_NAME)) LIKE '˭ԧL14%' OR UPPER(TRIM(PRODUCT_NAME)) LIKE '% ˭ԧL14 %') AND UPPER(TRIM(CATEGORY_FINAL)) = 'FASHION' THEN 'GIRL'
								WHEN (UPPER(TRIM(PRODUCT_NAME)) LIKE '% L64' OR UPPER(TRIM(PRODUCT_NAME)) LIKE 'L64%' OR UPPER(TRIM(PRODUCT_NAME)) LIKE '% L64 %') AND UPPER(TRIM(CATEGORY_FINAL)) = 'FASHION' THEN 'BOY'
								WHEN (UPPER(TRIM(PRODUCT_NAME)) LIKE '% ˭ԧXL46' OR UPPER(TRIM(PRODUCT_NAME)) LIKE '˭ԧXL46%' OR UPPER(TRIM(PRODUCT_NAME)) LIKE '% ˭ԧXL46 %') AND UPPER(TRIM(CATEGORY_FINAL)) = 'FASHION' THEN 'GIRL'
								WHEN (UPPER(TRIM(PRODUCT_NAME)) LIKE '% ���XXL10' OR UPPER(TRIM(PRODUCT_NAME)) LIKE '���XXL10%' OR UPPER(TRIM(PRODUCT_NAME)) LIKE '% ���XXL10 %') AND UPPER(TRIM(CATEGORY_FINAL)) = 'FASHION' THEN 'BOY'
								WHEN (UPPER(TRIM(PRODUCT_NAME)) LIKE '% �M64' OR UPPER(TRIM(PRODUCT_NAME)) LIKE '�M64%' OR UPPER(TRIM(PRODUCT_NAME)) LIKE '% �M64 %') AND UPPER(TRIM(CATEGORY_FINAL)) = 'FASHION' THEN 'BOY'
								WHEN (UPPER(TRIM(PRODUCT_NAME)) LIKE '% �ǴRPP4OZ.�Ե���+�ء�Թ�S' OR UPPER(TRIM(PRODUCT_NAME)) LIKE '�ǴRPP4OZ.�Ե���+�ء�Թ�S%' OR UPPER(TRIM(PRODUCT_NAME)) LIKE '% �ǴRPP4OZ.�Ե���+�ء�Թ�S %') AND UPPER(TRIM(CATEGORY_FINAL)) = 'FASHION' THEN 'GIRL'
								WHEN (UPPER(TRIM(PRODUCT_NAME)) LIKE '% ���XL46' OR UPPER(TRIM(PRODUCT_NAME)) LIKE '���XL46%' OR UPPER(TRIM(PRODUCT_NAME)) LIKE '% ���XL46 %') AND UPPER(TRIM(CATEGORY_FINAL)) = 'FASHION' THEN 'BOY'
								WHEN (UPPER(TRIM(PRODUCT_NAME)) LIKE '% ˭ԧL52' OR UPPER(TRIM(PRODUCT_NAME)) LIKE '˭ԧL52%' OR UPPER(TRIM(PRODUCT_NAME)) LIKE '% ˭ԧL52 %') AND UPPER(TRIM(CATEGORY_FINAL)) = 'FASHION' THEN 'GIRL'
								WHEN (UPPER(TRIM(PRODUCT_NAME)) LIKE '% ���XL12' OR UPPER(TRIM(PRODUCT_NAME)) LIKE '���XL12%' OR UPPER(TRIM(PRODUCT_NAME)) LIKE '% ���XL12 %') AND UPPER(TRIM(CATEGORY_FINAL)) = 'FASHION' THEN 'BOY'
								WHEN (UPPER(TRIM(PRODUCT_NAME)) LIKE '% ���L52' OR UPPER(TRIM(PRODUCT_NAME)) LIKE '���L52%' OR UPPER(TRIM(PRODUCT_NAME)) LIKE '% ���L52 %') AND UPPER(TRIM(CATEGORY_FINAL)) = 'FASHION' THEN 'BOY'
								WHEN (UPPER(TRIM(PRODUCT_NAME)) LIKE '% TWINZ' OR UPPER(TRIM(PRODUCT_NAME)) LIKE 'TWINZ%' OR UPPER(TRIM(PRODUCT_NAME)) LIKE '% TWINZ %') AND UPPER(TRIM(CATEGORY_FINAL)) = 'FASHION' THEN 'GIRL'
								WHEN (UPPER(TRIM(PRODUCT_NAME)) LIKE '% ���M17' OR UPPER(TRIM(PRODUCT_NAME)) LIKE '���M17%' OR UPPER(TRIM(PRODUCT_NAME)) LIKE '% ���M17 %') AND UPPER(TRIM(CATEGORY_FINAL)) = 'FASHION' THEN 'BOY'
								WHEN (UPPER(TRIM(PRODUCT_NAME)) LIKE '% �ç�տѹ��2-4Y����' OR UPPER(TRIM(PRODUCT_NAME)) LIKE '�ç�տѹ��2-4Y����%' OR UPPER(TRIM(PRODUCT_NAME)) LIKE '% �ç�տѹ��2-4Y���� %') AND UPPER(TRIM(CATEGORY_FINAL)) = 'FASHION' THEN 'GIRL'
								WHEN (UPPER(TRIM(PRODUCT_NAME)) LIKE '% ˭ԧM17' OR UPPER(TRIM(PRODUCT_NAME)) LIKE '˭ԧM17%' OR UPPER(TRIM(PRODUCT_NAME)) LIKE '% ˭ԧM17 %') AND UPPER(TRIM(CATEGORY_FINAL)) = 'FASHION' THEN 'GIRL'
								WHEN (UPPER(TRIM(PRODUCT_NAME)) LIKE '% �ѡ�����ŵ���PANTS���' OR UPPER(TRIM(PRODUCT_NAME)) LIKE '�ѡ�����ŵ���PANTS���%' OR UPPER(TRIM(PRODUCT_NAME)) LIKE '% �ѡ�����ŵ���PANTS��� %') AND UPPER(TRIM(CATEGORY_FINAL)) = 'FASHION' THEN 'BOY'
								WHEN (UPPER(TRIM(PRODUCT_NAME)) LIKE '% BOY' OR UPPER(TRIM(PRODUCT_NAME)) LIKE 'BOY%' OR UPPER(TRIM(PRODUCT_NAME)) LIKE '% BOY %') AND UPPER(TRIM(CATEGORY_FINAL)) = 'FASHION' THEN 'BOY'
								WHEN (UPPER(TRIM(PRODUCT_NAME)) LIKE '% GIRL' OR UPPER(TRIM(PRODUCT_NAME)) LIKE 'GIRL%' OR UPPER(TRIM(PRODUCT_NAME)) LIKE '% GIRL %') AND UPPER(TRIM(CATEGORY_FINAL)) = 'FASHION' THEN 'GIRL'
								WHEN (UPPER(TRIM(PRODUCT_NAME)) LIKE '% KITTY' OR UPPER(TRIM(PRODUCT_NAME)) LIKE 'KITTY%' OR UPPER(TRIM(PRODUCT_NAME)) LIKE '% KITTY %') AND UPPER(TRIM(CATEGORY_FINAL)) = 'FASHION' THEN 'GIRL'	
END
;

UPDATE VPM_DATA.PM_SKUTAGGING_KIDS_PRODUCT_MASTER
SET KID_GENDER_FINAL = UPPER(TRIM(KID_GENDER_KEYWORD))
WHERE KID_GENDER_FINAL IS NULL
;
/*SUB CLASS*/

UPDATE VPM_DATA.PM_SKUTAGGING_KIDS_PRODUCT_MASTER
SET KID_GENDER_SUBCLASS = UPPER(TRIM(TEMP.KID_GENDER_FINAL))
FROM
	(
	SELECT * 
	FROM 
		(
		SELECT A.PARTNER_CODE,A.DEPT_NAME,A.SUBDEPT_NAME,A.CLASS_NAME,A.SUBCLASS_NAME,A.KID_GENDER_FINAL,TTL,COUNT(DISTINCT CASE WHEN KID_GENDER_FINAL IS NOT NULL THEN  SKU_CODE END ) AS CNT_ITM
		,CASE WHEN TTL_ITM > 0 THEN COUNT(DISTINCT CASE WHEN KID_GENDER_FINAL IS NOT NULL THEN  SKU_CODE END )*1.0/TTL_ITM*1.0 ELSE 0 END AS PERCENTAGE,TTL_ITM*1.0/TTL*1.0 AS RATIO,ROW_NUMBER()OVER(PARTITION BY A.PARTNER_CODE,A.DEPT_NAME,A.SUBDEPT_NAME,A.CLASS_NAME,A.SUBCLASS_NAME ORDER BY CASE WHEN TTL_ITM > 0 THEN COUNT(DISTINCT CASE WHEN KID_GENDER_FINAL IS NOT NULL THEN  SKU_CODE END)*1.0/TTL_ITM*1.0 ELSE 0 END DESC NULLS LAST) RK 
		FROM VPM_DATA.PM_SKUTAGGING_KIDS_PRODUCT_MASTER A
		INNER JOIN 
			   (
			   SELECT PARTNER_CODE,DEPT_NAME,SUBDEPT_NAME,CLASS_NAME,SUBCLASS_NAME,COUNT(*) AS TTL,COUNT(DISTINCT CASE WHEN KID_GENDER_FINAL IS NOT NULL THEN  SKU_CODE END ) AS TTL_ITM
			   FROM VPM_DATA.PM_SKUTAGGING_KIDS_PRODUCT_MASTER
			   --WHERE KID_GENDER_FINAL IS NOT NULL
			   GROUP BY PARTNER_CODE,DEPT_NAME,SUBDEPT_NAME,CLASS_NAME,SUBCLASS_NAME
			   ) B
		ON UPPER(TRIM(A.PARTNER_CODE)) = UPPER(TRIM(B.PARTNER_CODE))
		AND UPPER(TRIM(A.DEPT_NAME)) = UPPER(TRIM(B.DEPT_NAME))
		AND UPPER(TRIM(A.SUBDEPT_NAME)) = UPPER(TRIM(B.SUBDEPT_NAME))
		AND UPPER(TRIM(A.CLASS_NAME)) = UPPER(TRIM(B.CLASS_NAME))
		AND UPPER(TRIM(A.SUBCLASS_NAME)) = UPPER(TRIM(B.SUBCLASS_NAME))
		GROUP BY A.PARTNER_CODE,A.DEPT_NAME,A.SUBDEPT_NAME,A.CLASS_NAME,A.SUBCLASS_NAME,A.KID_GENDER_FINAL,TTL,TTL_ITM
		) TMP 
	WHERE (CNT_ITM >= 50 OR  (RATIO >=0.5 AND CNT_ITM > 10)) AND PERCENTAGE > 0.85 AND RK = 1
	) TEMP
WHERE UPPER(TRIM(VPM_DATA.PM_SKUTAGGING_KIDS_PRODUCT_MASTER.PARTNER_CODE)) = UPPER(TRIM(TEMP.PARTNER_CODE))
AND UPPER(TRIM(VPM_DATA.PM_SKUTAGGING_KIDS_PRODUCT_MASTER.DEPT_NAME)) = UPPER(TRIM(TEMP.DEPT_NAME))
AND UPPER(TRIM(VPM_DATA.PM_SKUTAGGING_KIDS_PRODUCT_MASTER.SUBDEPT_NAME)) = UPPER(TRIM(TEMP.SUBDEPT_NAME))
AND UPPER(TRIM(VPM_DATA.PM_SKUTAGGING_KIDS_PRODUCT_MASTER.CLASS_NAME)) = UPPER(TRIM(TEMP.CLASS_NAME))
AND UPPER(TRIM(VPM_DATA.PM_SKUTAGGING_KIDS_PRODUCT_MASTER.SUBCLASS_NAME)) = UPPER(TRIM(TEMP.SUBCLASS_NAME))
AND VPM_DATA.PM_SKUTAGGING_KIDS_PRODUCT_MASTER.KID_GENDER_FINAL IS NULL 
;
/*CLASS*/
UPDATE  VPM_DATA.PM_SKUTAGGING_KIDS_PRODUCT_MASTER
SET KID_GENDER_SUBCLASS = TEMP.KID_GENDER_FINAL
FROM
	(
		SELECT * 
		FROM 
			(
			SELECT A.PARTNER_CODE,A.DEPT_NAME,A.SUBDEPT_NAME,A.CLASS_NAME,A.KID_GENDER_FINAL,TTL,COUNT(DISTINCT CASE WHEN KID_GENDER_FINAL IS NOT NULL THEN  SKU_CODE END ) AS CNT_ITM
			,CASE WHEN TTL_ITM > 0 THEN COUNT(DISTINCT CASE WHEN KID_GENDER_FINAL IS NOT NULL THEN  SKU_CODE END )*1.0/TTL_ITM*1.0 ELSE 0 END AS PERCENTAGE,TTL_ITM*1.0/TTL*1.0 AS RATIO,ROW_NUMBER()OVER(PARTITION BY A.PARTNER_CODE,A.DEPT_NAME,A.SUBDEPT_NAME,A.CLASS_NAME ORDER BY CASE WHEN TTL_ITM > 0 THEN COUNT(DISTINCT CASE WHEN KID_GENDER_FINAL IS NOT NULL THEN  SKU_CODE END )*1.0/TTL_ITM*1.0 ELSE 0 END DESC NULLS LAST) RK 
			FROM VPM_DATA.PM_SKUTAGGING_KIDS_PRODUCT_MASTER A
			INNER JOIN 
				   (
				   SELECT PARTNER_CODE,DEPT_NAME,SUBDEPT_NAME,CLASS_NAME,COUNT(*) AS TTL,COUNT(DISTINCT CASE WHEN KID_GENDER_FINAL IS NOT NULL THEN  SKU_CODE END ) AS TTL_ITM
				   FROM VPM_DATA.PM_SKUTAGGING_KIDS_PRODUCT_MASTER
				   --WHERE KID_GENDER_FINAL IS NOT NULL
				   GROUP BY PARTNER_CODE,DEPT_NAME,SUBDEPT_NAME,CLASS_NAME
				   ) B
			ON UPPER(TRIM(A.PARTNER_CODE)) = UPPER(TRIM(B.PARTNER_CODE))
			AND UPPER(TRIM(A.DEPT_NAME)) = UPPER(TRIM(B.DEPT_NAME))
			AND UPPER(TRIM(A.SUBDEPT_NAME)) = UPPER(TRIM(B.SUBDEPT_NAME))
			AND UPPER(TRIM(A.CLASS_NAME)) = UPPER(TRIM(B.CLASS_NAME))
			GROUP BY A.PARTNER_CODE,A.DEPT_NAME,A.SUBDEPT_NAME,A.CLASS_NAME,A.KID_GENDER_FINAL,TTL,TTL_ITM
			) TMP 
		WHERE (CNT_ITM >= 500 OR  (RATIO >=0.5 AND CNT_ITM > 50)) AND PERCENTAGE > 0.90 AND RK = 1
		) TEMP
WHERE UPPER(TRIM(VPM_DATA.PM_SKUTAGGING_KIDS_PRODUCT_MASTER.PARTNER_CODE)) = UPPER(TRIM(TEMP.PARTNER_CODE))
AND UPPER(TRIM(VPM_DATA.PM_SKUTAGGING_KIDS_PRODUCT_MASTER.DEPT_NAME)) = UPPER(TRIM(TEMP.DEPT_NAME))
AND UPPER(TRIM(VPM_DATA.PM_SKUTAGGING_KIDS_PRODUCT_MASTER.SUBDEPT_NAME)) = UPPER(TRIM(TEMP.SUBDEPT_NAME))
AND VPM_DATA.PM_SKUTAGGING_KIDS_PRODUCT_MASTER.KID_GENDER_FINAL IS NULL 
AND VPM_DATA.PM_SKUTAGGING_KIDS_PRODUCT_MASTER.KID_GENDER_SUBCLASS IS NULL
;
UPDATE VPM_DATA.PM_SKUTAGGING_KIDS_PRODUCT_MASTER
SET KID_GENDER_FINAL = UPPER(TRIM(KID_GENDER_SUBCLASS))
WHERE KID_GENDER_FINAL IS NULL 
;

/************************************************** DIAPER **************************************************/

UPDATE VPM_DATA.PM_SKUTAGGING_KIDS_PRODUCT_MASTER
SET DIAPER_SIZE = CASE WHEN (UPPER(TRIM(PRODUCT_NAME)) LIKE '%S[0-9]%'OR UPPER(TRIM(PRODUCT_NAME)) LIKE '% S %') THEN 'S'
					   WHEN (UPPER(TRIM(PRODUCT_NAME)) LIKE '%M[0-9]%'OR UPPER(TRIM(PRODUCT_NAME)) LIKE '% M %') THEN 'M'
					   WHEN (UPPER(TRIM(PRODUCT_NAME)) LIKE '%L[0-9]%'OR UPPER(TRIM(PRODUCT_NAME)) LIKE '% L %') THEN 'L'
					   WHEN (UPPER(TRIM(PRODUCT_NAME)) LIKE '%NB%'OR UPPER(TRIM(PRODUCT_NAME)) LIKE '%NEW BORN%' OR UPPER(TRIM(PRODUCT_NAME)) LIKE '%NEWBORN%') THEN 'NB'
					   WHEN (UPPER(TRIM(PRODUCT_NAME)) LIKE '%XL%'OR UPPER(TRIM(PRODUCT_NAME)) LIKE '% XL %') THEN 'XL'
					   ELSE NULL END
WHERE TYPE_FINAL LIKE '%DIAPER%'
;


UPDATE VPM_DATA.PM_SKUTAGGING_KIDS_PRODUCT_MASTER
SET DIAPER_PCS = CASE WHEN UPPER(TRIM(PRODUCT_NAME)) LIKE '%PCSX4%' AND UPPER(TRIM(DIAPER_SIZE)) IS NOT NULL THEN CAST(LEFT(SUBSTRING(UPPER(TRIM(PRODUCT_NAME)),  VPM_DATA.PATINDEX('%[0-9]%', UPPER(TRIM(PRODUCT_NAME))), LENGTH(UPPER(TRIM(PRODUCT_NAME)))), VPM_DATA.PATINDEX('%[^0-9]%', SUBSTRING(UPPER(TRIM(PRODUCT_NAME)),  VPM_DATA.PATINDEX('%[0-9]%', UPPER(TRIM(PRODUCT_NAME))), LENGTH(UPPER(TRIM(PRODUCT_NAME)))) || 'T') - 1) AS FLOAT)*4
						WHEN UPPER(TRIM(DIAPER_SIZE)) IS NOT NULL AND VPM_DATA.PATINDEX_STAGE('%[0-9]%', UPPER(TRIM(PRODUCT_NAME))) > 0 THEN VPM_DATA.GET_SIZE(PRODUCT_NAME,'')
						WHEN VPM_DATA.PATINDEX('%[0-9]%', UPPER(TRIM(PRODUCT_NAME))) <= 0 THEN 0
						ELSE NULL END
;

/************************************************** KID STAGE **************************************************/

/*SUB CLASS*/

UPDATE  VPM_DATA.PM_SKUTAGGING_KIDS_PRODUCT_MASTER
SET KID_STAGE_SUBCLASS = TEMP.KIDS_STAGE_FINAL
FROM
(
SELECT * 
FROM 
	(
	SELECT A.PARTNER_CODE,A.DEPT_NAME,A.SUBDEPT_NAME,A.CLASS_NAME,A.SUBCLASS_NAME,A.KIDS_STAGE_FINAL,TTL,COUNT(DISTINCT CASE WHEN KIDS_STAGE_FINAL IS NOT NULL THEN  SKU_CODE END ) AS CNT_ITM
	,CASE WHEN TTL_ITM > 0 THEN COUNT(DISTINCT CASE WHEN KIDS_STAGE_FINAL IS NOT NULL THEN  SKU_CODE END )*1.0/TTL_ITM*1.0 ELSE 0 END AS PERCENTAGE,TTL_ITM*1.0/TTL*1.0 AS RATIO,ROW_NUMBER()OVER(PARTITION BY A.PARTNER_CODE,A.DEPT_NAME,A.SUBDEPT_NAME,A.CLASS_NAME,A.SUBCLASS_NAME ORDER BY CASE WHEN TTL_ITM > 0 THEN COUNT(DISTINCT CASE WHEN KIDS_STAGE_FINAL IS NOT NULL THEN  SKU_CODE END )*1.0/TTL_ITM*1.0 ELSE 0 END DESC NULLS LAST) RK 
	FROM VPM_DATA.PM_SKUTAGGING_KIDS_PRODUCT_MASTER A
	INNER JOIN 
		   (
		   SELECT PARTNER_CODE,DEPT_NAME,SUBDEPT_NAME,CLASS_NAME,SUBCLASS_NAME,COUNT(*) AS TTL,COUNT(DISTINCT CASE WHEN KIDS_STAGE_FINAL IS NOT NULL THEN  SKU_CODE END ) AS TTL_ITM
		   FROM VPM_DATA.PM_SKUTAGGING_KIDS_PRODUCT_MASTER
		   GROUP BY PARTNER_CODE,DEPT_NAME,SUBDEPT_NAME,CLASS_NAME,SUBCLASS_NAME
		   ) B
	ON UPPER(TRIM(A.PARTNER_CODE)) = UPPER(TRIM(B.PARTNER_CODE))
	AND UPPER(TRIM(A.DEPT_NAME)) = UPPER(TRIM(B.DEPT_NAME))
	AND UPPER(TRIM(A.SUBDEPT_NAME)) = UPPER(TRIM(B.SUBDEPT_NAME))
	AND UPPER(TRIM(A.CLASS_NAME)) = UPPER(TRIM(B.CLASS_NAME))
	AND UPPER(TRIM(A.SUBCLASS_NAME)) = UPPER(TRIM(B.SUBCLASS_NAME))
	GROUP BY A.PARTNER_CODE,A.DEPT_NAME,A.SUBDEPT_NAME,A.CLASS_NAME,A.SUBCLASS_NAME,A.KIDS_STAGE_FINAL,TTL,TTL_ITM
	) TMP 
WHERE (CNT_ITM >= 50 OR  (RATIO >=0.5 AND CNT_ITM > 10)) AND PERCENTAGE > 0.85 AND RK = 1
) TEMP
WHERE UPPER(TRIM(VPM_DATA.PM_SKUTAGGING_KIDS_PRODUCT_MASTER.PARTNER_CODE)) = UPPER(TRIM(TEMP.PARTNER_CODE))
AND UPPER(TRIM(VPM_DATA.PM_SKUTAGGING_KIDS_PRODUCT_MASTER.DEPT_NAME)) = UPPER(TRIM(TEMP.DEPT_NAME))
AND UPPER(TRIM(VPM_DATA.PM_SKUTAGGING_KIDS_PRODUCT_MASTER.SUBDEPT_NAME)) = UPPER(TRIM(TEMP.SUBDEPT_NAME))
AND UPPER(TRIM(VPM_DATA.PM_SKUTAGGING_KIDS_PRODUCT_MASTER.CLASS_NAME)) = UPPER(TRIM(TEMP.CLASS_NAME))
AND UPPER(TRIM(VPM_DATA.PM_SKUTAGGING_KIDS_PRODUCT_MASTER.SUBCLASS_NAME)) = UPPER(TRIM(TEMP.SUBCLASS_NAME))
AND VPM_DATA.PM_SKUTAGGING_KIDS_PRODUCT_MASTER.KIDS_STAGE_FINAL IS NULL 
;
/*CLASS*/
UPDATE  VPM_DATA.PM_SKUTAGGING_KIDS_PRODUCT_MASTER
SET KID_STAGE_SUBCLASS = TEMP.KIDS_STAGE_FINAL
FROM
	(
		SELECT * 
		FROM 
			(
			SELECT A.PARTNER_CODE,A.DEPT_NAME,A.SUBDEPT_NAME,A.CLASS_NAME,A.KIDS_STAGE_FINAL,TTL,COUNT(DISTINCT CASE WHEN KIDS_STAGE_FINAL IS NOT NULL THEN  SKU_CODE END ) AS CNT_ITM
			,CASE WHEN TTL_ITM > 0 THEN COUNT(DISTINCT CASE WHEN KIDS_STAGE_FINAL IS NOT NULL THEN  SKU_CODE END )*1.0/TTL_ITM*1.0 ELSE 0 END AS PERCENTAGE,TTL_ITM*1.0/TTL*1.0 AS RATIO,ROW_NUMBER()OVER(PARTITION BY A.PARTNER_CODE,A.DEPT_NAME,A.SUBDEPT_NAME,A.CLASS_NAME ORDER BY CASE WHEN TTL_ITM > 0 THEN COUNT(DISTINCT CASE WHEN KIDS_STAGE_FINAL IS NOT NULL THEN  SKU_CODE END )*1.0/TTL_ITM*1.0 ELSE 0 END DESC NULLS LAST) RK 
			FROM VPM_DATA.PM_SKUTAGGING_KIDS_PRODUCT_MASTER A
			INNER JOIN 
				   (
				   SELECT PARTNER_CODE,DEPT_NAME,SUBDEPT_NAME,CLASS_NAME,COUNT(*) AS TTL,COUNT(DISTINCT CASE WHEN KIDS_STAGE_FINAL IS NOT NULL THEN  SKU_CODE END ) AS TTL_ITM
				   FROM VPM_DATA.PM_SKUTAGGING_KIDS_PRODUCT_MASTER
				   --WHERE KIDS_STAGE_FINAL IS NOT NULL
				   GROUP BY PARTNER_CODE,DEPT_NAME,SUBDEPT_NAME,CLASS_NAME
				   ) B
			ON UPPER(TRIM(A.PARTNER_CODE)) = UPPER(TRIM(B.PARTNER_CODE))
			AND UPPER(TRIM(A.DEPT_NAME)) = UPPER(TRIM(B.DEPT_NAME))
			AND UPPER(TRIM(A.SUBDEPT_NAME)) = UPPER(TRIM(B.SUBDEPT_NAME))
			AND UPPER(TRIM(A.CLASS_NAME)) = UPPER(TRIM(B.CLASS_NAME))
			GROUP BY A.PARTNER_CODE,A.DEPT_NAME,A.SUBDEPT_NAME,A.CLASS_NAME,A.KIDS_STAGE_FINAL,TTL,TTL_ITM
			) TMP 
	WHERE (CNT_ITM >= 500 OR  (RATIO >=0.5 AND CNT_ITM > 50)) AND PERCENTAGE > 0.90 AND RK = 1
	) TEMP
WHERE  UPPER(TRIM(VPM_DATA.PM_SKUTAGGING_KIDS_PRODUCT_MASTER.PARTNER_CODE)) = UPPER(TRIM(TEMP.PARTNER_CODE))
AND UPPER(TRIM(VPM_DATA.PM_SKUTAGGING_KIDS_PRODUCT_MASTER.DEPT_NAME)) = UPPER(TRIM(TEMP.DEPT_NAME))
AND UPPER(TRIM(VPM_DATA.PM_SKUTAGGING_KIDS_PRODUCT_MASTER.SUBDEPT_NAME)) = UPPER(TRIM(TEMP.SUBDEPT_NAME))
AND VPM_DATA.PM_SKUTAGGING_KIDS_PRODUCT_MASTER.KIDS_STAGE_FINAL IS NULL;

UPDATE VPM_DATA.PM_SKUTAGGING_KIDS_PRODUCT_MASTER
SET KIDS_STAGE_FINAL =  CASE WHEN UPPER(TRIM(KIDS_STAGE_FINAL)) = 'TODDLE' THEN 'TODDLER' ELSE KIDS_STAGE_FINAL END;

UPDATE VPM_DATA.PM_SKUTAGGING_KIDS_PRODUCT_MASTER
SET KIDS_STAGE_FINAL = KID_STAGE_SUBCLASS
WHERE KIDS_STAGE_FINAL IS NULL ;

UPDATE VPM_DATA.PM_SKUTAGGING_KIDS_PRODUCT_MASTER
SET KIDS_STAGE_FINAL = 'TODDLER'
WHERE KIDS_STAGE_FINAL IS NULL AND UPPER(CLASS_NAME) = 'TODDLER';

/*CLEANED MORE STAGE*/
UPDATE VPM_DATA.PM_SKUTAGGING_KIDS_PRODUCT_MASTER
SET KIDS_STAGE_FINAL = CASE WHEN UPPER(TRIM(TYPE_FINAL)) IN ('MILK PUMP MACHINE','BABY CARRIER','BABY CRADLE','BABY FOOD BOOK','BABY NAME BOOK','BABY WALKER','BOUNCER/SWING','BREAST PADS','BABY BOUNCER',
												'CAR SEAT','CHILDCARE BOOK','FEEDING ACCESSORIES','HOT WATER BAG','MATERNITY','MILK STORAGE','PLAYPEN',
												'PREGNANCY BOOK','RUBBER PAD','STICKER','STROLLER','SUCKER','THERMOMETER','TOOTHBRUSH','WEANING','BREAST PUMPING')
							THEN 'NEWBORN,INFANT,TODDLER' ELSE UPPER(TRIM(KIDS_STAGE_FINAL)) END
WHERE KIDS_STAGE_FINAL IS NULL;

UPDATE VPM_DATA.PM_SKUTAGGING_KIDS_PRODUCT_MASTER
SET KIDS_STAGE_FINAL   = CASE WHEN UPPER(SUBCLASS_NAME) = 'INFANT' THEN  'NEWBORN,INFANT'  
							 WHEN UPPER(TRIM(PRODUCT_NAME)) LIKE '%����ͼ������͹%' THEN 'NEWBORN,INFANT'
							 WHEN UPPER(TRIM(PRODUCT_NAME)) LIKE '%BREAST PUMP%' THEN  'NEWBORN,INFANT'
							 WHEN UPPER(TRIM(TYPE_FINAL)) IN( 'BREAST PUMPING' ,'MILK PUMP MACHINE','PREGNANCY BOOK','BABY FOOD BOOK','BABY NAME BOOK','MATERNITY') THEN 'NEWBORN,INFANT'
							 ELSE UPPER(TRIM(KIDS_STAGE_FINAL))   END
WHERE KIDS_STAGE_FINAL IS NULL;

/************************************************** CLEAN MORE **************************************************/

UPDATE VPM_DATA.PM_SKUTAGGING_KIDS_PRODUCT_MASTER
SET TYPE_FINAL = CASE WHEN UPPER(TRIM(KIDS_STAGE_FINAL)) = 'NEWBORN,INFANT' AND UPPER(TRIM(TYPE_FINAL)) = 'OUTDOOR' AND UPPER(TRIM(PRODUCT_NAME)) LIKE '%BATH%' THEN 'BATHING'
                      WHEN UPPER(TRIM(KIDS_STAGE_FINAL)) = 'NEWBORN,INFANT' AND UPPER(TRIM(TYPE_FINAL)) = 'OUTDOOR' AND UPPER(TRIM(PRODUCT_NAME)) LIKE '%�ش��%' THEN 'OTHER'
                      WHEN UPPER(TRIM(KIDS_STAGE_FINAL)) = 'NEWBORN,INFANT' AND UPPER(TRIM(TYPE_FINAL)) = 'WASHING' THEN 'BATHING'
					  WHEN UPPER(TRIM(KIDS_STAGE_FINAL)) = 'NEWBORN,INFANT' AND UPPER(TRIM(PRODUCT_NAME)) LIKE '%���ٴ%' THEN 'FEEDING BOTTLE AND NIPPLE'
					  WHEN UPPER(TRIM(TYPE_FINAL)) = 'BABY CRADLE' THEN 'ACCESSORIES' 
					  WHEN UPPER(TRIM(TYPE_FINAL)) IN ( 'PLAYPEN' ,'BOUNCER/SWING') THEN 'BABY BOUNCER'
					  WHEN UPPER(TRIM(TYPE_FINAL)) = 'RUBBER PAD' THEN 'BEDDING'     
					  WHEN UPPER(SUBCLASS_NAME) = 'PILLOW/BUMPER' THEN 'BEDDING'   
					  WHEN UPPER(SUBCLASS_NAME) = 'BABY FOOD' AND PARTNER_CODE != 'B2S' THEN 'NURISHMENT' 
					  WHEN UPPER(SUBCLASS_NAME) = 'BABY FOOD' AND PARTNER_CODE = 'CFR' THEN 'NURISHMENT' 
					  WHEN UPPER(TRIM(KIDS_STAGE_FINAL)) = 'NEWBORN,INFANT' AND UPPER(TRIM(TYPE_FINAL)) = 'UNDERWEAR'  THEN 'DIAPERS'
					  WHEN UPPER(TRIM(TYPE_FINAL)) = 'BOOK' THEN 'BOOK'
					  WHEN UPPER(SUBDEPT_NAME) LIKE '%SHOES%' THEN 'FOOTWEAR'
					  WHEN UPPER(TRIM(TYPE_FINAL)) IN ('SUCKER') THEN 'FEEDING BOTTLE AND NIPPLE'
					  ELSE UPPER(TRIM(TYPE_FINAL))
					  END                                                                             				                                                         
;                                                           

UPDATE VPM_DATA.PM_SKUTAGGING_KIDS_PRODUCT_MASTER
SET TYPE_FINAL = CASE WHEN UPPER(TRIM(TYPE_FINAL)) = 'WEANING' AND UPPER(TRIM(PRODUCT_NAME)) LIKE '%�ҧࡧ%' THEN 'DAYWEAR'
					  WHEN UPPER(TRIM(TYPE_FINAL)) = 'WEANING' AND UPPER(TRIM(PRODUCT_NAME)) LIKE '%���͹%' THEN 'BEDDING'
					  WHEN UPPER(TRIM(TYPE_FINAL)) = 'BREAST PADS' AND UPPER(TRIM(PRODUCT_NAME)) LIKE '%��Ң�˹�%' THEN 'FABRIC'
					  WHEN UPPER(TRIM(PRODUCT_NAME)) LIKE '%�蹫���⤹%' AND UPPER(TRIM(PRODUCT_NAME)) LIKE '%��%' THEN 'BREAST PADS'
					  WHEN UPPER(TRIM(PRODUCT_NAME)) LIKE '%��ͧ�ѹ%��ǹ�%' OR UPPER(TRIM(PRODUCT_NAME)) LIKE '%NIPPLE%SHIELD%' OR UPPER(TRIM(PRODUCT_NAME)) LIKE '%BREAST%PAD%' OR UPPER(TRIM(PRODUCT_NAME)) LIKE '%��%��ӹ�%' THEN 'BREAST PADS'
					  WHEN UPPER(TRIM(PRODUCT_NAME)) LIKE '%�����%��ҧ%' OR UPPER(TRIM(PRODUCT_NAME)) LIKE '%�����%�ѡ%' OR UPPER(TRIM(PRODUCT_NAME)) LIKE '%��ҹ���%' THEN 'WASHING'
					  WHEN UPPER(TRIM(PRODUCT_NAME)) LIKE '%����%' THEN 'PERSONAL CARE'
					  WHEN UPPER(TRIM(TYPE_FINAL)) = 'BATHING' AND UPPER(TRIM(CATEGORY_FINAL)) = 'NECESSITIES'
							AND UPPER(TRIM(PRODUCT_NAME)) LIKE '%ö��%' AND UPPER(TRIM(PRODUCT_NAME)) NOT LIKE '%���ͧ%' AND UPPER(TRIM(PRODUCT_NAME)) NOT LIKE '%����ͧ%' AND UPPER(TRIM(PRODUCT_NAME)) NOT LIKE '%�ǹ%' AND UPPER(TRIM(PRODUCT_NAME)) NOT LIKE '%�Т�%'  THEN 'ACCESSORIES'		
					  WHEN UPPER(TRIM(PRODUCT_NAME)) LIKE '%��%�¡%' AND UPPER(TRIM(PRODUCT_NAME)) NOT LIKE '%��Ҥ���%' AND UPPER(TRIM(PRODUCT_NAME)) NOT LIKE '%���%' AND UPPER(SUBCLASS_NAME) NOT LIKE '%PLAYPEN%' THEN 'BABY BOUNCER'	
					  WHEN UPPER(TRIM(TYPE_FINAL)) != 'MILK PUMP MACHINE' AND (UPPER(TRIM(PRODUCT_NAME)) LIKE '%����ͧ%����%��%' OR UPPER(TRIM(PRODUCT_NAME)) LIKE '%MILK%PUMP%') THEN 'MILK PUMP MACHINE'
					  WHEN UPPER(TRIM(PRODUCT_NAME)) LIKE '%������%���%��͹%' OR UPPER(TRIM(PRODUCT_NAME)) LIKE '%HOT%WATER%BAG%' THEN 'ACCESSORIES'
					  WHEN UPPER(TRIM(PRODUCT_NAME)) LIKE '%WIPES%' OR UPPER(TRIM(PRODUCT_NAME)) LIKE '%�Ǿ�%' THEN 'WIPES'
					ELSE UPPER(TRIM(TYPE_FINAL)) END  				
WHERE KIDS_STAGE_FINAL LIKE '%INFANT%'
;

UPDATE VPM_DATA.PM_SKUTAGGING_KIDS_PRODUCT_MASTER
SET TYPE_FINAL = CASE WHEN UPPER(TRIM(CATEGORY_FINAL)) = 'BOOKS' THEN 'BOOK'
					  WHEN UPPER(TRIM(CATEGORY_FINAL)) = 'TOYS' THEN 'TOY'
					  WHEN UPPER(TRIM(CATEGORY_FINAL)) = 'FASHION' AND UPPER(CLASS_NAME) LIKE '%SLEEPWEAR%' THEN 'NIGHTWEAR'
					  WHEN UPPER(TRIM(CATEGORY_FINAL)) = 'FASHION' AND UPPER(CLASS_NAME) LIKE '%HOSIERY%' THEN 'DAYWEAR'
					  WHEN UPPER(TRIM(CATEGORY_FINAL)) = 'FASHION' AND UPPER(CLASS_NAME) LIKE '%UNDERGARMENTS%' THEN 'UNDERWEAR'
					  WHEN UPPER(TRIM(CATEGORY_FINAL)) = 'NECESSITIES' AND UPPER(CLASS_NAME) LIKE '%MATERNITY%' THEN 'MATERNITY'
					  WHEN UPPER(TRIM(CATEGORY_FINAL)) = 'NECESSITIES' AND UPPER(CLASS_NAME) LIKE '%BATH%' THEN 'BATHING'
					  WHEN UPPER(TRIM(CATEGORY_FINAL)) = 'NECESSITIES' AND UPPER(CLASS_NAME) LIKE '%TOILETRIES%' THEN 'WASHING'
					  WHEN UPPER(TRIM(CATEGORY_FINAL)) = 'NECESSITIES' AND (UPPER(CLASS_NAME) LIKE '%SAFETY%' OR UPPER(SUBCLASS_NAME) LIKE '%SAFETY%') THEN 'SAFETY'
					  WHEN UPPER(DEPT_NAME) = 'BOOK' THEN 'BOOK'
					  WHEN UPPER(TRIM(CATEGORY_FINAL)) = 'FASHION' AND PARTNER_SUBCATEGORY_NAME IS NOT NULL THEN 'UNDERWEAR'
					  WHEN UPPER(TRIM(CATEGORY_FINAL)) = 'FASHION' AND PARTNER_SUBCATEGORY_NAME IS NULL THEN 'DAYWEAR'
					  WHEN UPPER(TRIM(CATEGORY_FINAL)) = 'FOOD' THEN
							CASE WHEN UPPER(CLASS_NAME) LIKE '%MILK%POWDER%' THEN 'MILK POWDER'
							ELSE 'NURISHMENT' END
					  WHEN COALESCE(UPPER(TRIM(CATEGORY_FINAL)),'') IN ('NECESSITIES','KID','') THEN
							CASE WHEN UPPER(TRIM(PRODUCT_NAME)) LIKE '%�ش%' OR UPPER(TRIM(PRODUCT_NAME)) LIKE '%�����%' OR UPPER(TRIM(PRODUCT_NAME)) LIKE '%�ا���%' OR UPPER(TRIM(PRODUCT_NAME)) LIKE '%�ا���%' OR UPPER(TRIM(PRODUCT_NAME)) LIKE '%����ç%' OR UPPER(TRIM(PRODUCT_NAME)) LIKE '%�ҧࡧ%' THEN 'DAYWEAR'
								 WHEN UPPER(TRIM(PRODUCT_NAME)) LIKE '%��Ҿѹ��%' OR UPPER(TRIM(PRODUCT_NAME)) LIKE '%��ǡ%' OR UPPER(TRIM(PRODUCT_NAME)) LIKE '%���Ҵ��%' OR UPPER(TRIM(PRODUCT_NAME)) LIKE '%����Ѵ%' OR UPPER(TRIM(PRODUCT_NAME)) LIKE '%��駤�ͺ%' OR
									  UPPER(TRIM(PRODUCT_NAME)) LIKE '%��Ҥ���%' OR UPPER(TRIM(PRODUCT_NAME)) LIKE '%���%' OR UPPER(TRIM(PRODUCT_NAME)) LIKE '%������%' OR UPPER(TRIM(PRODUCT_NAME)) LIKE '%���%' OR UPPER(TRIM(PRODUCT_NAME)) LIKE '%���%' OR UPPER(TRIM(PRODUCT_NAME)) LIKE '%���ö��%' THEN 'ACCESSORIES'
								 WHEN UPPER(TRIM(PRODUCT_NAME)) LIKE '%������%' OR UPPER(TRIM(PRODUCT_NAME)) LIKE '%����ͧ�͹%' THEN 'BEDDING'
								 WHEN UPPER(TRIM(PRODUCT_NAME)) LIKE '%�������§%' THEN 'SAFETY'
								 WHEN UPPER(TRIM(PRODUCT_NAME)) LIKE '%��Ң�%' THEN 'FABRIC'
								 WHEN UPPER(TRIM(PRODUCT_NAME)) LIKE '%��еԴö%' OR UPPER(TRIM(PRODUCT_NAME)) LIKE '%������%' THEN 'CAR SEAT'
								 WHEN UPPER(TRIM(PRODUCT_NAME)) LIKE '%�ѡ��ҹ%' OR UPPER(TRIM(PRODUCT_NAME)) LIKE '%ö������%' THEN 'TOY' END
					  WHEN UPPER(TRIM(TYPE_FINAL)) IN ('THERMOMETER') THEN 'ACCESSORIES'
				 ELSE UPPER(TRIM(TYPE_FINAL)) END
WHERE TYPE_FINAL IS NULL
;

UPDATE VPM_DATA.PM_SKUTAGGING_KIDS_PRODUCT_MASTER
SET TYPE_TEXT = CASE WHEN UPPER(TRIM(PRODUCT_NAME)) LIKE '%͹غ��%' THEN 'BOOK'
						WHEN UPPER(TRIM(PRODUCT_NAME)) LIKE '%�Ѵ��ҹ%' THEN 'BOOK'
						WHEN UPPER(TRIM(PRODUCT_NAME)) LIKE '%�Ѵ�Ѵ%' THEN 'BOOK'
						WHEN UPPER(TRIM(PRODUCT_NAME)) LIKE '%��ش�աB5%' THEN 'STATIONERY'
						WHEN UPPER(TRIM(PRODUCT_NAME)) LIKE '%ʹء�Ѻ��õԴʵԡ����%' THEN 'ART&CRAFT'
						WHEN UPPER(TRIM(PRODUCT_NAME)) LIKE '%�.2-B%' THEN 'BOOK'
						WHEN UPPER(TRIM(PRODUCT_NAME)) LIKE '%�.1-B%' THEN 'BOOK'
						WHEN UPPER(TRIM(PRODUCT_NAME)) LIKE '%�.1-B%' THEN 'BOOK'
						WHEN UPPER(TRIM(PRODUCT_NAME)) LIKE '%����)-B%' THEN 'BOOK'
						WHEN UPPER(TRIM(PRODUCT_NAME)) LIKE '%�Ѳ�����մ�%' THEN 'BOOK'
						WHEN UPPER(TRIM(PRODUCT_NAME)) LIKE '%��B%' THEN 'BOOK'
						WHEN UPPER(TRIM(PRODUCT_NAME)) LIKE '%�ҡ�����ŷ��Թ�Ź�����DONG%' THEN 'STATIONERY'
						WHEN UPPER(TRIM(PRODUCT_NAME)) LIKE '%�ҡ��%' THEN 'STATIONERY'
						WHEN UPPER(TRIM(PRODUCT_NAME)) LIKE '%�.1-B%' THEN 'BOOK'
						WHEN UPPER(TRIM(PRODUCT_NAME)) LIKE '%�Թ�͡�%' THEN 'STATIONERY'
						WHEN UPPER(TRIM(PRODUCT_NAME)) LIKE '%�Թ��%' THEN 'STATIONERY'
						WHEN UPPER(TRIM(PRODUCT_NAME)) LIKE '%�ͧ�Ի���Թ��%' THEN 'STATIONERY'
						WHEN UPPER(TRIM(PRODUCT_NAME)) LIKE '%�ͧ�ԻF4%' THEN 'STATIONERY'
						WHEN UPPER(TRIM(PRODUCT_NAME)) LIKE '%�ͧ�ԻA5%' THEN 'STATIONERY'
						WHEN UPPER(TRIM(PRODUCT_NAME)) LIKE '%��ꡫ���%' THEN 'BOOK'
						WHEN UPPER(TRIM(PRODUCT_NAME)) LIKE '%��Ե�Դ����%' THEN 'BOOK'
						WHEN UPPER(TRIM(PRODUCT_NAME)) LIKE '%�����ҴԹ��%' THEN 'STATIONERY'
						WHEN UPPER(TRIM(PRODUCT_NAME)) LIKE '%�����ҴԹ��%' THEN 'STATIONERY'
						WHEN UPPER(TRIM(PRODUCT_NAME)) LIKE '%TAPE%' THEN 'STATIONERY'
						WHEN UPPER(TRIM(PRODUCT_NAME)) LIKE '%SOCKS%' THEN 'STATIONERY'
						WHEN UPPER(TRIM(PRODUCT_NAME)) LIKE '%PVCNET%' THEN 'STATIONERY'
						WHEN UPPER(TRIM(PRODUCT_NAME)) LIKE '%POUCH%' THEN 'STATIONERY'
						WHEN UPPER(TRIM(PRODUCT_NAME)) LIKE '%NOTEBOOK%' THEN 'STATIONERY'
						WHEN UPPER(TRIM(PRODUCT_NAME)) LIKE '% MESSY %' THEN 'STATIONERY'
						WHEN UPPER(TRIM(PRODUCT_NAME)) LIKE '% MEMO %' THEN 'STATIONERY'
						WHEN UPPER(TRIM(PRODUCT_NAME)) LIKE '% MECH %' THEN 'STATIONERY'
						WHEN UPPER(TRIM(PRODUCT_NAME)) LIKE '% JIGSAW %' THEN 'BOOK'
						WHEN UPPER(TRIM(PRODUCT_NAME)) LIKE '% HEART %' THEN 'STATIONERY'
						WHEN UPPER(TRIM(PRODUCT_NAME)) LIKE '% FROZEN %' THEN 'BOOK'
						WHEN UPPER(TRIM(PRODUCT_NAME)) LIKE '%FOLDER%' THEN 'STATIONERY'
						WHEN UPPER(TRIM(PRODUCT_NAME)) LIKE '%FILE%' THEN 'STATIONERY'
						WHEN UPPER(TRIM(PRODUCT_NAME)) LIKE '%ERASER%' THEN 'STATIONERY'
						WHEN UPPER(TRIM(PRODUCT_NAME)) LIKE '% CUPCAKE %' THEN 'STATIONERY'
						WHEN UPPER(TRIM(PRODUCT_NAME)) LIKE '%COVER%' THEN 'STATIONERY'
						WHEN UPPER(TRIM(PRODUCT_NAME)) LIKE '%BLK-A%' THEN 'STATIONERY'
						WHEN UPPER(TRIM(PRODUCT_NAME)) LIKE '%BALLPOINT%' THEN 'STATIONERY'
						WHEN UPPER(TRIM(PRODUCT_NAME)) LIKE '%ALPHABET%' THEN 'STATIONERY'
						WHEN UPPER(TRIM(PRODUCT_NAME)) LIKE '% �Ǻ %' THEN 'BOOK'
						WHEN UPPER(TRIM(PRODUCT_NAME)) LIKE '% XO %' THEN 'STATIONERY'
						WHEN UPPER(TRIM(PRODUCT_NAME)) LIKE '% TS %' THEN 'STATIONERY'
						WHEN UPPER(TRIM(PRODUCT_NAME)) LIKE '% SUMMER %' THEN 'STATIONERY'
						WHEN UPPER(TRIM(PRODUCT_NAME)) LIKE '% STICK %' THEN 'BOOK'
						WHEN UPPER(TRIM(PRODUCT_NAME)) LIKE '% ROOM %' THEN 'STATIONERY'
						WHEN UPPER(TRIM(PRODUCT_NAME)) LIKE '% QUICK %' THEN 'BOOK'
						WHEN UPPER(TRIM(PRODUCT_NAME)) LIKE '% MM %' THEN 'STATIONERY'
						WHEN UPPER(TRIM(PRODUCT_NAME)) LIKE '% KT %' THEN 'STATIONERY'
						WHEN UPPER(TRIM(PRODUCT_NAME)) LIKE '% IQ %' THEN 'BOOK'
						WHEN UPPER(TRIM(PRODUCT_NAME)) LIKE '% F4 %' THEN 'STATIONERY'
						WHEN UPPER(TRIM(PRODUCT_NAME)) LIKE '% DESIGN %' THEN 'STATIONERY'
						WHEN UPPER(TRIM(PRODUCT_NAME)) LIKE '% DATA %' THEN 'STATIONERY'
						WHEN UPPER(TRIM(PRODUCT_NAME)) LIKE '% CASE %' THEN 'STATIONERY'
						WHEN UPPER(TRIM(PRODUCT_NAME)) LIKE '% CAP %' THEN 'STATIONERY'
						WHEN UPPER(TRIM(PRODUCT_NAME)) LIKE '% BAG %' THEN 'STATIONERY'
						WHEN UPPER(TRIM(PRODUCT_NAME)) LIKE '% BA %' THEN 'STATIONERY'
						WHEN UPPER(TRIM(PRODUCT_NAME)) LIKE '% ABC-B %' THEN 'BOOK'
						WHEN UPPER(TRIM(PRODUCT_NAME)) LIKE '% ABC %' THEN 'BOOK'
						WHEN UPPER(TRIM(PRODUCT_NAME)) LIKE '% A5 %' THEN 'STATIONERY'
						WHEN UPPER(TRIM(PRODUCT_NAME)) LIKE '% A4 %' THEN 'STATIONERY'
						WHEN UPPER(TRIM(PRODUCT_NAME)) LIKE '% 2-B %' THEN 'BOOK'
						WHEN UPPER(TRIM(PRODUCT_NAME)) LIKE '% 123-B %' THEN 'BOOK'
						ELSE NULL END
WHERE CATEGORY_FINAL = 'BOOKS'
;

UPDATE VPM_DATA.PM_SKUTAGGING_KIDS_PRODUCT_MASTER
SET TYPE_TEXT = CASE WHEN UPPER(TRIM(PRODUCT_NAME)) LIKE '%���¹��%' THEN 'UNDERWEAR'
						WHEN UPPER(TRIM(PRODUCT_NAME)) LIKE '%�ͧ����������%' THEN 'FOOTWEAR'
						WHEN UPPER(TRIM(PRODUCT_NAME)) LIKE '%�ͧ���ʵ��PRO%' THEN 'FOOTWEAR'
						WHEN UPPER(TRIM(PRODUCT_NAME)) LIKE '%�ͧ��Ҽ��㺹ѡ���¹%' THEN 'FOOTWEAR'
						WHEN UPPER(TRIM(PRODUCT_NAME)) LIKE '%�ͧ��ҹѡ���¹͹غ��˭ԧ%' THEN 'FOOTWEAR'
						WHEN UPPER(TRIM(PRODUCT_NAME)) LIKE '%�ͧ��ҹѡ���¹˭ԧ%' THEN 'FOOTWEAR'
						WHEN UPPER(TRIM(PRODUCT_NAME)) LIKE '%�ͧ�����%' THEN 'FOOTWEAR'
						WHEN UPPER(TRIM(PRODUCT_NAME)) LIKE '%��ҡѹ���͹�١��͡%' THEN 'ACCESSORIES'
						WHEN UPPER(TRIM(PRODUCT_NAME)) LIKE '%���Ҵ��%' THEN 'DAYWEAR'
						WHEN UPPER(TRIM(PRODUCT_NAME)) LIKE '%�ا��ҹѡ���¹COTTON%' THEN 'DAYWEAR'
						WHEN UPPER(TRIM(PRODUCT_NAME)) LIKE '%�ا��ҹѡ���¹%' THEN 'DAYWEAR'
						WHEN UPPER(TRIM(PRODUCT_NAME)) LIKE '%�ا��Ң�����%' THEN 'DAYWEAR'
						WHEN UPPER(TRIM(PRODUCT_NAME)) LIKE '%�ا��Ң��LOW%' THEN 'DAYWEAR'
						WHEN UPPER(TRIM(PRODUCT_NAME)) LIKE '%�ا���PACK3%' THEN 'DAYWEAR'
						WHEN UPPER(TRIM(PRODUCT_NAME)) LIKE '% SOCK%' OR UPPER(TRIM(PRODUCT_NAME)) LIKE 'SOCK%' THEN 'DAYWEAR'
						WHEN UPPER(TRIM(PRODUCT_NAME)) LIKE '%�ش��պʹ���ٷ%' THEN 'DAYWEAR'
						WHEN UPPER(TRIM(PRODUCT_NAME)) LIKE '%�ش��բ���ǡ�д��˹��%' THEN 'DAYWEAR'
						WHEN UPPER(TRIM(PRODUCT_NAME)) LIKE '%�ش��բ���ǡ�д����ҧ%' THEN 'DAYWEAR'
						WHEN UPPER(TRIM(PRODUCT_NAME)) LIKE '%�ش�����ٷ%' THEN 'DAYWEAR'
						WHEN UPPER(TRIM(PRODUCT_NAME)) LIKE '%�ش����ç�ҧࡧ㹤Ҵ��%' THEN 'DAYWEAR'
						WHEN UPPER(TRIM(PRODUCT_NAME)) LIKE '%�ش����ç%' THEN 'DAYWEAR'
						WHEN UPPER(TRIM(PRODUCT_NAME)) LIKE '%�شᢹ���%' THEN 'NIGHTWEAR'
						WHEN UPPER(TRIM(PRODUCT_NAME)) LIKE '%�شᢹ�����%' THEN 'NIGHTWEAR'
						WHEN UPPER(TRIM(PRODUCT_NAME)) LIKE '%�ѷ�٪��%' THEN 'FOOTWEAR'
						WHEN UPPER(TRIM(PRODUCT_NAME)) LIKE '%�ѷ������е���%' THEN 'FOOTWEAR'
						WHEN UPPER(TRIM(PRODUCT_NAME)) LIKE '%�ѷ���觾��%' THEN 'FOOTWEAR'
						WHEN UPPER(TRIM(PRODUCT_NAME)) LIKE '%�ѷ��%' THEN 'FOOTWEAR'
						WHEN UPPER(TRIM(PRODUCT_NAME)) LIKE '%�Ѫ�٩�ش��%' THEN 'FOOTWEAR'
						WHEN UPPER(TRIM(PRODUCT_NAME)) LIKE '%�Ѫ��Ὺ��%' THEN 'FOOTWEAR'
						WHEN UPPER(TRIM(PRODUCT_NAME)) LIKE '%�ҧࡧ�١����տ��%' THEN 'DAYWEAR'
						WHEN UPPER(TRIM(PRODUCT_NAME)) LIKE '%�ҧࡧ�١����ա��%' THEN 'DAYWEAR'
						WHEN UPPER(TRIM(PRODUCT_NAME)) LIKE '%�ҧࡧ�١���������ͧ%' THEN 'DAYWEAR'
						WHEN UPPER(TRIM(PRODUCT_NAME)) LIKE '%�ҧࡧ�١���%' THEN 'DAYWEAR'
						WHEN UPPER(TRIM(PRODUCT_NAME)) LIKE '%�ҧࡧ�������%' THEN 'UNDERWEAR'
						WHEN UPPER(TRIM(PRODUCT_NAME)) LIKE '%�ҧࡧ�����SPANDEX%' THEN 'UNDERWEAR'
						WHEN UPPER(TRIM(PRODUCT_NAME)) LIKE '%�ҧࡧ�����BALLET%' THEN 'UNDERWEAR'
						WHEN UPPER(TRIM(PRODUCT_NAME)) LIKE '%�ҧࡧ����ǫ�%' THEN 'DAYWEAR'
						WHEN UPPER(TRIM(PRODUCT_NAME)) LIKE '%�ҧࡧ�����%' THEN 'DAYWEAR'
						WHEN UPPER(TRIM(PRODUCT_NAME)) LIKE '%�ҧࡧ��3��ǹ%' THEN 'DAYWEAR'
						WHEN UPPER(TRIM(PRODUCT_NAME)) LIKE '%�ҧࡧ��%' THEN 'DAYWEAR'
						WHEN UPPER(TRIM(PRODUCT_NAME)) LIKE '%�ҧࡧ����¹��%' THEN 'UNDERWEAR'
						WHEN UPPER(TRIM(PRODUCT_NAME)) LIKE '%�ҧࡧ㹴.�.%' THEN 'UNDERWEAR'
						WHEN UPPER(TRIM(PRODUCT_NAME)) LIKE '%�ҧࡧ�SPANDEX%' THEN 'UNDERWEAR'
						WHEN UPPER(TRIM(PRODUCT_NAME)) LIKE '%�ҧࡧ�PACK2%' THEN 'UNDERWEAR'
						WHEN UPPER(TRIM(PRODUCT_NAME)) LIKE '%�ҧࡧ�GIRL%' THEN 'UNDERWEAR'
						WHEN UPPER(TRIM(PRODUCT_NAME)) LIKE '%�ҧࡧ�BALLET%' THEN 'UNDERWEAR'
						WHEN UPPER(TRIM(PRODUCT_NAME)) LIKE '%�ҧࡧ�%' THEN 'UNDERWEAR'
						WHEN UPPER(TRIM(PRODUCT_NAME)) LIKE '%�ҧࡧ�Ť���%' THEN 'DAYWEAR'
						WHEN UPPER(TRIM(PRODUCT_NAME)) LIKE '%�ҧࡧBOXER%' THEN 'UNDERWEAR'
						WHEN UPPER(TRIM(PRODUCT_NAME)) LIKE '%���%' THEN 'UNDERWEAR'
						WHEN UPPER(TRIM(PRODUCT_NAME)) LIKE '%�����%' THEN 'FOOTWEAR'
						WHEN UPPER(TRIM(PRODUCT_NAME)) LIKE '%������״SUPER%' THEN 'DAYWEAR'
						WHEN UPPER(TRIM(PRODUCT_NAME)) LIKE '%������״SNAP%' THEN 'DAYWEAR'
						WHEN UPPER(TRIM(PRODUCT_NAME)) LIKE '%����ͼ��˹��ᢹ���%' THEN 'DAYWEAR'
						WHEN UPPER(TRIM(PRODUCT_NAME)) LIKE '%����ͼ����%' THEN 'DAYWEAR'
						WHEN UPPER(TRIM(PRODUCT_NAME)) LIKE '%����ͻ���ᢹ���%' THEN 'DAYWEAR'
						WHEN UPPER(TRIM(PRODUCT_NAME)) LIKE '%����ͻ���ᢹ���%' THEN 'DAYWEAR'
						WHEN UPPER(TRIM(PRODUCT_NAME)) LIKE '%����͡�������¹��%' THEN 'DAYWEAR'
						WHEN UPPER(TRIM(PRODUCT_NAME)) LIKE '% SHINE %' THEN 'DAYWEAR'
						WHEN UPPER(TRIM(PRODUCT_NAME)) LIKE '% SCOTH %' THEN 'NIGHTWEAR'
						WHEN UPPER(TRIM(PRODUCT_NAME)) LIKE '% SANDOL %' THEN 'FOOTWEAR'
						WHEN UPPER(TRIM(PRODUCT_NAME)) LIKE '% SAILOR %' THEN 'DAYWEAR'
						WHEN UPPER(TRIM(PRODUCT_NAME)) LIKE '% PRGR %' THEN 'DAYWEAR'
						WHEN UPPER(TRIM(PRODUCT_NAME)) LIKE '% PRBL %' THEN 'DAYWEAR'
						WHEN UPPER(TRIM(PRODUCT_NAME)) LIKE '% NV %' THEN 'FOOTWEAR'
						WHEN UPPER(TRIM(PRODUCT_NAME)) LIKE '% NB %' THEN 'FOOTWEAR'
						WHEN UPPER(TRIM(PRODUCT_NAME)) LIKE '% GIRL %' THEN 'DAYWEAR'
						WHEN UPPER(TRIM(PRODUCT_NAME)) LIKE '% FASHION2 %' THEN 'FOOTWEAR'
						WHEN UPPER(TRIM(PRODUCT_NAME)) LIKE '% FASHION %' THEN 'FOOTWEAR'
						WHEN UPPER(TRIM(PRODUCT_NAME)) LIKE '% DRESS %' THEN 'NIGHTWEAR'
						WHEN UPPER(TRIM(PRODUCT_NAME)) LIKE '% DPI %' THEN 'DAYWEAR'
						WHEN UPPER(TRIM(PRODUCT_NAME)) LIKE '% CUT %' THEN 'ACCESSORIES'
						WHEN UPPER(TRIM(PRODUCT_NAME)) LIKE '% CRAY %' THEN 'DAYWEAR'
						WHEN UPPER(TRIM(PRODUCT_NAME)) LIKE '% CHPI %' THEN 'DAYWEAR'
						WHEN UPPER(TRIM(PRODUCT_NAME)) LIKE '% BOXER %' THEN 'UNDERWEAR'
						WHEN UPPER(TRIM(PRODUCT_NAME)) LIKE '% BAHB %' THEN 'DAYWEAR'
						WHEN UPPER(TRIM(PRODUCT_NAME)) LIKE '% BABY %' THEN 'DAYWEAR'
						WHEN UPPER(TRIM(PRODUCT_NAME)) LIKE '% ANGLE %' THEN 'DAYWEAR'
						WHEN UPPER(TRIM(PRODUCT_NAME)) LIKE '% 3WH %' THEN 'UNDERWEAR'
						ELSE NULL END
WHERE CATEGORY_FINAL = 'FASHION'
;
UPDATE VPM_DATA.PM_SKUTAGGING_KIDS_PRODUCT_MASTER
SET TYPE_TEXT = CASE WHEN UPPER(TRIM(PRODUCT_NAME)) LIKE '%�ѡ�����ŵ���%' THEN 'DIAPERS'
						WHEN UPPER(TRIM(PRODUCT_NAME)) LIKE '%���������д��%' THEN 'DIAPERS'
						WHEN UPPER(TRIM(PRODUCT_NAME)) LIKE '%�������S%' THEN 'DIAPERS'
						WHEN UPPER(TRIM(PRODUCT_NAME)) LIKE '%�������R%' THEN 'DIAPERS'
						WHEN UPPER(TRIM(PRODUCT_NAME)) LIKE '%�������POKO%' THEN 'DIAPERS'
						WHEN UPPER(TRIM(PRODUCT_NAME)) LIKE '%�������BBL%' THEN 'DIAPERS'
						WHEN UPPER(TRIM(PRODUCT_NAME)) LIKE '%�ء������͹%' THEN 'FEEDING BOTTLE AND NIPPLE'
						WHEN UPPER(TRIM(PRODUCT_NAME)) LIKE '%������%' THEN 'BEDDING'
						WHEN UPPER(TRIM(PRODUCT_NAME)) LIKE '%�Ǵ��%' THEN 'FEEDING BOTTLE AND NIPPLE'
						WHEN UPPER(TRIM(PRODUCT_NAME)) LIKE '%�Ǵ%' THEN 'FEEDING BOTTLE AND NIPPLE'
						WHEN UPPER(TRIM(PRODUCT_NAME)) LIKE '%�ç�տѹ%' THEN 'BATHING'
						WHEN UPPER(TRIM(PRODUCT_NAME)) LIKE '%ZOO%' THEN 'FEEDING BOTTLE AND NIPPLE'
						WHEN UPPER(TRIM(PRODUCT_NAME)) LIKE '%WIPES%' THEN 'WIPES'
						WHEN UPPER(TRIM(PRODUCT_NAME)) LIKE '%WIPES%' THEN 'WIPES'
						WHEN UPPER(TRIM(PRODUCT_NAME)) LIKE '%TODDY%' THEN 'FEEDING BOTTLE AND NIPPLE'
						WHEN UPPER(TRIM(PRODUCT_NAME)) LIKE '%TAPE%' THEN 'DIAPERS'
						WHEN UPPER(TRIM(PRODUCT_NAME)) LIKE '%TAPE%' THEN 'DIAPERS'
						WHEN UPPER(TRIM(PRODUCT_NAME)) LIKE '%STRAW%' THEN 'FEEDING BOTTLE AND NIPPLE'
						WHEN UPPER(TRIM(PRODUCT_NAME)) LIKE '%SPONGE%' THEN 'BATHING'
						WHEN UPPER(TRIM(PRODUCT_NAME)) LIKE '%SOOTHER%' THEN 'FEEDING BOTTLE AND NIPPLE'
						WHEN UPPER(TRIM(PRODUCT_NAME)) LIKE '%SINGLE%' THEN 'FEEDING BOTTLE AND NIPPLE'
						WHEN UPPER(TRIM(PRODUCT_NAME)) LIKE '%POKO%' THEN 'DIAPERS'
						WHEN UPPER(TRIM(PRODUCT_NAME)) LIKE '%PLAYPANTNNP%' THEN 'DIAPERS'
						WHEN UPPER(TRIM(PRODUCT_NAME)) LIKE '%PANTS%' THEN 'DIAPERS'
						WHEN UPPER(TRIM(PRODUCT_NAME)) LIKE '%PANTS%' THEN 'DIAPERS'
						WHEN UPPER(TRIM(PRODUCT_NAME)) LIKE '%NIPPLE%' THEN 'FEEDING BOTTLE AND NIPPLE'
						WHEN UPPER(TRIM(PRODUCT_NAME)) LIKE '%MOSQUITO%' THEN 'PERSONAL CARE'
						WHEN UPPER(TRIM(PRODUCT_NAME)) LIKE '%MERRIES%' THEN 'DIAPERS'
						WHEN UPPER(TRIM(PRODUCT_NAME)) LIKE '%MAMYPOKO%' THEN 'DIAPERS'
						WHEN UPPER(TRIM(PRODUCT_NAME)) LIKE '%MAMY%' THEN 'DIAPERS'
						WHEN UPPER(TRIM(PRODUCT_NAME)) LIKE '%HUGGIES%' THEN 'DIAPERS'
						WHEN UPPER(TRIM(PRODUCT_NAME)) LIKE '%;O.N%' THEN 'DIAPERS'
						WHEN UPPER(TRIM(PRODUCT_NAME)) LIKE '%DIAPER%' THEN 'DIAPERS'
						WHEN UPPER(TRIM(PRODUCT_NAME)) LIKE '%DIAPER%' THEN 'DIAPERS'
						WHEN UPPER(TRIM(PRODUCT_NAME)) LIKE '%COTTON%' THEN 'PERSONAL CARE'
						WHEN UPPER(TRIM(PRODUCT_NAME)) LIKE '%8OZ%' THEN 'FEEDING BOTTLE AND NIPPLE'
						WHEN UPPER(TRIM(PRODUCT_NAME)) LIKE '% ULTRA %' THEN 'DIAPERS'
						WHEN UPPER(TRIM(PRODUCT_NAME)) LIKE '% ULT %' THEN 'DIAPERS'
						WHEN UPPER(TRIM(PRODUCT_NAME)) LIKE '% SPJ %' THEN 'DIAPERS'
						WHEN UPPER(TRIM(PRODUCT_NAME)) LIKE '% RPP %' THEN 'FEEDING BOTTLE AND NIPPLE'
						WHEN UPPER(TRIM(PRODUCT_NAME)) LIKE '% PP %' THEN 'FEEDING BOTTLE AND NIPPLE'
						WHEN UPPER(TRIM(PRODUCT_NAME)) LIKE '% PLUS %' THEN 'FEEDING BOTTLE AND NIPPLE'
						WHEN UPPER(TRIM(PRODUCT_NAME)) LIKE '% PLAY %' THEN 'DIAPERS'
						WHEN UPPER(TRIM(PRODUCT_NAME)) LIKE '% OZ%' THEN 'FEEDING BOTTLE AND NIPPLE'
						WHEN UPPER(TRIM(PRODUCT_NAME)) LIKE '% ECO %' THEN 'DIAPERS'
						WHEN UPPER(TRIM(PRODUCT_NAME)) LIKE '% DAY %' THEN 'DIAPERS'
						WHEN UPPER(TRIM(PRODUCT_NAME)) LIKE '% BPA %' THEN 'FEEDING BOTTLE AND NIPPLE'
						WHEN UPPER(TRIM(PRODUCT_NAME)) LIKE '% ANTI %' THEN 'PERSONAL CARE'
						ELSE NULL END
WHERE CATEGORY_FINAL = 'NECESSITIES'
;
UPDATE VPM_DATA.PM_SKUTAGGING_KIDS_PRODUCT_MASTER
SET TYPE_TEXT = CASE WHEN UPPER(TRIM(PRODUCT_NAME)) LIKE '%�껻�Ҿԡ%' THEN 'NURISHMENT'
						WHEN UPPER(TRIM(PRODUCT_NAME)) LIKE '%UHT%' THEN 'UHT MILK'
						WHEN UPPER(TRIM(PRODUCT_NAME)) LIKE '%S26%' THEN 'MILK POWDER'
						WHEN UPPER(TRIM(PRODUCT_NAME)) LIKE '%PREBIO%' THEN 'MILK POWDER'
						WHEN UPPER(TRIM(PRODUCT_NAME)) LIKE '%POWDER%' THEN 'MILK POWDER'
						WHEN UPPER(TRIM(PRODUCT_NAME)) LIKE '%PEACHY%' THEN 'NURISHMENT'
						WHEN UPPER(TRIM(PRODUCT_NAME)) LIKE '%INFANT%' THEN 'MILK POWDER'
						WHEN UPPER(TRIM(PRODUCT_NAME)) LIKE '%FOLLOW%' THEN 'MILK POWDER'
						WHEN UPPER(TRIM(PRODUCT_NAME)) LIKE '%ENFALAC%' THEN 'MILK POWDER'
						WHEN UPPER(TRIM(PRODUCT_NAME)) LIKE '%APPLE%' THEN 'NURISHMENT'
						WHEN UPPER(TRIM(PRODUCT_NAME)) LIKE '% SUPER %' THEN 'MILK POWDER'
						WHEN UPPER(TRIM(PRODUCT_NAME)) LIKE '% ;LD %' THEN 'MILK POWDER'
						ELSE NULL END
WHERE CATEGORY_FINAL = 'FOOD'
;
UPDATE VPM_DATA.PM_SKUTAGGING_KIDS_PRODUCT_MASTER
SET TYPE_TEXT = CASE WHEN UPPER(TRIM(PRODUCT_NAME)) LIKE '%���TRA%' THEN 'ACTION FIGURE & COLLECTIBLE'
						WHEN UPPER(TRIM(PRODUCT_NAME)) LIKE '%���SW%' THEN 'ACTION FIGURE & COLLECTIBLE'
						WHEN UPPER(TRIM(PRODUCT_NAME)) LIKE '%���%' THEN 'ACTION FIGURE & COLLECTIBLE'
						WHEN UPPER(TRIM(PRODUCT_NAME)) LIKE '%��ǧ�ҧ%' THEN 'SWIMMING'
						WHEN UPPER(TRIM(PRODUCT_NAME)) LIKE '%öHW%' THEN 'WHEEL GOODS'
						WHEN UPPER(TRIM(PRODUCT_NAME)) LIKE '%ö%' THEN 'WHEEL GOODS'
						WHEN UPPER(TRIM(PRODUCT_NAME)) LIKE '%�׹�մ���%' THEN 'OUTDOOR'
						WHEN UPPER(TRIM(PRODUCT_NAME)) LIKE '%�׹����NER%' THEN 'GUN & WEAPON'
						WHEN UPPER(TRIM(PRODUCT_NAME)) LIKE '%�׹%' THEN 'GUN & WEAPON'
						WHEN UPPER(TRIM(PRODUCT_NAME)) LIKE '%��꡵�SPARKLE%' THEN 'DOLL & ACCESSORY'
						WHEN UPPER(TRIM(PRODUCT_NAME)) LIKE '%��꡵�MLP%' THEN 'DOLL & ACCESSORY'
						WHEN UPPER(TRIM(PRODUCT_NAME)) LIKE '%��ǵ��%' THEN 'BLOCK'
						WHEN UPPER(TRIM(PRODUCT_NAME)) LIKE '%��鹶ا%' THEN 'DOLL & ACCESSORY'
						WHEN UPPER(TRIM(PRODUCT_NAME)) LIKE '%��е��������%' THEN 'TOY'
						WHEN UPPER(TRIM(PRODUCT_NAME)) LIKE '%����%' THEN 'ACTION FIGURE & COLLECTIBLE'
						WHEN UPPER(TRIM(PRODUCT_NAME)) LIKE '%�����PD%' THEN 'ARTS & CRAFT'
						WHEN UPPER(TRIM(PRODUCT_NAME)) LIKE '%�����%' THEN 'ARTS & CRAFT'
						WHEN UPPER(TRIM(PRODUCT_NAME)) LIKE '%ZOMBIE%' THEN 'GUN & WEAPON'
						WHEN UPPER(TRIM(PRODUCT_NAME)) LIKE '%ULTRAMAN%' THEN 'ACTION FIGURE & COLLECTIBLE'
						WHEN UPPER(TRIM(PRODUCT_NAME)) LIKE '%ULTIMATE%' THEN 'BLOCK'
						WHEN UPPER(TRIM(PRODUCT_NAME)) LIKE '%TROLL%' THEN 'DOLL & ACCESSORY'
						WHEN UPPER(TRIM(PRODUCT_NAME)) LIKE '%TRANSFORMING%' THEN 'ACTION FIGURE & COLLECTIBLE'
						WHEN UPPER(TRIM(PRODUCT_NAME)) LIKE '%TEETHER%' THEN 'BABY TOY'
						WHEN UPPER(TRIM(PRODUCT_NAME)) LIKE '%STRIKE %' THEN 'GUN & WEAPON'
						WHEN UPPER(TRIM(PRODUCT_NAME)) LIKE '%SNOOPY%' THEN 'OTHER'
						WHEN UPPER(TRIM(PRODUCT_NAME)) LIKE '%ROBOT%' THEN 'ACTION FIGURE & COLLECTIBLE'
						WHEN UPPER(TRIM(PRODUCT_NAME)) LIKE '%RILAKKUMA%' THEN 'TOY'
						WHEN UPPER(TRIM(PRODUCT_NAME)) LIKE '%POPPIT%' THEN 'ARTS & CRAFT'
						WHEN UPPER(TRIM(PRODUCT_NAME)) LIKE '%PONY%' THEN 'DOLL & ACCESSORY'
						WHEN UPPER(TRIM(PRODUCT_NAME)) LIKE '%PLAYDOH%' THEN 'ARTS & CRAFT'
						WHEN UPPER(TRIM(PRODUCT_NAME)) LIKE '%PLACES%' THEN 'ACTION FIGURE & COLLECTIBLE'
						WHEN UPPER(TRIM(PRODUCT_NAME)) LIKE '%PEPPA%' THEN 'DOLL & ACCESSORY'
						WHEN UPPER(TRIM(PRODUCT_NAME)) LIKE '%NSTRIKE %' THEN 'GUN & WEAPON'
						WHEN UPPER(TRIM(PRODUCT_NAME)) LIKE '%MODULUS%' THEN 'GUN & WEAPON'
						WHEN UPPER(TRIM(PRODUCT_NAME)) LIKE '%MEDAL%' THEN 'ACTION FIGURE & COLLECTIBLE'
						WHEN UPPER(TRIM(PRODUCT_NAME)) LIKE '%MAJORETTE%' THEN 'WHEEL GOODS'
						WHEN UPPER(TRIM(PRODUCT_NAME)) LIKE '%LEGENDS%' THEN 'ACTION FIGURE & COLLECTIBLE'
						WHEN UPPER(TRIM(PRODUCT_NAME)) LIKE '%GUNDAM%' THEN 'ACTION FIGURE & COLLECTIBLE'
						WHEN UPPER(TRIM(PRODUCT_NAME)) LIKE '%GENERATIONS%' THEN 'ACTION FIGURE & COLLECTIBLE'
						WHEN UPPER(TRIM(PRODUCT_NAME)) LIKE '%FASHION%' THEN 'DOLL & ACCESSORY'
						WHEN UPPER(TRIM(PRODUCT_NAME)) LIKE '%EX-STANDARD%' THEN 'ACTION FIGURE & COLLECTIBLE'
						WHEN UPPER(TRIM(PRODUCT_NAME)) LIKE '%EXPLORE%' THEN 'DOLL & ACCESSORY'
						WHEN UPPER(TRIM(PRODUCT_NAME)) LIKE '%EQUESTRIA%' THEN 'DOLL & ACCESSORY'
						WHEN UPPER(TRIM(PRODUCT_NAME)) LIKE '%DPR%' THEN 'DOLL & ACCESSORY'
						WHEN UPPER(TRIM(PRODUCT_NAME)) LIKE '%DIECAST%' THEN 'ACTION FIGURE & COLLECTIBLE'
						WHEN UPPER(TRIM(PRODUCT_NAME)) LIKE '%CONFIDENTIAL%' THEN 'BLOCK'
						WHEN UPPER(TRIM(PRODUCT_NAME)) LIKE '%CLASS%' THEN 'ACTION FIGURE & COLLECTIBLE'
						WHEN UPPER(TRIM(PRODUCT_NAME)) LIKE '%BACKPACK%' THEN 'ACTION FIGURE & COLLECTIBLE'
						WHEN UPPER(TRIM(PRODUCT_NAME)) LIKE '% WINTER %' THEN 'DOLL & ACCESSORY'
						WHEN UPPER(TRIM(PRODUCT_NAME)) LIKE '% V29 %' THEN 'BLOCK'
						WHEN UPPER(TRIM(PRODUCT_NAME)) LIKE '% TY %' THEN 'DOLL & ACCESSORY'
						WHEN UPPER(TRIM(PRODUCT_NAME)) LIKE '% TRA %' THEN 'ACTION FIGURE & COLLECTIBLE'
						WHEN UPPER(TRIM(PRODUCT_NAME)) LIKE '% TOWN %' THEN 'DOLL & ACCESSORY'
						WHEN UPPER(TRIM(PRODUCT_NAME)) LIKE '% TITAN %' THEN 'ACTION FIGURE & COLLECTIBLE'
						WHEN UPPER(TRIM(PRODUCT_NAME)) LIKE '% T&F %' THEN 'WHEEL GOODS'
						WHEN UPPER(TRIM(PRODUCT_NAME)) LIKE '% RID %' THEN 'ACTION FIGURE & COLLECTIBLE'
						WHEN UPPER(TRIM(PRODUCT_NAME)) LIKE '% REG %' THEN 'DOLL & ACCESSORY'
						WHEN UPPER(TRIM(PRODUCT_NAME)) LIKE '% NER %' THEN 'GUN & WEAPON'
						WHEN UPPER(TRIM(PRODUCT_NAME)) LIKE '% MV5 %' THEN 'ACTION FIGURE & COLLECTIBLE'
						WHEN UPPER(TRIM(PRODUCT_NAME)) LIKE '% LBM %' THEN 'BLOCK'
						WHEN UPPER(TRIM(PRODUCT_NAME)) LIKE '% HAPPY %' THEN 'ACTION FIGURE & COLLECTIBLE'
						WHEN UPPER(TRIM(PRODUCT_NAME)) LIKE '% GIFT %' THEN 'DOLL & ACCESSORY'
						WHEN UPPER(TRIM(PRODUCT_NAME)) LIKE '% FRZ %' THEN 'DOLL & ACCESSORY'
						WHEN UPPER(TRIM(PRODUCT_NAME)) LIKE '% FIM %' THEN 'DOLL & ACCESSORY'
						WHEN UPPER(TRIM(PRODUCT_NAME)) LIKE '% ELITE %' THEN 'GUN & WEAPON'
						WHEN UPPER(TRIM(PRODUCT_NAME)) LIKE '% EASY %' THEN 'DOLL & ACCESSORY'
						WHEN UPPER(TRIM(PRODUCT_NAME)) LIKE '% CONE %' THEN 'DOLL & ACCESSORY'
						WHEN UPPER(TRIM(PRODUCT_NAME)) LIKE '% BOX %' THEN 'BLOCK'
						WHEN UPPER(TRIM(PRODUCT_NAME)) LIKE '% BEAR  %' THEN 'TOY'
						ELSE NULL END
WHERE CATEGORY_FINAL = 'TOYS'
;

UPDATE VPM_DATA.PM_SKUTAGGING_KIDS_PRODUCT_MASTER
SET TYPE_FINAL = UPPER(TRIM(TYPE_TEXT))
WHERE TYPE_FINAL IS NULL;

UPDATE VPM_DATA.PM_SKUTAGGING_KIDS_PRODUCT_MASTER
SET TYPE_FINAL = UPPER(TYPE_FINAL)
;


-- CLEAN CLEANED BRANDNAME
UPDATE VPM_DATA.PM_SKUTAGGING_KIDS_PRODUCT_MASTER
SET CLEANED_BRANDNAME = CASE WHEN UPPER(TRIM(CLEANED_BRANDNAME)) = 'CADEAUX' THEN 'CADEAU'
							 WHEN UPPER(TRIM(CLEANED_BRANDNAME)) = 'CAMERA THAI' THEN 'CAMERA'
							 WHEN UPPER(TRIM(CLEANED_BRANDNAME)) = 'MAMY POKO' THEN 'MAMYPOKO'
							 WHEN UPPER(TRIM(CLEANED_BRANDNAME)) = 'PUREEN CORNER' THEN 'PUREEN'
							 WHEN UPPER(TRIM(CLEANED_BRANDNAME)) = 'PICNIC' THEN 'PICNIC BABY'
							 WHEN UPPER(TRIM(CLEANED_BRANDNAME)) = 'HUGGIES ULTRA' THEN 'HUGGIES'
							 WHEN UPPER(TRIM(CLEANED_BRANDNAME)) = 'ARIBEBE' THEN 'AILEBEBE'
							 WHEN UPPER(TRIM(CLEANED_BRANDNAME)) = 'OTHER' THEN
								CASE WHEN UPPER(PRODUCT_NAME) LIKE '%BABY LOVE%' THEN 'BABY LOVE'
									 WHEN UPPER(PRODUCT_NAME) LIKE '%MAMY%POKO%' THEN 'MAMYPOKO'
									 WHEN UPPER(PRODUCT_NAME) LIKE '%D-NEE%' THEN 'DNEE'
									ELSE UPPER(TRIM(CLEANED_BRANDNAME)) END
							ELSE UPPER(TRIM(CLEANED_BRANDNAME)) END
;

/************************************************** CATEGORY FINAL **************************************************/

--UPDATE CATEGORY_FINAL BASED ON TYPE_FINAL
UPDATE VPM_DATA.PM_SKUTAGGING_KIDS_PRODUCT_MASTER
SET CATEGORY_FINAL = CASE WHEN UPPER(TRIM(TYPE_FINAL)) IN ('ACTIVITY BOOK','BABY NAME BOOK','BOOK','BOOKS','CHILDCARE BOOK','EDUCATION MATERIAL','MEDIA&ENTERTAINMENT','PREGNANCY BOOK','STATIONERY','TALE BOOK') THEN 'BOOKS'
						  WHEN UPPER(TRIM(TYPE_FINAL)) IN ('DAYWEAR','DRESS UP & PRETEND PLAY','FOOTWEAR','NIGHTWEAR','SWIMWEAR','UNDERWEAR') THEN 'FASHION'
						  WHEN UPPER(TRIM(TYPE_FINAL)) IN ('MILK POWDER','NURISHMENT') THEN 'FOOD'
						  WHEN UPPER(TRIM(TYPE_FINAL)) IN ('GIFT SET','WEANING','ACCESSORIES','BABY CARRIER','BABY CRADLE','BABY WALKER','BATHING','BEDDING','BOTTLE WARMER','BOUNCER/SWING','BREAST PADS','CAR SEAT','BABY BOUNCER',
											  'DIAPERS','FABRIC','FEEDING ACCESSORIES','FEEDING BOTTLE AND NIPPLE','HOT WATER BAG','MATERNITY','MILK PUMP MACHINE','MILK STORAGE','NIPPLE SHIELD',
											  'OTHER','OUTDOOR','PERSONAL CARE','PLAYPEN','RUBBER PAD','SAFETY','STROLLER','SUCKER','THERMOMETER','TOOTHBRUSH','WASHING') THEN 'NECESSITIES'
						  WHEN UPPER(TRIM(TYPE_FINAL)) IN ('TOY') THEN 'TOYS'
					 ELSE CATEGORY_FINAL END;

DELETE FROM VPM_DATA.PM_SKUTAGGING_KIDS_PRODUCT_MASTER
WHERE CATEGORY_FINAL = 'FOOD' AND TYPE_FINAL = 'UHT MILK';

UPDATE VPM_DATA.PM_SKUTAGGING_KIDS_PRODUCT_MASTER
SET CATEGORY_FINAL = 'KID',
	TYPE_FINAL = 'KID'
WHERE CATEGORY_FINAL IS NULL OR TYPE_FINAL IS NULL;


/************************************************** SIZE, MEASUREMENT **************************************************/

UPDATE VPM_DATA.PM_SKUTAGGING_KIDS_PRODUCT_MASTER
SET SIZE = CASE WHEN VPM_DATA.PATINDEX('%[0-9]'||'G%',UPPER(TRIM(PRODUCT_NAME))) != '0' THEN 
			(CASE	WHEN VPM_DATA.ISNUMERIC(LEFT(RIGHT(RTRIM(LEFT(UPPER(TRIM(PRODUCT_NAME)),VPM_DATA.PATINDEX('%[0-9]'||'G%',UPPER(TRIM(PRODUCT_NAME))))),4),1)) = 1 THEN RIGHT(LEFT(UPPER(TRIM(PRODUCT_NAME)),VPM_DATA.PATINDEX('%[0-9]'||'G%',UPPER(TRIM(PRODUCT_NAME)))),4)
					WHEN VPM_DATA.ISNUMERIC(LEFT(RIGHT(RTRIM(LEFT(UPPER(TRIM(PRODUCT_NAME)),VPM_DATA.PATINDEX('%[0-9]'||'G%',UPPER(TRIM(PRODUCT_NAME))))),3),1)) = 1 THEN RIGHT(LEFT(UPPER(TRIM(PRODUCT_NAME)),VPM_DATA.PATINDEX('%[0-9]'||'G%',UPPER(TRIM(PRODUCT_NAME)))),3)
					WHEN VPM_DATA.ISNUMERIC(LEFT(RIGHT(RTRIM(LEFT(UPPER(TRIM(PRODUCT_NAME)),VPM_DATA.PATINDEX('%[0-9]'||'G%',UPPER(TRIM(PRODUCT_NAME))))),2),1)) = 1 THEN RIGHT(LEFT(UPPER(TRIM(PRODUCT_NAME)),VPM_DATA.PATINDEX('%[0-9]'||'G%',UPPER(TRIM(PRODUCT_NAME)))),2)
					WHEN VPM_DATA.ISNUMERIC(LEFT(RIGHT(RTRIM(LEFT(UPPER(TRIM(PRODUCT_NAME)),VPM_DATA.PATINDEX('%[0-9]'||'G%',UPPER(TRIM(PRODUCT_NAME))))),1),1)) = 1 THEN RIGHT(LEFT(UPPER(TRIM(PRODUCT_NAME)),VPM_DATA.PATINDEX('%[0-9]'||'G%',UPPER(TRIM(PRODUCT_NAME)))),1)
			END)
		WHEN VPM_DATA.PATINDEX('%[0-9] '||'G%',UPPER(TRIM(PRODUCT_NAME))) != '0' THEN 
			(CASE	WHEN VPM_DATA.ISNUMERIC(LEFT(RIGHT(RTRIM(LEFT(UPPER(TRIM(PRODUCT_NAME)),VPM_DATA.PATINDEX('%[0-9] '||'G%',UPPER(TRIM(PRODUCT_NAME))))),3),1)) = 1 THEN RIGHT(LEFT(UPPER(TRIM(PRODUCT_NAME)),VPM_DATA.PATINDEX('%[0-9] '||'G%',UPPER(TRIM(PRODUCT_NAME)))),3)
					WHEN VPM_DATA.ISNUMERIC(LEFT(RIGHT(RTRIM(LEFT(UPPER(TRIM(PRODUCT_NAME)),VPM_DATA.PATINDEX('%[0-9] '||'G%',UPPER(TRIM(PRODUCT_NAME))))),2),1)) = 1 THEN RIGHT(LEFT(UPPER(TRIM(PRODUCT_NAME)),VPM_DATA.PATINDEX('%[0-9] '||'G%',UPPER(TRIM(PRODUCT_NAME)))),2)
					WHEN VPM_DATA.ISNUMERIC(LEFT(RIGHT(RTRIM(LEFT(UPPER(TRIM(PRODUCT_NAME)),VPM_DATA.PATINDEX('%[0-9] '||'G%',UPPER(TRIM(PRODUCT_NAME))))),1),1)) = 1 THEN RIGHT(LEFT(UPPER(TRIM(PRODUCT_NAME)),VPM_DATA.PATINDEX('%[0-9] '||'G%',UPPER(TRIM(PRODUCT_NAME)))),1)
					END)
		WHEN UPPER(TRIM(PRODUCT_NAME)) LIKE '%ML%' AND POSITION('ML' IN UPPER(TRIM(PRODUCT_NAME)))-1 > 1 THEN
			(CASE WHEN VPM_DATA.ISNUMERIC(LEFT(RIGHT(RTRIM(LEFT(UPPER(TRIM(PRODUCT_NAME)),POSITION('ML' IN UPPER(TRIM(PRODUCT_NAME)))-1)),4),1)) = 1
					THEN RIGHT(RTRIM(LEFT(UPPER(TRIM(PRODUCT_NAME)),POSITION('ML' IN UPPER(TRIM(PRODUCT_NAME)))-1)),4) 
				  WHEN VPM_DATA.ISNUMERIC(LEFT(RIGHT(RTRIM(LEFT(UPPER(TRIM(PRODUCT_NAME)),POSITION('ML' IN UPPER(TRIM(PRODUCT_NAME)))-1)),3),1)) = 1
					THEN RIGHT(RTRIM(LEFT(UPPER(TRIM(PRODUCT_NAME)),POSITION('ML' IN UPPER(TRIM(PRODUCT_NAME)))-1)),3) 
				  WHEN VPM_DATA.ISNUMERIC(LEFT(RIGHT(RTRIM(LEFT(UPPER(TRIM(PRODUCT_NAME)),POSITION('ML' IN UPPER(TRIM(PRODUCT_NAME)))-1)),2),1)) = 1
					THEN RIGHT(RTRIM(LEFT(UPPER(TRIM(PRODUCT_NAME)),POSITION('ML' IN UPPER(TRIM(PRODUCT_NAME)))-1)),2)
				  WHEN VPM_DATA.ISNUMERIC(LEFT(RIGHT(RTRIM(LEFT(UPPER(TRIM(PRODUCT_NAME)),POSITION('ML' IN UPPER(TRIM(PRODUCT_NAME)))-1)),1),1)) = 1
					THEN RIGHT(RTRIM(LEFT(UPPER(TRIM(PRODUCT_NAME)),POSITION('ML' IN UPPER(TRIM(PRODUCT_NAME)))-1)),2) END)
		WHEN VPM_DATA.PATINDEX('%[0-9]'||'CC%',UPPER(TRIM(PRODUCT_NAME))) != '0' THEN 
			(CASE	WHEN VPM_DATA.ISNUMERIC(LEFT(RIGHT(RTRIM(LEFT(UPPER(TRIM(PRODUCT_NAME)),VPM_DATA.PATINDEX('%[0-9]'||'CC%',UPPER(TRIM(PRODUCT_NAME))))),4),1)) = 1 THEN RIGHT(LEFT(UPPER(TRIM(PRODUCT_NAME)),VPM_DATA.PATINDEX('%[0-9]'||'CC%',UPPER(TRIM(PRODUCT_NAME)))),4)
					WHEN VPM_DATA.ISNUMERIC(LEFT(RIGHT(RTRIM(LEFT(UPPER(TRIM(PRODUCT_NAME)),VPM_DATA.PATINDEX('%[0-9]'||'CC%',UPPER(TRIM(PRODUCT_NAME))))),3),1)) = 1 THEN RIGHT(LEFT(UPPER(TRIM(PRODUCT_NAME)),VPM_DATA.PATINDEX('%[0-9]'||'CC%',UPPER(TRIM(PRODUCT_NAME)))),3)
					WHEN VPM_DATA.ISNUMERIC(LEFT(RIGHT(RTRIM(LEFT(UPPER(TRIM(PRODUCT_NAME)),VPM_DATA.PATINDEX('%[0-9]'||'CC%',UPPER(TRIM(PRODUCT_NAME))))),2),1)) = 1 THEN RIGHT(LEFT(UPPER(TRIM(PRODUCT_NAME)),VPM_DATA.PATINDEX('%[0-9]'||'CC%',UPPER(TRIM(PRODUCT_NAME)))),2)
					WHEN VPM_DATA.ISNUMERIC(LEFT(RIGHT(RTRIM(LEFT(UPPER(TRIM(PRODUCT_NAME)),VPM_DATA.PATINDEX('%[0-9]'||'CC%',UPPER(TRIM(PRODUCT_NAME))))),1),1)) = 1 THEN RIGHT(LEFT(UPPER(TRIM(PRODUCT_NAME)),VPM_DATA.PATINDEX('%[0-9]'||'CC%',UPPER(TRIM(PRODUCT_NAME)))),1)
					END)
		WHEN VPM_DATA.PATINDEX('%[0-9]'||'OZ%',UPPER(TRIM(PRODUCT_NAME))) != '0' THEN 
			(CASE	WHEN VPM_DATA.ISNUMERIC(LEFT(RIGHT(RTRIM(LEFT(UPPER(TRIM(PRODUCT_NAME)),VPM_DATA.PATINDEX('%[0-9]'||'OZ%',UPPER(TRIM(PRODUCT_NAME))))),4),1)) = 1 THEN RIGHT(LEFT(UPPER(TRIM(PRODUCT_NAME)),VPM_DATA.PATINDEX('%[0-9]'||'OZ%',UPPER(TRIM(PRODUCT_NAME)))),4)
					WHEN VPM_DATA.ISNUMERIC(LEFT(RIGHT(RTRIM(LEFT(UPPER(TRIM(PRODUCT_NAME)),VPM_DATA.PATINDEX('%[0-9]'||'OZ%',UPPER(TRIM(PRODUCT_NAME))))),3),1)) = 1 THEN RIGHT(LEFT(UPPER(TRIM(PRODUCT_NAME)),VPM_DATA.PATINDEX('%[0-9]'||'OZ%',UPPER(TRIM(PRODUCT_NAME)))),3)
					WHEN VPM_DATA.ISNUMERIC(LEFT(RIGHT(RTRIM(LEFT(UPPER(TRIM(PRODUCT_NAME)),VPM_DATA.PATINDEX('%[0-9]'||'OZ%',UPPER(TRIM(PRODUCT_NAME))))),2),1)) = 1 THEN RIGHT(LEFT(UPPER(TRIM(PRODUCT_NAME)),VPM_DATA.PATINDEX('%[0-9]'||'OZ%',UPPER(TRIM(PRODUCT_NAME)))),2)
					WHEN VPM_DATA.ISNUMERIC(LEFT(RIGHT(RTRIM(LEFT(UPPER(TRIM(PRODUCT_NAME)),VPM_DATA.PATINDEX('%[0-9]'||'OZ%',UPPER(TRIM(PRODUCT_NAME))))),1),1)) = 1 THEN RIGHT(LEFT(UPPER(TRIM(PRODUCT_NAME)),VPM_DATA.PATINDEX('%[0-9]'||'OZ%',UPPER(TRIM(PRODUCT_NAME)))),1)
					END)
		WHEN VPM_DATA.PATINDEX('%[0-9] '||'OZ%',UPPER(TRIM(PRODUCT_NAME))) != '0' THEN 
			(CASE	WHEN VPM_DATA.ISNUMERIC(LEFT(RIGHT(RTRIM(LEFT(UPPER(TRIM(PRODUCT_NAME)),VPM_DATA.PATINDEX('%[0-9] '||'OZ%',UPPER(TRIM(PRODUCT_NAME))))),2),1)) = 1 THEN RIGHT(LEFT(UPPER(TRIM(PRODUCT_NAME)),VPM_DATA.PATINDEX('%[0-9] '||'OZ%',UPPER(TRIM(PRODUCT_NAME)))),2)
					WHEN VPM_DATA.ISNUMERIC(LEFT(RIGHT(RTRIM(LEFT(UPPER(TRIM(PRODUCT_NAME)),VPM_DATA.PATINDEX('%[0-9] '||'OZ%',UPPER(TRIM(PRODUCT_NAME))))),1),1)) = 1 THEN RIGHT(LEFT(UPPER(TRIM(PRODUCT_NAME)),VPM_DATA.PATINDEX('%[0-9] '||'OZ%',UPPER(TRIM(PRODUCT_NAME)))),1)
					END)
		WHEN VPM_DATA.PATINDEX('%[0-9]'||'KG%',UPPER(TRIM(PRODUCT_NAME))) != '0' THEN 
			(CASE	WHEN VPM_DATA.ISNUMERIC(LEFT(RIGHT(RTRIM(LEFT(UPPER(TRIM(PRODUCT_NAME)),VPM_DATA.PATINDEX('%[0-9]'||'KG%',UPPER(TRIM(PRODUCT_NAME))))),4),1)) = 1 THEN RIGHT(LEFT(UPPER(TRIM(PRODUCT_NAME)),VPM_DATA.PATINDEX('%[0-9]'||'KG%',UPPER(TRIM(PRODUCT_NAME)))),4)
					WHEN VPM_DATA.ISNUMERIC(LEFT(RIGHT(RTRIM(LEFT(UPPER(TRIM(PRODUCT_NAME)),VPM_DATA.PATINDEX('%[0-9]'||'KG%',UPPER(TRIM(PRODUCT_NAME))))),3),1)) = 1 THEN RIGHT(LEFT(UPPER(TRIM(PRODUCT_NAME)),VPM_DATA.PATINDEX('%[0-9]'||'KG%',UPPER(TRIM(PRODUCT_NAME)))),3)
					WHEN VPM_DATA.ISNUMERIC(LEFT(RIGHT(RTRIM(LEFT(UPPER(TRIM(PRODUCT_NAME)),VPM_DATA.PATINDEX('%[0-9]'||'KG%',UPPER(TRIM(PRODUCT_NAME))))),2),1)) = 1 THEN RIGHT(LEFT(UPPER(TRIM(PRODUCT_NAME)),VPM_DATA.PATINDEX('%[0-9]'||'KG%',UPPER(TRIM(PRODUCT_NAME)))),2)
					WHEN VPM_DATA.ISNUMERIC(LEFT(RIGHT(RTRIM(LEFT(UPPER(TRIM(PRODUCT_NAME)),VPM_DATA.PATINDEX('%[0-9]'||'KG%',UPPER(TRIM(PRODUCT_NAME))))),1),1)) = 1 THEN RIGHT(LEFT(UPPER(TRIM(PRODUCT_NAME)),VPM_DATA.PATINDEX('%[0-9]'||'KG%',UPPER(TRIM(PRODUCT_NAME)))),1)
					END)			
		WHEN VPM_DATA.PATINDEX('%[0-9] '||'KG%',UPPER(TRIM(PRODUCT_NAME))) != '0' THEN 
			(CASE	WHEN VPM_DATA.ISNUMERIC(LEFT(RIGHT(LEFT(UPPER(TRIM(PRODUCT_NAME)),VPM_DATA.PATINDEX('%[0-9] '||'KG%',UPPER(TRIM(PRODUCT_NAME)))),3),1)) = 1 THEN CASE	WHEN LEFT(RIGHT(LEFT(UPPER(TRIM(PRODUCT_NAME)),VPM_DATA.PATINDEX('%[0-9] '||'KG%',UPPER(TRIM(PRODUCT_NAME)))),3),1) = '.' THEN RIGHT(LEFT(UPPER(TRIM(PRODUCT_NAME)),VPM_DATA.PATINDEX('%[0-9] '||'KG%',UPPER(TRIM(PRODUCT_NAME)))),4)
																											ELSE RIGHT(LEFT(UPPER(TRIM(PRODUCT_NAME)),VPM_DATA.PATINDEX('%[0-9] '||'KG%',UPPER(TRIM(PRODUCT_NAME)))),3) END
					WHEN VPM_DATA.ISNUMERIC(LEFT(RIGHT(LEFT(UPPER(TRIM(PRODUCT_NAME)),VPM_DATA.PATINDEX('%[0-9] '||'KG%',UPPER(TRIM(PRODUCT_NAME)))),2),1)) = 1 THEN CASE	WHEN LEFT(RIGHT(LEFT(UPPER(TRIM(PRODUCT_NAME)),VPM_DATA.PATINDEX('%[0-9] '||'KG%',UPPER(TRIM(PRODUCT_NAME)))),2),1) = '.' THEN RIGHT(LEFT(UPPER(TRIM(PRODUCT_NAME)),VPM_DATA.PATINDEX('%[0-9] '||'KG%',UPPER(TRIM(PRODUCT_NAME)))),3)
																											ELSE RIGHT(LEFT(UPPER(TRIM(PRODUCT_NAME)),VPM_DATA.PATINDEX('%[0-9] '||'KG%',UPPER(TRIM(PRODUCT_NAME)))),2) END
					WHEN VPM_DATA.ISNUMERIC(LEFT(RIGHT(LEFT(UPPER(TRIM(PRODUCT_NAME)),VPM_DATA.PATINDEX('%[0-9] '||'KG%',UPPER(TRIM(PRODUCT_NAME)))),1),1)) = 1 THEN CASE	WHEN LEFT(RIGHT(LEFT(UPPER(TRIM(PRODUCT_NAME)),VPM_DATA.PATINDEX('%[0-9] '||'KG%',UPPER(TRIM(PRODUCT_NAME)))),1),1) = '.' THEN RIGHT(LEFT(UPPER(TRIM(PRODUCT_NAME)),VPM_DATA.PATINDEX('%[0-9] '||'KG%',UPPER(TRIM(PRODUCT_NAME)))),2)
																											ELSE RIGHT(LEFT(UPPER(TRIM(PRODUCT_NAME)),VPM_DATA.PATINDEX('%[0-9] '||'KG%',UPPER(TRIM(PRODUCT_NAME)))),1) END
					END)
		WHEN (VPM_DATA.PATINDEX('%[0-9] '||'L%',UPPER(TRIM(PRODUCT_NAME))) != '0' OR VPM_DATA.PATINDEX('%[0-9]'||'L%',UPPER(TRIM(PRODUCT_NAME))) != '0') AND NOT (VPM_DATA.PATINDEX('%[0-9]'||'LB%',UPPER(TRIM(PRODUCT_NAME))) != '0') AND CATEGORY_FINAL IN ('BEVERAGE','DAIRY PRODUCT','INGREDIENT','LIQUOR','READY TO EAT') THEN
			(CASE	WHEN VPM_DATA.ISNUMERIC(LEFT(RIGHT(RTRIM(LEFT(UPPER(TRIM(PRODUCT_NAME)),VPM_DATA.PATINDEX('%[0-9]'||'L%',UPPER(TRIM(PRODUCT_NAME))))),4),1)) = 1 THEN CASE WHEN 
						VPM_DATA.ISNUMERIC(RIGHT(LEFT(UPPER(TRIM(PRODUCT_NAME)),VPM_DATA.PATINDEX('%[0-9]'||'L%',UPPER(TRIM(PRODUCT_NAME)))),4)) = 1 THEN RIGHT(LEFT(UPPER(TRIM(PRODUCT_NAME)),VPM_DATA.PATINDEX('%[0-9]'||'L%',UPPER(TRIM(PRODUCT_NAME)))),4) ELSE RIGHT(RIGHT(LEFT(UPPER(TRIM(PRODUCT_NAME)),VPM_DATA.PATINDEX('%[0-9]'||'L%',UPPER(TRIM(PRODUCT_NAME)))),4),1) END
					WHEN VPM_DATA.ISNUMERIC(LEFT(RIGHT(RTRIM(LEFT(UPPER(TRIM(PRODUCT_NAME)),VPM_DATA.PATINDEX('%[0-9]'||'L%',UPPER(TRIM(PRODUCT_NAME))))),3),1)) = 1 THEN CASE WHEN
							VPM_DATA.ISNUMERIC(RIGHT(LEFT(UPPER(TRIM(PRODUCT_NAME)),VPM_DATA.PATINDEX('%[0-9]'||'L%',UPPER(TRIM(PRODUCT_NAME)))),3)) = 1 THEN RIGHT(LEFT(UPPER(TRIM(PRODUCT_NAME)),VPM_DATA.PATINDEX('%[0-9]'||'L%',UPPER(TRIM(PRODUCT_NAME)))),3) ELSE RIGHT(RIGHT(LEFT(UPPER(TRIM(PRODUCT_NAME)),VPM_DATA.PATINDEX('%[0-9]'||'L%',UPPER(TRIM(PRODUCT_NAME)))),3),1) END
					WHEN VPM_DATA.ISNUMERIC(LEFT(RIGHT(RTRIM(LEFT(UPPER(TRIM(PRODUCT_NAME)),VPM_DATA.PATINDEX('%[0-9]'||'L%',UPPER(TRIM(PRODUCT_NAME))))),2),1)) = 1 THEN CASE WHEN
							VPM_DATA.ISNUMERIC(RIGHT(LEFT(UPPER(TRIM(PRODUCT_NAME)),VPM_DATA.PATINDEX('%[0-9]'||'L%',UPPER(TRIM(PRODUCT_NAME)))),2)) = 1 THEN RIGHT(LEFT(UPPER(TRIM(PRODUCT_NAME)),VPM_DATA.PATINDEX('%[0-9]'||'L%',UPPER(TRIM(PRODUCT_NAME)))),2) ELSE RIGHT(RIGHT(LEFT(UPPER(TRIM(PRODUCT_NAME)),VPM_DATA.PATINDEX('%[0-9]'||'L%',UPPER(TRIM(PRODUCT_NAME)))),2),1) END
					WHEN VPM_DATA.ISNUMERIC(LEFT(RIGHT(RTRIM(LEFT(UPPER(TRIM(PRODUCT_NAME)),VPM_DATA.PATINDEX('%[0-9]'||'L%',UPPER(TRIM(PRODUCT_NAME))))),1),1)) = 1 THEN CASE WHEN
							VPM_DATA.ISNUMERIC(RIGHT(LEFT(UPPER(TRIM(PRODUCT_NAME)),VPM_DATA.PATINDEX('%[0-9]'||'L%',UPPER(TRIM(PRODUCT_NAME)))),1)) = 1 THEN RIGHT(LEFT(UPPER(TRIM(PRODUCT_NAME)),VPM_DATA.PATINDEX('%[0-9]'||'L%',UPPER(TRIM(PRODUCT_NAME)))),1) ELSE RIGHT(RIGHT(LEFT(UPPER(TRIM(PRODUCT_NAME)),VPM_DATA.PATINDEX('%[0-9]'||'L%',UPPER(TRIM(PRODUCT_NAME)))),1),1) END   
					WHEN VPM_DATA.ISNUMERIC(LEFT(RIGHT(RTRIM(LEFT(UPPER(TRIM(PRODUCT_NAME)),VPM_DATA.PATINDEX('%[0-9] '||'L%',UPPER(TRIM(PRODUCT_NAME))))),4),1)) = 1 THEN CASE WHEN
							VPM_DATA.ISNUMERIC(RIGHT(LEFT(UPPER(TRIM(PRODUCT_NAME)),VPM_DATA.PATINDEX('%[0-9] '||'L%',UPPER(TRIM(PRODUCT_NAME)))),4)) = 1 THEN RIGHT(LEFT(UPPER(TRIM(PRODUCT_NAME)),VPM_DATA.PATINDEX('%[0-9] '||'L%',UPPER(TRIM(PRODUCT_NAME)))),4) ELSE RIGHT(RIGHT(LEFT(UPPER(TRIM(PRODUCT_NAME)),VPM_DATA.PATINDEX('%[0-9] '||'L%',UPPER(TRIM(PRODUCT_NAME)))),4),1) END
					WHEN VPM_DATA.ISNUMERIC(LEFT(RIGHT(RTRIM(LEFT(UPPER(TRIM(PRODUCT_NAME)),VPM_DATA.PATINDEX('%[0-9] '||'L%',UPPER(TRIM(PRODUCT_NAME))))),3),1)) = 1 THEN CASE WHEN
							VPM_DATA.ISNUMERIC(RIGHT(LEFT(UPPER(TRIM(PRODUCT_NAME)),VPM_DATA.PATINDEX('%[0-9] '||'L%',UPPER(TRIM(PRODUCT_NAME)))),3)) = 1 THEN RIGHT(LEFT(UPPER(TRIM(PRODUCT_NAME)),VPM_DATA.PATINDEX('%[0-9] '||'L%',UPPER(TRIM(PRODUCT_NAME)))),3) ELSE RIGHT(RIGHT(LEFT(UPPER(TRIM(PRODUCT_NAME)),VPM_DATA.PATINDEX('%[0-9] '||'L%',UPPER(TRIM(PRODUCT_NAME)))),3),1) END
					WHEN VPM_DATA.ISNUMERIC(LEFT(RIGHT(RTRIM(LEFT(UPPER(TRIM(PRODUCT_NAME)),VPM_DATA.PATINDEX('%[0-9] '||'L%',UPPER(TRIM(PRODUCT_NAME))))),2),1)) = 1 THEN CASE WHEN
							VPM_DATA.ISNUMERIC(RIGHT(LEFT(UPPER(TRIM(PRODUCT_NAME)),VPM_DATA.PATINDEX('%[0-9] '||'L%',UPPER(TRIM(PRODUCT_NAME)))),2)) = 1 THEN RIGHT(LEFT(UPPER(TRIM(PRODUCT_NAME)),VPM_DATA.PATINDEX('%[0-9] '||'L%',UPPER(TRIM(PRODUCT_NAME)))),2) ELSE RIGHT(RIGHT(LEFT(UPPER(TRIM(PRODUCT_NAME)),VPM_DATA.PATINDEX('%[0-9] '||'L%',UPPER(TRIM(PRODUCT_NAME)))),2),1) END
					WHEN VPM_DATA.ISNUMERIC(LEFT(RIGHT(RTRIM(LEFT(UPPER(TRIM(PRODUCT_NAME)),VPM_DATA.PATINDEX('%[0-9] '||'L%',UPPER(TRIM(PRODUCT_NAME))))),1),1)) = 1 THEN CASE WHEN
							VPM_DATA.ISNUMERIC(RIGHT(LEFT(UPPER(TRIM(PRODUCT_NAME)),VPM_DATA.PATINDEX('%[0-9] '||'L%',UPPER(TRIM(PRODUCT_NAME)))),1)) = 1 THEN RIGHT(LEFT(UPPER(TRIM(PRODUCT_NAME)),VPM_DATA.PATINDEX('%[0-9] '||'L%',UPPER(TRIM(PRODUCT_NAME)))),1) ELSE RIGHT(RIGHT(LEFT(UPPER(TRIM(PRODUCT_NAME)),VPM_DATA.PATINDEX('%[0-9] '||'L%',UPPER(TRIM(PRODUCT_NAME)))),1),1) END
					END)
			END
,MEASUREMENT =	CASE WHEN VPM_DATA.PATINDEX('%[0-9]'||'G%',UPPER(TRIM(PRODUCT_NAME))) != '0' THEN 'G' 
				WHEN VPM_DATA.PATINDEX('%[0-9] '||'G%',UPPER(TRIM(PRODUCT_NAME))) != '0' THEN 'G' 
				WHEN UPPER(TRIM(PRODUCT_NAME)) LIKE '%ML%' AND POSITION('ML' IN UPPER(TRIM(PRODUCT_NAME))) - 1 > 1 
					AND (VPM_DATA.ISNUMERIC(LEFT(RIGHT(RTRIM(LEFT(UPPER(TRIM(PRODUCT_NAME)),POSITION('ML' IN UPPER(TRIM(PRODUCT_NAME)))-1)),1),1)) = 1 OR 
						VPM_DATA.ISNUMERIC(LEFT(RIGHT(RTRIM(LEFT(UPPER(TRIM(PRODUCT_NAME)),POSITION('ML' IN UPPER(TRIM(PRODUCT_NAME)))-1)),2),1)) = 1) THEN 'ML'
				WHEN VPM_DATA.PATINDEX('%[0-9]'||'CC%',UPPER(TRIM(PRODUCT_NAME))) != '0' THEN 'CC' 
				WHEN VPM_DATA.PATINDEX('%[0-9]'||'OZ%',UPPER(TRIM(PRODUCT_NAME))) != '0' THEN 'OZ' 
				WHEN VPM_DATA.PATINDEX('%[0-9] '||'OZ%',UPPER(TRIM(PRODUCT_NAME))) != '0' THEN 'OZ' 
				WHEN VPM_DATA.PATINDEX('%[0-9]'||'KG%',UPPER(TRIM(PRODUCT_NAME))) != '0' THEN 'KG' 
				WHEN VPM_DATA.PATINDEX('%[0-9] '||'KG%',UPPER(TRIM(PRODUCT_NAME))) != '0' THEN 'KG' 
				WHEN VPM_DATA.PATINDEX('%[0-9]'||'G%',UPPER(TRIM(PRODUCT_NAME))) != '0' THEN 'G' 
				WHEN (VPM_DATA.PATINDEX('%[0-9] '||'L%',UPPER(TRIM(PRODUCT_NAME))) != '0' OR VPM_DATA.PATINDEX('%[0-9]'||'L%',UPPER(TRIM(PRODUCT_NAME))) != '0') AND NOT (VPM_DATA.PATINDEX('%[0-9]'||'LB%',UPPER(TRIM(PRODUCT_NAME))) != '0') THEN 'L'
				END
;

/************************************************** REF DATE **************************************************/

/*UPDATE REF DATE T0*/

UPDATE VPM_DATA.PM_SKUTAGGING_KIDS_PRODUCT_MASTER
SET REF_DATE_FROM_T0 = PERCENTILE_50TH
FROM VPM_DATA.PM_MOM_KID_NB_DIAPER_CYCLE_REF A
WHERE A.TYPE_FINAL = VPM_DATA.PM_SKUTAGGING_KIDS_PRODUCT_MASTER.TYPE_FINAL
AND KIDS_STAGE_FINAL IN ('NEWBORN,INFANT')
; 
