INSERT INTO VPM_DATA.SALESSKU_LOG_TIME(task,start_time) VALUES ('H.HOBBY AND INTEREST Step1.0 - SPORT',CURRENT_TIMESTAMP)
;

--DROP TABLE IF EXISTS TEMP_A_PM_SSP_SALESSKU_1YR;
SELECT MEMBER_NUMBER
      ,PARTNER_CODE
      ,BRANCH_CODE
      ,TXN_DATE
      ,TICKET_NUM
      ,SKU_CODE
      ,TXN_AMT
      ,DEPT_NAME
      ,SUBDEPT_NAME
      ,CLASS_NAME
      ,SUBCLASS_NAME
      ,PRODUCT_NAME
	  ,CAST(NULL AS VARCHAR(20)) AS SPORT_TYPE
	  ,CAST(NULL AS INT) AS SPORT_FLAG
	  ,CAST(NULL AS VARCHAR(30)) AS CATE_TYPE
  INTO TEMP TABLE TEMP_A_PM_SSP_SALESSKU_1YR
  FROM VPM_DATA.PM_ALL_SALESSKU_1YR_NEW_SCHEMA
  WHERE  PRODUCT_NAME IS NOT NULL
		AND TXN_AMT > 0
		AND PARTNER_CODE = 'SSP'
;

UPDATE TEMP_A_PM_SSP_SALESSKU_1YR
SET SPORT_TYPE = CASE WHEN UPPER(DEPT_NAME) LIKE '%SWIM%'
			    OR UPPER(SUBDEPT_NAME) LIKE '%SWIM%'
			    OR UPPER(CLASS_NAME) LIKE '%SWIM%'
			    OR UPPER(SUBCLASS_NAME) LIKE '%SWIM%'
			    OR UPPER(DEPT_NAME) LIKE '%WATER SPORT%'
			    OR UPPER(SUBDEPT_NAME) LIKE '%WATER SPORT%'
			    OR UPPER(CLASS_NAME) LIKE '%WATER SPORT%'
			    OR UPPER(SUBCLASS_NAME) LIKE '%WATER SPORT%'
				OR UPPER(DEPT_NAME) LIKE '%SPEEDO%' THEN 'SWIM & WATER_SPORT' 
              WHEN UPPER(DEPT_NAME) LIKE '%GOLF%'
			    OR UPPER(SUBDEPT_NAME) LIKE '%GOLF%'
			    OR UPPER(CLASS_NAME) LIKE '%GOLF%'
			    OR UPPER(SUBCLASS_NAME) LIKE '%GOLF%' THEN 'GOLF'
              WHEN UPPER(DEPT_NAME) LIKE '%BADMINTON%'
			    OR UPPER(SUBDEPT_NAME) LIKE '%BADMINTON%'
			    OR UPPER(CLASS_NAME) LIKE '%BADMINTON%'
			    OR UPPER(SUBCLASS_NAME) LIKE '%BADMINTON%' THEN 'BADMINTON'
              WHEN (UPPER(DEPT_NAME) LIKE '%TENNIS%' AND UPPER(DEPT_NAME) NOT LIKE '%TABLE%TENNIS%')
			    OR (UPPER(SUBDEPT_NAME) LIKE '%TENNIS%' AND UPPER(SUBDEPT_NAME) NOT LIKE '%TABLE%TENNIS%')
			    OR (UPPER(CLASS_NAME) LIKE '%TENNIS%' AND UPPER(CLASS_NAME) NOT LIKE '%TABLE%TENNIS%')
			    OR (UPPER(SUBCLASS_NAME) LIKE '%TENNIS%' AND UPPER(SUBCLASS_NAME) NOT LIKE '%TABLE%TENNIS%') THEN 'TENNIS'
              WHEN (UPPER(DEPT_NAME) LIKE '%SOCCER%' AND UPPER(DEPT_NAME) NOT LIKE '%NON%SOCCER%')
			    OR (UPPER(SUBDEPT_NAME) LIKE '%SOCCER%' AND UPPER(SUBDEPT_NAME) NOT LIKE '%NON%SOCCER%')
			    OR (UPPER(CLASS_NAME) LIKE '%SOCCER%' AND UPPER(CLASS_NAME) NOT LIKE '%NON%SOCCER%')
			    OR (UPPER(SUBCLASS_NAME) LIKE '%SOCCER%' AND UPPER(SUBCLASS_NAME) NOT LIKE '%NON%SOCCER%') THEN 'SOCCER & FUTSAL'
		      WHEN UPPER(DEPT_NAME) LIKE '%FUTSAL%'
			    OR UPPER(SUBDEPT_NAME) LIKE '%FUTSAL%'
			    OR UPPER(CLASS_NAME) LIKE '%FUTSAL%'
			    OR UPPER(SUBCLASS_NAME) LIKE '%FUTSAL%' THEN 'SOCCER & FUTSAL'
              WHEN UPPER(DEPT_NAME) LIKE '%YOGA%'
			    OR UPPER(SUBDEPT_NAME) LIKE '%YOGA%'
			    OR UPPER(CLASS_NAME) LIKE '%YOGA%'
			    OR (UPPER(SUBCLASS_NAME) LIKE '%YOGA%' AND (UPPER(PRODUCT_NAME) LIKE '%YOGA%' OR UPPER(PRODUCT_NAME) LIKE '%MAT%')) THEN 'YOGA'
             --FITNESS PUT AT LOW SEQUENCE
			 WHEN UPPER(DEPT_NAME) LIKE '%FITNESS%'
				OR UPPER(SUBDEPT_NAME) LIKE '%FITNESS%'
				OR UPPER(CLASS_NAME) LIKE '%FITNESS%'
				OR UPPER(SUBCLASS_NAME) LIKE '%FITNESS%' THEN 'OTHER SPORTS'
			 WHEN UPPER(DEPT_NAME) LIKE '%SPORT%'
				OR UPPER(SUBDEPT_NAME) LIKE '%SPORT%'
				OR UPPER(CLASS_NAME) LIKE '%SPORT%'
				OR UPPER(SUBCLASS_NAME) LIKE '%SPORT%' THEN 'OTHER SPORTS'
			 WHEN UPPER(DEPT_NAME) LIKE '%RUNNING%'
				OR UPPER(SUBDEPT_NAME) LIKE '%RUNNING%'
				OR UPPER(CLASS_NAME) LIKE '%RUNNING%'
				OR UPPER(SUBCLASS_NAME) LIKE '%RUNNING%' THEN 'RUNNING'
			WHEN UPPER(DEPT_NAME) LIKE '%TRAINING%'
				OR UPPER(SUBDEPT_NAME) LIKE '%TRAINING%'
				OR UPPER(CLASS_NAME) LIKE '%TRAINING%'
				OR UPPER(SUBCLASS_NAME) LIKE '%TRAINING%' THEN 'OTHER SPORTS'
			WHEN UPPER(DEPT_NAME) LIKE '%BASKETBALL%'
				OR UPPER(SUBDEPT_NAME) LIKE '%BASKETBALL%'
				OR UPPER(CLASS_NAME) LIKE '%BASKETBALL%'
				OR UPPER(SUBCLASS_NAME) LIKE '%BASKETBALL%' THEN 'OTHER SPORTS'
			WHEN UPPER(DEPT_NAME) LIKE '%RACKET%BALL%'
				OR UPPER(SUBDEPT_NAME) LIKE '%RACKET%BALL%'
				OR UPPER(CLASS_NAME) LIKE '%RACKET%BALL%'
				OR UPPER(SUBCLASS_NAME) LIKE '%RACKET%BALL%' THEN 'OTHER SPORTS'
			WHEN UPPER(DEPT_NAME) LIKE '%BIKE%'
				OR UPPER(SUBDEPT_NAME) LIKE '%BIKE%'
				OR UPPER(CLASS_NAME) LIKE '%BIKE%'
				OR UPPER(SUBCLASS_NAME) LIKE '%BIKE%' THEN 'OTHER SPORTS'
            ELSE 'OTHER SPORTS'
				END
;

UPDATE TEMP_A_PM_SSP_SALESSKU_1YR
SET SPORT_FLAG = CASE WHEN UPPER(DEPT_NAME) = 'CASUAL FOOTWEAR' THEN 0
			  WHEN UPPER(DEPT_NAME) = 'CROCS' THEN 0
			  WHEN UPPER(DEPT_NAME) = 'PREMIUM MDSG' THEN 0
			  WHEN UPPER(DEPT_NAME) = 'PREMIUM MKT' THEN 0
			  WHEN UPPER(DEPT_NAME) = 'SERVICE' THEN 0
			  WHEN UPPER(DEPT_NAME) = 'OUTDOOR & ACCESSORIES' 
					AND UPPER(SUBDEPT_NAME) IN ('CAMPING','PREMLUM','WATCH & SUNGLASS') THEN 0
			  WHEN UPPER(DEPT_NAME) = 'OUTDOOR & ACCESSORIES' 
					AND UPPER(SUBDEPT_NAME) = 'BAG'
					AND UPPER(SUBDEPT_NAME) <> 'GOLF BAG' THEN 0
			  WHEN UPPER(SUBDEPT_NAME) = 'CASUAL' THEN 0
			  WHEN UPPER(SUBDEPT_NAME) = 'PREMIUM' THEN 0
			  WHEN UPPER(SUBDEPT_NAME) = 'PREMIUM MKT' THEN 0
			  WHEN UPPER(SUBDEPT_NAME) = 'SERVICE' THEN 0
			  ELSE 1
		 END
;

--DROP TABLE IF EXISTS TEMP_N_FASHION_SPORT;

SELECT MEMBER_NUMBER
      ,PARTNER_CODE
      ,BRANCH_CODE
      ,TXN_DATE
      ,TICKET_NUM
      ,SKU_CODE
      ,TXN_AMT
      ,DEPT_NAME
      ,SUBDEPT_NAME
      ,CLASS_NAME
      ,SUBCLASS_NAME
      ,PRODUCT_NAME
	   ,CASE WHEN UPPER(DEPT_NAME) LIKE '%SWIM%'
				  OR UPPER(SUBDEPT_NAME) LIKE '%SWIM%'
				  OR UPPER(CLASS_NAME) LIKE '%SWIM%'
				  OR UPPER(SUBCLASS_NAME) LIKE '%SWIM%' THEN 'SWIM & WATER_SPORT'
			 WHEN UPPER(DEPT_NAME) LIKE '%WATER%SPORT%'
				  OR UPPER(SUBDEPT_NAME) LIKE '%WATER%SPORT%'
				  OR UPPER(CLASS_NAME) LIKE '%WATER%SPORT%'
				  OR UPPER(SUBCLASS_NAME) LIKE '%WATER%SPORT%' THEN 'SWIM & WATER_SPORT'
			 WHEN UPPER(DEPT_NAME) LIKE '%SPEEDO%'
				  OR UPPER(SUBDEPT_NAME) LIKE '%SPEEDO%'
				  OR UPPER(CLASS_NAME) LIKE '%SPEEDO%'
				  OR UPPER(SUBCLASS_NAME) LIKE '%SPEEDO%' THEN 'SWIM & WATER_SPORT'
			 WHEN UPPER(DEPT_NAME) LIKE '%GOLF%'
				  OR UPPER(SUBDEPT_NAME) LIKE '%GOLF%'
				  OR UPPER(CLASS_NAME) LIKE '%GOLF%'
				  OR UPPER(SUBCLASS_NAME) LIKE '%GOLF%' THEN 'GOLF'
			 WHEN UPPER(DEPT_NAME) LIKE '%BADMINTON%'
				  OR UPPER(SUBDEPT_NAME) LIKE '%BADMINTON%'
				  OR UPPER(CLASS_NAME) LIKE '%BADMINTON%'
				  OR UPPER(SUBCLASS_NAME) LIKE '%BADMINTON%' THEN 'BADMINTON'
			 WHEN (UPPER(DEPT_NAME) LIKE '%TENNIS%' AND UPPER(DEPT_NAME) NOT LIKE '%TABLE%TENNIS%')
				  OR (UPPER(SUBDEPT_NAME) LIKE '%TENNIS%' AND UPPER(SUBDEPT_NAME) NOT LIKE '%TABLE%TENNIS%')
				  OR (UPPER(CLASS_NAME) LIKE '%TENNIS%' AND UPPER(CLASS_NAME) NOT LIKE '%TABLE%TENNIS%')
				  OR (UPPER(SUBCLASS_NAME) LIKE '%TENNIS%' AND UPPER(SUBCLASS_NAME) NOT LIKE '%TABLE%TENNIS%') THEN 'TENNIS'
			 WHEN (UPPER(DEPT_NAME) LIKE '%SOCCER%' AND UPPER(DEPT_NAME) NOT LIKE '%NON%SOCCER%')
				  OR (UPPER(SUBDEPT_NAME) LIKE '%SOCCER%' AND UPPER(SUBDEPT_NAME) NOT LIKE '%NON%SOCCER%')
				  OR (UPPER(CLASS_NAME) LIKE '%SOCCER%' AND UPPER(CLASS_NAME) NOT LIKE '%NON%SOCCER%')
				  OR (UPPER(SUBCLASS_NAME) LIKE '%SOCCER%' AND UPPER(SUBCLASS_NAME) NOT LIKE '%NON%SOCCER%') THEN 'SOCCER & FUTSAL'
			 WHEN UPPER(DEPT_NAME) LIKE '%FUTSAL%'
				  OR UPPER(SUBDEPT_NAME) LIKE '%FUTSAL%'
				  OR UPPER(CLASS_NAME) LIKE '%FUTSAL%'
				  OR UPPER(SUBCLASS_NAME) LIKE '%FUTSAL%' THEN 'SOCCER & FUTSAL'
			 WHEN UPPER(DEPT_NAME) LIKE '%YOGA%'
				  OR UPPER(SUBDEPT_NAME) LIKE '%YOGA%'
				  OR UPPER(CLASS_NAME) LIKE '%YOGA%'
				  OR UPPER(SUBCLASS_NAME) LIKE '%YOGA%' THEN 'YOGA'
			 --FITNESS PUT AT LOW SEQUENCE
			 WHEN UPPER(DEPT_NAME) LIKE '%FITNESS%'
				  OR UPPER(SUBDEPT_NAME) LIKE '%FITNESS%'
				  OR UPPER(CLASS_NAME) LIKE '%FITNESS%'
				  OR UPPER(SUBCLASS_NAME) LIKE '%FITNESS%' THEN 'OTHER SPORTS'
			 WHEN UPPER(DEPT_NAME) LIKE '%SPORT%'
				  OR UPPER(SUBDEPT_NAME) LIKE '%SPORT%'
				  OR UPPER(CLASS_NAME) LIKE '%SPORT%'
				  OR UPPER(SUBCLASS_NAME) LIKE '%SPORT%' THEN 'OTHER SPORTS'
			 WHEN UPPER(DEPT_NAME) LIKE '%RUNNING%'
				  OR UPPER(SUBDEPT_NAME) LIKE '%RUNNING%'
				  OR UPPER(CLASS_NAME) LIKE '%RUNNING%'
				  OR UPPER(SUBCLASS_NAME) LIKE '%RUNNING%' THEN 'RUNNING'
			WHEN UPPER(DEPT_NAME) LIKE '%TRAINING%'
				  OR UPPER(SUBDEPT_NAME) LIKE '%TRAINING%'
				  OR UPPER(CLASS_NAME) LIKE '%TRAINING%'
				  OR UPPER(SUBCLASS_NAME) LIKE '%TRAINING%' THEN 'OTHER SPORTS'
			WHEN UPPER(DEPT_NAME) LIKE '%BASKETBALL%'
				  OR UPPER(SUBDEPT_NAME) LIKE '%BASKETBALL%'
				  OR UPPER(CLASS_NAME) LIKE '%BASKETBALL%'
				  OR UPPER(SUBCLASS_NAME) LIKE '%BASKETBALL%' THEN 'OTHER SPORTS'
			WHEN UPPER(DEPT_NAME) LIKE '%RACKET%BALL%'
				  OR UPPER(SUBDEPT_NAME) LIKE '%RACKET%BALL%'
				  OR UPPER(CLASS_NAME) LIKE '%RACKET%BALL%'
				  OR UPPER(SUBCLASS_NAME) LIKE '%RACKET%BALL%' THEN 'OTHER SPORTS'
			WHEN UPPER(DEPT_NAME) LIKE '%BIKE%'
				  OR UPPER(SUBDEPT_NAME) LIKE '%BIKE%'
				  OR UPPER(CLASS_NAME) LIKE '%BIKE%'
				  OR UPPER(SUBCLASS_NAME) LIKE '%BIKE%' THEN 'OTHER SPORTS'
		END AS SPORT_TYPE
INTO TEMP TABLE TEMP_N_FASHION_SPORT
FROM VPM_DATA.PM_SKUTAGGING_FASHION_SALESSKU_1YR_NEW_SCHEMA
WHERE PARTNER_CODE IN ('CDS','MSL','RBS')
AND UPPER(SUBCLASS_NAME) <> 'FITNESS SCALE' --CDS/RBS REVISED PRODUCT MASTER
	  AND TXN_AMT > 0
;

--DROP TABLE IF EXISTS TEMP_A_SPORT_SALESSKU_1YR;

SELECT MEMBER_NUMBER
      ,PARTNER_CODE
      ,BRANCH_CODE
      ,TXN_DATE
      ,TICKET_NUM
      ,SKU_CODE
      ,TXN_AMT
      ,DEPT_NAME
      ,SUBDEPT_NAME
      ,CLASS_NAME
      ,SUBCLASS_NAME
      ,PRODUCT_NAME
	  ,SPORT_TYPE
      ,SPORT_FLAG
INTO TEMP TABLE TEMP_A_SPORT_SALESSKU_1YR
FROM TEMP_A_PM_SSP_SALESSKU_1YR
WHERE SPORT_FLAG = 1
UNION
SELECT *
       ,1 AS SPORT_FLAG
FROM TEMP_N_FASHION_SPORT
WHERE SPORT_TYPE IS NOT NULL

UNION

SELECT A.MEMBER_NUMBER
	,'GCS' AS PARTNER_CODE
	,NULL AS BRANCH_CODE
	,TXN_DATE
	,NULL AS TICKET_NUM
	,NULL AS SKU_CODE
	,SPENDING_AMOUNT AS TXN_AMT
	,MERCHANT_LEVEL1 AS DEPT_NAME
	,MERCHANT_LEVEL2 AS SUBDEPT_NAME
	,MERCHANT_LEVEL3 AS CLASS_NAME
	,MERCHANT_LEVEL3 AS SUBCLASS_NAME
	,MERCHANT_NAME AS PRODUCT_NAME
	,CASE WHEN MERCHANT_LEVEL3 = 'BIKE' THEN 'OTHER SPORTS'
	      WHEN MERCHANT_LEVEL3 = 'GOLF/COUNTRY CLUB' AND CLEANED_MERCHANT LIKE '%GOLF%' THEN 'GOLF'
		  WHEN MERCHANT_LEVEL3 = 'MEMBERSHIP/SPORT CLUB' AND CLEANED_MERCHANT LIKE '%YOGA%' THEN 'YOGA'
		  WHEN MERCHANT_LEVEL3 = 'MEMBERSHIP/SPORT CLUB' AND CLEANED_MERCHANT LIKE '%OTHER FITNESS/SPORT CLUB%' THEN 'FITNESS MEMEBERSHIP'
		  WHEN MERCHANT_LEVEL3 = 'MEMBERSHIP/SPORT CLUB' AND CLEANED_MERCHANT LIKE '%FITNESS FIRST%' THEN 'FITNESS MEMEBERSHIP'
		  WHEN MERCHANT_LEVEL3 = 'MEMBERSHIP/SPORT CLUB' AND CLEANED_MERCHANT LIKE '%VIRGIN ACTIVE%' THEN 'FITNESS MEMEBERSHIP'
		  WHEN MERCHANT_LEVEL3 = 'MEMBERSHIP/SPORT CLUB' AND CLEANED_MERCHANT LIKE '%WE FITNESS%' THEN 'FITNESS MEMEBERSHIP'
		  WHEN MERCHANT_LEVEL3 = 'MEMBERSHIP/SPORT CLUB' AND CLEANED_MERCHANT LIKE '%EVOLUTION WELLNESS%' THEN 'FITNESS MEMEBERSHIP'
		  WHEN MERCHANT_LEVEL3 = 'MEMBERSHIP/SPORT CLUB' AND CLEANED_MERCHANT LIKE '%ABSOLUTE%' THEN 'FITNESS MEMEBERSHIP'
		  WHEN MERCHANT_LEVEL3 = 'MEMBERSHIP/SPORT CLUB' AND CLEANED_MERCHANT LIKE '%FITNESS%' THEN 'FITNESS MEMEBERSHIP'
		  WHEN MERCHANT_LEVEL3 = 'MEMBERSHIP/SPORT CLUB' AND CLEANED_MERCHANT LIKE '%CLASSPASS%' THEN 'FITNESS MEMEBERSHIP'
		  WHEN MERCHANT_LEVEL3 = 'MEMBERSHIP/SPORT CLUB' AND CLEANED_MERCHANT LIKE '%CASCADE CLUB%' THEN 'FITNESS MEMEBERSHIP'
		  WHEN MERCHANT_LEVEL3 = 'MEMBERSHIP/SPORT CLUB' AND CLEANED_MERCHANT LIKE '%THE OLYMPIC CLUB%' THEN 'FITNESS MEMEBERSHIP'
		  WHEN MERCHANT_LEVEL3 = 'MEMBERSHIP/SPORT CLUB' AND CLEANED_MERCHANT LIKE '%FITWHEY GYM%' THEN 'FITNESS MEMEBERSHIP'
		  WHEN MERCHANT_LEVEL3 = 'MEMBERSHIP/SPORT CLUB' AND CLEANED_MERCHANT LIKE '%GYM%' THEN 'FITNESS MEMEBERSHIP'
		  WHEN MERCHANT_LEVEL3 = 'MEMBERSHIP/SPORT CLUB' AND CLEANED_MERCHANT LIKE '%THE FIT BANGKOK%' THEN 'FITNESS MEMEBERSHIP'
		  WHEN MERCHANT_LEVEL3 = 'MEMBERSHIP/SPORT CLUB' AND CLEANED_MERCHANT LIKE '%FIT D RANGSIT%' THEN 'FITNESS MEMEBERSHIP'
		  WHEN MERCHANT_LEVEL3 = 'MEMBERSHIP/SPORT CLUB' AND CLEANED_MERCHANT LIKE '%ALPHA HEALTH CLUB%' THEN 'FITNESS MEMEBERSHIP'
		  WHEN MERCHANT_LEVEL3 = 'MEMBERSHIP/SPORT CLUB' AND CLEANED_MERCHANT LIKE '%D WORK OUT%' THEN 'FITNESS MEMEBERSHIP'
		  WHEN MERCHANT_LEVEL3 = 'MEMBERSHIP/SPORT CLUB' AND CLEANED_MERCHANT LIKE '%FIT%' THEN 'FITNESS MEMEBERSHIP'
		  WHEN MERCHANT_LEVEL3 = 'MEMBERSHIP/SPORT CLUB' AND CLEANED_MERCHANT LIKE '%FITFAC%' THEN 'FITNESS MEMEBERSHIP'
		  WHEN MERCHANT_LEVEL3 = 'MEMBERSHIP/SPORT CLUB' AND CLEANED_MERCHANT LIKE '%BOUNCEDANCEFIT%' THEN 'FITNESS MEMEBERSHIP'
		  WHEN MERCHANT_LEVEL3 = 'MEMBERSHIP/SPORT CLUB' AND CLEANED_MERCHANT LIKE '%MUAYTHAI%' THEN 'FITNESS MEMEBERSHIP'
		  WHEN MERCHANT_LEVEL3 = 'MEMBERSHIP/SPORT CLUB' AND CLEANED_MERCHANT LIKE '%SLIMMERS WORLD%' THEN 'FITNESS MEMEBERSHIP'
		  WHEN MERCHANT_LEVEL3 = 'MEMBERSHIP/SPORT CLUB' AND CLEANED_MERCHANT LIKE '%SWIM%' THEN 'FITNESS MEMEBERSHIP'
		  WHEN MERCHANT_LEVEL3 = 'MEMBERSHIP/SPORT CLUB' AND CLEANED_MERCHANT LIKE '%BOXING%' THEN 'FITNESS MEMEBERSHIP'
		  --WHEN MERCHANT_LEVEL3 = 'MEMBERSHIP/SPORT CLUB' AND CLEANED_MERCHANT LIKE '%C.R.C SPORT%' THEN 'OTHER SPORTS'
		  --WHEN MERCHANT_LEVEL3 = 'MEMBERSHIP/SPORT CLUB' AND CLEANED_MERCHANT LIKE '%SUPERSPORTS%' THEN 'OTHER SPORTS'
		  END AS SPORT_TYPE
	,1 AS SPORT_FLAG
FROM (SELECT * FROM VPM_Backup.GCS_TXN_1YR_NEW_SCHEMA WHERE PRIMARY_CARD_FLAG = 1) A
WHERE (MERCHANT_LEVEL3 = 'BIKE'
     OR MERCHANT_LEVEL3 = 'GOLF/COUNTRY CLUB'
	 OR MERCHANT_LEVEL3 = 'MEMBERSHIP/SPORT CLUB')
;

DELETE FROM TEMP_A_SPORT_SALESSKU_1YR
WHERE SPORT_TYPE IS NULL
      AND PARTNER_CODE = 'GCS'
;

ALTER TABLE TEMP_A_SPORT_SALESSKU_1YR
ADD COLUMN CATE_TYPE VARCHAR(20)
;

UPDATE TEMP_A_SPORT_SALESSKU_1YR
SET CATE_TYPE = CASE WHEN UPPER(SUBDEPT_NAME) LIKE '%CAP%' THEN 'CAP'
                    WHEN UPPER(CLASS_NAME) LIKE '%CAP%' THEN 'CAP'
                    WHEN UPPER(SUBCLASS_NAME) LIKE '%CAP%' THEN 'CAP'
					WHEN UPPER(SUBDEPT_NAME) LIKE '%GOGGLE%' THEN 'GOGGLE'
					WHEN UPPER(CLASS_NAME) LIKE '%GOGGLE%' THEN 'GOGGLE'
					WHEN UPPER(SUBCLASS_NAME) LIKE '%GOGGLE%' THEN 'GOGGLE'
					WHEN UPPER(SUBDEPT_NAME) LIKE '%SNORKEL%' THEN 'MASK&SNORKEL'
					WHEN UPPER(SUBDEPT_NAME) LIKE '%MASK%' THEN 'MASK&SNORKEL'
					WHEN UPPER(CLASS_NAME) LIKE '%SNORKEL%' THEN 'MASK&SNORKEL'
					WHEN UPPER(CLASS_NAME) LIKE '%MASK%' THEN 'MASK&SNORKEL'
					WHEN UPPER(SUBCLASS_NAME) LIKE '%SNORKEL%' THEN 'MASK&SNORKEL'
					WHEN UPPER(SUBCLASS_NAME) LIKE '%MASK%' THEN 'MASK&SNORKEL'
					WHEN UPPER(SUBDEPT_NAME) LIKE '%FIN%' THEN 'FIN'
					WHEN UPPER(CLASS_NAME) LIKE '%FIN%' THEN 'FIN'
					WHEN UPPER(SUBCLASS_NAME) LIKE '%FIN%' THEN 'FIN'
					WHEN UPPER(SUBDEPT_NAME) LIKE '%NOSE%CLIP%' THEN 'NOSE CLIP'
					WHEN UPPER(CLASS_NAME) LIKE '%NOSE%CLIP%' THEN 'NOSE CLIP'
					WHEN UPPER(SUBCLASS_NAME) LIKE '%NOSE%CLIP%' THEN 'NOSE CLIP'
					WHEN UPPER(SUBDEPT_NAME) LIKE '%EAR%PLUG%' THEN 'EAR PLUG'
					WHEN UPPER(CLASS_NAME) LIKE '%EAR%PLUG%' THEN 'EAR PLUG'
					WHEN UPPER(SUBCLASS_NAME) LIKE '%EAR%PLUG%' THEN 'EAR PLUG'
					WHEN UPPER(SUBDEPT_NAME) LIKE '%OTHER - APPAREL%' THEN 'BEACHWEAR'
					WHEN UPPER(SUBDEPT_NAME) LIKE '%SWIMWEAR%' THEN 'SWIMWEAR'
                    WHEN UPPER(CLASS_NAME) LIKE '%SWIMWEAR%' THEN 'SWIMWEAR'
					WHEN UPPER(SUBDEPT_NAME) LIKE '%SWIM - SWIMWEAR%' THEN 'SWIMWEAR'
					WHEN UPPER(SUBDEPT_NAME) LIKE '%SWIM - WATERSHORTS%' THEN 'SWIMWEAR'
					WHEN UPPER(DEPT_NAME) LIKE '%SWIMWEAR%' THEN 'SWIMWEAR'
					WHEN UPPER(SUBCLASS_NAME) LIKE '%SWIM SHORTS%' THEN 'SWIMWEAR'
					ELSE 'OTHER ACCESSORIES'
					END
WHERE SPORT_TYPE = 'SWIM & WATER_SPORT'
;

--DROP TABLE IF EXISTS TEMP_TMP_SPORT;
SELECT MEMBER_NUMBER
	   ,COUNT(DISTINCT TXN_DATE) AS SPORT_DAYS_VISIT
		,COUNT(*) AS SPORT_CNT_TXN
		,COUNT(DISTINCT TICKET_NUM) AS SPORT_CNT_TICKET
		,SUM(TXN_AMT) AS SPORT_TOTAL_SPEND
INTO TEMP TABLE TEMP_TMP_SPORT
FROM TEMP_A_SPORT_SALESSKU_1YR 
GROUP BY MEMBER_NUMBER
;

--DROP TABLE IF EXISTS TEMP_TMP_BADMINTON;
SELECT MEMBER_NUMBER
	  	,COUNT(DISTINCT TXN_DATE) AS BADMINTON_DAYS_VISIT
		,COUNT(*) AS BADMINTON_CNT_TXN
		,COUNT(DISTINCT TICKET_NUM) AS BADMINTON_CNT_TICKET
		,SUM(TXN_AMT) AS BADMINTON_TOTAL_SPEND
INTO TEMP TABLE TEMP_TMP_BADMINTON
FROM TEMP_A_SPORT_SALESSKU_1YR
WHERE SPORT_TYPE = 'BADMINTON'
GROUP BY MEMBER_NUMBER
;

--DROP TABLE IF EXISTS TEMP_TMP_GOLF;
SELECT MEMBER_NUMBER
	   ,COUNT(DISTINCT TXN_DATE) AS GOLF_DAYS_VISIT
		,COUNT(*) AS GOLF_CNT_TXN
		,COUNT(DISTINCT TICKET_NUM) AS GOLF_CNT_TICKET
		,SUM(TXN_AMT) AS GOLF_TOTAL_SPEND
INTO TEMP TABLE TEMP_TMP_GOLF
FROM TEMP_A_SPORT_SALESSKU_1YR
WHERE SPORT_TYPE = 'GOLF'
GROUP BY MEMBER_NUMBER
;

--DROP TABLE IF EXISTS TEMP_TMP_RUNNING;
SELECT MEMBER_NUMBER
	   ,COUNT(DISTINCT TXN_DATE) AS RUNNING_DAYS_VISIT
		,COUNT(*) AS RUNNING_CNT_TXN
		,COUNT(DISTINCT TICKET_NUM) AS RUNNING_CNT_TICKET
		,SUM(TXN_AMT) AS RUNNING_TOTAL_SPEND
INTO TEMP TABLE TEMP_TMP_RUNNING
FROM TEMP_A_SPORT_SALESSKU_1YR
WHERE SPORT_TYPE = 'RUNNING'
GROUP BY MEMBER_NUMBER
;

--DROP TABLE IF EXISTS TEMP_TMP_SOCCER_FUTSAL;
SELECT MEMBER_NUMBER
	  	,COUNT(DISTINCT TXN_DATE) AS SOCCER_FUTSAL_DAYS_VISIT
		,COUNT(*) AS SOCCER_FUTSAL_CNT_TXN
		,COUNT(DISTINCT TICKET_NUM) AS SOCCER_FUTSAL_CNT_TICKET
		,SUM(TXN_AMT) AS SOCCER_FUTSAL_TOTAL_SPEND
INTO TEMP TABLE TEMP_TMP_SOCCER_FUTSAL
FROM TEMP_A_SPORT_SALESSKU_1YR
WHERE SPORT_TYPE = 'SOCCER & FUTSAL'
GROUP BY MEMBER_NUMBER
;

--DROP TABLE IF EXISTS TEMP_TMP_SWIM_WATER_CUST;

SELECT MEMBER_NUMBER
INTO TEMP TABLE TEMP_TMP_SWIM_WATER_CUST
FROM TEMP_A_SPORT_SALESSKU_1YR
WHERE SPORT_TYPE = 'SWIM & WATER_SPORT'
    AND CATE_TYPE <> 'OTHER ACCESSORIES'
	AND CATE_TYPE <> 'BEACHWEAR'
GROUP BY MEMBER_NUMBER
HAVING COUNT(DISTINCT CATE_TYPE) >= 2
;

--DROP TABLE IF EXISTS TEMP_TMP_SWIM_WATER;

SELECT MEMBER_NUMBER
	   ,COUNT(DISTINCT TXN_DATE) AS SWIM_WATER_DAYS_VISIT
		,COUNT(*) AS SWIM_WATER_CNT_TXN
		,COUNT(DISTINCT TICKET_NUM) AS SWIM_WATER_CNT_TICKET
		,SUM(TXN_AMT) AS SWIM_WATER_TOTAL_SPEND
INTO TEMP TABLE TEMP_TMP_SWIM_WATER
FROM TEMP_A_SPORT_SALESSKU_1YR
WHERE SPORT_TYPE = 'SWIM & WATER_SPORT'
      AND CATE_TYPE <> 'OTHER ACCESSORIES'
	  AND CATE_TYPE <> 'BEACHWEAR'
GROUP BY MEMBER_NUMBER
;

--DROP TABLE IF EXISTS TEMP_TMP_TENNIS;
SELECT MEMBER_NUMBER
	  	,COUNT(DISTINCT TXN_DATE) AS TENNIS_DAYS_VISIT
		,COUNT(*) AS TENNIS_CNT_TXN
		,COUNT(DISTINCT TICKET_NUM) AS TENNIS_CNT_TICKET
		,SUM(TXN_AMT) AS TENNIS_TOTAL_SPEND
INTO TEMP TABLE TEMP_TMP_TENNIS
FROM TEMP_A_SPORT_SALESSKU_1YR 
WHERE SPORT_TYPE = 'TENNIS'
GROUP BY MEMBER_NUMBER
;

--DROP TABLE IF EXISTS TEMP_TMP_YOGA;
SELECT MEMBER_NUMBER
	  	,COUNT(DISTINCT TXN_DATE) AS YOGA_DAYS_VISIT
		,COUNT(*) AS YOGA_CNT_TXN
		,COUNT(DISTINCT TICKET_NUM) AS YOGA_CNT_TICKET
		,SUM(TXN_AMT) AS YOGA_TOTAL_SPEND
INTO TEMP TABLE TEMP_TMP_YOGA
FROM TEMP_A_SPORT_SALESSKU_1YR 
WHERE SPORT_TYPE = 'YOGA'
GROUP BY MEMBER_NUMBER
;

--------MEMBERSHIP-----------
--DROP TABLE IF EXISTS TEMP_TMP_FITNESS_MEMBER;
SELECT MEMBER_NUMBER
	  	,COUNT(DISTINCT TXN_DATE) AS FITNESS_MEMBER_DAYS_VISIT
		,COUNT(*) AS FITNESS_MEMBER_CNT_TXN
		,COUNT(DISTINCT TICKET_NUM) AS FITNESS_MEMBER_CNT_TICKET
		,SUM(TXN_AMT) AS FITNESS_MEMBER_TOTAL_SPEND
INTO TEMP TABLE TEMP_TMP_FITNESS_MEMBER
FROM TEMP_A_SPORT_SALESSKU_1YR
WHERE SPORT_TYPE = 'FITNESS MEMEBERSHIP'
GROUP BY MEMBER_NUMBER
;

--DROP TABLE IF EXISTS TEMP_A_SPORT_SVOC;

SELECT A.*
		,B.BADMINTON_DAYS_VISIT
		,B.BADMINTON_CNT_TXN
		,B.BADMINTON_CNT_TICKET
		,B.BADMINTON_TOTAL_SPEND
		,D.GOLF_DAYS_VISIT
		,D.GOLF_CNT_TXN
		,D.GOLF_CNT_TICKET
		,D.GOLF_TOTAL_SPEND
		,E.RUNNING_DAYS_VISIT
		,E.RUNNING_CNT_TXN
		,E.RUNNING_CNT_TICKET
		,E.RUNNING_TOTAL_SPEND
		,F.SOCCER_FUTSAL_DAYS_VISIT
		,F.SOCCER_FUTSAL_CNT_TXN
		,F.SOCCER_FUTSAL_CNT_TICKET
		,F.SOCCER_FUTSAL_TOTAL_SPEND
		,G.SWIM_WATER_DAYS_VISIT
		,G.SWIM_WATER_CNT_TXN
		,G.SWIM_WATER_CNT_TICKET
		,G.SWIM_WATER_TOTAL_SPEND
		,H.TENNIS_DAYS_VISIT
		,H.TENNIS_CNT_TXN
		,H.TENNIS_CNT_TICKET
		,H.TENNIS_TOTAL_SPEND
		,I.YOGA_DAYS_VISIT
		,I.YOGA_CNT_TXN
		,I.YOGA_CNT_TICKET
		,I.YOGA_TOTAL_SPEND
		,J.FITNESS_MEMBER_DAYS_VISIT
		,J.FITNESS_MEMBER_CNT_TXN
		,J.FITNESS_MEMBER_CNT_TICKET
		,J.FITNESS_MEMBER_TOTAL_SPEND
		,CAST(NULL AS INT) AS BADMINTON_FLAG
		,CAST(NULL AS INT) AS GOLF_FLAG
		,CAST(NULL AS INT) AS RUNNING_FLAG
		,CAST(NULL AS INT) AS SOCCER_FUTSAL_FLAG
		,CAST(NULL AS INT) AS SWIM_WATER_FLAG
		,CAST(NULL AS INT) AS TENNIS_FLAG
		,CAST(NULL AS INT) AS YOGA_FLAG
		,CAST(NULL AS INT) AS FITNESS_FLAG
		,CAST(NULL AS INT) AS SPORT_FLAG
		,CAST(NULL AS VARCHAR(100)) AS SPORT_COMBINE
INTO TEMP TABLE TEMP_A_SPORT_SVOC
FROM TEMP_TMP_SPORT A
LEFT JOIN TEMP_TMP_BADMINTON B ON A.MEMBER_NUMBER = B.MEMBER_NUMBER
LEFT JOIN TEMP_TMP_GOLF D ON A.MEMBER_NUMBER = D.MEMBER_NUMBER
LEFT JOIN TEMP_TMP_RUNNING E ON A.MEMBER_NUMBER = E.MEMBER_NUMBER
LEFT JOIN TEMP_TMP_SOCCER_FUTSAL F ON A.MEMBER_NUMBER = F.MEMBER_NUMBER
LEFT JOIN TEMP_TMP_SWIM_WATER G ON A.MEMBER_NUMBER = G.MEMBER_NUMBER
LEFT JOIN TEMP_TMP_TENNIS H ON A.MEMBER_NUMBER = H.MEMBER_NUMBER
LEFT JOIN TEMP_TMP_YOGA I ON A.MEMBER_NUMBER = I.MEMBER_NUMBER
LEFT JOIN TEMP_TMP_FITNESS_MEMBER J ON A.MEMBER_NUMBER = J.MEMBER_NUMBER
;

UPDATE TEMP_A_SPORT_SVOC
SET BADMINTON_FLAG = CASE WHEN BADMINTON_CNT_TXN >= 1 THEN 1
					  	  ELSE 0
				     END
	,GOLF_FLAG = CASE WHEN GOLF_CNT_TXN >= 1 THEN 1
					  ELSE 0
				 END  
	,RUNNING_FLAG = CASE WHEN RUNNING_CNT_TXN >= 3 THEN 1
					     ELSE 0
				    END   
	,SOCCER_FUTSAL_FLAG  = CASE WHEN SOCCER_FUTSAL_CNT_TXN >= 3 THEN 1
								ELSE 0
						   END  
	,SWIM_WATER_FLAG = CASE WHEN MEMBER_NUMBER IN (SELECT MEMBER_NUMBER FROM TEMP_TMP_SWIM_WATER_CUST) THEN 1
						    ELSE 0
					   END   
	,TENNIS_FLAG = CASE WHEN TENNIS_CNT_TXN >= 1 THEN 1
						ELSE 0
					END  
	,YOGA_FLAG = CASE WHEN YOGA_CNT_TXN >= 1 THEN 1
					 ELSE 0
				 END  
	,FITNESS_FLAG = CASE WHEN FITNESS_MEMBER_CNT_TXN >= 1 THEN 1
					  	 ELSE 0
				    END
;

UPDATE TEMP_A_SPORT_SVOC 
SET SPORT_FLAG = CASE WHEN COALESCE(BADMINTON_FLAG,0)  
							+COALESCE(GOLF_FLAG,0) 
							+COALESCE(RUNNING_FLAG,0)  
							+COALESCE(SOCCER_FUTSAL_FLAG,0)  
							+COALESCE(SWIM_WATER_FLAG,0)  
							+COALESCE(TENNIS_FLAG,0)  
							+COALESCE(YOGA_FLAG,0) 
							+COALESCE(FITNESS_FLAG,0) >= 1 THEN 1
					  WHEN SPORT_CNT_TXN >= 3 THEN 1
					  ELSE 0
				  END
;

UPDATE TEMP_A_SPORT_SVOC
SET SPORT_COMBINE = CASE WHEN COALESCE(BADMINTON_FLAG,0) = 1 THEN  ',BADMINTON' ELSE '' END
					|| CASE WHEN COALESCE(GOLF_FLAG,0) = 1 THEN  ',GOLF' ELSE '' END
					|| CASE WHEN COALESCE(RUNNING_FLAG,0) = 1 THEN  ',RUNNING' ELSE '' END
					|| CASE WHEN COALESCE(SOCCER_FUTSAL_FLAG,0) = 1 THEN  ',SOCCER/FUTSAL' ELSE '' END
					|| CASE WHEN COALESCE(SWIM_WATER_FLAG,0) = 1 THEN  ',SWIM/WATER SPORT' ELSE '' END
					|| CASE WHEN COALESCE(TENNIS_FLAG,0) = 1 THEN  ',TENNIS'ELSE ''  END
					|| CASE WHEN COALESCE(YOGA_FLAG,0) = 1 THEN  ',YOGA' ELSE '' END
					|| CASE WHEN COALESCE(FITNESS_FLAG,0) = 1 THEN  ',FITNESS' ELSE '' END
;

UPDATE TEMP_A_SPORT_SVOC
SET SPORT_COMBINE = CASE WHEN LEFT(SPORT_COMBINE,1) = ',' THEN RIGHT(SPORT_COMBINE,LENGTH(SPORT_COMBINE)-1)
						 WHEN SPORT_COMBINE = '' AND SPORT_FLAG = 1 THEN 'OTHERS'
						 ELSE NULL
					END
;


---------------------------------------------------------------------- HOBBY & INTEREST CUST SEGMENT-------------------------------------------------------------
DROP TABLE IF EXISTS VPM_DATA.PM_SKUTAGGING_HOBBY_INTEREST_NEW_SCHEMA
;
SELECT A.MEMBER_NUMBER
	,B.BADMINTON_FLAG 
	,B.GOLF_FLAG 
	,B.RUNNING_FLAG 
	,B.SOCCER_FUTSAL_FLAG 
	,B.SWIM_WATER_FLAG 
	,B.TENNIS_FLAG 
	,B.YOGA_FLAG 
	,B.FITNESS_FLAG
	,B.SPORT_FLAG 
	,B.SPORT_COMBINE
	,CAST(NULL AS INT) AS WINE_FLAG
	,CAST(NULL AS INT) AS WINE_ACCESSORY_FLAG
	,CAST(NULL AS INT) AS BEER_FLAG
	,CAST(NULL AS INT) AS WHISKY_FLAG
	,CAST(NULL AS INT) AS TRAVEL_INTEREST_FLAG
	,CAST(NULL AS FLOAT) AS TRAVEL_SPEND
	,CAST(NULL AS INT) AS TRAVEL_DAY_VISIT
	,CAST(NULL AS VARCHAR(150)) AS TRAVEL_PREFER_TYPE
	,CAST(NULL AS VARCHAR(150)) AS TRAVEL_PREFER_MERCHANT
	,CAST(NULL AS INT) AS TRAVEL_DOMESTICS_FLAG
	,CAST(NULL AS INT) AS TRAVEL_ABOARD_FLAG
	,CAST(NULL AS INT) AS TRAVEL_ASIA_FLAG
	,CAST(NULL AS INT) AS TRAVEL_SOUTHEAST_ASIA_FLAG
	,CAST(NULL AS INT) AS TRAVEL_USA_FLAG
	,CAST(NULL AS INT) AS TRAVEL_EUROPE_FLAG
	,CAST(NULL AS INT) AS TRAVEL_UK_FLAG
	,CAST(NULL AS INT) AS TRAVEL_JAPAN_FLAG
	,CAST(NULL AS INT) AS TRAVEL_CHINA_FLAG
	,CAST(NULL AS INT) AS TRAVEL_HONGKONG_FLAG
	,CAST(NULL AS INT) AS TRAVEL_MACAO_FLAG
	,CAST(NULL AS INT) AS TRAVEL_SOUTH_KOREA_FLAG
	,CAST(NULL AS INT) AS TRAVEL_SINGAPORE_FLAG
	,CAST(NULL AS INT) AS DINING_INTEREST_FLAG
	,CAST(NULL AS FLOAT) AS DINING_SPEND
	,CAST(NULL AS INT) AS DINING_DAY_VISIT
	,CAST(NULL AS VARCHAR(150)) AS DINING_PREFERRED_CUISINE
	,CAST(NULL AS VARCHAR(150)) AS DINING_PREFERRED_CUISINE_GROUP
	,CAST(NULL AS INT) AS BOOK_INTEREST_FLAG
	,CAST(NULL AS INT) AS EBOOK_DOWNLOAD_FLAG
	,CAST(NULL AS INT) AS EBOOK_GADGET_FLAG
	,CAST(NULL AS INT) AS BOOK_STORE_FLAG
	,CAST(NULL AS VARCHAR(150)) AS BOOK_PREFER_TYPE
	,CAST(NULL AS VARCHAR(100)) AS BOOK_PREFER_TYPE_1
	,CAST(NULL AS VARCHAR(100)) AS BOOK_PREFER_TYPE_2
	,CAST(NULL AS VARCHAR(100)) AS BOOK_PREFER_TYPE_3
	--,CAST(NULL AS INT) AS IT_GADGET_INTEREST
	,CAST(NULL AS INT) AS AUDIO_FLAG
	,CAST(NULL AS INT) AS TRAVEL_GEAR_FLAG
	,CAST(NULL AS INT) AS GAMING_GEAR_FLAG
	,CAST(NULL AS INT) AS SMART_HOME_FLAG
	,CAST(NULL AS INT) AS SMART_WATCH_FLAG 
	--,CAST(NULL AS VARCHAR(150)) AS IT_GADGET_TYPE
	,CAST(NULL AS INT) AS ACTIVE_SPORT_FLAG
	,CAST(NULL AS VARCHAR(150)) AS SPORT_PREFER_TYPE
	,CAST(NULL AS FLOAT) AS HEALTHY_FOOD_SCORE
	,CAST(NULL AS INT) AS HEALTHY_FOOD_FLAG
	,CAST(NULL AS VARCHAR(150)) AS HEALTHY_FOOD_SEGMENT
	,CAST(NULL AS INT) AS HEALTHY_LIFESTYLE_FLAG
	,CAST(NULL AS VARCHAR(150)) AS HEALTHY_LIFESTYLE_PREFERRED
	,CAST(NULL AS INT) AS BEAUTY_INTEREST_FLAG
	,CAST(NULL AS VARCHAR(100)) AS BEAUTY_PREFER_SERVICES
	,CAST(NULL AS VARCHAR(100)) AS BEAUTY_PREFER_TYPE_1
	,CAST(NULL AS VARCHAR(100)) AS BEAUTY_PREFER_TYPE_2
	,CAST(NULL AS VARCHAR(100)) AS BEAUTY_PREFER_TYPE_3
	,CAST(NULL AS INT) AS HOSPITAL_FLAG
	,CAST(NULL AS INT) AS LUXURY_HOSPITAL_FLAG
	,CAST(NULL AS VARCHAR(100)) AS COFFEE_SEGMENT
INTO VPM_DATA.PM_SKUTAGGING_HOBBY_INTEREST_NEW_SCHEMA
FROM VPM_DATA.PM_SVOC_NEW_SCHEMA A
LEFT JOIN TEMP_A_SPORT_SVOC B
ON A.MEMBER_NUMBER=B.MEMBER_NUMBER
;

UPDATE VPM_DATA.SALESSKU_LOG_TIME SET end_time = CURRENT_TIMESTAMP WHERE task = 'H.HOBBY AND INTEREST Step1.0 - SPORT' AND EXTRACT(MONTH FROM start_time) = EXTRACT(MONTH FROM CURRENT_TIMESTAMP)
;