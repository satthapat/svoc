/*PLEASE CONSIDER LINE 700 WHETHER THE YEAR IS APPROPRIATE*/
INSERT INTO VPM_DATA.SALESSKU_LOG_TIME(task,start_time) VALUES ('GCS Step2.4 - GCS Affinity and GCS SVOC',current_timestamp)
;

/******************************************************************/
/********  GCS CUSTOMER LIST FROM K.VENG UPDATE 10/05/2019 ********/

--19m 40s --29m 51s
--DROP TABLE IF EXISTS TEMP_CENTRAL_THE1_MEMBER
;

select A.*
		,B.BIN_NUMBER AS CC_BIN_NUMBER
		,B.PAYMENT_CARD_TYPE
		,B.ISSUING_BANK AS BANK
		,B.CARD_TYPE
		,B.CARD_LEVEL
		,B.CARD_DESC
		,B.MINIMUM_INCOME
		,B.MINIMUM_DEPOSIT
		,RANK() OVER(PARTITION BY MEMBER_NUMBER ORDER BY B.CARD_RANK ASC,A.CARD_TYPE_RANK ASC,CAST(A.PermanentCreditLine AS FLOAT) DESC NULLS LAST) AS CARD_RANK
INTO TEMP TABLE TEMP_CENTRAL_THE1_MEMBER
FROM (SELECT DISTINCT *
	,(CASE WHEN CardType = 'P' THEN 1 WHEN CardType = 'S' THEN 2 END) AS CARD_TYPE_RANK /*P:PRIMARY, S:SECOND*/
		FROM Analysis_Data.GCS_Demographics) A
LEFT JOIN  (SELECT *
				,(CASE WHEN CARD_DESC = 'CENTRAL THE 1 THE BLACK' THEN 1
						WHEN CARD_DESC = 'CENTRAL THE 1 BLACK' THEN 2
						WHEN CARD_DESC = 'CENTRAL THE 1 LUXE' THEN 3
						WHEN CARD_DESC = 'CENTRAL THE 1 REDZ' THEN 4
						END) AS CARD_RANK
			FROM VPM_DATA.PM_CREDIT_CARD_PRODUCT_MASTER_6DIGIT
			WHERE CARD_DESC LIKE '%CENTRAL%') B
ON LEFT(A.CreditCardNo,6) = B.BIN_NUMBER
;

DROP TABLE IF EXISTS VPM_SVOC.CENTRAL_THE1_MEMBER_NEW_SCHEMA
;

SELECT MEMBER_NUMBER
      ,MAX(PermanentCreditLine) AS PermanentCreditLine
      ,MAX(T1C_CARD_NO) AS T1C_CARD_NO
      ,MAX(CreditCardNo) AS CreditCardNo
      ,MAX(CardStatus) AS CardStatus
      ,MAX(CardType) AS CardType
      ,MAX(CustomerNumber) AS CustomerNumber
      ,MAX(CC_BIN_NUMBER) AS CC_BIN_NUMBER
      ,MAX(PAYMENT_CARD_TYPE) AS PAYMENT_CARD_TYPE
      ,MAX(BANK) AS BANK
      ,MAX(CARD_TYPE) AS CARD_TYPE
      ,MAX(CARD_LEVEL) AS CARD_LEVEL
      ,MAX(CARD_DESC) AS CARD_DESC
      ,MAX(MINIMUM_INCOME) AS MINIMUM_INCOME
      ,MAX(MINIMUM_DEPOSIT) AS MINIMUM_DEPOSIT
      ,MAX(CARD_RANK) AS CARD_RANK
INTO VPM_SVOC.CENTRAL_THE1_MEMBER_NEW_SCHEMA
FROM TEMP_CENTRAL_THE1_MEMBER
WHERE CARD_RANK = 1
GROUP BY MEMBER_NUMBER
;


/**************************************************************/
/****************** CALCULATE AFFINITY SCORE ******************/


--DROP TABLE IF EXISTS TEMP_CNT_ALL_CREDIT_CARD
;
SELECT MEMBER_NUMBER, COUNT(DISTINCT SUBSTRING(RangCreditCard,1,6)) AS NO_OF_CREDITCARD_ALL
INTO TEMP TABLE TEMP_CNT_ALL_CREDIT_CARD
FROM VPM_DATA.PM_CUSTOMER_CC_NEW_SCHEMA
GROUP BY MEMBER_NUMBER
;

DROP TABLE IF EXISTS VPM_BACKUP.GCS_AFF_SCORE_NEW_SCHEMA
;

CREATE TABLE VPM_BACKUP.GCS_AFF_SCORE_NEW_SCHEMA AS
SELECT	A.MEMBER_NUMBER
		,A.CC_BIN_NUMBER
		,A.BANK
		,A.CARD_DESC
		,A.CARD_LEVEL
		,A.MINIMUM_INCOME
		,A.MINIMUM_DEPOSIT
		,D.NO_OF_CREDITCARD_ALL
		,B.CREDIT_LIMIT
		,COUNT(B.MEMBER_NUMBER)  AS CARD_NO_TXN
		,COUNT(*)*1.0/12.0  AS CARD_MONTHLY_TXN
		,datediff(month,MAX(TXN_DATE),(SELECT MAX(TXN_DATE) FROM VPM_BACKUP.GCS_TXN_1YR_NEW_SCHEMA)) AS CARD_RECENCY
		,COUNT(DISTINCT EXTRACT(MONTH FROM TXN_DATE)) AS CARD_ACTIVITY_MONTH
		,COUNT(DISTINCT MERCHANT_LEVEL3) AS CATEGORY_COUNT
		,SUM(SPENDING_AMOUNT) AS TTL_SPD
		,SUM(CASE WHEN BUorNonBU = 'Non BU' THEN SPENDING_AMOUNT ELSE 0 END) AS TTL_SPD_NONBU
		,SUM(SPENDING_AMOUNT)*1.0/12.0 AS CARD_MONTHLY_SPD
		,(CASE WHEN B.CREDIT_LIMIT > 0 THEN (SUM(SPENDING_AMOUNT)*1.0/12.0)/B.CREDIT_LIMIT
			 ELSE 0 END) AS AVG_UTIL
		,CAST(NULL AS FLOAT) AS CARD_COUNT_TXN_PCT
		,CAST(NULL AS FLOAT) AS CARD_RECENCY_PCT
		,CAST(NULL AS FLOAT) AS CARD_COUNT_ACT_PCT
		,CAST(NULL AS FLOAT) AS CARD_COUNT_CAT_PCT
		,CAST(NULL AS FLOAT) AS CARD_SPD_AMT_PCT
		,CAST(NULL AS FLOAT) AS CARD_NO_CC_PCT
		,CAST(NULL AS FLOAT) AS CARD_AFFINITY_SCORE
		,CAST(NULL AS VARCHAR(20)) AS AFFINITY_SEGMENT
FROM VPM_SVOC.CENTRAL_THE1_MEMBER_NEW_SCHEMA A
LEFT JOIN (SELECT MEMBER_NUMBER, MAX(PermanentCreditLine) AS CREDIT_LIMIT
	        FROM Analysis_Data.GCS_Demographics
		    GROUP BY MEMBER_NUMBER) B ON A.MEMBER_NUMBER = B.MEMBER_NUMBER
LEFT JOIN (SELECT * FROM VPM_BACKUP.GCS_TXN_1YR_NEW_SCHEMA WHERE PRIMARY_CARD_FLAG = 1) C ON A.MEMBER_NUMBER = C.MEMBER_NUMBER
LEFT JOIN TEMP_CNT_ALL_CREDIT_CARD D ON A.MEMBER_NUMBER = D.MEMBER_NUMBER
GROUP BY A.MEMBER_NUMBER,
		A.CC_BIN_NUMBER,
		A.BANK,
		A.CARD_DESC,
		A.CARD_LEVEL,
		A.MINIMUM_INCOME,
		A.MINIMUM_DEPOSIT,
		D.NO_OF_CREDITCARD_ALL,
		B.CREDIT_LIMIT;

/*** VAR1 Number of Txn ***/	
UPDATE VPM_BACKUP.GCS_AFF_SCORE_NEW_SCHEMA 
SET	CARD_COUNT_TXN_PCT = CASE WHEN CARD_MONTHLY_TXN >= 10 THEN 1
							  ELSE SQRT(4*0.025*CARD_MONTHLY_TXN) END /** Parabola open right (h,k) = (0,0) **/
;

/*** VAR2 Recent Activity ***/
UPDATE VPM_BACKUP.GCS_AFF_SCORE_NEW_SCHEMA 
SET	CARD_RECENCY_PCT = CASE WHEN CARD_RECENCY = 0 THEN 1
							ELSE 1- SQRT(0.07363*CARD_RECENCY) END 
;

/*** VAR3 Activity Month ***/
UPDATE VPM_BACKUP.GCS_AFF_SCORE_NEW_SCHEMA
SET	CARD_COUNT_ACT_PCT = CASE WHEN CARD_ACTIVITY_MONTH >= 12 THEN 1							  
							  ELSE (0.0818*CARD_ACTIVITY_MONTH)+0.01819 END /** LINEAR (12,1) (1,0.1)  **/
;

/*** VAR4 CC Category Variety ***/
UPDATE VPM_BACKUP.GCS_AFF_SCORE_NEW_SCHEMA
SET	CARD_COUNT_CAT_PCT = CASE WHEN CATEGORY_COUNT >= 10 THEN 1
							  ELSE 0.1*CATEGORY_COUNT END /** LINEAR (10,1) (1,0.1)  **/
;

/*** VAR5 CC Spending Volumn ***/

UPDATE VPM_BACKUP.GCS_AFF_SCORE_NEW_SCHEMA
SET	CARD_SPD_AMT_PCT = CASE WHEN CARD_MONTHLY_SPD >= spd THEN 1
                            WHEN CARD_MONTHLY_SPD >= 0 THEN SQRT(CARD_MONTHLY_SPD)/SQRT(spd) 
                            ELSE 0 END
FROM (SELECT member_number,MIN(CARD_MONTHLY_SPD) AS spd FROM
					(
					SELECT member_number,CARD_MONTHLY_SPD,NTILE(10)OVER(ORDER BY CARD_MONTHLY_SPD DESC) AS PCT
					FROM VPM_BACKUP.GCS_AFF_SCORE_NEW_SCHEMA
					WHERE CARD_MONTHLY_SPD > 0
					) A
				WHERE PCT = 1
			GROUP BY member_number) T
WHERE VPM_BACKUP.GCS_AFF_SCORE_NEW_SCHEMA.member_number = t.member_number;


/**************************************************************/
/****************** CALCULATE AFFINITY SCORE ******************/

UPDATE VPM_BACKUP.GCS_AFF_SCORE_NEW_SCHEMA
SET CARD_AFFINITY_SCORE = (CARD_COUNT_TXN_PCT*0.2) +
							(CARD_RECENCY_PCT*0.2) +
							(CARD_COUNT_ACT_PCT*0.2) +
							(CARD_COUNT_CAT_PCT*0.2) +																		
							(CARD_SPD_AMT_PCT*0.2)								 
;

UPDATE  VPM_BACKUP.GCS_AFF_SCORE_NEW_SCHEMA
SET AFFINITY_SEGMENT = CASE WHEN CARD_AFFINITY_SCORE >=0.8 THEN 'HIGH'
							WHEN CARD_AFFINITY_SCORE >= 0.5  THEN 'MID'
                            WHEN CARD_AFFINITY_SCORE < 0.5  THEN 'LOW'
                            END
;


DROP TABLE IF EXISTS VPM_BACKUP.GCS_CUST_SVOC_SEGMENT_REF_NEW_SCHEMA
;

DROP TABLE IF EXISTS VPM_Backup.GCS_CUST_SVOC_SEGMENT_NEW_SCHEMA
;
SELECT A.*,
	/*B.LUXURY_SCORE AS DINING_LUXURY_SCORE,*/
	B.PERCENTILE_SCORE AS DINING_LUXURY_PERCENTILE_SCORE,
	/*C.LUXURY_SCORE AS HOSPITAL_LUXURY_SCORE,*/
	C.PERCENTILE_SCORE AS HOSPITAL_LUXURY_PERCENTILE_SCORE,
	/*D.LUXURY_SCORE AS HOTEL_LUXURY_SCORE,*/
	D.PERCENTILE_SCORE AS HOTEL_LUXURY_PERCENTILE_SCORE,
	/*E.LUXURY_SCORE AS SHOPPING_LUXURY_SCORE,*/
	E.PERCENTILE_SCORE AS SHOPPING_LUXURY_PERCENTILE_SCORE,
	COALESCE(F.DINING_SEGMENT,'') AS DINING_SEGMENT,
	COALESCE(G.CAR_USAGE_SEGMENT,'') AS CAR_USAGE_SEGMENT,
	CASE WHEN G.BTS_TOPUP_COUNT >= 3 AND G.MRT_TOPUP_COUNT >= 3 THEN 'BTS AND MRT'
		 WHEN G.BTS_TOPUP_COUNT >= 3 THEN 'BTS'
		 WHEN G.MRT_TOPUP_COUNT >= 3 THEN 'MRT' END AS PUBLIC_TRANSPORTATION_MODE,
	COALESCE(H.HOUSEHOLD_SEGMENT,'') AS  HOUSEHOLD_SEGMENT,
	COALESCE(I.TRAVEL_SEGMENT,'') AS TRAVEL_SEGMENT,
	I.DUTY_FREE_SPEND,
	I.BUDGET_AIR_SUM_AMT AS BUDGET_AIRLINE_SPEND,
	I.FULL_AIR_SUM_AMT AS FULL_SERVICE_AIRLINE_SPEND,
	I.INTER_HOTEL_SPEND,
	I.INTER_SHOPPING_SPEND,
	I.INTER_DINING_SPEND,
	CASE WHEN I.LUXURY_TYPE_COUNT >= 2 OR I.TRAVEL_LUXURY_SEGMENT = 'LUXURY' THEN 'HIGH'
		 WHEN I.LUXURY_TYPE_COUNT = 1 THEN 'MID'
		 WHEN I.LUXURY_TYPE_COUNT = 0 THEN 'LOW'
		 END AS TRAVEL_LUXURY,
	COALESCE(J.SHOPPING_SEGMENT,'') AS SHOPPING_SEGMENT,
	COALESCE(K.HEALTH_SEGMENT,'') AS HEALTH_SEGMENT,
	COALESCE(L.KIDS_SEGMENT,'') AS KIDS_SEGMENT,
	COALESCE(M.LEISURE_SEGMENT,'') AS LEISURE_SEGMENT,
	COALESCE(N.INS_SEGMENT,'') AS INS_SEGMENT,
	O.TOTAL_DAYS_SPEND,
	O.TOTAL_DAYS_SPEND_IN_MALL,
	O.TOTAL_DAYS_SPEND_IN_CDS,
	O.TOTAL_DAYS_SPEND_IN_RBS,
	O.TOTAL_DAYS_SPEND_BU_IN_MALL,
	O.TOTAL_DAYS_SPEND_BU_IN_CDS,
	O.TOTAL_DAYS_SPEND_BU_IN_RBS,
	O.TOTAL_DAYS_SPEND_NON_BU,
	O.TOTAL_DAYS_SPEND_NON_BU_IN_MALL,
	O.TOTAL_DAYS_SPEND_NON_BU_IN_CDS,
	O.TOTAL_DAYS_SPEND_NON_BU_IN_RBS,
	O.TOTAL_BU_SPEND,
	O.TOTAL_NON_BU_SPEND,
	O.TOTAL_NON_BU_IN_CDS_SPEND,
	O.TOTAL_NON_BU_IN_RBS_SPEND,
	O.TOTAL_DAYS_DINING_IN_MALL,
	O.TOTAL_DAYS_DINING_IN_CDS,
	O.TOTAL_DAYS_DINING_IN_RBS,
	O.TOTAL_FASHION_SPEND_BU,
	O.TOTAL_FASHION_SPEND_NON_BU,
	O.TOTAL_FASHION_SPEND_NON_BU_IN_MALL,
	O.TOTAL_BEAUTY_SPEND_BU,
	O.TOTAL_BEAUTY_SPEND_NON_BU,
	O.TOTAL_BEAUTY_SPEND_NON_BU_IN_MALL,
	O.TOTAL_ELECTRONICS_SPEND_BU,
	O.TOTAL_ELECTRONICS_SPEND_NON_BU,
	O.TOTAL_ELECTRONICS_SPEND_NON_BU_IN_MALL,
	O.TOTAL_HOME_IMPROVEMENT_SPEND_BU,
	O.TOTAL_HOME_IMPROVEMENT_SPEND_NON_BU,
	O.TOTAL_GROCERY_SPEND_BU,
	O.TOTAL_GROCERY_SPEND_NON_BU,
	O.TOTAL_BOOK_AND_STATIONARY_SPEND_BU,
	O.TOTAL_BOOK_AND_STATIONARY_SPEND_NON_BU,
	O.TOTAL_BOOK_AND_STATIONARY_SPEND_NON_BU_IN_MALL,
	O.GCS_FASHION_LUX_SEGMENT
	,CAST(NULL AS FLOAT) AS LUXURY_SCORE
	,CAST(NULL AS FLOAT) AS INSURANCE_PREMIUM
	,CAST(NULL AS FLOAT) AS  INVESTMENT_AMOUNT
    ,CAST(NULL AS FLOAT) AS CAR_OWNERSHIPS
	,CAST(NULL AS FLOAT) AS EUROPE_CAR_OWNERSHIPS
	,CAST(NULL AS VARCHAR(50)) AS CAR_BRAND
	,CAST(NULL AS FLOAT) AS TRAVEL_SCORE
	,CAST(NULL AS FLOAT) AS TRAVEL_SPENDING
	,CAST(NULL AS FLOAT) AS DINING_SCORE
	,CAST(NULL AS FLOAT) AS DINING_SPENDING
	,CAST(NULL AS FLOAT) AS EDUCATION_SCORE
	,CAST(NULL AS FLOAT) AS EDUCATION_SPENDING
	,CAST(NULL AS FLOAT) AS BOOK_AND_STATIONARY_SCORE
	,CAST(NULL AS FLOAT) AS BOOK_AND_STATIONARY_SPENDING
	,CAST(NULL AS FLOAT) AS PERSONAL_CARE_SCORE
	,CAST(NULL AS FLOAT) AS PERSONAL_CARE_SPENDING
	,CAST(NULL AS FLOAT) AS HOSPITAL_AND_DRUG_SCORE
	,CAST(NULL AS FLOAT) AS HOSPITAL_AND_DRUG_SPENDING
	,CAST(NULL AS FLOAT) AS HOME_IMPROVEMENT_SCORE
	,CAST(NULL AS FLOAT) AS HOME_IMPROVEMENT_SPENDING
	,CAST(NULL AS FLOAT) AS FASHION_SCORE
	,CAST(NULL AS FLOAT) AS FASHION_SPENDING
	,CAST(NULL AS FLOAT) AS BEAUTY_SCORE
	,CAST(NULL AS FLOAT) AS BEAUTY_SPENDING		
	,CAST(NULL AS FLOAT) AS ELECTRONICS_SCORE
	,CAST(NULL AS FLOAT) AS ELECTRONICS_SPENDING
	,CAST(NULL AS FLOAT) AS GROCERY_SCORE
	,CAST(NULL AS FLOAT) AS GROCERY_SPENDING
	,CAST(NULL AS FLOAT) AS LEISURE_ACTIVITY_SCORE
	,CAST(NULL AS FLOAT) AS LEISURE_ACTIVITY_SPENDING
	,CAST(NULL AS FLOAT) AS DINING_LUXURY_SCORE
	,CAST(NULL AS VARCHAR(50)) AS DINING_LUXURY_SEGMENT
	,CAST(NULL AS VARCHAR(200)) AS DINING_PREFERRED_CUISINE
	,CAST(NULL AS VARCHAR(200)) AS DINING_PREFERRED_CUISINE_GROUP
	,CAST(NULL AS FLOAT) AS FASHION_LUXURY_SCORE
	,CAST(NULL AS VARCHAR(50)) AS FASHION_LUXURY_SEGMENT
	,CAST(NULL AS FLOAT) AS GCS_FASHION_SPEND
INTO VPM_Backup.GCS_CUST_SVOC_SEGMENT_NEW_SCHEMA
FROM VPM_BACKUP.GCS_AFF_SCORE_NEW_SCHEMA A
LEFT JOIN VPM_BACKUP.GCS_CUST_DINING_LUXURY_SCORE_NEW_SCHEMA B
ON A.MEMBER_NUMBER = B.MEMBER_NUMBER
LEFT JOIN VPM_BACKUP.GCS_CUST_HOSPITAL_LUXURY_SCORE_NEW_SCHEMA C
ON A.MEMBER_NUMBER = C.MEMBER_NUMBER
LEFT JOIN VPM_BACKUP.GCS_CUST_HOTEL_LUXURY_SCORE_NEW_SCHEMA D
ON A.MEMBER_NUMBER = D.MEMBER_NUMBER
LEFT JOIN VPM_BACKUP.GCS_CUST_SHOPPING_LUXURY_SCORE_NEW_SCHEMA E
ON A.MEMBER_NUMBER = E.MEMBER_NUMBER
LEFT JOIN VPM_BACKUP.GCS_PART1_DINING_INFO_NEW_SCHEMA F
ON A.MEMBER_NUMBER = F.MEMBER_NUMBER
LEFT JOIN VPM_Backup.GCS_PART2_GAS_TRANSP_INFO_NEW_SCHEMA G
ON A.MEMBER_NUMBER = G.MEMBER_NUMBER
LEFT JOIN VPM_BACKUP.GCS_PART3_HOUSEHOLD_INFO_NEW_SCHEMA H
ON A.MEMBER_NUMBER = H.MEMBER_NUMBER
LEFT JOIN VPM_BACKUP.GCS_PART4_TRAVEL_INFO_NEW_SCHEMA I
ON A.MEMBER_NUMBER = I.MEMBER_NUMBER
LEFT JOIN VPM_BACKUP.GCS_PART5_SHOPPING_INFO_NEW_SCHEMA J
ON A.MEMBER_NUMBER = J.MEMBER_NUMBER
LEFT JOIN VPM_BACKUP.GCS_PART6_HEALTH_INFO_NEW_SCHEMA K
ON A.MEMBER_NUMBER = K.MEMBER_NUMBER
LEFT JOIN VPM_BACKUP.GCS_PART7_KIDS_INFO_NEW_SCHEMA L
ON A.MEMBER_NUMBER = L.MEMBER_NUMBER
LEFT JOIN VPM_BACKUP.GCS_PART8_LEISURE_INFO_NEW_SCHEMA M
ON A.MEMBER_NUMBER = M.MEMBER_NUMBER
LEFT JOIN VPM_BACKUP.GCS_PART9_INS_INFO_NEW_SCHEMA N
ON A.MEMBER_NUMBER = N.MEMBER_NUMBER
LEFT JOIN VPM_BACKUP.GCS_CUST_SVOC_CATEGORY_SPENDING_NEW_SCHEMA O
ON A.MEMBER_NUMBER = O.MEMBER_NUMBER
;

UPDATE VPM_Backup.GCS_CUST_SVOC_SEGMENT_NEW_SCHEMA
SET LUXURY_SCORE = CASE WHEN (CASE WHEN DINING_LUXURY_PERCENTILE_SCORE IS NULL THEN 0 ELSE 1 END+
					CASE WHEN HOSPITAL_LUXURY_PERCENTILE_SCORE IS NULL THEN 0 ELSE 1 END+
					CASE WHEN HOTEL_LUXURY_PERCENTILE_SCORE IS NULL THEN 0 ELSE 1 END+
					CASE WHEN SHOPPING_LUXURY_PERCENTILE_SCORE IS NULL THEN 0 ELSE 1 END+
					CASE WHEN CAR_USAGE_SEGMENT = '' THEN 0 ELSE 1 END+
					CASE WHEN LEISURE_SEGMENT = '' THEN 0 ELSE 1 END) > 0 THEN
					
					(COALESCE(DINING_LUXURY_PERCENTILE_SCORE,0)+
					COALESCE(HOSPITAL_LUXURY_PERCENTILE_SCORE,0)+
					COALESCE(HOTEL_LUXURY_PERCENTILE_SCORE,0)+
					COALESCE(SHOPPING_LUXURY_PERCENTILE_SCORE,0)+
					COALESCE(CASE WHEN CAR_USAGE_SEGMENT = 'A.LUXURY CAR' THEN 100 END,0)+
					COALESCE(CASE WHEN LEISURE_SEGMENT = 'A.LUXURY LEISURE' THEN 100 END,0))*1.0
					/(CASE WHEN DINING_LUXURY_PERCENTILE_SCORE IS NULL THEN 0 ELSE 1 END+
					CASE WHEN HOSPITAL_LUXURY_PERCENTILE_SCORE IS NULL THEN 0 ELSE 1 END+
					CASE WHEN HOTEL_LUXURY_PERCENTILE_SCORE IS NULL THEN 0 ELSE 1 END+
					CASE WHEN SHOPPING_LUXURY_PERCENTILE_SCORE IS NULL THEN 0 ELSE 1 END+
					CASE WHEN CAR_USAGE_SEGMENT = '' THEN 0 ELSE 1 END+
					CASE WHEN LEISURE_SEGMENT = '' THEN 0 ELSE 1 END)*1.0					
					ELSE 0 END,
	NO_OF_CREDITCARD_ALL = COALESCE(NO_OF_CREDITCARD_ALL,1)
;

UPDATE VPM_Backup.GCS_CUST_SVOC_SEGMENT_NEW_SCHEMA
SET INSURANCE_PREMIUM = T.INS_PREMIUM,
	INVESTMENT_AMOUNT = T.INVESTMENT
FROM (	SELECT MEMBER_NUMBER,
			SUM(CASE WHEN MERCHANT_LEVEL3 = 'INSURANCE' THEN SPENDING_AMOUNT ELSE 0 END) AS INS_PREMIUM,   
			SUM(CASE WHEN MERCHANT_LEVEL3 = 'INVESTMENT' THEN SPENDING_AMOUNT ELSE 0 END) AS INVESTMENT 
		FROM (SELECT * FROM VPM_BACKUP.GCS_TXN_1YR_NEW_SCHEMA WHERE PRIMARY_CARD_FLAG = 1) A
		GROUP BY MEMBER_NUMBER) T
WHERE VPM_Backup.GCS_CUST_SVOC_SEGMENT_NEW_SCHEMA.MEMBER_NUMBER = T.MEMBER_NUMBER
;

UPDATE VPM_Backup.GCS_CUST_SVOC_SEGMENT_NEW_SCHEMA
SET CAR_OWNERSHIPS = T.CNT_CAR,
	EUROPE_CAR_OWNERSHIPS = T.CNT_EUROPE_CAR
FROM (SELECT MEMBER_NUMBER, 1 AS CNT_CAR, 
			 (CASE WHEN CAR_USAGE_SEGMENT = 'A.LUXURY CAR' THEN 1 ELSE 0 END) AS CNT_EUROPE_CAR
	  FROM VPM_Backup.GCS_PART2_GAS_TRANSP_INFO_NEW_SCHEMA
	  WHERE CAR_USAGE_SEGMENT <> 'G.RARELY DRIVE' AND CAR_USAGE_SEGMENT IS NOT NULL) T
WHERE VPM_Backup.GCS_CUST_SVOC_SEGMENT_NEW_SCHEMA.MEMBER_NUMBER = T.MEMBER_NUMBER
;

DROP TABLE IF EXISTS TEMP_CAR_OWN_BRAND_NEW_SCHEMA;
SELECT MEMBER_NUMBER,CAR_BRAND,
		   COUNT(DISTINCT CASE WHEN MERCHANT_LEVEL3 = 'EUROPE CAR DEALER' THEN 1 ELSE NULL END) AS CNT_EUROPE_CAR
INTO TEMP TABLE TEMP_CAR_OWN_BRAND_NEW_SCHEMA
FROM VPM_BACKUP.GCS_TXN_1YR_NEW_SCHEMA
WHERE PRIMARY_CARD_FLAG = 1
AND MERCHANT_LEVEL2 IN ('GAS','AUTOMOTIVE')
GROUP BY MEMBER_NUMBER,CAR_BRAND
ORDER BY MEMBER_NUMBER,CNT_EUROPE_CAR DESC
;
	
UPDATE VPM_Backup.GCS_CUST_SVOC_SEGMENT_NEW_SCHEMA
SET CAR_OWNERSHIPS = T.CNT_CAR,
	EUROPE_CAR_OWNERSHIPS = T.CNT_EUROPE_CAR
FROM   (SELECT MEMBER_NUMBER,COUNT(DISTINCT CAR_BRAND) AS CNT_CAR,SUM(CNT_EUROPE_CAR) AS CNT_EUROPE_CAR
		FROM TEMP_CAR_OWN_BRAND_NEW_SCHEMA
		WHERE COALESCE(CAR_BRAND,'OTHERS') <> 'OTHERS'
		GROUP BY MEMBER_NUMBER) T
WHERE VPM_Backup.GCS_CUST_SVOC_SEGMENT_NEW_SCHEMA.MEMBER_NUMBER = T.MEMBER_NUMBER
;

UPDATE VPM_Backup.GCS_CUST_SVOC_SEGMENT_NEW_SCHEMA
SET CAR_BRAND = T.CAR_BRAND
FROM   (SELECT MEMBER_NUMBER,CAR_BRAND,CNT_EUROPE_CAR
		FROM TEMP_CAR_OWN_BRAND_NEW_SCHEMA
		WHERE CNT_EUROPE_CAR = 0 AND COALESCE(CAR_BRAND,'OTHERS') <> 'OTHERS') T
WHERE VPM_Backup.GCS_CUST_SVOC_SEGMENT_NEW_SCHEMA.MEMBER_NUMBER = T.MEMBER_NUMBER
;

UPDATE VPM_Backup.GCS_CUST_SVOC_SEGMENT_NEW_SCHEMA
SET CARD_DESC = T.CARD_DESC,
	BANK = 'CENTRAL'
FROM   (SELECT DISTINCT MEMBER_NUMBER, CARD_DESC
		FROM VPM_DATA.PM_CUSTOMER_CC_NEW_SCHEMA
		WHERE ISSUING_BANK LIKE 'CENTRAL%' AND CARD_DESC IS NOT NULL AND CARD_DESC NOT LIKE '%FIRST CHOICE%') T
WHERE VPM_Backup.GCS_CUST_SVOC_SEGMENT_NEW_SCHEMA.MEMBER_NUMBER = T.MEMBER_NUMBER
AND VPM_Backup.GCS_CUST_SVOC_SEGMENT_NEW_SCHEMA.CARD_DESC IS NULL
;

/*** MAP CREDIT CARD HOLDING (MINIMUM DEPOSIT/INCOME) ***/

UPDATE VPM_Backup.GCS_CUST_SVOC_SEGMENT_NEW_SCHEMA
SET TRAVEL_SCORE = T.CAT_SCORE_ADJUST,
	TRAVEL_SPENDING = T.TOTAL_AMT
FROM VPM_Backup.GCS_CC_TXN_RETAIL_STAT_RANK_ALL_LV2_NEW_SCHEMA T
WHERE VPM_Backup.GCS_CUST_SVOC_SEGMENT_NEW_SCHEMA.MEMBER_NUMBER = T.MEMBER_NUMBER 
AND T.MERCHANT_LEVEL2 = 'TRAVEL'
;

UPDATE VPM_Backup.GCS_CUST_SVOC_SEGMENT_NEW_SCHEMA
SET DINING_SCORE = T.CAT_SCORE_ADJUST,
	DINING_SPENDING = T.TOTAL_AMT
FROM VPM_Backup.GCS_CC_TXN_RETAIL_STAT_RANK_ALL_LV2_NEW_SCHEMA T
WHERE VPM_Backup.GCS_CUST_SVOC_SEGMENT_NEW_SCHEMA.MEMBER_NUMBER = T.MEMBER_NUMBER 
AND T.MERCHANT_LEVEL2 = 'DINING'
;

UPDATE VPM_Backup.GCS_CUST_SVOC_SEGMENT_NEW_SCHEMA
SET EDUCATION_SCORE = T.CAT_SCORE_ADJUST,
	EDUCATION_SPENDING = T.TOTAL_AMT
FROM VPM_Backup.GCS_CC_TXN_RETAIL_STAT_RANK_ALL_LV2_NEW_SCHEMA T
WHERE VPM_Backup.GCS_CUST_SVOC_SEGMENT_NEW_SCHEMA.MEMBER_NUMBER = T.MEMBER_NUMBER 
AND T.MERCHANT_LEVEL2 = 'EDUCATION'
;

UPDATE VPM_Backup.GCS_CUST_SVOC_SEGMENT_NEW_SCHEMA
SET BOOK_AND_STATIONARY_SCORE = T.CAT_SCORE_ADJUST,
	BOOK_AND_STATIONARY_SPENDING = T.TOTAL_AMT
FROM VPM_Backup.GCS_CC_TXN_RETAIL_STAT_RANK_ALL_LV3_NEW_SCHEMA T
WHERE VPM_Backup.GCS_CUST_SVOC_SEGMENT_NEW_SCHEMA.MEMBER_NUMBER = T.MEMBER_NUMBER 
AND T.MERCHANT_LEVEL3 = 'BOOKSTORE AND STATIONARY'
;

UPDATE VPM_Backup.GCS_CUST_SVOC_SEGMENT_NEW_SCHEMA
SET PERSONAL_CARE_SCORE = T.CAT_SCORE_ADJUST,
	PERSONAL_CARE_SPENDING = T.TOTAL_AMT
FROM VPM_Backup.GCS_CC_TXN_RETAIL_STAT_RANK_ALL_LV2_NEW_SCHEMA T
WHERE VPM_Backup.GCS_CUST_SVOC_SEGMENT_NEW_SCHEMA.MEMBER_NUMBER = T.MEMBER_NUMBER 
AND T.MERCHANT_LEVEL2 = 'PERSONAL CARE'
;

UPDATE VPM_Backup.GCS_CUST_SVOC_SEGMENT_NEW_SCHEMA
SET HOSPITAL_AND_DRUG_SCORE = T.CAT_SCORE_ADJUST,
	HOSPITAL_AND_DRUG_SPENDING = T.TOTAL_AMT
FROM VPM_Backup.GCS_CC_TXN_RETAIL_STAT_RANK_ALL_LV2_NEW_SCHEMA T
WHERE VPM_Backup.GCS_CUST_SVOC_SEGMENT_NEW_SCHEMA.MEMBER_NUMBER = T.MEMBER_NUMBER 
AND T.MERCHANT_LEVEL2 = 'HOSPITAL/DRUG STORE'
;

UPDATE VPM_Backup.GCS_CUST_SVOC_SEGMENT_NEW_SCHEMA
SET HOME_IMPROVEMENT_SCORE = T.CAT_SCORE_ADJUST,
	HOME_IMPROVEMENT_SPENDING = T.TOTAL_AMT
FROM VPM_Backup.GCS_CC_TXN_RETAIL_STAT_RANK_ALL_LV2_NEW_SCHEMA T
WHERE VPM_Backup.GCS_CUST_SVOC_SEGMENT_NEW_SCHEMA.MEMBER_NUMBER = T.MEMBER_NUMBER 
AND T.MERCHANT_LEVEL2 = 'HOME IMPROVEMENT'
;

UPDATE VPM_Backup.GCS_CUST_SVOC_SEGMENT_NEW_SCHEMA
SET FASHION_SCORE = T.CAT_SCORE_ADJUST,
	FASHION_SPENDING = T.TOTAL_AMT
FROM (SELECT MEMBER_NUMBER, MAX(CAT_SCORE_ADJUST) AS CAT_SCORE_ADJUST, SUM(TOTAL_AMT) AS TOTAL_AMT
	  FROM VPM_Backup.GCS_CC_TXN_RETAIL_STAT_RANK_ALL_LV3_NEW_SCHEMA
	  WHERE MERCHANT_LEVEL3 IN ('APPAREL/LEATHERS','SPORT GOODS')
	  GROUP BY MEMBER_NUMBER) T
WHERE VPM_Backup.GCS_CUST_SVOC_SEGMENT_NEW_SCHEMA.MEMBER_NUMBER = T.MEMBER_NUMBER 
;

UPDATE VPM_Backup.GCS_CUST_SVOC_SEGMENT_NEW_SCHEMA
SET GROCERY_SCORE = T.CAT_SCORE_ADJUST,
	GROCERY_SPENDING = T.TOTAL_AMT
FROM (SELECT MEMBER_NUMBER, MAX(CAT_SCORE_ADJUST) AS CAT_SCORE_ADJUST, SUM(TOTAL_AMT) AS TOTAL_AMT
	  FROM VPM_Backup.GCS_CC_TXN_RETAIL_STAT_RANK_ALL_LV2_NEW_SCHEMA
	  WHERE MERCHANT_LEVEL1 = 'HYPERMARKET/GROCERY'
	  GROUP BY MEMBER_NUMBER) T
WHERE VPM_Backup.GCS_CUST_SVOC_SEGMENT_NEW_SCHEMA.MEMBER_NUMBER = T.MEMBER_NUMBER 
;

UPDATE VPM_Backup.GCS_CUST_SVOC_SEGMENT_NEW_SCHEMA
SET BEAUTY_SCORE = T.CAT_SCORE_ADJUST,
	BEAUTY_SPENDING = T.TOTAL_AMT
FROM VPM_Backup.GCS_CC_TXN_RETAIL_STAT_RANK_ALL_LV3_NEW_SCHEMA T
WHERE VPM_Backup.GCS_CUST_SVOC_SEGMENT_NEW_SCHEMA.MEMBER_NUMBER = T.MEMBER_NUMBER 
AND T.MERCHANT_LEVEL3 = 'COSMETICS AND BEAUTY'
;

UPDATE VPM_Backup.GCS_CUST_SVOC_SEGMENT_NEW_SCHEMA
SET ELECTRONICS_SCORE = T.CAT_SCORE_ADJUST,
	ELECTRONICS_SPENDING = T.TOTAL_AMT
FROM (SELECT MEMBER_NUMBER, MAX(CAT_SCORE_ADJUST) AS CAT_SCORE_ADJUST, SUM(TOTAL_AMT) AS TOTAL_AMT
	  FROM VPM_Backup.GCS_CC_TXN_RETAIL_STAT_RANK_ALL_LV2_NEW_SCHEMA
	  WHERE MERCHANT_LEVEL2 IN ('HOME APPLIANCE','ELECTRONICS/IT')
	  GROUP BY MEMBER_NUMBER) T
WHERE VPM_Backup.GCS_CUST_SVOC_SEGMENT_NEW_SCHEMA.MEMBER_NUMBER = T.MEMBER_NUMBER 
;

UPDATE VPM_Backup.GCS_CUST_SVOC_SEGMENT_NEW_SCHEMA
SET LEISURE_ACTIVITY_SCORE = T.CAT_SCORE_ADJUST,
	LEISURE_ACTIVITY_SPENDING = T.TOTAL_AMT
FROM (SELECT MEMBER_NUMBER, MAX(CAT_SCORE_ADJUST) AS CAT_SCORE_ADJUST, SUM(TOTAL_AMT) AS TOTAL_AMT
	  FROM VPM_Backup.GCS_CC_TXN_RETAIL_STAT_RANK_ALL_LV2_NEW_SCHEMA
	  WHERE MERCHANT_LEVEL2 = 'LEISURE ACTIVITY'
	  GROUP BY MEMBER_NUMBER) T
WHERE VPM_Backup.GCS_CUST_SVOC_SEGMENT_NEW_SCHEMA.MEMBER_NUMBER = T.MEMBER_NUMBER
;

UPDATE VPM_DATA.SALESSKU_LOG_TIME SET end_time = current_timestamp WHERE task = 'GCS Step2.4 - GCS Affinity and GCS SVOC' AND EXTRACT(MONTH FROM start_time) = EXTRACT(MONTH FROM current_timestamp)
;

