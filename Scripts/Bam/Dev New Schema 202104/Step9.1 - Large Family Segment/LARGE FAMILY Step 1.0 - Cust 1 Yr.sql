--*********************************************home_electronics*************************************************--
--*****big size*****--
INSERT INTO VPM_DATA.SALESSKU_LOG_TIME(task,start_time) VALUES ('LARGE FAMILY Step 1.0 - Cust 1 Yr',CURRENT_TIMESTAMP)
;

--drop table if exists TMP_rice_cooker;
select distinct MEMBER_NUMBER 
INTO TEMP TABLE TMP_rice_cooker
from vpm_data.pm_skutagging_electronics_salessku_3yr_NEW_SCHEMA 
where type_final = 'RICE COOKER' and  (cast(size_final as float) >= 1.8) and COALESCE(MEMBER_NUMBER,'') <> ''
;

--drop table if exists TMP_refrigerator;
select distinct MEMBER_NUMBER 
INTO TEMP TABLE TMP_refrigerator
from vpm_data.pm_skutagging_electronics_salessku_3yr_NEW_SCHEMA 
where type_final = 'REFRIGERATOR' and  (cast(size_final as float) >= 20.0) and COALESCE(MEMBER_NUMBER,'') <> ''
;

--*****high quantity*****--
--drop table if exists TMP_elec_qty;
select MEMBER_NUMBER,type_final, sum(qty) as total_qty 
INTO TEMP TABLE TMP_elec_qty
from vpm_data.pm_skutagging_electronics_salessku_3yr_NEW_SCHEMA
where revised_category_level_1 = 'HOME ELECTRONICS' and COALESCE(MEMBER_NUMBER,'') <> ''
group by MEMBER_NUMBER,type_final
;

--drop table if exists TMP_elec_qty2;
select MEMBER_NUMBER,type_final, sum(qty) as total_qty 
INTO TEMP TABLE TMP_elec_qty2
from vpm_data.pm_skutagging_electronics_salessku_3yr_NEW_SCHEMA
where revised_category_level_1 = 'HOME ELECTRONICS' and type_final in ('REFRIGERATOR') and (cast(size_final as float) >= 2.0) and COALESCE(MEMBER_NUMBER,'') <> ''
group by MEMBER_NUMBER,type_final
;

--drop table if exists TMP_elec_morethan3_qty;
select distinct MEMBER_NUMBER
INTO TEMP TABLE TMP_elec_morethan3_qty
from TMP_elec_qty
where type_final in ('AIR CONDITIONER','AIR PURIFIER','FAN','TV','WATER HEATER') and total_qty > 3 and COALESCE(MEMBER_NUMBER,'') <> ''
;

--drop table if exists TMP_elec_washer_qty;
select distinct MEMBER_NUMBER
INTO TEMP TABLE TMP_elec_washer_qty
from TMP_elec_qty
where type_final in ('WASHER & DRYER') and total_qty > 1 and COALESCE(MEMBER_NUMBER,'') <> ''
;

--drop table if exists TMP_elec_refrigerator_qty;
select distinct MEMBER_NUMBER
INTO TEMP TABLE TMP_elec_refrigerator_qty
from TMP_elec_qty2
where type_final in ('REFRIGERATOR') and total_qty > 1 and COALESCE(MEMBER_NUMBER,'') <> ''
;

--drop table if exists TMP_a_large_family_electronics_3yr_cust;
select distinct MEMBER_NUMBER
INTO TEMP TABLE TMP_a_large_family_electronics_3yr_cust
from TMP_rice_cooker
union
select distinct MEMBER_NUMBER
from TMP_refrigerator
union
select distinct MEMBER_NUMBER
from TMP_elec_morethan3_qty
union
select distinct MEMBER_NUMBER
from TMP_elec_washer_qty
union
select distinct MEMBER_NUMBER
from TMP_elec_refrigerator_qty
;


ALTER TABLE TMP_a_large_family_electronics_3yr_cust ADD COLUMN rice_cooker_bigsize_flag int;
ALTER TABLE TMP_a_large_family_electronics_3yr_cust ADD column refrigerator_bigsize_flag int;

update TMP_a_large_family_electronics_3yr_cust
set rice_cooker_bigsize_flag = case when (MEMBER_NUMBER in (select distinct MEMBER_NUMBER from TMP_rice_cooker)) then 1 else 0 end
   ,refrigerator_bigsize_flag = case when (MEMBER_NUMBER in (select distinct MEMBER_NUMBER from TMP_refrigerator)) then 1 else 0 end
;


alter table TMP_a_large_family_electronics_3yr_cust ADD column home_elec_morethan3_flag int;
alter table TMP_a_large_family_electronics_3yr_cust ADD column washer_dryer_morethan1_flag int;
alter table TMP_a_large_family_electronics_3yr_cust ADD column refrigerator_morethan1_flag int;

update TMP_a_large_family_electronics_3yr_cust
set home_elec_morethan3_flag = case when (MEMBER_NUMBER in (select distinct MEMBER_NUMBER from TMP_elec_morethan3_qty)) then 1 else 0 end
   ,washer_dryer_morethan1_flag = case when (MEMBER_NUMBER in (select distinct MEMBER_NUMBER from TMP_elec_washer_qty)) then 1 else 0 end
   ,refrigerator_morethan1_flag = case when (MEMBER_NUMBER in (select distinct MEMBER_NUMBER from TMP_elec_refrigerator_qty)) then 1 else 0 end
;


alter table TMP_a_large_family_electronics_3yr_cust
add home_elec_bigsize_highquantity_flag int
;

update TMP_a_large_family_electronics_3yr_cust
set home_elec_bigsize_highquantity_flag = case when (rice_cooker_bigsize_flag = 1 or refrigerator_bigsize_flag = 1 or home_elec_morethan3_flag = 1 or washer_dryer_morethan1_flag = 1 or refrigerator_morethan1_flag = 1) then 1 else 0 end
;

        
--*********************************************home_non_electronics*************************************************--
--drop table if exists TMP_bed_qty;
select MEMBER_NUMBER,type_final, sum(qty) as total_qty 
INTO TEMP TABLE TMP_bed_qty
from vpm_data.pm_home_product_salessku_1yr_NEW_SCHEMA
group by MEMBER_NUMBER,type_final
;

--drop table if exists TMP_bed;
select distinct MEMBER_NUMBER
INTO TEMP TABLE TMP_bed
from TMP_bed_qty
where type_final in ('BED','MATTRESS & ACCESSORIES','DRESSING TABLE') and total_qty >= 3 and COALESCE(MEMBER_NUMBER,'') <> ''
;

--drop table if exists TMP_bed_accessory;
select distinct MEMBER_NUMBER
INTO TEMP TABLE TMP_bed_accessory
from TMP_bed_qty
where type_final in ('BEDLINEN','BEDROOM SET','PILLOW') and total_qty >= 6 and COALESCE(MEMBER_NUMBER,'') <> ''
;

--drop table if exists TMP_a_large_family_home_non_cust;
select distinct MEMBER_NUMBER
INTO TEMP TABLE TMP_a_large_family_home_non_cust
from TMP_bed
union
select distinct MEMBER_NUMBER
from TMP_bed_accessory
;


alter table TMP_a_large_family_home_non_cust add COLUMN bed_morethan2_flag int;
alter table TMP_a_large_family_home_non_cust add COLUMN bed_accessory_morethan5_flag int;

update TMP_a_large_family_home_non_cust
set bed_morethan2_flag = case when (MEMBER_NUMBER in (select distinct MEMBER_NUMBER from TMP_bed)) then 1 else 0 end
   ,bed_accessory_morethan5_flag = case when (MEMBER_NUMBER in (select distinct MEMBER_NUMBER from TMP_bed_accessory)) then 1 else 0 end
   ;


alter table TMP_a_large_family_home_non_cust
add home_non_elec_highquantity_flag int
;

update TMP_a_large_family_home_non_cust
set home_non_elec_highquantity_flag = case when (bed_morethan2_flag = 1 or bed_accessory_morethan5_flag = 1) then 1 else 0 end
;

--*********************************************kids*************************************************--
--drop table if exists TMP_a_large_family_kids_3stage_and_declared_cust;
select MEMBER_NUMBER
               ,kid_infant
           ,kid_toddler
		   ,kid_presch
		   ,kid_junior
		   ,kid_teenage
		   ,case when (coalesce(kid_infant,0) + coalesce(kid_toddler,0) + coalesce(kid_presch,0) + coalesce(kid_junior,0) + coalesce(kid_teenage,0)) >= 3 then 1 else 0 end as kid_3stage_valid_flag
		   ,noofchildren  
INTO TEMP TABLE TMP_a_large_family_kids_3stage_and_declared_cust
from vpm_data.pm_svoc_kids_new A INNER JOIN ANALYSIS_DATA.CUSTOMER_SBL B ON A.CUSTOMER_ID = B.CUSTOMERID
where (coalesce(kid_infant,0) + coalesce(kid_toddler,0) + coalesce(kid_presch,0) + coalesce(kid_junior,0) + coalesce(kid_teenage,0)) >= 3 
UNION
select MEMBER_NUMBER
               ,kid_infant
           ,kid_toddler
		   ,kid_presch
		   ,kid_junior
		   ,kid_teenage
		   ,case when (coalesce(kid_infant,0) + coalesce(kid_toddler,0) + coalesce(kid_presch,0) + coalesce(kid_junior,0) + coalesce(kid_teenage,0)) >= 3 then 1 else 0 end as kid_3stage_valid_flag
		   ,noofchildren  
from vpm_data.pm_svoc_kids_new A INNER JOIN ANALYSIS_DATA.CUSTOMER_SBL B ON A.CUSTOMER_ID = B.CUSTOMERID
WHERE noofchildren::INT >= 3 AND noofchildren <> ''
;
--*********************************************fashion*************************************************-- 
--drop table if exists TMP_fashion_sale ;
 select a.MEMBER_NUMBER
      ,a.qty
	  ,case when a.revised_category_level_1 = 'KIDS' then 'KIDS' else coalesce(a.revised_category_level_2,'OTHER') end as men_female_unisex_kid
 INTO TEMP TABLE TMP_fashion_sale 
  from vpm_data.pm_all_salessku_1yr_NEW_SCHEMA a
  where a.revised_category_level_1 = 'FASHION' or (a.revised_category_level_1 = 'KIDS' and a.revised_category_level_2 = 'KID FASHION') and COALESCE(MEMBER_NUMBER,'') <> ''
  union all 
 select a.MEMBER_NUMBER
      ,a.qty
	  ,case when a.revised_category_level_1 = 'KIDS' then 'KIDS' else coalesce(a.revised_category_level_2,'OTHER') end as men_female_unisex_kid
	  from vpm_data.pm_tops_salessku_1yr_NEW_SCHEMA a
      where a.revised_category_level_1 = 'FASHION' or (a.revised_category_level_1 = 'KIDS' and a.revised_category_level_2 = 'KID FASHION') and COALESCE(MEMBER_NUMBER,'') <> ''
	   union all 
      select a.MEMBER_NUMBER
      ,a.qty
	  ,case when a.revised_category_level_1 = 'KIDS' then 'KIDS' else coalesce(a.revised_category_level_2,'OTHER') end as men_female_unisex_kid
	  from vpm_data.pm_cfm_salessku_1yr_NEW_SCHEMA a
      where a.revised_category_level_1 = 'FASHION' or (a.revised_category_level_1 = 'KIDS' and a.revised_category_level_2 = 'KID FASHION') and COALESCE(MEMBER_NUMBER,'') <> ''
;

--drop table if exists TMP_fashion_kids;
select distinct MEMBER_NUMBER 
INTO TEMP TABLE TMP_fashion_kids
from TMP_fashion_sale
where men_female_unisex_kid = 'KIDS'
group by MEMBER_NUMBER
having sum(qty) >= 3
;

--drop table if exists TMP_fashion_women;
select distinct MEMBER_NUMBER 
INTO TEMP TABLE TMP_fashion_women
from TMP_fashion_sale
where men_female_unisex_kid = 'WOMEN' 
group by MEMBER_NUMBER
having sum(qty) >= 3
;

--drop table if exists TMP_fashion_men;
select distinct MEMBER_NUMBER 
INTO TEMP TABLE TMP_fashion_men
from TMP_fashion_sale
where men_female_unisex_kid = 'MEN' 
group by MEMBER_NUMBER
having sum(qty) >= 3
;

--drop table if exists TMP_fashion_unisex;
select distinct MEMBER_NUMBER 
INTO TEMP TABLE TMP_fashion_unisex
from TMP_fashion_sale
where men_female_unisex_kid = 'UNISEX'
group by MEMBER_NUMBER
having sum(qty) >= 3
;

--drop table if exists TMP_fashion_various_gender_cust;
select distinct a.MEMBER_NUMBER
INTO TEMP TABLE TMP_fashion_various_gender_cust
from TMP_fashion_kids a
	 inner join TMP_fashion_women b on a.MEMBER_NUMBER = b.MEMBER_NUMBER
	 inner join TMP_fashion_men c on a.MEMBER_NUMBER = c.MEMBER_NUMBER
	 inner join TMP_fashion_unisex d on a.MEMBER_NUMBER = d.MEMBER_NUMBER
;

--*********************************************agg table*************************************************--
drop table if exists TMP_sales_all;
select a.MEMBER_NUMBER
      ,a.txn_date
      ,a.ticket_num
      ,a.txn_amt
      ,a.qty
      ,a.revised_category_level_1
      ,a.revised_category_level_3
INTO TEMP TABLE TMP_sales_all
from vpm_data.pm_all_salessku_1yr_NEW_SCHEMA a
where coalesce(txn_amt,0) <> 0 
	  and revised_category_level_1 in ('FOOD GROCERY','HOME GROCERY','HBA','HOME NON-ELECTRONICS') and COALESCE(MEMBER_NUMBER,'') <> ''
union all
select a.MEMBER_NUMBER
      ,a.txn_date
      ,a.ticket_num
      ,a.txn_amt
      ,a.qty
      ,a.revised_category_level_1
      ,a.revised_category_level_3
from vpm_data.pm_cfm_salessku_1yr_NEW_SCHEMA a
where coalesce(txn_amt,0) <> 0 
      and revised_category_level_1 in ('FOOD GROCERY','HOME GROCERY','HBA','HOME NON-ELECTRONICS') and COALESCE(MEMBER_NUMBER,'') <> ''
union all
select a.MEMBER_NUMBER
      ,a.txn_date
      ,a.ticket_num
      ,a.txn_amt
      ,a.qty
      ,a.revised_category_level_1
      ,a.revised_category_level_3
from vpm_data.pm_tops_salessku_1yr_NEW_SCHEMA a
where coalesce(txn_amt,0) <> 0 
	  and revised_category_level_1 in ('FOOD GROCERY','HOME GROCERY','HBA','HOME NON-ELECTRONICS') and COALESCE(MEMBER_NUMBER,'') <> ''
;

--drop table if exists pm_large_family_cat1_agg_1yr_NEW_SCHEMA;
select MEMBER_NUMBER
      ,coalesce(revised_category_level_1,'OTHER') as revised_category_level_1
	  ,sum(txn_amt) as total_spending
      ,count(distinct ticket_num) as total_ticket
      ,sum(qty) as total_qty
      ,count(distinct txn_date) as total_day_visit
	  ,count(distinct revised_category_level_3) as total_product_variety
into TEMP TABLE pm_large_family_cat1_agg_1yr_NEW_SCHEMA
from TMP_sales_all
group by MEMBER_NUMBER
	  ,coalesce(revised_category_level_1,'OTHER')
;

--drop table if exists TMP_foodgrocery1;
select distinct MEMBER_NUMBER
INTO TEMP TABLE TMP_foodgrocery1
from pm_large_family_cat1_agg_1yr_NEW_SCHEMA
where revised_category_level_1 = 'FOOD GROCERY' and total_qty >= 260 and total_day_visit >= 15 
	and (case when total_spending >=16000 then 1 else 0 end + case when total_day_visit >= 30 then 1 else 0 end + case when total_product_variety >= 22 then 1 else 0 end >=2) and COALESCE(MEMBER_NUMBER,'') <> ''
;

--drop table if exists TMP_homegrocery1;
select distinct MEMBER_NUMBER
INTO TEMP TABLE TMP_homegrocery1
from pm_large_family_cat1_agg_1yr_NEW_SCHEMA
where revised_category_level_1 = 'HOME GROCERY' and total_qty >= 40 and total_day_visit >= 5 
	and (case when total_spending >=3000 then 1 else 0 end + case when total_day_visit >= 10 then 1 else 0 end + case when total_product_variety >= 4 then 1 else 0 end >=2) and COALESCE(MEMBER_NUMBER,'') <> ''
;

--drop table if exists TMP_hba1;
select distinct MEMBER_NUMBER
INTO TEMP TABLE TMP_hba1
from pm_large_family_cat1_agg_1yr_NEW_SCHEMA
where revised_category_level_1 = 'HBA' and total_qty >= 30 and total_day_visit >= 5 
	and (case when total_spending >=3000 then 1 else 0 end + case when total_day_visit >= 10 then 1 else 0 end + case when total_product_variety >= 4 then 1 else 0 end >=2) and COALESCE(MEMBER_NUMBER,'') <> ''
;

--drop table if exists TMP_homenon1;
select distinct MEMBER_NUMBER
INTO TEMP TABLE TMP_homenon1
from pm_large_family_cat1_agg_1yr_NEW_SCHEMA
where revised_category_level_1 = 'HOME NON-ELECTRONICS' and total_qty >= 20 
	and (case when total_spending >=7000 then 1 else 0 end + case when total_day_visit >= 5 then 1 else 0 end + case when total_product_variety >= 4 then 1 else 0 end >=2) and COALESCE(MEMBER_NUMBER,'') <> ''
;

--drop table if exists TMP_affinity_cust;
select distinct MEMBER_NUMBER
INTO TEMP TABLE TMP_affinity_cust
from TMP_foodgrocery1
union
select distinct MEMBER_NUMBER
from TMP_homegrocery1
union
select distinct MEMBER_NUMBER
from TMP_hba1
union
select distinct MEMBER_NUMBER
from TMP_homenon1
;


alter table TMP_affinity_cust add COLUMN selected_food_grocery_flag int;
alter table TMP_affinity_cust ADD COLUMN selected_hba_flag int;
alter table TMP_affinity_cust ADD COLUMN selected_home_grocery_flag int;
alter table TMP_affinity_cust aDD COLUMN selected_homenon_flag int;

update TMP_affinity_cust
set selected_food_grocery_flag = case when (MEMBER_NUMBER in (select distinct MEMBER_NUMBER from TMP_foodgrocery1)) then 1 else 0 end
   ,selected_hba_flag = case when (MEMBER_NUMBER in (select distinct MEMBER_NUMBER from TMP_hba1)) then 1 else 0 end
   ,selected_home_grocery_flag = case when (MEMBER_NUMBER in (select distinct MEMBER_NUMBER from TMP_homegrocery1)) then 1 else 0 end
   ,selected_homenon_flag = case when (MEMBER_NUMBER in (select distinct MEMBER_NUMBER from TMP_homenon1)) then 1 else 0 end
;


--*********************************************combined large fam cust*************************************************--
drop table if exists vpm_data.pm_large_family_cust_1yr_NEW_SCHEMA
;
select distinct a.MEMBER_NUMBER
           ,coalesce(b.rice_cooker_bigsize_flag,0) as rice_cooker_bigsize_flag 
           ,coalesce(b.refrigerator_bigsize_flag,0) as refrigerator_bigsize_flag 
		   ,coalesce(b.home_elec_morethan3_flag,0) as home_elec_morethan3_flag  
           ,coalesce(b.washer_dryer_morethan1_flag,0) as washer_dryer_morethan1_flag 
		   ,coalesce(b.refrigerator_morethan1_flag,0) as refrigerator_morethan1_flag 
		   ,coalesce(b.home_elec_bigsize_highquantity_flag,0) as home_elec_bigsize_highquantity_flag 

		   ,coalesce(c.bed_morethan2_flag,0) as bed_morethan2_flag 
           ,coalesce(c.bed_accessory_morethan5_flag,0) as bed_accessory_morethan5_flag 
		   ,coalesce(c.home_non_elec_highquantity_flag,0) as home_non_elec_highquantity_flag 

		   ,coalesce(d.kid_infant,0) as kid_infant 
           ,coalesce(d.kid_toddler,0) as kid_toddler 
		   ,coalesce(d.kid_presch,0) as kid_presch 
		   ,coalesce(d.kid_junior,0) as kid_junior 
		   ,coalesce(d.kid_teenage,0) as kid_teenage 
		   ,coalesce(d.kid_3stage_valid_flag,0) as kid_3stage_valid_flag 
		   ,CASE WHEN d.noofchildren <> '' THEN coalesce(d.noofchildren::INT,0) ELSE 0 END as declared_num_children
		   ,case when (CASE WHEN d.noofchildren <> '' THEN coalesce(d.noofchildren::INT,0) ELSE 0 END) >= 3   then 1 else 0 end as declared_3_children_valid_flag
		   ,case when coalesce(d.kid_3stage_valid_flag,0) = 1 or (CASE WHEN d.noofchildren <> '' THEN coalesce(d.noofchildren::INT,0) ELSE 0 END >= 3) then 1 else 0 end as kid_3stage_and_declared_valid_flag 

		   ,case when COALESCE(E.MEMBER_NUMBER,'') <> '' then 1 else 0 end as fashion_various_gender_flag

		   ,coalesce(f.selected_food_grocery_flag,0) as selected_food_grocery_flag 
           ,coalesce(f.selected_hba_flag,0) as selected_hba_flag 
		   ,coalesce(f.selected_home_grocery_flag,0) as selected_home_grocery_flag 
		   ,coalesce(f.selected_homenon_flag,0) as selected_homenon_flag 

into vpm_data.pm_large_family_cust_1yr_NEW_SCHEMA
from (select distinct MEMBER_NUMBER from TMP_a_large_family_electronics_3yr_cust
      union 
	  select distinct MEMBER_NUMBER from TMP_a_large_family_home_non_cust
	  union 
	  select distinct MEMBER_NUMBER from TMP_a_large_family_kids_3stage_and_declared_cust
	  union
	  select distinct MEMBER_NUMBER from TMP_fashion_various_gender_cust
	  union
	  select distinct MEMBER_NUMBER from TMP_affinity_cust) a
	  left join TMP_a_large_family_electronics_3yr_cust b on a.MEMBER_NUMBER = b.MEMBER_NUMBER
	  left join TMP_a_large_family_home_non_cust c on a.MEMBER_NUMBER = c.MEMBER_NUMBER
	  left join TMP_a_large_family_kids_3stage_and_declared_cust d on a.MEMBER_NUMBER = d.MEMBER_NUMBER
	  left join TMP_fashion_various_gender_cust e on a.MEMBER_NUMBER = e.MEMBER_NUMBER
	  left join TMP_affinity_cust f on a.MEMBER_NUMBER = f.MEMBER_NUMBER
	  left join vpm_data.pm_commercial_segment_NEW_SCHEMA g on a.MEMBER_NUMBER = g.MEMBER_NUMBER
where coalesce(cg_commercial_segment,'N/A') like '%SEMI%' or coalesce(cg_commercial_segment,'N/A') = 'N/A'
;


alter table vpm_data.pm_large_family_cust_1yr_NEW_SCHEMA
add pass_affinity_criteria_flag int;

update vpm_data.pm_large_family_cust_1yr_NEW_SCHEMA
set pass_affinity_criteria_flag = case when (selected_food_grocery_flag =1 
                                               or selected_hba_flag = 1 
											   or selected_home_grocery_flag = 1
                                               or selected_homenon_flag = 1
											   or fashion_various_gender_flag = 1) then 1 else 0 end
;

alter table vpm_data.pm_large_family_cust_1yr_NEW_SCHEMA
add have_additional_sign_flag int
;

update vpm_data.pm_large_family_cust_1yr_NEW_SCHEMA
set have_additional_sign_flag = case when home_non_elec_highquantity_flag = 1 then 1 
								       when home_elec_bigsize_highquantity_flag = 1 then 1
									   when kid_3stage_and_declared_valid_flag = 1 then 1 else 0 end
;


alter table vpm_data.pm_large_family_cust_1yr_NEW_SCHEMA
add declare_profile_info_flag int
;

update vpm_data.pm_large_family_cust_1yr_NEW_SCHEMA
set declare_profile_info_flag = case when declared_num_children >= 3 then 1 else 0 end
;


----IF CURRENT MONTH IS JANUARY, THEN CREATE LARGE FAMILY TABLE AS FOR LAST YEAR
CREATE OR REPLACE PROCEDURE large_fam_create_table()
LANGUAGE plpgsql
AS $$
DECLARE
  YEAR_END TEXT;
BEGIN
  YEAR_END := (SELECT TO_CHAR(CURRENT_TIMESTAMP-INTERVAL '1 MONTH','yyyy'));

  IF TO_CHAR(CURRENT_TIMESTAMP-INTERVAL '2 MONTH','MON') = 'JAN' THEN
	EXECUTE 'CREATE TABLE VPM_DATA.PM_LARGE_FAMILY_CUST_1YR_NEW_SCHEMA_'||YEAR_END||' AS
	SELECT * FROM vpm_data.pm_large_family_cust_1yr_NEW_SCHEMA
	';
  END IF; 
END;
$$;


CALL large_fam_create_table();


UPDATE VPM_DATA.SALESSKU_LOG_TIME SET end_time = CURRENT_TIMESTAMP WHERE task = 'LARGE FAMILY Step 1.0 - Cust 1 Yr' AND EXTRACT(MONTH FROM start_time) = EXTRACT(MONTH FROM CURRENT_TIMESTAMP)
;



