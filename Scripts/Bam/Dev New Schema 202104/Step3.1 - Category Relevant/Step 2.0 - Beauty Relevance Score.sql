
----------------------------------/*  CATREV : BEAUTY */ ------------------------------------
/* BEAUTY SALE 1 YEAR */ --10mins	  
--DROP TABLE IF EXISTS TEMP_BEAUTY_1YR;
CREATE TEMP TABLE TEMP_BEAUTY_1YR AS (
	SELECT  A.MEMBER_NUMBER,
			A.TXN_AMT,
			A.TYPE_FINAL,
			A.TXN_DATE,
			DATE_PART('MONTH',A.TXN_DATE) AS MONTH_COUNT,
			CLEANED_BRANDNAME
	FROM VPM_DATA.PM_SKUTAGGING_BEAUTY_SALESSKU_1YR_NEW_SCHEMA A
	WHERE PARTNER_CODE IN ('CDS','RBS','MSL') 
		  AND  REVISED_CATEGORY_LEVEL_2 IN ('MAKE UP','TREATMENT','FRAGRANCE')
		  AND TXN_AMT>0 )
;


--DROP TABLE IF EXISTS TEMP_BEAUTY_HIGH_RELEVANCE;
CREATE TEMP TABLE TEMP_BEAUTY_HIGH_RELEVANCE AS (
	SELECT A.MEMBER_NUMBER
		    ,SUM(COALESCE(TXN_AMT,0)) AS BEAUTY_SPEND
			,COUNT(DISTINCT(TXN_DATE)) AS BEAUTY_DAY_VISIT
			,COUNT(DISTINCT MONTH_COUNT) AS BEAUTY_MONTH
		    ,COUNT(DISTINCT(TYPE_FINAL)) AS BEAUTY_PRODUCT_TYPE
		    ,CAST(NULL AS FLOAT) AS BEAUTY_SPENDING_SCORE
		    ,CAST(NULL AS FLOAT) AS BEAUTY_DAY_VISIT_SCORE
		    ,CAST(NULL AS FLOAT) AS BEAUTY_PRODUCT_TYPE_SCORE
		    ,CAST(NULL AS FLOAT) AS BEAUTY_MONTH_VISIT_SCORE
		    ,CAST(NULL AS FLOAT) AS BEAUTY_RELEVANCE_SCORE
	FROM TEMP_BEAUTY_1YR A
	GROUP BY A.MEMBER_NUMBER)
;

-- BEAUTY CUTOFF --

DROP TABLE IF EXISTS VPM_DATA.BEAUTY_HIGH_RELEVANCE_TOP20_NEW_SCHEMA
;
SELECT A.*
		,CASE WHEN  BEAUTY_SPEND > 0  THEN NTILE(100) OVER (ORDER BY BEAUTY_SPEND) END AS BEAUTY_SPEND_PERCENTILE
		,CASE WHEN BEAUTY_DAY_VISIT >  0   THEN NTILE(100) OVER (ORDER BY BEAUTY_DAY_VISIT) END AS BEAUTY_DAY_VISIT_PERCENTILE
		,CASE WHEN  BEAUTY_MONTH > 0   THEN NTILE(100) OVER (ORDER BY BEAUTY_MONTH)  END AS BEAUTY_MONTH_PERCENTILE
		,CASE WHEN  BEAUTY_PRODUCT_TYPE > 0  THEN NTILE(100) OVER (ORDER BY BEAUTY_PRODUCT_TYPE)END AS BEAUTY_PRODUCT_TYPE_PERCENTILE
INTO VPM_DATA.BEAUTY_HIGH_RELEVANCE_TOP20_NEW_SCHEMA
FROM TEMP_BEAUTY_HIGH_RELEVANCE A
INNER JOIN VPM_DATA.TOP20_NEW_SCHEMA B 
ON A.MEMBER_NUMBER =B.MEMBER_NUMBER 
LEFT JOIN (SELECT X.*,Y.MEMBER_NUMBER FROM VPM_SVOC.TOP1_SPENDER_FINAL_PM_2019 X INNER JOIN analysis_data.customer_sbl Y ON X."customer_id"=Y.customerid) C ON A.MEMBER_NUMBER=C.MEMBER_NUMBER 
LEFT JOIN VPM_BACKUP.PM_SVOC_MASTER D ON A.MEMBER_NUMBER =D.MEMBER_NUMBER 
WHERE CASE WHEN UPPER(FINAL_SUBSEGMENT_V2) LIKE '%COMMERCIAL-BEAUTY%' OR BEAUTY_COMMERCIAL_FLAG='YES' THEN 1 ELSE 0 END =0 
;


CREATE TEMP TABLE VAR_BEAUTY_CAT_REV AS
SELECT MEMBER_NUMBER,(SELECT MAX(BEAUTY_SPEND) AS BEAUTY_SPEND 
								FROM VPM_DATA.BEAUTY_HIGH_RELEVANCE_TOP20_NEW_SCHEMA
								WHERE BEAUTY_SPEND_PERCENTILE = 95) AS TOP20_P95_BEAUTY_SPEND --49988.620000
		  ,(SELECT MAX(BEAUTY_DAY_VISIT) AS BEAUTY_DAY_VISIT
								FROM VPM_DATA.BEAUTY_HIGH_RELEVANCE_TOP20_NEW_SCHEMA
								WHERE BEAUTY_DAY_VISIT_PERCENTILE = 95) AS TOP20_P95_BEAUTY_DAY_VISIT
		  ,(SELECT MAX(BEAUTY_MONTH) AS BEAUTY_MONTH
								FROM VPM_DATA.BEAUTY_HIGH_RELEVANCE_TOP20_NEW_SCHEMA
								WHERE BEAUTY_MONTH_PERCENTILE = 95) AS TOP20_P95_BEAUTY_CONSISTENCY
		  ,(SELECT MAX(BEAUTY_PRODUCT_TYPE) AS BEAUTY_PRODUCT_TYPE 
								FROM VPM_DATA.BEAUTY_HIGH_RELEVANCE_TOP20_NEW_SCHEMA
								WHERE BEAUTY_PRODUCT_TYPE_PERCENTILE  = 95) AS TOP20_P95_BEAUTY_PRODUCT_TYPE
FROM TEMP_BEAUTY_HIGH_RELEVANCE
;
							
UPDATE TEMP_BEAUTY_HIGH_RELEVANCE
SET  BEAUTY_SPENDING_SCORE = CASE WHEN BEAUTY_SPEND > TOP20_P95_BEAUTY_SPEND THEN 1 ELSE BEAUTY_SPEND/(TOP20_P95_BEAUTY_SPEND*1.0)  END
	,BEAUTY_DAY_VISIT_SCORE = CASE WHEN BEAUTY_DAY_VISIT > TOP20_P95_BEAUTY_DAY_VISIT THEN 1 ELSE BEAUTY_DAY_VISIT/(TOP20_P95_BEAUTY_DAY_VISIT*1.0)  END
	,BEAUTY_PRODUCT_TYPE_SCORE = CASE WHEN BEAUTY_PRODUCT_TYPE > TOP20_P95_BEAUTY_PRODUCT_TYPE THEN 1 ELSE BEAUTY_PRODUCT_TYPE/(TOP20_P95_BEAUTY_PRODUCT_TYPE*1.0)  END
	,BEAUTY_MONTH_VISIT_SCORE = CASE WHEN BEAUTY_MONTH > TOP20_P95_BEAUTY_CONSISTENCY THEN 1 ELSE BEAUTY_MONTH/(TOP20_P95_BEAUTY_CONSISTENCY*1.0) END
FROM VAR_BEAUTY_CAT_REV A WHERE A.MEMBER_NUMBER = TEMP_BEAUTY_HIGH_RELEVANCE.MEMBER_NUMBER
;


UPDATE TEMP_BEAUTY_HIGH_RELEVANCE
SET BEAUTY_RELEVANCE_SCORE= 0.4*COALESCE(BEAUTY_DAY_VISIT_SCORE,0) +
							 0.2*COALESCE(BEAUTY_SPENDING_SCORE,0) + 
							 0.2*COALESCE(BEAUTY_PRODUCT_TYPE_SCORE,0) +
							 0.2*COALESCE(BEAUTY_MONTH_VISIT_SCORE,0)
;

DROP TABLE IF EXISTS VPM_DATA.P_PM_CAT_REV_BEAUTY_BYRANK_NEW_SCHEMA
;
SELECT MEMBER_NUMBER,
	   BEAUTY_DAY_VISIT,
	   BEAUTY_SPEND,
	   BEAUTY_PRODUCT_TYPE,
	   BEAUTY_MONTH,
	   BEAUTY_RELEVANCE_SCORE
	   , (CASE WHEN BEAUTY_RELEVANCE_SCORE>0.45 THEN 'LOVER'
			   WHEN BEAUTY_RELEVANCE_SCORE>0.15 THEN'RELEVANCE'
			   ELSE 'PURCHASE' END ) AS CATEGORY_RELEVANCY
INTO VPM_DATA.P_PM_CAT_REV_BEAUTY_BYRANK_NEW_SCHEMA
FROM TEMP_BEAUTY_HIGH_RELEVANCE A
;
