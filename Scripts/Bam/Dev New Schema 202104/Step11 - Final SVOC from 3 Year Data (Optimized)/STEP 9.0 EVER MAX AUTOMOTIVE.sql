
--3MINS(APR2020),3.42MINS(MAY2020),3.16min(jun2020)
--26MINS(JAN21)
DROP TABLE IF EXISTS VPM_BACKUP.PM_EVER_MAX_AUTOMOTIVE_NEW_SCHEMA
;
CREATE TABLE VPM_BACKUP.PM_EVER_MAX_AUTOMOTIVE_NEW_SCHEMA AS
SELECT
		SVOC_CURRENT.MEMBER_NUMBER
		,SVOC_CURRENT.AUTOMOTIVE_SPENDING_RATIO
		,CASE WHEN SVOC_CURRENT.CAR_OWNERSHIP_FLAG= 'YES'
				OR EVER_MAX_PREVIOUS.CAR_OWNERSHIP_FLAG = 'YES' THEN 'YES' ELSE 'N/A' END AS CAR_OWNERSHIP_FLAG
		,CASE WHEN SVOC_CURRENT.MOTORCYCLE_OWNERSHIP_FLAG = 'YES'
				OR EVER_MAX_PREVIOUS.MOTORCYCLE_OWNERSHIP_FLAG = 'YES' THEN 'YES' ELSE 'N/A' END AS MOTORCYCLE_OWNERSHIP_FLAG
		,CASE WHEN SVOC_CURRENT.CAR_OWNERSHIP_FLAG = 'YES'
				OR EVER_MAX_PREVIOUS.CAR_OWNERSHIP_FLAG = 'YES' THEN CASE WHEN SVOC_CURRENT.EUROPE_CAR_OWNERSHIP_FLAG = 'YES'
																			OR EVER_MAX_PREVIOUS.EUROPE_CAR_OWNERSHIP_FLAG = 'YES' THEN 'YES' ELSE 'N/A' END
				ELSE 'N/A' END AS EUROPE_CAR_OWNERSHIP_FLAG
		,CASE WHEN SVOC_CURRENT.CAR_BRAND <> 'N/A' THEN SVOC_CURRENT.CAR_BRAND
				WHEN EVER_MAX_PREVIOUS.CAR_BRAND <> 'N/A' THEN EVER_MAX_PREVIOUS.CAR_BRAND
				ELSE 'N/A' END AS CAR_BRAND
		,CASE WHEN SVOC_CURRENT.PUBLIC_TRANSPORTATION_MODE <> 'N/A' THEN SVOC_CURRENT.PUBLIC_TRANSPORTATION_MODE
				WHEN EVER_MAX_PREVIOUS.PUBLIC_TRANSPORTATION_MODE <> 'N/A' THEN EVER_MAX_PREVIOUS.PUBLIC_TRANSPORTATION_MODE
				ELSE 'N/A' END AS PUBLIC_TRANSPORTATION_MODE
		,CASE WHEN SVOC_CURRENT.CAR_OWNERSHIP_FLAG = 'YES'
				OR EVER_MAX_PREVIOUS.CAR_OWNERSHIP_FLAG = 'YES' THEN CASE WHEN SVOC_CURRENT.CAR_ENTHUSIAST_FLAG = 'YES'
																			OR EVER_MAX_PREVIOUS.CAR_ENTHUSIAST_FLAG = 'YES' THEN 'YES' ELSE 'N/A' END
				ELSE 'N/A' END AS CAR_ENTHUSIAST_FLAG
		,CASE WHEN SVOC_CURRENT.CROSS_PROVINCE_DRIVER_FLAG = 'YES'
				OR EVER_MAX_PREVIOUS.CROSS_PROVINCE_DRIVER_FLAG = 'YES' THEN 'YES' ELSE 'N/A' END AS CROSS_PROVINCE_DRIVER_FLAG
		,CASE WHEN SVOC_CURRENT.CROSS_PROVINCE_SHOPPER_FLAG = 'YES'
				OR EVER_MAX_PREVIOUS.CROSS_PROVINCE_SHOPPER_FLAG = 'YES' THEN 'YES' ELSE 'N/A' END AS CROSS_PROVINCE_SHOPPER_FLAG
		,CASE WHEN SVOC_CURRENT.CROSS_PROVINCE_TRAVELLER_FLAG = 'YES'
				OR EVER_MAX_PREVIOUS.CROSS_PROVINCE_TRAVELLER_FLAG = 'YES' THEN 'YES' ELSE 'N/A' END AS CROSS_PROVINCE_TRAVELLER_FLAG
FROM VPM_BACKUP.PM_1YR_AUTOMOTIVE_NEW_SCHEMA SVOC_CURRENT
LEFT JOIN VPM_Backup.PM_SVOC_MASTER EVER_MAX_PREVIOUS ON EVER_MAX_PREVIOUS.MEMBER_NUMBER = SVOC_CURRENT.MEMBER_NUMBER;







