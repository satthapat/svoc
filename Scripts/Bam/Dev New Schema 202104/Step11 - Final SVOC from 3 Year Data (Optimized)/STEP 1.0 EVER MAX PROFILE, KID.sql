--4.42MINS(APR2020), 6MINS(MAY2020),4.44mins(jun2020)

drop table if exists VPM_BACKUP.PM_EVER_MAX_PROFILE_KID_NEW_SCHEMA
;


CREATE OR REPLACE PROCEDURE CREATE_PM_EVER_MAX_PROFILE_KID_TABLE()
LANGUAGE PLPGSQL
AS $$
DECLARE LAST_2_YEAR TEXT;
BEGIN
LAST_2_YEAR := (SELECT TO_CHAR(CURRENT_TIMESTAMP-INTERVAL '2 YEAR','yyyy'));
EXECUTE 'CREATE TABLE VPM_BACKUP.PM_EVER_MAX_PROFILE_KID_NEW_SCHEMA AS
select
		SVOC_CURRENT.MEMBER_NUMBER
		,SVOC_CURRENT.T1_CARD_NUMBER
		,SVOC_CURRENT.DOB
		,SVOC_CURRENT.CURRENT_AGE
		,SVOC_CURRENT.ENTRY_AGE
		,SVOC_CURRENT.NATIONALITY
		,SVOC_CURRENT.DECLARED_OCCUPATION
		,CASE WHEN SVOC_CURRENT.INFERRED_SALARYMAN_FLAG = ''YES''
					OR EVER_MAX_PREVIOUS.INFERRED_SALARYMAN_FLAG = ''YES'' THEN ''YES'' ELSE ''N/A'' END AS INFERRED_SALARYMAN_FLAG
		,SVOC_CURRENT.SALARYMAN_SCORE
		,SVOC_CURRENT.EDUCATIONLEVEL
		,SVOC_CURRENT.INFERRED_EDUCATION
		,SVOC_CURRENT.MONTHLYINCOME
		,EVER_MAX_PREVIOUS.HOUSEHOLDINCOME
		,SVOC_CURRENT.REGISTERDATE
		,SVOC_CURRENT.TENURE
		,SVOC_CURRENT.DECLARED_GENDER
		,SVOC_CURRENT.INFERRED_GENDER
		,SVOC_CURRENT.DECLARED_MARITAL_STATUS
		,SVOC_CURRENT.PROB_MARRIED
		,SVOC_CURRENT.LIFE_STAGE


		--2020.11.23
		,CASE WHEN (CASE WHEN CURRENT_COMMERCIAL.CG_COMMERCIAL_SEGMENT <> ''N/A'' THEN CURRENT_COMMERCIAL.CG_COMMERCIAL_SEGMENT
						WHEN EVER_MAX_PREVIOUS.CG_COMMERCIAL_SEGMENT <> ''N/A'' THEN EVER_MAX_PREVIOUS.CG_COMMERCIAL_SEGMENT
						ELSE ''N/A'' END) = ''N/A''
				OR (CASE WHEN CURRENT_COMMERCIAL.CG_COMMERCIAL_SEGMENT <> ''N/A'' THEN CURRENT_COMMERCIAL.CG_COMMERCIAL_SEGMENT
						WHEN EVER_MAX_PREVIOUS.CG_COMMERCIAL_SEGMENT <> ''N/A'' THEN EVER_MAX_PREVIOUS.CG_COMMERCIAL_SEGMENT
						ELSE ''N/A'' END) LIKE ''%%SEMI%%''
			THEN		CASE WHEN EXTRACT(MONTH FROM CURRENT_TIMESTAMP) = 1
								THEN CASE WHEN
											SVOC_CURRENT.LARGE_FAMILY_FLAG = ''YES''
											OR LARGE_FAMILY_'||LAST_2_YEAR||'.MEMBER_NUMBER IS NOT NULL
										THEN ''YES'' ELSE ''N/A'' END
								ELSE CASE WHEN
											SVOC_CURRENT.LARGE_FAMILY_FLAG = ''YES'' OR EVER_MAX_PREVIOUS.LARGE_FAMILY_FLAG = ''YES''
										THEN ''YES'' ELSE ''N/A'' END
								END
			ELSE ''N/A'' END AS LARGE_FAMILY_FLAG



		,SVOC_CURRENT.DECLARED_NUM_CHILDREN
		,SVOC_CURRENT.DECLARED_NUM_KIDS
		,SVOC_CURRENT.KID_TOTAL_SPEND
		,SVOC_CURRENT.KID_SPENDING_RATIO
		,SVOC_CURRENT.BOOK_STATIONERY_SPENDING_RATIO
		,SVOC_CURRENT.KID_DAY_SPEND
		,SVOC_CURRENT.KID_NUM_TYPE
		,SVOC_CURRENT.INFERRED_NUM_KIDS
		,SVOC_CURRENT.CURRENT_CHILD1_AGE
		,SVOC_CURRENT.CURRENT_CHILD2_AGE
		,SVOC_CURRENT.CURRENT_CHILD3_AGE
		,SVOC_CURRENT.CURRENT_CHILD4_AGE
		,SVOC_CURRENT.KIDS_FLAG
		,SVOC_CURRENT.KIDS_SCORE
		
		
		,SVOC_CURRENT.KID_DESC
		,SVOC_CURRENT.KID_HIGH_ACTIVITY_FLAG
		,SVOC_CURRENT.KID_MID_ACTIVITY_FLAG
		,SVOC_CURRENT.KID_ROLLING_FLAG
		,SVOC_CURRENT.KID_SBL_FLAG
		--,SVOC_CURRENT.KID_ONIGIRI_FLAG
		,SVOC_CURRENT.KID_CDS_KIDS_CLUB_FLAG
		,SVOC_CURRENT.KID_RBS_MOM_BABY_CLUB_FLAG
		,SVOC_CURRENT.KID_API_FLAG
		,SVOC_CURRENT.KID_CPN_FLAG
		,SVOC_CURRENT.KID_MOMANDKIDSPROJECT_FLAG
		,SVOC_CURRENT.KID_UPDATEPROFILE_FLAG
		--,SVOC_CURRENT.KID_THE1_APP_FLAG
		--,SVOC_CURRENT.KID_HOSPITAL_FLAG
		
		
		
		,SVOC_CURRENT.KIDS_SPENDING_SEGMENT
		,SVOC_CURRENT.KID_STAGE_PRIORITY
		,SVOC_CURRENT.KID_RELATIONSHIP
		,SVOC_CURRENT.KID_LAST_PURCHASE_DATE
		,SVOC_CURRENT.KID_ACTIVITY
		,SVOC_CURRENT.KID_INFANT
		,SVOC_CURRENT.KID_INFANT_SPENDING_SCORE
		,SVOC_CURRENT.INFANT_SPENDING_SEGMENT
		,SVOC_CURRENT.INFANT_GENDER
		,SVOC_CURRENT.ESTIMATED_DATE_OF_GIVE_BIRTH
		,SVOC_CURRENT.ESTIMATED_INFANT_MONTH
		,SVOC_CURRENT.KID_TODDLER
		,SVOC_CURRENT.KID_TODDLER_SPENDING_SCORE
		,SVOC_CURRENT.TODDLER_SPENDING_SEGMENT
		,SVOC_CURRENT.TODDLER_GENDER
		,SVOC_CURRENT.KID_PRESCHOOL
		,SVOC_CURRENT.KID_PRESCHOOL_SPENDING_SCORE
		,SVOC_CURRENT.PRESCHOOL_SPENDING_SEGMENT
		,SVOC_CURRENT.PRESCHOOL_GENDER
		,SVOC_CURRENT.KID_JUNIOR
		,SVOC_CURRENT.KID_JUNIOR_SPENDING_SCORE
		,SVOC_CURRENT.JUNIOR_SPENDING_SEGMENT
		,SVOC_CURRENT.JUNIOR_GENDER
		,SVOC_CURRENT.KID_TEENAGE
		,SVOC_CURRENT.KID_TEEN_SPENDING_SCORE
		,SVOC_CURRENT.TEEN_SPENDING_SEGMENT
		,SVOC_CURRENT.TEEN_GENDER
		,SVOC_CURRENT.KID_BURDEN_SCORE
		,SVOC_CURRENT.KID_BURDEN_LEVEL
		,SVOC_CURRENT.OPPOSITE_GENDER_SPENDING_SCORE
		,SVOC_CURRENT.SPENDING_AMT_FOR_HIM
		,SVOC_CURRENT.SPENDING_AMT_FOR_HER
from VPM_BACKUP.PM_1YR_PROFILE_KID_NEW_SCHEMA SVOC_CURRENT
LEFT JOIN VPM_BACKUP.PM_1YR_COMMERCIAL_NEW_SCHEMA CURRENT_COMMERCIAL ON CURRENT_COMMERCIAL.MEMBER_NUMBER = SVOC_CURRENT.MEMBER_NUMBER
left join (SELECT A.*,B.MEMBER_NUMBER FROM vpm_data.pm_large_family_cust_1yr_'||LAST_2_YEAR||' A
			INNER JOIN ANALYSIS_DATA.CUSTOMER_SBL B ON A.CUSTOMER_ID = B.CUSTOMERID) LARGE_FAMILY_'||LAST_2_YEAR||' on LARGE_FAMILY_'||LAST_2_YEAR||'.MEMBER_NUMBER = SVOC_CURRENT.MEMBER_NUMBER
left join VPM_BACKUP.PM_SVOC_MASTER EVER_MAX_PREVIOUS on EVER_MAX_PREVIOUS.MEMBER_NUMBER = SVOC_CURRENT.MEMBER_NUMBER


'
;

END $$
;

CALL CREATE_PM_EVER_MAX_PROFILE_KID_TABLE();