--------------------- FOOD
SELECT * FROM (
SELECT 'BEEF_FLAG' AS FEATURE_NAME,COALESCE((CASE WHEN BEEF_FLAG = 1 THEN 'YES' ELSE 'N/A' END),'N/A') AS FEATURE_VALUE,COUNT(*) AS CNT
FROM VPM_DATA.PM_SKUTAGGING_FOOD_CUST_SEGMENT
GROUP BY COALESCE((CASE WHEN BEEF_FLAG = 1 THEN 'YES' ELSE 'N/A' END),'N/A')
--UNION ALL
--SELECT 'FOOD_HEALTH_FLAG' AS FEATURE_NAME,COALESCE((CASE WHEN HEALTHY_FOOD_FLAG = 1 THEN 'YES' ELSE 'N/A' END),'N/A') AS FEATURE_VALUE,COUNT(*) AS CNT
--FROM VPM_DATA.pm_skutagging_hobby_interest
--GROUP BY COALESCE((CASE WHEN HEALTHY_FOOD_FLAG = 1 THEN 'YES' ELSE 'N/A' END),'N/A')
UNION ALL
SELECT 'FOOD_LUXURY_SEGMENT' AS FEATURE_NAME,COALESCE(FOOD_LUXURY_SEGMENT ,'N/A') AS FEATURE_VALUE,COUNT(*) AS CNT
FROM VPM_DATA.PM_SKUTAGGING_FOOD_CUST_SEGMENT
GROUP BY COALESCE(FOOD_LUXURY_SEGMENT ,'N/A')
UNION ALL
SELECT 'FOOD_PROMO_SENSITIVITY_SEGMENT' AS FEATURE_NAME,COALESCE(FOOD_PROMO_SENSITIVITY_SEGMENT ,'N/A') AS FEATURE_VALUE,COUNT(*) AS CNT
FROM VPM_DATA.PM_SKUTAGGING_FOOD_CUST_SEGMENT
GROUP BY COALESCE(FOOD_PROMO_SENSITIVITY_SEGMENT ,'N/A')
UNION ALL
SELECT 'FRUIT_LOVER_FLAG' AS FEATURE_NAME,COALESCE((CASE WHEN FRUIT_LOVER_FLAG = 1 THEN 'YES' ELSE 'N/A' END),'N/A') AS FEATURE_VALUE,COUNT(*) AS CNT
FROM VPM_DATA.PM_SKUTAGGING_FOOD_CUST_SEGMENT
GROUP BY COALESCE((CASE WHEN FRUIT_LOVER_FLAG = 1 THEN 'YES' ELSE 'N/A' END),'N/A')
UNION ALL
SELECT 'BASKET_FLAG' AS FEATURE_NAME,COALESCE((CASE WHEN BASKET_FLAG = 1 THEN 'YES' ELSE 'N/A' END),'N/A') AS FEATURE_VALUE,COUNT(*) AS CNT
FROM VPM_DATA.PM_SKUTAGGING_FOOD_CUST_SEGMENT
GROUP BY COALESCE((CASE WHEN BASKET_FLAG = 1 THEN 'YES' ELSE 'N/A' END),'N/A')
UNION ALL
SELECT 'MEAT_LOVER_FLAG' AS FEATURE_NAME,COALESCE((CASE WHEN MEAT_LOVER_FLAG = 1 THEN 'YES' ELSE 'N/A' END),'N/A') AS FEATURE_VALUE,COUNT(*) AS CNT
FROM VPM_DATA.PM_SKUTAGGING_FOOD_CUST_SEGMENT
GROUP BY COALESCE((CASE WHEN MEAT_LOVER_FLAG = 1 THEN 'YES' ELSE 'N/A' END),'N/A')
UNION ALL
SELECT 'PORK_FLAG' AS FEATURE_NAME,COALESCE((CASE WHEN PORK_FLAG = 1 THEN 'YES' ELSE 'N/A' END),'N/A') AS FEATURE_VALUE,COUNT(*) AS CNT
FROM VPM_DATA.PM_SKUTAGGING_FOOD_CUST_SEGMENT
GROUP BY COALESCE((CASE WHEN PORK_FLAG = 1 THEN 'YES' ELSE 'N/A' END),'N/A')
UNION ALL
SELECT 'RTE_FLAG' AS FEATURE_NAME,COALESCE((CASE WHEN RTE_FLAG = 1 THEN 'YES' ELSE 'N/A' END),'N/A') AS FEATURE_VALUE,COUNT(*) AS CNT
FROM VPM_DATA.PM_SKUTAGGING_FOOD_CUST_SEGMENT
GROUP BY COALESCE((CASE WHEN RTE_FLAG = 1 THEN 'YES' ELSE 'N/A' END),'N/A')
UNION ALL
SELECT 'SEAFOOD_LOVER_FLAG' AS FEATURE_NAME,COALESCE((CASE WHEN SEAFOOD_LOVER_FLAG = 1 THEN 'YES' ELSE 'N/A' END),'N/A') AS FEATURE_VALUE,COUNT(*) AS CNT
FROM VPM_DATA.PM_SKUTAGGING_FOOD_CUST_SEGMENT
GROUP BY COALESCE((CASE WHEN SEAFOOD_LOVER_FLAG = 1 THEN 'YES' ELSE 'N/A' END),'N/A')
UNION ALL
SELECT 'NEW_FOOD_GROCERY_LUXURY_SEGMENT' AS FEATURE_NAME,COALESCE(new_food_grocery_luxury_segment ,'N/A') AS FEATURE_VALUE,COUNT(*) AS CNT
FROM VPM_DATA.PM_SKUTAGGING_FOOD_CUST_SEGMENT
GROUP BY COALESCE(new_food_grocery_luxury_segment ,'N/A')
) A
ORDER BY FEATURE_NAME,FEATURE_VALUE

