/*****  1.PREPARE DATA ON SALESSKU AND PRODUCT MASTER  *******/ --11 MIN (DEC2019)
-- 1.1 ADD MAX-MIN SKU PRICE TO PRODUCT MASTER

-- 1.2 ADD MAX-MIN SKU PRICE TO SALESSKU >> 7MIN

/********** 2. CREATE PROMOTION SENSITIVITY SCORE ON CUSTOMER LEVEL *************/
-- 2.1 UPDATE PROMOTION SENSITIVITY FACTOR TO SALESSKU LEVEL 
--24m 32s
-- 2.2 CREATE CUSTOMER PROMOTION SENSITIVTY BY PRODUCT TYPE >> 5 MIN
INSERT INTO VPM_DATA.SALESSKU_LOG_TIME(task,start_time) VALUES ('D.ELECTRONICS STEP 3.0 - PROMOTION SENSITIVITY',current_timestamp)
;

DROP TABLE IF EXISTS VPM_DATA.PM_SKUTAGGING_ELECTRONICS_CUST_PROMOTION_SENSITIVITY_BY_TYPE_NEW_SCHEMA
;
SELECT *
	,ROW_NUMBER () OVER (PARTITION BY MEMBER_NUMBER ORDER BY COALESCE(ADJ_PCT_DCT,-1) DESC NULLS LAST) AS RANK_DCT
	,CAST(NULL AS FLOAT) AS TTL_DCT_SCORE
	,CAST(NULL AS FLOAT) AS ELECTRONICS_PROMO_SENSITIVITY_SCORE
INTO VPM_DATA.PM_SKUTAGGING_ELECTRONICS_CUST_PROMOTION_SENSITIVITY_BY_TYPE_NEW_SCHEMA	
FROM 
(
	SELECT MEMBER_NUMBER
		,CATEGORY_FINAL
		,TYPE_FINAL
		,COUNT(MEMBER_NUMBER) AS CNT_TXN
		,1-SUM(CASE WHEN UNRELIABLE_PRICE_FLAG = 1 THEN NULL ELSE TXN_AMT END)/SUM(CASE WHEN UNRELIABLE_PRICE_FLAG = 1 THEN NULL ELSE FULL_TXN_AMT END) AS TTL_DCT
		,AVG(DISCOUNT_PRICE_RATIO) AS ADJ_PCT_DCT
	FROM VPM_DATA.PM_SKUTAGGING_ELECTRONICS_SALESSKU_3YR_NEW_SCHEMA
	WHERE TYPE_FINAL IN (SELECT DISTINCT TYPE_FINAL FROM VPM_DATA.PM_SKUTAGGING_ELECTRONICS_PRODUCT_MASTER_NEW_SCHEMA WHERE TYPE_CNT_CUST > 1000)
	GROUP BY MEMBER_NUMBER,CATEGORY_FINAL,TYPE_FINAL
) A
ORDER BY MEMBER_NUMBER,CNT_TXN DESC
;

-- 2.3 COMBINE SENSITIVITY FACTOR TO CUSTOMER-TYPE LEVEL
--DROP TABLE IF EXISTS TMP_TOP_DCT; -- 3 MINS (JAN2020)
SELECT DISTINCT TYPE_FINAL,
	   PERCENTILE_DISC (0.95) WITHIN GROUP (ORDER BY TTL_DCT ASC) OVER (PARTITION BY TYPE_FINAL)  AS TOP_TTL_DCT
INTO TEMP TABLE TMP_TOP_DCT
FROM VPM_DATA.PM_SKUTAGGING_ELECTRONICS_CUST_PROMOTION_SENSITIVITY_BY_TYPE_NEW_SCHEMA
;

UPDATE VPM_DATA.PM_SKUTAGGING_ELECTRONICS_CUST_PROMOTION_SENSITIVITY_BY_TYPE_NEW_SCHEMA
SET TTL_DCT_SCORE = CASE WHEN TTL_DCT*1.0 > TOP_TTL_DCT THEN 1 ELSE TTL_DCT*1.0/TOP_TTL_DCT	END  
FROM TMP_TOP_DCT A
WHERE VPM_DATA.PM_SKUTAGGING_ELECTRONICS_CUST_PROMOTION_SENSITIVITY_BY_TYPE_NEW_SCHEMA.TYPE_FINAL = A.TYPE_FINAL
		AND TOP_TTL_DCT <> 0
;

UPDATE VPM_DATA.PM_SKUTAGGING_ELECTRONICS_CUST_PROMOTION_SENSITIVITY_BY_TYPE_NEW_SCHEMA
SET ELECTRONICS_PROMO_SENSITIVITY_SCORE = CASE WHEN TTL_DCT IS NOT NULL
												THEN (TTL_DCT_SCORE+ADJ_PCT_DCT)/2 
												ELSE ADJ_PCT_DCT END
;

-- 2.4 COMBINE SENSITIVITY FACTOR TO CUSTOMER LEVEL
-- 3 MINS(JAN2020)

DROP TABLE IF EXISTS VPM_DATA.ELECT_HIGH_PRICE_MOVEMENT_NEW_SCHEMA;
SELECT CATEGORY_FINAL,TYPE_FINAL, AVG(PERCENT_DISCOUNT) AS AVG_PCT_DISCOUNT, AVG(PRICE_DISCOUNT) AS AVG_AMT_DISCOUNT , COUNT(distinct MEMBER_NUMBER) AS CNT_CUST
INTO VPM_DATA.ELECT_HIGH_PRICE_MOVEMENT_NEW_SCHEMA
FROM VPM_DATA.PM_SKUTAGGING_ELECTRONICS_SALESSKU_3YR_NEW_SCHEMA
GROUP BY CATEGORY_FINAL,TYPE_FINAL
HAVING AVG(PRICE_DISCOUNT) >= 100 and AVG(PERCENT_DISCOUNT) >= 0.05 and COUNT(distinct MEMBER_NUMBER) > 100
;

UPDATE VPM_DATA.PM_SKUTAGGING_ELECTRONICS_CUST_SEGMENT_NEW_SCHEMA
SET SUM_RELIABLE_AMT = A.SUM_RELIABLE_AMT,
	SUM_RELIABLE_FULL_AMT = A.SUM_RELIABLE_FULL_AMT,
	DISCOUNT_PRICE_RATIO = A.DISCOUNT_PRICE_RATIO,
	MAX_DISCOUNT_PRICE_RATIO = A.MAX_DISCOUNT_PRICE_RATIO,
	WEIGHTED_DISCOUNT_PRICE_RATIO = A.WEIGHTED_DISCOUNT_PRICE_RATIO,
	DISCOUNT_SKU_NEW_BARCODE_FLAG = A.DISCOUNT_SKU_NEW_BARCODE_FLAG
FROM (SELECT MEMBER_NUMBER,
				SUM(CASE WHEN UNRELIABLE_PRICE_FLAG = 1 THEN NULL ELSE TXN_AMT END) AS SUM_RELIABLE_AMT,
				SUM(CASE WHEN UNRELIABLE_PRICE_FLAG = 1 THEN NULL ELSE FULL_TXN_AMT END) AS SUM_RELIABLE_FULL_AMT,
				MAX(UNRELIABLE_PRICE_FLAG) AS DISCOUNT_SKU_NEW_BARCODE_FLAG,
				AVG(DISCOUNT_PRICE_RATIO) AS DISCOUNT_PRICE_RATIO,
				MAX(DISCOUNT_PRICE_RATIO) AS MAX_DISCOUNT_PRICE_RATIO,
				SUM(DISCOUNT_PRICE_RATIO*FULL_TXN_AMT)/SUM(FULL_TXN_AMT) AS WEIGHTED_DISCOUNT_PRICE_RATIO
		FROM VPM_DATA.PM_SKUTAGGING_ELECTRONICS_SALESSKU_3YR_NEW_SCHEMA A
		INNER JOIN VPM_DATA.ELECT_HIGH_PRICE_MOVEMENT_NEW_SCHEMA B
		ON A.CATEGORY_FINAL = B.CATEGORY_FINAL AND A.TYPE_FINAL = B.TYPE_FINAL
		GROUP BY MEMBER_NUMBER) A
WHERE VPM_DATA.PM_SKUTAGGING_ELECTRONICS_CUST_SEGMENT_NEW_SCHEMA.MEMBER_NUMBER = A.MEMBER_NUMBER
;

UPDATE VPM_DATA.PM_SKUTAGGING_ELECTRONICS_CUST_SEGMENT_NEW_SCHEMA
	SET TTL_DISCOUNT = 1-(SUM_RELIABLE_AMT*1.0/SUM_RELIABLE_FULL_AMT)
;


							
CREATE temp TABLE var_TTL_DISCOUNT_SCORE AS
SELECT member_number
	,(SELECT DISTINCT PERCENTILE_DISC (0.95) WITHIN GROUP (ORDER BY TTL_DISCOUNT ASC) OVER ()
								FROM VPM_DATA.PM_SKUTAGGING_ELECTRONICS_CUST_SEGMENT_NEW_SCHEMA) AS TOP_TTL_DCT
FROM VPM_DATA.PM_SKUTAGGING_ELECTRONICS_CUST_SEGMENT_NEW_SCHEMA;


UPDATE VPM_DATA.PM_SKUTAGGING_ELECTRONICS_CUST_SEGMENT_NEW_SCHEMA
SET TTL_DISCOUNT_SCORE = CASE WHEN TTL_DISCOUNT*1.0 > TOP_TTL_DCT 
							  THEN 1 ELSE TTL_DISCOUNT*1.0/TOP_TTL_DCT END
FROM var_TTL_DISCOUNT_SCORE t
WHERE VPM_DATA.PM_SKUTAGGING_ELECTRONICS_CUST_SEGMENT_NEW_SCHEMA.member_number = t.member_number;



UPDATE VPM_DATA.PM_SKUTAGGING_ELECTRONICS_CUST_SEGMENT_NEW_SCHEMA
	SET ELECTRONICS_PROMO_SENSITIVITY_SCORE = CASE WHEN DISCOUNT_SKU_NEW_BARCODE_FLAG = 1 and TTL_DISCOUNT_SCORE = 0
												   THEN MAX_DISCOUNT_PRICE_RATIO*0.5 + WEIGHTED_DISCOUNT_PRICE_RATIO*0.5
												   ELSE MAX_DISCOUNT_PRICE_RATIO*0.25 + WEIGHTED_DISCOUNT_PRICE_RATIO*0.25 + TTL_DISCOUNT_SCORE*0.5 END
	/*SET ELECTRONICS_PROMO_SENSITIVITY_SCORE = CASE WHEN DISCOUNT_SKU_NEW_BARCODE_FLAG = 1 OR TTL_DISCOUNT_SCORE = 0
												   THEN DISCOUNT_PRICE_RATIO
												   ELSE (DISCOUNT_PRICE_RATIO+TTL_DISCOUNT_SCORE)/2 END,
		ELECTRONICS_PROMO_SENSITIVITY_SCORE_V2 = CASE WHEN DISCOUNT_SKU_NEW_BARCODE_FLAG = 1 OR TTL_DISCOUNT_SCORE = 0
												   THEN MAX_DISCOUNT_PRICE_RATIO
												   ELSE (MAX_DISCOUNT_PRICE_RATIO+TTL_DISCOUNT_SCORE)/2 END,
		ELECTRONICS_PROMO_SENSITIVITY_SCORE_V3 = CASE WHEN DISCOUNT_SKU_NEW_BARCODE_FLAG = 1 OR TTL_DISCOUNT_SCORE = 0
												   THEN WEIGHTED_DISCOUNT_PRICE_RATIO
												   ELSE (WEIGHTED_DISCOUNT_PRICE_RATIO+TTL_DISCOUNT_SCORE)/2 END*/
;


UPDATE VPM_DATA.PM_SKUTAGGING_ELECTRONICS_CUST_SEGMENT_NEW_SCHEMA
SET ELECTRONICS_PROMO_SENSITIVITY_SEGMENT = CASE WHEN ELECTRONICS_PROMO_SENSITIVITY_SCORE <= 0.5 THEN 'LOW'
											 	 WHEN ELECTRONICS_PROMO_SENSITIVITY_SCORE <= 0.7 THEN 'MID'
												 WHEN ELECTRONICS_PROMO_SENSITIVITY_SCORE <= 1.0 THEN 'HIGH'
												 ELSE NULL END
;
												
UPDATE VPM_DATA.SALESSKU_LOG_TIME SET end_time = current_timestamp WHERE task = 'D.ELECTRONICS STEP 3.0 - PROMOTION SENSITIVITY' AND EXTRACT(MONTH FROM start_time) = EXTRACT(MONTH FROM current_timestamp)
;



