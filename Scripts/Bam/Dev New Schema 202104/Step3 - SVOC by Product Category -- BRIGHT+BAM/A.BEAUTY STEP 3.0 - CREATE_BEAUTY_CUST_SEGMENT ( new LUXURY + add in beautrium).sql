
----------------------------------------- BEAUTY LUXURY SEGMENT -----------------------------------------
INSERT INTO VPM_DATA.SALESSKU_LOG_TIME(task,start_time) VALUES ('A.BEAUTY STEP 3.0 - CREATE_BEAUTY_CUST_SEGMENT (new LUXURY + add in beautrium)',current_timestamp)
;

DROP TABLE IF EXISTS vpm_data.BEAUTRIUM_PRODUCT_MASTER_NEW_SCHEMA
;-- AVOID DUPLICATION
SELECT DISTINCT PARTNER_CODE
      ,PAD_SKU_ID
      ,PRODUCT_NAME_EN AS PRODUCT_NAME
      ,DEPT_ID
      ,DEPT_NAME
      ,SUBDEPT_ID
      ,SUBDEPT_NAME
      ,CLASS_ID
      ,CLASS_NAME
      ,SUBCLASS_ID
      ,SUBCLASS_NAME
      ,VENDOR_ID
      ,VENDOR_NAME
      ,BRAND_ID
      ,BRAND_NAME
INTO vpm_data.BEAUTRIUM_PRODUCT_MASTER_NEW_SCHEMA
FROM Analysis_Data.MS_PRODUCT_BTM
WHERE DEPT_NAME IS NOT NULL OR DEPT_NAME <> '' --40604 FROM 44906
;

----------- SELECT ONLY BEAUTY PRODUCT FROM BEAUTRIUM -----------

DROP TABLE IF EXISTS vpm_data.BEAUTRIUM_BEAUTY_PRODUCT_NEW_SCHEMA
;
SELECT * 
INTO vpm_data.BEAUTRIUM_BEAUTY_PRODUCT_NEW_SCHEMA
FROM vpm_data.BEAUTRIUM_PRODUCT_MASTER_NEW_SCHEMA
WHERE (UPPER(DEPT_NAME) = 'MAKEUP' AND UPPER(SUBDEPT_NAME) NOT IN ('NAIL COLOR','CONTACT LEN')) OR
      (UPPER(DEPT_NAME) = 'DERMA' AND  UPPER(SUBDEPT_NAME) NOT IN ('BODY CARE','HAIR CARE')) OR
	   UPPER(DEPT_NAME) = 'FACIAL SKINCARE' OR
	  (UPPER(DEPT_NAME) = 'FRAGRANCE' AND  UPPER(SUBDEPT_NAME) != 'HOME SCENT') OR
	  (UPPER(DEPT_NAME) ='MEN'AND UPPER(SUBDEPT_NAME) IN ('SKINCARE','SUN CARE'))--32039
;

ALTER TABLE vpm_data.BEAUTRIUM_BEAUTY_PRODUCT_NEW_SCHEMA
ADD EDITED_CAT_TYPE_GRP VARCHAR(50)
;

--SELECT DISTINCT DEPT_NAME,SUBDEPT_NAME	SELECT * FROM vpm_data.BEAUTRIUM_TRANSACTION
--FROM vpm_data.BEAUTRIUM_BEAUTY_PRODUCT_NEW_SCHEMA
--ORDER BY DEPT_NAME,SUBDEPT_NAME

UPDATE vpm_data.BEAUTRIUM_BEAUTY_PRODUCT_NEW_SCHEMA
SET EDITED_CAT_TYPE_GRP = CASE WHEN DEPT_NAME = 'DERMA' AND SUBDEPT_NAME = 'CLEANSER & EXFOLIATOR' THEN 'SKIN CARE'
                               WHEN DEPT_NAME = 'DERMA' AND SUBDEPT_NAME = 'DERMA SET' THEN 'SKIN CARE'
                               WHEN DEPT_NAME = 'DERMA' AND SUBDEPT_NAME = 'MAKEUP' THEN 'MAKE-UP'
                               WHEN DEPT_NAME = 'DERMA' AND SUBDEPT_NAME = 'MAKEUP REMOVER' THEN 'SKIN CARE'
                               WHEN DEPT_NAME = 'DERMA' AND SUBDEPT_NAME = 'MASK & TREATMENT' THEN 'SKIN CARE'
                               WHEN DEPT_NAME = 'DERMA' AND SUBDEPT_NAME = 'MOISTURISER' THEN 'SKIN CARE'
                               WHEN DEPT_NAME = 'DERMA' AND SUBDEPT_NAME = 'SUN CARE' THEN 'SKIN CARE'
                               WHEN DEPT_NAME = 'DERMA' AND SUBDEPT_NAME = 'TONER' THEN 'SKIN CARE'
                               WHEN DEPT_NAME = 'FACIAL SKINCARE' THEN 'SKIN CARE'
                               WHEN DEPT_NAME = 'FRAGRANCE' THEN 'FRAGRANCE'
                               WHEN DEPT_NAME = 'MAKEUP' THEN 'MAKE-UP'
                               WHEN DEPT_NAME = 'MEN' THEN 'SKIN CARE' END
;

----------- ADD BEAUTY BRAND CLUSTER TO THE PRODUCT -----------
--DROP TABLE IF EXISTS TEMP_BEAUTY_CLUSTER;
SELECT CLEANED_BRANDNAME , MAX(BEAUTY_BRAND_CLUSTER) AS BEAUTY_BRAND_CLUSTER
INTO TEMP TABLE TEMP_BEAUTY_CLUSTER
FROM VPM_DATA.PM_SKUTAGGING_BEAUTY_PRODUCT_MASTER
GROUP BY CLEANED_BRANDNAME --3759
;

DROP TABLE IF EXISTS vpm_data.BEAUTRIUM_BEAUTY_PRODUCT_FINAL_NEW_SCHEMA;
SELECT A.* 
      ,B.BEAUTY_BRAND_CLUSTER
INTO vpm_data.BEAUTRIUM_BEAUTY_PRODUCT_FINAL_NEW_SCHEMA
FROM vpm_data.BEAUTRIUM_BEAUTY_PRODUCT_NEW_SCHEMA A
     LEFT JOIN TEMP_BEAUTY_CLUSTER B 
     ON UPPER(A.BRAND_NAME)  = UPPER(B.CLEANED_BRANDNAME) --32039
;

----------- BEAUTRIUM TRANSACTION (162316) -----------
DROP TABLE IF EXISTS vpm_data.NEW_BEAUTY_TRANSACTION_NEW_SCHEMA
;

CREATE TABLE vpm_data.NEW_BEAUTY_TRANSACTION_NEW_SCHEMA AS
SELECT MEMBER_NUMBER
      ,TXN_DATETIME
      ,TXN_DATE
      ,TICKET_NUM
      ,SKU_CODE
      ,TXN_AMT
      ,DEPT_CODE
      ,SUBDEPT_CODE
      ,QTY
      ,PRICE_PER_UNIT
      ,PARTNER_CODE
      ,DEPT_NAME
      ,SUBDEPT_NAME
      ,CLASS_ID
      ,CLASS_NAME
      ,SUBCLASS_ID
      ,SUBCLASS_NAME
      ,CLEANED_BRANDNAME
      ,PRODUCT_NAME
      ,CAST(EDITED_CAT_TYPE_GRP AS VARCHAR(50)) AS EDITED_CAT_TYPE_GRP
      ,BEAUTY_BRAND_CLUSTER
FROM VPM_DATA.PM_SKUTAGGING_BEAUTY_SALESSKU_1YR_NEW_SCHEMA
WHERE EDITED_CAT_TYPE_GRP IN ('MAKE-UP','SKIN CARE','FRAGRANCE') UNION ALL
SELECT 
	   A.MEMBER_NUMBER
      ,A.Trans_Date AS TXN_DATETIME
      ,CAST(A.Trans_Date AS DATE) AS TXN_DATE
      ,A.RECEIPT_NO 
      ,A.SKU_ID AS SKU_CODE
      ,A.NET_PRICE_TOT AS  TXN_AMT
      ,B.DEPT_ID AS DEPT_ID
      ,B.SUBDEPT_ID AS SUBDEPT_ID
      ,A.QTY
      ,A.NET_PRICE_TOT/A.QTY AS PRICE_PER_UNIT
      ,'BTM' AS PARTNER_CODE
      ,B.DEPT_NAME AS DEPT_NAME
      ,B.SUBDEPT_NAME AS SUBDEPT_NAME
      ,B.CLASS_ID AS CLASS_ID
      ,B.CLASS_NAME AS CLASS_NAME
      ,B.SUBCLASS_ID AS SUBCLASS_ID
      ,B.SUBCLASS_NAME AS SUBCLASS_NAME
      ,B.BRAND_NAME AS CLEANED_BRANDNAME
      ,B.PRODUCT_NAME
      ,CAST(B.EDITED_CAT_TYPE_GRP AS VARCHAR(50)) AS EDITED_CAT_TYPE_GRP
      ,B.BEAUTY_BRAND_CLUSTER
FROM Analysis_Data.Sales_SKU_BTM_2020 A INNER JOIN vpm_data.BEAUTRIUM_BEAUTY_PRODUCT_FINAL_NEW_SCHEMA B 
ON A.SKU_ID = B.PAD_SKU_ID --118655 (NULL 43661)
WHERE CAST(A.TRANS_DATE AS DATE) <= VPM_DATA.GET_END_DATE(current_timestamp) AND CAST(A.TRANS_DATE AS DATE)>= VPM_DATA.GET_START_DATE(current_timestamp)
AND COALESCE(MEMBER_NUMBER,'') <> ''
/*UNION ALL
SELECT 
	   A.MEMBER_NUMBER
      ,A.Trans_Date AS TXN_DATETIME
      ,CAST(A.Trans_Date AS DATE) AS TXN_DATE
      ,A.RECEIPT_NO 
      ,A.SKU_ID AS SKU_CODE
      ,A.NET_PRICE_TOT AS  TXN_AMT
      ,B.DEPT_ID AS DEPT_ID
      ,B.SUBDEPT_ID AS SUBDEPT_ID
      ,A.QTY
      ,A.NET_PRICE_TOT/A.QTY AS PRICE_PER_UNIT
      ,'BTM' AS PARTNER_CODE
      ,B.DEPT_NAME AS DEPT_NAME
      ,B.SUBDEPT_NAME AS SUBDEPT_NAME
      ,B.CLASS_ID AS CLASS_ID
      ,B.CLASS_NAME AS CLASS_NAME
      ,B.SUBCLASS_ID AS SUBCLASS_ID
      ,B.SUBCLASS_NAME AS SUBCLASS_NAME
      ,B.BRAND_NAME AS CLEANED_BRANDNAME
      ,B.PRODUCT_NAME
      ,CAST(B.EDITED_CAT_TYPE_GRP AS VARCHAR(50)) AS EDITED_CAT_TYPE_GRP
      ,B.BEAUTY_BRAND_CLUSTER
FROM Analysis_Data.Sales_SKU_BTM_2021 A INNER JOIN vpm_data.BEAUTRIUM_BEAUTY_PRODUCT_FINAL_NEW_SCHEMA B 
ON A.SKU_ID = B.PAD_SKU_ID --118655 (NULL 43661)
WHERE CAST(A.TRANS_DATE AS DATE) <= SELECT VPM_DATA.GET_END_DATE(current_timestamp) AND CAST(A.TRANS_DATE AS DATE)>= VPM_DATA.GET_START_DATE(current_timestamp)
AND COALESCE(MEMBER_NUMBER,'') <> ''*/
;

----------- AGGREGRATE TO CUSTOMER LEVEL -----------
----- OVERALL BEAUTY
--DROP TABLE IF EXISTS BEAUTY_CUSTOMER_NEW_SCHEMA;
SELECT A.MEMBER_NUMBER
      ,MAX(A.BEAUTY_BRAND_CLUSTER) AS MAX_BEAUTY_BRAND_CLUSTER
      ,MAX(CASE WHEN A.BEAUTY_BRAND_CLUSTER = 'CLUSTER 6' THEN 1 ELSE 0 END) AS BEAUTY_SUPER_LUXURY_FLAG
      ,MAX(CASE WHEN A.BEAUTY_BRAND_CLUSTER = 'CLUSTER 5' THEN 1 ELSE 0 END) AS BEAUTY_LUXURY_FLAG
      ,MAX(CASE WHEN A.BEAUTY_BRAND_CLUSTER = 'CLUSTER 4' THEN 1 ELSE 0 END) AS BEAUTY_ACCESSIBLE_LUXURY_FLAG
      ,MAX(CASE WHEN A.BEAUTY_BRAND_CLUSTER = 'CLUSTER 3' THEN 1 ELSE 0 END) AS BEAUTY_UPPER_MASS_FLAG
      ,MAX(CASE WHEN A.BEAUTY_BRAND_CLUSTER IN ('CLUSTER 2','CLUSTER 1') OR A.BEAUTY_BRAND_CLUSTER IS NULL THEN 1 ELSE 0 END) AS BEAUTY_MASS_FLAG
INTO TEMP TABLE BEAUTY_CUSTOMER_NEW_SCHEMA
FROM vpm_data.NEW_BEAUTY_TRANSACTION_NEW_SCHEMA A 
GROUP BY A.MEMBER_NUMBER ----2842857;
;

--DROP TABLE IF EXISTS BEAUTY_CUSTOMER_FINAL_NEW_SCHEMA;
SELECT A.*
      ,B.DAYVISIT AS MAX_BEAUTY_CLUSTER_DAYVISIT
	  ,B.CLUSTER_6_SALES
INTO TEMP TABLE BEAUTY_CUSTOMER_FINAL_NEW_SCHEMA
FROM BEAUTY_CUSTOMER_NEW_SCHEMA A LEFT JOIN (SELECT MEMBER_NUMBER
										 ,BEAUTY_BRAND_CLUSTER
										 ,COUNT(DISTINCT TXN_DATE) AS DAYVISIT
										 ,SUM(CASE WHEN BEAUTY_BRAND_CLUSTER = 'CLUSTER 6' THEN TXN_AMT ELSE 0 END) AS CLUSTER_6_SALES
									FROM vpm_data.NEW_BEAUTY_TRANSACTION_NEW_SCHEMA
									WHERE BEAUTY_BRAND_CLUSTER IS NOT NULL
									GROUP BY MEMBER_NUMBER, BEAUTY_BRAND_CLUSTER) B
								    ON (A.MEMBER_NUMBER = B.MEMBER_NUMBER AND A.MAX_BEAUTY_BRAND_CLUSTER = B.BEAUTY_BRAND_CLUSTER) --2842857
;
----- SKINCARE
--DROP TABLE IF EXISTS SKINCARE_CUSTOMER_NEW_SCHEMA;
SELECT A.MEMBER_NUMBER
      ,MAX(A.BEAUTY_BRAND_CLUSTER) AS MAX_SKINCARE_BRAND_CLUSTER
      ,MAX(CASE WHEN A.BEAUTY_BRAND_CLUSTER = 'CLUSTER 6' THEN 1 ELSE 0 END) AS SKINCARE_SUPER_LUXURY_FLAG
      ,MAX(CASE WHEN A.BEAUTY_BRAND_CLUSTER = 'CLUSTER 5' THEN 1 ELSE 0 END) AS SKINCARE_LUXURY_FLAG
      ,MAX(CASE WHEN A.BEAUTY_BRAND_CLUSTER = 'CLUSTER 4' THEN 1 ELSE 0 END) AS SKINCARE_ACCESSIBLE_LUXURY_FLAG
      ,MAX(CASE WHEN A.BEAUTY_BRAND_CLUSTER = 'CLUSTER 3' THEN 1 ELSE 0 END) AS SKINCARE_UPPER_MASS_FLAG
      ,MAX(CASE WHEN A.BEAUTY_BRAND_CLUSTER IN ('CLUSTER 2','CLUSTER 1') OR A.BEAUTY_BRAND_CLUSTER IS NULL THEN 1 ELSE 0 END) AS SKINCARE_MASS_FLAG
INTO TEMP TABLE SKINCARE_CUSTOMER_NEW_SCHEMA
FROM vpm_data.NEW_BEAUTY_TRANSACTION_NEW_SCHEMA A 
WHERE EDITED_CAT_TYPE_GRP = 'SKIN CARE'
GROUP BY A.MEMBER_NUMBER ----2418828
;

--DROP TABLE IF EXISTS SKINCARE_CUSTOMER_FINAL_NEW_SCHEMA;
SELECT A.*
      ,B.DAYVISIT AS MAX_SKINCARE_CLUSTER_DAYVISIT
INTO TEMP TABLE SKINCARE_CUSTOMER_FINAL_NEW_SCHEMA
FROM SKINCARE_CUSTOMER_NEW_SCHEMA A LEFT JOIN (SELECT MEMBER_NUMBER
										 ,BEAUTY_BRAND_CLUSTER
										 ,COUNT(DISTINCT TXN_DATE) AS DAYVISIT
									FROM vpm_data.NEW_BEAUTY_TRANSACTION_NEW_SCHEMA
									WHERE EDITED_CAT_TYPE_GRP = 'SKIN CARE' AND BEAUTY_BRAND_CLUSTER IS NOT NULL
									GROUP BY MEMBER_NUMBER, BEAUTY_BRAND_CLUSTER) B
								    ON (A.MEMBER_NUMBER = B. MEMBER_NUMBER AND A.MAX_SKINCARE_BRAND_CLUSTER = B.BEAUTY_BRAND_CLUSTER)
;
----- FRAGRANCE
--DROP TABLE IF EXISTS FRAGRANCE_CUSTOMER_NEW_SCHEMA;
SELECT A.MEMBER_NUMBER
      ,MAX(A.BEAUTY_BRAND_CLUSTER) AS MAX_FRAGRANCE_BRAND_CLUSTER
      ,MAX(CASE WHEN A.BEAUTY_BRAND_CLUSTER = 'CLUSTER 6' THEN 1 ELSE 0 END) AS FRAGRANCE_SUPER_LUXURY_FLAG
      ,MAX(CASE WHEN A.BEAUTY_BRAND_CLUSTER = 'CLUSTER 5' THEN 1 ELSE 0 END) AS FRAGRANCE_LUXURY_FLAG
      ,MAX(CASE WHEN A.BEAUTY_BRAND_CLUSTER = 'CLUSTER 4' THEN 1 ELSE 0 END) AS FRAGRANCE_ACCESSIBLE_LUXURY_FLAG
      ,MAX(CASE WHEN A.BEAUTY_BRAND_CLUSTER = 'CLUSTER 3' THEN 1 ELSE 0 END) AS FRAGRANCE_UPPER_MASS_FLAG
      ,MAX(CASE WHEN A.BEAUTY_BRAND_CLUSTER IN ('CLUSTER 2','CLUSTER 1') OR A.BEAUTY_BRAND_CLUSTER IS NULL THEN 1 ELSE 0 END) AS FRAGRANCE_MASS_FLAG
INTO TEMP TABLE FRAGRANCE_CUSTOMER_NEW_SCHEMA
FROM vpm_data.NEW_BEAUTY_TRANSACTION_NEW_SCHEMA A 
WHERE EDITED_CAT_TYPE_GRP = 'FRAGRANCE'
GROUP BY A.MEMBER_NUMBER ----253057
;

--DROP TABLE IF EXISTS FRAGRANCE_CUSTOMER_FINAL_NEW_SCHEMA;
SELECT A.*
      ,B.DAYVISIT AS MAX_FRAGRANCE_CLUSTER_DAYVISIT
INTO TEMP TABLE FRAGRANCE_CUSTOMER_FINAL_NEW_SCHEMA
FROM FRAGRANCE_CUSTOMER_NEW_SCHEMA A LEFT JOIN (SELECT MEMBER_NUMBER
										 ,BEAUTY_BRAND_CLUSTER
										 ,COUNT(DISTINCT TXN_DATE) AS DAYVISIT
									FROM vpm_data.NEW_BEAUTY_TRANSACTION_NEW_SCHEMA
									WHERE EDITED_CAT_TYPE_GRP = 'FRAGRANCE' AND BEAUTY_BRAND_CLUSTER IS NOT NULL
									GROUP BY MEMBER_NUMBER, BEAUTY_BRAND_CLUSTER) B
								    ON (A.MEMBER_NUMBER = B. MEMBER_NUMBER AND A.MAX_FRAGRANCE_BRAND_CLUSTER = B.BEAUTY_BRAND_CLUSTER)
;
----- MAKE-UP
--DROP TABLE IF EXISTS MAKEUP_CUSTOMER_NEW_SCHEMA;
SELECT A.MEMBER_NUMBER
      ,MAX(A.BEAUTY_BRAND_CLUSTER) AS MAX_MAKEUP_BRAND_CLUSTER
      ,MAX(CASE WHEN A.BEAUTY_BRAND_CLUSTER = 'CLUSTER 6' THEN 1 ELSE 0 END) AS MAKEUP_SUPER_LUXURY_FLAG
      ,MAX(CASE WHEN A.BEAUTY_BRAND_CLUSTER = 'CLUSTER 5' THEN 1 ELSE 0 END) AS MAKEUP_LUXURY_FLAG
      ,MAX(CASE WHEN A.BEAUTY_BRAND_CLUSTER = 'CLUSTER 4' THEN 1 ELSE 0 END) AS MAKEUP_ACCESSIBLE_LUXURY_FLAG
      ,MAX(CASE WHEN A.BEAUTY_BRAND_CLUSTER = 'CLUSTER 3' THEN 1 ELSE 0 END) AS MAKEUP_UPPER_MASS_FLAG
      ,MAX(CASE WHEN A.BEAUTY_BRAND_CLUSTER IN ('CLUSTER 2','CLUSTER 1') OR A.BEAUTY_BRAND_CLUSTER IS NULL THEN 1 ELSE 0 END) AS MAKEUP_MASS_FLAG
INTO TEMP TABLE MAKEUP_CUSTOMER_NEW_SCHEMA
FROM vpm_data.NEW_BEAUTY_TRANSACTION_NEW_SCHEMA A 
WHERE EDITED_CAT_TYPE_GRP = 'MAKE-UP'
GROUP BY A.MEMBER_NUMBER ----1290927
;

--DROP TABLE IF EXISTS MAKEUP_CUSTOMER_FINAL_NEW_SCHEMA;
SELECT A.*
      ,B.DAYVISIT AS MAX_MAKEUP_CLUSTER_DAYVISIT
INTO TEMP TABLE MAKEUP_CUSTOMER_FINAL_NEW_SCHEMA
FROM MAKEUP_CUSTOMER_NEW_SCHEMA A LEFT JOIN (SELECT MEMBER_NUMBER
										 ,BEAUTY_BRAND_CLUSTER
										 ,COUNT(DISTINCT TXN_DATE) AS DAYVISIT
									FROM vpm_data.NEW_BEAUTY_TRANSACTION_NEW_SCHEMA
									WHERE EDITED_CAT_TYPE_GRP = 'MAKE-UP' AND BEAUTY_BRAND_CLUSTER IS NOT NULL
									GROUP BY MEMBER_NUMBER, BEAUTY_BRAND_CLUSTER) B
								    ON (A.MEMBER_NUMBER = B. MEMBER_NUMBER AND A.MAX_MAKEUP_BRAND_CLUSTER = B.BEAUTY_BRAND_CLUSTER)
;

DROP TABLE IF EXISTS VPM_DATA.BEAUTY_LUXURY_CUST_SEGMENT_NEW_SCHEMA;
SELECT A.*
      ,B.MAX_MAKEUP_BRAND_CLUSTER
	  ,B.MAKEUP_SUPER_LUXURY_FLAG
	  ,B.MAKEUP_LUXURY_FLAG
	  ,B.MAKEUP_ACCESSIBLE_LUXURY_FLAG
	  ,B.MAKEUP_UPPER_MASS_FLAG
	  ,B.MAKEUP_MASS_FLAG
	  ,B.MAX_MAKEUP_CLUSTER_DAYVISIT
	  ,C.MAX_SKINCARE_BRAND_CLUSTER
	  ,C.SKINCARE_SUPER_LUXURY_FLAG
	  ,C.SKINCARE_LUXURY_FLAG
	  ,C.SKINCARE_ACCESSIBLE_LUXURY_FLAG
	  ,C.SKINCARE_UPPER_MASS_FLAG
	  ,C.SKINCARE_MASS_FLAG
	  ,C.MAX_SKINCARE_CLUSTER_DAYVISIT
	  ,D.MAX_FRAGRANCE_BRAND_CLUSTER
	  ,D.FRAGRANCE_SUPER_LUXURY_FLAG
	  ,D.FRAGRANCE_LUXURY_FLAG
	  ,D.FRAGRANCE_ACCESSIBLE_LUXURY_FLAG
	  ,D.FRAGRANCE_UPPER_MASS_FLAG
	  ,D.FRAGRANCE_MASS_FLAG
	  ,D.MAX_FRAGRANCE_CLUSTER_DAYVISIT
INTO VPM_DATA.BEAUTY_LUXURY_CUST_SEGMENT_NEW_SCHEMA
FROM BEAUTY_CUSTOMER_FINAL_NEW_SCHEMA A LEFT JOIN MAKEUP_CUSTOMER_FINAL_NEW_SCHEMA B ON A.MEMBER_NUMBER = B.MEMBER_NUMBER
							  LEFT JOIN SKINCARE_CUSTOMER_FINAL_NEW_SCHEMA C ON A.MEMBER_NUMBER = C.MEMBER_NUMBER
							  LEFT JOIN FRAGRANCE_CUSTOMER_FINAL_NEW_SCHEMA D ON A.MEMBER_NUMBER = D.MEMBER_NUMBER --2842857
;

----------- ADD LUXURY SEGMENT -----------

ALTER TABLE VPM_DATA.BEAUTY_LUXURY_CUST_SEGMENT_NEW_SCHEMA ADD NEW_BEAUTY_LUXURY_SEGMENT VARCHAR(50);
ALTER TABLE VPM_DATA.BEAUTY_LUXURY_CUST_SEGMENT_NEW_SCHEMA ADD MAKEUP_LUXURY_SEGMENT VARCHAR(50);
ALTER TABLE VPM_DATA.BEAUTY_LUXURY_CUST_SEGMENT_NEW_SCHEMA ADD SKINCARE_LUXURY_SEGMENT VARCHAR(50);
ALTER TABLE VPM_DATA.BEAUTY_LUXURY_CUST_SEGMENT_NEW_SCHEMA ADD FRAGRANCE_LUXURY_SEGMENT VARCHAR(50);

UPDATE VPM_DATA.BEAUTY_LUXURY_CUST_SEGMENT_NEW_SCHEMA
SET NEW_BEAUTY_LUXURY_SEGMENT = CASE WHEN MAX_BEAUTY_BRAND_CLUSTER = 'CLUSTER 6' AND MAX_BEAUTY_CLUSTER_DAYVISIT >=2 THEN 'SUPER LUXURY'
									 WHEN MAX_BEAUTY_BRAND_CLUSTER = 'CLUSTER 6' AND BEAUTY_LUXURY_FLAG = 1 THEN 'LUXURY'
									 WHEN MAX_BEAUTY_BRAND_CLUSTER = 'CLUSTER 6' AND CLUSTER_6_SALES >= 20000 THEN 'LUXURY'
									 WHEN MAX_BEAUTY_BRAND_CLUSTER = 'CLUSTER 6' THEN 'ACCESSIBLE LUXURY'
									 WHEN MAX_BEAUTY_BRAND_CLUSTER = 'CLUSTER 5' AND MAX_BEAUTY_CLUSTER_DAYVISIT >=2 THEN 'LUXURY'
									 WHEN MAX_BEAUTY_BRAND_CLUSTER = 'CLUSTER 5' THEN 'ACCESSIBLE LUXURY'
									 WHEN MAX_BEAUTY_BRAND_CLUSTER = 'CLUSTER 4' THEN 'ACCESSIBLE LUXURY'
									 WHEN MAX_BEAUTY_BRAND_CLUSTER = 'CLUSTER 3' THEN 'UPPER MASS'
									 ELSE 'MASS'
									 END
;

UPDATE VPM_DATA.BEAUTY_LUXURY_CUST_SEGMENT_NEW_SCHEMA
SET MAKEUP_LUXURY_SEGMENT = CASE WHEN MAX_MAKEUP_BRAND_CLUSTER = 'CLUSTER 6' AND MAX_MAKEUP_CLUSTER_DAYVISIT >=2 THEN 'SUPER LUXURY'
									 WHEN MAX_MAKEUP_BRAND_CLUSTER = 'CLUSTER 6' AND MAKEUP_LUXURY_FLAG = 1 THEN 'LUXURY'
									 WHEN MAX_MAKEUP_BRAND_CLUSTER = 'CLUSTER 6' AND MAKEUP_LUXURY_FLAG = 0 THEN 'ACCESSIBLE LUXURY'
									 WHEN MAX_MAKEUP_BRAND_CLUSTER = 'CLUSTER 5' AND MAX_MAKEUP_CLUSTER_DAYVISIT >=2 THEN 'LUXURY'
									 WHEN MAX_MAKEUP_BRAND_CLUSTER = 'CLUSTER 5' THEN 'ACCESSIBLE LUXURY'
									 WHEN MAX_MAKEUP_BRAND_CLUSTER = 'CLUSTER 4' THEN 'ACCESSIBLE LUXURY'
									 WHEN MAX_MAKEUP_BRAND_CLUSTER = 'CLUSTER 3' THEN 'UPPER MASS'
									 ELSE 'MASS'
									 END
;

UPDATE VPM_DATA.BEAUTY_LUXURY_CUST_SEGMENT_NEW_SCHEMA
SET SKINCARE_LUXURY_SEGMENT = CASE WHEN MAX_SKINCARE_BRAND_CLUSTER = 'CLUSTER 6' AND MAX_SKINCARE_CLUSTER_DAYVISIT >=2 THEN 'SUPER LUXURY'
									 WHEN MAX_SKINCARE_BRAND_CLUSTER = 'CLUSTER 6' AND SKINCARE_LUXURY_FLAG = 1 THEN 'LUXURY'
									 WHEN MAX_SKINCARE_BRAND_CLUSTER = 'CLUSTER 6' AND SKINCARE_LUXURY_FLAG = 0 THEN 'ACCESSIBLE LUXURY'
									 WHEN MAX_SKINCARE_BRAND_CLUSTER = 'CLUSTER 5' AND MAX_SKINCARE_CLUSTER_DAYVISIT >=2 THEN 'LUXURY'
									 WHEN MAX_SKINCARE_BRAND_CLUSTER = 'CLUSTER 5' THEN 'ACCESSIBLE LUXURY'
									 WHEN MAX_SKINCARE_BRAND_CLUSTER = 'CLUSTER 4' THEN 'ACCESSIBLE LUXURY'
									 WHEN MAX_SKINCARE_BRAND_CLUSTER = 'CLUSTER 3' THEN 'UPPER MASS'
									 ELSE 'MASS'
									 END
;

UPDATE VPM_DATA.BEAUTY_LUXURY_CUST_SEGMENT_NEW_SCHEMA
SET FRAGRANCE_LUXURY_SEGMENT = CASE WHEN MAX_FRAGRANCE_BRAND_CLUSTER = 'CLUSTER 6' AND MAX_FRAGRANCE_CLUSTER_DAYVISIT >=2 THEN 'SUPER LUXURY'
									 WHEN MAX_FRAGRANCE_BRAND_CLUSTER = 'CLUSTER 6' AND FRAGRANCE_LUXURY_FLAG = 1 THEN 'LUXURY'
									 WHEN MAX_FRAGRANCE_BRAND_CLUSTER = 'CLUSTER 6' AND FRAGRANCE_LUXURY_FLAG = 0 THEN 'ACCESSIBLE LUXURY'
									 WHEN MAX_FRAGRANCE_BRAND_CLUSTER = 'CLUSTER 5' AND MAX_FRAGRANCE_CLUSTER_DAYVISIT >=2 THEN 'LUXURY'
									 WHEN MAX_FRAGRANCE_BRAND_CLUSTER = 'CLUSTER 5' THEN 'ACCESSIBLE LUXURY'
									 WHEN MAX_FRAGRANCE_BRAND_CLUSTER = 'CLUSTER 4' THEN 'ACCESSIBLE LUXURY'
									 WHEN MAX_FRAGRANCE_BRAND_CLUSTER = 'CLUSTER 3' THEN 'UPPER MASS'
									 ELSE 'MASS'
									 END
;



----------------------------------------- HBA LUXURY SEGMENT -----------------------------------------

----------- SELECT ONLY HBA PRODUCT FROM BEAUTRIUM -----------

--DROP TABLE IF EXISTS BEAUTRIUM_HBA_PRODUCT_NEW_SCHEMA;
SELECT * 
INTO TEMP TABLE BEAUTRIUM_HBA_PRODUCT_NEW_SCHEMA
FROM vpm_data.BEAUTRIUM_PRODUCT_MASTER_NEW_SCHEMA
WHERE (UPPER(DEPT_NAME) = 'BATH & BODY') OR
      (UPPER(DEPT_NAME) = 'DERMA' AND  UPPER(SUBDEPT_NAME) IN ('BODY CARE','HAIR CARE')) OR
	  (UPPER(DEPT_NAME) = 'MEN' AND UPPER(SUBDEPT_NAME) IN ('GROOMING','HAIR CARE','HAIR TREATMENT','INTIMATE WASH')) --2390
;

----------- ADD BEAUTY BRAND CLUSTER TO THE PRODUCT -----------

--DROP TABLE IF EXISTS BEAUTRIUM_HBA_PRODUCT_FINAL_NEW_SCHEMA;
SELECT A.* 
      ,B.BEAUTY_BRAND_CLUSTER AS HBA_BRAND_CLUSTER
INTO TEMP TABLE BEAUTRIUM_HBA_PRODUCT_FINAL_NEW_SCHEMA
FROM BEAUTRIUM_HBA_PRODUCT_NEW_SCHEMA A
     LEFT JOIN TEMP_BEAUTY_CLUSTER B 
     ON A.BRAND_NAME = B.CLEANED_BRANDNAME --2390
;
----------- BEAUTRIUM HBA TRANSACTION (162316) -----------

DROP TABLE IF EXISTS vpm_data.NEW_HBA_TRANSACTION_NEW_SCHEMA;

CREATE TABLE vpm_data.NEW_HBA_TRANSACTION_NEW_SCHEMA  AS
SELECT MEMBER_NUMBER
      ,TXN_DATETIME
      ,TXN_DATE
      ,TICKET_NUM
      ,SKU_CODE
      ,TXN_AMT
      ,DEPT_CODE
      ,SUBDEPT_CODE
      ,QTY
      ,PRICE_PER_UNIT
      ,PARTNER_CODE
      ,DEPT_NAME
      ,SUBDEPT_NAME
      ,CLASS_ID
      ,CLASS_NAME
      ,SUBCLASS_ID
      ,SUBCLASS_NAME
      ,BRAND_NAME
      ,CLEANED_BRANDNAME
      ,PRODUCT_NAME
      ,CAST(EDITED_CAT_TYPE_GRP AS VARCHAR(50)) AS EDITED_CAT_TYPE_GRP
      ,BEAUTY_BRAND_CLUSTER AS HBA_BRAND_CLUSTER
FROM VPM_DATA.PM_SKUTAGGING_BEAUTY_SALESSKU_1YR_NEW_SCHEMA 
WHERE EDITED_CAT_TYPE_GRP LIKE '%TOILETRIES%' 
UNION ALL
(SELECT A.MEMBER_NUMBER
      ,A.Trans_Date AS TXN_DATETIME
      ,CAST(A.Trans_Date AS DATE) AS TXN_DATE
      ,A.RECEIPT_NO 
      ,A.SKU_ID AS SKU_CODE
      ,A.NET_PRICE_TOT AS TXN_AMT
      ,B.DEPT_ID AS DEPT_ID
      ,B.SUBDEPT_ID AS SUBDEPT_ID
      ,A.QTY
      ,A.NET_PRICE_TOT/A.QTY AS PRICE_PER_UNIT
      ,'BTM' AS PARTNER_CODE
      ,B.DEPT_NAME AS DEPT_NAME
      ,B.SUBDEPT_NAME AS SUBDEPT_NAME
      ,B.CLASS_ID AS CLASS_ID
      ,B.CLASS_NAME AS CLASS_NAME
      ,B.SUBCLASS_ID AS SUBCLASS_ID
      ,B.SUBCLASS_NAME AS SUBCLASS_NAME
      ,B.BRAND_NAME AS BRAND_NAME
      ,B.BRAND_NAME AS CLEANED_BRANDNAME
      ,B.PRODUCT_NAME
      ,CAST('HBA' AS VARCHAR(50)) AS EDITED_CAT_TYPE_GRP
      ,HBA_BRAND_CLUSTER
FROM Analysis_Data.Sales_SKU_BTM_2020 A INNER JOIN BEAUTRIUM_HBA_PRODUCT_FINAL_NEW_SCHEMA B ON A.SKU_ID = B.PAD_SKU_ID --23819943
WHERE CAST(A.TRANS_DATE AS DATE) <= VPM_DATA.GET_END_DATE(current_timestamp) AND CAST(A.TRANS_DATE AS DATE)>= VPM_DATA.GET_START_DATE(current_timestamp)
AND COALESCE(MEMBER_NUMBER,'') <> '');

----------- AGGREGRATE TO CUSTOMER LEVEL -----------

DROP TABLE IF EXISTS HBA_CUSTOMER_BEAUTY_NEW_SCHEMA
;
SELECT A.MEMBER_NUMBER
      ,MAX(A.HBA_BRAND_CLUSTER) AS MAX_HBA_BRAND_CLUSTER
      ,MAX(CASE WHEN A.HBA_BRAND_CLUSTER = 'CLUSTER 6' THEN 1 ELSE 0 END) AS HBA_SUPER_LUXURY_FLAG
      ,MAX(CASE WHEN A.HBA_BRAND_CLUSTER = 'CLUSTER 5' THEN 1 ELSE 0 END) AS HBA_LUXURY_FLAG
      ,MAX(CASE WHEN A.HBA_BRAND_CLUSTER = 'CLUSTER 4' THEN 1 ELSE 0 END) AS HBA_ACCESSIBLE_LUXURY_FLAG
      ,MAX(CASE WHEN A.HBA_BRAND_CLUSTER = 'CLUSTER 3' THEN 1 ELSE 0 END) AS HBA_UPPER_MASS_FLAG
      ,MAX(CASE WHEN A.HBA_BRAND_CLUSTER IN ('CLUSTER 2','CLUSTER 1') OR A.HBA_BRAND_CLUSTER IS NULL THEN 1 ELSE 0 END) AS HBA_MASS_FLAG
INTO TEMP TABLE HBA_CUSTOMER_BEAUTY_NEW_SCHEMA
FROM vpm_data.NEW_HBA_TRANSACTION_NEW_SCHEMA A 
GROUP BY A.MEMBER_NUMBER ----3010246
;

DROP TABLE IF EXISTS VPM_DATA.HBA_LUXURY_CUST_SEGMENT_NEW_SCHEMA
;
SELECT A.*
      ,B.DAYVISIT AS MAX_HBA_CLUSTER_DAYVISIT
INTO VPM_DATA.HBA_LUXURY_CUST_SEGMENT_NEW_SCHEMA
FROM HBA_CUSTOMER_BEAUTY_NEW_SCHEMA A LEFT JOIN (SELECT MEMBER_NUMBER
										 ,HBA_BRAND_CLUSTER
										 ,COUNT(DISTINCT TXN_DATE) AS DAYVISIT
									FROM vpm_data.NEW_HBA_TRANSACTION_NEW_SCHEMA
									WHERE HBA_BRAND_CLUSTER IS NOT NULL
									GROUP BY MEMBER_NUMBER, HBA_BRAND_CLUSTER) B
								    ON (A.MEMBER_NUMBER = B. MEMBER_NUMBER AND A.MAX_HBA_BRAND_CLUSTER = B.HBA_BRAND_CLUSTER) --3010246
;

ALTER TABLE VPM_DATA.HBA_LUXURY_CUST_SEGMENT_NEW_SCHEMA
ADD HBA_LUXURY_SEGMENT VARCHAR(50)
;


UPDATE VPM_DATA.HBA_LUXURY_CUST_SEGMENT_NEW_SCHEMA
SET HBA_LUXURY_SEGMENT = CASE WHEN MAX_HBA_BRAND_CLUSTER = 'CLUSTER 6' AND MAX_HBA_CLUSTER_DAYVISIT >=2 THEN 'SUPER LUXURY'
									 WHEN MAX_HBA_BRAND_CLUSTER = 'CLUSTER 6' AND HBA_LUXURY_FLAG = 1 THEN 'LUXURY'
									 WHEN MAX_HBA_BRAND_CLUSTER = 'CLUSTER 6' AND HBA_LUXURY_FLAG = 0 THEN 'ACCESSIBLE LUXURY'
									 WHEN MAX_HBA_BRAND_CLUSTER = 'CLUSTER 5' AND MAX_HBA_CLUSTER_DAYVISIT >=2 THEN 'LUXURY'
									 WHEN MAX_HBA_BRAND_CLUSTER = 'CLUSTER 5' THEN 'ACCESSIBLE LUXURY'
									 WHEN MAX_HBA_BRAND_CLUSTER = 'CLUSTER 4' THEN 'ACCESSIBLE LUXURY'
									 WHEN MAX_HBA_BRAND_CLUSTER = 'CLUSTER 3' THEN 'UPPER MASS'
									 ELSE 'MASS'END
;


----------------------------------------- APPLY LUXURY SEGMENT TO SVOC -----------------------------------------

---- BEAUTY LUXURY

UPDATE VPM_DATA.PM_SKUTAGGING_BEAUTY_CUST_SEGMENT_NEW_SCHEMA
SET OVERALL_BEAUTY_LUXURY_SEGMENT = CASE WHEN VPM_DATA.PM_SKUTAGGING_BEAUTY_CUST_SEGMENT_NEW_SCHEMA.MEMBER_NUMBER = A.MEMBER_NUMBER 
									     THEN A.NEW_BEAUTY_LUXURY_SEGMENT END,
	MAKEUP_LUXURY_SEGMENT = CASE WHEN VPM_DATA.PM_SKUTAGGING_BEAUTY_CUST_SEGMENT_NEW_SCHEMA.MEMBER_NUMBER = A.MEMBER_NUMBER 
									     THEN A.MAKEUP_LUXURY_SEGMENT END,
	SKINCARE_LUXURY_SEGMENT = CASE WHEN VPM_DATA.PM_SKUTAGGING_BEAUTY_CUST_SEGMENT_NEW_SCHEMA.MEMBER_NUMBER = A.MEMBER_NUMBER 
									     THEN A.SKINCARE_LUXURY_SEGMENT END,
	FRAGRANCE_LUXURY_SEGMENT = CASE WHEN VPM_DATA.PM_SKUTAGGING_BEAUTY_CUST_SEGMENT_NEW_SCHEMA.MEMBER_NUMBER = A.MEMBER_NUMBER 
									     THEN A.FRAGRANCE_LUXURY_SEGMENT END								     
FROM VPM_DATA.BEAUTY_LUXURY_CUST_SEGMENT_NEW_SCHEMA A
WHERE VPM_DATA.PM_SKUTAGGING_BEAUTY_CUST_SEGMENT_NEW_SCHEMA.MEMBER_NUMBER = A.MEMBER_NUMBER
;

---- HBA LUXURY

UPDATE VPM_DATA.PM_SKUTAGGING_BEAUTY_CUST_SEGMENT_NEW_SCHEMA
SET HBA_LUXURY_SEGMENT = CASE WHEN VPM_DATA.PM_SKUTAGGING_BEAUTY_CUST_SEGMENT_NEW_SCHEMA.MEMBER_NUMBER = A.MEMBER_NUMBER 
									     THEN A.HBA_LUXURY_SEGMENT END
FROM VPM_DATA.HBA_LUXURY_CUST_SEGMENT_NEW_SCHEMA A
WHERE VPM_DATA.PM_SKUTAGGING_BEAUTY_CUST_SEGMENT_NEW_SCHEMA.MEMBER_NUMBER = A.MEMBER_NUMBER
;

UPDATE VPM_DATA.SALESSKU_LOG_TIME SET end_time = current_timestamp WHERE task = 'A.BEAUTY STEP 3.0 - CREATE_BEAUTY_CUST_SEGMENT (new LUXURY + add in beautrium)' AND EXTRACT(MONTH FROM start_time) = EXTRACT(MONTH FROM current_timestamp)
;
---------------------------------------------------------------------------------------------------------------------------
