--1.21HOUR(DEC2019)
/*NEW ARRIVAL LOVER AND PRICE SENSITIVITY SEGMENT*/

/*NEW VERSHION*/
/*** UPDATE PRICE BY SKU ***/


/*NEW ARRIVAL SCORE*/
--63m 44s

--53m 49s
--59m 24s

INSERT INTO VPM_DATA.SALESSKU_LOG_TIME(task,start_time) VALUES ('B.FASHION STEP 3.0 - CREATE_FASHION_CUST_PRICE_SENSITIVE_NEW_ARRIVAL',current_timestamp)
;

--DROP TABLE IF EXISTS PROD;
SELECT * 
INTO TEMP TABLE PROD
FROM VPM_DATA.PM_SKUTAGGING_FASHION_PRODUCT_MASTER_NEW_SCHEMA
WHERE UPPER(CAT_TYPE_GRP) NOT IN ('JEWELRY','SOCKS','UNDERWEAR AND NIGHTWEAR')
--AND TTL_CUST > 20
;

--DROP TABLE IF EXISTS SKU; --9m 15s
SELECT PARTNER_CODE,SKU_CODE,COUNT(DISTINCT MEMBER_NUMBER) AS TTL_CUST
INTO TEMP TABLE SKU
FROM VPM_DATA.PM_SKUTAGGING_FASHION_SALESSKU_1YR_NEW_SCHEMA
GROUP BY PARTNER_CODE,SKU_CODE
HAVING COUNT(DISTINCT MEMBER_NUMBER) > 20
;

--DROP TABLE IF EXISTS TEMP_NEW_PRICE_SCORE; --8m 19s

CREATE TEMP TABLE TEMP_NEW_PRICE_SCORE AS
SELECT MEMBER_NUMBER
	,COUNT(DISTINCT TXN_DATE) AS TTL_FREQ
	,COUNT(DISTINCT CASE WHEN datediff(day,MIN_TXN_DATE,TXN_DATE) < 15 THEN TXN_DATE END) AS NEW_ARRIVAL_FREQ  
	,AVG(CASE WHEN datediff(day,MIN_TXN_DATE,TXN_DATE) < 15
		  THEN 
		  ( CASE WHEN A.EDITED_MAX_PRICE_SKU!= A.EDITED_MIN_PRICE_SKU THEN
					(((EDITED_PRICE_PER_UNIT)-(A.EDITED_MIN_PRICE_SKU-((A.EDITED_MAX_PRICE_SKU-A.EDITED_MIN_PRICE_SKU)*1.0/9)))*1.0)/((A.EDITED_MAX_PRICE_SKU-(A.EDITED_MIN_PRICE_SKU-((A.EDITED_MAX_PRICE_SKU-A.EDITED_MIN_PRICE_SKU)/9)))*1.0) 
				 WHEN A.EDITED_MAX_PRICE_SKU = A.EDITED_MIN_PRICE_SKU THEN 1
				 END
		  ) END) AS NEW_PRICE_SCORE ,
	AVG(datediff(day,MIN_TXN_DATE,TXN_DATE)) AS AVG_DAY_FROM_MIN_DATE
FROM VPM_DATA.PM_SKUTAGGING_FASHION_SALESSKU_1YR_NEW_SCHEMA A INNER JOIN PROD B
ON A.SKU_CODE = B.PAD_SKU_ID
AND CASE WHEN A.PARTNER_CODE = 'RBS' THEN 'CDS' WHEN A.PARTNER_CODE = 'TWD' THEN 'HWS' ELSE A.PARTNER_CODE END = B.PARTNER_CODE
AND MIN_TXN_DATE > (dateadd(MONTH,1,vpm_data.get_start_date(current_timestamp))) 
WHERE TXN_DATE > vpm_data.get_start_date(current_timestamp)
  AND TXN_DATE < vpm_data.get_end_date(current_timestamp)
  AND EXISTS (SELECT * FROM SKU C WHERE A.PARTNER_CODE = C.PARTNER_CODE AND A.SKU_CODE = C.SKU_CODE)
GROUP BY MEMBER_NUMBER;

--DROP TABLE IF EXISTS TEMP_1;
SELECT * 
	,NEW_ARRIVAL_FREQ*1.0/TTL_FREQ*1.0 AS NEW_FREQ_RATIO
	,CAST(NULL AS FLOAT) AS FASHION_NEW_ARRIVAL_SCORE
INTO TEMP TABLE TEMP_1
FROM TEMP_NEW_PRICE_SCORE
;


					
CREATE temp TABLE var_FASHION_NEW_ARRIVAL_SCORE AS
SELECT member_number
	,(SELECT MIN(TTL_FREQ) FROM (SELECT TTL_FREQ, PERCENT_RANK() OVER(ORDER BY TTL_FREQ DESC) AS PCT_RANK
						FROM TEMP_1 ) A WHERE PCT_RANK <= 0.1) AS freq
FROM TEMP_1;

UPDATE TEMP_1
SET FASHION_NEW_ARRIVAL_SCORE = ((CASE WHEN TTL_FREQ >= FREQ THEN 1
                         WHEN COALESCE(TTL_FREQ,0) < FREQ THEN COALESCE(TTL_FREQ,0)*1.0/FREQ*1.0 END)
					   *(CASE WHEN NEW_FREQ_RATIO > 0.8 THEN 1
							  ELSE NEW_FREQ_RATIO*1.0/0.8 END)*0.7)
                          + (COALESCE(NEW_PRICE_SCORE,0)*0.3)
FROM var_FASHION_NEW_ARRIVAL_SCORE t
WHERE TEMP_1.member_number = t.member_number;


DROP TABLE IF EXISTS VPM_DATA.PM_FASHION_CUST_LUX_NEW_SCHEMA_1
;
ALTER TABLE VPM_DATA.PM_FASHION_CUST_LUX_NEW_SCHEMA RENAME TO PM_FASHION_CUST_LUX_NEW_SCHEMA_1
;

SELECT A.*
	,B.NEW_ARRIVAL_FREQ
	,B.NEW_PRICE_SCORE
	,B.NEW_FREQ_RATIO
	,B.FASHION_NEW_ARRIVAL_SCORE
	,B.AVG_DAY_FROM_MIN_DATE
	,CAST(NULL AS INT) AS NEW_ARRIVAL_DECILE
	,CAST(NULL AS VARCHAR(50)) AS NEW_ARRIVAL_SEGMENT
	,CAST(NULL AS FLOAT) AS FASHION_PROMO_SENSITIVITY_SCORE
	,CAST(NULL AS VARCHAR(50)) AS FASHION_PROMOTION_SENSITIVITY_SEGMENT
	,CAST(NULL AS FLOAT) AS FASHION_PURCHASE_PRICE_WITHIN_SKU_PRICE_RANGE
	,CAST(NULL AS FLOAT) AS FASHION_AVG_DISCOUNT_PERCENT
	,CAST(NULL AS FLOAT) AS FASHION_CNT_DISCOUNT_OVER_10PCT_ITEMS
	,CAST(NULL AS FLOAT) AS FASHION_CNT_DISCOUNT_ITEMS_RATIO
	,CAST(NULL AS FLOAT) AS FASHION_CNT_ALL_ITEMS
	,CAST(NULL AS FLOAT) AS FASHION_PROMO_SENSITIVITY_SCORE_FEMALE
	,CAST(NULL AS FLOAT) AS AVG_PCT_DISCOUNT_FEMALE
	,CAST(NULL AS FLOAT) AS FASHION_PROMO_SENSITIVITY_SCORE_MALE
	,CAST(NULL AS FLOAT) AS AVG_PCT_DISCOUNT_MALE
INTO VPM_DATA.PM_FASHION_CUST_LUX_NEW_SCHEMA
FROM VPM_DATA.PM_FASHION_CUST_LUX_NEW_SCHEMA_1 A LEFT JOIN TEMP_1 B
ON A.MEMBER_NUMBER = B.MEMBER_NUMBER
;

DROP TABLE IF EXISTS VPM_DATA.PM_FASHION_CUST_LUX_NEW_SCHEMA_1;

UPDATE VPM_DATA.PM_FASHION_CUST_LUX_NEW_SCHEMA
SET NEW_ARRIVAL_DECILE = A.NEW_ARRIVAL_DECILE
FROM 
	(
	SELECT MEMBER_NUMBER ,NTILE(10)OVER(ORDER BY FASHION_NEW_ARRIVAL_SCORE DESC) AS NEW_ARRIVAL_DECILE
	FROM VPM_DATA.PM_FASHION_CUST_LUX_NEW_SCHEMA
	WHERE FASHION_NEW_ARRIVAL_SCORE > 0
	) A
WHERE A.MEMBER_NUMBER = VPM_DATA.PM_FASHION_CUST_LUX_NEW_SCHEMA.MEMBER_NUMBER;  

UPDATE VPM_DATA.PM_FASHION_CUST_LUX_NEW_SCHEMA
SET NEW_ARRIVAL_SEGMENT = CASE WHEN COALESCE(FASHION_LUXURY_SEGMENT,'MASS') IN ('ACCESSIBLE LUXURY' ,'LUXURY','SUPER LUXURY') AND COALESCE(FASHION_NEW_ARRIVAL_SCORE,0) >= 0.6 THEN 'HIGH'
					           WHEN COALESCE(FASHION_LUXURY_SEGMENT,'MASS') = 'UPPER MASS' AND COALESCE(FASHION_NEW_ARRIVAL_SCORE,0) >= 0.7 THEN 'HIGH'
					           WHEN COALESCE(FASHION_LUXURY_SEGMENT,'MASS') IN ('ACCESSIBLE LUXURY' ,'LUXURY','SUPER LUXURY') AND COALESCE(FASHION_NEW_ARRIVAL_SCORE,0) >= 0.5 AND COALESCE(FASHION_NEW_ARRIVAL_SCORE,0) < 0.6 THEN 'MID'
					           WHEN COALESCE(FASHION_LUXURY_SEGMENT,'MASS') = 'UPPER MASS' AND FASHION_NEW_ARRIVAL_SCORE >= 0.6 AND COALESCE(FASHION_NEW_ARRIVAL_SCORE,0) < 0.7 THEN 'MID'
							   WHEN COALESCE(FASHION_LUXURY_SEGMENT,'MASS') IN ('ACCESSIBLE LUXURY' ,'LUXURY','SUPER LUXURY') AND COALESCE(FASHION_NEW_ARRIVAL_SCORE,0) < 0.5 AND  COALESCE(FASHION_NEW_ARRIVAL_SCORE,0) > 0 THEN 'LOW'
							   WHEN COALESCE(FASHION_LUXURY_SEGMENT,'MASS') = 'UPPER MASS' AND COALESCE(FASHION_NEW_ARRIVAL_SCORE,0) < 0.6 AND COALESCE(FASHION_NEW_ARRIVAL_SCORE,0) > 0 THEN 'LOW'
							   WHEN COALESCE(FASHION_LUXURY_SEGMENT,'MASS') = 'MASS' AND COALESCE(FASHION_NEW_ARRIVAL_SCORE,0) > 0  THEN 'LOW'
							   ELSE 'NO SCORE' END;
/*PRICE SENSITIVITY*/
/*CUSTOMER LEVEL*/

--SET MINIMUM NUMBER OF CUSTOMERS
--DROP TABLE IF EXISTS DISTINCT_SKU;
SELECT DISTINCT PARTNER_CODE,PAD_SKU_ID,AVG_PRICE
INTO TEMP TABLE DISTINCT_SKU
FROM VPM_DATA.PM_SKUTAGGING_FASHION_PRODUCT_MASTER_NEW_SCHEMA
;

--DROP TABLE IF EXISTS TEMP_MAX_MIN;
/** SELECT PRICE RANGE BY PRODUCT TYPE EXCLUDING 10% OUTLIER SKU **/
SELECT CATEGORY_FINAL,CAT_TYPE_GRP,MIN(MAX_MIN_CAT) AS MAX_MIN_CAT
INTO TEMP TABLE TEMP_MAX_MIN
FROM
(
SELECT CATEGORY_FINAL,CAT_TYPE_GRP,PARTNER_CODE,SKU_CODE,MAX(EDITED_PRICE_PER_UNIT )-MIN(EDITED_PRICE_PER_UNIT ) AS MAX_MIN_CAT
,NTILE(10)OVER(PARTITION BY CATEGORY_FINAL,CAT_TYPE_GRP ORDER BY MAX(EDITED_PRICE_PER_UNIT )-MIN(EDITED_PRICE_PER_UNIT) DESC) AS PCT_PRICE
FROM VPM_DATA.PM_SKUTAGGING_FASHION_SALESSKU_1YR_NEW_SCHEMA
GROUP BY CATEGORY_FINAL,CAT_TYPE_GRP,PARTNER_CODE,SKU_CODE
) A
WHERE PCT_PRICE = 1
GROUP BY CATEGORY_FINAL,CAT_TYPE_GRP
;

DROP TABLE IF EXISTS VPM_DATA.FASHION_SENSITIVITY_NEW_SCHEMA;
CREATE TABLE VPM_DATA.FASHION_SENSITIVITY_NEW_SCHEMA AS
SELECT A.MEMBER_NUMBER,A.GENDER,A.CLEANED_BRANDNAME,A.TYPE_FINAL,A.PRODUCT_NAME,A.EDITED_PRICE_PER_UNIT 
,A.EDITED_MAX_PRICE_SKU,A.EDITED_MIN_PRICE_SKU,B.AVG_PRICE,C.MAX_MIN_CAT,
(CASE WHEN (PRODUCT_NAME LIKE '%PROMO%' OR PRODUCT_NAME LIKE '%PRO') AND DISCOUNT < 0.1 
	  THEN 0.1 ELSE DISCOUNT END) AS DISCOUNT
FROM VPM_DATA.PM_SKUTAGGING_FASHION_SALESSKU_1YR_NEW_SCHEMA  A
INNER JOIN DISTINCT_SKU B
ON A.SKU_CODE =  B.PAD_SKU_ID
AND CASE WHEN A.PARTNER_CODE = 'RBS' THEN 'CDS' WHEN A.PARTNER_CODE = 'TWD' THEN 'HWS' ELSE A.PARTNER_CODE END = B.PARTNER_CODE
INNER JOIN TEMP_MAX_MIN C
ON A.CATEGORY_FINAL = C.CATEGORY_FINAL
AND A.CAT_TYPE_GRP = C.CAT_TYPE_GRP
WHERE TXN_DATE > vpm_data.get_start_date(current_timestamp)
AND TXN_DATE < vpm_data.get_end_date(current_timestamp)
AND EXISTS ( SELECT * FROM SKU D WHERE A.PARTNER_CODE = D.PARTNER_CODE AND A.SKU_CODE = D.SKU_CODE)
ORDER BY MEMBER_NUMBER;


/** Exclude Brand with very low %discount and unit price **/
DROP TABLE IF EXISTS VPM_DATA.BRAND_lOW_PRICE_NO_DISCOUNT_NEW_SCHEMA;
SELECT CLEANED_BRANDNAME, AVG(DISCOUNT) AS AVG_DISCOUNT,AVG(EDITED_PRICE_PER_UNIT) AS AVG_UNIT_PRICE
INTO VPM_DATA.BRAND_lOW_PRICE_NO_DISCOUNT_NEW_SCHEMA
FROM VPM_DATA.FASHION_SENSITIVITY_NEW_SCHEMA
GROUP BY CLEANED_BRANDNAME
HAVING AVG(DISCOUNT) < 0.05 and AVG(EDITED_PRICE_PER_UNIT) < 300;

DROP TABLE IF EXISTS VPM_DATA.FASHION_SENSITIVITY_NEW_SCHEMA_1;
SELECT A.*,
CASE WHEN EDITED_PRICE_PER_UNIT = AVG_PRICE THEN 0.5
	 WHEN EDITED_PRICE_PER_UNIT > AVG_PRICE 
		  AND (PRODUCT_NAME LIKE '%PROMO%' OR PRODUCT_NAME LIKE '%PRO')
		  THEN 0.5
     WHEN EDITED_PRICE_PER_UNIT > AVG_PRICE AND EDITED_MAX_PRICE_SKU-AVG_PRICE > 0 
		  THEN 0.5-(((EDITED_PRICE_PER_UNIT-AVG_PRICE)*1.0/(EDITED_MAX_PRICE_SKU-AVG_PRICE)*1.0)/2)
     WHEN EDITED_PRICE_PER_UNIT < AVG_PRICE AND AVG_PRICE-EDITED_MIN_PRICE_SKU > 0 
		  THEN 0.5+(((AVG_PRICE-EDITED_PRICE_PER_UNIT )*1.0/(AVG_PRICE-EDITED_MIN_PRICE_SKU)*1.0)/2) 
     END AS B /* Purchase Price within Price Range and Avg. Price */
,CASE WHEN EDITED_MAX_PRICE_SKU-EDITED_MIN_PRICE_SKU >= MAX_MIN_CAT 
			THEN 1
      WHEN EDITED_MAX_PRICE_SKU-EDITED_MIN_PRICE_SKU < MAX_MIN_CAT AND MAX_MIN_CAT > 0 
			THEN (EDITED_MAX_PRICE_SKU-EDITED_MIN_PRICE_SKU)*1.0 / MAX_MIN_CAT END AS A /* Product Price Range (Weight) */
INTO VPM_DATA.FASHION_SENSITIVITY_NEW_SCHEMA_1
FROM VPM_DATA.FASHION_SENSITIVITY_NEW_SCHEMA A
WHERE NOT EXISTS (SELECT CLEANED_BRANDNAME FROM VPM_DATA.BRAND_lOW_PRICE_NO_DISCOUNT_NEW_SCHEMA B WHERE A.CLEANED_BRANDNAME = B.CLEANED_BRANDNAME)
;

/*
DROP TABLE IF EXISTS FASHION_SENSITIVITY_CUST
;
SELECT MEMBER_NUMBER
,CASE WHEN SUM(A) != 0 THEN SUM(A*B)*1.0/SUM(A)*1.0 ELSE 0 END AS FASHION_PROMO_SENSITIVITY_SCORE
,CASE WHEN SUM(A) != 0 THEN SUM(A*DISCOUNT)*1.0/SUM(A)*1.0 ELSE 0 END  AS FASHION_AVG_DISCOUNT_PERCENT
INTO VPM_DATA.FASHION_SENSITIVITY_NEW_SCHEMA_CUST
FROM FASHION_SENSITIVITY_1
GROUP BY MEMBER_NUMBER
ORDER BY MEMBER_NUMBER
;
*/

DROP TABLE IF EXISTS VPM_DATA.FASHION_SENSITIVITY_CUST_NEW_SCHEMA
;
SELECT A.MEMBER_NUMBER, B.INFERRED_GENDER
,CASE WHEN SUM(A) != 0 THEN SUM(A*B)*1.0/SUM(A)*1.0 ELSE 0 END AS FASHION_PURCHASE_PRICE_WITHIN_SKU_PRICE_RANGE
,CASE WHEN SUM(A) != 0 AND SUM(EDITED_MAX_PRICE_SKU) > 0 THEN SUM(EDITED_MAX_PRICE_SKU-EDITED_PRICE_PER_UNIT)*1.0/SUM(EDITED_MAX_PRICE_SKU)*1.0 ELSE 0 END  AS FASHION_AVG_DISCOUNT_PERCENT
,SUM(CASE WHEN EDITED_MAX_PRICE_SKU > 0  THEN 
	 CASE WHEN (EDITED_MAX_PRICE_SKU-EDITED_PRICE_PER_UNIT)*1.0/(EDITED_MAX_PRICE_SKU*1.0) > 0.1 THEN 1 ELSE 0 END
	 ELSE 0 END) AS CNT_DISCOUNT_OVER_10PCT_ITEMS
,COUNT(*) AS CNT_ALL_ITEMS
,CASE WHEN SUM(CASE WHEN B.INFERRED_GENDER = 'F' AND UPPER(A.GENDER) IN ('FEMALE','UNISEX',NULL) THEN A ELSE 0 END) != 0 
	  THEN SUM(CASE WHEN B.INFERRED_GENDER = 'F' AND UPPER(A.GENDER) IN ('FEMALE','UNISEX',NULL) THEN A*B ELSE 0 END)*1.0
		  /SUM(CASE WHEN B.INFERRED_GENDER = 'F' AND UPPER(A.GENDER) IN ('FEMALE','UNISEX',NULL) THEN A ELSE 0 END)*1.0
	  WHEN SUM(CASE WHEN B.INFERRED_GENDER IN ('M','UNKNOWN') AND UPPER(A.GENDER) IN ('FEMALE') THEN A ELSE 0 END) != 0 
	  THEN SUM(CASE WHEN B.INFERRED_GENDER IN ('M','UNKNOWN') AND UPPER(A.GENDER) IN ('FEMALE') THEN A*B ELSE 0 END)*1.0
		  /SUM(CASE WHEN B.INFERRED_GENDER IN ('M','UNKNOWN') AND UPPER(A.GENDER) IN ('FEMALE') THEN A ELSE 0 END)*1.0 
	  ELSE 0 END AS FASHION_PROMO_SENSITIVITY_SCORE_FEMALE
,CASE WHEN SUM(CASE WHEN B.INFERRED_GENDER = 'M' AND UPPER(A.GENDER) IN ('MALE','UNISEX',NULL) THEN A ELSE 0 END) != 0 
	  THEN SUM(CASE WHEN B.INFERRED_GENDER = 'M' AND UPPER(A.GENDER) IN ('MALE','UNISEX',NULL) THEN A*B ELSE 0 END)*1.0
		  /SUM(CASE WHEN B.INFERRED_GENDER = 'M' AND UPPER(A.GENDER) IN ('MALE','UNISEX',NULL) THEN A ELSE 0 END)*1.0 
	  WHEN SUM(CASE WHEN B.INFERRED_GENDER IN ('F','UNKNOWN') AND UPPER(A.GENDER) IN ('MALE') THEN A ELSE 0 END) != 0 
	  THEN SUM(CASE WHEN B.INFERRED_GENDER IN ('F','UNKNOWN') AND UPPER(A.GENDER) IN ('MALE') THEN A*B ELSE 0 END)*1.0
		  /SUM(CASE WHEN B.INFERRED_GENDER IN ('F','UNKNOWN') AND UPPER(A.GENDER) IN ('MALE') THEN A ELSE 0 END)*1.0 
	  ELSE 0 END AS FASHION_PROMO_SENSITIVITY_SCORE_MALE
,CASE WHEN SUM(CASE WHEN B.INFERRED_GENDER = 'F' AND UPPER(A.GENDER) IN ('FEMALE','UNISEX',NULL) THEN A ELSE 0 END) != 0 
	  THEN SUM(CASE WHEN B.INFERRED_GENDER = 'F' AND UPPER(A.GENDER) IN ('FEMALE','UNISEX',NULL) THEN EDITED_MAX_PRICE_SKU-EDITED_PRICE_PER_UNIT ELSE 0 END)*1.0
		  /SUM(CASE WHEN B.INFERRED_GENDER = 'F' AND UPPER(A.GENDER) IN ('FEMALE','UNISEX',NULL) THEN EDITED_MAX_PRICE_SKU ELSE 0 END)*1.0
	  WHEN SUM(CASE WHEN B.INFERRED_GENDER IN ('M','UNKNOWN') AND UPPER(A.GENDER) IN ('FEMALE') THEN A ELSE 0 END) != 0 
	  THEN SUM(CASE WHEN B.INFERRED_GENDER IN ('M','UNKNOWN') AND UPPER(A.GENDER) IN ('FEMALE') THEN EDITED_MAX_PRICE_SKU-EDITED_PRICE_PER_UNIT ELSE 0 END)*1.0
		  /SUM(CASE WHEN B.INFERRED_GENDER IN ('M','UNKNOWN') AND UPPER(A.GENDER) IN ('FEMALE') THEN EDITED_MAX_PRICE_SKU ELSE 0 END)*1.0 
	  ELSE 0 END AS AVG_PCT_DISCOUNT_FEMALE
,CASE WHEN SUM(CASE WHEN B.INFERRED_GENDER = 'M' AND UPPER(A.GENDER) IN ('MALE','UNISEX',NULL) THEN A ELSE 0 END) != 0 
	  THEN SUM(CASE WHEN B.INFERRED_GENDER = 'M' AND UPPER(A.GENDER) IN ('MALE','UNISEX',NULL) THEN EDITED_MAX_PRICE_SKU-EDITED_PRICE_PER_UNIT ELSE 0 END)*1.0
		  /SUM(CASE WHEN B.INFERRED_GENDER = 'M' AND UPPER(A.GENDER) IN ('MALE','UNISEX',NULL) THEN EDITED_MAX_PRICE_SKU ELSE 0 END)*1.0 
	  WHEN SUM(CASE WHEN B.INFERRED_GENDER IN ('F','UNKNOWN') AND UPPER(A.GENDER) IN ('MALE') THEN A ELSE 0 END) != 0 
	  THEN SUM(CASE WHEN B.INFERRED_GENDER IN ('F','UNKNOWN') AND UPPER(A.GENDER) IN ('MALE') THEN EDITED_MAX_PRICE_SKU-EDITED_PRICE_PER_UNIT ELSE 0 END)*1.0
		  /SUM(CASE WHEN B.INFERRED_GENDER IN ('F','UNKNOWN') AND UPPER(A.GENDER) IN ('MALE') THEN EDITED_MAX_PRICE_SKU ELSE 0 END)*1.0 
	  ELSE 0 END AS AVG_PCT_DISCOUNT_MALE	  	  
INTO VPM_DATA.FASHION_SENSITIVITY_CUST_NEW_SCHEMA
FROM VPM_DATA.FASHION_SENSITIVITY_NEW_SCHEMA_1 A
LEFT JOIN VPM_DATA.PM_SVOC_NEW_SCHEMA B ON A.MEMBER_NUMBER = B.MEMBER_NUMBER --EDIT 2019.10.16
GROUP BY A.MEMBER_NUMBER, B.INFERRED_GENDER
;


UPDATE VPM_DATA.PM_FASHION_CUST_LUX_NEW_SCHEMA
SET FASHION_PURCHASE_PRICE_WITHIN_SKU_PRICE_RANGE = A.FASHION_PURCHASE_PRICE_WITHIN_SKU_PRICE_RANGE,
	FASHION_AVG_DISCOUNT_PERCENT = A.FASHION_AVG_DISCOUNT_PERCENT,
	FASHION_CNT_DISCOUNT_OVER_10PCT_ITEMS = A.CNT_DISCOUNT_OVER_10PCT_ITEMS,
	FASHION_CNT_DISCOUNT_ITEMS_RATIO = A.CNT_DISCOUNT_OVER_10PCT_ITEMS*1.0/CNT_ALL_ITEMS*1.0,
	FASHION_CNT_ALL_ITEMS = A.CNT_ALL_ITEMS,	
	FASHION_PROMO_SENSITIVITY_SCORE_FEMALE = A.FASHION_PROMO_SENSITIVITY_SCORE_FEMALE,
	AVG_PCT_DISCOUNT_FEMALE = A.AVG_PCT_DISCOUNT_FEMALE,
	FASHION_PROMO_SENSITIVITY_SCORE_MALE = A.FASHION_PROMO_SENSITIVITY_SCORE_MALE,
	AVG_PCT_DISCOUNT_MALE = A.AVG_PCT_DISCOUNT_MALE
FROM VPM_DATA.FASHION_SENSITIVITY_CUST_NEW_SCHEMA A
WHERE A.MEMBER_NUMBER = VPM_DATA.PM_FASHION_CUST_LUX_NEW_SCHEMA.MEMBER_NUMBER
;

UPDATE VPM_DATA.PM_FASHION_CUST_LUX_NEW_SCHEMA
SET FASHION_PROMO_SENSITIVITY_SCORE = FASHION_PURCHASE_PRICE_WITHIN_SKU_PRICE_RANGE*0.25
									  +(CASE WHEN FASHION_AVG_DISCOUNT_PERCENT > 0.7 THEN 1 ELSE FASHION_AVG_DISCOUNT_PERCENT/0.7 END)*0.25 /** 95th Percentile (Max Score) **/
									  +FASHION_CNT_DISCOUNT_ITEMS_RATIO*0.2
									  +(CASE WHEN FASHION_CNT_DISCOUNT_OVER_10PCT_ITEMS > 10 THEN 1 ELSE FASHION_CNT_DISCOUNT_OVER_10PCT_ITEMS*1.0/10.0 END)*0.2 /** 95th Percentile (Max Score) **/
									  +(CASE WHEN FASHION_CNT_ALL_ITEMS > 12 THEN 1 ELSE FASHION_CNT_ALL_ITEMS*1.0/12.0 END)*0.1 /** 90th Percentile (Max Score) **/
;


UPDATE VPM_DATA.PM_FASHION_CUST_LUX_NEW_SCHEMA
SET FASHION_PROMOTION_SENSITIVITY_SEGMENT = CASE WHEN FASHION_PROMO_SENSITIVITY_SCORE > 0.70 THEN 'HIGH'
												  WHEN FASHION_PROMO_SENSITIVITY_SCORE > 0.5 AND FASHION_PROMO_SENSITIVITY_SCORE <= 0.70 THEN 'MID' 
												  WHEN FASHION_PROMO_SENSITIVITY_SCORE <= 0.5 AND FASHION_PROMO_SENSITIVITY_SCORE > 0 THEN 'LOW'
												  ELSE 'NO SCORE' END
;



UPDATE VPM_DATA.PM_SKUTAGGING_FASHION_CUST_SEGMENT_NEW_SCHEMA
SET FASHION_NEW_ARRIVAL_SCORE = A.FASHION_NEW_ARRIVAL_SCORE
	,NEW_ARRIVAL_SEGMENT = A.NEW_ARRIVAL_SEGMENT
FROM VPM_DATA.PM_FASHION_CUST_LUX_NEW_SCHEMA A
WHERE VPM_DATA.PM_SKUTAGGING_FASHION_CUST_SEGMENT_NEW_SCHEMA.MEMBER_NUMBER = A.MEMBER_NUMBER
;

UPDATE VPM_DATA.PM_SKUTAGGING_FASHION_CUST_SEGMENT_NEW_SCHEMA
SET FASHION_PROMO_SENSITIVITY_SCORE = A.FASHION_PROMO_SENSITIVITY_SCORE,
FASHION_PROMO_SENSITIVITY_SEGMENT = A.FASHION_PROMOTION_SENSITIVITY_SEGMENT,
FASHION_AVG_DISCOUNT_PERCENT = A.FASHION_AVG_DISCOUNT_PERCENT,
FASHION_PROMO_SENSITIVITY_SCORE_FEMALE = A.FASHION_PROMO_SENSITIVITY_SCORE_FEMALE,
FASHION_AVG_DISCOUNT_PERCENT_FEMALE = A.AVG_PCT_DISCOUNT_FEMALE,
FASHION_PROMO_SENSITIVITY_SCORE_MALE = A.FASHION_PROMO_SENSITIVITY_SCORE_MALE,
FASHION_AVG_DISCOUNT_PERCENT_MALE = A.AVG_PCT_DISCOUNT_MALE
FROM VPM_DATA.PM_FASHION_CUST_LUX_NEW_SCHEMA A
WHERE A.MEMBER_NUMBER = VPM_DATA.PM_SKUTAGGING_FASHION_CUST_SEGMENT_NEW_SCHEMA.MEMBER_NUMBER
;


UPDATE VPM_DATA.SALESSKU_LOG_TIME SET end_time = current_timestamp WHERE task = 'B.FASHION STEP 3.0 - CREATE_FASHION_CUST_PRICE_SENSITIVE_NEW_ARRIVAL' AND EXTRACT(MONTH FROM start_time) = EXTRACT(MONTH FROM current_timestamp)
;
