DROP TABLE IF EXISTS LOCATION_VALIDATION;
CREATE TEMP TABLE LOCATION_VALIDATION
(
TABLE_NAME VARCHAR(100)
,SEGMENT VARCHAR(100)
,SEGMENT_VALUE VARCHAR(100)
,TOTAL_CUST INT
)
;


INSERT INTO LOCATION_VALIDATION
SELECT 'GEO_CUST_SVOC_ALL_BU_VISIT_IN_MALL_6M_NEW_SCHEMA' AS TABLE_NAME
,'MOST_VISITED_MALL' AS SEGMENT
,COALESCE(CASE WHEN MOST_VISITED_MALL in ('','Non-Mall BUs') THEN 'N/A' ELSE MOST_VISITED_MALL END,'N/A') AS SEGMENT_VALUE
,COUNT(DISTINCT MEMBER_NUMBER) AS TOTAL_CUST
FROM VPM_BACKUP.GEO_CUST_SVOC_ALL_BU_VISIT_IN_MALL_6M_NEW_SCHEMA
GROUP BY COALESCE(CASE WHEN MOST_VISITED_MALL in ('','Non-Mall BUs') THEN 'N/A' ELSE MOST_VISITED_MALL END,'N/A')
;
INSERT INTO LOCATION_VALIDATION
SELECT 'GEO_CUST_SVOC_ALL_BU_VISIT_IN_MALL_6M_NEW_SCHEMA' AS TABLE_NAME
,'MALLS_VISIT_SEGMENT' AS SEGMENT
,COALESCE(CASE WHEN MALLS_VISIT_SEGMENT = '' THEN 'N/A' ELSE MALLS_VISIT_SEGMENT END,'N/A') AS SEGMENT_VALUE
,COUNT(DISTINCT MEMBER_NUMBER) AS TOTAL_CUST
FROM VPM_BACKUP.GEO_CUST_SVOC_ALL_BU_VISIT_IN_MALL_6M_NEW_SCHEMA
GROUP BY COALESCE(CASE WHEN MALLS_VISIT_SEGMENT = '' THEN 'N/A' ELSE MALLS_VISIT_SEGMENT END,'N/A')
;
INSERT INTO LOCATION_VALIDATION
SELECT 'GEO_CUST_SVOC_TIME_SPENDING_NEW_SCHEMA' AS TABLE_NAME
,'PREFERRED_TIME_SEGMENT' AS SEGMENT
,COALESCE(CASE WHEN PREFERRED_TIME_SEGMENT = '' THEN 'N/A' ELSE PREFERRED_TIME_SEGMENT END,'N/A') AS SEGMENT_VALUE
,COUNT(DISTINCT MEMBER_NUMBER) AS TOTAL_CUST
FROM VPM_BACKUP.GEO_CUST_SVOC_TIME_SPENDING_NEW_SCHEMA
GROUP BY COALESCE(CASE WHEN PREFERRED_TIME_SEGMENT = '' THEN 'N/A' ELSE PREFERRED_TIME_SEGMENT END,'N/A')
;
INSERT INTO LOCATION_VALIDATION
SELECT 'GEO_CUST_VISIT_MAIN_BU_BRANCHES_NEW_SCHEMA' AS TABLE_NAME
,'MALL_BRANCH_NAME' AS SEGMENT
,COALESCE(CASE WHEN MALL_BRANCH_NAME = '' THEN 'N/A' ELSE MALL_BRANCH_NAME END,'N/A') AS SEGMENT_VALUE
,COUNT(DISTINCT MEMBER_NUMBER) AS TOTAL_CUST
FROM VPM_BACKUP.geo_cust_visit_main_bu_branches_new_schema
GROUP BY COALESCE(CASE WHEN MALL_BRANCH_NAME = '' THEN 'N/A' ELSE MALL_BRANCH_NAME END,'N/A')
;
INSERT INTO LOCATION_VALIDATION
SELECT 'GEO_CUST_VISIT_MAIN_BU_BRANCHES_NEW_SCHEMA' AS TABLE_NAME
,'MAIN_BU' AS SEGMENT
,COALESCE(partner_code,'N/A') AS SEGMENT_VALUE
,COUNT(DISTINCT MEMBER_NUMBER) AS TOTAL_CUST
FROM VPM_BACKUP.GEO_CUST_VISIT_MAIN_BU_BRANCHES_NEW_SCHEMA
WHERE SEQ = 1
GROUP BY COALESCE(partner_code,'N/A')
;
INSERT INTO LOCATION_VALIDATION
SELECT 'GEO_CUST_VISIT_MAIN_BU_BRANCHES_NEW_SCHEMA' AS TABLE_NAME
,'VISITED_BU_BRANCH1' AS SEGMENT
,COALESCE(MALL_BRANCH_NAME,'N/A') AS SEGMENT_VALUE
,COUNT(DISTINCT MEMBER_NUMBER) AS TOTAL_CUST
FROM VPM_BACKUP.GEO_CUST_VISIT_MAIN_BU_BRANCHES_NEW_SCHEMA
WHERE SEQ = 1
GROUP BY COALESCE(MALL_BRANCH_NAME,'N/A')
;
INSERT INTO LOCATION_VALIDATION
SELECT 'GEO_CUST_VISIT_MAIN_BU_BRANCHES_NEW_SCHEMA' AS TABLE_NAME
,'VISITED_BU_BRANCH2' AS SEGMENT
,COALESCE(MALL_BRANCH_NAME,'N/A') AS SEGMENT_VALUE
,COUNT(DISTINCT MEMBER_NUMBER) AS TOTAL_CUST
FROM VPM_BACKUP.GEO_CUST_VISIT_MAIN_BU_BRANCHES_NEW_SCHEMA
WHERE SEQ = 2
GROUP BY COALESCE(MALL_BRANCH_NAME,'N/A')
;
INSERT INTO LOCATION_VALIDATION
SELECT 'GEO_CUST_VISIT_MAIN_BU_BRANCHES_NEW_SCHEMA' AS TABLE_NAME
,'VISITED_BU_BRANCH3' AS SEGMENT
,COALESCE(MALL_BRANCH_NAME,'N/A') AS SEGMENT_VALUE
,COUNT(DISTINCT MEMBER_NUMBER) AS TOTAL_CUST
FROM VPM_BACKUP.GEO_CUST_VISIT_MAIN_BU_BRANCHES_NEW_SCHEMA
WHERE SEQ = 3
GROUP BY COALESCE(MALL_BRANCH_NAME,'N/A')
;
INSERT INTO LOCATION_VALIDATION
SELECT 'GEO_CUST_SVOC_LOCATION_VISIT_NEW_SCHEMA' AS TABLE_NAME
,'CUST_LOCATION_SEGMENT' AS SEGMENT
,COALESCE(CASE WHEN CUST_LOCATION_SEGMENT IN ('') THEN 'N/A' ELSE CUST_LOCATION_SEGMENT END,'N/A') AS SEGMENT_VALUE
,COUNT(DISTINCT MEMBER_NUMBER) AS TOTAL_CUST
FROM VPM_BACKUP.GEO_CUST_SVOC_LOCATION_VISIT_NEW_SCHEMA
GROUP BY COALESCE(CASE WHEN CUST_LOCATION_SEGMENT IN ('') THEN 'N/A' ELSE CUST_LOCATION_SEGMENT END,'N/A')
;
INSERT INTO LOCATION_VALIDATION
SELECT 'GEO_CUST_SVOC_LOCATION_VISIT_NEW_SCHEMA' AS TABLE_NAME
,'MOST_VISIT_PROVINCE1' AS SEGMENT
,COALESCE(MOST_VISIT_PROVINCE1,'N/A') AS SEGMENT_VALUE
,COUNT(DISTINCT MEMBER_NUMBER) AS TOTAL_CUST
FROM VPM_BACKUP.GEO_CUST_SVOC_LOCATION_VISIT_NEW_SCHEMA
GROUP BY COALESCE(MOST_VISIT_PROVINCE1,'N/A')
;
INSERT INTO LOCATION_VALIDATION
SELECT 'GEO_CUST_SVOC_LOCATION_VISIT_NEW_SCHEMA' AS TABLE_NAME
,'MOST_VISIT_PROVINCE2' AS SEGMENT
,COALESCE(MOST_VISIT_PROVINCE2,'N/A') AS SEGMENT_VALUE
,COUNT(DISTINCT MEMBER_NUMBER) AS TOTAL_CUST
FROM VPM_BACKUP.GEO_CUST_SVOC_LOCATION_VISIT_NEW_SCHEMA
GROUP BY COALESCE(MOST_VISIT_PROVINCE2,'N/A')
;
INSERT INTO LOCATION_VALIDATION
SELECT 'GEO_CUST_SVOC_LOCATION_VISIT_NEW_SCHEMA' AS TABLE_NAME
,'MOST_VISIT_REGION1' AS SEGMENT
,COALESCE(MOST_VISIT_REGION1,'N/A') AS SEGMENT_VALUE
,COUNT(DISTINCT MEMBER_NUMBER) AS TOTAL_CUST
FROM VPM_BACKUP.GEO_CUST_SVOC_LOCATION_VISIT_NEW_SCHEMA
GROUP BY COALESCE(MOST_VISIT_REGION1,'N/A')
;
INSERT INTO LOCATION_VALIDATION
SELECT 'GEO_CUST_SVOC_LOCATION_VISIT_NEW_SCHEMA' AS TABLE_NAME
,'MOST_VISIT_SUB_REGION1' AS SEGMENT
,COALESCE(MOST_VISIT_SUB_REGION1,'N/A') AS SEGMENT_VALUE
,COUNT(DISTINCT MEMBER_NUMBER) AS TOTAL_CUST
FROM VPM_BACKUP.GEO_CUST_SVOC_LOCATION_VISIT_NEW_SCHEMA
GROUP BY COALESCE(MOST_VISIT_SUB_REGION1,'N/A');




SELECT * FROM LOCATION_VALIDATION ORDER BY TABLE_NAME,SEGMENT,SEGMENT_VALUE;
--UNION ALL
--SELECT 'GEO_CUST_SVOC_LOCATION_VISIT_NEW_SCHEMA' AS TABLE_NAME
--,'' AS SEGMENT
--, AS SEGMENT_VALUE
--,COUNT(DISTINCT MEMBER_NUMBER) AS TOTAL_CUST
--FROM VPM_BACKUP.GEO_CUST_SVOC_LOCATION_VISIT_NEW_SCHEMA
--GROUP BY 