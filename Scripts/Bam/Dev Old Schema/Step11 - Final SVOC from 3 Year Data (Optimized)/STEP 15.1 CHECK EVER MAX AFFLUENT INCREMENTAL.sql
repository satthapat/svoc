DROP TABLE IF EXISTS VPM_DATA.cte_aff_previous;
DROP TABLE IF exists VPM_DATA.cte_aff_incremental;
DROP TABLE IF exists vpm_data.B_AFFLUENT_INCREMENTAL;

DO $$

DECLARE PREVIOUS_MONTH_CHAR TEXT := (SELECT TO_CHAR(NOW()-INTERVAL '1 MONTH','MON'));

BEGIN
EXECUTE FORMAT('create table VPM_DATA.cte_aff_previous as
SELECT CUSTOMER_ID
FROM vpm_backup.pm_svoc_master
WHERE wealth_v6_segment  = ''B.AFFLUENT''
;

create table VPM_DATA.cte_aff_incremental as
SELECT A.CUSTOMER_ID
FROM vpm_backup.pm_ever_max_wealth A WHERE wealth_v6_segment  = ''B.AFFLUENT''
AND NOT EXISTS (SELECT * FROM VPM_DATA.cte_aff_previous b WHERE a.customer_id = b.customer_id)
;

create table vpm_data.B_AFFLUENT_INCREMENTAL as
SELECT A.*
FROM VPM_DATA.P_PM_AFFLUENT_VERFIED_WEALTH_FLAG A
INNER JOIN VPM_DATA.cte_aff_incremental B ON A.customer_id = B.CUSTOMER_ID
;

')
;

END $$;


--90035
SELECT COUNT(DISTINCT CUSTOMER_ID) FROM vpm_data.B_AFFLUENT_INCREMENTAL
;
--1388
SELECT COUNT(DISTINCT CUSTOMER_ID) FROM vpm_data.B_AFFLUENT_INCREMENTAL
WHERE THE_LUXE_FLAG = 1;

--5229
SELECT COUNT(DISTINCT CUSTOMER_ID) FROM vpm_data.B_AFFLUENT_INCREMENTAL
WHERE CC2_10_FLAG = 1;

--4
SELECT COUNT(DISTINCT CUSTOMER_ID) FROM vpm_data.B_AFFLUENT_INCREMENTAL
WHERE VIP_FLAG = 1;

SELECT new_wealth_desc,count(*) FROM vpm_data.B_AFFLUENT_INCREMENTAL GROUP BY new_wealth_desc ORDER BY new_wealth_desc;


SELECT A.CUSTOMER_ID,A.beauty_luxury_segment ,A.fashion_luxury_segment ,A.food_grocery_luxury_segment ,A.dining_luxury_segment 
,A.luxury_hospital_flag
FROM vpm_backup.PM_SVOC_MASTER_JAN A
INNER JOIN (SELECT * FROM VPM_DATA.B_AFFLUENT_INCREMENTAL WHERE NEW_WEALTH_DESC IS NULL) B
ON A.CUSTOMER_ID = B.CUSTOMER_ID



--DROP TABLE IF EXISTS VPM_DATA.cte_aff_previous;
--SELECT CUSTOMER_ID
--INTO VPM_DATA.cte_aff_previous
--FROM analysis_data.w_revised_wealth_v6_customer_segment_wb_20201204
--WHERE REVISED_wealth_v6_segment  = 'B.AFFLUENT'
--;
--
--DROP TABLE IF exists VPM_DATA.cte_aff_incremental;
--SELECT A.CUSTOMER_ID 
--INTO VPM_DATA.cte_aff_incremental
--FROM vpm_backup.pm_ever_max_wealth A WHERE wealth_v6_segment  = 'B.AFFLUENT'
--AND NOT EXISTS (SELECT * FROM VPM_DATA.cte_aff_previous b WHERE a.customer_id = b.customer_id)
--;
--DROP TABLE vpm_data.B_AFFLUENT_INCREMENTAL_1220;
--SELECT A.*
--INTO vpm_data.B_AFFLUENT_INCREMENTAL_1220
--FROM VPM_DATA.P_PM_AFFLUENT_VERFIED_WEALTH_FLAG A
--INNER JOIN VPM_DATA.cte_aff_incremental B ON A.customer_id = B.CUSTOMER_ID
--;
--
--
----90035
--SELECT COUNT(DISTINCT CUSTOMER_ID) FROM vpm_data.B_AFFLUENT_INCREMENTAL_1220
--;
----1388
--SELECT COUNT(DISTINCT CUSTOMER_ID) FROM vpm_data.B_AFFLUENT_INCREMENTAL_1220
--WHERE THE_LUXE_FLAG = 1;
--
----5229
--SELECT COUNT(DISTINCT CUSTOMER_ID) FROM vpm_data.B_AFFLUENT_INCREMENTAL_1220
--WHERE CC2_10_FLAG = 1;
--
----4
--SELECT COUNT(DISTINCT CUSTOMER_ID) FROM vpm_data.B_AFFLUENT_INCREMENTAL_1220
--WHERE VIP_FLAG = 1;
--
--SELECT new_wealth_desc,count(*) FROM vpm_data.B_AFFLUENT_INCREMENTAL_1220 GROUP BY new_wealth_desc ORDER BY new_wealth_desc