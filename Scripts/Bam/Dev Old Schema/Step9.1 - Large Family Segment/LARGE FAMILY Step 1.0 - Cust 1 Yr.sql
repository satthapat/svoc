--*********************************************home_electronics*************************************************--
--*****big size*****--
INSERT INTO VPM_DATA.SALESSKU_LOG_TIME(task,start_time) VALUES ('LARGE FAMILY Step 1.0 - Cust 1 Yr',NOW());

drop table if exists vpm_data.tmp_rice_cooker;
select distinct customer_id 
into vpm_data.tmp_rice_cooker
from vpm_data.pm_skutagging_electronics_salessku_3yr 
where type_final = 'RICE COOKER' and  (cast(size_final as float) >= 1.8) and customer_id is not null;

drop table if exists vpm_data.tmp_refrigerator;
select distinct customer_id 
into vpm_data.tmp_refrigerator
from vpm_data.pm_skutagging_electronics_salessku_3yr 
where type_final = 'REFRIGERATOR' and  (cast(size_final as float) >= 20.0) and customer_id is not null;

--*****high quantity*****--
drop table if exists vpm_data.tmp_elec_qty;
select customer_id,type_final, sum(qty) as total_qty 
into vpm_data.tmp_elec_qty
from vpm_data.pm_skutagging_electronics_salessku_3yr
where revised_category_level_1 = 'HOME ELECTRONICS'
group by customer_id,type_final;

drop table if exists vpm_data.tmp_elec_qty2;
select customer_id,type_final, sum(qty) as total_qty 
into vpm_data.tmp_elec_qty2
from vpm_data.pm_skutagging_electronics_salessku_3yr
where revised_category_level_1 = 'HOME ELECTRONICS' and type_final in ('REFRIGERATOR') and (cast(size_final as float) >= 2.0)
group by customer_id,type_final;

drop table if exists vpm_data.tmp_elec_morethan3_qty;
select distinct customer_id
into vpm_data.tmp_elec_morethan3_qty
from vpm_data.tmp_elec_qty
where type_final in ('AIR CONDITIONER','AIR PURIFIER','FAN','TV','WATER HEATER') and total_qty > 3 and customer_id is not null;

drop table if exists vpm_data.tmp_elec_washer_qty;
select distinct customer_id
into vpm_data.tmp_elec_washer_qty
from vpm_data.tmp_elec_qty
where type_final in ('WASHER & DRYER') and total_qty > 1 and customer_id is not null;

drop table if exists vpm_data.tmp_elec_refrigerator_qty;
select distinct customer_id
into vpm_data.tmp_elec_refrigerator_qty
from vpm_data.tmp_elec_qty2
where type_final in ('REFRIGERATOR') and total_qty > 1 and customer_id is not null;

drop table if exists vpm_data.tmp_a_large_family_electronics_3yr_cust;
select distinct customer_id
into vpm_data.tmp_a_large_family_electronics_3yr_cust
from vpm_data.tmp_rice_cooker
union
select distinct customer_id
from vpm_data.tmp_refrigerator
union
select distinct customer_id
from vpm_data.tmp_elec_morethan3_qty
union
select distinct customer_id
from vpm_data.tmp_elec_washer_qty
union
select distinct customer_id
from vpm_data.tmp_elec_refrigerator_qty;


/*alter table vpm_data.tmp_a_large_family_electronics_3yr_cust
drop column rice_cooker_bigsize_flag 
           ,refrigerator_bigsize_flag
;*/

ALTER TABLE vpm_data.tmp_a_large_family_electronics_3yr_cust
ADD rice_cooker_bigsize_flag int
   ,ADD refrigerator_bigsize_flag int;

update vpm_data.tmp_a_large_family_electronics_3yr_cust
set rice_cooker_bigsize_flag = case when (customer_id in (select distinct customer_id from vpm_data.tmp_rice_cooker)) then 1 else 0 end
   ,refrigerator_bigsize_flag = case when (customer_id in (select distinct customer_id from vpm_data.tmp_refrigerator)) then 1 else 0 end;

/*alter table vpm_data.tmp_a_large_family_electronics_3yr_cust
drop column home_elec_morethan3_flag 
           ,washer_dryer_morethan1_flag
		   ,refrigerator_morethan1_flag
;*/

alter table vpm_data.tmp_a_large_family_electronics_3yr_cust
add home_elec_morethan3_flag int
   ,ADD washer_dryer_morethan1_flag int
   ,ADD refrigerator_morethan1_flag int;

update vpm_data.tmp_a_large_family_electronics_3yr_cust
set home_elec_morethan3_flag = case when (customer_id in (select distinct customer_id from vpm_data.tmp_elec_morethan3_qty)) then 1 else 0 end
   ,washer_dryer_morethan1_flag = case when (customer_id in (select distinct customer_id from vpm_data.tmp_elec_washer_qty)) then 1 else 0 end
   ,refrigerator_morethan1_flag = case when (customer_id in (select distinct customer_id from vpm_data.tmp_elec_refrigerator_qty)) then 1 else 0 end;

/*alter table vpm_data.tmp_a_large_family_electronics_3yr_cust
drop column home_elec_bigsize_highquantity_flag
;*/

alter table vpm_data.tmp_a_large_family_electronics_3yr_cust
add home_elec_bigsize_highquantity_flag int;

update vpm_data.tmp_a_large_family_electronics_3yr_cust
set home_elec_bigsize_highquantity_flag = case when (rice_cooker_bigsize_flag = 1 or refrigerator_bigsize_flag = 1 or home_elec_morethan3_flag = 1 or washer_dryer_morethan1_flag = 1 or refrigerator_morethan1_flag = 1) then 1 else 0 end;

        
--*********************************************home_non_electronics*************************************************--
drop table if exists vpm_data.tmp_bed_qty;
select customer_id,type_final, sum(qty) as total_qty 
into vpm_data.tmp_bed_qty
from vpm_data.pm_home_product_salessku_1yr
group by customer_id,type_final;

drop table if exists vpm_data.tmp_bed;
select distinct customer_id
into vpm_data.tmp_bed
from vpm_data.tmp_bed_qty
where type_final in ('BED','MATTRESS & ACCESSORIES','DRESSING TABLE') and total_qty >= 3 and customer_id is not null;

drop table if exists vpm_data.tmp_bed_accessory;
select distinct customer_id
into vpm_data.tmp_bed_accessory
from vpm_data.tmp_bed_qty
where type_final in ('BEDLINEN','BEDROOM SET','PILLOW') and total_qty >= 6 and customer_id is not null;

drop table if exists vpm_data.tmp_a_large_family_home_non_cust;
select distinct customer_id
into vpm_data.tmp_a_large_family_home_non_cust
from vpm_data.tmp_bed
union
select distinct customer_id
from vpm_data.tmp_bed_accessory;

/*alter table vpm_data.tmp_a_large_family_home_non_cust
drop column bed_morethan2_flag 
           ,bed_accessory_morethan5_flag
;*/

alter table vpm_data.tmp_a_large_family_home_non_cust
add bed_morethan2_flag int
   ,ADD bed_accessory_morethan5_flag int;

update vpm_data.tmp_a_large_family_home_non_cust
set bed_morethan2_flag = case when (customer_id in (select distinct customer_id from vpm_data.tmp_bed)) then 1 else 0 end
   ,bed_accessory_morethan5_flag = case when (customer_id in (select distinct customer_id from vpm_data.tmp_bed_accessory)) then 1 else 0 end;

/*alter table vpm_data.tmp_a_large_family_home_non_cust
drop column home_non_elec_highquantity_flag;*/

alter table vpm_data.tmp_a_large_family_home_non_cust
add home_non_elec_highquantity_flag int;

update vpm_data.tmp_a_large_family_home_non_cust
set home_non_elec_highquantity_flag = case when (bed_morethan2_flag = 1 or bed_accessory_morethan5_flag = 1) then 1 else 0 end;

--*********************************************kids*************************************************--
drop table if exists vpm_data.tmp_a_large_family_kids_3stage_and_declared_cust;
select customer_id
               ,kid_infant
           ,kid_toddler
		   ,kid_presch
		   ,kid_junior
		   ,kid_teenage
		   ,case when (coalesce(kid_infant,0) + coalesce(kid_toddler,0) + coalesce(kid_presch,0) + coalesce(kid_junior,0) + coalesce(kid_teenage,0)) >= 3 then 1 else 0 end as kid_3stage_valid_flag
		   ,noofchildren  
into vpm_data.tmp_a_large_family_kids_3stage_and_declared_cust
from vpm_data.pm_svoc_kids_new_feb
where (coalesce(kid_infant,0) + coalesce(kid_toddler,0) + coalesce(kid_presch,0) + coalesce(kid_junior,0) + coalesce(kid_teenage,0)) >= 3 
UNION
select customer_id
               ,kid_infant
           ,kid_toddler
		   ,kid_presch
		   ,kid_junior
		   ,kid_teenage
		   ,case when (coalesce(kid_infant,0) + coalesce(kid_toddler,0) + coalesce(kid_presch,0) + coalesce(kid_junior,0) + coalesce(kid_teenage,0)) >= 3 then 1 else 0 end as kid_3stage_valid_flag
		   ,noofchildren  
--into vpm_data.tmp_a_large_family_kids_3stage_and_declared_cust
from vpm_data.pm_svoc_kids_new_feb
WHERE noofchildren::INT >= 3 AND noofchildren <> '';
--*********************************************fashion*************************************************-- 
drop table if exists vpm_data.tmp_fashion_sale ;
 select a.customer_id
      --,a.branchid
      --,a.branchenglishname
      --,a.location_name
      --,a.province_eng
      --,a.txn_datetime
      --,a.txn_date
      --,a.ticket_num
      --,a.sku_code
      --,a.txn_amt
      ,a.qty
      --,a.bu_name
      --,a.dept_name
      --,a.subdept_name
      --,a.class_name
      --,a.subclass_name
      --,a.sub_cat_name
      --,a.cleaned_brandname
      --,a.product_name
      --,a.type_final
      --,a.weekday_flag
      --,a.revised_category_level_1
      --,a.revised_category_level_2
      --,a.revised_category_level_3
      --,a.sales_channel_id
	  ,case when a.revised_category_level_1 = 'KIDS' then 'KIDS' else coalesce(a.revised_category_level_2,'OTHER') end as men_female_unisex_kid
 into vpm_data.tmp_fashion_sale 
  from vpm_data.pm_all_salessku_1yr a
  where a.revised_category_level_1 = 'FASHION' or (a.revised_category_level_1 = 'KIDS' and a.revised_category_level_2 = 'KID FASHION')
  union all 
 select a.customer_id
      --,a.branchid
      --,a.branchenglishname
      --,a.location_name
      --,a.province_eng
      --,a.txn_datetime
      --,a.txn_date
      --,a.ticket_num
      --,a.sku_code
      --,a.txn_amt
      ,a.qty
      --,a.bu_name
      --,a.dept_name
      --,a.subdept_name
      --,a.class_name
      --,a.subclass_name
      --,a.sub_cat_name
      --,a.cleaned_brandname
      --,a.product_name
      --,a.type_final
      --,a.weekday_flag
      --,a.revised_category_level_1
      --,a.revised_category_level_2
      --,a.revised_category_level_3
      --,a.sales_channel_id
	  ,case when a.revised_category_level_1 = 'KIDS' then 'KIDS' else coalesce(a.revised_category_level_2,'OTHER') end as men_female_unisex_kid
	  from vpm_data.pm_tops_salessku_1yr a
      where a.revised_category_level_1 = 'FASHION' or (a.revised_category_level_1 = 'KIDS' and a.revised_category_level_2 = 'KID FASHION')
	   union all 
      select a.customer_id
      --,a.branchid
      --,a.branchenglishname
      --,a.location_name
      --,a.province_eng
      --,a.txn_datetime
      --,a.txn_date
      --,a.ticket_num
      --,a.sku_code
      --,a.txn_amt
      ,a.qty
      --,a.bu_name
      --,a.dept_name
      --,a.subdept_name
      --,a.class_name
      --,a.subclass_name
      --,a.sub_cat_name
      --,a.cleaned_brandname
      --,a.product_name
      --,a.type_final
      --,a.weekday_flag
      --,a.revised_category_level_1
      --,a.revised_category_level_2
      --,a.revised_category_level_3
      --,a.sales_channel_id
	  ,case when a.revised_category_level_1 = 'KIDS' then 'KIDS' else coalesce(a.revised_category_level_2,'OTHER') end as men_female_unisex_kid
	  from vpm_data.pm_cfm_salessku_1yr a
      where a.revised_category_level_1 = 'FASHION' or (a.revised_category_level_1 = 'KIDS' and a.revised_category_level_2 = 'KID FASHION');

drop table if exists vpm_data.tmp_fashion_kids;
select distinct customer_id 
into vpm_data.tmp_fashion_kids
from vpm_data.tmp_fashion_sale
where men_female_unisex_kid = 'KIDS'
group by customer_id
having sum(qty) >= 3;

drop table if exists vpm_data.tmp_fashion_women;
select distinct customer_id 
into vpm_data.tmp_fashion_women
from vpm_data.tmp_fashion_sale
where men_female_unisex_kid = 'WOMEN' 
group by customer_id
having sum(qty) >= 3;

drop table if exists vpm_data.tmp_fashion_men;
select distinct customer_id 
into vpm_data.tmp_fashion_men
from vpm_data.tmp_fashion_sale
where men_female_unisex_kid = 'MEN' 
group by customer_id
having sum(qty) >= 3;

drop table if exists vpm_data.tmp_fashion_unisex;
select distinct customer_id 
into vpm_data.tmp_fashion_unisex
from vpm_data.tmp_fashion_sale
where men_female_unisex_kid = 'UNISEX'
group by customer_id
having sum(qty) >= 3;

drop table if exists vpm_data.tmp_fashion_various_gender_cust;
select distinct a.customer_id
into vpm_data.tmp_fashion_various_gender_cust
from vpm_data.tmp_fashion_kids a
	 inner join vpm_data.tmp_fashion_women b on a.customer_id = b.customer_id
	 inner join vpm_data.tmp_fashion_men c on a.customer_id = c.customer_id
	 inner join vpm_data.tmp_fashion_unisex d on a.customer_id = d.customer_id;

--*********************************************agg table*************************************************--
drop table if exists vpm_data.tmp_sales_all;
select a.customer_id
      --,a.buid
      --,a.txn_datetime
      ,a.txn_date
      ,a.ticket_num
      --,a.sku_code
      ,a.txn_amt
      ,a.qty
      --,a.bu_name
      --,a.cleaned_brandname
      --,a.type_final
      --,a.weekday_flag
      ,a.revised_category_level_1
      --,a.revised_category_level_2
      ,a.revised_category_level_3
      --,a.sales_channel_id
into vpm_data.tmp_sales_all
from vpm_data.pm_all_salessku_1yr a
where coalesce(txn_amt,0) <> 0 
	  and revised_category_level_1 in ('FOOD GROCERY','HOME GROCERY','HBA','HOME NON-ELECTRONICS')
union all
select a.customer_id
      --,a.buid
      --,a.txn_datetime
      ,a.txn_date
      ,a.ticket_num
      --,a.sku_code
      ,a.txn_amt
      ,a.qty
      --,a.bu_name
      --,a.cleaned_brandname
      --,a.type_final
      --,a.weekday_flag
      ,a.revised_category_level_1
      --,a.revised_category_level_2
      ,a.revised_category_level_3
      --,a.sales_channel_id
from vpm_data.pm_cfm_salessku_1yr a
where coalesce(txn_amt,0) <> 0 
      and revised_category_level_1 in ('FOOD GROCERY','HOME GROCERY','HBA','HOME NON-ELECTRONICS')
union all
select a.customer_id
      --,a.buid
      --,a.txn_datetime
      ,a.txn_date
      ,a.ticket_num
      --,a.sku_code
      ,a.txn_amt
      ,a.qty
      --,a.bu_name
      --,a.cleaned_brandname
      --,a.type_final
      --,a.weekday_flag
      ,a.revised_category_level_1
      --,a.revised_category_level_2
      ,a.revised_category_level_3
      --,a.sales_channel_id
from vpm_data.pm_tops_salessku_1yr a
where coalesce(txn_amt,0) <> 0 
	  and revised_category_level_1 in ('FOOD GROCERY','HOME GROCERY','HBA','HOME NON-ELECTRONICS');

drop table if exists vpm_data.pm_large_family_cat1_agg_1yr;
select customer_id
      ,coalesce(revised_category_level_1,'OTHER') as revised_category_level_1
	  ,sum(txn_amt) as total_spending
      ,count(distinct ticket_num) as total_ticket
      ,sum(qty) as total_qty
      ,count(distinct txn_date) as total_day_visit
	  ,count(distinct revised_category_level_3) as total_product_variety
into vpm_data.pm_large_family_cat1_agg_1yr
from vpm_data.tmp_sales_all
group by customer_id
	  ,coalesce(revised_category_level_1,'OTHER');

drop table if exists vpm_data.tmp_foodgrocery1;
select distinct customer_id
into vpm_data.tmp_foodgrocery1
from vpm_data.pm_large_family_cat1_agg_1yr
where revised_category_level_1 = 'FOOD GROCERY' and total_qty >= 260 and total_day_visit >= 15 
	and (case when total_spending >=16000 then 1 else 0 end + case when total_day_visit >= 30 then 1 else 0 end + case when total_product_variety >= 22 then 1 else 0 end >=2) and customer_id is not null;

drop table if exists vpm_data.tmp_homegrocery1;
select distinct customer_id
into vpm_data.tmp_homegrocery1
from vpm_data.pm_large_family_cat1_agg_1yr
where revised_category_level_1 = 'HOME GROCERY' and total_qty >= 40 and total_day_visit >= 5 
	and (case when total_spending >=3000 then 1 else 0 end + case when total_day_visit >= 10 then 1 else 0 end + case when total_product_variety >= 4 then 1 else 0 end >=2) and customer_id is not null;

drop table if exists vpm_data.tmp_hba1;
select distinct customer_id
into vpm_data.tmp_hba1
from vpm_data.pm_large_family_cat1_agg_1yr
where revised_category_level_1 = 'HBA' and total_qty >= 30 and total_day_visit >= 5 
	and (case when total_spending >=3000 then 1 else 0 end + case when total_day_visit >= 10 then 1 else 0 end + case when total_product_variety >= 4 then 1 else 0 end >=2) and customer_id is not null;

drop table if exists vpm_data.tmp_homenon1;
select distinct customer_id
into vpm_data.tmp_homenon1
from vpm_data.pm_large_family_cat1_agg_1yr
where revised_category_level_1 = 'HOME NON-ELECTRONICS' and total_qty >= 20 
	and (case when total_spending >=7000 then 1 else 0 end + case when total_day_visit >= 5 then 1 else 0 end + case when total_product_variety >= 4 then 1 else 0 end >=2) and customer_id is not null;

drop table if exists vpm_data.tmp_affinity_cust;
select distinct customer_id
into vpm_data.tmp_affinity_cust
from vpm_data.tmp_foodgrocery1
union
select distinct customer_id
from vpm_data.tmp_homegrocery1
union
select distinct customer_id
from vpm_data.tmp_hba1
union
select distinct customer_id
from vpm_data.tmp_homenon1;

/*alter table vpm_data.tmp_affinity_cust
drop column selected_food_grocery_flag
           ,selected_hba_flag
		   ,selected_home_grocery_flag
		   ,selected_homenon_flag
;*/

alter table vpm_data.tmp_affinity_cust
add selected_food_grocery_flag int
           ,ADD selected_hba_flag int
		   ,ADD selected_home_grocery_flag int
		   ,ADD selected_homenon_flag int ;

update vpm_data.tmp_affinity_cust
set selected_food_grocery_flag = case when (customer_id in (select distinct customer_id from vpm_data.tmp_foodgrocery1)) then 1 else 0 end
   ,selected_hba_flag = case when (customer_id in (select distinct customer_id from vpm_data.tmp_hba1)) then 1 else 0 end
   ,selected_home_grocery_flag = case when (customer_id in (select distinct customer_id from vpm_data.tmp_homegrocery1)) then 1 else 0 end
   ,selected_homenon_flag = case when (customer_id in (select distinct customer_id from vpm_data.tmp_homenon1)) then 1 else 0 end;


--*********************************************combined large fam cust*************************************************--
drop table if exists vpm_data.pm_large_family_cust_1yr;
select distinct a.customer_id
           ,coalesce(b.rice_cooker_bigsize_flag,0) as rice_cooker_bigsize_flag 
           ,coalesce(b.refrigerator_bigsize_flag,0) as refrigerator_bigsize_flag 
		   ,coalesce(b.home_elec_morethan3_flag,0) as home_elec_morethan3_flag  
           ,coalesce(b.washer_dryer_morethan1_flag,0) as washer_dryer_morethan1_flag 
		   ,coalesce(b.refrigerator_morethan1_flag,0) as refrigerator_morethan1_flag 
		   ,coalesce(b.home_elec_bigsize_highquantity_flag,0) as home_elec_bigsize_highquantity_flag 

		   ,coalesce(c.bed_morethan2_flag,0) as bed_morethan2_flag 
           ,coalesce(c.bed_accessory_morethan5_flag,0) as bed_accessory_morethan5_flag 
		   ,coalesce(c.home_non_elec_highquantity_flag,0) as home_non_elec_highquantity_flag 

		   ,coalesce(d.kid_infant,0) as kid_infant 
           ,coalesce(d.kid_toddler,0) as kid_toddler 
		   ,coalesce(d.kid_presch,0) as kid_presch 
		   ,coalesce(d.kid_junior,0) as kid_junior 
		   ,coalesce(d.kid_teenage,0) as kid_teenage 
		   ,coalesce(d.kid_3stage_valid_flag,0) as kid_3stage_valid_flag 
		   ,CASE WHEN d.noofchildren <> '' THEN coalesce(d.noofchildren::INT,0) ELSE 0 END as declared_num_children
		   ,case when (CASE WHEN d.noofchildren <> '' THEN coalesce(d.noofchildren::INT,0) ELSE 0 END) >= 3   then 1 else 0 end as declared_3_children_valid_flag
		   ,case when coalesce(d.kid_3stage_valid_flag,0) = 1 or (CASE WHEN d.noofchildren <> '' THEN coalesce(d.noofchildren::INT,0) ELSE 0 END >= 3) then 1 else 0 end as kid_3stage_and_declared_valid_flag 

		   ,case when e.customer_id is not null then 1 else 0 end as fashion_various_gender_flag

		   ,coalesce(f.selected_food_grocery_flag,0) as selected_food_grocery_flag 
           ,coalesce(f.selected_hba_flag,0) as selected_hba_flag 
		   ,coalesce(f.selected_home_grocery_flag,0) as selected_home_grocery_flag 
		   ,coalesce(f.selected_homenon_flag,0) as selected_homenon_flag 

into vpm_data.pm_large_family_cust_1yr
from (select distinct customer_id from vpm_data.tmp_a_large_family_electronics_3yr_cust
      union 
	  select distinct customer_id from vpm_data.tmp_a_large_family_home_non_cust
	  union 
	  select distinct customer_id from vpm_data.tmp_a_large_family_kids_3stage_and_declared_cust
	  union
	  select distinct customer_id from vpm_data.tmp_fashion_various_gender_cust
	  union
	  select distinct customer_id from vpm_data.tmp_affinity_cust) a
	  left join vpm_data.tmp_a_large_family_electronics_3yr_cust b on a.customer_id = b.customer_id
	  left join vpm_data.tmp_a_large_family_home_non_cust c on a.customer_id = c.customer_id
	  left join vpm_data.tmp_a_large_family_kids_3stage_and_declared_cust d on a.customer_id = d.customer_id
	  left join vpm_data.tmp_fashion_various_gender_cust e on a.customer_id = e.customer_id
	  left join vpm_data.tmp_affinity_cust f on a.customer_id = f.customer_id
	  left join vpm_data.pm_commercial_segment_feb g on a.customer_id = g.customer_id
where coalesce(cg_commercial_segment,'N/A') like '%SEMI%' or coalesce(cg_commercial_segment,'N/A') = 'N/A';

/*alter table vpm_data.pm_large_family_cust_1yr
drop column pass_affinity_criteria_flag
;*/

alter table vpm_data.pm_large_family_cust_1yr
add pass_affinity_criteria_flag int;

update vpm_data.pm_large_family_cust_1yr
set pass_affinity_criteria_flag = case when (selected_food_grocery_flag =1 
                                               or selected_hba_flag = 1 
											   or selected_home_grocery_flag = 1
                                               or selected_homenon_flag = 1
											   or fashion_various_gender_flag = 1) then 1 else 0 end;

/*alter table vpm_data.pm_large_family_cust_1yr
drop column have_additional_sign_flag
;*/

alter table vpm_data.pm_large_family_cust_1yr
add have_additional_sign_flag int;

update vpm_data.pm_large_family_cust_1yr
set have_additional_sign_flag = case when home_non_elec_highquantity_flag = 1 then 1 
								       when home_elec_bigsize_highquantity_flag = 1 then 1
									   when kid_3stage_and_declared_valid_flag = 1 then 1 else 0 end;

/*alter table vpm_data.pm_large_family_cust_1yr
drop column declare_profile_info_flag
;*/

alter table vpm_data.pm_large_family_cust_1yr
add declare_profile_info_flag int;

update vpm_data.pm_large_family_cust_1yr
set declare_profile_info_flag = case when declared_num_children >= 3 then 1 else 0 end;


----IF CURRENT MONTH IS JANUARY, THEN CREATE LARGE FAMILY TABLE AS FOR LAST YEAR
DO $$

DECLARE YEAR_END TEXT := (SELECT TO_CHAR(NOW()-INTERVAL '1 MONTH','yyyy'));

BEGIN
IF TO_CHAR(NOW(),'MON') = 'JAN' THEN
	EXECUTE FORMAT('CREATE TABLE VPM_DATA.PM_LARGE_FAMILY_CUST_1YR_'||YEAR_END||' AS
	SELECT * FROM vpm_data.pm_large_family_cust_1yr
	');
END IF;
END $$;


UPDATE VPM_DATA.SALESSKU_LOG_TIME SET end_time = now() WHERE task = 'LARGE FAMILY Step 1.0 - Cust 1 Yr' AND EXTRACT(MONTH FROM start_time) = EXTRACT(MONTH FROM now());

select *
from vpm_data.pm_large_family_cust_1yr where have_additional_sign_flag = 0 and pass_affinity_criteria_flag = 0 and declare_profile_info_flag = 0;

----------------------------------------------drop temp table-----------------------------------------------
--drop table if exists vpm_data.tmp_rice_cooker;
--drop table if exists vpm_data.tmp_refrigerator;
--drop table if exists vpm_data.tmp_elec_qty;
--drop table if exists vpm_data.tmp_elec_qty2;
--drop table if exists vpm_data.tmp_elec_morethan3_qty;
--drop table if exists vpm_data.tmp_elec_washer_qty;
--drop table if exists vpm_data.tmp_elec_refrigerator_qty;
--drop table if exists vpm_data.tmp_a_large_family_electronics_3yr_cust;
--
--drop table if exists vpm_data.tmp_bed_qty;
--drop table if exists vpm_data.tmp_bed;
--drop table if exists vpm_data.tmp_bed_accessory;
--drop table if exists vpm_data.tmp_a_large_family_home_non_cust;
--
--drop table if exists vpm_data.tmp_a_large_family_kids_3stage_and_declared_cust;
--
--drop table if exists vpm_data.tmp_fashion_sale ;
--drop table if exists vpm_data.tmp_fashion_kids;
--drop table if exists vpm_data.tmp_fashion_women;
--drop table if exists vpm_data.tmp_fashion_men;
--drop table if exists vpm_data.tmp_fashion_unisex;
--drop table if exists vpm_data.tmp_fashion_various_gender_cust;
--
--drop table if exists vpm_data.tmp_sales_all;
--
--drop table if exists vpm_data.pm_large_family_cat1_agg_1yr;
--drop table if exists vpm_data.tmp_foodgrocery1;
--drop table if exists vpm_data.tmp_homegrocery1;
--drop table if exists vpm_data.tmp_hba1;
--drop table if exists vpm_data.tmp_homenon1;
--drop table if exists vpm_data.tmp_affinity_cust;
