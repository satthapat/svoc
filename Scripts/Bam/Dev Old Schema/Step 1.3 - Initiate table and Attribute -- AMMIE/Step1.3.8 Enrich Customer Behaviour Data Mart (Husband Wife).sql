--STEP7.0: Enrich Customer Behaviour Data Mart (Husband Wife Kids)
--Calculate Husband, and Wife by using Actinve Months, TXN_AMT Decile, Frequency ,and Quantity per day

--WAITFOR DELAY '06:00:00'
--WAITFOR DELAY '24:00:00'


--CREATE SALSE SKU TEMP TABLE
INSERT INTO VPM_DATA.SALESSKU_LOG_TIME(task,start_time) VALUES ('Step1.3.8 Enrich Customer Behaviour Data Mart (Husband Wife)',NOW());

DROP TABLE IF EXISTS VPM_DATA.TEMP_GENDER; --1m 19s
SELECT BUID,BU_NAME,SKU_CODE,GENDER_FINAL
INTO VPM_DATA.TEMP_GENDER
FROM VPM_DATA.PM_PRODUCT_MASTER
WHERE GENDER_FINAL IS NOT NULL;

DROP TABLE IF EXISTS VPM_DATA.TEMP_TABLE_STAGING_1; --32m 14s
SELECT CUSTOMER_ID,TXN_DATE,GENDER_FINAL,SUM(TXN_AMT) AS TXN_AMT,SUM(QTY) AS QTY
INTO VPM_DATA.TEMP_TABLE_STAGING_1
FROM VPM_DATA.PM_ALL_SALESSKU_1YR A
INNER JOIN VPM_DATA.TEMP_GENDER B
ON CASE WHEN A.BUID IN ('1','2','71','998') THEN '1' ELSE A.BUID END = B.BUID --CDS/RBS REVISED PRODUCT MASTER
	AND A.SKU_CODE = B.SKU_CODE
GROUP BY CUSTOMER_ID,TXN_DATE,GENDER_FINAL;

DROP TABLE IF EXISTS VPM_DATA.TEMP_TABLE_STAGING_2; --21m 8s
SELECT CUSTOMER_ID,TXN_DATE,GENDER_FINAL,SUM(TXN_AMT) AS TXN_AMT,SUM(QTY) AS QTY
INTO VPM_DATA.TEMP_TABLE_STAGING_2
FROM VPM_DATA.PM_TOPS_SALESSKU_1YR A
INNER JOIN VPM_DATA.TEMP_GENDER B
ON A.BUID = B.BUID AND A.SKU_CODE = B.SKU_CODE
GROUP BY CUSTOMER_ID,TXN_DATE,GENDER_FINAL;

DROP TABLE IF EXISTS VPM_DATA.TEMP_TABLE_STAGING_3; --8m 44s
SELECT CUSTOMER_ID,TXN_DATE,GENDER_FINAL,SUM(TXN_AMT) AS TXN_AMT,SUM(QTY) AS QTY
INTO VPM_DATA.TEMP_TABLE_STAGING_3
FROM VPM_DATA.PM_CFM_SALESSKU_1YR A
INNER JOIN VPM_DATA.TEMP_GENDER B
ON A.BUID = B.BUID AND A.SKU_CODE = B.SKU_CODE
GROUP BY CUSTOMER_ID,TXN_DATE,GENDER_FINAL;

DROP TABLE IF EXISTS VPM_DATA.TEMP_TABLE; --20.632s
SELECT CUSTOMER_ID,TXN_DATE,GENDER_FINAL AS PRODUCT_GENDER,TXN_AMT,QTY
INTO VPM_DATA.TEMP_TABLE
FROM
(SELECT * FROM VPM_DATA.TEMP_TABLE_STAGING_1
UNION ALL SELECT * FROM VPM_DATA.TEMP_TABLE_STAGING_2
UNION ALL SELECT * FROM VPM_DATA.TEMP_TABLE_STAGING_3
) A;

DROP TABLE IF EXISTS VPM_DATA.TEMP_HUSBAND;
SELECT	A.CUSTOMER_ID, 
		1 AS HUSBAND_PURCHASE_FLAG,
		COUNT(DISTINCT EXTRACT(MONTH FROM TXN_DATE)) AS ACTIVE_M_HUSBAND,
		NTILE(10)OVER(ORDER BY SUM(TXN_AMT)/COUNT(DISTINCT EXTRACT(MONTH FROM TXN_DATE)) DESC) AS DECILE_HUSBAND_M,
		SUM(TXN_AMT)/COUNT(DISTINCT EXTRACT(MONTH FROM TXN_DATE)) AS AVG_HUSBAND_M,
		SUM(QTY)/COUNT(DISTINCT TXN_DATE) AS QTY_DAY_HUSBAND,
		COUNT(DISTINCT TXN_DATE) AS FREQ_HUSBAND
INTO VPM_DATA.TEMP_HUSBAND
FROM VPM_DATA.TEMP_TABLE A,VPM_DATA.PM_SVOC B
WHERE A.CUSTOMER_ID = B.CUSTOMER_ID
AND B.GENDER = 'F' AND PRODUCT_GENDER = 'MALE'
GROUP BY A.CUSTOMER_ID;

CREATE INDEX TEMP_HUSBAND_CUST ON VPM_DATA.TEMP_HUSBAND
(
	CUSTOMER_ID
)
; 

--STEP7.0.1.2: HUSBAND SCORE: 4 FACTORS
--1.NO ACTIVE MTH: ACTIVE_M_HUSBAND
--2.DECILE AVG TXN_AMT PER MTH: DECILE_HUSBAND_M 
--3.QUANTITY PER DAY: QTY_DAY_HUSBAND
--4.FREQUENCY: FREQ_HUSBAND

UPDATE VPM_DATA.PM_SVOC --6m 35s
SET HUSBAND_PURCHASE_FLAG = A.HUSBAND_PURCHASE_FLAG,
	ACTIVE_M_HUSBAND = A.ACTIVE_M_HUSBAND,
	AVG_HUSBAND_M = A.AVG_HUSBAND_M,
	DECILE_HUSBAND_M = A.DECILE_HUSBAND_M,
	QTY_DAY_HUSBAND = A.QTY_DAY_HUSBAND,
	FREQ_HUSBAND = A.FREQ_HUSBAND
FROM VPM_DATA.TEMP_HUSBAND A
WHERE VPM_DATA.PM_SVOC.CUSTOMER_ID = A.CUSTOMER_ID
AND VPM_DATA.PM_SVOC.GENDER = 'F'
;

--5m 13s
DROP TABLE IF EXISTS VPM_DATA.TEMP_HUS_FREQ;
SELECT CUSTOMER_ID
	  ,COUNT(*) AS FREQ_HUSBAND
INTO VPM_DATA.TEMP_HUS_FREQ
FROM
	(
		SELECT CUSTOMER_ID   
		FROM VPM_DATA.TEMP_TABLE
		WHERE PRODUCT_GENDER = 'MALE'
		GROUP BY CUSTOMER_ID
				  ,TXN_DATE
	)B
GROUP BY CUSTOMER_ID;

CREATE INDEX CUST_TEMP_HUS_FREQ ON VPM_DATA.TEMP_HUS_FREQ
(
	CUSTOMER_ID
);

UPDATE VPM_DATA.PM_SVOC
SET FREQ_DAY_MEN = A.FREQ_HUSBAND
FROM VPM_DATA.TEMP_HUS_FREQ A
WHERE VPM_DATA.PM_SVOC.CUSTOMER_ID = A.CUSTOMER_ID;

----------------------------------------------------------------------------------------------------
--STEP7.0.1.5: HUSBAND SCORING

DO $$ --4m 20s
DECLARE ACTIVE_M_HUSBAND_CUT FLOAT := (SELECT MIN(ACTIVE_M_HUSBAND) FROM (
											SELECT CUSTOMER_ID, ACTIVE_M_HUSBAND,PERCENT_RANK() OVER (ORDER BY ACTIVE_M_HUSBAND DESC) AS PCT_RANK
											FROM VPM_DATA.PM_SVOC
											WHERE HUSBAND_PURCHASE_FLAG = 1
											) A
									WHERE PCT_RANK <= 0.1);
		FREQ_HUSBAND_CUT FLOAT := (SELECT MIN(FREQ_HUSBAND) FROM (
										SELECT CUSTOMER_ID, FREQ_HUSBAND,PERCENT_RANK() OVER (ORDER BY FREQ_HUSBAND DESC) AS PCT_RANK
										FROM VPM_DATA.PM_SVOC
										WHERE HUSBAND_PURCHASE_FLAG = 1
										) A
									WHERE PCT_RANK <= 0.1);
		QTY_DAY_HUSBAND_CUT FLOAT := (SELECT MIN(QTY_DAY_HUSBAND)FROM
											(
											SELECT CUSTOMER_ID, QTY_DAY_HUSBAND,PERCENT_RANK() OVER (ORDER BY QTY_DAY_HUSBAND DESC) AS PCT_RANK
											FROM VPM_DATA.PM_SVOC
											WHERE HUSBAND_PURCHASE_FLAG = 1
											) A
									WHERE PCT_RANK <= 0.1);
BEGIN 
	UPDATE VPM_DATA.PM_SVOC
	SET HUSBAND_SCORE = ((CASE WHEN COALESCE(ACTIVE_M_HUSBAND,0) <= ACTIVE_M_HUSBAND_CUT	THEN (CAST(COALESCE(ACTIVE_M_HUSBAND,0) AS FLOAT)-0)/(ACTIVE_M_HUSBAND_CUT-0)
						     WHEN COALESCE(ACTIVE_M_HUSBAND,0) > ACTIVE_M_HUSBAND_CUT THEN 1 
						     ELSE 0 END)*0.3)
					+ ((CASE WHEN COALESCE(DECILE_HUSBAND_M,0) <= 10 THEN COALESCE(1.1-(CAST(DECILE_HUSBAND_M AS FLOAT)/10),0)
					         ELSE 0 END)*0.3)
				    + ((CASE WHEN COALESCE(FREQ_HUSBAND,0) <= FREQ_HUSBAND_CUT THEN (COALESCE(FREQ_HUSBAND,0)-0)/(FREQ_HUSBAND_CUT-0)
							 WHEN COALESCE(FREQ_HUSBAND,0) > FREQ_HUSBAND_CUT THEN 1
							 ELSE 0 END)*0.2)
					+ ((CASE WHEN COALESCE(QTY_DAY_HUSBAND,0) <= QTY_DAY_HUSBAND_CUT THEN (CAST(COALESCE(QTY_DAY_HUSBAND,0) AS FLOAT)-0)/(QTY_DAY_HUSBAND_CUT-0)
							 WHEN COALESCE(QTY_DAY_HUSBAND,0) > QTY_DAY_HUSBAND_CUT THEN 1
							 ELSE 0 END)*0.2)
	WHERE HUSBAND_PURCHASE_FLAG = 1
	;
END $$;

/***********************************************************************************************************/
/***********************************************************************************************************/

--STEP7.0.2: WIFE SCORE
--STEP7.0.2.1: ADD WIFE FLAG IN CUSTOMER LEVEL
--16m 3s
DROP TABLE IF EXISTS VPM_DATA.TEMP_WIFE;
SELECT	A.CUSTOMER_ID,
		1 AS WIFE_PURCHASE_FLAG,
		COUNT(DISTINCT EXTRACT(MONTH FROM TXN_DATE)) AS ACTIVE_M_WIFE,
		SUM(TXN_AMT)/COUNT(DISTINCT EXTRACT(MONTH FROM TXN_DATE)) AS AVG_WIFE_M, 
		NTILE(10)OVER(ORDER BY SUM(TXN_AMT)/COUNT(DISTINCT EXTRACT(MONTH FROM TXN_DATE)) DESC) AS DECILE_WIFE_M,
		SUM(QTY)/COUNT(DISTINCT TXN_DATE) AS QTY_DAY_WIFE,
		COUNT(DISTINCT TXN_DATE) AS FREQ_WIFE
INTO VPM_DATA.TEMP_WIFE 
FROM VPM_DATA.TEMP_TABLE A, VPM_DATA.PM_SVOC B
WHERE A.CUSTOMER_ID = B.CUSTOMER_ID
AND B.GENDER = 'M' AND PRODUCT_GENDER = 'FEMALE'
GROUP BY A.CUSTOMER_ID ;

UPDATE VPM_DATA.PM_SVOC
SET WIFE_PURCHASE_FLAG = A.WIFE_PURCHASE_FLAG,
	AVG_WIFE_M = A.AVG_WIFE_M,
	ACTIVE_M_WIFE = A.ACTIVE_M_WIFE,
	DECILE_WIFE_M = A.DECILE_WIFE_M,
	QTY_DAY_WIFE = A.QTY_DAY_WIFE,
	FREQ_WIFE = A.FREQ_WIFE
FROM VPM_DATA.TEMP_WIFE A
WHERE VPM_DATA.PM_SVOC.CUSTOMER_ID = A.CUSTOMER_ID 
AND VPM_DATA.PM_SVOC.GENDER = 'M';

--STEP7.0.2.2: WIFE SCORE: 4 FACTORS

--1.NO ACTIVE MTH: ACTIVE_M_WIFE
--2.DECILE AVG TXN_AMT PER MTH: DECILE_WIFE_M
--3.QUANTITY PER DAY: QTY_DAY_WIFE 
--4.FREQUENCY: FREQ_WIFE

DROP TABLE IF EXISTS VPM_DATA.TEMP_WOMEN_FREQ;
SELECT CUSTOMER_ID
	   ,COUNT(*) AS FREQ_WIFE
INTO VPM_DATA.TEMP_WOMEN_FREQ
FROM
	(
		SELECT CUSTOMER_ID   
		FROM VPM_DATA.TEMP_TABLE
		WHERE PRODUCT_GENDER = 'FEMALE'
		GROUP BY CUSTOMER_ID
				  ,TXN_DATE
	)B
GROUP BY CUSTOMER_ID
;

CREATE INDEX TEMP_WOMEN_FREQ_CUST ON VPM_DATA.TEMP_WOMEN_FREQ
(
 CUSTOMER_ID
);

UPDATE VPM_DATA.PM_SVOC
SET FREQ_DAY_WOMEN = A.FREQ_WIFE
FROM VPM_DATA.TEMP_WOMEN_FREQ A
WHERE VPM_DATA.PM_SVOC.CUSTOMER_ID = A.CUSTOMER_ID 
;


--STEP7.0.2.5: WIFE SCORING
--AUTOMATIC CUT OFF
DO $$
DECLARE ACTIVE_M_WIFE_CUT FLOAT := (SELECT MIN(ACTIVE_M_WIFE) FROM (
										SELECT CUSTOMER_ID, ACTIVE_M_WIFE,PERCENT_RANK() OVER (ORDER BY ACTIVE_M_WIFE DESC) AS PCT_RANK
										FROM VPM_DATA.PM_SVOC
										WHERE WIFE_PURCHASE_FLAG = 1
										) A
									WHERE PCT_RANK <= 0.1);
		FREQ_WIFE_CUT FLOAT := (SELECT MIN(FREQ_WIFE) FROM (
									SELECT CUSTOMER_ID, FREQ_WIFE,PERCENT_RANK() OVER (ORDER BY FREQ_WIFE DESC) AS PCT_RANK
									FROM VPM_DATA.PM_SVOC 
									WHERE WIFE_PURCHASE_FLAG = 1
									) A
								WHERE PCT_RANK <= 0.1);
		QTY_DAY_WIFE_CUT FLOAT := (SELECT MIN(QTY_DAY_WIFE) FROM (
										SELECT CUSTOMER_ID, QTY_DAY_WIFE,PERCENT_RANK() OVER (ORDER BY QTY_DAY_WIFE DESC) AS PCT_RANK
										FROM VPM_DATA.PM_SVOC 
										WHERE WIFE_PURCHASE_FLAG = 1
										) A	
									WHERE PCT_RANK <= 0.1);								
BEGIN
	UPDATE VPM_DATA.PM_SVOC
	SET WIFE_SCORE = ((CASE WHEN COALESCE(ACTIVE_M_WIFE,0) <= ACTIVE_M_WIFE_CUT	THEN (CAST(COALESCE(ACTIVE_M_WIFE,0) AS FLOAT)-0)/(ACTIVE_M_WIFE_CUT-0)
						     WHEN COALESCE(ACTIVE_M_WIFE,0) > ACTIVE_M_WIFE_CUT THEN 1 
						     ELSE 0 END)*0.3)
					+ ((CASE WHEN COALESCE(DECILE_WIFE_M,0) <= 10 THEN COALESCE(1.1-(CAST(DECILE_WIFE_M AS FLOAT)/10),0)
					         ELSE 0 END)*0.3)
				    + ((CASE WHEN COALESCE(FREQ_WIFE,0) <= FREQ_WIFE_CUT THEN (COALESCE(FREQ_WIFE,0)-0)/(FREQ_WIFE_CUT-0)
							 WHEN COALESCE(FREQ_WIFE,0) > FREQ_WIFE_CUT THEN 1
							 ELSE 0 END)*0.2)
					+ ((CASE WHEN COALESCE(QTY_DAY_WIFE,0) <= QTY_DAY_WIFE_CUT THEN (CAST(COALESCE(QTY_DAY_WIFE,0) AS FLOAT)-0)/(QTY_DAY_WIFE_CUT-0)
							 WHEN COALESCE(QTY_DAY_WIFE,0) > QTY_DAY_WIFE_CUT THEN 1
							 ELSE 0 END)*0.2)
	WHERE WIFE_PURCHASE_FLAG = 1
	;
END $$;

DROP TABLE IF EXISTS VPM_DATA.TEMP_GENDER;
DROP TABLE IF EXISTS VPM_DATA.TEMP_HUSBAND;
DROP TABLE IF EXISTS VPM_DATA.TEMP_HUS_FREQ;
DROP TABLE IF EXISTS VPM_DATA.TEMP_WIFE;
DROP TABLE IF EXISTS VPM_DATA.TEMP_WOMEN_FREQ;

UPDATE VPM_DATA.SALESSKU_LOG_TIME SET end_time = now() WHERE task = 'Step1.3.8 Enrich Customer Behaviour Data Mart (Husband Wife)' AND EXTRACT(MONTH FROM start_time) = EXTRACT(MONTH FROM now());
