/*
	REWRITE
	PM_CUSTOMER_CC
	
	--CHECK UPDATE
	SELECT MAX(CAST(Transactiondate AS DATE)) FROM analysis_data.SalesCredit_CDS2020
	SELECT MAX(CAST(Transactiondate AS DATE)) FROM analysis_data.SalesCredit_RBS2020
	SELECT MAX(CAST(Transactiondate AS DATE)) FROM analysis_data.SalesCredit_B2S2020
	SELECT MAX(CAST(Transactiondate AS DATE)) FROM analysis_data.SalesCredit_CMG2020
	SELECT MAX(CAST(Transactiondate AS DATE)) FROM analysis_data.SalesCredit_SSP2020
	SELECT MAX(CAST(Transactiondate AS DATE)) FROM analysis_data.SalesCredit_PWB2020
	SELECT MAX(CAST(Transactiondate AS DATE)) FROM analysis_data.SalesCredit_ODP2020
	SELECT MAX(CAST(Transactiondate AS DATE)) FROM analysis_data.SalesCredit_HWS2020
	SELECT MAX(CAST(Transactiondate AS DATE)) FROM analysis_data.SalesCredit_TWD2020
	SELECT MAX(CAST(Transactiondate AS DATE)) FROM analysis_data.SalesCredit_CFM2020
	SELECT MAX(CAST(Transactiondate AS DATE)) FROM analysis_data.SalesCredit_TOPS2020
	
	
	SP_RENAME 'PM_CUSTOMER_CC','PM_CUSTOMER_CC_MAY'
	
*/


/******************************************************************************** MERGING 2 YEARS CREDIT CARD TXN ********************************************************************************/
--87m --155m 14s
--290m 40s
--8m 10s
/*DO $$
DECLARE START_DATE DATE := (SELECT VPM_DATA.GET_START_DATE(NOW()));
		END_DATE DATE := (SELECT VPM_DATA.GET_START_DATE(NOW()));
BEGIN*/
INSERT INTO VPM_DATA.SALESSKU_LOG_TIME(task,start_time) VALUES ('Step1.3.2 Create Credit Card payment Table',NOW());

DROP TABLE IF EXISTS VPM_DATA.TEMP_SALESCREDIT_CDS_STAGING;
CREATE TABLE VPM_DATA.TEMP_SALESCREDIT_CDS_STAGING AS
SELECT	CustomerID,BUID,CAST(TransactionDate AS DATE) AS TXN_DATE
		,UPPER(GroupCard) AS GroupCard,Amount,ReferenceId1
		,CASE WHEN VPM_DATA.ISNUMERIC(RIGHT(LEFT(ReferenceId1,9),1)) = 1 THEN LEFT(ReferenceId1,9)
			  WHEN VPM_DATA.ISNUMERIC(RIGHT(LEFT(ReferenceId1,8),1)) = 1 THEN LEFT(ReferenceId1,8)
			  ELSE RangCreditCard END AS RangCreditCard
FROM
(
SELECT CustomerID,BUID,TransactionDate,GroupCard,Amount,ReferenceId1,RangCreditCard FROM analysis_data.SalesCredit_CDS2019
WHERE COALESCE(RangCreditCard,'') != ''
AND Transactiondate >= VPM_DATA.GET_START_DATE(NOW()) - INTERVAL '12 MONTH'
AND CustomerID <> ''
UNION ALL
SELECT CustomerID,BUID,TransactionDate,GroupCard,Amount,ReferenceId1,RangCreditCard FROM analysis_data.SalesCredit_CDS2020
WHERE COALESCE(RangCreditCard,'') != ''
AND CustomerID <> ''
UNION ALL
SELECT CustomerID,BUID,TransactionDate,GroupCard,Amount,ReferenceId1,RangCreditCard FROM analysis_data.SalesCredit_CDS2021
WHERE COALESCE(RangCreditCard,'') != ''
AND Transactiondate <= VPM_DATA.GET_END_DATE(NOW())
AND CustomerID <> ''
) A;
/*END; 
$$;*/

--5m 51s
DROP TABLE IF EXISTS VPM_DATA.TEMP_SALESCREDIT_RBS_STAGING;
CREATE TABLE VPM_DATA.TEMP_SALESCREDIT_RBS_STAGING AS
SELECT	CustomerID,BUID,CAST(TransactionDate AS DATE) AS TXN_DATE
		,UPPER(GroupCard) AS GroupCard,Amount,ReferenceId1
		,CASE WHEN VPM_DATA.ISNUMERIC(RIGHT(LEFT(ReferenceId1,9),1)) = 1 THEN LEFT(ReferenceId1,9)
			  WHEN VPM_DATA.ISNUMERIC(RIGHT(LEFT(ReferenceId1,8),1)) = 1 THEN LEFT(ReferenceId1,8)
			  ELSE RangCreditCard END AS RangCreditCard
FROM
(
SELECT CustomerID,BUID,TransactionDate,GroupCard,Amount,ReferenceId1,RangCreditCard FROM analysis_data.SalesCredit_RBS2019
WHERE COALESCE(RangCreditCard,'') != ''
AND Transactiondate >= VPM_DATA.GET_START_DATE(NOW()) - INTERVAL '12 MONTH'
AND CustomerID <> ''
UNION ALL
SELECT CustomerID,BUID,TransactionDate,GroupCard,Amount,ReferenceId1,RangCreditCard FROM analysis_data.SalesCredit_RBS2020
WHERE COALESCE(RangCreditCard,'') != ''
AND CustomerID <> ''
UNION ALL
SELECT CustomerID,BUID,TransactionDate,GroupCard,Amount,ReferenceId1,RangCreditCard FROM analysis_data.SalesCredit_RBS2021
WHERE COALESCE(RangCreditCard,'') != ''
AND Transactiondate <= VPM_DATA.GET_END_DATE(NOW())
AND CustomerID <> ''
) A;

--1m 16s
DROP TABLE IF EXISTS VPM_DATA.TEMP_SALESCREDIT_B2S_STAGING;
CREATE TABLE VPM_DATA.TEMP_SALESCREDIT_B2S_STAGING AS
SELECT	CustomerID,BUID,CAST(TransactionDate AS DATE) AS TXN_DATE
		,UPPER(GroupCard) AS GroupCard,Amount,ReferenceId1
		,CASE WHEN VPM_DATA.ISNUMERIC(RIGHT(LEFT(ReferenceId1,9),1)) = 1 THEN LEFT(ReferenceId1,9)
			  WHEN VPM_DATA.ISNUMERIC(RIGHT(LEFT(ReferenceId1,8),1)) = 1 THEN LEFT(ReferenceId1,8)
			  ELSE RangCreditCard END AS RangCreditCard
FROM
(
SELECT BUID,BranchId,Transactiondate,TypeGroup,GroupCard,CustomerId,CardNo,RangCreditCard,TicketNumber,Amount,ReferenceId1
FROM analysis_data.SalesCredit_B2S2019
WHERE COALESCE(RangCreditCard,'') != ''
AND Transactiondate >= VPM_DATA.GET_START_DATE(NOW()) - INTERVAL '12 MONTH'
AND CustomerID <> ''
UNION ALL
SELECT BUID,BranchId,Transactiondate,TypeGroup,GroupCard,CustomerId,CardNo,RangCreditCard,TicketNumber,Amount,ReferenceId1
FROM analysis_data.SalesCredit_B2S2020
WHERE COALESCE(RangCreditCard,'') != ''
AND CustomerID <> ''
UNION ALL
SELECT BUID,BranchId,Transactiondate,TypeGroup,GroupCard,CustomerId,CardNo,RangCreditCard,TicketNumber,Amount,ReferenceId1
FROM analysis_data.SalesCredit_B2S2021
WHERE COALESCE(RangCreditCard,'') != ''
AND Transactiondate <= VPM_DATA.GET_END_DATE(NOW())
AND CustomerID <> ''
) A;

--2m 3s
DROP TABLE IF EXISTS VPM_DATA.TEMP_SALESCREDIT_SSP_STAGING;
CREATE TABLE VPM_DATA.TEMP_SALESCREDIT_SSP_STAGING AS
SELECT	CustomerID,BUID,CAST(TransactionDate AS DATE) AS TXN_DATE
		,UPPER(GroupCard) AS GroupCard,Amount,ReferenceId1
		,CASE WHEN VPM_DATA.ISNUMERIC(RIGHT(LEFT(ReferenceId1,9),1)) = 1 THEN LEFT(ReferenceId1,9)
			  WHEN VPM_DATA.ISNUMERIC(RIGHT(LEFT(ReferenceId1,8),1)) = 1 THEN LEFT(ReferenceId1,8)
			  ELSE RangCreditCard END AS RangCreditCard
FROM
(
SELECT CustomerID,BUID,TransactionDate,GroupCard,Amount,ReferenceId1,RangCreditCard FROM analysis_data.SalesCredit_SSP2019
WHERE COALESCE(RangCreditCard,'') != ''
AND Transactiondate >= VPM_DATA.GET_START_DATE(NOW()) - INTERVAL '12 MONTH'
AND CustomerID <> ''
UNION ALL
SELECT CustomerID,BUID,TransactionDate,GroupCard,Amount,ReferenceId1,RangCreditCard FROM analysis_data.SalesCredit_SSP2020
WHERE COALESCE(RangCreditCard,'') != ''
AND CustomerID <> ''
UNION ALL
SELECT CustomerID,BUID,TransactionDate,GroupCard,Amount,ReferenceId1,RangCreditCard FROM analysis_data.SalesCredit_SSP2021
WHERE COALESCE(RangCreditCard,'') != ''
AND Transactiondate <= VPM_DATA.GET_END_DATE(NOW())
AND CustomerID <> ''
) A;

--1m 28s
DROP TABLE IF EXISTS VPM_DATA.TEMP_SALESCREDIT_PWB_STAGING;
CREATE TABLE VPM_DATA.TEMP_SALESCREDIT_PWB_STAGING AS
SELECT	CustomerID,BUID,CAST(TransactionDate AS DATE) AS TXN_DATE
		,UPPER(GroupCard) AS GroupCard,Amount,ReferenceId1
		,CASE WHEN VPM_DATA.ISNUMERIC(RIGHT(LEFT(ReferenceId1,9),1)) = 1 THEN LEFT(ReferenceId1,9)
			  WHEN VPM_DATA.ISNUMERIC(RIGHT(LEFT(ReferenceId1,8),1)) = 1 THEN LEFT(ReferenceId1,8)
			  ELSE RangCreditCard END AS RangCreditCard
FROM
(
SELECT CustomerID,BUID,TransactionDate,GroupCard,Amount,ReferenceId1,RangCreditCard FROM analysis_data.SalesCredit_PWB2019
WHERE COALESCE(RangCreditCard,'') != ''
AND Transactiondate >= VPM_DATA.GET_START_DATE(NOW()) - INTERVAL '12 MONTH'
AND CustomerID <> ''
UNION ALL
SELECT CustomerID,BUID,TransactionDate,GroupCard,Amount,ReferenceId1,RangCreditCard FROM analysis_data.SalesCredit_PWB2020
WHERE COALESCE(RangCreditCard,'') != ''
AND CustomerID <> ''
UNION ALL
SELECT CustomerID,BUID,TransactionDate,GroupCard,Amount,ReferenceId1,RangCreditCard FROM analysis_data.SalesCredit_PWB2021
WHERE COALESCE(RangCreditCard,'') != ''
AND Transactiondate <= VPM_DATA.GET_END_DATE(NOW())
AND CustomerID <> ''
) A;

--39.371s
DROP TABLE IF EXISTS VPM_DATA.TEMP_SALESCREDIT_CMG_STAGING;
CREATE TABLE VPM_DATA.TEMP_SALESCREDIT_CMG_STAGING AS
SELECT	CustomerID,BUID,CAST(TransactionDate AS DATE) AS TXN_DATE
		,UPPER(GroupCard) AS GroupCard,Amount,ReferenceId1
		,CASE WHEN VPM_DATA.ISNUMERIC(RIGHT(LEFT(ReferenceId1,9),1)) = 1 THEN LEFT(ReferenceId1,9)
			  WHEN VPM_DATA.ISNUMERIC(RIGHT(LEFT(ReferenceId1,8),1)) = 1 THEN LEFT(ReferenceId1,8)
			  ELSE RangCreditCard END AS RangCreditCard
FROM
(
SELECT CustomerID,BUID,TransactionDate,GroupCard,Amount,ReferenceId1,RangCreditCard FROM analysis_data.SalesCredit_CMG2019
WHERE COALESCE(RangCreditCard,'') != ''
AND Transactiondate >= VPM_DATA.GET_START_DATE(NOW()) - INTERVAL '12 MONTH'
AND CustomerID <> ''
UNION ALL
SELECT CustomerID,BUID,TransactionDate,GroupCard,Amount,ReferenceId1,RangCreditCard FROM analysis_data.SalesCredit_CMG2020
WHERE COALESCE(RangCreditCard,'') != ''
AND CustomerID <> ''
UNION ALL
SELECT CustomerID,BUID,TransactionDate,GroupCard,Amount,ReferenceId1,RangCreditCard FROM analysis_data.SalesCredit_CMG2021
WHERE COALESCE(RangCreditCard,'') != ''
AND Transactiondate <= VPM_DATA.GET_END_DATE(NOW())
AND CustomerID <> ''
) A;

--16.940s
DROP TABLE IF EXISTS VPM_DATA.TEMP_SALESCREDIT_HWS_STAGING;
CREATE TABLE VPM_DATA.TEMP_SALESCREDIT_HWS_STAGING AS
SELECT	CustomerID,BUID,CAST(TransactionDate AS DATE) AS TXN_DATE
		,UPPER(GroupCard) AS GroupCard,Amount,ReferenceId1
		,CASE WHEN VPM_DATA.ISNUMERIC(RIGHT(LEFT(ReferenceId1,9),1)) = 1 THEN LEFT(ReferenceId1,9)
			  WHEN VPM_DATA.ISNUMERIC(RIGHT(LEFT(ReferenceId1,8),1)) = 1 THEN LEFT(ReferenceId1,8)
			  ELSE RangCreditCard END AS RangCreditCard
FROM
(
SELECT CustomerID,BUID,TransactionDate,GroupCard,Amount,ReferenceId1,RangCreditCard FROM analysis_data.SalesCredit_HWS2019
WHERE COALESCE(RangCreditCard,'') != ''
AND Transactiondate >= VPM_DATA.GET_START_DATE(NOW()) - INTERVAL '12 MONTH'
AND CustomerID <> ''
UNION ALL
SELECT CustomerID,BUID,TransactionDate,GroupCard,Amount,ReferenceId1,RangCreditCard FROM analysis_data.SalesCredit_HWS2020
WHERE COALESCE(RangCreditCard,'') != ''
AND CustomerID <> ''
UNION ALL
SELECT CustomerID,BUID,TransactionDate,GroupCard,Amount,ReferenceId1,RangCreditCard FROM analysis_data.SalesCredit_HWS2021
WHERE COALESCE(RangCreditCard,'') != ''
AND Transactiondate <= VPM_DATA.GET_END_DATE(NOW())
AND CustomerID <> ''
) A;

--1m 45s
DROP TABLE IF EXISTS VPM_DATA.TEMP_SALESCREDIT_TWD_STAGING;
CREATE TABLE VPM_DATA.TEMP_SALESCREDIT_TWD_STAGING AS
SELECT	CustomerID,BUID,CAST(TransactionDate AS DATE) AS TXN_DATE
		,UPPER(GroupCard) AS GroupCard,Amount,ReferenceId1
		,CASE WHEN VPM_DATA.ISNUMERIC(RIGHT(LEFT(ReferenceId1,9),1)) = 1 THEN LEFT(ReferenceId1,9)
			  WHEN VPM_DATA.ISNUMERIC(RIGHT(LEFT(ReferenceId1,8),1)) = 1 THEN LEFT(ReferenceId1,8)
			  ELSE RangCreditCard END AS RangCreditCard
FROM
(
SELECT CustomerID,BUID,TransactionDate,GroupCard,Amount,ReferenceId1,RangCreditCard FROM analysis_data.SalesCredit_TWD2019
WHERE COALESCE(RangCreditCard,'') != ''
AND Transactiondate >= VPM_DATA.GET_START_DATE(NOW()) - INTERVAL '12 MONTH'
AND CustomerID <> ''
UNION ALL
SELECT CustomerID,BUID,TransactionDate,GroupCard,Amount,ReferenceId1,RangCreditCard FROM analysis_data.SalesCredit_TWD2020
WHERE COALESCE(RangCreditCard,'') != ''
AND CustomerID <> ''
UNION ALL
SELECT CustomerID,BUID,TransactionDate,GroupCard,Amount,ReferenceId1,RangCreditCard FROM analysis_data.SalesCredit_TWD2021
WHERE COALESCE(RangCreditCard,'') != ''
AND Transactiondate <= VPM_DATA.GET_END_DATE(NOW())
AND CustomerID <> ''
) A;

--37.536s
DROP TABLE IF EXISTS VPM_DATA.TEMP_SALESCREDIT_ODP_STAGING;
CREATE TABLE VPM_DATA.TEMP_SALESCREDIT_ODP_STAGING AS
SELECT	CustomerID,BUID,CAST(TransactionDate AS DATE) AS TXN_DATE
		,UPPER(GroupCard) AS GroupCard,Amount,ReferenceId1
		,CASE WHEN VPM_DATA.ISNUMERIC(RIGHT(LEFT(ReferenceId1,9),1)) = 1 THEN LEFT(ReferenceId1,9)
			  WHEN VPM_DATA.ISNUMERIC(RIGHT(LEFT(ReferenceId1,8),1)) = 1 THEN LEFT(ReferenceId1,8)
			  ELSE RangCreditCard END AS RangCreditCard
FROM
(
SELECT CustomerID,BUID,TransactionDate,GroupCard,Amount,ReferenceId1,RangCreditCard FROM analysis_data.SalesCredit_ODP2019
WHERE COALESCE(RangCreditCard,'') != ''
AND Transactiondate >= VPM_DATA.GET_START_DATE(NOW()) - INTERVAL '12 MONTH'
AND CustomerID <> ''
UNION ALL
SELECT CustomerID,BUID,TransactionDate,GroupCard,Amount,ReferenceId1,RangCreditCard FROM analysis_data.SalesCredit_ODP2020
WHERE COALESCE(RangCreditCard,'') != ''
AND CustomerID <> ''
UNION ALL
SELECT CustomerID,BUID,TransactionDate,GroupCard,Amount,ReferenceId1,RangCreditCard FROM analysis_data.SalesCredit_ODP2021
WHERE COALESCE(RangCreditCard,'') != ''
AND Transactiondate <= VPM_DATA.GET_END_DATE(NOW())
AND CustomerID <> ''
) A;

--5m 22s
DROP TABLE IF EXISTS VPM_DATA.TEMP_SALESCREDIT_CFM_STAGING;
CREATE TABLE VPM_DATA.TEMP_SALESCREDIT_CFM_STAGING AS
SELECT	CustomerID,BUID,CAST(TransactionDate AS DATE) AS TXN_DATE
		,UPPER(GroupCard) AS GroupCard,Amount,ReferenceId1
		,CASE WHEN VPM_DATA.ISNUMERIC(RIGHT(LEFT(ReferenceId1,9),1)) = 1 THEN LEFT(ReferenceId1,9)
			  WHEN VPM_DATA.ISNUMERIC(RIGHT(LEFT(ReferenceId1,8),1)) = 1 THEN LEFT(ReferenceId1,8)
			  ELSE RangCreditCard END AS RangCreditCard
FROM
(
SELECT CustomerID,BUID,TransactionDate,GroupCard,Amount,ReferenceId1,RangCreditCard FROM analysis_data.SalesCredit_CFM2019
WHERE COALESCE(RangCreditCard,'') != ''
AND Transactiondate >= VPM_DATA.GET_START_DATE(NOW()) - INTERVAL '12 MONTH'
AND CustomerID <> ''
UNION ALL
SELECT CustomerID,BUID,TransactionDate,GroupCard,Amount,ReferenceId1,RangCreditCard FROM analysis_data.SalesCredit_CFM2020
WHERE COALESCE(RangCreditCard,'') != ''
AND CustomerID <> ''
UNION ALL
SELECT CustomerID,BUID,TransactionDate,GroupCard,Amount,ReferenceId1,RangCreditCard FROM analysis_data.SalesCredit_CFM2021
WHERE COALESCE(RangCreditCard,'') != ''
AND Transactiondate <= VPM_DATA.GET_END_DATE(NOW())
AND CustomerID <> ''
) A;

--12m 43s
DROP TABLE IF EXISTS VPM_DATA.TEMP_SALESCREDIT_TOPS_STAGING; --34m 36s
CREATE TABLE VPM_DATA.TEMP_SALESCREDIT_TOPS_STAGING AS
SELECT	CustomerID,BUID,CAST(TransactionDate AS DATE) AS TXN_DATE
		,UPPER(GroupCard) AS GroupCard,Amount,ReferenceId1
		,CASE WHEN VPM_DATA.ISNUMERIC(RIGHT(LEFT(ReferenceId1,9),1)) = 1 THEN LEFT(ReferenceId1,9)
			  WHEN VPM_DATA.ISNUMERIC(RIGHT(LEFT(ReferenceId1,8),1)) = 1 THEN LEFT(ReferenceId1,8)
			  ELSE RangCreditCard END AS RangCreditCard
FROM
(
SELECT CustomerID,BUID,TransactionDate,GroupCard,Amount,ReferenceId1,RangCreditCard FROM analysis_data.SalesCredit_TOPS2019
WHERE COALESCE(RangCreditCard,'') != ''
AND Transactiondate >= VPM_DATA.GET_START_DATE(NOW()) - INTERVAL '12 MONTH'
AND CustomerID <> ''
UNION ALL
SELECT CustomerID,BUID,TransactionDate,GroupCard,Amount,ReferenceId1,RangCreditCard FROM analysis_data.SalesCredit_TOPS2020
WHERE COALESCE(RangCreditCard,'') != ''
AND CustomerID <> ''
UNION ALL
SELECT CustomerID,BUID,TransactionDate,GroupCard,Amount,ReferenceId1,RangCreditCard FROM analysis_data.SalesCredit_TOPS2021
WHERE COALESCE(RangCreditCard,'') != ''
AND Transactiondate <= VPM_DATA.GET_END_DATE(NOW())
AND CustomerID <> ''
) A;


DROP TABLE IF EXISTS VPM_DATA.TEMP_CC_ALL_TXN; --3m 42s
SELECT * INTO VPM_DATA.TEMP_CC_ALL_TXN
FROM VPM_DATA.TEMP_SALESCREDIT_CDS_STAGING--CDS	
UNION SELECT * FROM VPM_DATA.TEMP_SALESCREDIT_RBS_STAGING--RBS	
UNION SELECT * FROM VPM_DATA.TEMP_SALESCREDIT_B2S_STAGING--B2S	
UNION SELECT * FROM VPM_DATA.TEMP_SALESCREDIT_SSP_STAGING--SSP	
UNION SELECT * FROM VPM_DATA.TEMP_SALESCREDIT_PWB_STAGING--PWB	
UNION SELECT * FROM VPM_DATA.TEMP_SALESCREDIT_CMG_STAGING--CMG	
UNION SELECT * FROM VPM_DATA.TEMP_SALESCREDIT_HWS_STAGING--HWS	
UNION SELECT * FROM VPM_DATA.TEMP_SALESCREDIT_TWD_STAGING--TWD
UNION SELECT * FROM VPM_DATA.TEMP_SALESCREDIT_ODP_STAGING--ODP
UNION SELECT * FROM VPM_DATA.TEMP_SALESCREDIT_CFM_STAGING--CFM
UNION SELECT * FROM VPM_DATA.TEMP_SALESCREDIT_TOPS_STAGING;--TOPS

/*DROP TABLE IF EXISTS VPM_DATA.TEMP_SALESCREDIT_CDS_STAGING;
DROP TABLE IF EXISTS VPM_DATA.TEMP_SALESCREDIT_RBS_STAGING;
DROP TABLE IF EXISTS VPM_DATA.TEMP_SALESCREDIT_B2S_STAGING;
DROP TABLE IF EXISTS VPM_DATA.TEMP_SALESCREDIT_SSP_STAGING;
DROP TABLE IF EXISTS VPM_DATA.TEMP_SALESCREDIT_PWB_STAGING;
DROP TABLE IF EXISTS VPM_DATA.TEMP_SALESCREDIT_CMG_STAGING;
DROP TABLE IF EXISTS VPM_DATA.TEMP_SALESCREDIT_HWS_STAGING;
DROP TABLE IF EXISTS VPM_DATA.TEMP_SALESCREDIT_TWD_STAGING;
DROP TABLE IF EXISTS VPM_DATA.TEMP_SALESCREDIT_ODP_STAGING;
DROP TABLE IF EXISTS VPM_DATA.TEMP_SALESCREDIT_CFM_STAGING;
DROP TABLE IF EXISTS VPM_DATA.TEMP_SALESCREDIT_TOPS_STAGING;*/


CREATE INDEX TEMP_CC_ALL_TXN_CUSTOMER ON VPM_DATA.TEMP_CC_ALL_TXN (CustomerID); --7m 2s

DELETE FROM VPM_DATA.TEMP_CC_ALL_TXN --12m 17s
WHERE VPM_DATA.ISNUMERIC(RIGHT(LEFT(RangCreditCard,1),1)) = 0;

UPDATE VPM_DATA.TEMP_CC_ALL_TXN --13m 16s
SET RangCreditCard = CASE WHEN VPM_DATA.ISNUMERIC(RIGHT(LEFT(RangCreditCard,7),1)) = 0 THEN LEFT(RangCreditCard,6)
							WHEN VPM_DATA.ISNUMERIC(RIGHT(LEFT(RangCreditCard,8),1)) = 0 THEN LEFT(RangCreditCard,7)
							WHEN VPM_DATA.ISNUMERIC(RIGHT(LEFT(RangCreditCard,9),1)) = 0 THEN LEFT(RangCreditCard,8)
							ELSE RangCreditCard END
WHERE VPM_DATA.ISNUMERIC(RIGHT(LEFT(RangCreditCard,7),1)) = 0
	OR  VPM_DATA.ISNUMERIC(RIGHT(LEFT(RangCreditCard,9),1)) = 0;

/******************************************************************************** AGGREGATE CREDIT CARD BY CUSTOMER ********************************************************************************/
--Merge credit card customer from year aug2019 (Because RangCreditCard changed from 8 digits to 6 digits thus we cannot defined some cards)
DROP TABLE IF EXISTS VPM_DATA.TEMP_PM_CUSTOMER_CC; --14m 39s
SELECT CUSTOMER_ID,RangCreditCard,MAX(GroupCard) AS GroupCard
INTO VPM_DATA.TEMP_PM_CUSTOMER_CC
FROM (SELECT DISTINCT "CUSTOMER_ID" AS CUSTOMER_ID
					  ,"RangCreditCard" AS RangCreditCard
					  ,(CASE WHEN "CLEANED_CC_BANK" LIKE '%KBANK%' THEN 'KBANK' 
							 WHEN "CLEANED_CC_BANK" LIKE 'CENTRAL%' THEN 'CENTRAL' 
							 WHEN "CLEANED_CC_BANK" LIKE 'FIRST CHOICE' THEN 'KCC'
							 WHEN "CLEANED_CC_BANK" = 'KTC' AND "RangCreditCard" LIKE '409344%' THEN 'KCC' ELSE "CLEANED_CC_BANK" END) AS GroupCard
	from VPM_DATA.PM_CUSTOMER_CC_8DIGIT
	WHERE "RangCreditCard" NOT LIKE '0%'
	AND "RangCreditCard" NOT LIKE '1%'
	AND "RangCreditCard" NOT LIKE '2%'
	AND "RangCreditCard" NOT LIKE '7%'
	AND "RangCreditCard" NOT LIKE '8%'
	AND "RangCreditCard" NOT LIKE '9%'
	UNION
	SELECT DISTINCT CUSTOMERID AS CUSTOMER_ID
				  ,RangCreditCard
				  ,(CASE WHEN GroupCard LIKE 'KBANK%' THEN 'KBANK' 
				   WHEN GroupCard = 'KTC' AND RangCreditCard LIKE '409344%' THEN 'KCC' ELSE GroupCard END) AS GroupCard
	from VPM_DATA.TEMP_CC_ALL_TXN
	WHERE RangCreditCard NOT LIKE '0%'
	AND RangCreditCard NOT LIKE '1%'
	AND RangCreditCard NOT LIKE '2%'
	AND RangCreditCard NOT LIKE '7%'
	AND RangCreditCard NOT LIKE '8%'
	AND RangCreditCard NOT LIKE '9%') AS A
GROUP BY CUSTOMER_ID,RangCreditCard;

CREATE INDEX TEMP_PM_CUSTOMER_CC_CUST_RANG_GROUP ON VPM_DATA.TEMP_PM_CUSTOMER_CC(CUSTOMER_ID,RangCreditCard,GroupCard);
CREATE INDEX TEMP_CC_ALL_TXN_CUST_RANG_GROUP ON VPM_DATA.TEMP_CC_ALL_TXN(CUSTOMERID,RangCreditCard,GroupCard);

--Merge all TXN
--16m 40s

DROP TABLE IF EXISTS VPM_DATA.PM_CUSTOMER_CC;
CREATE TABLE VPM_DATA.PM_CUSTOMER_CC AS
SELECT A.CUSTOMER_ID AS CUSTOMER_ID
	,A.RangCreditCard
	,A.GroupCard
	,MAX(TXN_DATE) AS MAX_TXN_DATE
	,MIN(TXN_DATE) AS MIN_TXN_DATE
	,vpm_data.datediff('day',MAX(TXN_DATE),VPM_DATA.GET_END_DATE(NOW())) AS RECENCY
	,COUNT(*) AS NO_OF_TXN_2Y
    ,SUM(CASE WHEN TXN_DATE >= VPM_DATA.GET_END_DATE(NOW())-INTERVAL '1 YEAR' THEN 1 END) AS NO_OF_TXN_1Y
    ,SUM(CASE WHEN TXN_DATE >= VPM_DATA.GET_END_DATE(NOW())-INTERVAL '6 MONTHS' THEN 1 END) AS NO_OF_TXN_6M
    ,SUM(Amount) AS SUM_AMT_2Y
	,SUM(CASE WHEN TXN_DATE >= VPM_DATA.GET_END_DATE(NOW())-INTERVAL '1 YEAR' THEN Amount END) AS SUM_AMT_1Y
	,COUNT(DISTINCT EXTRACT(MONTH FROM TXN_DATE)) AS NO_OF_MONTH_2Y
	,COUNT(DISTINCT TXN_DATE) AS DAY_VISIT_2Y --ADD
	,COUNT(DISTINCT CASE WHEN TXN_DATE >= VPM_DATA.GET_END_DATE(NOW())-INTERVAL '1 YEAR' THEN TXN_DATE END) AS DAY_VISIT_1Y --ADD
	,ROW_NUMBER() OVER(PARTITION BY A.CUSTOMER_ID ORDER BY COUNT(B.CUSTOMERID) DESC NULLS LAST) AS RANK_CC_TXN
	,ROW_NUMBER() OVER(PARTITION BY A.CUSTOMER_ID ORDER BY SUM(B.Amount) DESC NULLS LAST) AS RANK_CC_AMT
	,CAST(NULL AS VARCHAR(50)) AS PAYMENT_CARD_TYPE
	,CAST(NULL AS VARCHAR(50)) AS ISSUING_BANK
	,CAST(NULL AS VARCHAR(50)) AS CARD_TYPE
	,CAST(NULL AS VARCHAR(50)) AS CARD_LEVEL
	,CAST(NULL AS VARCHAR(150)) AS CARD_DESC
	,CAST(NULL AS FLOAT) AS MINIMUM_INCOME
	,CAST(NULL AS FLOAT) AS MINIMUM_DEPOSIT
	,CAST(NULL AS INT) AS TRAVEL_FLAG
	,CAST(NULL AS INT) AS COMPETITORS_SHOPPING_FLAG
	,CAST(NULL AS INT) AS HEALTH_WELLNESS_FLAG
	,CAST(NULL AS INT) AS CAR_FLAG
	,CAST(NULL AS INT) AS DOCTOR_FLAG
	,CAST(NULL AS INT) AS CO_BRAND_FLAG
FROM VPM_DATA.TEMP_PM_CUSTOMER_CC A
LEFT JOIN VPM_DATA.TEMP_CC_ALL_TXN B
ON A.CUSTOMER_ID = B.CUSTOMERID
AND A.RangCreditCard = B.RangCreditCard
AND A.GroupCard = B.GroupCard
GROUP BY A.CUSTOMER_ID
   ,A.RangCreditCard
   ,A.GroupCard;

-------------------- add
--8m

UPDATE VPM_DATA.PM_CUSTOMER_CC
SET PAYMENT_CARD_TYPE = A.PAYMENT_CARD_TYPE
	,ISSUING_BANK =	A.ISSUING_BANK
	,CARD_TYPE = CASE WHEN COALESCE(A.CARD_TYPE,'') <> '' THEN A.CARD_TYPE
				 ELSE CASE WHEN LEFT(RangCreditCard,1) = '4' THEN 'VISA'
						   WHEN LEFT(RangCreditCard,1) = '5' THEN 'Master Card'
						   WHEN LEFT(RangCreditCard,2) = '35' THEN 'JCB'
						   WHEN LEFT(RangCreditCard,2) = '37' THEN 'AMEX'
						   WHEN LEFT(RangCreditCard,1) = '6' THEN 'UnionPay'
						   END END
	,CARD_LEVEL = A.CARD_LEVEL
	,CARD_DESC = TRIM(A.CARD_DESC)
	,MINIMUM_INCOME = A.MINIMUM_INCOME
	,MINIMUM_DEPOSIT = A.MINIMUM_DEPOSIT
	,TRAVEL_FLAG = A.TRAVEL_FLAG
	,COMPETITORS_SHOPPING_FLAG = A.COMPETITORS_SHOPPING_FLAG
	,HEALTH_WELLNESS_FLAG = A.HEALTH_WELLNESS_FLAG
	,CAR_FLAG = A.CAR_FLAG
	,DOCTOR_FLAG = A.DOCTOR_FLAG
FROM VPM_DATA.PM_CREDIT_CARD_PRODUCT_MASTER_8DIGIT A
WHERE A.BIN_NUMBER = VPM_DATA.PM_CUSTOMER_CC.RangCreditCard
  AND LENGTH(VPM_DATA.PM_CUSTOMER_CC.RangCreditCard) = 8;

UPDATE VPM_DATA.PM_CUSTOMER_CC
SET PAYMENT_CARD_TYPE = A.PAYMENT_CARD_TYPE
	,ISSUING_BANK =	A.ISSUING_BANK
	,CARD_TYPE = CASE WHEN COALESCE(A.CARD_TYPE,'') <> '' THEN A.CARD_TYPE
				 ELSE CASE WHEN LEFT(RangCreditCard,1) = '4' THEN 'VISA'
						   WHEN LEFT(RangCreditCard,1) = '5' THEN 'Master Card'
						   WHEN LEFT(RangCreditCard,2) = '35' THEN 'JCB'
						   WHEN LEFT(RangCreditCard,2) = '37' THEN 'AMEX'
						   WHEN LEFT(RangCreditCard,1) = '6' THEN 'UnionPay'
						   END END
	,CARD_LEVEL = A.CARD_LEVEL
	,CARD_DESC = A.CARD_DESC
	,MINIMUM_INCOME = A.MINIMUM_INCOME
	,MINIMUM_DEPOSIT = A.MINIMUM_DEPOSIT
	,TRAVEL_FLAG = A.TRAVEL_FLAG
	,COMPETITORS_SHOPPING_FLAG = A.COMPETITORS_SHOPPING_FLAG
	,HEALTH_WELLNESS_FLAG = A.HEALTH_WELLNESS_FLAG
	,CAR_FLAG = A.CAR_FLAG
	,DOCTOR_FLAG = A.DOCTOR_FLAG
FROM VPM_DATA.PM_CREDIT_CARD_PRODUCT_MASTER_6DIGIT A
WHERE A.BIN_NUMBER = VPM_DATA.PM_CUSTOMER_CC.RangCreditCard
  AND LENGTH(VPM_DATA.PM_CUSTOMER_CC.RangCreditCard) = 6;

UPDATE VPM_DATA.PM_CUSTOMER_CC
SET TRAVEL_FLAG = COALESCE(TRAVEL_FLAG,0),
	COMPETITORS_SHOPPING_FLAG = COALESCE(COMPETITORS_SHOPPING_FLAG,0),
	HEALTH_WELLNESS_FLAG = COALESCE(HEALTH_WELLNESS_FLAG,0),
	CAR_FLAG = COALESCE(CAR_FLAG,0),
	DOCTOR_FLAG = COALESCE(DOCTOR_FLAG,0);

-- addddddddddddddddddddddddddddddddddd

UPDATE VPM_DATA.PM_CUSTOMER_CC
SET CARD_TYPE =  UPPER(CASE WHEN COALESCE(CARD_TYPE,'') <> '' THEN CARD_TYPE
				 ELSE CASE WHEN LEFT(RangCreditCard,1) = '4' THEN 'VISA'
						   WHEN LEFT(RangCreditCard,1) = '5' THEN 'Master Card'
						   WHEN LEFT(RangCreditCard,2) = '35' THEN 'JCB'
						   WHEN LEFT(RangCreditCard,2) = '37' THEN 'AMEX'
						   WHEN LEFT(RangCreditCard,1) = '6' THEN 'UnionPay'
						   END END)
WHERE CARD_TYPE IS NULL;

-- add
--UPDATE CENTRAL CARD
UPDATE VPM_DATA.PM_CUSTOMER_CC
SET PAYMENT_CARD_TYPE = 'CREDIT'
	,ISSUING_BANK =	'CENTRAL'
	,CARD_TYPE = 'MASTERCARD'
	,CARD_LEVEL = CASE WHEN LEFT(RangCreditCard,6) = '537798' THEN 'THE BLACK'
					  WHEN LEFT(RangCreditCard,6) IN ('528560','528561','528566')  THEN 'BLACK'
					  WHEN LEFT(RangCreditCard,6) = '525669' THEN 'LUXE'
					  WHEN LEFT(RangCreditCard,6) IN ('525667','525668')  THEN 'REDZ'
				 ELSE CARD_LEVEL END
	,CARD_DESC = CASE WHEN LEFT(RangCreditCard,6) = '537798' THEN 'CENTRAL THE 1 THE BLACK'
					  WHEN LEFT(RangCreditCard,6) IN ('528560','528561','528566')  THEN 'CENTRAL THE 1 BLACK'
					  WHEN LEFT(RangCreditCard,6) = '525669' THEN 'CENTRAL THE 1 LUXE'
					  WHEN LEFT(RangCreditCard,6) IN ('525667','525668')  THEN 'CENTRAL THE 1 REDZ'
				 ELSE CARD_DESC END
	,MINIMUM_INCOME = CASE WHEN LEFT(RangCreditCard,6) = '537798' THEN NULL
						   WHEN LEFT(RangCreditCard,6) IN ('528560','528561','528566')  THEN NULL
						   WHEN LEFT(RangCreditCard,6) = '525669' THEN 100000
						   WHEN LEFT(RangCreditCard,6) IN ('525667','525668')  THEN 50000
					  ELSE MINIMUM_INCOME END
WHERE (LEFT(RangCreditCard,6) LIKE '537798%' OR
	   LEFT(RangCreditCard,6) LIKE '528560%' OR
	   LEFT(RangCreditCard,6) LIKE '528561%' OR
	   LEFT(RangCreditCard,6) LIKE '528566%' OR
	   LEFT(RangCreditCard,6) LIKE '525669%' OR
	   LEFT(RangCreditCard,6) LIKE '525667%' OR
	   LEFT(RangCreditCard,6) LIKE '525668%');

/** UPDATE CO-BRAND FLAG **/
--add


UPDATE VPM_DATA.PM_CUSTOMER_CC
SET CO_BRAND_FLAG = CASE WHEN CARD_DESC LIKE '%AIR ASIA%' THEN 1
						 WHEN CARD_DESC LIKE '%ROYAL ORCHID%' THEN 1
						 WHEN CARD_DESC LIKE '%HOMEPRO%' THEN 1
						 WHEN CARD_DESC LIKE '%MAKRO%' THEN 1
						 WHEN CARD_DESC LIKE '%BDMS%' THEN 1
						 WHEN CARD_DESC LIKE '%BIG C%' THEN 1
						 WHEN CARD_DESC LIKE '%BUMRUNGRAD%' THEN 1
						 WHEN CARD_DESC LIKE '%SIAM TAKASHIMAYA%' THEN 1
						 WHEN CARD_DESC LIKE '% AIA %' THEN 1
						 WHEN CARD_DESC LIKE '% M %' THEN 1
						 WHEN CARD_DESC LIKE '%TESCO LOTUS%' THEN 1
					END
WHERE LENGTH(RangCreditCard) = 6;

UPDATE VPM_DATA.PM_CUSTOMER_CC
SET CO_BRAND_FLAG = CASE WHEN CARD_DESC LIKE '%AIR ASIA%' THEN 1
						 WHEN CARD_DESC LIKE '%ROYAL ORCHID%' THEN 1
						 WHEN CARD_DESC LIKE '%HOMEPRO%' THEN 1
						 WHEN CARD_DESC LIKE '%MAKRO%' THEN 1
						 WHEN CARD_DESC LIKE '%BDMS%' THEN 1
						 WHEN CARD_DESC LIKE '%BIG C%' THEN 1
						 WHEN CARD_DESC LIKE '%BUMRUNGRAD%' THEN 1
						 WHEN CARD_DESC LIKE '%SIAM TAKASHIMAYA%' THEN 1
						 WHEN CARD_DESC LIKE '% AIA %' THEN 1
						 WHEN CARD_DESC LIKE '% M %' THEN 1
						 WHEN CARD_DESC LIKE '%TESCO LOTUS%' THEN 1
						 WHEN CARD_DESC LIKE '%KING POWER%' THEN 1
						 WHEN CARD_DESC LIKE '%MERCEDES%' THEN 1
						 WHEN CARD_DESC LIKE '%B-QUIK%' THEN 1
						 WHEN CARD_DESC LIKE '%TOYOTA%' THEN 1
						 WHEN CARD_DESC LIKE '%TANG HUA SENG%' THEN 1
						 WHEN CARD_DESC LIKE '%(THAI DEPARTMENT STORES POOL)%' THEN 1
					END
WHERE LENGTH(RangCreditCard) = 8;


/******************************************************************************** AGGREGATE MONTLY CREDIT CARD SPEND BY CUSTOMER ********************************************************************************/
 
--16m 51s
DROP TABLE IF EXISTS VPM_DATA.PM_CC_MONTHLY_SPEND_BY_CARD_STAGING1;
CREATE TABLE VPM_DATA.PM_CC_MONTHLY_SPEND_BY_CARD_STAGING1 AS
SELECT A.CUSTOMER_ID AS CUSTOMER_ID
	  ,A.RangCreditCard
	  ,vpm_data.datediff('month',txn_date,vpm_data.get_end_date(now())) AS TXN_MONTH
	  ,SUM(Amount) AS MONTHLY_SPEND
FROM VPM_DATA.TEMP_PM_CUSTOMER_CC A
LEFT JOIN VPM_DATA.TEMP_CC_ALL_TXN B
ON A.CUSTOMER_ID = B.CUSTOMERID
AND A.RangCreditCard = B.RangCreditCard
AND A.GroupCard = B.GroupCard
LEFT JOIN VPM_DATA.PM_CUSTOMER_CC C
ON A.CUSTOMER_ID = C.CUSTOMER_ID
AND A.RangCreditCard = C.RangCreditCard
WHERE C.PAYMENT_CARD_TYPE = 'CREDIT'
GROUP BY A.CUSTOMER_ID
   ,A.RangCreditCard
   ,vpm_data.datediff('month',txn_date,vpm_data.get_end_date(now()));

DROP TABLE IF EXISTS VPM_DATA.PM_CC_MONTHLY_SPEND_BY_CARD_STAGING2;
CREATE TABLE VPM_DATA.PM_CC_MONTHLY_SPEND_BY_CARD_STAGING2 AS
SELECT UPPER(CUSTOMERID) AS CUSTOMER_ID
	,SUBSTRING(CardnumberwithMasking,1,6) AS RangCreditCard
	,vpm_data.datediff('month',TransactionDate::DATE,vpm_data.get_end_date(now())) AS TXN_MONTH
	,SUM(SpendingAmount) AS MONTHLY_SPEND   
FROM analysis_data.GCS_DetSpd
WHERE BUorNonBU = 'NON BU'
AND CAST(TransactionDate AS DATE) >= VPM_DATA.GET_START_DATE(NOW())-INTERVAL '12 MONTH'
AND CAST(TransactionDate AS DATE) <= VPM_DATA.GET_END_DATE(NOW())
AND CustomerID <> ''
GROUP BY CUSTOMERID,SUBSTRING(CardnumberwithMasking,1,6)
	,vpm_data.datediff('month',TransactionDate::DATE,vpm_data.get_end_date(now()));
;

DROP TABLE IF EXISTS VPM_DATA.PM_CC_MONTHLY_SPEND_BY_CARD;
SELECT * INTO VPM_DATA.PM_CC_MONTHLY_SPEND_BY_CARD
FROM VPM_DATA.PM_CC_MONTHLY_SPEND_BY_CARD_STAGING1
UNION ALL SELECT * FROM VPM_DATA.PM_CC_MONTHLY_SPEND_BY_CARD_STAGING2;

CREATE INDEX CUSTOMER ON VPM_DATA.PM_CC_MONTHLY_SPEND_BY_CARD(CUSTOMER_ID); --13.920s

UPDATE VPM_DATA.SALESSKU_LOG_TIME SET end_time = now() WHERE task = 'Step1.3.2 Create Credit Card payment Table' AND EXTRACT(MONTH FROM start_time) = EXTRACT(MONTH FROM now());