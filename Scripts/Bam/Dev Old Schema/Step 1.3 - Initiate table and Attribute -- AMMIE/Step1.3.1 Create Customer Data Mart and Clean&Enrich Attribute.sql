
--63m 24s
--1hr
--5m 44s
INSERT INTO VPM_DATA.SALESSKU_LOG_TIME(task,start_time) VALUES ('Step1.3.1 Create Customer Data Mart and Clean&Enrich Attribute',NOW());

DROP TABLE IF EXISTS VPM_DATA.TEMP_CUSTOMER_CG_WHOLEBASE;
CREATE TABLE VPM_DATA.TEMP_CUSTOMER_CG_WHOLEBASE as
SELECT CUSTOMERID
FROM (
	SELECT CUSTOMERID
	FROM analysis_data.Customer_SBL
	UNION
	SELECT CustomerID AS CUSTOMERID
	FROM analysis_data.SalesSubCat_Table_02_2018
	UNION
	SELECT CustomerID AS CUSTOMERID
	FROM analysis_data.SalesSubCat_Table_02_2019
	UNION
	SELECT customerid AS CUSTOMERID
	FROM VPM_BACKUP.CG_RANKING_19_20
) A;
--(16402947 row(s) affected)

/*SELECT COUNT(*)
FROM analysis_data.Customer_SBL*/
--16305032
--4m 50s
DROP TABLE IF EXISTS VPM_DATA.TEMP_CUST_SBL;
CREATE TABLE VPM_DATA.TEMP_CUST_SBL as
SELECT  Z.CUSTOMERID,A.MEMBER_NUMBER,A.DateofBirth,B.NoofChildren,B.AgeOfChildren
		,A.HomeCity,A.HomeDistrict,A.HomeSubDistrict,A.HomePostalCode AS PostalCode
		,A.RegisterDate,A.BuidRegister AS BURegister,A.BranchidRegister AS BranchRegister,NULL AS IsDiamond
		,A.Nationality AS NATIONALITY
		,CASE WHEN A.GENDER = 'Female' THEN 'F'
			  WHEN A.GENDER = 'Male' THEN 'M'
			  WHEN A.GENDER = 'Unknown' THEN 'U'
			  ELSE A.GENDER END AS GENDER
		,CASE WHEN A.MaritalStatus = 'Unknown' THEN 'U'
			  WHEN A.MaritalStatus = 'Married' THEN 'M'
			  WHEN A.MaritalStatus = 'Single' THEN 'S'
			  ELSE 'U' END AS MARITALSTATUS
		,A.WorkType AS OCCUPATION
		,A.EducationLevel AS EDUCATIONLEVEL
        ,A.PersonalIncome AS MONTHLYINCOME
        ,A.HouseholdIncome AS HOUSEHOLDINCOME
        ,A.IsMobilePhone
        ,A.IsAddress
        ,A.IsEmail
        ,A.Is_Send_SMS_Thai
        ,A.Is_OnlineAccount AS HaveOnlineAccountFlag 
		,A.Is_OnlineAccount AS LoginWebsite
FROM VPM_DATA.TEMP_CUSTOMER_CG_WHOLEBASE Z
LEFT JOIN analysis_data.Customer_SBL A
ON Z.CUSTOMERID = A.CUSTOMERID
LEFT JOIN VPM_DATA.PM_SVOC_JAN B
ON Z.CUSTOMERID = B.CUSTOMER_ID;

--4m 18s
CREATE INDEX temp_cust_sbl_customerid_idx ON vpm_data.temp_cust_sbl (customerid);
CREATE INDEX temp_cust_sbl_buregister_idx ON vpm_data.temp_cust_sbl (buregister);
CREATE INDEX temp_cust_sbl_branchregister_idx ON vpm_data.temp_cust_sbl (branchregister);
CREATE INDEX temp_cust_sbl_postalcode_idx ON vpm_data.temp_cust_sbl (postalcode);

--2m 12s
DO $$
DECLARE START_DATE DATE := (SELECT VPM_DATA.GET_START_DATE(NOW()));
		END_DATE DATE := (SELECT VPM_DATA.GET_END_DATE(NOW()));
BEGIN
	DROP TABLE IF EXISTS VPM_DATA.PM_SVOC;
	CREATE TABLE VPM_DATA.PM_SVOC AS
	SELECT /*DISTINCT*/ A.CUSTOMERID AS CUSTOMER_ID
			,A.MEMBER_NUMBER
			,CAST(NULL AS VARCHAR(50)) AS T1_CARD_NUMBER
			,A.DateofBirth AS DOB
			,CASE WHEN DATE_PART('year',A.DateofBirth::DATE) != 1900 
			THEN DATE_PART('year',age(A.DateofBirth::date)) END AS CURRENT_AGE
			,CASE WHEN DATE_PART('year',A.DateofBirth::DATE) != 1900 
			THEN DATE_PART('year',age(A.RegisterDate::date,A.DateofBirth::date)) END AS ENTRY_AGE
			,A.NATIONALITY
			,A.GENDER
			,A.MARITALSTATUS
			,NoofChildren AS NOOFCHILDREN
			,AgeOfChildren AS AGEOFCHILDREN
			,A.HomeCity AS PROVINCE_TH
			,A.HomeDistrict AS AMPHOR
			,A.HomeSubDistrict AS TAMBON
			,A.PostalCode AS ZIPCODE
			,OCCUPATION
			,A.EDUCATIONLEVEL
			,A.MONTHLYINCOME
			,A.HOUSEHOLDINCOME      
			,A.RegisterDate AS REGISTERDATE
			,A.BURegister AS CardRegisterBUID
			,A.BranchRegister AS CardRegisterBranchID
			,J.BUCode AS CardRegisterBU
			,K.BranchEnglishname AS CardRegisterLocation
			,A.HaveOnlineAccountFlag 
			,A.LoginWebsite
			,NULL AS LoginMobile
			,A.IsMobilePhone
			,A.IsAddress
			,A.IsEmail
			,A.Is_Send_SMS_Thai
			,CASE WHEN L."T1C_Customer_ID" IS NOT NULL THEN 1 ELSE 0 END AS MOBILEAPP_USER	--CHANGE 
			,A.IsDiamond AS DIAMOND
			,NULL AS WHOLE_SALE
			--,B.IsWholeSaler AS WHOLE_SALE
			,CAST(DATE_PART('year',age(END_DATE::date,A.RegisterDate::date)) AS INT) AS TENURE
			,CASE WHEN NoofChildren <> '' AND NoofChildren::INT > 0 THEN 1 ELSE 0 END  AS DEPENDANT_FLAG
			,M."REGION2" AS PROVINCE
			,CAST(NULL AS VARCHAR(50)) AS REGION_ECONOMY_GRP
			,CAST(NULL AS VARCHAR(50)) AS AgeofChildren1
			,CAST(NULL AS VARCHAR(50)) AS AgeofChildren2
			,CAST(NULL AS VARCHAR(50)) AS AgeofChildren3
			,CAST(NULL AS VARCHAR(50)) AS AgeofChildren4
			,CAST(NULL AS INT) AS CURRENT_CHILD1_AGE
			,CAST(NULL AS INT) AS CURRENT_CHILD2_AGE
			,CAST(NULL AS INT) AS CURRENT_CHILD3_AGE
			,CAST(NULL AS INT) AS CURRENT_CHILD4_AGE
			,CAST(NULL AS VARCHAR(100)) AS PRIMARY_BANK_CARD
			,CAST(NULL AS VARCHAR(100)) AS PRIMARY_BANK_CARD_FACE
			,CAST(NULL AS INT) AS NO_BU_PURCHASE
			,CAST(NULL AS INT) AS NO_TXN
			,CAST(NULL AS INT) AS NO_MONTH
			,CAST(NULL AS FLOAT) AS SUM_TXN_AMT
			,CAST(NULL AS INT) AS NO_DAYS_SPEND
			,CAST(NULL AS FLOAT) AS AVG_TXN_AMT_PER_M
			,CAST(NULL AS INT) AS NO_OF_CREDITCARD_ALL
			,CAST(NULL AS INT) AS NO_CREDITCARD_TXN
			,CAST(NULL AS INT) AS NO_CREDITCARD_TXN_6M
			,CAST(NULL AS INT) AS MONTH_90PCT_SPENDING
			,CAST(NULL AS INT) AS HUSBAND_PURCHASE_FLAG
			,CAST(NULL AS FLOAT) AS ACTIVE_M_HUSBAND
			,CAST(NULL AS FLOAT) AS AVG_HUSBAND_M
			,CAST(NULL AS INT) AS DECILE_HUSBAND_M
			,CAST(NULL AS NUMERIC(12,0)) AS FREQ_HUSBAND
			,CAST(NULL AS NUMERIC(12,0)) AS FREQ_DAY_MEN
			,CAST(NULL AS FLOAT) AS QTY_DAY_HUSBAND
			,CAST(NULL AS FLOAT) AS HUSBAND_SCORE
			,CAST(NULL AS INT) AS WIFE_PURCHASE_FLAG
			,CAST(NULL AS FLOAT) AS ACTIVE_M_WIFE
			,CAST(NULL AS FLOAT) AS AVG_WIFE_M
			,CAST(NULL AS INT) AS DECILE_WIFE_M
			,CAST(NULL AS NUMERIC(12,0)) AS FREQ_WIFE
			,CAST(NULL AS NUMERIC(12,0)) AS FREQ_DAY_WOMEN
			,CAST(NULL AS FLOAT) AS QTY_DAY_WIFE
			,CAST(NULL AS FLOAT) AS WIFE_SCORE
			,CAST(NULL AS FLOAT) AS MAN_SUM_AMT
			,CAST(NULL AS INT) AS MAN_ACTIVE_M
			,CAST(NULL AS SMALLINT) AS MAN_FREQ
			,CAST(NULL AS FLOAT) AS MAN_AVG_AMT_M
			,CAST(NULL AS INT) AS MAN_DECILE_M
			,CAST(NULL AS SMALLINT) AS MAN_QTY_DAY
			,CAST(NULL AS FLOAT) AS MAN_PRODUCT_SCORE
			,CAST(NULL AS FLOAT) AS WOMAN_SUM_AMT
			,CAST(NULL AS INT) AS WOMAN_ACTIVE_M
			,CAST(NULL AS SMALLINT) AS WOMAN_FREQ
			,CAST(NULL AS FLOAT) AS WOMAN_AVG_AMT_M
			,CAST(NULL AS INT) AS WOMAN_DECILE_M
			,CAST(NULL AS SMALLINT) AS WOMAN_QTY_DAY
			,CAST(NULL AS FLOAT) AS WOMAN_PRODUCT_SCORE
			--,CAST(NULL AS NUMERIC(5,4)) AS KIDS_SCORE
			,CAST(NULL AS FLOAT) AS FAMILY_CARD_SCORE
			,CAST(NULL AS SMALLINT) AS FAMILY_CARD_FLAG
			,CAST(NULL AS SMALLINT) AS CROSS_REGION_FLAG
			,CAST(NULL AS INT) AS SHAREDCARD_FLAG
			,CAST(NULL AS VARCHAR(10)) AS CLEANED_GENDER
			,CAST(NULL AS VARCHAR(10)) AS INFERRED_GENDER
			,CAST(NULL AS VARCHAR(15)) AS INFERRED_EDUCATION
			,CAST(NULL AS NUMERIC(20,5)) AS PROB_MARRIED
			,CAST(NULL AS VARCHAR(15)) AS MARITAL_STATUS_INFER
			,CAST(NULL AS VARCHAR(30)) AS LIFE_STAGE
			,CAST(NULL AS INT) AS HM_PEAK_M
			,CAST(NULL AS FLOAT) AS HM_QTY_DAY_3M
			,CAST(NULL AS FLOAT) AS HM_FREQ_3M
			,CAST(NULL AS FLOAT) AS HM_NO_CAT_3M
			,CAST(NULL AS FLOAT) AS HM_AVG_LAGTIME_3M
			,CAST(NULL AS FLOAT) AS HM_AVG_SPEND_3M
			,CAST(NULL AS INT) AS HOUSEMOVING
			,CAST(NULL AS FLOAT) AS HM_SCORE
			,CAST(NULL AS FLOAT) AS HH_QTY_DAY
			,CAST(NULL AS FLOAT) AS HH_FREQ
			,CAST(NULL AS FLOAT) AS HH_NO_CAT
			,CAST(NULL AS FLOAT) AS HH_AVG_SPEND
			,CAST(NULL AS FLOAT) AS HH_SCORE
	FROM VPM_DATA.TEMP_CUST_SBL A
	/*LEFT JOIN analysis_data.Customer_SBL B --Project_CRM_Analysis B
	ON A.CUSTOMERID = B.CUSTOMERID*/
	LEFT JOIN analysis_data.BUSinessUnit_LT J
	ON A.BURegister = J.BUID
	LEFT JOIN analysis_data.Branch_LT K
	ON A.BranchRegister = K.BranchID AND A.BURegister = K.BUID
	LEFT JOIN VPM_DATA.Perx_CustomerMobileUserID L
	ON A.CUSTOMERID = L."T1C_Customer_ID"
	LEFT JOIN (SELECT DISTINCT "ZIP","REGION2" FROM VPM_DATA.GeoPC) M
	ON A.PostalCode = M."ZIP";
END $$;

--CARD
--50m 6s
DROP TABLE IF EXISTS VPM_DATA.TEMP_CARD;
SELECT CUSTOMERID AS CUSTOMER_ID,CardNo,LENGTH(CardNo) AS LEN_CARD
INTO VPM_DATA.TEMP_CARD
FROM analysis_data.CardMember_SBL
GROUP BY CUSTOMERID,CardNo;

SELECT CUSTOMER_ID,CardNo,COUNT(*)
FROM VPM_DATA.TEMP_CARD
GROUP BY CUSTOMER_ID,CardNo
HAVING COUNT(*) > 1;

DROP TABLE IF EXISTS VPM_DATA.TEMP_CARD_FINAL;
SELECT *,ROW_NUMBER() OVER (PARTITION BY CUSTOMER_ID ORDER BY LEN_CARD DESC NULLS LAST) AS ROW
INTO VPM_DATA.TEMP_CARD_FINAL
FROM VPM_DATA.TEMP_CARD;

DELETE FROM VPM_DATA.TEMP_CARD_FINAL
WHERE ROW > 1;

--SELECT CUSTOMER_ID,COUNT(*)
--FROM VPM_DATA.TEMP_CARD_FINAL
--GROUP BY CUSTOMER_ID
--HAVING COUNT(*) > 1;

CREATE INDEX CUSTOMER_ID_TEMP_CARD_FINAL ON VPM_DATA.TEMP_CARD_FINAL (CUSTOMER_ID);

UPDATE VPM_DATA.PM_SVOC
SET T1_CARD_NUMBER = A.CardNo
FROM VPM_DATA.TEMP_CARD_FINAL A
WHERE A.CUSTOMER_ID = PM_SVOC.CUSTOMER_ID;

-----CHECK UNIQUE
--SELECT CUSTOMER_ID,COUNT(*)
--FROM VPM_DATA.PM_SVOC
--GROUP BY CUSTOMER_ID
--HAVING COUNT(*) > 1;

UPDATE VPM_DATA.PM_SVOC
SET CURRENT_AGE = NULL
WHERE CURRENT_AGE < 0 OR CURRENT_AGE > 100;


-- UPDATE NO ZIP CODE BUT HAVE PROVINCE
UPDATE VPM_DATA.PM_SVOC
SET PROVINCE = TMP."PROVINCE"
FROM VPM_DATA.REF_PROVINCE_ZIP_UNIQUE TMP 
WHERE UPPER(PM_SVOC.PROVINCE_TH) = TMP."PROVINCE"
AND PM_SVOC.PROVINCE IS NULL;

-- UPDATE NO ZIP CODE BUT HAVE TH PROVINCE
UPDATE VPM_DATA.PM_SVOC
SET PROVINCE = TMP."PROVINCE"
FROM VPM_DATA.REF_PROVINCE_ZIP_UNIQUE TMP  
WHERE UPPER(PM_SVOC.PROVINCE_TH) = TMP."PROVINCE_TH"
AND PM_SVOC.PROVINCE IS NULL;

UPDATE VPM_DATA.PM_SVOC
SET PROVINCE = CASE WHEN UPPER(PROVINCE_TH) = 'CHIANGMAI' THEN 'Chiang Mai'
							  WHEN UPPER(PROVINCE_TH) = 'SAMUTSAKHON' THEN 'Samut Sakhon'
							  WHEN UPPER(PROVINCE_TH) = 'SURATTHANI' THEN 'Surat Thani'
							  WHEN UPPER(PROVINCE_TH) = 'SAMUTPRAKAN' THEN 'Samut Prakan'
							  END
WHERE PROVINCE IS NULL;

UPDATE VPM_DATA.PM_SVOC
SET PROVINCE  = 'Chiang Mai'
WHERE UPPER(PROVINCE) IN ('CHIANGMAI','Chiangmai');

UPDATE VPM_DATA.PM_SVOC
SET PROVINCE = 'U'
WHERE PROVINCE IN ('','---','00000','f','r','t') OR PROVINCE IS NULL;

UPDATE VPM_DATA.PM_SVOC 
SET REGION_ECONOMY_GRP = CASE WHEN PROVINCE = 'Bangkok' THEN 'Bangkok'
							WHEN PROVINCE = 'Samut Prakan' THEN 'Industrial Area'
							WHEN PROVINCE = 'Nonthaburi' THEN 'Big Cities'
							WHEN PROVINCE = 'Samut Sakhon' THEN 'Industrial Area'
							WHEN PROVINCE = 'Pathum Thani' THEN 'Industrial Area'
							WHEN PROVINCE = 'Chonburi' THEN 'Industrial Area'
							WHEN PROVINCE = 'Phra Nakhon Si Ayutthaya' THEN 'Industrial Area'
							WHEN PROVINCE = 'Nakhon Pathom' THEN 'Industrial Area'
							WHEN PROVINCE = 'Rayong' THEN 'Industrial Area'
							WHEN PROVINCE = 'Phuket' THEN 'Tourist Area'
							WHEN PROVINCE = 'Songkhla' THEN 'Big Cities'
							WHEN PROVINCE = 'Saraburi' THEN 'Industrial Area'
							WHEN PROVINCE = 'Chachoengsao' THEN 'Industrial Area'
							WHEN PROVINCE = 'Khon Kaen' THEN 'Industrial Area'
							WHEN PROVINCE = 'Ratchaburi' THEN 'Industrial Area'
							WHEN PROVINCE = 'Nakhon Si Thammarat' THEN 'Big Cities'
							WHEN PROVINCE = 'Nakhon Ratchasima' THEN 'Big Cities'
							WHEN PROVINCE = 'Pattani' THEN 'Agriculture'
							WHEN PROVINCE = 'Udon Thani' THEN 'Agriculture'
							WHEN PROVINCE = 'Suphan Buri' THEN 'Agriculture'
							WHEN PROVINCE = 'Buri Ram' THEN 'Agriculture'
							WHEN PROVINCE = 'Prachin Buri' THEN 'Industrial Area'
							WHEN PROVINCE = 'Surin' THEN 'Border'
							WHEN PROVINCE = 'Roi Et' THEN 'Agriculture'
							WHEN PROVINCE = 'Lopburi' THEN 'Industrial Area'
							WHEN PROVINCE = 'Maha Sarakham' THEN 'Agriculture'
							WHEN PROVINCE = 'Sisaket' THEN 'Border'
							WHEN PROVINCE = 'Ubon Ratchathani' THEN 'Border'
							WHEN PROVINCE = 'Nakhon Sawan' THEN 'Agriculture'
							WHEN PROVINCE = 'Chiang Mai' THEN 'Tourist Area'
							WHEN PROVINCE = 'Trang' THEN 'Agriculture'
							WHEN PROVINCE = 'Kalasin' THEN 'Agriculture'
							WHEN PROVINCE = 'Narathiwat' THEN 'Agriculture'
							WHEN PROVINCE = 'Chiang Rai' THEN 'Agriculture'
							WHEN PROVINCE = 'Surat Thani' THEN 'Tourist Area'
							WHEN PROVINCE = 'Nong Khai' THEN 'Border'
							WHEN PROVINCE = 'Sing Buri' THEN 'Industrial Area'
							WHEN PROVINCE = 'Krabi' THEN 'Tourist Area'
							WHEN PROVINCE = 'Prachuap Khiri Khan' THEN 'Tourist Area'
							WHEN PROVINCE = 'Samut Songkhram' THEN 'Industrial Area'
							WHEN PROVINCE = 'Ang Thong' THEN 'Agriculture'
							WHEN PROVINCE = 'Kamphaeng Phet' THEN 'Agriculture'
							WHEN PROVINCE = 'Chanthaburi' THEN 'Agriculture'
							WHEN PROVINCE = 'Lamphun' THEN 'Industrial Area'
							WHEN PROVINCE = 'Sakon Nakhon' THEN 'Agriculture'
							WHEN PROVINCE = 'Phatthalung' THEN 'Agriculture'
							WHEN PROVINCE = 'Nakhon Phanom' THEN 'Agriculture'
							WHEN PROVINCE = 'Satun' THEN 'Agriculture'
							WHEN PROVINCE = 'Phitsanulok' THEN 'Agriculture'
							WHEN PROVINCE = 'Chumphon' THEN 'Agriculture'
							WHEN PROVINCE = 'Chai Nat' THEN 'Agriculture'
							WHEN PROVINCE = 'Bueng Kan' THEN 'Agriculture'
							WHEN PROVINCE = 'Phichit' THEN 'Agriculture'
							WHEN PROVINCE = 'Chaiyaphum' THEN 'Agriculture'
							WHEN PROVINCE = 'Phetchabun' THEN 'Agriculture'
							WHEN PROVINCE = 'Kanchanaburi' THEN 'Border'
							WHEN PROVINCE = 'Yala' THEN 'Agriculture'
							WHEN PROVINCE = 'Sukhothai' THEN 'Agriculture'
							WHEN PROVINCE = 'Lampang' THEN 'Agriculture'
							WHEN PROVINCE = 'Phetchaburi' THEN 'Agriculture'
							WHEN PROVINCE = 'Yasothon' THEN 'Agriculture'
							WHEN PROVINCE = 'Nong Bua Lamphu' THEN 'Agriculture'
							WHEN PROVINCE = 'Nakhon Nayok' THEN 'Agriculture'
							WHEN PROVINCE = 'Trat' THEN 'Agriculture'
							WHEN PROVINCE = 'Phang Nga' THEN 'Tourist Area'
							WHEN PROVINCE = 'Amnat Charoen' THEN 'Agriculture'
							WHEN PROVINCE = 'Sa Kaeo' THEN 'Agriculture'
							WHEN PROVINCE = 'Loei' THEN 'Agriculture'
							WHEN PROVINCE = 'Tak' THEN 'Border'
							WHEN PROVINCE = 'Phayao' THEN 'Agriculture'
							WHEN PROVINCE = 'Uttaradit' THEN 'Border'
							WHEN PROVINCE = 'Mukdahan' THEN 'Agriculture'
							WHEN PROVINCE = 'Phrae' THEN 'Agriculture'
							WHEN PROVINCE = 'Ranong' THEN 'Agriculture'
							WHEN PROVINCE = 'Uthai Thani' THEN 'Agriculture'
							WHEN PROVINCE = 'Nan' THEN 'Border'
							WHEN PROVINCE = 'Mae Hong Son' THEN 'Border'
							END
WHERE REGION_ECONOMY_GRP IS NULL;

UPDATE VPM_DATA.PM_SVOC
SET AgeofChildren1 = SPLIT_PART(AGEOFCHILDREN, ',',1),
AgeofChildren2 = SPLIT_PART(AGEOFCHILDREN, ',',2),
AgeofChildren3 = SPLIT_PART(AGEOFCHILDREN, ',',3),
AgeofChildren4 = SPLIT_PART(AGEOFCHILDREN, ',',4)
WHERE AGEOFCHILDREN <> '';


UPDATE VPM_DATA.PM_SVOC
SET AgeofChildren1 = CASE WHEN VPM_DATA.ISNUMERIC(LTRIM(RTRIM(REPLACE(AgeofChildren1,'?','')))) != 0 AND LTRIM(RTRIM(REPLACE(AgeofChildren1,'?',''))) NOT IN ('090000000000000002','18,13')   THEN LTRIM(RTRIM(REPLACE(AgeofChildren1,'?','')))			
					 ELSE NULL END,
AgeofChildren2 = CASE WHEN VPM_DATA.ISNUMERIC(LTRIM(RTRIM(REPLACE(AgeofChildren2,'?','')))) != 0 AND LTRIM(RTRIM(REPLACE(AgeofChildren2,'?',''))) NOT IN ('090000000000000002','18,13')    THEN LTRIM(RTRIM(REPLACE(AgeofChildren2,'?','')))					
					 ELSE NULL END,
AgeofChildren3 = CASE WHEN VPM_DATA.ISNUMERIC(LTRIM(RTRIM(REPLACE(AgeofChildren3,'?','')))) != 0 AND LTRIM(RTRIM(REPLACE(AgeofChildren3,'?',''))) NOT IN ('090000000000000002','18,13')    THEN LTRIM(RTRIM(REPLACE(AgeofChildren3,'?','')))					
					 ELSE NULL END;
					
UPDATE VPM_DATA.PM_SVOC
SET AgeofChildren4 = CASE WHEN LEFT(AgeofChildren4,NULLIF(POSITION(',' IN AgeofChildren4),0)-1) IS NULL THEN AgeofChildren4 ELSE LEFT(AgeofChildren4,NULLIF(POSITION(',' IN AgeofChildren4),0)-1) END;

UPDATE VPM_DATA.PM_SVOC
SET AgeofChildren4 = CASE WHEN VPM_DATA.ISNUMERIC(LTRIM(RTRIM(REPLACE(AgeofChildren4,'?','')))) != 0 AND LTRIM(RTRIM(REPLACE(AgeofChildren4,'?',''))) NOT IN ('090000000000000002','18,13')   THEN LTRIM(RTRIM(REPLACE(AgeofChildren4,'?','')))					
					 ELSE NULL END;

ALTER TABLE VPM_DATA.PM_SVOC 
ALTER COLUMN AgeofChildren1 TYPE INT USING (AgeofChildren1::int);

ALTER TABLE VPM_DATA.PM_SVOC 
ALTER COLUMN AgeofChildren2 TYPE INT USING (AgeofChildren2::int);

ALTER TABLE VPM_DATA.PM_SVOC 
ALTER COLUMN AgeofChildren3 TYPE INT USING (AgeofChildren3::int);

ALTER TABLE VPM_DATA.PM_SVOC 
ALTER COLUMN AgeofChildren4 TYPE INT USING (AgeofChildren4::int);

UPDATE VPM_DATA.PM_SVOC
SET CURRENT_CHILD1_AGE = AgeofChildren1 + TENURE,
CURRENT_CHILD2_AGE = AgeofChildren2 + TENURE,
CURRENT_CHILD3_AGE = AgeofChildren3 + TENURE,
CURRENT_CHILD4_AGE = AgeofChildren4 + TENURE
WHERE AgeofChildren1 > 0;

UPDATE VPM_DATA.PM_SVOC
SET CURRENT_CHILD2_AGE = CASE WHEN CURRENT_CHILD1_AGE = CURRENT_CHILD2_AGE THEN NULL
                              ELSE CURRENT_CHILD2_AGE END;
                             
UPDATE VPM_DATA.PM_SVOC
SET CURRENT_CHILD3_AGE = CASE WHEN CURRENT_CHILD2_AGE = CURRENT_CHILD3_AGE THEN NULL
                              ELSE CURRENT_CHILD3_AGE END;
                             
UPDATE VPM_DATA.PM_SVOC
SET CURRENT_CHILD4_AGE = CASE WHEN CURRENT_CHILD3_AGE = CURRENT_CHILD4_AGE THEN NULL
                              ELSE CURRENT_CHILD4_AGE END;
                              
UPDATE VPM_DATA.SALESSKU_LOG_TIME SET end_time = now() WHERE task = 'Step1.3.1 Create Customer Data Mart and Clean&Enrich Attribute' AND EXTRACT(MONTH FROM start_time) = EXTRACT(MONTH FROM now());