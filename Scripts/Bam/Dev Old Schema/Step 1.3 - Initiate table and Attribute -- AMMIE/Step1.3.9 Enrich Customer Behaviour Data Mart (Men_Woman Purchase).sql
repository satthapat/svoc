--12m 44s
INSERT INTO VPM_DATA.SALESSKU_LOG_TIME(task,start_time) VALUES ('Step1.3.9 Enrich Customer Behaviour Data Mart (Men_Woman Purchase)',NOW());

DROP TABLE IF EXISTS VPM_DATA.TEMP_GENDER;
SELECT BUID,BU_NAME,SKU_CODE,GENDER_FINAL
INTO VPM_DATA.TEMP_GENDER
FROM VPM_DATA.PM_PRODUCT_MASTER
WHERE UPPER(GENDER_FINAL) IN ('MALE','FEMALE');
/*
DROP TABLE IF EXISTS VPM_DATA.TEMP_TABLE_STAGING_1; --38m 21s
SELECT CUSTOMER_ID,TXN_DATE,GENDER_FINAL,SUM(TXN_AMT) AS TXN_AMT,SUM(QTY) AS QTY
INTO VPM_DATA.TEMP_TABLE_STAGING_1
FROM VPM_DATA.PM_ALL_SALESSKU_1YR A
INNER JOIN TEMP_GENDER B
ON A.BUID = B.BUID 
	AND A.SKU_CODE = B.SKU_CODE
GROUP BY CUSTOMER_ID,TXN_DATE,GENDER_FINAL;

DROP TABLE IF EXISTS VPM_DATA.TEMP_TABLE_STAGING_2; --45m 34s
SELECT CUSTOMER_ID,TXN_DATE,GENDER_FINAL,SUM(TXN_AMT) AS TXN_AMT,SUM(QTY) AS QTY
INTO VPM_DATA.TEMP_TABLE_STAGING_2
FROM VPM_DATA.PM_TOPS_SALESSKU_1YR A
INNER JOIN TEMP_GENDER B
ON A.BUID = B.BUID AND A.SKU_CODE = B.SKU_CODE
GROUP BY CUSTOMER_ID,TXN_DATE,GENDER_FINAL;

DROP TABLE IF EXISTS VPM_DATA.TEMP_TABLE_STAGING_3; --22m 48s
SELECT CUSTOMER_ID,TXN_DATE,GENDER_FINAL,SUM(TXN_AMT) AS TXN_AMT,SUM(QTY) AS QTY
INTO VPM_DATA.TEMP_TABLE_STAGING_3
FROM VPM_DATA.PM_CFM_SALESSKU_1YR A
INNER JOIN TEMP_GENDER B
ON A.BUID = B.BUID AND A.SKU_CODE = B.SKU_CODE
GROUP BY CUSTOMER_ID,TXN_DATE,GENDER_FINAL;

DROP TABLE IF EXISTS VPM_DATA.TEMP_TABLE; --21.723s
SELECT CUSTOMER_ID,TXN_DATE,GENDER_FINAL AS PRODUCT_GENDER,TXN_AMT,QTY
INTO VPM_DATA.TEMP_TABLE
FROM
(SELECT * FROM VPM_DATA.TEMP_TABLE_STAGING_1
UNION ALL SELECT * FROM VPM_DATA.TEMP_TABLE_STAGING_2
UNION ALL SELECT * FROM VPM_DATA.TEMP_TABLE_STAGING_3
) A;*/

DROP TABLE IF EXISTS VPM_DATA.TEMP_GENDER_2; --2m 22s
SELECT	A.CUSTOMER_ID
		,PRODUCT_GENDER
		,SUM(TXN_AMT) AS SUM_AMT
		,COUNT(DISTINCT EXTRACT(MONTH FROM TXN_DATE)) AS ACTIVE_M
		,(SUM(TXN_AMT)*1.0)/COUNT(DISTINCT EXTRACT(MONTH FROM TXN_DATE)) AS AVG_AMT_M
		,NTILE(10)OVER(ORDER BY (SUM(TXN_AMT)*1.0)/COUNT(DISTINCT EXTRACT(MONTH FROM TXN_DATE)) DESC) AS DECILE_M
		,SUM(QTY)/COUNT(DISTINCT TXN_DATE) AS QTY_DAY
		,COUNT(*) AS FREQ
		,CAST(NULL AS FLOAT) AS MAN_PRODUCT_SCORE
		,CAST(NULL AS FLOAT) AS WOMAN_PRODUCT_SCORE
INTO VPM_DATA.TEMP_GENDER_2 
FROM VPM_DATA.TEMP_TABLE A
INNER JOIN VPM_DATA.PM_SVOC B
ON A.CUSTOMER_ID = B.CUSTOMER_ID
GROUP BY A.CUSTOMER_ID,PRODUCT_GENDER
;

-- MAN_PRODUCT_SCORE
DO $$
DECLARE ACTIVE_CUT FLOAT := (SELECT MIN(ACTIVE_M) FROM (
								SELECT CUSTOMER_ID, ACTIVE_M, PERCENT_RANK() OVER(ORDER BY ACTIVE_M DESC NULLS LAST) AS PCT_RANK
								FROM VPM_DATA.TEMP_GENDER_2
								WHERE PRODUCT_GENDER = 'MALE') A
							WHERE PCT_RANK <= 0.2); 
		FREQ_CUT FLOAT := (SELECT MIN(FREQ) FROM (
								SELECT CUSTOMER_ID, FREQ, PERCENT_RANK() OVER(ORDER BY FREQ DESC NULLS LAST) AS PCT_RANK
								FROM VPM_DATA.TEMP_GENDER_2 
								WHERE PRODUCT_GENDER = 'MALE') A
							WHERE PCT_RANK <= 0.2);
		QTY_DAY_CUT FLOAT := (SELECT MIN(QTY_DAY) FROM (
								SELECT CUSTOMER_ID, QTY_DAY, PERCENT_RANK() OVER(ORDER BY QTY_DAY DESC NULLS LAST) AS PCT_RANK 
								FROM VPM_DATA.TEMP_GENDER_2 
								WHERE PRODUCT_GENDER = 'MALE') A	
							WHERE PCT_RANK <= 0.2);
BEGIN
	UPDATE VPM_DATA.TEMP_GENDER_2
	SET MAN_PRODUCT_SCORE = ((CASE	 WHEN COALESCE(ACTIVE_M,0) <= ACTIVE_CUT	THEN (CAST(COALESCE(ACTIVE_M,0) AS FLOAT)-0)/(ACTIVE_CUT-0)
									 WHEN COALESCE(ACTIVE_M,0) > ACTIVE_CUT THEN 1 
									 ELSE 0 END)*0.3)
							+ ((CASE WHEN COALESCE(DECILE_M,0) <= 10 THEN COALESCE(1.1-(CAST(DECILE_M AS FLOAT)/10),0)
									 ELSE 0 END)*0.3)
							+ ((CASE WHEN COALESCE(FREQ,0) <= FREQ_CUT THEN (COALESCE(FREQ,0)-0)/(FREQ_CUT-0)
									 WHEN COALESCE(FREQ,0) > FREQ_CUT THEN 1
									 ELSE 0 END)*0.2)
							+ ((CASE WHEN COALESCE(QTY_DAY,0) <= QTY_DAY_CUT THEN (CAST(COALESCE(QTY_DAY,0) AS FLOAT)-0)/(QTY_DAY_CUT-0)
									 WHEN COALESCE(QTY_DAY,0) > QTY_DAY_CUT THEN 1
									 ELSE 0 END)*0.2)
	WHERE PRODUCT_GENDER = 'MALE';	
END $$;


-- WOMAN_PRODUCT_SCORE
DO $$
DECLARE ACTIVE_CUT FLOAT := (SELECT MIN(ACTIVE_M) FROM (
								SELECT CUSTOMER_ID, ACTIVE_M, PERCENT_RANK() OVER(ORDER BY ACTIVE_M DESC NULLS LAST) AS PCT_RANK
								FROM VPM_DATA.TEMP_GENDER_2
								WHERE PRODUCT_GENDER = 'FEMALE') A
							WHERE PCT_RANK <= 0.2); 
		FREQ_CUT FLOAT := (SELECT MIN(FREQ) FROM (
								SELECT CUSTOMER_ID, FREQ, PERCENT_RANK() OVER(ORDER BY FREQ DESC NULLS LAST) AS PCT_RANK
								FROM VPM_DATA.TEMP_GENDER_2 
								WHERE PRODUCT_GENDER = 'FEMALE') A
							WHERE PCT_RANK <= 0.2);
		QTY_DAY_CUT FLOAT := (SELECT MIN(QTY_DAY) FROM (
								SELECT CUSTOMER_ID, QTY_DAY, PERCENT_RANK() OVER(ORDER BY QTY_DAY DESC NULLS LAST) AS PCT_RANK 
								FROM VPM_DATA.TEMP_GENDER_2 
								WHERE PRODUCT_GENDER = 'FEMALE') A	
							WHERE PCT_RANK <= 0.2);
BEGIN
	UPDATE VPM_DATA.TEMP_GENDER_2
	SET WOMAN_PRODUCT_SCORE = ((CASE	 WHEN COALESCE(ACTIVE_M,0) <= ACTIVE_CUT	THEN (CAST(COALESCE(ACTIVE_M,0) AS FLOAT)-0)/(ACTIVE_CUT-0)
										 WHEN COALESCE(ACTIVE_M,0) > ACTIVE_CUT THEN 1 
										 ELSE 0 END)*0.3)
								+ ((CASE WHEN COALESCE(DECILE_M,0) <= 10 THEN COALESCE(1.1-(CAST(DECILE_M AS FLOAT)/10),0)
										 ELSE 0 END)*0.3)
								+ ((CASE WHEN COALESCE(FREQ,0) <= FREQ_CUT THEN (COALESCE(FREQ,0)-0)/(FREQ_CUT-0)
										 WHEN COALESCE(FREQ,0) > FREQ_CUT THEN 1
										 ELSE 0 END)*0.2)
								+ ((CASE WHEN COALESCE(QTY_DAY,0) <= QTY_DAY_CUT THEN (CAST(COALESCE(QTY_DAY,0) AS FLOAT)-0)/(QTY_DAY_CUT-0)
										 WHEN COALESCE(QTY_DAY,0) > QTY_DAY_CUT THEN 1
										 ELSE 0 END)*0.2)
	WHERE PRODUCT_GENDER = 'FEMALE';
END $$;


UPDATE VPM_DATA.PM_SVOC
SET MAN_SUM_AMT = A.SUM_AMT
	,MAN_ACTIVE_M = A.ACTIVE_M
	,MAN_FREQ = A.FREQ
	,MAN_AVG_AMT_M = A.AVG_AMT_M
	,MAN_DECILE_M = A.DECILE_M
	,MAN_QTY_DAY = A.QTY_DAY
	,MAN_PRODUCT_SCORE = A.MAN_PRODUCT_SCORE
FROM VPM_DATA.TEMP_GENDER_2 A
WHERE A.CUSTOMER_ID = PM_SVOC.CUSTOMER_ID
AND A.PRODUCT_GENDER = 'MALE'
;

UPDATE VPM_DATA.PM_SVOC
SET WOMAN_SUM_AMT = A.SUM_AMT
	,WOMAN_ACTIVE_M = A.ACTIVE_M
	,WOMAN_FREQ = A.FREQ
	,WOMAN_AVG_AMT_M = A.AVG_AMT_M
	,WOMAN_DECILE_M = A.DECILE_M
	,WOMAN_QTY_DAY = A.QTY_DAY
	,WOMAN_PRODUCT_SCORE = A.WOMAN_PRODUCT_SCORE
FROM VPM_DATA.TEMP_GENDER_2 A
WHERE A.CUSTOMER_ID = PM_SVOC.CUSTOMER_ID
AND A.PRODUCT_GENDER = 'FEMALE'
;

DROP TABLE IF EXISTS VPM_DATA.TEMP_GENDER;
DROP TABLE IF EXISTS VPM_DATA.TEMP_TABLE_STAGING_1;
DROP TABLE IF EXISTS VPM_DATA.TEMP_TABLE_STAGING_2;
DROP TABLE IF EXISTS VPM_DATA.TEMP_TABLE_STAGING_3;
DROP TABLE IF EXISTS VPM_DATA.TEMP_TABLE;
DROP TABLE IF EXISTS VPM_DATA.TEMP_GENDER_2;

UPDATE VPM_DATA.SALESSKU_LOG_TIME SET end_time = now() WHERE task = 'Step1.3.9 Enrich Customer Behaviour Data Mart (Men_Woman Purchase)' AND EXTRACT(MONTH FROM start_time) = EXTRACT(MONTH FROM now());