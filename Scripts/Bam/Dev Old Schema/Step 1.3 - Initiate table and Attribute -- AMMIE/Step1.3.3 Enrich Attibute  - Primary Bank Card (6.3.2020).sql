-- Getting Primary Bank Card from Payment Table 
-- count ticket 

--25m 17s --16m 57s
INSERT INTO VPM_DATA.SALESSKU_LOG_TIME(task,start_time) VALUES ('Step1.3.3 Enrich Attibute  - Primary Bank Card',NOW());

DROP TABLE IF EXISTS VPM_DATA.TMP_CUST_CARD;
SELECT CUSTOMER_ID,CARD_DESC,ISSUING_BANK,DAY_VISIT_2Y
INTO VPM_DATA.TMP_CUST_CARD 
FROM VPM_DATA.PM_CUSTOMER_CC
WHERE UPPER(PAYMENT_CARD_TYPE) = 'CREDIT'
;
DROP TABLE IF EXISTS VPM_DATA.TIC_RANK;
SELECT *, 
ROW_NUMBER() OVER ( PARTITION BY CUSTOMER_ID ORDER BY DAY_VISIT_2Y DESC NULLS LAST) AS ROW_NUM 
INTO VPM_DATA.TIC_RANK 
FROM VPM_DATA.TMP_CUST_CARD
WHERE ISSUING_BANK IS NOT NULL 
;
CREATE INDEX TIC_RANK_ROW_CUS ON VPM_DATA.TIC_RANK
(
ROW_NUM,
CUSTOMER_ID
)
;

-- update the most used card as primary bank card
UPDATE VPM_DATA.PM_SVOC
SET PRIMARY_BANK_CARD = TMP.ISSUING_BANK, 
PRIMARY_BANK_CARD_FACE = TMP.CARD_DESC
FROM VPM_DATA.TIC_RANK TMP 
WHERE ROW_NUM = '1'
AND TMP.CUSTOMER_ID = PM_SVOC.CUSTOMER_ID
;

DROP TABLE IF EXISTS VPM_DATA.TMP_CUST_CARD;
DROP TABLE IF EXISTS VPM_DATA.TIC_RANK;

UPDATE VPM_DATA.SALESSKU_LOG_TIME SET end_time = now() WHERE task = 'Step1.3.3 Enrich Attibute  - Primary Bank Card' AND EXTRACT(MONTH FROM start_time) = EXTRACT(MONTH FROM now());