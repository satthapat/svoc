/*
	COOKING SCORE CUSTOMER SEGMENT
	- COOKING
	- BAKING
	- UTENSIL
*/

/* COOKING SCORE */
INSERT INTO VPM_DATA.SALESSKU_LOG_TIME(task,start_time) VALUES ('F.COOKING STEP2.0 - PM_SVOC_COOKING',NOW());
--1.FOOD 
--5m 6s
DO $$
DECLARE  RECENCY_D SMALLINT := (SELECT DISTINCT PERCENTILE_DISC(0.99) 
						WITHIN GROUP (ORDER BY FOOD_RECENCY DESC NULLS LAST)
					FROM VPM_DATA.PM_SVOC_COOKING);	
		VISIT_DAY_D SMALLINT := (SELECT DISTINCT PERCENTILE_DISC(0.99) 
						WITHIN GROUP (ORDER BY FOOD_VISIT_DAY NULLS FIRST)
					FROM VPM_DATA.PM_SVOC_COOKING
					WHERE FOOD_VISIT_DAY >= 1);
		NUM_TYPE_D FLOAT := (SELECT DISTINCT PERCENTILE_DISC(0.99) 
							WITHIN GROUP (ORDER BY FOOD_NUM_TYPE NULLS FIRST)
					FROM VPM_DATA.PM_SVOC_COOKING
					WHERE FOOD_NUM_TYPE >= 1);
		/*NUM_SUBCAT_D FLOAT := (SELECT DISTINCT PERCENTILE_DISC(0.99) 
							WITHIN GROUP (ORDER BY FOOD_NUM_SUBCAT) OVER ()
					FROM PM_SVOC_COOKING
					WHERE FOOD_NUM_TYPE >= 1); --0,33,9,26*/
BEGIN
	UPDATE VPM_DATA.PM_SVOC_COOKING
	SET COOKING_FOOD_SCORE = 
		0.2*(CASE WHEN FOOD_RECENCY <= RECENCY_D THEN 1 ELSE (1-(((FOOD_RECENCY-RECENCY_D)*1.0)/((180-RECENCY_D)*1.0))) END)
	+	0.4*(CASE WHEN FOOD_VISIT_DAY >= VISIT_DAY_D THEN 1 ELSE (FOOD_VISIT_DAY*1.0)/VISIT_DAY_D END)
	--+	0.15*(CASE WHEN FOOD_NUM_SUBCAT >= @NUM_SUBCAT THEN 1 ELSE (FOOD_NUM_SUBCAT*1.0)/@NUM_SUBCAT END)
	+	0.4*(CASE WHEN FOOD_NUM_TYPE >= NUM_TYPE_D THEN 1 ELSE (FOOD_NUM_TYPE*1.0)/NUM_TYPE_D END);
END $$;
/***********************************************************************************/			
	
--2.UTENSIL
DO $$
DECLARE  RECENCY_D SMALLINT := (SELECT DISTINCT PERCENTILE_DISC(0.95) 
						WITHIN GROUP (ORDER BY FLOOR((UTENSIL_RECENCY*1.0)/30) DESC NULLS LAST)
					FROM VPM_DATA.PM_SVOC_COOKING);
		VISIT_DAY_D SMALLINT := (SELECT DISTINCT PERCENTILE_DISC(0.95) 
						WITHIN GROUP (ORDER BY UTENSIL_VISIT_DAY NULLS FIRST)
					FROM VPM_DATA.PM_SVOC_COOKING
					WHERE UTENSIL_VISIT_DAY >= 1);
		NUM_TYPE_D FLOAT := (SELECT DISTINCT PERCENTILE_DISC(0.95) 
							WITHIN GROUP (ORDER BY UTENSIL_NUM_TYPE NULLS FIRST)
					FROM VPM_DATA.PM_SVOC_COOKING
					WHERE UTENSIL_NUM_TYPE >= 1);
BEGIN
	UPDATE VPM_DATA.PM_SVOC_COOKING
	SET COOKING_UTENSIL_SCORE = 
		0.2*(CASE WHEN UTENSIL_RECENCY <= RECENCY_D THEN 1 ELSE (1-(((FLOOR((UTENSIL_RECENCY*1.0)/30)-RECENCY_D)*1.0)/((12-RECENCY_D)*1.0))) END)
	+	0.4*(CASE WHEN UTENSIL_VISIT_DAY >= VISIT_DAY_D THEN 1 ELSE (UTENSIL_VISIT_DAY*1.0)/VISIT_DAY_D END)
	+	0.4*(CASE WHEN UTENSIL_NUM_TYPE >= NUM_TYPE_D THEN 1 ELSE (UTENSIL_NUM_TYPE*1.0)/NUM_TYPE_D END);
END $$;


--3.ELECTRONIC
DO $$
DECLARE  RECENCY_D SMALLINT := (SELECT DISTINCT PERCENTILE_DISC(0.95) 
						WITHIN GROUP (ORDER BY FLOOR((ELECTRONIC_RECENCY*1.0)/90) DESC)
					FROM VPM_DATA.PM_SVOC_COOKING);	
		VISIT_DAY_D SMALLINT := (SELECT DISTINCT PERCENTILE_DISC(0.95) 
						WITHIN GROUP (ORDER BY ELECTRONIC_VISIT_DAY)
					FROM VPM_DATA.PM_SVOC_COOKING
					WHERE ELECTRONIC_VISIT_DAY >= 1);
		NUM_TYPE_D FLOAT := (SELECT DISTINCT PERCENTILE_DISC(0.95) 
							WITHIN GROUP (ORDER BY ELECTRONIC_NUM_TYPE)
					FROM VPM_DATA.PM_SVOC_COOKING
					WHERE ELECTRONIC_NUM_TYPE >= 1);
BEGIN
	UPDATE VPM_DATA.PM_SVOC_COOKING
	SET COOKING_ELECTRONIC_SCORE = 
		0.2*(CASE WHEN ELECTRONIC_RECENCY <= RECENCY_D THEN 1 ELSE (1-(((FLOOR((ELECTRONIC_RECENCY*1.0)/90)-RECENCY_D)*1.0)/((4-RECENCY_D)*1.0))) END)
	+	0.4*(CASE WHEN ELECTRONIC_VISIT_DAY >= VISIT_DAY_D THEN 1 ELSE (ELECTRONIC_VISIT_DAY*1.0)/VISIT_DAY_D END)
	+	0.4*(CASE WHEN ELECTRONIC_NUM_TYPE >= NUM_TYPE_D THEN 1 ELSE (ELECTRONIC_NUM_TYPE*1.0)/NUM_TYPE_D END);
END $$;

--4.KITCHENWARE

DO $$
DECLARE  RECENCY_D SMALLINT := (SELECT DISTINCT PERCENTILE_DISC(0.95) 
						WITHIN GROUP (ORDER BY FLOOR((KITCHENWARE_RECENCY*1.0)/90) DESC NULLS LAST)
					FROM VPM_DATA.PM_SVOC_COOKING);
		VISIT_DAY_D SMALLINT := (SELECT DISTINCT PERCENTILE_DISC(0.95) 
						WITHIN GROUP (ORDER BY KITCHENWARE_VISIT_DAY NULLS FIRST)
					FROM VPM_DATA.PM_SVOC_COOKING
					WHERE KITCHENWARE_VISIT_DAY >= 1);
		NUM_TYPE_D FLOAT := (SELECT DISTINCT PERCENTILE_DISC(0.95) 
							WITHIN GROUP (ORDER BY KITCHENWARE_NUM_TYPE NULLS FIRST)
					FROM VPM_DATA.PM_SVOC_COOKING
					WHERE KITCHENWARE_NUM_TYPE >= 1);
BEGIN
	UPDATE VPM_DATA.PM_SVOC_COOKING
	SET KITCHENWARE_SCORE = 
		0.2*(CASE WHEN KITCHENWARE_RECENCY <= RECENCY_D THEN 1 ELSE (1-(((FLOOR((KITCHENWARE_RECENCY*1.0)/90)-RECENCY_D)*1.0)/((4-RECENCY_D)*1.0))) END)
	+	0.4*(CASE WHEN KITCHENWARE_VISIT_DAY >= VISIT_DAY_D THEN 1 ELSE (KITCHENWARE_VISIT_DAY*1.0)/VISIT_DAY_D END)
	+	0.4*(CASE WHEN KITCHENWARE_NUM_TYPE >= NUM_TYPE_D THEN 1 ELSE (KITCHENWARE_NUM_TYPE*1.0)/NUM_TYPE_D END);
END $$;

UPDATE VPM_DATA.PM_SVOC_COOKING
SET COOKING_SCORE = CASE
							WHEN (	SELECT MAX(COL)
								FROM (VALUES (COALESCE(COOKING_FOOD_SCORE,0)),(COALESCE(COOKING_UTENSIL_SCORE,0))
								,(COALESCE(COOKING_ELECTRONIC_SCORE,0))) T (col))
								 = COOKING_FOOD_SCORE THEN COOKING_FOOD_SCORE
							WHEN (	SELECT MAX(COL)
								FROM (VALUES (COALESCE(COOKING_FOOD_SCORE,0)),(COALESCE(COOKING_UTENSIL_SCORE,0))
								,(COALESCE(COOKING_ELECTRONIC_SCORE,0))) T (col))
								 = COOKING_UTENSIL_SCORE THEN COOKING_UTENSIL_SCORE
							WHEN (	SELECT MAX(COL)
								FROM (VALUES (COALESCE(COOKING_FOOD_SCORE,0)),(COALESCE(COOKING_UTENSIL_SCORE,0))
								,(COALESCE(COOKING_ELECTRONIC_SCORE,0))) T (col))
								 = COOKING_ELECTRONIC_SCORE THEN COOKING_ELECTRONIC_SCORE
							END
;

UPDATE VPM_DATA.PM_SVOC_COOKING
SET COOKING_FLAG = CASE WHEN COOKING_SCORE >= 0.8 THEN 1 END;

UPDATE VPM_DATA.PM_SVOC_COOKING
SET KITCHENWARE_LOVER_FLAG = NULL;

UPDATE VPM_DATA.PM_SVOC_COOKING
SET KITCHENWARE_LOVER_FLAG = CASE WHEN KITCHENWARE_SCORE >= 0.8 THEN 1 END
WHERE COOKING_FLAG IS NULL;

--SELECT COUNT(DISTINCT CUSTOMER_ID)
--FROM PM_SVOC_COOKING	
--WHERE COOKING_FLAG = 1				


--SELECT COUNT(DISTINCT CUSTOMER_ID)
--FROM PM_SVOC_COOKING	
--WHERE KITCHENWARE_LOVER_FLAG = 1


/*********************************************************************************************/	

/* BAKING */

-- CUSTOMER LEVEL

DROP TABLE IF EXISTS VPM_DATA.BAKING_TXN;
SELECT CUSTOMER_ID
      ,BU_NAME,SKU_CODE,TXN_DATE
      ,SUBCLASS_NAME,SUB_CAT_NAME--,PRODUCT_NAME
      ,GROUPING_CAT,CATEGORY_FINAL,TYPE_FINAL
      ,HOME_GROUP,HOME_PRODUCT_TYPE,TXN_AMT
INTO VPM_DATA.BAKING_TXN
FROM VPM_DATA.PM_SVOC_COOKING_TXN
WHERE HOME_GROUP = 'FOOD' AND TYPE_FINAL = 'BAKING'
OR UPPER(TRIM(SUBCLASS_NAME)) IN ('BAKEWARE','BAKING UTENSILS, MIXER BOWL')
AND UPPER(TRIM(SUBCLASS_NAME)) NOT IN ('PLAIN FLOUR');

DO $$
DECLARE END_DATE DATE := CAST((EXTRACT(YEAR FROM NOW()))::TEXT||'-'||EXTRACT(MONTH FROM NOW())::TEXT||'-05' AS DATE);
BEGIN
	DROP TABLE IF EXISTS VPM_DATA.PM_SVOC_COOKING_BAKING;
	CREATE TABLE VPM_DATA.PM_SVOC_COOKING_BAKING AS
	SELECT	 CUSTOMER_ID
			,COUNT(DISTINCT SUBCLASS_NAME) AS CNT_SUBCLASS
			,COUNT(DISTINCT CASE WHEN SUB_CAT_NAME IS NULL THEN SUBCLASS_NAME ELSE SUB_CAT_NAME END) AS CNT_SUBCAT
			,COUNT(DISTINCT TXN_DATE) AS VISIT_DAY
			,SUM(TXN_AMT) AS SPENDING
			,SUM(TXN_AMT)/COUNT(DISTINCT TXN_DATE) AS SPENDING_PER_VISIT
			,vpm_data.datediff('day',MAX(TXN_DATE),END_DATE) AS RECENCY
	FROM VPM_DATA.BAKING_TXN
	GROUP BY CUSTOMER_ID;
END $$;

UPDATE VPM_DATA.PM_SVOC_COOKING
SET	 BAKING_CNT_SUBCLASS = A.CNT_SUBCLASS
	,BAKING_CNT_SUBCAT = A.CNT_SUBCAT
	,BAKING_VISIT_DAY = A.VISIT_DAY
	,BAKING_SPENDING = A.SPENDING
	,BAKING_SPENDING_PER_VISIT = A.SPENDING_PER_VISIT
	,BAKING_RECENCY = A.RECENCY
FROM VPM_DATA.PM_SVOC_COOKING_BAKING A
WHERE A.CUSTOMER_ID = PM_SVOC_COOKING.CUSTOMER_ID;

DO $$
DECLARE RECENCY_D SMALLINT := (SELECT DISTINCT PERCENTILE_DISC(0.95) 
						WITHIN GROUP (ORDER BY FLOOR((BAKING_RECENCY*1.0)/90) DESC)
					FROM VPM_DATA.PM_SVOC_COOKING);	
		VISIT_DAY_D SMALLINT := (SELECT DISTINCT PERCENTILE_DISC(0.95) 
						WITHIN GROUP (ORDER BY BAKING_VISIT_DAY)
					FROM VPM_DATA.PM_SVOC_COOKING
					WHERE BAKING_VISIT_DAY >= 1);
		CNT_SUBCAT_D FLOAT := (SELECT DISTINCT PERCENTILE_DISC(0.95) 
							WITHIN GROUP (ORDER BY BAKING_CNT_SUBCAT)
					FROM VPM_DATA.PM_SVOC_COOKING
					WHERE BAKING_CNT_SUBCAT >= 1);
BEGIN
	UPDATE VPM_DATA.PM_SVOC_COOKING
	SET BAKING_SCORE = 
		0.2*(CASE WHEN BAKING_RECENCY <= RECENCY_D THEN 1 ELSE (1-(((FLOOR((BAKING_RECENCY*1.0)/90)-RECENCY_D)*1.0)/((4-RECENCY_D)*1.0))) END)
	+	0.4*(CASE WHEN BAKING_VISIT_DAY >= VISIT_DAY_D THEN 1 ELSE (BAKING_VISIT_DAY*1.0)/VISIT_DAY_D END)
	+	0.4*(CASE WHEN BAKING_CNT_SUBCAT >= CNT_SUBCAT_D THEN 1 ELSE (BAKING_CNT_SUBCAT*1.0)/CNT_SUBCAT_D END);
END $$;

UPDATE VPM_DATA.PM_SVOC_COOKING
SET BAKING_FLAG = 1
WHERE (COOKING_FLAG = 1)
AND BAKING_SCORE >= 0.7;

UPDATE VPM_DATA.SALESSKU_LOG_TIME SET end_time = now() WHERE task = 'F.COOKING STEP2.0 - PM_SVOC_COOKING' AND EXTRACT(MONTH FROM start_time) = EXTRACT(MONTH FROM now());