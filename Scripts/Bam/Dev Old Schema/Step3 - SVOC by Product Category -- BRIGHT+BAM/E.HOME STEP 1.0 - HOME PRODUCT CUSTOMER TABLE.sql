-- 10 HOURS(DEC2019), LESS THAN 3 HOURS (JAN2020)
/*
	Step 3 - Create PM_HOME_PRODUCT_CUST_SEGMENT 
*/
INSERT INTO VPM_DATA.SALESSKU_LOG_TIME(task,start_time) VALUES ('E.HOME STEP 1.0 - HOME PRODUCT CUSTOMER TABLE',NOW());

DROP TABLE IF EXISTS VPM_DATA.PM_HOME_PRODUCT_CUST_SEGMENT; --17m 12s
DO $$
DECLARE END_DATE DATE := CAST((EXTRACT(YEAR FROM NOW()))::TEXT||'-'||EXTRACT(MONTH FROM NOW())::TEXT||'-05' AS DATE);
BEGIN
	CREATE TABLE VPM_DATA.PM_HOME_PRODUCT_CUST_SEGMENT AS
	SELECT CUSTOMER_ID
		,COUNT(DISTINCT TXN_DATE) AS HH_DAY_VISIT
		,SUM(QTY) AS HH_SUM_QTY
		,SUM(TXN_AMT) AS HH_TTL_SPENDING
		,DATE_PART('day',END_DATE::timestamp - MAX(TXN_DATE)::timestamp) AS HH_RECENCY
		,COUNT(DISTINCT TYPE_FINAL) AS HH_NUM_TYPE_FINAL
		,MIN(TXN_DATE) AS HH_MIN_DATE
		,MAX(TXN_DATE) AS HH_MAX_DATE
		,SUM(QTY)/COUNT(DISTINCT TXN_DATE) AS HH_QTY_PER_VISIT
		--HOME CONSTRUCTION
		,COUNT(DISTINCT CASE WHEN HOME_PRODUCT_GROUP =  'HOME CONSTRUCTION' THEN TXN_DATE END) AS HOME_CONSTRUCTION_DAY_VISIT
		,COUNT(DISTINCT CASE WHEN HOME_PRODUCT_GROUP =  'HOME CONSTRUCTION' THEN TYPE_FINAL END) AS HOME_CONSTRUCTION_NUM_TYPE_FINAL
		,DATE_PART('day',END_DATE::timestamp - MAX(CASE WHEN HOME_PRODUCT_GROUP =  'HOME CONSTRUCTION' THEN TXN_DATE END)::timestamp) AS HOME_CONSTRUCTION_RECENCY
		,SUM(DISTINCT CASE WHEN HOME_PRODUCT_GROUP =  'HOME CONSTRUCTION' THEN TXN_AMT END) AS HOME_CONSTRUCTION_TTL_SPENDING
		--HOME FINISHING
		,COUNT(DISTINCT CASE WHEN HOME_PRODUCT_GROUP =  'HOME FINISHING' THEN TXN_DATE END) AS HOME_FINISHING_DAY_VISIT
		,COUNT(DISTINCT CASE WHEN HOME_PRODUCT_GROUP =  'HOME FINISHING' THEN TYPE_FINAL END) AS HOME_FINISHING_NUM_TYPE_FINAL
		,DATE_PART('day',END_DATE::timestamp - MAX(CASE WHEN HOME_PRODUCT_GROUP =  'HOME FINISHING' THEN TXN_DATE END)::timestamp) AS HOME_FINISHING_RECENCY
		,SUM(DISTINCT CASE WHEN HOME_PRODUCT_GROUP =  'HOME FINISHING' THEN TXN_AMT END) AS HOME_FINISHING_TTL_SPENDING
		--HOME FURNITURE
		,COUNT(DISTINCT CASE WHEN HOME_PRODUCT_GROUP = 'HOME FURNITURE' THEN TXN_DATE END) AS HOME_FURNITURE_DAY_VISIT
		,COUNT(DISTINCT CASE WHEN HOME_PRODUCT_GROUP = 'HOME FURNITURE' THEN TYPE_FINAL END) AS HOME_FURNITURE_NUM_TYPE_FINAL
		,DATE_PART('day',END_DATE::timestamp - MAX(CASE WHEN HOME_PRODUCT_GROUP =  'HOME FURNITURE' THEN TXN_DATE END)::timestamp) AS HOME_FURNITURE_RECENCY
		,SUM(CASE WHEN HOME_PRODUCT_GROUP = 'HOME FURNITURE' THEN TXN_AMT END) AS HOME_FURNITURE_TTL_SPENDING
		--HOME NECESSITY
		,COUNT(DISTINCT CASE WHEN HOME_PRODUCT_GROUP = 'HOME NECESSITY' THEN TXN_DATE END) AS HOME_NECESSITY_DAY_VISIT
		,COUNT(DISTINCT CASE WHEN HOME_PRODUCT_GROUP = 'HOME NECESSITY' THEN TYPE_FINAL END) AS HOME_NECESSITY_NUM_TYPE_FINAL
		,DATE_PART('day',END_DATE::timestamp - MAX(CASE WHEN HOME_PRODUCT_GROUP =  'HOME NECESSITY' THEN TXN_DATE END)::timestamp) AS HOME_NECESSITY_RECENCY
		,SUM(CASE WHEN HOME_PRODUCT_GROUP = 'HOME NECESSITY' THEN TXN_AMT END) AS HOME_NECESSITY_TTL_SPENDING
		--MAJOR
		,COUNT(DISTINCT CASE WHEN HOME_PRODUCT_GROUP = 'MAJOR HOUSEHOLD APPLIANCE' THEN TXN_DATE END) AS MAJOR_APPLIANCE_DAY_VISIT
		,COUNT(DISTINCT CASE WHEN HOME_PRODUCT_GROUP = 'MAJOR HOUSEHOLD APPLIANCE' THEN TYPE_FINAL END) AS MAJOR_APPLIANCE_NUM_TYPE_FINAL
		,DATE_PART('day',END_DATE::timestamp - MAX(CASE WHEN HOME_PRODUCT_GROUP = 'MAJOR HOUSEHOLD APPLIANCE' THEN TXN_DATE END)::timestamp) AS MAJOR_APPLIANCE_RECENCY
		,SUM(CASE WHEN HOME_PRODUCT_GROUP = 'MAJOR HOUSEHOLD APPLIANCE' THEN TXN_AMT END) AS MAJOR_APPLIANCE_TTL_SPENDING
		,CAST(NULL AS SMALLINT) AS BU_CDS_FREQ
		,CAST(NULL AS SMALLINT) AS BU_RBS_FREQ
		,CAST(NULL AS SMALLINT) AS BU_CHG_FREQ
		,CAST(NULL AS SMALLINT) AS BU_PWB_FREQ
		,CAST(NULL AS SMALLINT) AS BU_OTHER_FREQ
		,CAST(NULL AS VARCHAR(10)) AS BU_MAX_FREQ
		,CAST(NULL AS INT) AS MAX_QTY_1_MTH
		,CAST(NULL AS INT) AS SUM_QTY_12_MTH
		,CAST(NULL AS FLOAT) AS WEIGHTED_SUM_QTY
		,CAST(NULL AS VARCHAR(50)) AS PURPOSE
		,CAST(NULL AS VARCHAR(50)) AS COMMERCIAL_SEGMENT
		,CAST(NULL AS VARCHAR(50)) AS COMMERCIAL_SUBSEGMENT
		,CAST(NULL AS VARCHAR(100)) AS HOME_EVENT
		,CAST(NULL AS FLOAT) AS HOME_CONSTRUCTION_SCORE
		,CAST(NULL AS FLOAT) AS HOME_FINISHING_SCORE
		,CAST(NULL AS FLOAT) AS HOME_FURNITURE_SCORE
		,CAST(NULL AS FLOAT) AS MAJOR_APPLIANCE_SCORE
		,CAST(NULL AS FLOAT) AS HOME_NECESSITY_SCORE
		,CAST(NULL AS FLOAT) AS SUM_PRODUCT_CONFIDENT_SCORE
		,CAST(NULL AS FLOAT) AS SUM_PRODUCT_QTY_SCORE
		,CAST(NULL AS FLOAT) AS HOUSEMOVING_SCORE
		,CAST(NULL AS VARCHAR(50)) AS HOUSEMOVING_SCORE_LEVEL
		,CAST(NULL AS VARCHAR(50)) AS HOME_SEGMENT
		,CAST(NULL AS SMALLINT) AS HOME_GARDENING_RECENCY
		,CAST(NULL AS SMALLINT) AS HOME_GARDENING_VISIT
		,CAST(NULL AS SMALLINT) AS HOME_IMPROVEMENT_RECENCY
		,CAST(NULL AS SMALLINT) AS HOME_IMPROVEMENT_VISIT
		,CAST(NULL AS SMALLINT) AS HOME_DECOR_RECENCY
		,CAST(NULL AS SMALLINT) AS HOME_DECOR_VISIT
		,CAST(NULL AS FLOAT) AS LIFESTYLE_IMPROVEMENT_SCORE
		,CAST(NULL AS FLOAT) AS LIFESTYLE_GARDENING_SCORE
		,CAST(NULL AS FLOAT) AS LIFESTYLE_DECOR_SCORE
		,CAST(NULL AS INT) AS LIFESTYLE_GARDENING_FLAG
		,CAST(NULL AS INT) AS LIFESTYLE_IMPROVEMENT_FLAG
		,CAST(NULL AS INT) AS LIFESTYLE_DECOR_FLAG
	FROM VPM_DATA.PM_HOME_PRODUCT_SALESSKU_1YR
	WHERE CUSTOMER_ID IS NOT NULL AND CUSTOMER_ID <> ''
	GROUP BY CUSTOMER_ID;
END $$;
/******************************************************************************************************/

/* MOST VISIT BU */

DROP TABLE IF EXISTS VPM_DATA.DAY_VISIT;
SELECT CUSTOMER_ID
	  ,CASE WHEN BU_NAME IN ('HWS','TWD') THEN 'CHG' 
			WHEN BU_NAME IN ('CDS','RBS','PWB')  THEN BU_NAME
			ELSE 'OTHER' END AS BU_NAME
	  ,COUNT(DISTINCT TXN_DATE) AS DAY_VISIT
INTO VPM_DATA.DAY_VISIT	  	  
FROM VPM_DATA.PM_HOME_PRODUCT_SALESSKU_1YR
GROUP BY CUSTOMER_ID
		,CASE WHEN BU_NAME IN ('HWS','TWD') THEN 'CHG' 
			WHEN BU_NAME IN ('CDS','RBS','PWB')  THEN BU_NAME
			ELSE 'OTHER' END
;

UPDATE VPM_DATA.PM_HOME_PRODUCT_CUST_SEGMENT
SET  BU_CDS_FREQ = A.CDS
	,BU_RBS_FREQ = A.RBS
	,BU_PWB_FREQ = A.PWB
	,BU_CHG_FREQ = A.CHG
	,BU_OTHER_FREQ = A.OTHER
FROM (
		SELECT CUSTOMER_ID
				,CASE WHEN BU_NAME = 'CHG' THEN DAY_VISIT END AS CHG
				,CASE WHEN BU_NAME = 'CDS' THEN DAY_VISIT END AS CDS
				,CASE WHEN BU_NAME = 'RBS' THEN DAY_VISIT END AS RBS
				,CASE WHEN BU_NAME = 'PWB' THEN DAY_VISIT END AS PWB
				,CASE WHEN BU_NAME = 'OTHER' THEN DAY_VISIT END AS OTHER
		FROM VPM_DATA.DAY_VISIT
	 ) A
WHERE A.CUSTOMER_ID = PM_HOME_PRODUCT_CUST_SEGMENT.CUSTOMER_ID
;

UPDATE VPM_DATA.PM_HOME_PRODUCT_CUST_SEGMENT
SET BU_MAX_FREQ = CASE WHEN (	SELECT GREATEST(COALESCE(BU_CDS_FREQ,0),COALESCE(BU_RBS_FREQ,0)
								,COALESCE(BU_PWB_FREQ,0),COALESCE(BU_CHG_FREQ,0),COALESCE(BU_OTHER_FREQ,0))) = BU_CHG_FREQ THEN 'CHG'
						WHEN (	SELECT GREATEST(COALESCE(BU_CDS_FREQ,0),COALESCE(BU_RBS_FREQ,0)
								,COALESCE(BU_PWB_FREQ,0),COALESCE(BU_CHG_FREQ,0),COALESCE(BU_OTHER_FREQ,0))) = BU_CDS_FREQ THEN 'CDS'
						WHEN (	SELECT GREATEST(COALESCE(BU_CDS_FREQ,0),COALESCE(BU_RBS_FREQ,0)
								,COALESCE(BU_PWB_FREQ,0),COALESCE(BU_CHG_FREQ,0),COALESCE(BU_OTHER_FREQ,0))) = BU_RBS_FREQ THEN 'RBS'
						WHEN (	SELECT GREATEST(COALESCE(BU_CDS_FREQ,0),COALESCE(BU_RBS_FREQ,0)
								,COALESCE(BU_PWB_FREQ,0),COALESCE(BU_CHG_FREQ,0),COALESCE(BU_OTHER_FREQ,0))) = BU_PWB_FREQ THEN 'PWB'	
						WHEN BU_OTHER_FREQ IS NOT NULL THEN 'OTHER'		
						END							
;

UPDATE VPM_DATA.SALESSKU_LOG_TIME SET end_time = now() WHERE task = 'E.HOME STEP 1.0 - HOME PRODUCT CUSTOMER TABLE' AND EXTRACT(MONTH FROM start_time) = EXTRACT(MONTH FROM now());
