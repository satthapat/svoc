DROP TABLE IF exists DIFF_TABLE;
WITH OLD_TABLE AS(SELECT a.skucode AS SKU_CODE,b.deptcode AS dept_code,COUNT(*) AS TXN_OLD,SUM(SPENDING) AS SALES_OLD
FROM analysis_data.salessku_rbs2020 a
INNER JOIN (SELECT DISTINCT skuid,deptcode FROM analysis_data.productmaster_cds) b ON a.skucode = b.skuid
WHERE transactiondate::date >= '2020-03-06' AND transactiondate::date <= '2020-12-31'
GROUP BY a.skucode,b.deptcode)
,NEW_TABLE AS (SELECT a.sku_id AS SKU_CODE,b.dept_id AS dept_code,COUNT(*) AS TXN_NEW,SUM(NET_PRICE_TOT) AS SALES_NEW
FROM analysis_data.sales_sku_rbs_2020 a
INNER JOIN (SELECT DISTINCT PAD_SKU_ID,dept_id FROM analysis_data.ms_product_cds) b ON a.sku_id = b.pad_sku_id
WHERE trans_date::date >= '2020-03-06' AND trans_date::date <= '2020-12-31'
GROUP BY a.sku_id,b.dept_id)
SELECT z.sku_code,a.dept_code,A.txn_new,a.sales_new,B.TXN_OLD,B.SALES_OLD
INTO TEMP TABLE DIFF_TABLE
FROM (SELECT DISTINCT sku_code::NUMERIC(38,0) AS sku_code,dept_code::numeric(38,0) AS dept_code
		FROM NEW_TABLE
		UNION
		SELECT DISTINCT sku_code::NUMERIC(38,0) AS sku_code,dept_code::numeric(38,0) AS dept_code
		FROM old_TABLE) z
LEFT join NEW_TABLE A ON z.sku_code = A.SKU_CODE::NUMERIC(38,0)
left JOIN OLD_TABLE B ON z.SKU_CODE = B.SKU_CODE::NUMERIC(38,0);

--new 6180065
--old 12506104

SELECT *
FROM (SELECT *,coalesce(sales_new,0)-coalesce(sales_old,0) AS diff_value
FROM diff_table) a
WHERE sku_code IN (6180065,12506104)
order by diff_value;
