/*CREATE KID SALSE SKU 1 Y*/

--DROP TABLE PM_SKUTAGGING_KIDS_SALESSKU_1YR_SEP
--SP_RENAME  'PM_SKUTAGGING_KIDS_SALESSKU_1YR' ,'PM_SKUTAGGING_KIDS_SALESSKU_1YR_NOV'
 SET tcp_keepalives_idle = 300;

INSERT INTO VPM_DATA.SALESSKU_LOG_TIME(task,start_time) VALUES ('Step1.2.6 Kid Sales SKU 1 Y',NOW());

INSERT INTO VPM_DATA.SALESSKU_LOG_TIME(task,start_time) VALUES ('KIDS STAGING 1',NOW());
DROP TABLE IF EXISTS VPM_DATA.TEMP_PM_SKUTAGGING_KIDS_SALESSKU_STAGING_1; --1hr 30mins
CREATE TABLE VPM_DATA.TEMP_PM_SKUTAGGING_KIDS_SALESSKU_STAGING_1 AS
SELECT A.CUSTOMER_ID,
	A.CARDNO,
	A.BUID,
	A.BRANCHID,
	--A.TXN_DATETIME,
	A.TXN_DATE,
	A.TICKET_NUM,
	A.SKU_CODE,
	A.TXN_AMT,
	A.DEPT_CODE,
	A.SUBDEPT_CODE,
	A.QTY,
	A.PRICE_PER_UNIT,
	A.BU_NAME,
	A.DEPT_NAME,
	A.SUBDEPT_NAME,
	A.CLASS_ID,
	A.CLASS_NAME,
	A.SUBCLASS_ID,
	A.SUBCLASS_NAME,
	A.BRANDNAME,
	A.CLEANED_BRANDNAME,
	A.PRODUCT_NAME,
	A.GROUPING_CAT,
	COALESCE(B.CATEGORY_FINAL,A.CATEGORY_FINAL) AS CATEGORY_FINAL,
	COALESCE(B.TYPE_FINAL,A.TYPE_FINAL) AS TYPE_FINAL,
	--A.GENDER,
	A.TXN_DAY,
	A.WEEKDAY_FLAG,
	A.TXN_TIME,
	A.CNT_MINUTES,
	A.TIME_SLOT,
	A.IS_HOLIDAY,
	A.WORKING_DAY_FLAG,
	A.WORKING_DAY_OFFPEAK,
	A.BUCode,
	B.KID_GENDER_FINAL,
	B.KIDS_STAGE_FINAL,
	B.DIAPER_SIZE,
	B.DIAPER_PCS,
	B.REF_DATE_FROM_T0,
	CAST(NULL AS VARCHAR(50)) AS KID_BRAND_CLUSTER,
	CAST(NULL AS  VARCHAR(50)) AS EDITED_CAT_GROUP,
	CAST(NULL AS  VARCHAR(50)) AS KID_LUXURY_SEGMENT,
	CAST(NULL AS  VARCHAR(50)) AS KID_LUXURY_SEGMENT_V2,
	CAST(NULL AS  VARCHAR(50)) AS TOY_LUXURY_SEGMENT,
	CAST(NULL AS  VARCHAR(50)) AS NECC_LUXURY_SEGMENT,
	CAST(NULL AS  VARCHAR(50)) AS FASHION_LUXURY_SEGMENT,
	CAST(NULL AS  VARCHAR(50)) AS TRAVEL_LUXURY_SEGMENT
	,C.REVISED_CATEGORY_LEVEL_1
    ,C.REVISED_CATEGORY_LEVEL_2
    ,C.REVISED_CATEGORY_LEVEL_3
FROM VPM_DATA.PM_ALL_SALESSKU_1YR A
INNER JOIN VPM_DATA.PM_SKUTAGGING_KIDS_PRODUCT_MASTER B
ON CASE WHEN A.BUID IN ('4','176') THEN '4' 
		WHEN A.BUID IN ('1','2','71','998') THEN '1' --CDS/RBS REVISED PRODUCT MASTER
		WHEN A.BUID IN ('15','56') THEN '15'
		WHEN A.BUID IN ('143','144') THEN '143'
	ELSE A.BUID END 
	= CASE WHEN B.BUID IN ('4','176') THEN '4' 
		WHEN B.BUID IN ('1','2','71','998') THEN '1' --CDS/RBS REVISED PRODUCT MASTER
		WHEN B.BUID IN ('15','56') THEN '15'
		WHEN B.BUID IN ('143','144') THEN '143'
	ELSE B.BUID END 
AND A.SKU_CODE = B.SKU_CODE
LEFT JOIN VPM_DATA.PM_PRODUCT_MASTER C
ON CASE WHEN A.BUID IN ('4','176') THEN '4' 
		WHEN A.BUID IN ('1','2','71','998') THEN '1' --CDS/RBS REVISED PRODUCT MASTER
		WHEN A.BUID IN ('15','56') THEN '15'
		WHEN A.BUID IN ('143','144') THEN '143'
	ELSE A.BUID END 
	= CASE WHEN C.BUID IN ('4','176') THEN '4' 
		WHEN C.BUID IN ('1','2','71','998') THEN '1' --CDS/RBS REVISED PRODUCT MASTER
		WHEN C.BUID IN ('15','56') THEN '15'
		WHEN C.BUID IN ('143','144') THEN '143'
	ELSE C.BUID END 
AND A.SKU_CODE = C.SKU_CODE;
UPDATE VPM_DATA.SALESSKU_LOG_TIME SET end_time = now() WHERE task = 'KIDS STAGING 1' AND EXTRACT(MONTH FROM start_time) = EXTRACT(MONTH FROM now());

--27m 36s
INSERT INTO VPM_DATA.SALESSKU_LOG_TIME(task,start_time) VALUES ('KIDS STAGING 2',NOW());
DROP TABLE IF EXISTS VPM_DATA.TEMP_PM_SKUTAGGING_KIDS_SALESSKU_STAGING_2; --20m 19s
CREATE TABLE VPM_DATA.TEMP_PM_SKUTAGGING_KIDS_SALESSKU_STAGING_2 AS
SELECT A.CUSTOMER_ID,
	A.CARDNO,
	A.BUID,
	A.BRANCHID,
	--A.TXN_DATETIME,
	A.TXN_DATE,
	A.TICKET_NUM,
	A.SKU_CODE,
	A.TXN_AMT,
	A.DEPT_CODE,
	A.SUBDEPT_CODE,
	A.QTY,
	A.PRICE_PER_UNIT,
	A.BU_NAME,
	A.DEPT_NAME,
	A.SUBDEPT_NAME,
	A.CLASS_ID,
	A.CLASS_NAME,
	A.SUBCLASS_ID,
	A.SUBCLASS_NAME,
	A.BRANDNAME,
	A.CLEANED_BRANDNAME,
	A.PRODUCT_NAME,
	A.GROUPING_CAT,
	COALESCE(B.CATEGORY_FINAL,A.CATEGORY_FINAL) AS CATEGORY_FINAL,
	COALESCE(B.TYPE_FINAL,A.TYPE_FINAL) AS TYPE_FINAL,
	--A.GENDER,
	A.TXN_DAY,
	A.WEEKDAY_FLAG,
	A.TXN_TIME,
	A.CNT_MINUTES,
	A.TIME_SLOT,
	A.IS_HOLIDAY,
	A.WORKING_DAY_FLAG,
	A.WORKING_DAY_OFFPEAK,
	A.BUCode,
	B.KID_GENDER_FINAL,
	B.KIDS_STAGE_FINAL,
	B.DIAPER_SIZE,
	B.DIAPER_PCS,
	B.REF_DATE_FROM_T0,
	CAST(NULL AS VARCHAR(50)) AS KID_BRAND_CLUSTER,
	CAST(NULL AS  VARCHAR(50)) AS EDITED_CAT_GROUP,
	CAST(NULL AS  VARCHAR(50)) AS KID_LUXURY_SEGMENT,
	CAST(NULL AS  VARCHAR(50)) AS KID_LUXURY_SEGMENT_V2,
	CAST(NULL AS  VARCHAR(50)) AS TOY_LUXURY_SEGMENT,
	CAST(NULL AS  VARCHAR(50)) AS NECC_LUXURY_SEGMENT,
	CAST(NULL AS  VARCHAR(50)) AS FASHION_LUXURY_SEGMENT,
	CAST(NULL AS  VARCHAR(50)) AS TRAVEL_LUXURY_SEGMENT
	,C.REVISED_CATEGORY_LEVEL_1
    ,C.REVISED_CATEGORY_LEVEL_2
    ,C.REVISED_CATEGORY_LEVEL_3
FROM VPM_DATA.PM_TOPS_SALESSKU_1YR A
INNER JOIN VPM_DATA.PM_SKUTAGGING_KIDS_PRODUCT_MASTER B
	ON A.BUID = B.BUID 
AND A.SKU_CODE = B.SKU_CODE
LEFT JOIN VPM_DATA.PM_PRODUCT_MASTER C
ON A.BUID = C.BUID
	AND A.SKU_CODE = C.SKU_CODE;
UPDATE VPM_DATA.SALESSKU_LOG_TIME SET end_time = now() WHERE task = 'KIDS STAGING 2' AND EXTRACT(MONTH FROM start_time) = EXTRACT(MONTH FROM now());

INSERT INTO VPM_DATA.SALESSKU_LOG_TIME(task,start_time) VALUES ('KIDS STAGING 3',NOW());
DROP TABLE IF EXISTS VPM_DATA.TEMP_PM_SKUTAGGING_KIDS_SALESSKU_STAGING_3; --8m 57s
CREATE TABLE VPM_DATA.TEMP_PM_SKUTAGGING_KIDS_SALESSKU_STAGING_3 AS
SELECT A.CUSTOMER_ID,
	A.CARDNO,
	A.BUID,
	A.BRANCHID,
	--A.TXN_DATETIME,
	A.TXN_DATE,
	A.TICKET_NUM,
	A.SKU_CODE,
	A.TXN_AMT,
	A.DEPT_CODE,
	A.SUBDEPT_CODE,
	A.QTY,
	A.PRICE_PER_UNIT,
	A.BU_NAME,
	A.DEPT_NAME,
	A.SUBDEPT_NAME,
	A.CLASS_ID,
	A.CLASS_NAME,
	A.SUBCLASS_ID,
	A.SUBCLASS_NAME,
	A.BRANDNAME,
	A.CLEANED_BRANDNAME,
	A.PRODUCT_NAME,
	A.GROUPING_CAT,
	COALESCE(B.CATEGORY_FINAL,A.CATEGORY_FINAL) AS CATEGORY_FINAL,
	COALESCE(B.TYPE_FINAL,A.TYPE_FINAL) AS TYPE_FINAL,
	--A.GENDER,
	A.TXN_DAY,
	A.WEEKDAY_FLAG,
	A.TXN_TIME,
	A.CNT_MINUTES,
	A.TIME_SLOT,
	A.IS_HOLIDAY,
	A.WORKING_DAY_FLAG,
	A.WORKING_DAY_OFFPEAK,
	A.BUCode,
	B.KID_GENDER_FINAL,
	B.KIDS_STAGE_FINAL,
	B.DIAPER_SIZE,
	B.DIAPER_PCS,
	B.REF_DATE_FROM_T0,
	CAST(NULL AS VARCHAR(50)) AS KID_BRAND_CLUSTER,
	CAST(NULL AS VARCHAR(50)) AS EDITED_CAT_GROUP,
	CAST(NULL AS VARCHAR(50)) AS KID_LUXURY_SEGMENT,
	CAST(NULL AS VARCHAR(50)) AS KID_LUXURY_SEGMENT_V2,
	CAST(NULL AS VARCHAR(50)) AS TOY_LUXURY_SEGMENT,
	CAST(NULL AS VARCHAR(50)) AS NECC_LUXURY_SEGMENT,
	CAST(NULL AS VARCHAR(50)) AS FASHION_LUXURY_SEGMENT,
	CAST(NULL AS VARCHAR(50)) AS TRAVEL_LUXURY_SEGMENT
	,C.REVISED_CATEGORY_LEVEL_1
    ,C.REVISED_CATEGORY_LEVEL_2
    ,C.REVISED_CATEGORY_LEVEL_3
FROM VPM_DATA.PM_CFM_SALESSKU_1YR A
INNER JOIN VPM_DATA.PM_SKUTAGGING_KIDS_PRODUCT_MASTER B
ON A.BUID = B.BUID 
AND A.SKU_CODE = B.SKU_CODE
LEFT JOIN VPM_DATA.PM_PRODUCT_MASTER C
ON A.BUID = C.BUID
	AND A.SKU_CODE = C.SKU_CODE;
UPDATE VPM_DATA.SALESSKU_LOG_TIME SET end_time = now() WHERE task = 'KIDS STAGING 3' AND EXTRACT(MONTH FROM start_time) = EXTRACT(MONTH FROM now());

DROP TABLE IF EXISTS VPM_DATA.PM_SKUTAGGING_KIDS_SALESSKU_1YR; --2m 11s
SELECT * 
INTO VPM_DATA.PM_SKUTAGGING_KIDS_SALESSKU_1YR
FROM VPM_DATA.TEMP_PM_SKUTAGGING_KIDS_SALESSKU_STAGING_1
UNION ALL
SELECT * FROM VPM_DATA.TEMP_PM_SKUTAGGING_KIDS_SALESSKU_STAGING_2
UNION ALL
SELECT * FROM VPM_DATA.TEMP_PM_SKUTAGGING_KIDS_SALESSKU_STAGING_3;

UPDATE VPM_DATA.SALESSKU_LOG_TIME SET end_time = now() WHERE task = 'Step1.2.6 Kid Sales SKU 1 Y' AND EXTRACT(MONTH FROM start_time) = EXTRACT(MONTH FROM now());

/*
UPDATE PM_SKUTAGGING_KIDS_SALESSKU_1YR
SET TYPE_FINAL = 'ADULT DIAPERS'
WHERE TYPE_FINAL = 'DIAPERS' AND (CLEANED_BRANDNAME LIKE '%CERTAINTY%' OR CLEANED_BRANDNAME = 'LIFREE')*/
