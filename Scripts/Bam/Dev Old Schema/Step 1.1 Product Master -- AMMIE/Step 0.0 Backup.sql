
DO $$
DECLARE PREVIOUS_MONTH TEXT := (SELECT TO_CHAR(NOW()-INTERVAL '2 MONTH','MON'));
		CURRENT_MONTH TEXT := (SELECT TO_CHAR(NOW()-INTERVAL '1 MONTH','MON'));
		a TEXT;
		TABLE_CHANGE TEXT[] := ARRAY['vpm_data.pm_product_master','vpm_data.pm_skutagging_beauty_product_master','vpm_data.pm_skutagging_fashion_product_master',
								'vpm_data.pm_skutagging_food_product_master','vpm_data.pm_skutagging_kids_product_master','vpm_data.pm_skutagging_electronics_product_master',
								'vpm_data.pm_home_product_product_master','vpm_data.pm_skutagging_beauty_salessku_1yr','vpm_data.pm_skutagging_fashion_salessku_1yr',
								'vpm_data.pm_skutagging_kids_salessku_1yr','vpm_data.pm_skutagging_electronics_salessku_3yr','vpm_data.pm_skutagging_food_salessku_6m',
								'vpm_data.pm_home_product_salessku_1yr','vpm_data.pm_skutagging_beauty_cust_segment','vpm_data.pm_skutagging_fashion_cust_segment',
								'vpm_data.pm_skutagging_food_cust_segment','vpm_data.pm_skutagging_electronics_cust_segment','vpm_data.pm_home_product_cust_segment',
								'vpm_data.pm_svoc_cooking','vpm_data.pm_home_product_pet_cust','vpm_data.pm_svoc_car_ownership','vpm_data.pm_commercial_segment','vpm_data.pm_svoc'
								,'vpm_data.pm_customer_cc','vpm_data.pm_svoc_kids_new'];
		--TABLE_AFTER TEXT := RIGHT(TABLE_CHANGE,length(TABLE_CHANGE) - strpos(TABLE_CHANGE,'.'));
BEGIN
	FOREACH a IN ARRAY TABLE_CHANGE LOOP
		--EXECUTE format('DROP TABLE IF EXISTS '||a||'_'||PREVIOUS_MONTH);	
		EXECUTE format('ALTER TABLE '||a||' RENAME TO '||RIGHT(a,LENGTH(a) - STRPOS(a,'.'))||'_'||CURRENT_MONTH);
	END LOOP;
END $$;

/*
vpm_data.pm_svoc
vpm_data.pm_customer_cc
vpm_data.pm_svoc_kids_new
ALTER TABLE vpm_data.pm_product_master RENAME TO pm_product_master_jun;
ALTER TABLE vpm_data.pm_cgo_product_master RENAME TO pm_cgo_product_master_jun;
ALTER TABLE vpm_data.pm_skutagging_beauty_product_master RENAME TO pm_skutagging_beauty_product_master_jun;
ALTER TABLE vpm_data.pm_skutagging_fashion_product_master RENAME TO pm_skutagging_fashion_product_master_jun;
ALTER TABLE vpm_data.pm_skutagging_food_product_master RENAME TO pm_skutagging_food_product_master_jun;
ALTER TABLE vpm_data.pm_skutagging_kids_product_master RENAME TO pm_skutagging_kids_product_master_jun;
ALTER TABLE vpm_data.pm_skutagging_electronics_product_master RENAME TO pm_skutagging_electronics_product_master_jun;
ALTER TABLE vpm_data.pm_home_product_product_master RENAME TO pm_home_product_product_master_jun;
ALTER TABLE vpm_data.pm_skutagging_beauty_salessku_1yr RENAME TO pm_skutagging_beauty_salessku_1yr_jun;
ALTER TABLE vpm_data.pm_skutagging_fashion_salessku_1yr RENAME TO pm_skutagging_fashion_salessku_1yr_jun;
ALTER TABLE vpm_data.pm_skutagging_kids_salessku_1yr RENAME TO pm_skutagging_kids_salessku_1yr_jun;
ALTER TABLE vpm_data.pm_skutagging_electronics_salessku_3yr RENAME TO pm_skutagging_electronics_salessku_3yr_jun;
ALTER TABLE vpm_data.pm_skutagging_food_salessku_6m RENAME TO pm_skutagging_food_salessku_6m_jun;
ALTER TABLE vpm_data.pm_home_product_salessku_1yr RENAME TO pm_home_product_salessku_1yr_jun;
ALTER TABLE vpm_data.pm_skutagging_beauty_cust_segment RENAME TO pm_skutagging_beauty_cust_segment_jun;
ALTER TABLE vpm_data.pm_skutagging_fashion_cust_segment RENAME TO pm_skutagging_fashion_cust_segment_jun;
ALTER TABLE vpm_data.pm_skutagging_food_cust_segment RENAME TO pm_skutagging_food_cust_segment_jun;
ALTER TABLE vpm_data.pm_skutagging_electronics_cust_segment RENAME TO pm_skutagging_electronics_cust_segment_jun;
ALTER TABLE vpm_data.pm_home_product_cust_segment RENAME TO pm_home_product_cust_segment_jun;
ALTER TABLE vpm_data.pm_svoc_cooking RENAME TO pm_svoc_cooking_jun;
ALTER TABLE vpm_data.pm_home_product_pet_cust RENAME TO pm_home_product_pet_cust_jun;
ALTER TABLE vpm_data.pm_svoc_car_ownership RENAME TO pm_svoc_car_ownership_jun;
ALTER TABLE vpm_data.pm_commercial_segment RENAME TO pm_commercial_segment_jun;
*/