--7MINS TEST
DROP TABLE IF EXISTS vpm_backup.pm_1yr_fashion;
SELECT  A.CUSTOMER_ID,
		/** FASHION SPENDING **/				
		CAST( CAT2.FASHION_TOTAL_SPEND AS DECIMAL(18,2)) AS FASHION_TOTAL_SPEND,				
		CAST( SPEND.FASHION_SPEND_RATIO  AS DECIMAL(9,4)) AS FASHION_SPENDING_RATIO,				
		CAT2.FASHION_DAYS_SPEND,				
		CAST( CAT2.FASHION_LUXURY_SCORE AS DECIMAL(9,4)) AS FASHION_LUXURY_SCORE,				
		--�� ��� ������
		COALESCE(CASE WHEN CAT2.FASHION_TOTAL_SPEND > 0 OR CAT2.FASHION_LUXURY_SCORE > 0 OR GCS.GCS_FASHION_SPEND > 0 OR GCS.FASHION_LUXURY_SCORE_BY_BRAND > 0  
					THEN (CASE WHEN CAT2.FASHION_LUXURY_SEGMENT='SUPER LUXURY' OR GCS.FASHION_LUXURY_SEGMENT_BY_BRAND='SUPER LUXURY' THEN 'SUPER LUXURY'
								WHEN CAT2.FASHION_LUXURY_SEGMENT='LUXURY' OR GCS.FASHION_LUXURY_SEGMENT_BY_BRAND='LUXURY' THEN 'LUXURY'
								WHEN CAT2.FASHION_LUXURY_SEGMENT='ACCESSIBLE LUXURY' OR GCS.FASHION_LUXURY_SEGMENT_BY_BRAND='ACCESSIBLE LUXURY' THEN 'ACCESSIBLE LUXURY'
								WHEN CAT2.FASHION_LUXURY_SEGMENT='UPPER MASS' OR GCS.FASHION_LUXURY_SEGMENT_BY_BRAND='UPPER MASS' THEN 'UPPER MASS'
								WHEN COALESCE(CAT2.FASHION_LUXURY_SEGMENT,'MASS')='MASS' OR COALESCE(GCS.FASHION_LUXURY_SEGMENT_BY_BRAND,'MASS')='MASS' THEN 'MASS' 
								ELSE 'N/A' END) END ,'N/A') AS FASHION_LUXURY_SEGMENT,				
		CAST( CAT2.APPAREL_LUXURY_SCORE AS DECIMAL(9,4)) AS APPAREL_LUXURY_SCORE,				
		COALESCE(CASE WHEN CAT2.APPAREL_SPENDING > 0 OR CAT2.APPAREL_LUXURY_SCORE > 0 THEN COALESCE(CAT2.APPAREL_LUXURY_SEGMENT,'MASS') ELSE 'N/A' END,'N/A') AS APPAREL_LUXURY_SEGMENT,				
		COALESCE(CASE WHEN LENGTH(COALESCE(CAT2.APPAREL_PREFER_BRAND,'')) > 0 THEN CAT2.APPAREL_PREFER_BRAND ELSE 'N/A' END,'N/A') AS APPAREL_PREFER_BRAND, --ADDED 18/10/19				
		CAST( CAT2.LEATHER_GOODS_LUXURY_SCORE AS DECIMAL(9,4)) AS LEATHER_GOODS_LUXURY_SCORE,				
		COALESCE(CASE WHEN CAT2.LEATHER_GOODS_SPENDING > 0 OR CAT2.LEATHER_GOODS_LUXURY_SCORE > 0 THEN COALESCE(CAT2.LEATHER_GOODS_LUXURY_SEGMENT,'MASS') ELSE 'N/A' END,'N/A') AS LEATHER_GOODS_LUXURY_SEGMENT,				
		COALESCE(CASE WHEN LENGTH(COALESCE(CAT2.LEATHER_GOODS_PREFER_BRAND,'')) > 0 THEN CAT2.LEATHER_GOODS_PREFER_BRAND ELSE 'N/A' END,'N/A') AS LEATHER_GOODS_PREFER_BRAND, --ADDED 18/10/19				
		CAST( CAT2.ACCESSORIES_LUXURY_SCORE AS DECIMAL(9,4)) AS ACCESSORIES_LUXURY_SCORE,				
		COALESCE(CASE WHEN CAT2.ACCESSORIES_SPENDING > 0 OR CAT2.ACCESSORIES_LUXURY_SCORE > 0 THEN COALESCE(CAT2.ACCESSORIES_LUXURY_SEGMENT,'MASS') ELSE 'N/A' END,'N/A') AS ACCESSORIES_LUXURY_SEGMENT,				
		COALESCE(CASE WHEN LENGTH(COALESCE(CAT2.ACCESSORY_PREFER_BRAND,'')) > 0 THEN CAT2.ACCESSORY_PREFER_BRAND ELSE 'N/A' END,'N/A') AS ACCESSORIES_PREFER_BRAND, --ADDED 18/10/19				
		CAST( CAT2.FASHION_PROMO_SENSITIVITY_SCORE AS DECIMAL(9,4)) AS FASHION_PROMO_SENSITIVITY_SCORE,				
		COALESCE( CAT2.FASHION_PROMO_SENSITIVITY_SEGMENT,'NO SCORE') AS FASHION_PROMO_SENSITIVITY_SEGMENT,				
		CAST (CAT2.FASHION_AVG_DISCOUNT_PERCENT AS DECIMAL(9,4)) AS FASHION_AVG_DISCOUNT_PERCENT,				
		CAT2.SUM_QTY_FOR_HER AS FASHION_SUM_QTY_FOR_HER,				
		CAT2.SUM_QTY_FOR_HIM AS FASHION_SUM_QTY_FOR_HIM,				
		CAST(  CAT2.SUM_SPEND_FOR_HER AS DECIMAL(18,2)) AS FASHION_SUM_SPEND_FOR_HER,				
		CAST( CAT2.SUM_SPEND_FOR_HIM AS DECIMAL(18,2))  AS FASHION_SUM_SPEND_FOR_HIM,				
		CAST( CAT2.FASHION_PROMO_SENSITIVITY_SCORE_FEMALE AS DECIMAL(9,4)) AS FASHION_PROMO_SENSITIVITY_SCORE_FEMALE,  				
		CAST( CAT2.FASHION_PROMO_SENSITIVITY_SCORE_MALE AS DECIMAL(9,4)) AS FASHION_PROMO_SENSITIVITY_SCORE_MALE,      				
		CAST( CAT2.FASHION_NEW_ARRIVAL_SCORE AS DECIMAL(9,4)) AS FASHION_NEW_ARRIVAL_SCORE,				
		COALESCE(CAT2.NEW_ARRIVAL_SEGMENT,'N/A') AS FASHION_NEW_ARRIVAL_SEGMENT,				
		--CAST( CAT2.FASHION_HEALTH_SCORE AS DECIMAL(9,4)) AS FASHION_HEALTH_SCORE,-- DROP AS OF 05/20				
		--COALESCE((CASE WHEN CAT2.FASHION_HEALTH_FLAG = 1 THEN 'YES' ELSE 'N/A' END),'N/A') AS FASHION_HEALTH_FLAG, -- DROP AS OF 05/20				
		--CAST( CAT2.BUSINESS_SCORE AS DECIMAL(9,4)) AS FASHION_BUSINESS_SCORE,				
		--COALESCE((CASE WHEN CAT2.FASHION_BUSINESS_FLAG = 1 THEN 'YES' ELSE 'N/A' END),'N/A')  AS FASHION_BUSINESS_FLAG,		-- DROP AS OF 05/20				
		--drop 09/10/20 
		--CAST( S.MEN_BIZ_FASHION_SCORE AS DECIMAL(9,4)) AS MEN_BIZ_FASHION_SCORE,				
		--MEN.MEN_FASHION_SEGMENT AS MEN_FASHION_SEGMENT,				
		--COALESCE((CASE WHEN CAT2.FASHION_JEAN_FLAG = 1 THEN 'YES' ELSE 'N/A' END),'N/A') AS  FASHION_JEAN_FLAG,				-- DROP AS OF 04/20	
		--COALESCE(CAT2.FASHION_CHARM_FLAG,0) AS FASHION_CHARM_FLAG,				
		/*CAT2.FASHION_OVERSIZE_FLAG,*/				
		COALESCE(CAT2.MAIN_SIZE,'N/A') AS FASHION_SIZE,				
		CAST (/*W.SHIRT*/NULL AS DECIMAL(18,2)) AS MAX_SHIRT_PRICE,				
		CAST (W.TSHIRTANDPOLO AS DECIMAL(18,2)) AS MAX_TSHIRT_POLO_PRICE,				
		CAST (W.SPORTWEAR AS  DECIMAL(18,2)) AS MAX_SPORTWEAR_PRICE,				
		CAST (W.UNDERWEARANDNIGHTWEAR AS DECIMAL(18,2)) AS MAX_UNDERWEAR_PRICE,				
		CAST (W.PANTS AS DECIMAL(18,2)) AS MAX_PANTS_PRICE,				
		CAST (W.DRESS AS DECIMAL(18,2)) AS MAX_DRESS_PRICE,				
		CAST (NULL AS DECIMAL(18,2)) AS MAX_SUIT_PRICE,				
		CAST (W.FOOTWEAR AS DECIMAL(18,2)) AS MAX_FOOTWEAR_PRICE,				
		CAST (W.SNEAKERS AS DECIMAL(18,2)) AS MAX_SNEAKER_PRICE,				
		CAST (W.OUTERWEAR AS DECIMAL(18,2))AS MAX_OUTERWEAR_PRICE,				
		CAST (W.BAGS AS DECIMAL(18,2)) AS MAX_BAG_PRICE,				
		CAST (W.HANDBAG AS DECIMAL(18,2)) AS MAX_HANDBAG_PRICE,				
		CAST (W.WATCHES AS DECIMAL(18,2)) AS MAX_WATCH_PRICE,				
		CAST (W.JEWELRY AS DECIMAL(18,2))AS MAX_JEWELRY_PRICE, 
		--COALESCE((CASE WHEN FASHION.FASHION_WEDDING_GOWN_FLAG=1  THEN 'YES' ELSE 'N/A' END),'N/A') AS FASHION_WEDDING_GOWN_FLAG  ,
		COALESCE((CASE WHEN FASHION.NEW_EVENING_GOWN_FLAG=1 THEN 'YES' ELSE 'N/A' END), 'N/A') AS  FASHION_EVENING_GOWN_FLAG,
		COALESCE((CASE WHEN FASHION.NEW_LUXURY_THAI_DESIGNER_FLAG =1  THEN 'YES' ELSE 'N/A' END),'N/A') AS FASHION_LUXURY_THAI_DESIGNER_FLAG,
		--COALESCE((CASE WHEN FASHION.FASHION_TAILOR_MADE_TO_ORDER_FLAG =1  THEN 'YES' ELSE 'N/A' END),'N/A') AS FASHION_TAILOR_MADE_TO_ORDER_FLAG,
		COALESCE((CASE WHEN FASHION.NEW_MADAME_STYLE_FLAG =1  THEN 'YES' ELSE 'N/A' END),'N/A') AS FASHION_MADAME_STYLE_FLAG,
		--COALESCE((CASE WHEN FASHION.FASHION_MULTIBRAND_STORE_FLAG =1  THEN 'YES' ELSE 'N/A' END),'N/A') AS FASHION_MULTIBRAND_STORE_FLAG,
		COALESCE((CASE WHEN FASHION.NEW_OFFICE_AND_PROFESSIONAL_WEAR_FLAG=1  THEN 'YES' ELSE 'N/A' END),'N/A') AS FASHION_OFFICE_PROFESSIONAL_WEAR_FLAG,
		COALESCE((CASE WHEN FASHION.NEW_FAST_FASHION_FLAG=1  THEN 'YES' ELSE 'N/A' END),'N/A') AS FASHION_FAST_FASHION_FLAG,
		--CHANGE NAME FROM YOUNG_cSASSY_GIRLY 
		COALESCE((CASE WHEN FASHION.NEW_MATURE_FEMININE_FLAG=1  THEN 'YES' ELSE 'N/A' END),'N/A') AS FASHION_MATURE_FEMININE_FLAG,
		COALESCE((CASE WHEN FASHION.NEW_STREET_FLAG=1  THEN 'YES' ELSE 'N/A' END),'N/A') AS FASHION_STREET_STYLE_FLAG,
		--COALESCE((CASE WHEN FASHION.FASHION_JEANS_FLAG=1  THEN 'YES' ELSE 'N/A' END),'N/A') AS FASHION_JEANS_FLAG,
		COALESCE((CASE WHEN FASHION.NEW_EVERYDAY_BASIC_FLAG=1  THEN 'YES' ELSE 'N/A' END),'N/A') AS FASHION_EVERYDAY_BASIC_FLAG,
		--COALESCE((CASE WHEN FASHION.FASHION_CASUAL_FLAG=1  THEN 'YES' ELSE 'N/A' END),'N/A') AS FASHION_CASUAL_FLAG,
		COALESCE((CASE WHEN FASHION.NEW_YOUNG_FEMININE_FLAG=1  THEN 'YES' ELSE 'N/A' END),'N/A') AS FASHION_YOUNG_FEMININE_FLAG,
		--COALESCE((CASE WHEN FASHION.FASHION_MATERNITY_FLAG=1  THEN 'YES' ELSE 'N/A' END),'N/A') AS FASHION_MATERNITY_FLAG,
		COALESCE((CASE WHEN FASHION.NEW_SPORTS_FLAG=1  THEN 'YES' ELSE 'N/A' END),'N/A') AS FASHION_SPORTS_FLAG,
		COALESCE((CASE WHEN FASHION.NEW_ADVENTURE_FLAG=1  THEN 'YES' ELSE 'N/A' END),'N/A') AS FASHION_ADVENTURE_FLAG,
		COALESCE((CASE WHEN CAT2.FASHION_TRAVEL_FLAG = 1 THEN 'YES' ELSE 'N/A' END),'N/A')  AS FASHION_TRAVEL_FLAG
	INTO vpm_backup.pm_1yr_fashion
	FROM VPM_DATA.PM_SVOC A
	LEFT JOIN VPM_DATA.PM_SKUTAGGING_FASHION_CUST_SEGMENT CAT2 ON A.CUSTOMER_ID = CAT2.CUSTOMER_ID
	LEFT JOIN VPM_BACKUP.GCS_CUST_SVOC_SEGMENT GCS ON A.CUSTOMER_ID = GCS.CUSTOMERID
	LEFT JOIN VPM_DATA.AGG_CUSTOMER_SPENDING SPEND ON A.CUSTOMER_ID = SPEND.CUSTOMER_ID
	LEFT JOIN VPM_DATA.PM_SKUTAGGING_FASHION_CHARACTERISTICS FASHION ON A.CUSTOMER_ID=FASHION.CUSTOMER_ID
	LEFT JOIN VPM_BACKUP.T1C_CUST_ESTIMATED_WEALTH_V5 W ON A.CUSTOMER_ID = W.CUSTOMER_ID
;
